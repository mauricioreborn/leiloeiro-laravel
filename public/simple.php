<?php

require __DIR__.'/../vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__."/..");
$dotenv->load();

define('ENVIRONMENT', getenv("ENV"));

$start_time = microtime(TRUE);
$conex = mysqli_connect( getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));

if (mysqli_connect_errno()) {
    custom_header( "500 Internal Server Error" );
    echo "Failed to connect to MySQL: " . mysqli_connect_error().PHP_EOL;
    exit;
}

echo "connected in ".(microtime(TRUE)-$start_time).PHP_EOL;

$qry = mysqli_query($conex, "SELECT NOW()") or die(mysqli_error());
$now = mysqli_fetch_all($qry);
echo "now: ". $now[0][0] . PHP_EOL;

echo "Fim: " . (microtime(TRUE) - $start_time) . PHP_EOL;
