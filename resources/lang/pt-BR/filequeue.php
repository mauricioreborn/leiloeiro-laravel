<?php

return [
    'success' => 'arquivo adicionado com sucesso.',
    'company_not_found' => 'company :name não encontrada',
    'type_not_found' => 'tipo não encontrado'
    ];
