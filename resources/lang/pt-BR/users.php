<?php

return [
    'created' => 'O usuário foi cadastrado.',
    'updated' => 'O usuário foi atualizado.',
    'notFound' => 'Usuario não encontrado.',
];
