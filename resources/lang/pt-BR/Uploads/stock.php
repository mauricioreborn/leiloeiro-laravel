<?php

return [
    'title' => 'Estoque',
    'description' => 'Atualizar volume de estoque',
    'expired' => ':product | origem: :origin_code expirado.',
    'not_active' => ':product | origem: :origin_code bloqueado.',
    'without_product_price' => ':product | origem: :origin_code não tem preço configurado na UF.',
    'without_min_order' => ':product | origem: :origin_code | volume :volume não atinge o pedido minimo.',
    'invalid_weight_piece' => ':product está com a gramatura zerada.',
];
