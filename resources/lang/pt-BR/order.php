<?php

return [
    'boxes' => 'caixa|caixas',
    'reason_cancellation' => [
        'account_ballance' =>
            'O valor deste pedido supera seu limite de crédito (R$ :price). Este limite é atualizado diariamente.',
        'min_order' => 'O valor deste pedido não supera o valor de pedido mínimo (R$ :min_order).'
    ],
    'status' => [
        'canceled' => 'CANCELADO'
    ]
];
