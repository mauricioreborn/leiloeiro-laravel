<?php

return [
    'approval_percentage_gt_reject' =>
        'porcentagem de aprovação :approval_percentage maior que porcentagem de reprovação :reprove_percentage'
];
