<?php

return [
    'type.required' => 'Você deve informar um tipo.',
    'type.exists' => 'O tipo selecionado é inválido.',
    'scheduled_at.required' => 'Você deve informar uma data.',
    'scheduled_at.date' => 'O campo deve ser uma data válida.',
    'scheduled_at.date_format' => 'O campo deve ser uma data válida.',
    'scheduled_at.after' => 'A data deve ser maior do que a data de hoje.',
    'fefo_id.required_if' => 'Para campanhas de destaque, você deve selecionar um FEFO.',
];
