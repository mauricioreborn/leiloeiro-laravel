<?php
// phpcs:ignoreFile
return [
    'bid' => [
        'min_order' => [
            'label' => 'Cancelado',
            'mobile' => 'A indústria não tinha estoque para atender todo o volume (Kg) do seu lance, por isso o valor de pedido mínimo não foi atingido.',
            'class' => 'danger',
            'push' => [
                'title' => 'Lance :bid: CANCELADO',
                'message' => ':product. Valor parcial do lance não atingiu o pedido mínimo'
            ],
            'email' => [
                'label' => 'Cancelado',
                'color' => 'danger',
                'description' => 'A indústria não tinha estoque para atender todo o volume (Kg) do seu lance, por isso o valor de pedido mínimo não foi atingido.',
            ],
        ],
        'expired' => [
            'label' => 'Expirado',
            'mobile' => 'Seu lance expirou em :date.',
            'class' => 'dark',
            'push' => [
                'title' => 'Lance #:bid EXPIRADO',
                'message' => ':product. Expirado dia :expired_at',
            ],
            'email' => [
                'label' => 'Cancelado',
                'color' => 'neutral',
                'description' => 'Seu lance expirou no dia :date.',
            ],
        ],
        'price_rejected' => [
            'label' => 'Rejeitado',
            'mobile' => 'Seu lance não foi aprovado pela indústria. Utilize o histórico de lances na página do produto para saber os valores que estão sendo aceitos.',
            'class' => 'danger',
            'push' => [
                'title' => 'Lance #:bid REJEITADO',
                'message' => ':product - O lance de R$:price foi rejeitado',
            ],
            'email' => [
                'label' => 'Cancelado',
                'color' => 'danger',
                'description' => 'Seu lance não foi aprovado pela indústria. Utilize o histórico de lances na página do produto para saber os valores que estão sendo aceitos.',
            ],
        ],
        'invalid_fefo' => [
            'label' => 'Cancelado',
            'mobile' => 'Devido a indisponibilidade de estoque, a indústria não conseguiu atender o seu pedido.',
            'class' => 'danger',
            'push' => [
                'title' => 'Lance #:bid: CANCELADO',
                'message' => ':product. Falta de estoque',
            ],
            'email' => [
                'label' => 'Cancelado',
                'color' => 'danger',
                'description' => 'Devido a indisponibilidade de estoque, a indústria não conseguiu atender o seu pedido.',
            ],
        ],
        'customer_without_balance' => [
            'label' => 'Cancelado',
            'mobile' => 'Seu lance supera seu limite de crédito em R$ :account_balance. Este limite é atualizado diariamente.',
            'class' => 'danger',
            'push' => [
                'title' => 'Lance #:bid CANCELADO',
                'message' => ':product. Lance supera limite de crédito',
            ],
            'email' => [
                'label' => 'Cancelado',
                'color' => 'danger',
                'description' => 'Seu lance supera seu limite de crédito em R$ :account_balance. Este limite é atualizado diariamente.',
            ],
        ],
        'removed_by_user' => [
            'label' => 'Cancelado',
        ],
        'behaviour_rejection' => [
            'label' => 'Rejeitado',
            'email' => [
                'label' => 'Rejeitado',
                'color' => 'danger',
                'description' => 'Seu lance não foi aprovado pela indústria. Utilize o histórico de lances na página do produto para saber os valores que estão sendo aceitos.',
            ],
        ],
        'removed_by_user' => [
            'label' => 'Cancelado',
        ],
        'payment_denied' => [
            'label' => 'Cancelado',
        ],
        'behaviour_rejection' => [
            'label' => 'Rejeitado',
            'mobile' => 'Seu lance não foi aprovado pela indústria. Utilize o histórico de lances na página do produto para saber os valores que estão sendo aceitos.',
            'class' => 'danger',
            'email' => [
                'label' => 'Cancelado',
                'color' => 'danger',
                'description' => 'Seu lance não foi aprovado pela indústria. Utilize o histórico de lances na página do produto para saber os valores que estão sendo aceitos.',
            ],
            'push' => 'Lance :bid : REJEITADO :product_name. O lance de ::price foi rejeitado'
        ],
    ],
    'payment_denied' => [
        'mobile' => '"Pedido :order_code:CANCELADO. :product_name. Problemas no cartão de crédito"',
        'email' => 'Não conseguimos reservar o valor do lance no seu cartão de crédito. Verifique os dados do seu cartão e faça o pedido novamente.',
        'card_text' => 'Não conseguimos reservar o valor do lance no seu cartão de crédito. Verifique os dados do seu cartão e faça o pedido novamente.',
        'label' => 'Cancelado',
    ],
    'order' => [
        'invalid_fefo' => [
            'label' => 'Cancelado',
            'push' => [
                'title' => 'Pedido #:order_id: CANCELADO',
                'message' => 'Falta de estoque',
            ],
            'mobile' => 'Todos os itens do pedido foram cancelados devido a falta de estoque.',
            'email' => 'Todos os itens do pedido foram cancelados devido a falta de estoque.',
            'block' => 'CANCELADO',
            'class' => 'danger',
        ],
        'customer_without_balance' => [
            'label' => 'Cancelado',
            'push' => [
                'title' => 'Pedido #:order_id CANCELADO',
                'message' => ':product. Pedido supera limite de crédito',
            ],
            'mobile' => 'Seu pedido supera seu limite de crédito em R$ :account_balance. Este limite é atualizado diariamente.',
            'block' => 'CANCELADO',
            'class' => 'danger',
            'email' => 'Seu pedido supera seu limite de crédito em R$ :account_balance. Este limite é atualizado diariamente.',
        ],
        'min_order' => [
            'label' => 'Cancelado',
            'push' => [
                'title' => 'Pedido :order_id: CANCELADO',
                'message' => ':product. Valor parcial do pedido não atingiu o pedido mínimo'
            ],
            'mobile' => 'A indústria não tinha estoque para atender todo o volume (Kg) do seu pedido, por isso o valor de pedido mínimo não foi atingido.',
            'class' => 'danger',
            'block' => 'CANCELADO',
            'email' => 'A indústria não tinha estoque para atender todo o volume (Kg) do seu pedido, por isso o valor de pedido mínimo não foi atingido.'
        ],
    ],
];
