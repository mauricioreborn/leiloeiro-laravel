<?php

//phpcs:ignoreFile

return [
    'd0' => [
        'email' => [
            'subject' => "A Tirolez é a mais nova parceira da Souk, produtos com até 70% de desconto. Confira!",
            'slug' => [
                'base_1' => "souk_ativacao-tirolez_email_d0-base1",
                'base_2' => "souk_ativacao-tirolez_email_d0-base2",
            ],
            'link' => [
                'base_1' => "https://deep.souk.com.br/HsqrMzfU62",
                'base_2' => "https://deep.souk.com.br/I0DL7LsZ62",
            ],
        ],
        'push' => [
            'base_1' => [
                'title' => "Tá esperando o quê?",
                'message' => "A Tirolez agora também está na Souk. Como você já é cliente, pode começar a dar lances e fazer compras. Corre e vem negociar os melhores preços!",
                'campaign' => "souk_ativacao-tirolez_push_d0-base1",
            ],
            'base_2' => [
                'title' => "Tá esperando o quê?",
                'message' => "A Tirolez está agora na Souk e você como é cliente, já pode começar a dar lances e fazer compras. Corre e vem negociar direto com mais uma indústria!",
                'campaign' => "souk_ativacao-tirolez_push_d0-base2",
            ],
        ],
        'sms' => [
            'base_1' => "SOUK: A Tirolez já está disponível, por ser cliente você já pode comprar e dar lances, vem logo aproveitar os preços de lançamento: https://deep.souk.com.br/78OYnKQX62",
            'base_2' => "SOUK: A Tirolez chegou com preços que podem chegar a 70% de desconto e você ainda pode dar lance e conseguir melhores preços. Vem! https://deep.souk.com.br/Xa3mOhBZ62"
        ],
    ],
    'd1' => [
        'push' => [
            'base_1' => [
                'title' => "Descontos de até 70% nos produtos Tirolez!",
                'message' => "A Tirolez chegou com ótimos preços na Souk! Aproveite os preços de lançamento. Vai ficar de fora?",
                'campaign' => "souk_ativacao-tirolez_push_d1-base1"
            ],
            'base_2' => [
                'title' => "Compre novamente produtos da Tirolez!",
                'message' => "A Tirolez chegou com tudo na Souk e está com ótimos preços para você aproveitar. Agora é a hora!",
                'campaign' => "souk_ativacao-tirolez_push_d1-base2",
            ],
        ]
    ],
    'd3' => [
        'email' => [
            'subject' => "Lançamento da Tirolez está sendo um sucesso! Vai perder?",
            'slug' => [
                'base_1' => "souk_ativacao-tirolez_email_d3-base1",
                'base_2' => "souk_ativacao-tirolez_email_d3-base2",
            ],
            'link' => [
                'base_1' => "https://deep.souk.com.br/IThIQQi062",
                'base_2' => "https://deep.souk.com.br/pNgVfWm062",
            ],
        ],
        'push' => [
            'base_1' => [
                'title' => "Já deu uma olhadinha na loja da Tirolez?",
                'message' => "Vários produtos disponíveis, descontos de até 70% e você ainda pode negociar através do leilão. Aproveite agora mesmo!",
                'campaign' => "souk_ativacao-tirolez_push_d3-base1",
            ],
            'base_2' => [
                'title' => "Ficou mais prático e barato comprar Tirolez",
                'message' => "Volte a comprar da Tirolez utlizando a Souk, a ferramenta certa para fazer bons negócios. Os preços estão incríveis! Acesse e confira!",
                'campaign' => "souk_ativacao-tirolez_push_d3-base2",
            ],
        ]
    ],
    'd8' => [
        'email' => [
            'subject' => "Ficou mais fácil e prático comprar produtos Tirolez!",
            'slug' => [
                'base_1' => "souk_ativacao-tirolez_email_d8-base1",
                'base_2' => "souk_ativacao-tirolez_email_d8-base2",
            ],
            'link' => [
                'base_1' => "https://deep.souk.com.br/tWLKiOt062",
                'base_2' => "https://deep.souk.com.br/CMlzxgx062",
            ],
        ],
        'push' => [
            'base_1' => [
                'title' => "Queijo é bom demais né?",
                'message' => "A Tirolez conta com diversas categorias de queijos com a qualidade que você já conhece, né? Vem utilizar nosso leilão e pagar ainda mais barato!",
                'campaign' => "souk_ativacao-tirolez_push_d8-base1",
            ],
            'base_2' => [
                'title' => "Queijo é bom demais né?",
                'message' => "Agora você pode comprar os deliciosos queijos Tirolez com a facilidade da Souk. Vem utilizar nosso leilão e pagar ainda mais barato!",
                'campaign' => "souk_ativacao-tirolez_push_d8-base2",
            ],
        ]
    ],
];