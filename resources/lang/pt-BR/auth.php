<?php
// phpcs:disable


return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'logout' => 'Você foi desconectado.',
    'failed'   => 'Credenciais informadas não correspondem com nossos registros.',
    'throttle' => 'Você realizou muitas tentativas de login. Por favor, tente novamente em :seconds segundos.',
    'could_not_create_token' => 'Não foi possível criar o token de autenticação.',
    'token_expired' => 'Sua sessão expirou, faça login novamente.',
    'token_invalid' => 'Token de autenticação é inválido, faça login novamente.',
    'token_blacklisted' => 'Você acessou o sistema em outro dispositivo, faça login novamente.',
    'token_missing' => 'É necessário fazer login para acessar o sistema.',
    'access_denied' => 'Você não tem acesso, entre em contato com a administração.',
    'cnpj_duplicated' => 'O CNPJ já consta em nossa base, faça o login.',
    'cnpj_invalid' => 'CNPJ inválido.',
    'email_vefified_false' => 'Email não verificado, faça o cadastro novamente.',
    'register' => [
        'invalid_email' => 'Email inválido.',
        'cnpj_exists' => 'O CNPJ já consta em nossa base, faça o login.',
        'cnpj_in_use' => 'O valor informado para o campo cnpj já está em uso.',
        'invalid_email' => 'Email inválido',
        'neighborhood_string' => 'o bairro precisar ser uma string',
        'zipcode_min' => 'o cep precisa ser de :min digitos',
        'address_number_integer' => 'o numero do endereço precisa ser do tipo inteiro',
        'address_complement_string' => 'o complemento de endereço precisa ser uma string',
        'address_address_min' => 'o campo endereço deve conter no minimo :min caracteres ',
        'city_min' => 'o campo cidade deve conter no minimo :min caracteres',
        'social_reason_min' => 'o campo razão social deve conter no minimo :min caracteres',
    ],
    'change_password' => [
        'email_send' => 'Senha enviada para o e-mail cadastrado!',
        'reset_error' => 'Erro ao atualizar senha',
    ],

    'independent_client' => [
        'register_with_success' =>  'Cadastro realizado com sucesso! Aguarde nosso contato,vamos verificar seu CNPJ em nossos parceiros'
    ],
    'client' => [
        'register_with_success' => 'Cadastro realizado com sucesso! Você receberá um e-mail com os dados de acesso'
    ],
    'unauthorized' => [
        'code' => 'não autorizado',
        'title' => 'Não autorizado.'
    ],
    'resource_not_found' => [
        'code' => 'recurso não encontrado',
        'title' => 'Recurso não encontrado.'
    ],
    'unauthenticated' => [
        'code' => 'não autenticado',
        'title' => 'Não autenticado.'
    ],
    'route_not_found' => [
        'code' => 'rota não encontrada',
        'title' => 'Rota não encontrada.'
    ],
    'to_many_requests' => [
        'code' => 'solicitações excedidas',
        'title' => 'solicitações excedidas ao servidor. tente novamente em :time segundos'
    ],
    'invalid_password_reset_token' => 'Token inválido.',
];
