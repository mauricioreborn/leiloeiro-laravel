<?php
// phpcs:disable

return [
    'client_code_not_found' => 'codigo do cliente :client_code não encontrado',
    'product_code_not_found' => 'codigo do produto :product_code não encontrado',
    'order_product_not_found' => 'pedido produto com produto :product_code e  vencimento :expired_at não encontrado',
    'order_not_found' => 'codigo pedido :order não encontrado',
    'order_expired_at_not_found' => 'vencimento não encontrado',
];