<?php
// phpcs:disable

return [
    'client_code_not_found' => 'codigo do cliente :client_code não encontrado',
    'product_code_not_found' => 'codigo do produto :product_code não encontrado'
];