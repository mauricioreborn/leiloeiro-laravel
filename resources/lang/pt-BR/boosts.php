<?php

return [
    'status' => [
        'draft' => [
            'label' => 'Rascunho',
            'bg_class' => 'bg-gray-extra-dark',
            'text_class' => 'text-white',
        ],
        'in_progress' => [
            'label' => 'Ativo',
            'bg_class' => 'bg-success',
            'text_class' => 'text-white',
        ],
        'scheduled' => [
            'label' => 'Agendado',
            'bg_class' => 'bg-gray-extra-dark',
            'text_class' => 'text-white',
        ],
        'finished' => [
            'label' => 'Encerrado',
            'bg_class' => 'bg-danger',
            'text_class' => 'text-white',
        ],
        'canceled' => [
            'label' => 'Cancelado',
            'bg_class' => 'bg-danger',
            'text_class' => 'text-white',
        ],
    ],
    'validation' => [
        'name_required' => 'Por favor, preencha o nome do impulsionamento',
        'title_required' => 'Por favor, preencha o título do impulsionamento',
        'description_required' => 'Por favor, preencha o texto complementar do impulsionamento',
        'started_at_required' => 'Por favor, preencha a duração do impulsionamento',
        'finished_at_required' => 'Por favor, preencha a duração do impulsionamento',
        'client_lists_required' => 'Por favor, selecione uma lista de clientes',
        'client_lists_min' => 'Por favor, selecione uma lista de clientes',
        'channel_required' => 'Por favor, selecione ao menos um canal para o disparo',
        'product_code_required' => 'Por favor, selecione o produto a ser impulsionado',
        'origins_required' => 'Por favor, selecione ao menos um CD para o impulsionamento',
        'origins_min' => 'Por favor, selecione ao menos um CD para o impulsionamento',
        'origins_price_required' => 'Por favor, preencha o valor para o produto impulsionado',
    ],
];