<?php

return [
    'client_company_payment_invalid' => 'Meio de pagamento inválido, tente novamente',
    'company_payment_not_allowed' => 'company payment id :company_payment_id não encontrada',
    'credit_card' => 'Não conseguimos validar os dados do seu cartão de crédito.',
    'invoice_not_found' => 'Não foram encontradas faturas'
];
