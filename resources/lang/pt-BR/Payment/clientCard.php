<?php
//phpcs:ignoreFile

return [
    //phpcs:ignore
    'invoice_pending' => "Há faturas pendente para esse cartão, por favor aguarde a finalização do pedido para exclusão do mesmo.",
    'quota' => ":quota x :price <span>taxas e juros de :tax% a.m.</span>",
    'rejected' => "Não foi possível validar esse cartão. Verifique os dados e tente cadastrá-lo novamente ou cadastre um novo cartão.",
];
