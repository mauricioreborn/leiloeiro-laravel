<?php

// phpcs:ignoreFile
return [
    'created' => 'Criado',
    'integrated' => 'Integrado',
    'pending' => 'Pendente',
    'processing' => 'Processando',
    'paid' => 'Pago',
    'canceled' => 'Cancelado',
    'partially_paid' => 'Parcialmente pago',
    'refunded' => 'Reembolsado',
    'expired' => 'Expirado',
    'authorized' => 'Autorizado',
    'rejected' => 'Rejeitado',
    'draft' => 'Rascunho',
    'payment_in_progress' => 'Pagamento em progresso',
    'in_protest' => 'Em protesto',
    'in_analysis' => 'Em análise',
    'chargeback' => 'Estorno',
    'billed' => 'Faturado',
    'with_errors' => 'Com erros',
    'price_accepted' => 'Preço aceito',
    'evaluating_price' => 'Avaliando preço',
    'approved' => 'Aprovado',
    'bid' => [
        'pending' => [
            'label' =>  'Avaliando Preço',
            'mobile' => ':company_name: Aguarde a avaliação do seu lance.',
            'class' => 'secondary',
            'push' => '',
            'email' => ''
        ],
        'approved' => [
            'label' => 'Faturado',
        ],
    ],
    'order_bid' => [
        'pending' => [
            'label' =>  'Preço aceito',
            'mobile' => 'O valor do seu lance foi aceito. A indústria está analisando a disponibilidade de estoque.',
            'push' => [
                'title' => 'Lance #:bid: PREÇO ACEITO',
                'message' => ':product. Preço aceito R$:price (:discount% de desconto)',
            ],
            'class' => 'success',
            'color' => 'success',
            'email' => [
                'label' => 'Preço aceito',
                'color' => 'success',
                'description' => 'O valor do seu lance foi aceito. A indústria está analisando a disponibilidade de estoque.',
            ],
        ],
        'partially_billed' => [
            'label' => 'Faturado Parcial',
            'push' => [
                'title' => 'Lance #:bid: FATURADO PARCIAL',
                'message' => ':product. Previsão de entrega em :date'
            ],
            'mobile' => 'A indústria analisou a disponibilidade de estoque e seu lance gerou um pedido com volume parcial.',
            'email' => [
                'label' => 'Faturado',
                'color' => 'success',
                'description' => 'A indústria analisou a disponibilidade de estoque e seu lance gerou um pedido com volume parcial. Previsão de entrega em :date.',
            ],
            'class' => 'success',
        ],
        'billed_above' => [
            'label' => 'Faturado',
            'push' => [
                'title' => 'Lance #:bid: FATURADO',
                'message' => ':product. Houve aumento de volume. Previsão de entrega em :date'
            ],
            'mobile' => 'A indústria analisou a disponibilidade de estoque e seu lance gerou um pedido com volume alterado, pois esse produto tem peso variável. Previsão de entrega em :date.',
            'email' => [
                'label' => 'Faturado',
                'color' => 'success',
                'description' => 'A indústria analisou a disponibilidade de estoque e seu lance gerou um pedido com volume alterado, pois esse produto tem peso variável. Previsão de entrega em :date.',
            ],
            'class' => 'success',
        ],
        'approved' => [
            'label' => 'Faturado',
            'mobile' => 'A indústria analisou a disponibilidade de estoque e seu lance gerou um pedido.',
            'push' => [
                'title' => 'Lance #:bid: FATURADO',
                'message' => ':product. Estoque disponível, previsão de entrega em :date',
            ],
            'email' => [
                'color' => 'success',
                'label' => 'Faturado',
                'description' => 'A indústria analisou a disponibilidade de estoque e seu lance gerou um pedido. Previsão de entrega em :date.'
            ],
            'class' => 'success'
        ],
        'canceled' => [
            'email' => [
                'color' => 'danger',
                'label' => 'Cancelado',
                'description' => 'Devido a indisponbilidade de estoque, a indústria não conseguiu atender o seu pedido'
            ],
        ],
    ],
    'order' => [
        'approved' => [
            'label' => 'Faturado',
            'push' => [
                'title' => 'Pedido #:order_id: FATURADO',
                'message' => 'Todos os itens foram faturados',
            ],
            'mobile' => 'Todos os itens do pedido foram faturados. Em breve eles serão entregues.',
            'email' => 'Todos os itens do pedido foram faturados. Em breve eles serão entregues.',
            'block' => 'FATURADO',
            'class' => 'success',
        ],
        'partially_billed' => [
            'label' => 'Faturado Parcial',
            'push' => [
                'title' => 'Pedido #:order_id: FATURADO PARCIAL',
                'message' => 'Alguns itens foram alterados'
            ],
            'mobile' => 'Um ou mais produtos sofreu alteração por volume ou disponibilidade de estoque.',
            'email' => 'Um ou mais produtos sofreu alteração por volume ou disponibilidade de estoque.',
            'block' => 'FATURADO',
            'class' => 'success',
        ],
        'pending' => [
            'label' => 'Itens em análise',
            'mobile' => 'A indústria está analisando a disponibilidade de estoque.',
            'push' => [
                'title' => 'Pedido #:order_id: ATUALIZADO',
                'message' => ':product foi :status'
            ],
            'block' => 'EM ANÁLISE',
            'class' => 'secondary',
        ],
        'canceled' => [
            'label' => 'Cancelado',
            'push' => 'Pedido :order_id: CANCELADO. Falta de estoque',
            'mobile' => 'Todos os itens do pedido foram cancelados devido a falta de estoque.',
            'email' => 'Todos os itens do pedido foram cancelados devido a falta de estoque.',
            'block' => 'CANCELADO',
            'class' => 'danger',
        ],
    ],
];
