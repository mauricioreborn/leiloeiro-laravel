<?php

return [
    'daily' => '[1]dia|[0,*]dias',
    'weekly' => '[1]semana|[0,*]semanas',
    'monthly' => '[1]mês|[0,*]meses',
];
