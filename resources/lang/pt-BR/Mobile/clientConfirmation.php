<?php

// phpcs:ignoreFile
return [
    'errors' => [
        'invalid_type' => 'Tipo de confirmação inválido.',
    ],
    'sms' => [
        'title' => 'Sucesso!',
        'message' => 'Código de confirmação enviado',
        'confirmation' => 'Código de verificação de sms confirmado.',
        'notification' => 'O seu código de confirmação para o aplicativo da Souk é: :code',
        'errors' => [
            'invalid_number' => 'O número informado não é um número válido.',
            'invalid_code' => 'Oops, código errado! Verifique os números digitados com o código enviado para você via SMS',
            'already_has_validated' => 'Oops, você já validou seu telefone, por favor, valide seu e-mail no passo seguinte'
        ],
    ],
    'mail' => [
        'title' => 'Sucesso!',
        'message' => 'E-mail de confirmação enviado',
        'confirmation' => 'E-mail confirmado com sucesso!',
        'errors' => [
            'invalid_code' => 'Oops, código errado! Esse código expirou, por favor, reinvie novamente um e-mail de confirmação',
            'already_has_validated' => 'Oops, você já validou seu e-mail, não é necessário valida-lo novamente',
            'client_not_found' => 'Oops, este código de validação não está relacionado a nenhum cliente. Por favor, tente novamente.',
        ],
    ]
];
