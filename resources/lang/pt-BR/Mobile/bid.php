<?php

// phpcs:ignoreFile

return [
    'bids_and_buys_blocked' => 'Lances e pedidos bloqueados, para mais informações entre em contato com suporte@souk.com.br ou via whatsapp (acesso direto na tela inicial do app).',
    'bids_min_value' => 'Valor do lance não atingiu o valor mínimo de ',
    'bids.confirm.fail' => 'Não foi possível confirmar o lance.',
    'bids.cancelation_reason' => 'Motivo do cancelamento.',
    'bids.remove.success' => 'Lance removido com sucesso',
    'bids.disapprove.success' => 'Lance removido com sucesso',
    'bids.approve.success' => 'Os lances foram aprovados com sucesso.',
    'bids.not_found' => 'Lance não encontrado.',
    'bids.confirm.title' => 'Lance enviado!',
    'bids.confirm.description' => 'Aguarde a avaliação do preço.',
    'bids.update.title' => 'Lance atualizado!',
    'bids.update.description' => 'Aguarde a avaliação do preço.',
    'bids.labels.economy' => ":economy_percentage% desconto <span>(tabela de preço regular)</span>",
    'bids.labels.delivery_date' => "A data de entrega é calculada quando o valor do lance for aceito e após análise de estoque e logística",
    'bids.labels.delivery_canceled' => "Cancelada",
    'bids.update.fail' => 'Não foi possível atualizar o lance.',
];
