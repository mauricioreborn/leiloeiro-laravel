<?php
//phpcs:ignoreFile

return [
    'creditCardDisable' => 'Não é possível efetuar o pagamento via cartão de crédito. O valor supera seu limite de R$ :limit.',
    'companyCreditDisable' => 'Não é possível efetuar o pagamento via boleto. O valor supera seu limite de crédito em R$ :limit.',
    'companyCredit' => 'Boleto Bancário - :company',
    'cart' => [
        'company_credit' => 'Boleto da indústria (chega junto do seu pedido)',
        'credit_card' => 'Até 10x no cartão de crédito',
    ]
];
