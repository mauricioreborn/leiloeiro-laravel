<?php

return [
    'no_record' => "Nenhum produto.",
    'not_available_for_bids' => 'Produto não disponível para lances.',
    'not_available' => 'Produto não disponível.',
    'not_accept_purchase' => 'SEM LOGÍSTICA, SUA PRÓXIMA DATA DE COMPRAS É: :date',
    'custom_view_not_found' => 'custom_view não disponivel.',
    'best_price_from_track' => 'MELHORES DO DIA',
    'best_seller_typology' => 'PARA SEU NEGOCIO',
    'suggested_products' => "SUGERIDOS",
    'buy_again' => 'COMPRE NOVAMENTE',
    'without_tribute' => 'Valor sem ST'
];
