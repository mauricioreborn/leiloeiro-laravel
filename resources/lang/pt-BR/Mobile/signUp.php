<?php
//phpcs:ignoreFile
return [
    'validation' => [
        'zipcode' => 'Oops, por favor digite um CEP válido.',
        'cnpj' => 'Oops, por favor digite um CNPJ válido.',
        'email' => 'Oops, por favor digite um e-mail válido.',
        'email_unique' => 'Oops, este e-mail já está sendo usando em outro CNPJ.',
        'email_confirmed' => 'Oops, os e-mails não são iguais. Por favor verifique',
        'validation_code' => 'Oops, código errado! Verifique os números digitados com o código enviado para você via SMS',
        'phone' => 'Oops! Por favor insira um número de celular válido.',
    ],
    'first_step' => [
        'client_exists' => 'O CNPJ já está cadastrado na Souk. Utilize a senha enviada no momento do cadastro para acessar sua conta',
        'independent_client_exists' => 'O CNPJ está sendo analisado por nossos parceiros, este processo pode demorar alguns dias. Você receberá a senha no e-mail cadastrado. Se tiver dúvidas, fale conosco suporte@souk.com.br',
        'invalid_instance' => 'Não foi possível prosseguir com o cadastro. Fale conosco <strong>suporte@souk.com.br</strong> para mais informações',
        'phone_already_confirmed' => 'Seu telefone já foi confirmado.',
        'phone_confirmation_sent' => 'Código de confirmação enviado.',
    ],
    'phone_validation' => [
        'invalid_code' => 'Oops, código errado! Verifique os números digitados com o código enviado para você via SMS',
    ],
    'contact_information' => [
        'client_updated' => '<p>Enviamos um e-mail para <strong>:email</strong> com a senha.</p><p>Agora voce já pode aproveitar as oportunidades imperdíveis da Souk.</p>',
        'independent_client_updated' => '<p>Os dados da empresa <strong>:social_reason</strong> foram enviados para análise de nossos parceiros.</p><p>Este processo pode demorar alguns dias, você receberá a senha no e-mail <strong>:email</strong></p>',
    ]
];
