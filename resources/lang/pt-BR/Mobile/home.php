<?php

return [
    'logistics_not_found' => 'Sem informações de logística.',
    'logistics_info' => 'Os níveis de atendimento das indústrias para seus clientes são garantidos 
a partir da integração entre a <strong>visão logística</strong> – que representam os dias em que 
os pedidos de vendas podem ser atendidos, havendo a separação e alocação do estoque e a 
<strong>grade de embarque</strong> – que representam os dias em que os produtos podem ser 
embarcados para posteriormente serem entregues'



];
