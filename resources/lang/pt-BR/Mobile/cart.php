<?php

return [
    'product_cart_not_found' => 'produto não encontrado',
    'cart_not_found' => 'carrinho não encontrado',
    'cart_of_other_client' => 'carrinho não pertence ao usuario logado',
    'not_change_cart_product' => 'Não é possível alterar o produto.',
    'changed_product_cart' => 'product has been updated',
    'volume_unavailable' =>
        "Quantidade de caixas não disponível. Esse produto já está no seu carrinho com a quantidade máxima.|[1]
         Quantidade de caixas não disponível. Esse produto já está no seu carrinho com a quantidade máxima|[2,*]
         Quantidade de caixas não disponível. Total de :value caixas em estoque.",
    'not_available_volume' => 'Volume não disponível.',
    'min_order' => 'Faltam :value (pedido mínimo)'
];
