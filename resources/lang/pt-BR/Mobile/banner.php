<?php

// phpcs:ignoreFile
return [
    'creditCard' => 'Parcele suas compras em até <b>10x</b> com o seu cartão de crédito.',
    'updateEmailFone' => [
        'title' => 'VERIFICAÇÃO DE DADOS',
        'steps' => [
            1 => [
                'body' => 'Para sua segurança, precisamos que você verifique seu e-mail e telefone celular',
            ],
            2 => [
                'body' => 'Aguardando confirmação do nº de celular:<b> :phone_number </b>'
            ],
            3 => [
                'body' => 'Seu celular foi validado, precisamos agora que você verifique seu email'
            ],
            4 => [
                'body' => 'Aguardando confirmação do e-mail:<b> :email </b>'
            ],
        ],
    ],
    'updateVersion' => [
        'body' => 'Não perca nenhuma funcionalidade. Acesse a loja de aplicativos e atualize seu app. Tem novidade vindo por ai'
    ],
    'tirolez' => [
        'type' => [
            1 => [
                'title' => 'Nova loja disponível: Tirolez',
                'body' => 'Você já é cliente Tirolez e pode começar a comprar agora mesmo. Vem, não perca os preços de lançamento!'
            ],
            2 => [
                'title' => 'Nova loja disponível: Tirolez',
                'body' => 'Agora você consegue negociar diretamente com eles. Consiga bons preços através do leilão. Vem!'
            ]
        ]
    ]
];
