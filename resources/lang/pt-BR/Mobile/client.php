<?php

return [
    'register_updated' => 'confirmação de dados realizada com sucesso',
    'independent_client_cnpj_exist' => 'Dados já enviados anteriormente. O CNPJ enviado está sendo analisado por nossos parceiros, aguarde nosso contato. Para mais informações envie e-mail para suporte@souk.com.br',
    'independent_client_email_exist' => 'Dados já enviados anteriormente. O Email enviado está sendo analisado por nossos parceiros, aguarde nosso contato. Para mais informações envie e-mail para suporte@souk.com.br'

];
