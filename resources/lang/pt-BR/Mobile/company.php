<?php
return [
    // phpcs:ignore
    'payment' => '<p>A Souk é uma plataforma de negócios de oportunidade. Todas as compras por leilão ou arremate dependem da disponibilidade de produto em estoque.</p><p>Quando você realiza um pagamento com seu cartão de crédito, <strong>um valor é reservado do seu limite disponível</strong>. Esse valor não é debitado definitivamente, mas fica indisponível para uso no seu cartão.</p><h2>Por que é feito uma reserva, ao invés do pagamento?</h2><p>A indústria precisa verificar a disponibilidade do estoque dos produtos que você comprou ou teve lance aprovado. Quando isso ocorrer, seu pedido é faturado e está prestes a ser enviado para a logística.</p><p>Se o pedido não é faturado, <strong>o valor reservado retorna imediatamente</strong> para seu limite do cartão.</p><p>Portanto, <strong>a cobrança definitiva só é realizada quando seu pedido é liberado para entrega</strong> na logística, após ser faturado.</p><p>Esse procedimento evita estornos desnecessários e agiliza o processo de pagamento.</p><p>Qualquer dúvida, entre em contato no<br><span class="email">suporte@souk.com.br</span></p>',
    // phpcs:ignore
    'payment_methods' => '<p>A Souk disponibiliza duas formas de pagamento:</p><h2 class="icon-billet">BOLETO PÓS-PAGO DA INDÚSTRIA</h2><p>O boleto chega com o seu pedido e utiliza o seu limite de crédito e prazo de pagamento estabelecido com a indústria.</p><p>O prazo da <strong>:company</strong> é <strong>:days dias</strong>.</p><h2 class="icon-cc">Cartão de Crédito</h2><p><strong>Aceitamos Visa, Mastercard, American Express, Diners e Elo.</strong></p><p><strong>Máximo R$ 5.000 por transação.</strong></p><p>É possível parcelar seu lance em até <strong>12 vezes</strong>.</p><p>Taxas e juros são aplicados desde a <strong>primeira parcela</strong>.</p><p>Todas as parcelas incluem a <strong>taxa de administração</strong>.</p><p>Será feita uma <strong>reserva do valor</strong> do lance no seu cartão de crédito. Esse valor ficará indisponível para uso no seu cartão.</p><p>Se o lance não for aceito, o valor da <strong>reserva</strong> será <strong>automaticamente cancelado</strong> e nenhum débito será efetuado.</p><h2>Por que é feita uma reserva e não pagamento?</h2><p>A indústria precisa verificar a disponibilidade do estoque dos produtos que você comprou ou teve lance aprovado. Quando isso ocorrer, seu pedido é faturado e está prestes a ser enviado para a logística.</p><p>Se o pedido não é faturado, <strong>o valor reservado retorna imediatamente</strong> para seu limite do cartão.</p><p>Portanto, <strong>a cobrança definitiva só é realizada quando seu pedido é liberado para entrega</strong> pela logística, após ser faturado.</p><p>Esse procedimento evita estornos desnecessários e agiliza o processo de pagamento.</p>',
    // phpcs:ignore
    'conditions_rates' => '<h2>Aceitamos os seguintes cartões:</h2><table><tr><td class="col-5"><p class="brand-visa"><strong>Visa</strong></p><p class="brand-mastercard"><strong>Mastercard</strong></p><p class="brand-diners"><strong>Diners</strong></p></td><td class="col-7"><p class="brand-amex"><strong>American Express</strong></p><p class="brand-elo"><strong>Elo</strong></p></td></tr></table><h2>Juros</h2><p>- 2,51% à vista<br>- de 2-6 vezes: 3,21%<br>- de 7-12 vezes: 3,55%<br>- American Express: 4,60% (todas as parcelas)</p><p>Taxas e juros são aplicados desde a primeira parcela.</p><p>Todas as parcelas incluem a taxa de administração.</p><h2>Máximo de R$5.000 por transação.</h2>',
    // phpcs:ignore
    'cart' => '<p>A Souk tem 2 formas de comprar produtos. Através do leilão ou do carrinho de compras.</p><p>Com o <strong>Carrinho de Compras</strong> é possível comprar vários produtos de uma vez ao preço de tabela. Ao finalizar seu carrinho um pedido é emitido imediatamente para a indústria, mas lembre-se que alguns itens do pedido <strong>podem ser cancelados</strong> se não houver disponibilidade de estoque.</p><p>Os itens de um pedido são <strong>faturados individualmente</strong>, você irá receber <strong>mensagens no aplicativo</strong> conforme o status de cada item for atualizado e um e-mail será enviado ao final da análise de todos os itens.</p><p>Você pode pagar seu pedido através: :payment</p> <p>Condições <strong>:company_name</strong></p><p>- Pedido mínimo: R$ :min_order<br>- Prazo para pagamento do boleto: :billing_day dias</p>',
    // phpcs:ignore
    'cart_with_devolution' => '<p>A Souk tem 2 formas de comprar produtos. Através do leilão ou do carrinho de compras.</p><p>Com o <strong>Carrinho de Compras</strong> é possível comprar vários produtos de uma vez ao preço de tabela. Ao finalizar seu carrinho um pedido é emitido imediatamente para a indústria, mas lembre-se que alguns itens do pedido <strong>podem ser cancelados</strong> se não houver disponibilidade de estoque.</p><p>Os itens de um pedido são <strong>faturados individualmente</strong>, você irá receber <strong>mensagens no aplicativo</strong> conforme o status de cada item for atualizado e um e-mail será enviado ao final da análise de todos os itens.</p><p>Você pode pagar seu pedido através: :payment</p> <p>Condições <strong>:company_name</strong></p><p>- Pedido mínimo: R$ :min_order<br>- Prazo para pagamento do boleto: :billing_day dias<br>- Permite devolução dos itens</p>',
    'errors' => [
        'company_not_allowed' => 'ação não disponivel para esta empresa.'
    ],
    // phpcs:ignore
    'payment_cards_info' => '<h2>Aceitamos os seguintes cartões:</h2><table><tr><td class="col-5"><p class="brand-visa"><strong>Visa</strong></p><p class="brand-mastercard"><strong>Mastercard</strong></p><p class="brand-diners"><strong>Diners</strong></p></td><td class="col-7"><p class="brand-amex"><strong>American Express</strong></p><p class="brand-elo"><strong>Elo</strong></p></td></tr></table><h2>Como funciona o pagamento com cartão?</h2><p><strong>Máximo R$ 5.000 por transação.</strong></p><p>É possível parcelar seu lance em até <strong>12 vezes</strong>.</p><p>Taxas e juros são aplicados desde a <strong>primeira parcela</strong>.</p><p>Todas as parcelas incluem a <strong>taxa de administração</strong>.</p><h2 class="lowercase">Juros</h2><p>- 2,51% à vista<br>- de 2-6 vezes: 3,21%<br>- de 7-12 vezes: 3,55%<br>- American Express: 4,60% (todas as parcelas)</p><p>Quando você realiza um pagamento com seu cartão de crédito, um valor é <strong>reservado do seu limite</strong> disponível. Esse valor não é <strong>debitado definitivamente</strong>, mas fica indisponível para uso no seu cartão.</p><h2>Por que é feita uma reserva e não pagamento?</h2><p>A indústria precisa verificar a disponibilidade do estoque dos produtos que você comprou ou teve lance aprovado. Quando isso ocorrer, seu pedido é faturado e está prestes a ser enviado para a logística.</p><p>Se o pedido não é faturado, <strong>o valor reservado retorna imediatamente</strong> para seu limite do cartão.</p><p>Portanto, <strong>a cobrança definitiva só é realizada quando seu pedido é liberado para entrega</strong> pela logística, após ser faturado.</p><p>Esse procedimento evita estornos desnecessários e agiliza o processo de pagamento.</p>',
    'banner' => [
        'register_updated' => [
            'body' => 'Confirme seus dados para aproveitar as ofertas da Nestlé Iogurtes'
        ],
        'waiting_approval' => [
            'body' => 'Seus dados estão em análise pela Nestlé. Aguarde, você será avisado em breve.'
        ]
    ],
    1 => [
        'image_color' => 'assets/companies/seara_color.png',
        'image_background' => 'assets/companies/seara_bg.svg',
        'image_products' => 'assets/companies/seara_products.png',
        'discount' => 'Até 65% de desconto',
        'newest' => 'false',
    ],
    3 => [
        'image_color' => 'assets/companies/massaleve_color.png',
        'image_background' => 'assets/companies/massaleve_bg.svg',
        'image_products' => 'assets/companies/massaleve_products.png',
        'discount' => 'Até 50% de desconto',
        'newest' => 'false',
    ],
    4 => [
        'image_color' => 'assets/companies/nestle_color.png',
        'image_background' => 'assets/companies/nestle_bg.svg',
        'image_products' => 'assets/companies/nestle_products.png',
        'discount' => 'Até 60% de desconto',
        'newest' => 'false',
    ],
    5 => [
        'image_color' => 'https://souk-company.s3.amazonaws.com/icons/tirolez/logo-tirolez.png',
        'image_background' => 'https://souk-company.s3.amazonaws.com/icons/tirolez/wave-tirolez.svg',
        'image_products' => 'https://souk-company.s3.amazonaws.com/icons/tirolez/tirolez_produtos.png',
        'discount' => 'Até 70% de desconto',
        'newest' => 'true',
    ]
];
