<?php

return [
    'bid' => [
        'rejected' => [
            'title' => 'Lance #:bid: REJEITADO',
            'message' => ':product. O lance de R$:price foi rejeitado',
        ],
        'expired' => [
            'title' => 'Lance #:bid foi atualizado',
            'message' => ':product: Lance expirado',
        ],
        'cancelled' => [
            'title' => 'Lance #:bid foi atualizado',
            'message' => ':product: Cancelado',
        ],
        'approved' => [
            'title' => 'Lance #:bid PREÇO ACEITO',
            'message' => ':product. Preço aceito R$:price (:discount% de desconto)',
        ],
        'finished' => [
            'title' => 'Lance #:bid foi atualizado',
            'message' => ':product: Faturado',
        ],
    ],
    'campaign' => [
        'repurchase' => [
            'title' => ':product_name',
            'message' => ':price - MENOR PREÇO ESSA SEMANA'
        ],
        'first_purchase' => [
            'title' => 'A :company_name TEM A MELHOR OFERTA PARA VOCÊ!',
            'message' => ':product_name a partir de :price'
        ],
        'highlight_fefo' => [
            'title' => ':product_name - :price',
            'message' => 'CONFIRA AS OFERTAS ANTES QUE ACABEM OS ESTOQUES'
        ],
        'errors' => [
            'fefo_not_found' => 'nenhum fefo  encontrado'
        ]
    ],
    'order' => [
        'all_approved' => [
            'title' => 'Pedido #:order foi atualizado',
            'message' => 'Todos os itens foram faturados',
        ],
        'partial_approved' => [
            'title' => 'Pedido #:order foi atualizado',
            'message' => 'Itens foram faturados parcialmente',
        ],
        'all_canceled' => [
            'title' => 'Pedido #:order foi atualizado',
            'message' => 'Todos os Itens foram cancelados',
        ]
    ],
    'order_product' => [
        'title' => 'Pedido #:order foi atualizado',
        'message' => ':product: :status',
    ]
];
