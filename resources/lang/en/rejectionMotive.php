<?php

// phpcs:ignore
return [
    'invalid_fefo' => [
        'mobile' => 'Bid :bid: CANCELED. :product. Out of stock',
        'email' => 'Due to unavailability of stock, the industry could not fulfill your request.',
        'card_text' => 'Due to unavailability of stock, the industry could not fulfill your request.',
    ],
    'customer_without_balance' => [
        'mobile' => 'Bid :bid: CANCELED. :product. Bid exceeds credit limit',
        'email' => 'Your bid exceeds your credit limit R$:account_balance. This limit is updated daily.',
        'card_text' => 'Your bid exceeds your credit limit R$:account_balance. This limit is updated daily.',
    ],
    'min_order' => [
        'mobile' => 'Bid :bid: CANCELED. :product. Bid partial amount did not meet minimum order',
        'email' => 'The industry had no stock to meet the full volume (Kg) so the minimum order value was not met.',
        'card_text' => 'The industry had no stock to meet the full volume (Kg) so the minimum order value was not met.',
    ],
    'expired' => [
        'mobile' => 'Lance :bid EXPIRED. :product_name. expired at :date',
        'email' => 'Your bid expired on day :date.',
        'card_text' => 'Your bid expired on :date.'
    ]
];
