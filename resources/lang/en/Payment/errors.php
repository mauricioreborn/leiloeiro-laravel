<?php

return [
    'client_company_payment_invalid' => 'Invalid payment method, try again.',
    'company_payment_not_allowed' => 'company payment id :company_payment_id not allowed',
    'credit_card' => 'Invalid card, try again.',
     'invoice_not_found' => 'there is no invoice'
];
