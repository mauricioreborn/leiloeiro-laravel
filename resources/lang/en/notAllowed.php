<?php

return [
    'update' => 'This entry cannot be updated',
    'delete' => 'This entry cannot be deleted'
];
