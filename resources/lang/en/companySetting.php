<?php

return [
    'daily' => '[1]day|[0,*]days',
    'weekly' => '[1]week|[0,*]weeks',
    'monthly' => '[1]month|[0,*]months',
];
