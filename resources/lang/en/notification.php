<?php

return [
    'bid' => [
        'rejected' => [
            'title' => 'Bid #:bid has been updated',
            'message' => ':product - Price reproved :price/Kg',
        ],
        'cancelled' => [
            'title' => 'Bid #:bid has been updated',
            'message' => ':product: Canceled',
        ],
        'finished' => [
            'title' => 'Bid #:bid has been updated',
            'message' => ':product: has been billed',
        ],
        'expired' => [
            'title' => 'Bid #:bid was updated',
            'message' => ':product: Bid expired',
        ],
    ],
    'campaign' => [
        'repurchase' => [
            'title' => ':product_name',
            'message' => ':price - CHEPPEAR PRICE THIS WEEK'
        ],
        'first_purchase' => [
            'title' => 'A :company_name HAS THE BEST OFFER FOR YOU!',
            'message' => ':product_name starting on :price'
        ],
        'highlight_fefo' => [
            'title' => ':product_name - :price',
            'message' => 'CHECK IT OUT THE OFFERS BEFORE STOCK FINISHING'
        ],
        'errors' => [
            'fefo_not_found' => 'Product not found'
        ]
    ],
    'order' => [
        'all_approved' => [
            'title' => 'Order #:order has been updated',
            'message' => 'All itens has been billed',
        ],
        'partial_approved' => [
            'title' => 'Order #:order has been updated',
            'message' => 'Itens has been partialy billed',
        ],
        'finished' => [
            'title' => 'Order #:order has been updated',
            'message' => 'Status: Billed',
        ],
        'cancelled' => [
            'title' => 'Order #:order has been updated',
            'message' => 'Status: Canceled',
        ]
    ],
    'order_product' => [
        'title' => 'Order #:order has been updated',
        'message' => ':product: :status',
    ],
];
