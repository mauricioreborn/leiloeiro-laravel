<?php

return [
    'boxes' => 'box|boxes',
    'reason_cancellation' => [
        'account_ballance' =>
            'This order value overcome your credit limit (R$ :price). This limit is updated daily ',
        'min_order' => 'The value for this order overcome your min order.'
    ],
    'status' => [
        'canceled' => 'CANCELED'
    ]
];
