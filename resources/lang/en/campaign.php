<?php

return [
    'type.required' => 'The Type field is required.',
    'type.exists' => 'The selected type is invalid.',
    'scheduled_at.required' => 'The date field is required.',
    'scheduled_at.date' => 'The field is not a valid date.',
    'scheduled_at.date_format' => 'The field is not a valid date.',
    'scheduled_at.after' => 'The field must be a date after today',
    'fefo_id.required_if' => "For 'highlight' campaigns, you must select an FEFO.",
];
