<?php

return [
    'status' => [
        'in_analysis' => 'EM ANALISE',
        'processing' => 'EM ATENDIMENTO',
        'billed' => 'FATURADO',
        'canceled' => 'CANCELADO'
    ],
];
