<?php

// phpcs:ignore
return [
    'errors' => [
        'invalid_type' => 'Invalid confirmation type.',
    ],
    'sms' => [
        'title' => 'Confirmation sent.',
        'message' => 'Soon you will receive a text with your confirmation code.',
        'notification' => 'Your souk confirmation code is: :code',
        'confirmation' => 'Phone checked with success!',
        'errors' => [
            'invalid_number' => 'The number provided is invalid.',
            'invalid_code' => 'Oops, invalid code! Check the typed numbers with code that has been sent for you via SMS',
            'already_has_validated' => 'Oops, it looks like you already validated your phone number, please, validate your e-mail on next step'
        ],
    ],
    'mail' => [
        'title' => 'Confirmation sent.!',
        'message' => 'Soon you will receive an e-mail with your confirmation code.',
        'confirmation' => 'Mail checked with success!',
        'errors' => [
            'invalid_code' => 'Oops, invalid code! Check the code with code that has been sent for you via e-mail',
            'already_has_validated' => 'Oops, it looks like you already validated your e-mail, no action needed',
            'client_not_found' => 'Oops, this confirmation code is not related to any client in our database. Please, try again.',
        ],
    ]
];
