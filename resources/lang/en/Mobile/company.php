<?php

return [
    // phpcs:ignore
    'payment' => '<p>A Souk é uma plataforma de negócios de oportunidade. Todas as compras por leilão ou arremate dependem da disponibilidade de produto em estoque.</p><p>Quando você realiza um pagamento com seu cartão de crédito, <strong>um valor é reservado do seu limite disponível</strong>. Esse valor não é debitado definitivamente, mas fica indisponível para uso no seu cartão.</p><h2>Por que é feito uma reserva, ao invés do pagamento?</h2><p>A indústria precisa verificar a disponibilidade do estoque dos produtos que você comprou ou teve lance aprovado. Quando isso ocorrer, seu pedido é faturado e está prestes a ser enviado para a logística.</p><p>Se o pedido não é faturado, <strong>o valor reservado retorna imediatamente</strong> para seu limite do cartão.</p><p>Portanto, <strong>a cobrança definitiva só é realizada quando seu pedido é liberado para entrega</strong> na logística, após ser faturado.</p><p>Esse procedimento evita estornos desnecessários e agiliza o processo de pagamento.</p><p>Qualquer dúvida, entre em contato no<br><span class="email">suporte@souk.com.br</span></p>'
];
