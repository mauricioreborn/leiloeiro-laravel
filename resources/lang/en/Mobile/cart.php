<?php

return [
    'product_cart_not_found' => 'product not found',
    'cart_not_found' => 'cart not found',
    'cart_of_other_client' => 'cart does not belong to logged in user',
    'not_change_cart_product' => 'Unable to change product.',
    'not_available_volume' => 'Volume not available.',
    'changed_product_cart' => 'product has been updated',
    'volume_unavailable' =>
        "Number of boxes not available. This product is already in your cart with the maximum amount.|[1]
         Amount of boxes not available. Total volume of :value box|[2,*]
         Amount of boxes not available. Total volume of :value box.",
    'min_order' => 'Missing: value (minimum order)'
];
