<?php

return [
    'pending' => 'PENDENTE',
    'approved' => 'LIQUIDADO',
    'partially_approved' => 'LIQUIDADO PARCIAL',
    'canceled' => 'CANCELADO',
    'partially_canceled' => 'ITENS CANCELADOS',
    'finish' => [
        'cart_not_found' => 'carrinho não encontrado',
        'create_order_with_error' => 'Não foi possível finalizar o pedido, por favor, entre em contato com o suporte',
        'error_remove_product' => 'erro ao remover produtos do carrinho',
        'title' => 'Pedido enviado para análise!',
        'description' => 'Aguarde a confirmação de disponibilidade de estoque dos itens do seu pedido.',
        'product_unavailable' => 'Alguns produtos não estão mais disponíveis',
        'back_cart' => 'Volte ao carrinho para editá-lo.',
    ],
    'boxes' => '{0}Nenhuma caixa|[1]1 caixa|[2,*]:box caixas',
    'weeks' => '[1]1 week|[2,*]:weeks weeks',
    'validate' => '{0}Hoje|[1]1 dia|[2,*]:days dias',
    'expired' => 'Expirado',
];
