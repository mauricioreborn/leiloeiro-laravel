<?php

return [
    'creditCard' => 'You can pay your purchase in <b>10</b> installments using your credit card',
    'updateEmailFone' => [
        'title' => 'DATA VERIFICATION',
        'steps' => [
            1 => [
                'body' => 'For your safety, we need you to verify your e-mail and mobile number',
            ],
            2 => [
                'body' => 'Awaiting confirmation of mobile number: :phone_number'
            ],
            3 => [
                'body' => 'Your phone has been validated, we now need you to verify your email'
            ],
            4 => [
                'body' => 'Awaiting email confirmation: :email'
            ],
        ],
    ],
];
