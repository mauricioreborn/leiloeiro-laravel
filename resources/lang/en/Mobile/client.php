<?php

return [
    'register_updated' => 'register updated has been saved',
    'independent_client_cnpj_exist' => 'Data already sent previously. The submitted CNPJ is being reviewed by our partners, please wait for our contact. For more information email suporte@souk.com.br'
];
