<?php

return [
    'created' => 'The user was created.',
    'updated' => 'The user was updated.',
];
