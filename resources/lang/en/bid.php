<?php

return [
    'amount_changed' => 'bid amount has been changed',
    'Partial_not_allowed' => 'this bid does not accept partial product',
    'box_greater' => 'box amount greater than user defined',
    'total_less' => 'box amount less than necessary',
    'not_found' => 'bid not found',
    'not_partial_accept' => 'partial accept not allowed',
    'fefo_not_found' => 'product not available',
    'bid_not_first' => 'this bid is not the first'
];
