<?php

return [
    'duplicated_schedule' =>
        'You already has scheduled an notification for origin: :origin and period: :period for this date',
];
