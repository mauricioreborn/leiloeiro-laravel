<?php

return [
    'created' => 'Created',
    'integrated' => 'Integrated',
    'pending' => 'Pending',
    'processing' => 'Processing',
    'paid' => 'Paid',
    'canceled' => 'Canceled',
    'partially_paid' => 'Partially paid',
    'refunded' => 'Refunded',
    'expired' => 'Expired',
    'authorized' => 'Authorized',
    'rejected' => 'Rejected',
    'draft' => 'Draft',
    'payment_in_progress' => 'Payment in progress',
    'in_protest' => 'In protest',
    'in_analysis' => 'In analysis',
    'chargeback' => 'Chargeback',
    'billed' => 'Billed',
    'with_errors' => 'With errors',
    'price_accepted' => 'Price accepted',
    'evaluating_price' => 'Evaluating price',
    'approved' => 'Approved',

    'bid' => [
        'approved' => [
            'web' => [
                'status_description' => 'Approved',
            ],
        ],
        'canceled' => [
            'web' => [
                'status_description' => 'Canceled',
            ],
        ],
        'pending' => [
            'web' => [
                'status_description' => 'Pending',
            ],
        ],
    ],
];
