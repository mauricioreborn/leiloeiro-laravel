<?php

return [
    'title' => 'Stock',
    'description' => 'Update stock',
    'expired' => ':product | origin: :origin_code expired.',
    'not_active' => ':product | origin: :origin_code blocked.',
    'without_product_price' => ':product | origin: :origin_code has no price set in UF.',
    'without_min_order' => ':product | origin: :origin_code | volume :volume does not reach the minimum order.',
];
