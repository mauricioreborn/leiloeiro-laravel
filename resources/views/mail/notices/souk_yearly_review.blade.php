<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Souk Email</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">


</head>

<body style="background-color: #eeeeee; color: #333333; margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif;">
<table cellspacing="0" cellpadding="0" class="layout" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; width: 100%;" width="100%">
    <tr class="layout-header" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
        <th style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #7e28de linear-gradient(-135deg, #8b2fcb 0%, #7e28de 70%, #7321df 100%); background-color:#7e28de; text-align: center; width: 100%;" align="center">
            <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; text-align: center; max-width: 500px; width: 500px;">
                <tr><td style="padding: 30px 0 20px;"><img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/souk.png" width="143" height="36" alt="" style="max-width: 100%;"></td></tr>
                <tr><td style="height: 32px; border: 1px solid rgb(28,167,182); background-color: rgb(28,167,182); border-radius: 3px 3px 0 0;"></td></tr>
            </table>
        </th>
    </tr>
    <tr class="layout-body" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
        <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center">
            <table cellspacing="0" cellpadding="0" class="email-content" style="padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; max-width: 500px; width: 500px; margin: 0 auto;" width="500">
                <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; overflow: hidden;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/header.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>
                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="client-name" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 26px; font-weight: bold; padding: 16px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">Olá {{ $name }},</td>
                </tr>

                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="souk-volume-title" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 22px; padding: 0 42px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">Você sabia que este ano a <strong style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">SOUK</strong> transacionou:</td>
                </tr>
                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="label-box" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; padding: 8px 0 32px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">
                        <table style="padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; border-radius: 6px; border: 1px solid rgb(96, 26, 184); height: 90px; margin: 0 auto; min-width: 240px; overflow: hidden;" height="90">
                            <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                <td class="label-box-wrapper" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; text-align: center; background-color: #7e28de; color: white; background: linear-gradient(-135deg, rgb(139, 47, 203) 0%, rgb(126, 40, 222) 70%, rgb(115, 33, 223) 100%); border-radius: 6px; overflow: hidden; padding: 8px 16px;" align="center" bgcolor="#601ab8">
                                    <table cellspacing="0" cellpadding="0" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0;">
                                        <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                            <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background-color: #601ab8; color: white; background: transparent; text-align: left;" bgcolor="#601ab8" align="left">
                                                <img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/icon-volume-big.png" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                                            </td>
                                            <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background-color: #601ab8; color: white; background: transparent; text-align: left; padding-left: 16px;" bgcolor="#601ab8" align="left">
                                                <h1 style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 32px; font-weight: bold;">{{ $volume }}</h1>
                                                <span style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal;">Toneladas</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="souk-value-title" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 22px; padding: 0 42px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">E nossos clientes através do nosso leilão negociaram direto com a indústria e economizaram:</td>
                </tr>
                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="label-box" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; padding: 8px 0 32px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">
                        <table cellspacing="0" cellpadding="0" style="padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; border-radius: 6px; border: 1px solid rgb(96, 26, 184); height: 90px; margin: 0 auto; min-width: 240px; overflow: hidden;" height="90">
                            <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                <td class="label-box-wrapper" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; text-align: center; background-color: #7e28de; color: white; background: linear-gradient(-135deg, rgb(139, 47, 203) 0%, rgb(126, 40, 222) 70%, rgb(115, 33, 223) 100%); border-radius: 6px; overflow: hidden; padding: 8px 16px;" align="center" bgcolor="#601ab8">
                                    <table style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0;">
                                        <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                            <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background-color: #601ab8; color: white; background: transparent; text-align: left;" bgcolor="#601ab8" align="left">
                                                <img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/icon-valor.png" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                                            </td>
                                            <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background-color: #601ab8; color: white; background: transparent; text-align: left; padding-left: 16px;" bgcolor="#601ab8" align="left">
                                                <h1 style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 32px; font-weight: bold;">R$ {{ $economy }}</h1>
                                                <span style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal;">Em economia com o leilão*</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="economy-text" style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: normal; line-height: 26px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">Todos os dias mais de 2.000 empresas de todo o Brasil aumentam seus lucros comprando na SOUK.</td>
                </tr>

                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="label-box-obs" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 22px; padding: 8px 8px 16px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">*(comparado com preço da tabela regular)</td>
                </tr>
                <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/wave-1.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>

                <tr class="white" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="companies-title" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: bold; line-height: 22px; padding: 8px 16px 16px; background: #eee; text-align: center; background-color: white; color: #333;" align="center" bgcolor="white">Em 2020 vamos trazer novos parceiros da indústria e melhorar nosso aplicativo para ser cada vez mais útil no seu dia-a-dia. </td>
                </tr>
                <tr class="white" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center; background-color: white; color: #333; padding: 0 16px 8px;" align="center" bgcolor="white"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/logos-marcas.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>
                <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/wave-2.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>

                <tr class="light-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="download-title" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 26px; font-weight: bold; line-height: 30px; padding: 16px 0 8px; background: #eee; text-align: center; background-color: #d6f1f4; color: #333;" align="center" bgcolor="#d6f1f4">Vamos crescer juntos em 2020!</td>
                </tr>
                <tr class="light-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="download-text" style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; line-height: 30px; background: #eee; text-align: center; background-color: #d6f1f4; color: #333;" align="center" bgcolor="#d6f1f4">Venha comprar conosco!</td>
                </tr>
                <tr class="light-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="download-buttons-wrapper" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; padding: 16px 32px; background: #eee; text-align: center; background-color: #d6f1f4; color: #333;" align="center" bgcolor="#d6f1f4">
                        <table cellspacing="0" cellpadding="0" class="download-buttons" style="padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; margin: 0 auto;">
                            <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                <td style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background: #eee; text-align: center; background-color: #d6f1f4; color: #333; padding: 0 12px;" align="center" bgcolor="#d6f1f4">
                                    <a href="{{ $download_android }}"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/btn-google.png" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></a>
                                </td>
                                <td style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background: #eee; text-align: center; background-color: #d6f1f4; color: #333; padding: 0 12px;" align="center" bgcolor="#d6f1f4">
                                    <a href="{{ $download_ios }}"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/btn-apple.png" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="light-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="download-legend" style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 22px; padding-bottom: 32px; background: #eee; text-align: center; background-color: #d6f1f4; color: #333;" align="center" bgcolor="#d6f1f4">Baixe nosso aplicativo para começar a economizar!</td>
                </tr>
                <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/wave-3.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>

                <tr class="dark-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="ending-title" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 24px; font-weight: bold; padding: 32px 16px 8px; background: #eee; text-align: center; background-color: #1ca7b6; color: white;" align="center" bgcolor="#1ca7b6">Desejamos um Feliz Natal e um Próspero Ano Novo!</td>
                </tr>
                <tr class="dark-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="ending-team" style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; padding-bottom: 42px; background: #eee; text-align: center; background-color: #1ca7b6; color: white;" align="center" bgcolor="#1ca7b6">Equipe Souk</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="layout-footer" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
        <td style="margin: 0; background: #eee; color: #666; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; line-height: 22px; padding: 16px 0 24px; text-align: center;" align="center">Copyright &copy; 2019 - Todos os direitos reservados.</td>
    </tr>
</table>

<span style="background-image: url({{$tracking_url}})"></span>
</body>
</html>