<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            font-family: Roboto, Helvetica, Arial, sans-serif;
        }
        *[x-apple-data-detectors], .x-gmail-data-detectors, .x-gmail-data-detectors *, .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
    </style>
</head>

<body style="background-color:#eeeeee; color:#333333; margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="background: #7e28de linear-gradient(-135deg, #601AB8 0%, #7e28de 70%, #7321df 100%);height: 90px; width: 100%; text-align: center;">
                <table border="0" cellpadding="0" cellspacing="0" style="max-width: 600px; margin: 0 auto; text-align: center; width: 100%;">
                    <tr><td style="padding: 30px 0 20px;"><img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/souk.png" width="143" height="36" alt="" style="max-width: 100%;"></td></tr>
                    <tr><td style="border: 1px solid #e5e5e5; border-bottom-color: #F2ECFA; height: 24px; background-color: #F2ECFA; border-radius: 3px 3px 0 0;"></td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style=" background-color:  #eeeeee; margin-top: -32px; color: #333; font-family: 'Roboto', Helvetica, Arial, sans-serif;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:  #ffffff;border: none; border-top-color: transparent; border-radius: 0 0 3px 3px; max-width: 600px; margin: 0 auto;">
                    <tr>
                        <td style="text-align: left; height: 267px; padding: 0 16px; background: url('https://souk-company.s3.amazonaws.com/emails/img-email.jpg') no-repeat center center;">
                            <h1 style="font-size: 26px; font-weight: bold; margin-bottom: 4px; margin-top: -48px; width: 320px;">Ol&aacute; {{$name}},</h1>
                            <p style="font-size: 14px; font-weight: normal; margin-bottom:8px; line-height: 20px;">N&oacute;s ouvimos nossos clientes e simplificamos as<br>mensagens de status para pedidos e lances.</p>
                            <p style="font-size: 14px; font-weight: normal; line-height: 20px;">Veja abaixo a explica&ccedil;&atilde;o para todos os status.</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding: 24px 16px 16px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:  #ffffff;border: none; border-top-color: transparent; border-radius: 0 0 3px 3px; max-width: 600px; margin: 0 auto;">
                                <tr>
                                    <th align="left" style="border-bottom: 1px solid #eee; padding: 8px 0; font-size: 16px; font-weight: normal; color: #1ca7b6; text-transform: uppercase;">Lances</th>
                                    <th style="border-bottom: 1px solid #eee; padding: 8px 0; font-size: 12px; font-weight: normal; color: #1ca7b6; text-transform: uppercase;">status</th>
                                    <th style="border-bottom: 1px solid #eee; padding: 8px 0; font-size: 12px; font-weight: normal; color: #1ca7b6; text-transform: uppercase;  white-space: nowrap;">aba no app</th>
                                </tr>

                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando um lance &eacute; criado na Souk</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">O valor ofertado por voc&ecirc; no lance ser&aacute; enviado para a ind&uacute;stria que far&aacute; uma an&aacute;lise, podendo <strong>APROVAR</strong> ou <strong>REJEITAR</strong> o valor.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #ccc; color: #333; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Avaliando pre&ccedil;o</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Abertos</span></td>
                                </tr>


                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando um lance &eacute; aceito pela ind&uacute;stria</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">A ind&uacute;stria aceitou o valor ofertado e o seu lance gerou um pedido, a ind&uacute;stria ir&aacute;
                                            analisar a disponibilidade de estoque.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #639d21; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Pre&ccedil;o aceito</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Abertos</span></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando a disponibilidade do volume &eacute; total</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">A ind&uacute;stria analisou o volume em estoque e conseguiu atender o pedido gerado pelo seu lance em 100% do volume.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #639d21; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Faturado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Fechados</span></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando a disponibilidade de volume &eacute; parcial</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">A ind&uacute;stria analisou o volume em estoque e conseguiu atender o pedido gerado pelo seu lance parcialmente, neste caso a op&ccedil;&atilde;o &quot;Aceito Parcial&quot; foi selecionada na ato de dar um lance.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #639d21; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Faturado parcial</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Fechados</span></td>
                                </tr>

                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando o lance &eacute; rejeitado pela ind&uacute;stria</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">A ind&uacute;stria analisou o valor ofertado e o volume, mas infelizmente n&atilde;o conseguiu aceitar o valor.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #c61b1b; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Rejeitado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Cancelados</span></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando o lance &eacute; cancelado por falta de volume</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">A ind&uacute;stria aceitou o valor ofertado, mas infelizmente n&atilde;o houve disponibilidade de estoque.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #c61b1b; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Cancelado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Cancelados</span></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando o lance n&atilde;o atinge o pedido m&iacute;nimo</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">A ind&uacute;stria aceitou o seu valor, mas ele foi faturado parcialmente e o valor n&atilde;o atinge o valor para o pedido m&iacute;nimo da ind&uacute;stria.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #c61b1b; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Cancelado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Cancelados</span></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando o cliente est&aacute; sem saldo</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">A ind&uacute;stria aceitou o valor ofertado e o volume, mas infelizmente seu saldo na ind&uacute;stria &eacute; menor que o valor gerado.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #c61b1b; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Cancelado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Cancelados</span></td>
                                </tr>

                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando o volume do pedido aumenta</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">Existem alguns produtos que possuem peso vari&aacute;vel, com isso o valor do seu lance pode sofrer um pequeno aumento ap&oacute;s o faturamento.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #639d21; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Faturado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Fechados</span></td>
                                </tr>

                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando o lance &eacute; expirado</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">A ind&uacute;stria n&atilde;o aceitou o valor do seu lance dentro da data de validade estipulada no lance.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #333; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Expirado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Cancelados</span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding: 16px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:  #ffffff;border: none; border-top-color: transparent; border-radius: 0 0 3px 3px; max-width: 600px; margin: 0 auto;">
                                <tr>
                                    <th align="left" style="border-bottom: 1px solid #eee; padding: 8px 0; font-size: 16px; font-weight: normal; color: #1ca7b6; text-transform: uppercase;">Pedidos</th>
                                    <th style="border-bottom: 1px solid #eee; padding: 8px 0; font-size: 12px; font-weight: normal; color: #1ca7b6; text-transform: uppercase;">status</th>
                                    <th style="border-bottom: 1px solid #eee; padding: 8px 0; font-size: 12px; font-weight: normal; color: #1ca7b6; text-transform: uppercase;  white-space: nowrap;">aba no app</th>
                                </tr>

                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando um pedido &eacute; criado na Souk</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">Seu pedido foi enviado para a ind&uacute;stria que analisar&aacute; a disponibilidade de estoque.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #ccc; color: #333; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Itens em an&aacute;lise</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Abertos</span></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando um produto de um pedido &eacute; faturado</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">Cada produto dentro de um pedido tem seu volume dispon&iacute;vel analisado individualmente pela ind&uacute;stria, ao visualizar um pedido no app eles estar&atilde;o no grupo <strong>EM AN&Aacute;LISE</strong>, ap&oacute;s a an&aacute;lise ele pode ir para o grupo <strong>CANCELADO</strong> ou <strong>FATURADO</strong>.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #ccc; color: #333; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Itens em an&aacute;lise</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Abertos</span></td>
                                </tr>


                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando um pedido &eacute; analisado e tem todos os seus itens faturados</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">Neste caso a ind&uacute;stria faturou todos os produtos e n&atilde;o teve nenhuma altera&ccedil;&atilde;o do volume inicial dos produtos.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #639d21; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Faturado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Fechados</span></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando um pedido &eacute; analisado e algum produto teve seu volume aumentado</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">Existem alguns produtos que possuem peso vari&aacute;vel, por isso o valor do seu pedido pode sofrer um pequeno aumento ap&oacute;s o faturamento</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #639d21; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Faturado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Fechados</span></td>
                                </tr>

                                <tr>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando um pedido &eacute; analisado e todos os produtos foram cancelados</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">Neste caso a ind&uacute;stria cancelou todos os produtos de um pedido.</p>
                                    </td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px; border-right: 1px solid #eee;"><span style="background: #c61b1b; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Cancelado</span></td>
                                    <td style="border-bottom: 1px solid #eee; padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Cancelados</span></td>
                                </tr>

                                <tr>
                                    <td style="padding: 16px 8px 16px 0;border-right: 1px solid #eee;">
                                        <h5 style="font-size: 14px; font-weight: bold; line-height: 18px; color: #333; margin-bottom: 4px;">Quando um pedido &eacute; analisado e algum produto teve altera&ccedil;&atilde;o</h5>
                                        <p style="font-size: 12px; line-height: 16px; color: #333;">Neste caso a ind&uacute;stria fez a an&aacute;lise e um ou mais produtos teve seu volume alterado ou foi cancelado, entregando parcialmente o pedido.</p>
                                    </td>
                                    <td style="padding: 16px; border-right: 1px solid #eee;"><span style="background: #639d21; color: white; display:block; border-radius: 12px;height: 20px; line-height: 20px; margin: auto; white-space: nowrap; font-size: 11px; font-weight: bold; text-align: center;padding: 0 4px;">Faturado parcial</span></td>
                                    <td style="padding: 16px;"><span style="background: white; color: #1ca7b6; border: 1px solid #1ca7b6; display:block; border-radius: 3px; height: 20px; line-height: 20px; margin: auto; font-size: 9px; font-weight: 500; text-align: center;padding: 0 4px; text-transform: uppercase;">Fechados</span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td align="center" style="padding: 16px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top: 1px solid #eee;">
                                <tr>
                                    <td align="center">
                                        <p style="margin: 0; font-size: 14px; line-height: 22px; padding-top: 16px; padding-bottom: 8px;">
                                            Caso tenha alguma d&uacute;vida ou sugest&atilde;o sobre os status entre em contato com o nosso<br>Atendimento pelo e-mail <a href="mailto:suporte@souk.com.br" style="color: #601AB8">suporte@souk.com.br</a> ou de Segunda &agrave;
                                            Sexta-Feira das 09h &agrave;s<br>18h, atrav&eacute;s do telefone (11) 3090-0613 ou pelo <a href="https://wa.me/+5511952336062?" style="color: #601AB8">Whatsapp</a> no n&uacute;mero (11) 95233-6062.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: center;">
                <table border="0" cellpadding="0" cellspacing="0" style="max-width: 468px; margin: 0 auto; text-align: center; width: 100%;">
                    <tr>
                        <td style="background-color: #eeeeee; text-align: center; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; padding: 16px; color: #666;">
                            Copyright &copy; 2019 - Todos os direitos reservados.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
​
</html>