<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Souk Email</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">


</head>

<body style="background-color: #eeeeee; color: #333333; margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif;">
<table cellspacing="0" cellpadding="0" class="layout" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; width: 100%;" width="100%">
    <tr class="layout-header" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
        <th style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #7e28de linear-gradient(-135deg, #8b2fcb 0%, #7e28de 70%, #7321df 100%); background-color:#7e28de; text-align: center; width: 100%;" align="center">
            <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; text-align: center; max-width: 500px; width: 500px;">
                <tr><td style="padding: 30px 0 20px;"><img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/souk.png" width="143" height="36" alt="" style="max-width: 100%;"></td></tr>
                <tr><td style="height: 32px; border: 1px solid rgb(28,167,182); background-color: rgb(28,167,182); border-radius: 3px 3px 0 0;"></td></tr>
            </table>
        </th>
    </tr>
    <tr class="layout-body" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
        <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center">
            <table cellspacing="0" cellpadding="0" class="email-content" style="border-radius: 0 0 3px 3px;font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; max-width: 500px; width: 500px; margin: 0 auto;" width="500">
                <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;text-align: center;" align="center"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/header.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>
                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="client-name" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 26px; font-weight: bold; padding: 16px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">Olá {{ $name }},</td>
                </tr>
                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="email-intro" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 22px; padding: 0 16px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">Você sabia que todos os dias você e mais de <strong style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">2.000 empresas</strong>
                        de todo o<br>Brasil aumentam seus lucros comprando na <strong style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">SOUK</strong> e negociando<br>diretamente com a indústria ao utilizar nosso leilão?
                    </td>
                </tr>

                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="year-in-numbers" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: bold; padding: 24px 0; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">Seu ano com a <strong style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">SOUK</strong> em números:</td>
                </tr>
                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">
                        <table cellspacing="0" cellpadding="0" class="yearly-stats" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; table-layout: fixed; width: 100%;" width="100%">
                            <tr class="icons" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background: #eee; background-color: #601ab8; color: white; text-align: center; padding-bottom: 8px;" bgcolor="#601ab8" align="center">
                                    <img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/icon-pedidos.png" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; max-height: 48px; width: auto;">
                                </td>
                                <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background: #eee; background-color: #601ab8; color: white; text-align: center; padding-bottom: 8px;" bgcolor="#601ab8" align="center">
                                    <img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/icon-volume-big.png" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; max-height: 48px; width: auto;">
                                </td>
                                <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background: #eee; background-color: #601ab8; color: white; text-align: center; padding-bottom: 8px;" bgcolor="#601ab8" align="center">
                                    <img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/icon-lance.png" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; max-height: 48px; width: auto;">
                                </td>
                            </tr>
                            <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                                <td style="margin: 0; padding: 0; background: #eee; background-color: #601ab8; color: white; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; text-align: center;" bgcolor="#601ab8" align="center">Pedidos (carrinho)</td>
                                <td style="margin: 0; padding: 0; background: #eee; background-color: #601ab8; color: white; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; text-align: center;" bgcolor="#601ab8" align="center">Volume</td>
                                <td style="margin: 0; padding: 0; background: #eee; background-color: #601ab8; color: white; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; text-align: center;" bgcolor="#601ab8" align="center">Lances</td>
                            </tr>
                            <tr class="stats" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                                @if($orders > 0)
                                <td style="margin: 0; padding: 0; background: #eee; background-color: #601ab8; color: white; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 32px; font-weight: bold; text-align: center;" bgcolor="#601ab8" align="center">{{ $orders }}</td>
                                @else
                                <td style="margin: 0; padding: 0; background: #eee; background-color: #601ab8; color: white; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 11px; text-align: center;" bgcolor="#601ab8" align="center">Nenhum pedido efetuado</td>
                                @endif
                                <td style="margin: 0; padding: 0; background: #eee; background-color: #601ab8; color: white; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 32px; font-weight: bold; text-align: center;" bgcolor="#601ab8" align="center">{{ $volume }}<small style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Kg</small></td>
                                @if($bids > 0)
                                <td style="margin: 0; padding: 0; background: #eee; background-color: #601ab8; color: white; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 32px; font-weight: bold; text-align: center;" bgcolor="#601ab8" align="center">{{ $bids }}</td>
                                @else
                                <td style="margin: 0; padding: 0; background: #eee; background-color: #601ab8; color: white; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 11px; text-align: center;" bgcolor="#601ab8" align="center">Nenhum lance efetuado</td>
                                @endif
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="economy" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; padding: 32px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">
                        <table cellspacing="0" cellpadding="0" style="padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; border-radius: 6px; border: 1px solid rgb(96, 26, 184); height: 90px; margin: 0 auto; overflow: hidden; width: 320px;" width="320" height="90">
                            <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                <td class="economy-wrapper" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; text-align: center; color: white; background: linear-gradient(-135deg, rgb(139, 47, 203) 0%, rgb(126, 40, 222) 70%, rgb(115, 33, 223) 100%); background-color: #7e28de; border-radius: 6px; overflow: hidden; padding: 8px 16px;" align="center" bgcolor="#601ab8">
                                    <table cellspacing="0" cellpadding="0" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0;">
                                        <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                            <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; text-align: center; background-color: #601ab8; color: white; background: transparent;" align="center" bgcolor="#601ab8">
                                                <img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/icon-valor.png" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                                            </td>
                                            <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; text-align: center; background-color: #601ab8; color: white; background: transparent; padding-left: 16px;" align="center" bgcolor="#601ab8">
                                                @if($bids > 0)
                                                    <h1 style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 32px; font-weight: bold;">R$ {{ $economy }}</h1>
                                                    <span style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal;">Em economia com o leilão*</span>
                                                @else
                                                    <span style="margin:0;padding:0;font-family:'Roboto',Helvetica,Arial,sans-serif;font-size: 14px;font-weight:normal;">
                                                        Nossos clientes economizaram<br>
                                                        <b style="font-size: 26px;">R$ 10.000.000</b>
                                                        <br>
                                                        Aproveite também nosso leilão!
                                                    </span>
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="top-products-heading" style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 22px; margin-bottom: 8px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">
                        @if(count($products) === 3)
                            Top 3 produtos mais comprados:
                        @else
                            Produto(s) mais comprado(s):
                        @endif
                    </td>
                </tr>
                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">
                        <table cellspacing="0" cellpadding="0" class="top-products" style="padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; margin: 0 auto;">
                            @foreach($products as $product)
                                <tr class="product-row" style="border-bottom: 1px solid rgba(255, 255, 255, .3);margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                    <td class="top-number" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; text-align: center; background-color: #601ab8; color: white; padding: 16px 0;" align="center" bgcolor="#601ab8"><span style="margin: 0; padding: 0; background: #7e28de linear-gradient(-135deg, #8b2fcb 0%, #7e28de 70%, #7321df 100%); background-color:#7e28de; border-radius: 100%; color: #1ca7b6; display: block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: bold; height: 32px; line-height: 32px; text-align: center; width: 32px;">{{ $product['ranking'] }}</span></td>
                                    <td class="product-image" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background: #eee; text-align: center; background-color: #601ab8; color: white; padding: 16px;" align="center" bgcolor="#601ab8"><img src="{{ $product['picture_url'] }}" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; width: 48px;" width="48">
                                    </td>
                                    <td class="product-details" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background: #eee; background-color: #601ab8; color: white; padding: 16px 0; text-align: left;" bgcolor="#601ab8" align="left">
                                        <h2 style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 20px;">{{$product['name']}}</h2>
                                        <span style="margin: 0; padding: 0; color: rgba(255, 255, 255, 0.7); font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal;">{{$product['brand']}}</span>
                                        <h4 style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal;">{{$product['volume']}}Kg</h4>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
                <tr class="purple" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="economy-obs" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 22px; padding: 8px; background: #eee; text-align: center; background-color: #601ab8; color: white;" align="center" bgcolor="#601ab8">*(comparado com preço da tabela regular)</td>
                </tr>
                <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/wave-1.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>

                <tr class="white" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="companies-title" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: bold; line-height: 22px; padding: 8px 0 16px; background: #eee; text-align: center; background-color: white; color: #333;" align="center" bgcolor="white">Em 2020 teremos novas indústrias parceiras</td>
                </tr>
                <tr class="white" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center; background-color: white; color: #333; padding: 0 16px 8px;" align="center" bgcolor="white"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/logos-marcas.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>
                <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/wave-2.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>

                <tr class="light-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="nps-title" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: bold; padding: 16px 0; background: #eee; text-align: center; background-color: #d6f1f4; color: #333;" align="center" bgcolor="#d6f1f4">Sua opinião é muito importante para nós!</td>
                </tr>
                <tr class="light-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="nps-intro" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 22px; padding: 0 16px; background: #eee; text-align: center; background-color: #d6f1f4; color: #333;" align="center" bgcolor="#d6f1f4">Um dos nossos objetivos é melhorar nosso aplicativo para ser cada vez<br>mais
                        útil no seu dia-a-dia. Nos ajude a tornar isso possível, responda uma<br>breve pesquisa iniciando
                        com a pergunta abaixo (são só 5 perguntinhas):
                    </td>
                </tr>
                <tr class="light-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="nps-question" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; padding: 24px 32px; background: #eee; text-align: center; background-color: #d6f1f4; color: #333;" align="center" bgcolor="#d6f1f4">Qual a probabilidade de você recomendar a Souk para um amigo ou colega?</td>
                </tr>
                <tr class="light-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="nps-numbers-wrapper" style="margin: 0; font-family: Roboto, Helvetica, Arial, sans-serif; padding: 0 32px 24px; background: #eee; text-align: center; background-color: #d6f1f4; color: #333;" align="center" bgcolor="#d6f1f4">
                        <table cellspacing="0" cellpadding="0" class="nps-numbers" style="padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; margin: 0 auto;">
                            <tr class="numbers" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden;">
                                <td colspan="2" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; border-radius: 3px 3px 0 0; overflow: hidden; background: #eee; background-color: #d6f1f4; color: #333; text-align: left;" bgcolor="#d6f1f4" align="left">
                                    <ul style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: flex; width: 100%;">
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: 0;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; border-radius: 3px 0 0 3px; margin: 0;">0</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">1</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">2</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">3</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">4</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">5</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">6</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">7</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">8</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; margin: 0;">9</a></li>
                                        <li style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; display: inline-block; flex: 1; margin-left: -1px;"><a href="{{ $nps_url }}" style="padding: 0; background: #f5ecff; border: 1px solid #601ab8; color: #601ab8; display: inline-block; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; height: 40px; line-height: 40px; text-align: center; text-decoration: none; width: 37px; border-radius: 0 3px 3px 0; margin: 0;">10</a></li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="legend" style="margin: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; line-height: 22px; padding: 4px 0;">
                                <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; background-color: #d6f1f4; color: #333; text-align: left;" bgcolor="#d6f1f4" align="left">Nem um pouco provável</td>
                                <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; background-color: #d6f1f4; color: #333; text-align: right;" bgcolor="#d6f1f4" align="right">Extremamente provável</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif; background: #eee; text-align: center;" align="center"><img src="https://souk-company.s3.amazonaws.com/emails/final-de-ano-2019/wave-3.jpg" width="100%" style="display:block;margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;"></td>
                </tr>

                <tr class="dark-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="ending-title" style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 24px; font-weight: bold; padding-top: 32px; background: #eee; text-align: center; background-color: #1ca7b6; color: white;" align="center" bgcolor="#1ca7b6">Obrigado! Vamos crescer juntos em 2020!</td>
                </tr>
                <tr class="dark-green" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="ending-text" style="margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: normal; padding-top: 4px; background: #eee; text-align: center; background-color: #1ca7b6; color: white;" align="center" bgcolor="#1ca7b6">Desejamos um Feliz Natal e um Próspero Ano Novo!</td>
                </tr>
                <tr class="dark-green" style="border-radius: 0 0 3px 3px; overflow: hidden; margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
                    <td class="ending-team" style="border-radius: 0 0 3px 3px; overflow: hidden;margin: 0; padding: 0; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; padding-top: 16px; padding-bottom: 42px; background: #eee; text-align: center; background-color: #1ca7b6; color: white;" align="center" bgcolor="#1ca7b6">Equipe Souk</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="layout-footer" style="margin: 0; padding: 0; font-family: Roboto, Helvetica, Arial, sans-serif;">
        <td style="margin: 0; background: #eee; color: #666; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; line-height: 22px; padding: 16px 0 24px; text-align: center;" align="center">Copyright &copy; 2019 - Todos os direitos reservados.</td>
    </tr>
</table>

<span style="background-image: url({{$tracking_url}})"></span>
</body>
</html>