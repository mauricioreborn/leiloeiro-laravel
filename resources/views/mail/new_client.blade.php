@include('mail.layout')
<tr style="box-sizing:border-box;">
    <td class="content-wrap" style="box-sizing:border-box;vertical-align:top;padding:50px 30px;background:#f5f5f5;">
        <table width="100%" cellpadding="0" cellspacing="0" style="box-sizing:border-box;">
            <tr style="box-sizing:border-box;">
                <td class="content-block" style="box-sizing:border-box;vertical-align:top;padding:0 0 20px;">
                    <h2 style="color:#666;box-sizing:border-box;margin-bottom:10px;font-weight:normal;font-size: 20px;">Ol&aacute; {{(trim($params['name'])!=""? $params['name']:"")}}!</h2>
                </td>
            </tr>
            <tr style="box-sizing:border-box;">
                <td class="content-block" style="box-sizing:border-box;vertical-align:top;padding:0 0 0px;">
                    <p style="font-size:14px;color: #777777;box-sizing:border-box;margin-bottom:10px;font-weight:normal;line-height: 24px;margin-top: -5px;">
                        Seu cadastro na Souk foi realizado com sucesso! <br>

                        Nome: {{$params['name']}} <br>
                        Email: {{$params['email']}} <br>
                        CNPJ/Login: {{intval($params['cnpj'])}} <br>
                        Senha: {{$params['new_password']}} <br>

                        <br>Agora você pode fazer login com o <strong>CNPJ</strong> e <strong>senha</strong> informados acima.
                        Para alterar sua senha acesse o aplicativo no <i>Menu -&gt; Meus dados -&gt; Alterar senha</i>.
                    </p>
                </td>
            </tr>
            <tr style="box-sizing:border-box;">
                <td class="content-block" style="box-sizing:border-box;vertical-align:top;padding:0 0 20px;">
                    <p>
                        <span style="font-size:14px;color: #777777; line-height: 20px;">
                            Se ainda houver alguma dúvida relacionada ao seu pedido, fique a vontade para entrar em contato conosco pelo e-mail
                            <a style="color: #601AB8;" href="mailto:suporte@souk.com.br">suporte@souk.com.br</a> ou via
                            <a href="https://api.whatsapp.com/send?1=pt_BR&amp;phone=5511952336062" style="color: #601AB8;">Whatsapp</a> no número (11) 95233-6062.
                        </span>
                    </p>

                </td>
            </tr>
            <tr style="box-sizing:border-box;">
                <td class="content-block" style="box-sizing:border-box;vertical-align:top;padding:0 0 20px;">
                    <p style="box-sizing:border-box;margin-bottom:10px;font-weight:normal;color: #2c3d50;">
                    </p>

                    <hr style="box-sizing:border-box;border-top: 1px solid #dddddd;border-color: #dddddd;margin-top:10px;">
                    <span style="font-size:10px;color: #777777;">Data / Hora da solicitação: {{ now()->format("d/m/Y - H:i:s") }}.</span>
                </td>
            </tr>
        </table>
    </td>
</tr>
@include('mail.footer')
