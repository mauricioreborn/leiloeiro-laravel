<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Title Base</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        *[x-apple-data-detectors], .x-gmail-data-detectors, .x-gmail-data-detectors *, .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
    </style>
</head>

<body style="background-color:#eeeeee; color:#333333; margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="background: linear-gradient(-135deg, #601AB8 0%, #7e28de 70%, #7321df 100%); background-color: #7e28de; height: 90px; width: 100%; text-align: center;">
                <table border="0" cellpadding="0" cellspacing="0" style="max-width: 468px; margin: 0 auto; text-align: center; width: 100%;">
                    <tr><td style="padding: 30px 0 20px;"><img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/souk.png" width="143" height="36" alt="" style="max-width: 100%;"></td></tr>
                    <tr><td style="border: 1px solid #e5e5e5; border-bottom-color: transparent; height: 16px; background-color: white; border-radius: 3px 3px 0 0;"></td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style=" background-color:  #eeeeee; margin-top: -32px; color: #333; font-family: 'Roboto', Helvetica, Arial, sans-serif;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:  #ffffff;border: 1px solid #e5e5e5; border-top-color: transparent; border-radius: 0 0 3px 3px; max-width: 468px; margin: 0 auto; padding: 0 15px 32px;">
                    <tr>
                        <td align="center" style="padding-bottom: 16px; padding-left: 16px; padding-right: 16px;">
                            <img src="https://souk-company.s3.amazonaws.com/emails/cadastro_na_base.jpg" width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 26px; font-weight: bold; padding-bottom: 8px;">
                            <b>Parab&eacute;ns {{$params['contact_name']}}!</b>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 14px; line-height: 22px;padding-bottom: 24px;">
                            Abaixo est&atilde;o os dados para voc&ecirc; acessar o app e aproveitar as oportunidades que a <b>SOUK</b> tem para voc&ecirc;, compre pelo valor ofertado ou d&ecirc; seu lance para conseguir ainda melhores pre&ccedil;os.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 24px;">
                            <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                <td align="center" style="text-align: center">
                                    <table class="info-box" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #d6f1f4; border-radius: 3px; border-style: solid; border-color: #21bbcc; padding: 15px; text-align: center; font-family: 'Roboto', Helvetica, Arial, sans-serif; border-width: 1px; max-width: 310px; margin: 0 auto; padding: 16px;">
                                        <tr>
                                            <td style="font-size: 12px; font-weight: normal; color: #333333; padding-bottom: 8px;">LOGIN DE ACESSO</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 24px; font-weight: bold; color: #333333;">{{$params['cnpj']}}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px; font-weight: normal; line-height: 22px; padding-bottom: 16px;"><i>Para acessar o app voc&ecirc; usar&aacute; seu CNPJ</i></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px; font-weight: normal; color: #333333; padding-bottom: 8px;">SENHA</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 24px; font-weight: bold; color: #333333; padding-bottom: 8px;"> <b>{{$params['password']}}</b> </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px; font-weight: normal; color: #333333;">
                                                <i>Para alterar sua senha acesse no aplicativo: Menu > Meus dados > Alterar senha.</i>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 24px;">
                            <table cellpadding="0" border="0" cellspacing="0" width="100%"  style="text-align: center">
                                <tr>
                                    <td style="color: #1ca7b6; font-size: 12px; font-weight: normal; padding-bottom: 8px;">DADOS CADASTRAIS</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 20px; font-weight: bold;">
                                        <b>{{$params['name']}}</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 24px;">
                            <table cellpadding="0" border="0" cellspacing="0" width="100%"  style="text-align: center">
                                <tr>
                                    <td style="color: #1ca7b6; font-size: 12px; font-weight: normal; padding-bottom: 8px;">ENDERE&Ccedil;O DE ENTREGA</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 16px; font-weight: bold; line-height: 22px;"> <b> {{$params['address']}}, {{$params['city']}} - {{$params['state']}} </b> </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 32px;">
                            <table cellpadding="0" border="0" cellspacing="0" width="100%"  style="text-align: center">
                                <tr>
                                    <td style="color: #1ca7b6; font-size: 12px; padding-bottom: 8px;">DADOS DE CONTATO</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 16px; font-weight: bold; line-height: 22px;">
                                        <b>{{$params['contact_name']}}</b> <br>
                                        <b>{{ formatPhone($params['phone']) }}</b> <br>
                                        <b>{{$params['email']}}</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="border-top: 1px solid  #e5e5e5; border-bottom: 1px solid  #e5e5e5; font-size: 14px; line-height: 22px; padding-top: 16px; padding-bottom: 16px;">
                            <p style="margin: 0;">Caso algum dos dados acima estiver errado ou voc&ecirc; tenha qualquer outra d&uacute;vida, entre em contato pelo e-mail  <a href="mailto:suporte@souk.com.br" style="color: #601ab8;text-decoration: none;">suporte@souk.com.br</a> ou via <a href="https://wa.me/+5511952336062?" style="color: #601ab8;text-decoration: none;">Whatsapp</a> no n&uacute;mero (11) 95233-6062.</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 32px;">
                            <table cellpadding="0" border="0" cellspacing="0" width="100%" align="center">
                                <tr>
                                    <td align="center" colspan="4" style="font-size: 20px; font-weight: bold; color: #1ca7b6;padding-bottom: 24px;">
                                        Marcas que voc&ecirc; j&aacute; pode comprar. Acesse agora!
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; padding: 0 10px">
                                        <img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/logo-seara.jpg" alt="Seara" style="display: block; max-width: 100%" />
                                    </td>
                                    <td style="text-align: center; padding: 0 10px">
                                        <img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/logo-doriana.jpg" alt="Doriana" style="display: block; max-width: 100%" />
                                    </td>
                                    <td style="text-align: center; padding: 0 10px">
                                        <img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/logo-rezende.jpg" alt="Rezende" style="display: block; max-width: 100%" />
                                    </td>
                                    <td style="text-align: center; padding: 0 10px">
                                        <img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/logo-massaleve.jpg" alt="Souk" style="display: block; max-width: 100%" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 32px;">
                            <table cellpadding="0" border="0" cellspacing="0" width="100%" align="center">
                                <tr>
                                    <td style="font-size: 20px; font-weight: bold; color: #1ca7b6;" align="center">
                                        Aumente o lucro do seu neg&oacute;cio
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; font-size: 14px; font-weight: normal; line-height: 22px">
                                        <p style="margin: 0; font-size: 16px; color: #1ca7b6; padding-bottom: 16px;">Negocie diretamente com a ind&uacute;stria</p>
                                        <p style="margin: 0;">Com a <b>SOUK</b> voc&ecirc; tem poder de negocia&ccedil;&atilde;o, seja para comprar um produto ou para dar um lance, voc&ecirc; diz quanto est&aacute; disposto a pagar!</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a target="_blank" href="http://www.souk.com.br/?utm_source=cadastro_app&utm_medium=email_base&utm_campaign=btn_saiba_mais" style="margin: 25px auto 0; text-align: center; padding: 10px 15px; display: block; background-color: #601ab8; color: #fff; width: 148px; font-family: 'Roboto', Helvetica, Arial, sans-serif; text-decoration: none; border-radius: 3px">SAIBA MAIS</a></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: center;">
                <table border="0" cellpadding="0" cellspacing="0" style="max-width: 468px; margin: 0 auto; text-align: center; width: 100%;">
                    <tr>
                        <td style="background-color: #eeeeee; text-align: center; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; padding: 16px; color: #666;">
                            Copyright &copy; 2019 - Todos os direitos reservados.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
​
</html>