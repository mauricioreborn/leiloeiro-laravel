<!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Title Base</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        *[x-apple-data-detectors], .x-gmail-data-detectors, .x-gmail-data-detectors *, .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
    </style>
</head>

<body style="background-color:#eeeeee; color:#333333; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="background: linear-gradient(-135deg, #601AB8 0%, #7e28de 70%, #7321df 100%); background-color: #7e28de; height: 90px; width: 100%; text-align: center;">
            <table border="0" cellpadding="0" cellspacing="0" style="max-width: 500px; margin: 0 auto; text-align: center; width: 100%;">
                <tr><td style="padding: 30px 0 26px;"><img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/souk.png" width="143" height="36" alt="" style="max-width: 100%;"></td></tr>
                <tr><td style="border: 1px solid #e5e5e5; border-bottom-color: transparent; height: 26px; background-color: white; border-radius: 3px 3px 0 0;"></td></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style=" background-color:  #eeeeee; margin-top: -32px; color: #333; font-family: 'Roboto', Helvetica, Arial, sans-serif;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:  #ffffff;border: 1px solid #e5e5e5; border-top-color: transparent; border-radius: 0 0 3px 3px; max-width: 500px; margin: 0 auto; padding: 0 15px 24px;">
                <tr>
                    <td align="center" style="font-size: 26px; font-weight: bold; padding-bottom: 8px;">
                        <b>Desconto especial para voc&ecirc;!</b>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size: 14px; line-height: 22px;padding-bottom: 20px;">
                        O produto abaixo est&aacute; com pre&ccedil;o exclusivo no aplicativo Souk.
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; padding-bottom: 24px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:  #f9f9f9; border: 1px solid #e5e5e5; border-radius: 3px; padding: 16px;">
                            <tr>
                                <td style="padding: 0 16px 0 0; vertical-align: top;"><img src="{{ $product['image_url'] }}" style="max-width: 140px; width: 100%;"></td>
                                <td style="text-align: left;">
                                    <h2 style="font-size: 20px; font-weight: bold;">{{ $product['name'] }}</h2>
                                    <p style="color: #666666; font-size: 14px; font-weight: normal; line-height: 20px; margin-bottom: 4px;">{{ $product['company'] }}</p>
                                    <p style="font-size: 16px; font-weight: normal; line-height: 20px; margin-bottom: 8px;">{{ $product['shelf_life'] }}</p>
                                    <span style="background: #1ca7b6; border-radius: 12px; color: white; display: inline-block; height: 24px; font-size: 16px; font-weight: 500; margin-bottom: 16px; text-align: center; line-height: 24px; padding: 0 8px;">{{ $product['discount_percentage'] }}% desconto</span>

                                    <br>
                                    <p style="padding: 8px; background: #eeeeee; border-radius: 3px; display: inline-block; font-style: italic;">
                                        <strong style="font-size: 24px; color: #1ca7b6; line-height: 24px; font-weight: 900; display: block; margin-bottom: 2px;">{{ $product['price'] }}</strong>
                                        <small style="font-size: 14px; line-height: 18px; display: block;">{{ $product['kg_price'] }}</small>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 16px; padding-top: 0; text-align: center">
                        <a href="{{ $link }}" style="margin: 0 auto; text-align: center; padding: 10px; display: block; color: #fff; width: 184px; font-family: 'Roboto', Helvetica, Arial, sans-serif; text-decoration: none; text-transform: uppercase; border-radius: 3px; background-color: #601ab8; font-size: 18px; font-weight: 500;">Confira</a>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size: 14px; line-height: 22px;">Desconto v&aacute;lido at&eacute; {{ $duration }} ou at&eacute; durarem os estoques.</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 100%; text-align: center;">
            <table border="0" cellpadding="0" cellspacing="0" style="max-width: 468px; margin: 0 auto; text-align: center; width: 100%;">
                <tr>
                    <td style="background-color: #eeeeee; text-align: center; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; padding: 16px; color: #666;">
                        Copyright &copy; {{ now()->format('Y') }} - Todos os direitos reservados.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<span style="background-image: url({{$tracking_url}})"></span>
</body>
​
</html>