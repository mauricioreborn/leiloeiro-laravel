<!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400|500&display=swap" rel="stylesheet">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            font-family: Roboto, Helvetica, Arial, sans-serif;
        }
        *[x-apple-data-detectors], .x-gmail-data-detectors, .x-gmail-data-detectors *, .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
    </style>
</head>

<body style="background-color:#eeeeee; color:#333333; margin: 0; padding: 24px 0;font-family: 'Roboto', Arial, sans-serif;font-size: 16px; font-weight: normal;">
<table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; max-width: 600px; width: 100%;">
    <tr>
        <td style="background: #601ab8;padding: 11px 0;width: 100%; text-align: center;">
            <img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/souk.png" height="26" alt="">
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://souk-company.s3.amazonaws.com/emails/tirolez/header-D-0.jpg" style="display:block;margin: 0; padding: 0; max-width: 100%">
        </td>
    </tr>
    <tr>
        <td style="background: #f9f9f9; padding: 20px 20px 32px;">
            <p style="line-height: 24px;margin-bottom: 24px;">A <strong>SOUK</strong> fechou mais uma parceria, dessa vez com a marca que é famosa pela diversidade e qualidade dos seus queijos. Como você é cliente Tirolez, você já pode comprar via carrinho ou dar lance agora mesmo.</p>
            <h4 style="font-size: 18px; font-weight: bold; line-height: 24px; margin-bottom: 16px;">Veja alguns benefícios de comprar com a Souk:</h4>
            <p style="line-height: 24px;margin-bottom: 8px;">- Ótimos preços com descontos de até 70%;</p>
            <p style="line-height: 24px;margin-bottom: 8px;">- Negociação direta utilizando nosso leilão, podendo pagar ainda mais barato;</p>
            <p style="line-height: 24px;margin-bottom: 8px;">- Dê lances e compre via carrinho 24h por dia;</p>
            <p style="line-height: 24px;margin-bottom: 24px;">- O pedido mínimo é de R$ 300,00 com frete grátis.</p>
            <h4 style="font-size: 18px; font-weight: bold; line-height: 24px; margin-bottom: 16px;">Corre e vem aproveitar os preços de lançamento!</h4>
            <a href="{{ $link }}" style="text-decoration: none;cursor:pointer;display:inline-block; margin: 16px 0 24px; padding: 18px 24px; background: #1ca7b6; border-radius: 3px; color: white; font-size: 20px; font-weight: 500; letter-spacing: 0; line-height: 20px; text-align: center; text-transform: uppercase;">Acessar loja Tirolez</a>
            <p style="line-height: 22px;font-size: 14px;">
                Caso tenha alguma dúvida entre em contato com o nosso Atendimento pelo e-mail<br>
                <a href="mailto:suporte@souk.com.br" style="color: #601AB8;text-decoration: none;">suporte@souk.com.br</a> ou de Segunda &agrave; Sexta-Feira das 09h &agrave;s 18h, atrav&eacute;s do <br>
                <a href="https://wa.me/+5511952336062?" style="color: #601AB8;text-decoration: none;">Whatsapp</a> no número (11) 95233-6062.
            </p>
        </td>
    </tr>
    <tr>
        <td style="background: #4b1392; color:white; font-size: 12px;line-height: 24px; width: 100%; padding:8px 0; text-align: center;">
            © 2020 - Todos os direitos reservados
        </td>
    </tr>
</table>

<span style="background-image: url({{$tracking_url}})"></span>
</body>
​
</html>