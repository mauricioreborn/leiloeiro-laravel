<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Souk API</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <style type="text/css">
        @media screen {
            body {
                font-family: "Roboto", sans-serif;
                margin: 0;
                padding: 0;
                min-width: 100% !important;
            }
        }

        body {
            font-family: "Roboto", Arial, sans-serif;
            margin: 0;
            padding: 0;
            min-width: 100% !important;
        }

    </style>
</head>

<body>
<p style="box-sizing:border-box;margin-bottom:10px;font-weight:normal;" /></p>
<table class="body-wrap" style="box-sizing:border-box;background-color:#fff;width:100%;">
    <tr style="box-sizing:border-box;">
        <td style="box-sizing:border-box;vertical-align:top;"></td>
        <td class="container" width="700" style="box-sizing:border-box;vertical-align:top;display:block !important;max-width:700px !important;margin:0 auto !important;clear:both !important;">
            <div class="content" style="box-sizing:border-box;max-width:650px;margin:0 auto;display:block;padding:20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="box-sizing:border-box;background:#fff;border:1px solid #e9e9e9;">
                    <tr style="box-sizing:border-box;">
                        <td class="alert alert-good" style="box-sizing:border-box;vertical-align:top;font-size:16px;color:#fff;font-weight:500;padding:40px;text-align:center;background: #7e28de; background-color: #7e28de;">
                            <img src="https://web.souk.com.br/src/img/logo@2x.png" alt="Souk" style="box-sizing:border-box;width:50%;" />
                        </td>
                    </tr>

