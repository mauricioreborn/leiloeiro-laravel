<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Title Independentes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        *[x-apple-data-detectors], .x-gmail-data-detectors, .x-gmail-data-detectors *, .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
    </style>
</head>

<body style="background-color:#fff;color: #333;font-family: 'Roboto', Helvetica, Arial, sans-serif;">
<table border="0" cellpadding="0" cellspacing="0" style="box-sizing:border-box;background-color:#fff; width:100%; max-width: 600px;margin:0 auto;">
    <tr>
        <td style="background-color: #7e28de;border:1px solid #7e28de; height: 150px; width: 100%; text-align: center;"><img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/souk.png" height="68" alt="" style="max-width: 100%;"></td>
    </tr>
    <tr>
        <td style="background-color:#f5f5f5;border:1px solid #eeeeee;padding:48px 32px 32px;text-align: left;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr><th style="font-size: 20px; font-weight: bolder; height: 24px; letter-spacing: 0;padding-bottom: 16px;">OL&Aacute; {{ $name }},</th></tr>

                <tr><td style="font-size: 20px; font-weight: normal; height: 24px; letter-spacing: 0;padding-bottom: 16px;">O lance <strong>#{{ $bidId }}</strong> foi atualizado.</td></tr>
                <tr>
                    <td style="padding-bottom: 24px;">
                        @if($status === 'success')
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;background: #d0e1bc; border-radius: 3px; height: 56px; padding: 8px 16px;">
                                <tr>
                                    <td><span style="background: #639d21; color: white; display:block; border-radius: 12px;height: 24px; line-height: 24px; margin: auto; white-space: nowrap; font-size: 14px; font-weight: 500; text-align: center;padding: 0 4px;">{{ $statusLabelDescription }}</span></td>
                                    <td style="font-size: 14px; font-weight: 400; line-height: 20px;padding-left: 16px;">{{ $statusDescription }}</td>
                                </tr>
                            </table>
                        @elseif($status === 'danger')
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;background: #f7d7d8; border-radius: 3px; height: 56px; padding: 8px 16px;">
                                <tr>
                                    <td><span style="background: #c61b1b; color: white; display:block; border-radius: 12px;height: 24px; line-height: 24px; margin: auto; white-space: nowrap; font-size: 14px; font-weight: 500; text-align: center;padding: 0 4px;">{{ $statusLabelDescription }}</span></td>
                                    <td style="font-size: 14px; font-weight: 400; line-height: 20px;padding-left: 16px;">{{ $statusDescription }}</td>
                                </tr>
                            </table>
                        @elseif($status === 'neutral')
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;background: #e5e5e5; border-radius: 3px; height: 56px; padding: 8px 16px;">
                                <tr>
                                    <td><span style="background: #333333; color: white; display:block; border-radius: 12px;height: 24px; line-height: 24px; margin: auto; white-space: nowrap; font-size: 14px; font-weight: 500; text-align: center;padding: 0 4px;">{{ $statusLabelDescription }}</span></td>
                                    <td style="font-size: 14px; font-weight: 400; line-height: 20px;padding-left: 16px;">{{ $statusDescription }}</td>
                                </tr>
                            </table>
                        @endif
                    </td>
                </tr>

                <tr><th style="color: #333; font-size: 14px; font-weight: bolder;padding-bottom: 8px;">DETALHES DO LANCE</th></tr>
                <tr>
                    <td style="padding-bottom: 24px">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;border:1px solid #e5e5e5;background-color:white;">
                            <tr>
                                <td style="padding: 8px 16px;">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td style="font-size: 14px;line-height: 20px; white-space: nowrap">
                                                Situação do lance:
                                                @if($status === 'success')
                                                    <strong style="color:#639d21; text-transform: uppercase;">{{ $statusLabel }}</strong>
                                                @elseif($status === 'danger')
                                                    <strong style="color:#c61b1b; text-transform: uppercase;">{{ $statusLabel }}</strong>
                                                @else
                                                    <strong style="text-transform: uppercase;">{{ $statusLabel }}</strong>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr><td style="font-size: 14px;line-height: 20px;white-space: nowrap;">Cód. indústria: <strong>{{ $companyCode }}</strong></td></tr>
                                        <tr><td style="font-size: 14px;line-height: 20px;white-space: nowrap;">Data de criação: <strong>{{ $bidDate->format('d/m/Y') }}</strong></td></tr>
                                        <tr><td style="font-size: 14px;line-height: 20px;white-space: nowrap;">Validade do lance: <strong>{{ $bidExpirationDate->format('d/m/Y') }}</strong></td></tr>
                                    </table>
                                </td>
                                <td style="padding: 8px 16px;border-left: 1px solid #e5e5e5;">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr><td style="font-size: 14px;line-height: 20px;">Endereço de entrega:</td></tr>
                                        <tr><td style="font-size: 14px;line-height: 20px;"><strong>{{ $address }} {{ $city }}/{{ $state }}</strong></td></tr>
                                        @if($deliveryDate)
                                            <tr><td style="font-size: 14px;line-height: 20px;">Previsão de entrega: <strong>{{ $deliveryDate->format('d/m/Y') }}</strong></td></tr>
                                        @endif
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><th style="color: #333; font-size: 14px; font-weight: bolder;padding-bottom: 8px;">FORMA DE PAGAMENTO</th></tr>
                <tr>
                    <td style="padding-bottom: 24px">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;border:1px solid #e5e5e5;background-color:white;">
                            <tr>
                                <td style="filter: grayscale(100%);-webkit-filter: grayscale(100%);font-size:14px;font-weight:500;padding: 12px 16px;line-height: 20px;vertical-align: middle;position:relative;">
                                    <img src="{{ $paymentMethodIcon }}" width="24" style="margin-right: 8px;line-height: 20px;">
                                    <span style="position: absolute;white-space:nowrap;">{{ $paymentMethodLabel }}</span>
                                </td>
                                <td style="font-size:14px;font-weight:500;padding: 12px 16px;line-height: 20px;vertical-align: middle;position: relative;">
                                    @if($paymentMethod === 'credit_card')
                                        <div style="position: absolute; right: 16px; top:12px; display: inline-flex; align-items: center; height: 24px;white-space:nowrap;">
                                            <img src="{{ $creditCardBrandIcon }}" width="32" style="margin-right: 8px;line-height: 20px;">
                                            <span style="">{{ $creditCardDescription }}</span>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="padding-bottom: 16px;">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;border: 1px solid #e5e5e5;">
                            <tr>
                                <th style="background-color:#eee;border-bottom: 1px solid #e5e5e5;border-right: 1px solid #e5e5e5;font-size: 14px; font-weight: bold;padding:12px 8px;">Produto</th>
                                <th style="background-color:#eee;border-bottom: 1px solid #e5e5e5;border-right: 1px solid #e5e5e5;font-size: 14px; font-weight: bold; text-align: right;padding:12px 8px;">Kg</th>
                                <th style="background-color:#eee;border-bottom: 1px solid #e5e5e5;border-right: 1px solid #e5e5e5;font-size: 14px; font-weight: bold; text-align: right;padding:12px 8px;">Valor</th>
                                <th style="background-color:#eee;border-bottom: 1px solid #e5e5e5;font-size: 14px; font-weight: bold; text-align: right;padding:12px 8px;">Total</th>
                            </tr>

                            @foreach($products as $product)
                            <tr>
                                <td style="background-color:white;border-bottom: 1px solid #e5e5e5;border-right: 1px solid #e5e5e5;font-size: 12px;padding:12px 8px;">{{ $product['name'] }}</td>
                                <td style="background-color:white;border-bottom: 1px solid #e5e5e5;border-right: 1px solid #e5e5e5;font-size: 12px;text-align: right;padding:12px 8px;">{{ volume($product['volume'], ',', '', 'Kg') }}</td>
                                <td style="background-color:white;border-bottom: 1px solid #e5e5e5;border-right: 1px solid #e5e5e5;font-size: 12px;text-align: right;padding:12px 8px;">{{ formata_moeda($product['kg_price']) }}</td>
                                <td style="background-color:white;border-bottom: 1px solid #e5e5e5;font-size: 12px;text-align: right;padding:12px 8px;">{{ formata_moeda($product['price']) }}</td>
                            </tr>
                            @endforeach

                            <tr>
                                <td style="background-color:white;border-bottom: 1px solid #e5e5e5;border-right: 1px solid #e5e5e5;font-size: 12px;text-align: right;padding:12px 8px;" colspan="3">TAXA DE ENVIO:</td>
                                <td style="background-color:white;border-bottom: 1px solid #e5e5e5;font-size: 12px;text-align: right;padding:12px 8px;">FRETE GRÁTIS</td>
                            </tr>
                            <tr>
                                <td style="background-color:white;border-bottom: 1px solid #e5e5e5;border-right: 1px solid #e5e5e5;font-size: 14px; font-weight:bold;text-align: right;padding:12px 8px;" colspan="3">TOTAL:</td>
                                <td style="background-color:white;font-size: 14px; font-weight:bold;text-align: right;padding:12px 8px;">{{ formata_moeda($total) }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><th style="color: #333; font-size: 14px; font-weight: bolder;padding-bottom: 8px;">IMPORTANTE:</th></tr>
                @if($showNestleWarning)
                    <tr><td style="font-size:14px;line-height: 22px;padding-bottom: 4px;">- Os produtos da Nestlé Iogurtes não aceitam devolução</td></tr>
                @endif
                <tr><td style="font-size:14px;line-height: 22px;padding-bottom:24px;">- A previsão de entrega é uma data estimada e pode sofrer alterações.</td></tr>

                <tr><td style="font-size:14px;line-height: 20px;padding-bottom:24px;">Se ainda houver alguma dúvida relacionada ao seu pedido, fique a vontade para entrar em contato conosco pelo e-mail <a href="mailto:suporte@souk.com.br" style="color: #601ab8;text-decoration: none;">suporte@souk.com.br</a> ou via <a href="https://wa.me/+5511952336062?" style="color: #601ab8;text-decoration: none;">Whatsapp</a> no número (11) 95233-6062</td></tr>
            </table>
    </td>
    </tr>
    <tr>
        <td style="background-color: #eeeeee; text-align: center; font-size: 12px; font-weight: normal; padding: 16px; color: #666;">
            Copyright &copy; 2019 - Todos os direitos reservados.
        </td>
    </tr>
</table>
</body>
</html>
