<!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Title Base</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        *[x-apple-data-detectors], .x-gmail-data-detectors, .x-gmail-data-detectors *, .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
    </style>
</head>

<body style="background-color:#eeeeee; color:#333333; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="background: linear-gradient(-135deg, #601AB8 0%, #7e28de 70%, #7321df 100%); background-color: #7e28de; height: 90px; width: 100%; text-align: center;">
            <table border="0" cellpadding="0" cellspacing="0" style="max-width: 468px; margin: 0 auto; text-align: center; width: 100%;">
                <tr><td style="padding: 30px 0 20px;"><img src="https://souk-company.s3.amazonaws.com/emails/cadastro-souk/IndependentClientWelcome/souk.png" width="143" height="36" alt="" style="max-width: 100%;"></td></tr>
                <tr><td style="border: 1px solid #e5e5e5; border-bottom-color: transparent; height: 32px; background-color: white; border-radius: 3px 3px 0 0;"></td></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style=" background-color:  #eeeeee; margin-top: -32px; color: #333; font-family: 'Roboto', Helvetica, Arial, sans-serif;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:  #ffffff;border: 1px solid #e5e5e5; border-top-color: transparent; border-radius: 0 0 3px 3px; max-width: 468px; margin: 0 auto; padding: 0 15px 32px;">
                <tr>
                    <td align="center" style="font-size: 26px; font-weight: bold; padding-bottom: 8px;">
                        <b>Ol&aacute; {{$name}}!</b>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size: 14px; line-height: 22px;padding-bottom: 24px;">
                        Recebemos um pedido de redefini&ccedil;&atilde;o de senha para o seu email
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 32px; padding-top: 0; text-align: center">
                        <a href="{{ $url }}" style="margin: 25px auto 0; text-align: center; padding: 10px 15px; display: block; color: #fff; width: 148px; font-family: 'Roboto', Helvetica, Arial, sans-serif; text-decoration: none; text-transform: uppercase; border-radius: 3px; background-color: #1ca7b6;">Redefinir senha</a>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size: 14px; line-height: 22px;padding-bottom: 24px;">
                        Email enviado &agrave;s {{ now()->format('H:i') }}hs do dia {{ now()->format('d/m/Y') }}
                    </td>
                </tr>


                <tr>
                    <td align="center" style="border-top: 1px solid  #e5e5e5; font-size: 14px; line-height: 22px; padding-top: 16px; padding-bottom: 16px;">
                        <p style="margin: 0;">Caso tenha alguma d&uacute;vida entre em contato pelo e-mail <a href="mailto:suporte@souk.com.br" style="color: #601AB8">suporte@souk.com.br</a> ou via <a href="https://wa.me/+5511952336062?" style="color: #601AB8">Whatsapp</a> no n&uacute;mero (11) 95233-6062.</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 100%; text-align: center;">
            <table border="0" cellpadding="0" cellspacing="0" style="max-width: 468px; margin: 0 auto; text-align: center; width: 100%;">
                <tr>
                    <td style="background-color: #eeeeee; text-align: center; font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; padding: 16px; color: #666;">
                        Copyright &copy; 2019 - Todos os direitos reservados.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
​
</html>