@include('mail.layout')
<tr style="box-sizing:border-box;">
    <td class="content-wrap" style="box-sizing:border-box;vertical-align:top;padding:50px 30px;background:#f5f5f5;">
        <table width="100%" cellpadding="0" cellspacing="0" style="box-sizing:border-box;">
            <tr style="box-sizing:border-box;">
                <td class="content-block" style="box-sizing:border-box;vertical-align:top;padding:0 0 20px;">
                    <h2 style="color:#666;box-sizing:border-box;margin-bottom:10px;font-weight:normal;font-size: 20px;">Ola {{$params['name']}}</h2>
                </td>
            </tr>
            <tr style="box-sizing:border-box;">
                <td class="content-block" style="box-sizing:border-box;vertical-align:top;padding:0 0 20px;">
                    <p style="font-size:14px;color: #777777;box-sizing:border-box;margin-bottom:10px;font-weight:normal;line-height: 24px;margin-top: -5px;">
                        Voc&ecirc; est&aacute; recebendo este e-mail porque recebemos um pedido de redefinição de senha para sua conta.
                        Sua nova senha é: <strong> {{$params['new_password']}} </strong>
                        <br /><br />
                        Importante: Desativamos sua senha anterior.
                    </p>
                    <br style="box-sizing:border-box;">

                </td>
            </tr>
            <tr style="box-sizing:border-box;">
                <td class="content-block" style="box-sizing:border-box;vertical-align:top;padding:0 0 20px;">
                    <p>
                        <span style="font-size:14px;color: #777777;">
                            Em caso de qualquer dúvida, fique à vontade para entrar em contato conosco pelo site <a href="https://souk.com.br" title="Souk">https://souk.com.br</a>.
                        </span>
                    </p>

                </td>
            </tr>
            <tr style="box-sizing:border-box;">
                <td class="content-block" style="box-sizing:border-box;vertical-align:top;padding:0 0 20px;">
                    <p style="box-sizing:border-box;margin-bottom:10px;font-weight:normal;color: #2c3d50;">
                    </p>

                    <hr style="box-sizing:border-box;border-top: 1px solid #dddddd;border-color: #dddddd;margin-top:10px;">
                    <span style="font-size:10px;color: #777777;">Data / Hora da solicitação: {{ now()->format("d/m/Y - H:i:s") }}.</span>
                </td>
            </tr>
        </table>
    </td>
</tr>
@include('mail.footer')
