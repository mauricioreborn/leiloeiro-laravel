default: help

deps:
	composer install

help:
	@echo 'Available commands:'
	@echo '  make deps - install dependencies'
	@echo '  make help - show this message'
	@echo '  make init - init local settings'
	@echo '  make test - runs local test suite'

test:
	php artisan migrate:fresh --env=testing
	php artisan db:seed --class=TestDatabaseSeeder --env=testing
	vendor/bin/paratest --phpunit=vendor/bin/phpunit --stop-on-failure --runner=WrapperRunner

init:
	cp .env.template .env

.PHONY: deps help init test
