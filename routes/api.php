<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Route v1 prefix
 */
Route::prefix('v1')->group(function () {
    Route::get('/healthcheck', 'HealthCheckController@index');
    Route::get('/healthcheck/ready', 'HealthCheckController@ready');

    Route::get('/emailopened', 'EmailOpenedController');
});
