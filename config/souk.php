<?php

return [
    'sandbox_clients' => [
        '97747439000114', // André
        '98746233000132', // Cliente Seara Sandbox
        '53816944000106', // Cliente Tirolez Sandbox
    ],
    'terms_of_use_version' => '2',
    'privacy_policy_version' => '2',
];