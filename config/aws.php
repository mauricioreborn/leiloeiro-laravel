<?php

return [
    'sns' => [
        'sms' => [
            'defaultSenderID' => env('SNS_SMS_SENDER', 'SOUK'),
            'defaultSMSType' => env('SNS_SMS_TYPE', 'Transactional'),
        ],
    ],
];