# rocinha

API core da Souk.

"In Rocinha we trust"

## Local environment (for development)

### Setup dependencies

1. Download and install [minikube](https://github.com/kubernetes/minikube) (depends on [VirtualBox](https://www.virtualbox.org/))
1. Download and install [kubectl](https://github.com/kubernetes/kubernetes/releases/v1.10.5)
1. Download and install [helm](https://github.com/kubernetes/helm)
1. Start minikube: `minikube start`
1. Make sure `kubectl` is pointing to minikube: `kubectl cluster-info` (cluster URL must be http://192.168.99.100:8443)
1. Setup shell to use minikube's Docker: `eval $(minikube docker-env)`
1. Enable minikube ingress: `minikube addons enable ingress`
1. Install helm into minikube: `helm init`

### Setup Rocinha

1. Create Rocinha namespace: `kubectl create namespace rocinha`
1. Deploy MySQL: `helm install stable/mysql --name mysql-rocinha --namespace rocinha --wait --set mysqlRootPassword=local --set mysqlDatabase=souk --set persistence.enabled=false`
1. Deploy Redis: `helm install stable/redis --name redis-rocinha --namespace rocinha --wait --set image.repository=redis --set image.tag=5.0.0 --set cluster.enabled=false --set master.persistence.enabled=false`
1. Build Rocinha container: `docker build -t soukbrasil/rocinha:HEAD .`
1. Install Rocinha: `helm upgrade rocinha-head ./deploy --install --namespace rocinha --wait --set ingress.tls=false`
1. Check Rocinha containers are up and running: `kubectl get pod -n rocinha`
1. Test Rocinha is up and running: `curl -v http://head.rocinha.local.souk.com.br/api/v1/healthcheck`
1. Make changes to code, re-run steps 3-4


### Problemas de Pipeline

1. New Relic

- If in the build of the version error pipeline not found for New Relic, check the current version on the link: https://download.newrelic.com/php_agent/release/ and update Dockerfile in line 3  