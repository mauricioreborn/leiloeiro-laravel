<?php

use App\LeadTime\LeadTime;
use App\Products\Product;
use App\Cart\ProductCart;
use Faker\Generator as Faker;

$factory->define(ProductCart::class, function (Faker $faker) {
    return [
        'carrinho_id' => $faker->numberBetween(1, 36),
        'cod_origem' =>function () {
            return factory(LeadTime::class)->create()->code;
        },
        'cod_produto' => function () {
            return factory(Product::class)->create()->code;
        },
        'semanas' => $faker->numberBetween(1, 36),
        'qtd_caixas' => $faker->numberBetween(1, 5),
        'valor_kg' => $faker->randomFloat(2, 1.5, 99.9),
        'total_kg' => $faker->randomFloat(2, 1.5, 99.9),
        'valor' => $faker->randomFloat(2, 1.5, 999.9),
        'status' => 1,
    ];
});
