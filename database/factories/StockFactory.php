<?php

use App\Company\Company;
use App\Core\Entities\Status;
use App\Products\Product;
use App\Stock\Stock;
use Faker\Generator as Faker;

$factory->define(Stock::class, function (Faker $faker) {
    $statusId = Status::firstOrCreate(['name' => Status::CREATED]);
    $companyId = Company::first()->id;
    $fabricatedAt = now()->subDays(15)->toDateTimeString();
    $expiredAt = now()->addDays(7)->toDateTimeString();

    return [
        'file_queue_id' =>  null,
        'status_id' =>  $values['status_id'] ?? $statusId,
        'company_id' => $values['company_id'] ?? $companyId,
        'product_id' => function () {
            return factory(Product::class)->create()->id;
        },
        'origin_code' => $faker->randomNumber(5),
        'volume'  => $faker->randomElement($array = array('2000', '30000', '40000')),
        'batch' => $faker->randomNumber(5),
        'fabricated_at' => $values['fabricated_at'] ?? $fabricatedAt,
        'expired_at' => $values['expired_at'] ?? $expiredAt,
    ];
});

$factory->state(Stock::class, 'expired',function (Faker $faker) {
    return [
        'fabricated_at' => now()->subDays(30)->toDateTimeString(),
        'expired_at' => now()->subDays(7)->toDateTimeString(),
    ];
});

$factory->state(Stock::class, 'withoutVolume',function (Faker $faker) {
    return [
        'volume' => 0
    ];
});
