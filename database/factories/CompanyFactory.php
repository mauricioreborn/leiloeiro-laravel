<?php

use Faker\Generator as Faker;
use \App\Company\Company;

$factory->define(Company::class, function (Faker $faker) {

    $faker = \Faker\Factory::create('pt_BR');
    return [
        'name' => $faker->company,
        'code' => $faker->randomNumber,
        'image' => $faker->imageUrl,
        'cart' => $faker->randomHtml(2,3),
        'bid' => $faker->randomHtml(2,3),
        'rules' => $faker->randomHtml(2,3),
        'aboult' => $faker->randomHtml(2,3),
        'policy' => $faker->randomHtml(2,3),
        'information' => $faker->randomHtml(2,3),
        'product_info' => $faker->randomHtml(2,3),
    ];
});
