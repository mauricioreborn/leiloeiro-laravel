<?php

use Faker\Generator as Faker;
use App\Boost\ClientList;
use App\Company\Company;
use App\Auth\User;

$factory->define(ClientList::class, function (Faker $faker) {

    $fileName = $faker->word.'.csv';

    return [
        'company_id' => Company::first()->id,
        'user_id' => function () {
            return User::first()->id;
        },
        'name' => $faker->word,
        'file_name' => $fileName,
        'file_url' => $faker->url.'/'.$fileName,
        'rows' => $faker->numberBetween(1, 1000),
        'imported_rows' => $faker->numberBetween(1, 1000),
        'invalid_rows' => $faker->numberBetween(1, 1000),
        'duplicate_rows' => $faker->numberBetween(1, 1000),
    ];
});
