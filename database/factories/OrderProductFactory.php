<?php

use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use Faker\Generator as Faker;

use App\Order\Order;
use App\Products\Product;
use App\Order\OrderProduct;
use App\LeadTime\LeadTime;

$factory->define(OrderProduct::class, function (Faker $faker, $values) {
    $kg_total = $values['kg_total'] ?? $faker->randomNumber(4);
    $kg_price = $values['kg_price'] ?? $faker->randomFloat(2, 5, 20);
    $price = $values['price'] ?? ($kg_price * $kg_total);

    return [
        'order_product_code' => $faker->randomNumber(4),
        'order_id' => function () {
            return factory(Order::class)->create()->id;
        },
        'product_id' => function () {
            return factory(Product::class)->create()->code;
        },
        'origin_code' => function () {
            return factory(LeadTime::class)->create()->code;
        },
        'weeks' => $faker->randomDigitNotNull(),
        'box_amount' => $faker->randomNumber(2),
        'original_box_amount' => $faker->randomNumber(2),
        'box_amount' => $faker->randomNumber(2),
        'kg_total' => $kg_total,
        'kg_price' => $kg_price,
        'price' => $price,
        'shipment_date' => $faker->dateTimeInInterval('now', '+ 5 days'),
        'delivery_date' => $faker->dateTimeInInterval('now', '+ 5 days'),
        'email_sent' => $faker->boolean(40),
        'cd_empresa' => $faker->randomNumber(3),
        'cd_faixa_fefo' => $faker->randomNumber(5),
        'ds_chave_seara' => $faker->randomElement(['697|SP|7|27', '697|SP|11|30', '697|SP|11|12']),
        'status_id' => Status::type(Status::PENDING)->id,
        'rejection_motives_id' => null,
    ];
});
