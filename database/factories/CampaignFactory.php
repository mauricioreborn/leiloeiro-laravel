<?php

use Faker\Generator as Faker;

use App\Campaign\Campaign;
use App\Campaign\CampaignClient;
use App\Campaign\CampaignType;
use App\Company\Company;
use App\Auth\User;

$factory->define(Campaign::class, function (Faker $faker) {
    return [
        'company_id' => Company::first()->id,
        'user_id' => User::first()->id,
        'campaign_type_id' => function () {
            return factory(CampaignType::class)->create()->id;
        },
        'estimated_clients' => $faker->numberBetween($min = 1, $max = 3000),
        'scheduled_at' => $faker->dateTimeInInterval('now', '+ 5 days'),
        'processed_at' => $faker->dateTimeInInterval('now', '+ 5 days')
    ];
});
