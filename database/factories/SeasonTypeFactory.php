<?php

use Faker\Generator as Faker;
use App\Company\Company;
use App\Season\SeasonType;
use Carbon\Carbon;

$factory->define(SeasonType::class, function (Faker $faker) {
    $customColors = [
        'card_bg' => 'ffffff',
        'card_text' => '333',
        'price_bg' => 'eeeeee',
        'price_text' => '666',
        'price_highlight' => '1ca7b6'
    ];

    return [
        'name' => 'natalinos',
        'company_id' =>  Company::first()->id,
        'start_season' => Carbon::now()->format('Y-m-d H:i:s'),
        'finish_season' => Carbon::now()->addMonths(2)->format('Y-m-d H:i:s'),
        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        'custom_colors' => $customColors,
    ];
});
