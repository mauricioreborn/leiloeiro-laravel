<?php

use App\Company\Company;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Faker\Generator as Faker;

$factory->define(Fefo::class, function (Faker $faker, $values) {

    $min_price = $values['min_price'] ?? $faker->randomFloat(2, 1, 10);
    $max_price = $values['max_price'] ?? $min_price + 5;
    $selling_price = $values['selling_price'] ?? $min_price + 2;
    $cd_faixa_fefo = $values['cd_faixa_fefo'] ?? $faker->randomElement(['500001', '500002', '500003', '500004']);
    $faixa_fefo = $values['faixa_fefo'] ?? substr($cd_faixa_fefo, -3);
    $company_id = $values['company_id'] ?? Company::first()->id;

    return [
        'company_id' => $company_id,
        'date'  =>  date('Y-m-d'),
        'weeks'  => $faker->numberBetween(1, 12),
        'leadtime_code' => function () use ($company_id) {
            return factory(LeadTime::class)->create([
                'company_id' => $company_id
            ])->code;
        },
        'product_code' => function () use ($company_id) {
            return factory(Product::class)->create([
                'company_id' => $company_id
            ])->code;
        },
        'selling_price' => $selling_price,
        'max_price'     => $max_price,
        'min_price'     => $min_price,
        'changed_admin_price' => $faker->randomFloat(2, 1, 10),
        'highlight_app' => $faker->boolean(),
        'volume'  => $faker->randomElement($array = array('2000', '30000', '40000')),
        'cd_empresa'    => $faker->numberBetween(1, 12),
        'cd_faixa_fefo' => $cd_faixa_fefo,
        'faixa_fefo' => $faixa_fefo,
        'ds_chave_seara'=> $faker->randomElement($array = array('697|SP|7|32', '697|RJ|7|32', '697|SP|8|32')),
        'best_before'   => $faker->date($format = 'Y-m-d', $max = 'now')
    ];
});
