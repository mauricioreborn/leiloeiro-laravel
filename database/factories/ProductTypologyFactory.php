<?php

use App\Company\Company;
use App\ProductTypology\ProductTypology;
use App\Products\Product;
use Faker\Generator as Faker;

$factory->define(ProductTypology::class, function (Faker $faker) {
    return [
        'product_code' => function () {
            return factory(Product::class)->create()->code;
        },
        'typology_code' => $faker->randomNumber(),
        'state' => $faker->stateAbbr,
        'track_1' => 1,
        'track_2' => 1,
        'track_3' => 1,
        'track_4' => 1,
        'company_id' => Company::first()->id
    ];
});
