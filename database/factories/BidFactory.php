<?php

use App\Auth\User;
use App\Bid\Bid;
use App\Client\Client;
use App\Company\Company;
use App\Fefo\Fefo;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Core\Classes\SoukFakerProvider;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Faker\Generator as Faker;
use App\Client\ClientCompanyPayment;
use App\Client\ClientCard;

$factory->define(Bid::class, function (Faker $faker, $values) {
    $faker->addProvider(new SoukFakerProvider($faker));

    $client_id = $values['client_id'] ?? factory(Client::class)->create()->id;

    $box_amount = $values['box_amount'] ?? $faker->numberBetween(1, 5);
    $original_box_amount = $values['original_box_amount'] ?? $box_amount;

    return [
        'client_id' => $client_id,
        'date' => today()->format('Y-m-d'),
        'origin_code' => function () {
            return factory(LeadTime::class)->create()->code;
        },
        'product_code' => function () {
            return factory(Product::class)->create()->code;
        },
        'weeks' => $faker->numberBetween(1, 36),
        'box_amount'=> $box_amount,
        'total_kg'=> $faker->randomFloat(2, 1.5, 99.9),
        'kg_price'=> $faker->randomFloat(2, 1.5, 99.9),
        'price'=> $faker->randomFloat(2, 1.5, 999.9),
        'partial_accept' => 0,
        'duration' => $faker->dateTimeInInterval('now', '+ 5 days')->format('Y-m-d'),
        'original_box_amount' => $original_box_amount,
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'company_id' => Company::first()->id,
        'client_company_payment_id' => null,
        'client_card_id'  => null,
        'cc_months' => 1,
        'tax_percent' => 0,
        'nao_lido' => 1,
        'status_id' => Status::type(Status::PENDING)->id,
        'rejection_motives_id' => null,
        'sanctioned_at' => null
    ];
});

$factory->state(Bid::class, 'approved', function (Faker $faker, $values) {

    $statusApproved = Status::type(Status::APPROVED);
    $client = factory(Client::class)->create();

    if($values['client_id']) {
        $client = Client::find($values['client_id']);
    }

    $leadtime = factory(LeadTime::class)->create([
        'cnpj' => $client->cnpj,
    ]);

    $fefo = factory(Fefo::class)->create([
        'leadtime_code' => $leadtime->code
    ]);

    return [
        'client_id' => function () use ($client) {
            return $client->id;
        },
        'origin_code' => $fefo->leadtime_code,
        'product_code' => $fefo->product_code,
        'status_id' => $statusApproved->id
    ];
})->afterCreatingState(Bid::class, 'approved', function ($bid, $faker) {

    $statusApproved = Status::type(Status::APPROVED);
    $companyId = $bid->company->id;

    $order = factory(Order::class)->create([
        'status_id' => $statusApproved->id,
        'client_id' => $bid->client->id,
        'bid_id' => $bid->id,
        'company_id' => $companyId
    ]);

    $orderProduct = factory(OrderProduct::class)->create([
        'order_id' => $order->id,
        'product_id' => function () use ($companyId) {
            return factory(Product::class)->create([
                'company_id' => $companyId
            ]);
        },
        'status_id' => $statusApproved->id,
    ]);
});


$factory->state(Bid::class, 'canceled', function (Faker $faker, $values) {

    $statusCanceled = Status::type(Status::CANCELED);
    $priceRejected = RejectionMotive::type(RejectionMotive::PRICE_REJECTED);
    $client = factory(Client::class)->create();

    if($values['client_id']) {
        $client = Client::find($values['client_id']);
    }

    $leadtime = factory(LeadTime::class)->create([
        'cnpj' => $client->cnpj,
    ]);

    $fefo = factory(Fefo::class)->create([
        'leadtime_code' => $leadtime->code
    ]);

    return [
        'client_id' => function () use ($client) {
            return $client->id;
        },
        'origin_code' => $fefo->leadtime_code,
        'product_code' => $fefo->product_code,
        'status_id' => $statusCanceled->id,
        'rejection_motives_id' => $priceRejected->id
    ];
});
