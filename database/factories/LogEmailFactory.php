<?php

use App\Company\Company;
use App\Log\LogEmail;
use Faker\Generator as Faker;

$factory->define(LogEmail::class, function (Faker $faker) {
    return [
        'emails'=> $faker->unique()->safeEmail,
        'page'=> $faker->name,
        'subject'=> $faker->name,
        'message'=> $faker->randomHtml(2,3),
        'md5_message'=> $faker->md5(),
        'company_id'=> Company::first()->id
    ];
});
