<?php

use App\Client\ClientCard;
use App\Core\Entities\Status;
use App\Invoice\Invoice;
use App\Order\Order;
use Faker\Generator as Faker;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        'status_id' => function () {
            return factory(Status::class)->create()->id;
        },
        'client_card_id' => function () {
            return factory(ClientCard::class)->create()->id;
        },
        'order_id' => function () {
            return factory(Order::class)->create()->id;
        },
        'type' => $faker->randomElement([Invoice::AUTHORIZATION, Invoice::ORDER]),
        'invoice_id' => $faker->uuid,
        'transaction_number' => $faker->uuid,
        'total' => $faker->randomFloat(2, 100, 1000),
        'tax' => null,
        'commission' => null,
        'advance_fee' => null,
        'paid' => $faker->randomFloat(2, 100, 1000),
        'overpaid' => null,
        'due_date' => now()->addDay()->toDateString(),
        'paid_at' => null,
        'authorized_at' => null,
        'expired_at' => null,
        'refunded_at' => null,
        'chargeback_at' => null,
        'months' => 1,
    ];
});

$factory->state(Invoice::class, 'pending', function (Faker $faker, $values) {
    $total = $values['total'] ?? $faker->randomFloat(2, 100, 1000);

    $status = Status::type(Status::PENDING);

    return [
        'status_id' => $status->id,
        'client_card_id' => null,
        'type' => Invoice::ORDER,
        'total' => $total,
        'paid' => null,
        'authorized_at' => now()->toAtomString(),
    ];
});

$factory->state(Invoice::class, 'paid', function (Faker $faker, $values) {
    $total = $values['total'] ?? $faker->randomFloat(2, 100, 1000);

    $status = Status::type(Status::PAID);

    return [
        'status_id' => $status->id,
        'client_card_id' => null,
        'type' => Invoice::ORDER,
        'total' => $total,
        'paid' => $total,
        'paid_at' => now()->toAtomString(),
        'authorized_at' => now()->toAtomString(),
    ];
});

$factory->state(Invoice::class, 'in_analysis', function (Faker $faker, $values) {
    $total = $values['total'] ?? $faker->randomFloat(2, 100, 1000);

    $status = Status::type(Status::IN_ANALYSIS);

    return [
        'status_id' => $status->id,
        'client_card_id' => null,
        'type' => Invoice::ORDER,
        'total' => $total,
        'paid' => $total,
        'authorized_at' => now()->toAtomString(),
        'paid_at' => now()->toAtomString(),
    ];
});

$factory->state(Invoice::class, 'authorization', function (Faker $faker) {
    $status = Status::type(Status::CREATED);

    return [
        'status_id' => $status->id,
        'type' => Invoice::AUTHORIZATION,
        'total' => 1,
        'order_id' => null
    ];
});

$factory->state(Invoice::class, 'order', function (Faker $faker) {
    return [
        'type' => Invoice::ORDER
    ];
});
