<?php
use App\Core\Classes\SoukFakerProvider;
use App\Core\Entities\RejectionMotive;
use Faker\Generator as Faker;

$factory->define(RejectionMotive::class, function (Faker $faker) {
    $faker->addProvider(new SoukFakerProvider($faker));

    return [
        'name' => $faker->rejectionMotive(),
    ];
});

foreach(RejectionMotive::getMotives() as $motive) {
    $factory->state(RejectionMotive::class, camel_case($motive), function () use ($motive) {
        return [
            'name' => $motive,
        ];
    });
}