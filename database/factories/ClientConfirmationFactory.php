<?php

use Faker\Generator as Faker;

use App\Client\ClientConfirmation;
use App\Client\Client;

$factory->define(ClientConfirmation::class, function (Faker $faker, $values) {

    $faker = \Faker\Factory::create('pt_BR');

    return [
        'client_id' => function () {
            return factory(Client::class)->create();
        },
        'type' => $faker->randomElement(ClientConfirmation::TYPES),
        'code' => $faker->randomNumber(4),
        'value' => $faker->cellphoneNumber(false),
    ];
});

$factory->state(ClientConfirmation::class, 'phone', function () {
    return [
        'type' => ClientConfirmation::TYPES['phone'],
    ];
});

$factory->state(ClientConfirmation::class, 'mail', function (Faker $faker) {
    return [
        'type' => ClientConfirmation::TYPES['mail'],
        'code' => mailConfirmationCode(),
        'value' => $faker->email,
    ];
});