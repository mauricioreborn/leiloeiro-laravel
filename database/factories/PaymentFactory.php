<?php

use Faker\Generator as Faker;

use App\Payment\Payment;

$factory->define(Payment::class, function (Faker $faker, $values) {

  $payment = [
      'credit_card' => [
          'type' => Payment::CREDIT_CARD,
          'name' => 'Cartão de crédito'
      ],
      'billet' => [
          'type' => Payment::BILLET,
          'name' => 'Boleto'
      ],
      'company_credit' => [
          'type' => Payment::COMPANY_CREDIT,
          'name' => 'Saldo na industria'
      ]
  ];

  $paymentMethods = $faker->randomElement([
      $payment['credit_card'],
      $payment['billet'],
      $payment['company_credit']
  ]);

  return [
    'type' => $values['type'] ?? $paymentMethods['type'],
    'name' => $values['name'] ?? $paymentMethods['name'],
  ];
});
