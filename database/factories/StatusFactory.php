<?php
use App\Core\Classes\SoukFakerProvider;
use App\Core\Entities\Status;
use Faker\Generator as Faker;

$factory->define(Status::class, function (Faker $faker) {
    $faker->addProvider(new SoukFakerProvider($faker));

    return [
        'name' => $faker->status(),
    ];
});

foreach(Status::getStatuses() as $status) {
    $factory->state(Status::class, camel_case($status), function () use ($status) {
        return [
            'name' => $status,
        ];
    });
}
