<?php

use App\Client\Seller;
use App\Company\Company;
use Faker\Generator as Faker;

$factory->define(Seller::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'email' => $faker->email(),
        'phone' => $faker->randomElement(['9999-8888', '1234-5678', '98989-8989']),
        'company_id' => Company::first()->id
    ];
});
