<?php

use App\Client\Client;
use App\Company\Company;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Order\Order;
use App\Order\OrderProduct;
use Faker\Generator as Faker;
use App\Client\ClientCompanyPayment;
use App\Client\ClientCard;
use App\Payment\Payment;
use App\Company\CompanyPayment;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'order_code' => $faker->randomDigitNotNull(5),
        'client_id' => function () {
            return factory(Client::class)->create()->id;
        },
        'bid_id' => null,
        'payment_status_id' => null,
        'status_id' => Status::type(Status::PENDING)->id,
        'date' => $faker->dateTimeInInterval('now', '+ 5 days'),
        'partial_accept' => $faker->boolean(),
        'shipment_date' => $faker->dateTimeInInterval('now', '+ 5 days'),
        'delivery_date' => $faker->dateTimeInInterval('now', '+ 5 days'),
        'integrated' => 0,
        'unread' => $faker->boolean(),
        'company_id' => Company::first()->id,
        'client_company_payment_id' => null,
        'client_card_id'  => null,
        'cc_months' => 1,
        'tax_percent' => 0,
        'rejection_motives_id' => function () {
            return factory(RejectionMotive::class)->create()->id;
        },
        'company_id' => Company::first()->id,
        'client_account_balance' => 0
    ];
});

$factory->state(Order::class, 'open', function (Faker $faker, $values) {

    $clientId = $values['client_id'] ?? factory(Client::class)->create();

    return [
        'client_id' => function () use ($clientId) {
            return $clientId;
        },
        'bid_id' => null,
        'client_company_payment_id' => function () {
            return factory(ClientCompanyPayment::class)->state('companyCredit');
        },
        'payment_status_id' => null,
        'status_id' => Status::type(Status::PENDING)->id,
        'integrated' => 0,
        'company_id' => Company::first()->id
    ];
})->afterCreatingState(Order::class, 'open', function ($order, $faker) {

    $kg_total = $faker->randomNumber(4);
    $kg_price = $faker->randomFloat(2, 5, 20);
    $price = $kg_price * $kg_total;

    factory(OrderProduct::class, 3)->create([
        'order_id' => $order->id,
        'order_product_code' => null,
        'shipment_date' => null,
        'delivery_date' => null,
        'kg_total' => $kg_total,
        'kg_price' => $kg_price,
        'price' => $price,
        'status_id' => Status::type(Status::PENDING)->id,
        'email_sent' => 0,
        'rejection_motives_id' => null,
    ]);
});

$factory->state(Order::class, 'canceled', function (Faker $faker, $values) {

    $clientId = $values['client_id'] ?? factory(Client::class)->create();

    $payment = Payment::where('type', Payment::COMPANY_CREDIT)->first();

    $companyPayment = factory(CompanyPayment::class)->create([
        'payment_id' => $payment->id
    ]);

    $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
        'company_payment_id' => $companyPayment->id,
        'client_id' =>  function () use ($clientId) {
            return $clientId;
        }
    ]);

    return [
        'client_id' => function () use ($clientId) {
            return $clientId;
        },
        'bid_id' => null,
        'status_id' => Status::type(Status::CANCELED)->id,
        'payment_status_id' => Status::type(Status::PAID)->id,
        'client_company_payment_id' => $clientCompanyPayment->id,
        'integrated' => 0,
        'company_id' => Company::first()->id,
        'client_account_balance' => 10
    ];
})->afterCreatingState(Order::class, 'canceled', function ($order, $faker) {

    $kg_total = $faker->randomNumber(4);
    $kg_price = $faker->randomFloat(2, 5, 20);
    $price = $kg_price * $kg_total;

    factory(OrderProduct::class, 3)->create([
        'order_id' => $order->id,
        'order_product_code' => null,
        'shipment_date' => null,
        'delivery_date' => null,
        'kg_total' => $kg_total,
        'kg_price' => $kg_price,
        'status_id' => Status::type(Status::CANCELED)->id,
        'price' => $price,
        'email_sent' => 0
    ]);
});
