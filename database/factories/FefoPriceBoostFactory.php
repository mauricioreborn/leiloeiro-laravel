<?php

use App\Company\Company;
use App\Fefo\FefoPriceBoost;
use App\Fefo\FefoPrice;
use App\Client\Client;
use Faker\Generator as Faker;

$factory->define(FefoPriceBoost::class, function (Faker $faker, $values) {
    $client_id =  $values['client_id'] ?? factory(Client::class)->create()->id;
    $fefo_price_id =  $values['fefo_price_id'] ?? factory(FefoPrice::class)->create()->id;
    return [
        'fefo_price_id' => $fefo_price_id,
        'client_id' => $client_id,
        'start_date' => now(),
        'finish_date' => now(),
        'last_view' => null,
    ];
});