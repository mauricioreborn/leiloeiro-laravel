<?php

use App\Client\Client;
use App\Client\ClientTirolez;
use Faker\Generator as Faker;

$factory->define(ClientTirolez::class, function (Faker $faker, $values) {

    $banner_start_at = $values['banner_start_at'] ?? $faker->dateTimeBetween('-1 month', 'now');
    $banner_finish_at = $values['banner_finish_at'] ??
        $faker->dateTimeInInterval($banner_start_at, '+2 years');

    return [
        'client_id' => function () {
            return factory(Client::class)->create();
        },
        'type' => $faker->randomElement(ClientTirolez::TYPE),
        'modal_view_counter' => $faker->randomDigitNotNull(),
        'last_updated_at' => $faker->dateTimeBetween('now', '+5 years'),
        'banner_start_at' => $banner_start_at,
        'banner_finish_at' => $banner_finish_at,
    ];
});
