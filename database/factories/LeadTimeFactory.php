<?php

use App\Client\Client;
use App\Company\Company;
use App\LeadTime\LeadTime;
use Faker\Generator as Faker;

$factory->define(LeadTime::class, function (Faker $faker) {
    return [
        'code' => $faker->randomNumber(5),
        'name' => $faker->firstName,
        'cnpj' => function () {
            return factory(Client::class)->create()->cnpj;
        },
        'days' => $faker->randomNumber(1),
        'prediction' => 7,
        'limit_hour' => $faker->randomElement($array = array('2030', '2220', '0140')),
        'monday' => $faker->boolean(),
        'tuesday' => $faker->boolean(),
        'wednesday' => $faker->boolean(),
        'thursday' => $faker->boolean(),
        'friday' => $faker->boolean(),
        'saturday' => $faker->boolean(),
        'sunday' => $faker->boolean(),
        'operates_saturday' => $faker->boolean(),
        'operates_sunday' => $faker->boolean(),
        'company_id' => Company::first()->id
    ];
});

$factory->state(LeadTime::class, 'canPurchase',function (Faker $faker) {
    return [
        'name' => 'SAO PAULO - SP',
        'days' => 1,
        'prediction' => 1,
        'limit_hour' => 2300,
        'monday' => 1,
        'tuesday' => 1,
        'wednesday' => 1,
        'thursday' => 1,
        'friday' => 1,
        'saturday' => 1,
        'sunday' => 1,
        'operates_saturday' => 1,
        'operates_sunday' => 1,
    ];
});

$factory->state(LeadTime::class, 'cannotPurchase',function (Faker $faker) {
    return [
        'monday' => 0,
        'tuesday' => 0,
        'wednesday' => 0,
        'thursday' => 0,
        'friday' => 0,
        'saturday' => 0,
        'sunday' => 0,
        'operates_saturday' => 0,
        'operates_sunday' => 0,
    ];
});
