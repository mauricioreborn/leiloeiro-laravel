<?php

use App\Company\Company;
use App\Core\Entities\Status;
use Faker\Generator as Faker;
use \App\Client\Client;

$factory->define(Client::class, function (Faker $faker) {

    $faker = \Faker\Factory::create('pt_BR');
    return [
        'cnpj' => $faker->cnpj,
        'state_registration' => $faker->bothify('########'),
        'name' => $faker->firstName,
        'password' => $faker->randomElement(['12345']),
        'email' => $faker->email(),
        'email_verified' => (int) '1',
        'phone' => $faker->randomElement(['1199998888', '1212345678', '13989898989']),
    ];
});

$factory->state(Client::class, 'isStepTwo', function (Faker $faker, $values) {

    $statusPending = Status::type(Status::PENDING);

    return [
        'confirm_sms' => $statusPending->id,
    ];

});

$factory->state(Client::class, 'isStepThree', function (Faker $faker, $values) {

    $statusAuthorized = Status::type(Status::AUTHORIZED);

    return [
        'confirm_sms' => $statusAuthorized->id,
    ];

});

$factory->state(Client::class, 'isStepFour', function (Faker $faker, $values) {

    $statusAuthorized = Status::type(Status::AUTHORIZED);
    $statusPending = Status::type(Status::PENDING);

    return [
        'confirm_sms' => $statusAuthorized->id,
        'confirm_email' => $statusPending->id,
    ];

});

$factory->state(Client::class, 'isConfirmed', function (Faker $faker, $values) {

    $statusAuthorized = Status::type(Status::AUTHORIZED);

    return [
        'confirm_sms' => $statusAuthorized->id,
        'confirm_email' => $statusAuthorized->id,
    ];

});