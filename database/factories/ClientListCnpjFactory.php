<?php

use Faker\Generator as Faker;
use App\Boost\ClientList;
use App\Boost\ClientListCnpj;

$factory->define(ClientListCnpj::class, function (Faker $faker) {

    $faker = \Faker\Factory::create('pt_BR');

    return [
        'client_list_id' => ClientList::first()->id,
        'cnpj' => $faker->cnpj,
    ];
});
