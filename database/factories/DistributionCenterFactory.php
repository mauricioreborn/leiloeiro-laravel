<?php

use App\Company\Company;
use App\DistributionCenter\DistributionCenter;
use Faker\Generator as Faker;

$factory->define(DistributionCenter::class, function (Faker $faker) {
    $companyId = Company::first()->id;

    return [
        'company_id' => $values['company_id'] ?? $companyId,
        'code' => $faker->randomNumber(5),
        'name' => $faker->state,
        'state' => $faker->stateAbbr,
    ];
});