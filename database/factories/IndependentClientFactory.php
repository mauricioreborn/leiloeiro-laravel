<?php
use App\Client\IndependentClient;
use Faker\Generator as Faker;

$factory->define(IndependentClient::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('pt_BR');

    return [
        'cnpj' => $faker->cnpj,
        'name' => $faker->firstName,
        'password' => $faker->randomElement(['12345']),
        'email' => $faker->email(),
        'phone' => $faker->phoneNumber,
        'rede' => $faker->firstName,
        'status' => 1,
        'saldo' => 100,
        'endereco' => $faker->address,
        'uf' => $faker->randomElement(['SP', 'MG', 'RJ']),
        'municipio' => $faker->city,
        'integrado' => 0,
        'confirm_sms' => $faker->randomElement([null, 3, 10]),
        'zipcode' => '04438-250',
        'address_number' => $faker->numberBetween(0, 100),
        'neighborhood' => $faker->streetName,
        'address_complement' => 'souk house',
        'social_reason' => 'independent souk',
        'contact_name' => $faker->name,
    ];
});

$factory->state(IndependentClient::class, 'newRegistration', function () {
    return [
        'name' => null,
        'email' => null,
        'phone' => null,
        'endereco' => null,
        'uf' => null,
        'municipio' => null,
        'telefone' => null,
        'zipcode' => null,
        'address_number' => null,
        'neighborhood' => null,
        'address_complement' => null,
        'confirm_sms' => null,
    ];
});

$factory->state(IndependentClient::class, 'phoneValidated', function (Faker $faker) {
    return [
        'name' => null,
        'email' => null,
        'endereco' => null,
        'uf' => null,
        'municipio' => null,
        'zipcode' => null,
        'address_number' => null,
        'neighborhood' => null,
        'address_complement' => null,
        'confirm_sms' => 10,
    ];
});

$factory->state(IndependentClient::class, 'withoutContactInformation', function () {
    return [
        'email' => null,
        'phone' => null,
        'confirm_sms' => 10,
    ];
});
