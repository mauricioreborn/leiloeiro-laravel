<?php


use App\Cart\Cart;
use App\Company\Company;
use Faker\Generator as Faker;

$factory->define(Cart::class, function (Faker $faker) {
    return [
        'id_cliente' => $faker->numberBetween(1, 36),
        'data'  => date('Y-m-d'),
        'status'   => 1,
        'created_at' => date('Y-m-d'),
        'company_id' => Company::first()->id
    ];
});