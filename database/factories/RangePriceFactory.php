<?php

use App\Company\Company;
use App\LeadTime\LeadTime;
use App\Products\Product;
use App\RangePrice\RangePrice;
use Faker\Generator as Faker;

$factory->define(RangePrice::class, function (Faker $faker) {
    return [
        'product_code' => factory(Product::class)->create()->code,
        'origin_code' => factory(LeadTime::class)->create()->code,
        'track' => $faker->randomElement([1,2,3,4]),
        'approval_percentage' => $faker->randomElement([3,5,10,4]),
        'reprove_percentage' => $faker->randomElement([11,40,20,50]),
        'company_id' => Company::first()->id
    ];
});
