<?php

use App\Company\CompanyDivision;
use Faker\Generator as Faker;

$factory->define(CompanyDivision::class, function (Faker $faker) {
    return [
        'division' => $faker->randomNumber,
        'company_id' => Company::first()->id,
    ];
});