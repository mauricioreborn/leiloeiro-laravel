<?php
use App\Core\Classes\SoukFakerProvider;
use Faker\Generator as Faker;
use App\Boost\Boost;
use App\Company\Company;
use \App\Products\Product;
use \App\Core\Entities\Status;

$factory->define(Boost::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('pt_BR');
    $faker->addProvider(new SoukFakerProvider($faker));

    $company_id = $values['company_id'] ?? Company::first()->id;
    $allowedStatuses = [Status::CANCELED, Status::DRAFT, Status::IN_PROGRESS, Status::FINISHED, Status::SCHEDULED];

    return [
        'company_id' => $company_id,
        'name' => $faker->name(),
        'whatsapp' => $faker->boolean(),
        'push' => $faker->boolean(),
        'email' => $faker->boolean(),
        'sms' => $faker->boolean(),
        'title' => $faker->title(),
        'description' => $faker->text(),
        'product_code' => function () use ($company_id) {
            return factory(Product::class)->create([
                'company_id' => $company_id
            ])->code;
        },
        'weeks' => $faker->numberBetween(1, 20),
        'coverage' => $faker->numberBetween(1, 1000),
        'reach' => $faker->numberBetween(1, 1000),
        'conversion' => $faker->numberBetween(1, 1000),
        'disqualified' => $faker->numberBetween(1, 1000),
        'ruled_out' => $faker->numberBetween(1, 1000),
        'revenue' => $faker->randomFloat(2, 1, 100),

        'status_id' => Status::type($faker->status($allowedStatuses))->id,
        'applied_filters' => null,
        'started_at' => $faker->dateTimeInInterval('now', '+ 1 days'),
        'finished_at' => $faker->dateTimeInInterval('now', '+ 7 days'),
    ];
});