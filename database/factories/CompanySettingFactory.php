<?php

use App\Company\Company;
use App\Company\CompanySetting;
use Faker\Generator as Faker;

$factory->define(CompanySetting::class, function (Faker $faker) {
    return [
        'company_id' => Company::first()->id,
        'setting' => $faker->word,
        'value' => $faker->word,
    ];
});

$factory->state(CompanySetting::class, 'hasStockReplicate', function (Faker $faker, $values) {
    return [
        'setting' => 'hasStockReplicate',
        'value' => 1,
    ];
});