<?php

use App\Company\Company;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Auth\User::class, function (Faker $faker) {
    $company = Company::first();
    $companyId = $company !== null ? $company->id : 1;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => md5('somerandomstring'),
        'root' => 0,
        'price' => 0,
        'featured' => 0,
        'company_id' => $companyId,
    ];
});
