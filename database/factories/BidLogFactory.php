<?php

use Faker\Generator as Faker;
use App\Bid\BidLog;
use App\Auth\User;
use App\Client\Client;
use App\Bid\Bid;

$factory->define(BidLog::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'client_id' => function () {
            return factory(Client::class)->create()->id;
        },
        'bid_id' => function () {
            return factory(Bid::class)->create()->id;
        },
        'box_amount' => 10 ,
        'created_at' => date('Y-m-d'),
    ];
});