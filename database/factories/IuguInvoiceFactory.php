<?php

use App\Core\Entities\Status;
use App\Invoice\Invoice;
use App\Iugu\Services\IuguService;
use Faker\Generator as Faker;
use Tests\Mockup\IuguInvoice;

$factory->define(IuguInvoice::class, function (Faker $faker, $values) {
    $totalCents = $values['total_cents'] ?? $faker->numberBetween(10000, 50000);
    $totalPaidCents = $values['total_paid_cents'] ?? $totalCents;

    $id = $values['id'] ?? $faker->uuid;

    return [
        'id' => $id,
        'due_date' => today()->toDateString(),
        'currency' => 'BRL',
        'discount_cents' => null,
        'email' => $faker->email,
        'items_total_cents' => $totalCents,
        'notification_url' => null,
        'return_url' => null,
        'status' => 'paid',
        'tax_cents' => null,
        'total_cents' => $totalCents,
        'total_paid_cents' => $totalPaidCents,
        'taxes_paid_cents' => null,
        'paid_cents' => $totalPaidCents,
        'cc_emails' => null,
        'financial_return_date' => null,
        'payable_with' => 'credit_card',
        'overpaid_cents' => null,
        'ignore_due_email' => true,
        'ignore_canceled_email' => true,
        'advance_fee_cents' => null,
        'commission_cents' => 0,
        'early_payment_discount' => false,
        'order_id' => function () {
            return factory(Invoice::class)->create()->id;
        },
        'secure_id' => $id,
        'secure_url' => 'https://faturas.iugu.com/'.$id,
        'customer_id' => $faker->uuid,
        'customer_ref' => 'Client ref',
        'customer_name' => 'Cliente name',
        'user_id' => null,
        'total' => formata_moeda($totalCents/100),
        'taxes_paid' => 'R$ 0,00',
        'total_paid' => formata_moeda($totalPaidCents/100),
        'total_overpaid' => 'R$ 0,00',
        'commission' => 'R$ 0,00',
        'fines_on_occurrence_day' => 'R$ 0,00',
        'total_on_occurrence_day' => formata_moeda($totalCents/100),
        'fines_on_occurrence_day_cents' => 0,
        'total_on_occurrence_day_cents' => $totalCents,
        'advance_fee' => null,
        'paid' => formata_moeda($totalPaidCents/100),
        'original_payment_id' => null,
        'double_payment_id' => null,
        'interest' => null,
        'discount' => null,
        'created_at' => now()->format('d/m H:i \h'),
        'created_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
        'updated_at' => now()->format(DateTime::RFC3339_EXTENDED),
        'authorized_at' => null,
        'authorized_at_iso' => null,
        'paid_at' => null,
        'expired_at' => null,
        'expired_at_iso' => null,
        'refunded_at' => null,
        'refunded_at_iso' => null,
        'canceled_at' => null,
        'canceled_at_iso' => null,
        'protested_at' => null,
        'protested_at_iso' => null,
        'chargeback_at' => null,
        'chargeback_at_iso' => null,
        'occurrence_date' => today()->toDateString(),
        'refundable' => true,
        'installments' => '12',
        'transaction_number' => 1111,
        'payment_method' => 'iugu_credit_card_test',
        'financial_return_dates' => null,
        'bank_slip' => null,
        'items' => [
            [
                'id' => 'D431A0F7049F4FA7A0217DA4CA65702A',
                'description' => 'Item 1',
                'price_cents' => $totalCents,
                'quantity' => 1,
                'created_at' => now()->format(DateTime::RFC3339_EXTENDED),
                'updated_at' => now()->format(DateTime::RFC3339_EXTENDED),
                'price' => formata_moeda($totalCents/100),
            ],
        ],
        'early_payment_discounts' => [],
        'variables' => [
            [
                'id' => 'F176D8E77E9A414DA035F0244288DD0D',
                'variable' => 'customer_payment_method_id',
                'value' => '2C17CBBF0BB047C7815A638C756185BF',
            ],
            [
                'id' => '83CD66222CF64E61B8A647DBA6B60F30',
                'variable' => 'payer.cpf_cnpj',
                'value' => '97747439000114',
            ],
            [
                'id' => '3E45A6D8EB394685A79923BED3A3E42E',
                'variable' => 'payer.name',
                'value' => '205669 Cliente',
            ],
            [
                'id' => 'C646BB9852754C8C85175FE8A4A4C65F',
                'variable' => 'payment_data.arp',
                'value' => '00000',
            ],
            [
                'id' => '82E1F8B8C81145CE9C7E854562711660',
                'variable' => 'payment_data.installments',
                'value' => '1',
            ],
            [
                'id' => '80E24DC538B241FB8ADF559911F531ED',
                'variable' => 'payment_data.nsu',
                'value' => '00000',
            ],
            [
                'id' => 'DF61A769EFE140688434FF04CD9B0566',
                'variable' => 'payment_data.transaction_id',
                'value' => '00000000000000000001',
            ],
            [
                'id' => 'A26A840787844F6A8DF7BA56AA11A02F',
                'variable' => 'payment_data.transaction_number',
                'value' => '1111',
            ],
            [
                'id' => 'EBEF05997C114B39BF71819691368BD4',
                'variable' => 'payment_method',
                'value' => 'iugu_credit_card_test',
            ],
        ],
        'custom_variables' => [],
        'logs' => []
    ];
});

$factory->state(IuguInvoice::class, Status::CANCELED, function (Faker $faker, $values) {
    return [
        'status' => Status::CANCELED,
        'total_paid_cents' => 0,
        'canceled_at' => now()->format('d/m H:i \h'),
        'canceled_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});

$factory->state(IuguInvoice::class, Status::PAID, function (Faker $faker, $values) {
    return [
        'status' => Status::PAID,
        'authorized_at' => now()->format('d/m H:i \h'),
        'authorized_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
        'paid_at' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});

$factory->state(IuguInvoice::class, Status::PENDING, function (Faker $faker, $values) {
    return [
        'status' => Status::PENDING,
        'total_paid_cents' => 0,
        'authorized_at' => null,
        'authorized_at_iso' => null,
    ];
});

$factory->state(IuguInvoice::class, Status::IN_ANALYSIS, function (Faker $faker, $values) {
    return [
        'status' => Status::IN_ANALYSIS,
        'authorized_at' => now()->format('d/m H:i \h'),
        'authorized_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});

$factory->state(IuguInvoice::class, Status::PARTIALLY_PAID, function (Faker $faker, $values) {
    $totalCents = $values['total_cents'] ?? $faker->numberBetween(10000, 50000);

    return [
        'status' => Status::PARTIALLY_PAID,
        'total_cents' => $totalCents,
        'total_paid_cents' => $totalCents / 2,
        'authorized_at' => now()->format('d/m H:i \h'),
        'authorized_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
        'paid_at' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});

$factory->state(IuguInvoice::class, Status::EXPIRED, function (Faker $faker, $values) {
    return [
        'status' => Status::EXPIRED,
        'total_paid_cents' => 0,
        'expired_at' => now()->format('d/m H:i \h'),
        'expired_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});

$factory->state(IuguInvoice::class, Status::AUTHORIZED, function (Faker $faker, $values) {
    return [
        'status' => Status::AUTHORIZED,
        'authorized_at' => now()->format('d/m H:i \h'),
        'authorized_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});

$factory->state(IuguInvoice::class, Status::REFUNDED, function (Faker $faker, $values) {
    return [
        'status' => Status::REFUNDED,
        'total_paid_cents' => 0,
        'refunded_at' => now()->format('d/m H:i \h'),
        'refunded_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});

$factory->state(IuguInvoice::class, Status::IN_PROTEST, function (Faker $faker, $values) {
    return [
        'status' => Status::IN_PROTEST,
        'protested_at' => now()->format('d/m H:i \h'),
        'protested_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});

$factory->state(IuguInvoice::class, Status::CHARGEBACK, function (Faker $faker, $values) {
    return [
        'status' => Status::CHARGEBACK,
        'chargeback_at' => now()->format('d/m H:i \h'),
        'chargeback_at_iso' => now()->format(DateTime::RFC3339_EXTENDED),
    ];
});