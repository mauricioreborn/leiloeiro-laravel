<?php

use Faker\Generator as Faker;

use App\Client\ClientCompanyPayment;
use App\Client\Client;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Payment\Payment;

$factory->define(ClientCompanyPayment::class, function (Faker $faker) {
    return [
        'client_id' => function () {
            return factory(Client::class)->create();
        },
        'company_payment_id' => function () {
            return factory(CompanyPayment::class)->create();
        },
    ];
});

$factory->state(ClientCompanyPayment::class, 'companyCredit', function (Faker $faker, $values) {

    $clientId = $values['client_id'] ?? factory(Client::class)->create();
    $companyId = $values['company_id'] ?? factory(Company::class)->create();

    $payment = factory(Payment::class)->create([
        'type' => Payment::COMPANY_CREDIT
    ]);

    return [
        'client_id' => function () use ($clientId) {
            return $clientId;
        },
        'company_payment_id' => function () use ($payment, $companyId) {
            return factory(CompanyPayment::class)->create([
                'payment_id' => $payment->id,
                'company_id' => function () use ($companyId) {
                    return $companyId;
                }
            ]);
        }
    ];
});

$factory->state(ClientCompanyPayment::class, 'creditCard', function (Faker $faker, $values) {

    $clientId = $values['client_id'] ?? factory(Client::class)->create();

    $payment = factory(Payment::class)->create([
        'type' => Payment::CREDIT_CARD
    ]);

    return [
        'client_id' => function () use ($clientId) {
            return $clientId;
        },
        'company_payment_id' => function () use ($payment) {
            return factory(CompanyPayment::class)->create([
                'payment_id' => $payment->id,
                'company_id' => function () {
                    return factory(Company::class)->create();
                }
            ]);
        }
    ];
});
