<?php

use Faker\Generator as Faker;
use App\Season\SeasonProduct;
use Carbon\Carbon;
use App\Products\Product;
use App\Season\SeasonType;

$factory->define(SeasonProduct::class, function (Faker $faker) {
    return [
        'product_code' => factory(Product::class)->create()->code,
        'season_id' => function () {
            return factory(SeasonType::class)->create()->id;
        },
        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
    ];
});
