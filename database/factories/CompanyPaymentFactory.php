<?php

use Faker\Generator as Faker;

use App\Company\CompanyPayment;
use App\Company\Company;
use App\Payment\Payment;

$factory->define(CompanyPayment::class, function (Faker $faker, $values) {

    $companyId = $values['company_id'] ?? Company::first();

    return [
        'payment_id' => function () {
            return factory(Payment::class)->create();
        },
        'company_id' => $companyId
    ];
});

$factory->state(CompanyPayment::class, 'companyCredit', function (Faker $faker, $values) {

    $payment = Payment::firstOrCreate([
        'type' => Payment::COMPANY_CREDIT
    ]);

    $companyId = $values['company_id'] ?? Company::first();

    return [
        'payment_id' => $payment->id,
        'company_id' => $companyId
    ];

});

$factory->state(CompanyPayment::class, 'creditCard', function (Faker $faker, $values) {

    $payment = Payment::firstOrCreate([
        'type' => Payment::CREDIT_CARD
    ]);

    $companyId = $values['company_id'] ?? Company::first();

    return [
        'payment_id' => $payment->id,
        'company_id' => $companyId
    ];

});
