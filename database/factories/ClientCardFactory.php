<?php

use App\Client\Client;
use App\Client\ClientCard;
use App\Core\Entities\Status;
use Faker\Generator as Faker;

$factory->define(ClientCard::class, function (Faker $faker) {
    return [
        'status_id' => function () {
            return factory(Status::class)->create()->id;
        },
        'payment_method_id' => $faker->uuid(),
        'client_id' => function () {
            return factory(Client::class)->create();
        },
        'description' => $faker->randomElement([
            'Cartão de crédito Visa',
            'Cartão de crédito master',
            'Cartão de crédito amex',
        ]),
        'holder_name' => $faker->name(),
        'display_number' => $faker->randomElement([
            'XXXX-XXXX-XXXX-111',
            'XXXX-XXXX-XXXX-112',
            'XXXX-XXXX-XXXX-113',
        ]),
        'brand' => $faker->randomElement([
            'VISA',
            'MASTERCARD',
            'American Express'
        ]),
        'bin' => $faker->randomNumber(),
        'month' => $faker->numberBetween(1, 12),
        'year' => $faker->dateTimeBetween('now', '+5 years')->format('Y'),
    ];
});

$factory->state(ClientCard::class, 'pending',function (Faker $faker) {
    $status = Status::type(Status::PENDING);

    return [
        'status_id' => $status->id
    ];
});

$factory->state(ClientCard::class, 'processing',function (Faker $faker) {
    $status = Status::type(Status::PROCESSING);

    return [
        'status_id' => $status->id
    ];
});

$factory->state(ClientCard::class, 'authorized',function (Faker $faker) {
    $status = Status::type(Status::AUTHORIZED);

    return [
        'status_id' => $status->id
    ];
});

$factory->state(ClientCard::class, 'rejected',function (Faker $faker) {
    $status = Status::type(Status::REJECTED);

    return [
        'status_id' => $status->id
    ];
});
