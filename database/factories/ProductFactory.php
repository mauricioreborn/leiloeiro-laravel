<?php

use App\Products\Product;
use Faker\Generator as Faker;
use App\Company\Company;

$faker = \Faker\Factory::create();

$factory->define(Product::class, function (Faker $faker) {

    return [
        'name' => $faker->randomElement($array = array('chicken', 'meat', 'sausage')),
        'unit' => $faker->randomElement(['KG', 'L']),
        'code' => $faker->randomNumber(5),
        'category' => $faker->randomElement($array = array('chicken', 'meat', 'sausage')),
        'type' => $faker->randomElement($array = array('premium', 'basic', 'light')),
        'brand' => $faker->randomElement(['A', 'B']),
        'subbrand' => $faker->randomElement($array = array('chicken', 'meat', 'sausage')),
        'weight' => $faker->randomFloat(2, 1.5, 99.9),
        'weight_piece' => $faker->randomFloat(2, 1.5, 99.9),
        'pieces' => $faker->randomNumber(2),
        'picture' => $faker->randomElement($array = array('meat.jpg', 'chicken.jpg', 'frog.jpg')),
        'app_exhibition' => $faker->randomElement($array = array('20x40', '20x30', '10x30')),
        'min_billing' => null,
        'avg_price' => null,
        'price_increase' => null,
        'behaviour_approval' => $faker->boolean,
        'company_id' => Company::first()->id,
    ];
});
