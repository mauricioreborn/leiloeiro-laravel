<?php

use Faker\Generator as Faker;

use App\Campaign\CampaignType;

$factory->define(CampaignType::class, function (Faker $faker) {

    $campaignTypes = [
        'repurchase' => [
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ],
        'firstPurchase' => [
            'uid' => 'firstPurchase',
            'name' => 'Primeira Compra'
        ],
        'highlightFefo' => [
            'uid' => 'highlightFefo',
            'name' => 'Destaque'
        ]
    ];

    $campaignType = $faker->randomElements([
        $campaignTypes['repurchase'],
        $campaignTypes['firstPurchase'],
        $campaignTypes['highlightFefo']
    ]);

    return [
        'uid' => $campaignType[0]['uid'],
        'name' => $campaignType[0]['name'],
        'recurrence_days' => $faker->numberBetween($min = 1, $max = 7)
    ];
});
