<?php

use Faker\Generator as Faker;

use App\NotificationOrigin\NotificationOrigin;

use App\LeadTime\LeadTime;
use App\Company\Company;

$factory->define(NotificationOrigin::class, function (Faker $faker) {
    return [
        'origin_code' => function () {
            return factory(LeadTime::class)->create()->code;
        },
        'subject' => $faker->sentence(),
        'message' => $faker->text(172),
        'processed_at' => now()->format('Y-m-d H:i:s'),
        'total_sent' => $faker->randomDigitNotNull(),
        'day_period' => $faker->randomElement(['morning', 'afternoon', 'night']),
        'scheduled_at' => now()->format('Y-m-d'),
        'created_at' => now()->format('Y-m-d H:i:s'),
        'updated_at' => now()->format('Y-m-d H:i:s'),
        'company_id' => Company::first()->id
    ];
});
