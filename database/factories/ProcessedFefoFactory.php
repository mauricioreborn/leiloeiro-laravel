<?php

use App\ProcessedFefo\ProcessedFefo;
use Faker\Generator as Faker;
use App\Fefo\Fefo;

$factory->define(ProcessedFefo::class, function (Faker $faker, $values) {
    return [
        'fefo_id' => function () {
            return factory(Fefo::class)->create()->id;
        },
        'scheduled_at' => now()
    ];
});
