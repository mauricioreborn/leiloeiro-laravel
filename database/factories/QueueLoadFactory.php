<?php

use Faker\Generator as Faker;

$factory->define(App\QueueLoad\QueueLoad::class, function (Faker $faker){
    return [
        'file_name' => $faker->firstName,
        'file_url'=>$faker->url,
        'total_success'=>$faker->randomNumber(1),
        'total_error'=>$faker->randomNumber(1),
        'total_duplicates'=>$faker->randomNumber(1),
        'status'=>$faker->randomElement($array = array (0,2,'done')),
        'file_size'=>$faker->randomNumber(1),
        'file_type'=>$faker->randomElement($array = array (0,2,4)),
        'started_at'=>$faker->date(),
        'finished_at'=>$faker->date(),
    ];
});
