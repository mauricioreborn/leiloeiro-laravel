<?php

use App\Payment\PaymentReport;
use App\Order\Order;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(PaymentReport::class, function (Faker $faker) {

    return [
        'order_id' => factory(Order::class)->create()->id,
        'total' => 200,
        'release_at' => Carbon::now()
    ];
});