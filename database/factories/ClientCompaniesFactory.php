<?php

use App\Company\Company;
use Faker\Generator as Faker;
use \App\Client\Client;
use \App\ClientCompanies\ClientCompany;

$factory->define(ClientCompany::class, function (Faker $faker) {

    $faker = \Faker\Factory::create('pt_BR');
    return [
        'account_balance' => $faker->randomFloat(2, 1, 10),
        'client_id' => function () {
            return factory(Client::class)->create()->id;
        },
        'client_code' => $faker->randomNumber(),
        'company_id' => Company::first()->id,
        'min_order' => $faker->randomFloat(2, 1, 10),
        'address' => $faker->address(),
        'zip_code' => $faker->postcode(),
        'state' => $faker->stateAbbr(),
        'city' => $faker->city(),
        'typology' => $faker->randomElement(['AS 5 - 9 CK', 'AS 5 - 10 CK', 'ARMAZENS E MERCEARIAS']),
        'typology_code' => $faker->randomNumber(),
        'track_1' => 1,
        'track_2' => 1,
        'track_3' => 1,
        'track_4' => 1,
        'status' => 1,
        'channel' => $faker->randomElement(['ARMAZENS E MERCEARIAS', 'PADARIAS', 'PEIXARIAS']),
    ];
});
