<?php

use App\Products\Product;
use App\Products\ProductPrice;
use Faker\Generator as Faker;

$factory->define(ProductPrice::class, function (Faker $faker) {
    return [
        'product_id' => function () {
            return factory(Product::class)->create()->id;
        },
        'state' => $faker->randomElement($array = array('PA', 'BA', 'CE')),
        'price' => $faker->randomFloat(2, 10, 100),
    ];
});
