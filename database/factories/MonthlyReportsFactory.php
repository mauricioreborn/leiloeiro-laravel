<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use App\Payment\MonthlyReport;
use App\Company\Company;

$factory->define(MonthlyReport::class, function (Faker $faker) {

    return [
        'total' => $faker->randomElement([3900, 4500]),
        'month' => $faker->month($max = 'now'),
        'year' => $faker->year($max = 'now'),
        'company_id' => Company::first()->id,
        'status_id' => 1,
        'url' => $faker->url()
    ];
});