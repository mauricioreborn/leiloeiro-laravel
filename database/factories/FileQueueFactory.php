<?php

use App\Auth\User;
use App\Company\Company;
use App\Core\Entities\Status;
use App\FileQueue\FileQueue;
use Faker\Generator as Faker;

$factory->define(FileQueue::class, function (Faker $faker) {
    $statusId = Status::firstOrCreate(['name' => Status::CREATED]);
    $type = FileQueue::TYPES[array_rand(FileQueue::TYPES)];
    $companyId = Company::first()->id;
    $path = $faker->randomNumber(5) .'.csv';
    $originalName = $faker->randomNumber(5) .'.csv';

    return [
        'status_id' =>  $values['status_id'] ?? $statusId,
        'company_id' => $values['company_id'] ?? $companyId,
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'path' => $values['path'] ?? $path,
        'type' => $values['type'] ?? $type,
        'original_name' => $values['original_name'] ?? $originalName,
    ];
});

$factory->state(FileQueue::class, 'client',function (Faker $faker) {
    return [
        'type' => FileQueue::CLIENT
    ];
});

$factory->state(FileQueue::class, 'leadtime',function (Faker $faker) {
    return [
        'type' => FileQueue::LEADTIME
    ];
});

$factory->state(FileQueue::class, 'product',function (Faker $faker) {
    return [
        'type' => FileQueue::PRODUCT
    ];
});

$factory->state(FileQueue::class, 'stock',function (Faker $faker) {
    return [
        'type' => FileQueue::STOCK
    ];
});
