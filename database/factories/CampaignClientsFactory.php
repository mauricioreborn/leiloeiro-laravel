<?php

use Faker\Generator as Faker;

use App\Campaign\Campaign;
use App\Campaign\CampaignClient;
use App\Campaign\CampaignType;
use App\Company\Company;
use App\Auth\User;

$factory->define(CampaignClient::class, function (Faker $faker) {
    return [
        'campaign_id' => function () {
            return factory(Company::class)->create()->id;
        },
        'client_id' => function () {
            return factory(User::class)->create()->id;
        },
        'notification_id' => '',
        'date' => $faker->dateTimeInInterval('now', '+ 5 days'),
        'send_at' => $faker->dateTimeInInterval('now', '+ 5 days')
    ];
});
