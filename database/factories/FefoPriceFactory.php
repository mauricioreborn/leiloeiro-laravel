<?php

use App\Company\Company;
use App\Fefo\FefoPrice;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Faker\Generator as Faker;

$factory->define(FefoPrice::class, function (Faker $faker, $values) {
    $company_id = $values['company_id'] ?? Company::first()->id;

    return [
        'company_id' => $company_id,
        'weeks' => $faker->numberBetween(1, 12),
        'leadtime_code' => function () use ($company_id) {
            return factory(LeadTime::class)->create([
                'company_id' => $company_id
            ])->code;
        },
        'product_code' => function () use ($company_id) {
            return factory(Product::class)->create([
                'company_id' => $company_id
            ])->code;
        },
        'selling_price' => $faker->randomFloat(2, 1, 10),
        'expired_at' => $faker->date(),
    ];
});