<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFefoTableRecreateIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            DB::statement("ALTER TABLE fefo DROP INDEX codigo_produto_2");

            Schema::table('fefo', function (Blueprint $table) {
                $table->dropIndex('codigo_produto');
                $table->dropIndex('cod_origem');
            });

            Schema::table('fefo', function (Blueprint $table) {
                $table->index('codigo_produto');
                $table->index('cod_origem');
                $table->index('semanas');
                $table->index('date');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fefo', function (Blueprint $table) {
            $table->dropIndex(['codigo_produto']);
            $table->dropIndex(['cod_origem']);
            $table->dropIndex(['semanas']);
            $table->dropIndex(['date']);
        });

        Schema::table('fefo', function (Blueprint $table) {
            DB::statement("CREATE INDEX codigo_produto ON fefo (codigo_produto);");
            DB::statement("CREATE INDEX cod_origem ON fefo (cod_origem);");
            DB::statement("CREATE INDEX codigo_produto_2 ON fefo (codigo_produto);");
        });

    }
}
