<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixNewDuplicatedClientCompanyPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {

            $getAllCompanies = DB::table('companies')->get();

            foreach ($getAllCompanies as $company) {
                $this->normalizeClientCompanyPayments($company->id);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    private function normalizeClientCompanyPayments($companyId)
    {
        $duplicatedClientCompanyPayments = DB::table('client_company_payments')
            ->selectRaw('
                client_company_payments.id,
                client_company_payments.client_id,
                client_company_payments.created_at,
                company_payments.id as companyPaymentId
            ')
            ->join('company_payments', 'client_company_payments.company_payment_id', '=', 'company_payments.id')
            ->join('payments', 'payments.id', '=', 'company_payments.payment_id')
            ->where('company_payments.company_id', $companyId)
            // ->whereNull('client_company_payments.deleted_at')
            ->groupBy('client_company_payments.client_id')
            ->groupBy('payments.id')
            ->havingRaw('count(client_company_payments.client_id) > 1')
            ->orderBy('client_company_payments.created_at')
            ->get()
            ->toArray();

        foreach ($duplicatedClientCompanyPayments as $duplicatedClientCompanyPayment) {

            $lastClientCompanyPaymentDuplicated = DB::table('client_company_payments')
                ->select(
                    'client_company_payments.id',
                    'client_company_payments.client_id',
                    'client_company_payments.company_payment_id',
                    'client_company_payments.created_at',
                    'client_company_payments.created_at',
                    'client_company_payments.deleted_at'
                )
                ->join('company_payments', 'client_company_payments.company_payment_id', '=', 'company_payments.id')
                ->join('payments', 'payments.id', '=', 'company_payments.payment_id')
                ->where('client_id', $duplicatedClientCompanyPayment->client_id)
                ->where('company_payments.company_id', $companyId)
                ->where('client_company_payments.company_payment_id', $duplicatedClientCompanyPayment->companyPaymentId)
                ->orderBy('created_at', 'asc')
                ->get();

            $first = $lastClientCompanyPaymentDuplicated->first();

            //update all bids that have duplicated payments method to first
            $updateDuplicatedPaymentIdOnBid = DB::table('lances')
                ->whereIn('client_company_payment_id', $lastClientCompanyPaymentDuplicated->pluck('id'))
                ->where('client_company_payment_id', '!=', $first->id)
                ->update(['client_company_payment_id' => $first->id]);

            //update all bids that have duplicated payments method to first
            $updateDuplicatedPaymentIdOnOrder = DB::table('pedidos')
                ->whereIn('client_company_payment_id', $lastClientCompanyPaymentDuplicated->pluck('id'))
                ->where('client_company_payment_id', '!=', $first->id)
                ->update(['client_company_payment_id' => $first->id]);

            $deleteDuplicated = DB::table('client_company_payments')
                ->whereIn('id', $lastClientCompanyPaymentDuplicated->pluck('id'))
                ->where('id', '!=', $first->id)
                ->delete();

            $updateFirstPaymentMethod = DB::table('client_company_payments')
                ->where('id', $first->id)
                ->update(['deleted_at' => NULL]);

            Log::info('Deleting clientCompanyPaymentId'
                .$lastClientCompanyPaymentDuplicated->where('id', '!=', $first->id)->pluck('id').
            '');
        }
    }
}
