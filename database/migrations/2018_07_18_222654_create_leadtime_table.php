<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeadtimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('leadtime')) {
            Schema::create('leadtime', function (Blueprint $table){
                $table->integer('id', true);
                $table->integer('cod_origem')->index('origem');
                $table->string('nome')->nullable();
                $table->string('cnpj', 25)->index('cnpj');
                $table->integer('dias');
                $table->integer('visao_logistica')->default(0);
                $table->integer('hora_corte')->default(1500);
                $table->integer('segunda')->default(0);
                $table->integer('terca')->default(0);
                $table->integer('quarta')->default(0);
                $table->integer('quinta')->default(0);
                $table->integer('sexta')->default(0);
                $table->integer('sabado')->default(0);
                $table->integer('domingo')->default(0);
                $table->integer('opera_sabado')->default(0);
                $table->integer('opera_domingo')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('leadtime');
	}

}
