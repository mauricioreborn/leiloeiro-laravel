<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateRegistrationClientesIndependentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_independentes', function (Blueprint $table) {
            $table->string('state_registration')->nullable()->after('cnpj');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_independentes', function (Blueprint $table) {
            $table->dropColumn(['state_registration']);
        });
    }
}
