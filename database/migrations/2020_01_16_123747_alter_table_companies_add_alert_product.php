<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Company\Company;

class AlterTableCompaniesAddAlertProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->text('product_info')->nullable()->after('information');
        });

        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            $client = Company::find(5);

            //phpcs:ignore
            $client->product_info = "Você está acostumado a comprar produtos Tirolez negociando pelo <b>VALOR DA CAIXA</b>, na Souk é utilizado como referência o <b>VALOR DO QUILO</b> do produto";
            $client->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('product_info');
        });
    }
}
