<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        if (!Schema::hasTable('pedidos_produtos')) {
            Schema::create('pedidos_produtos', function (Blueprint $table){
                $table->integer('id', true);
                $table->string('codigo_pedido_produto', 25)->nullable();
                $table->integer('pedido_id');
                $table->integer('produto_id')->index('produto_id');
                $table->integer('cod_origem');
                $table->integer('semanas');
                $table->integer('qtd_caixas');
                $table->float('total_kg', 10);
                $table->decimal('valor_kg', 10);
                $table->decimal('valor', 10);
                $table->date('data_embarque')->nullable();
                $table->date('data_entrega')->nullable();
                $table->integer('cd_empresa');
                $table->integer('cd_faixa_fefo');
                $table->string('ds_chave_seara', 50)->nullable();
                $table->integer('status_final_id')->nullable();
                $table->string('status_final_descricao', 80)->nullable();
                $table->integer('status')->default(1);
                $table->integer('email_enviado')->default(1);
                $table->string('CD_MOTIVO_CCL_PEDIDO', 10)->nullable();
                $table->string('DS_MOTIVO_CCL_PEDIDO', 200)->nullable();
                $table->dateTime('created_at')->nullable();
                $table->softDeletes();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('pedidos_produtos');
	}

}
