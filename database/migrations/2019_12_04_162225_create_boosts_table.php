<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boosts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->string('name');
            $table->boolean('whatsapp')->default(false);
            $table->boolean('push')->default(false);
            $table->boolean('email')->default(false);
            $table->boolean('sms')->default(false);
            $table->string('title');
            $table->string('description');
            $table->integer('product_code')->nullable();
            $table->integer('weeks')->nullable();
            $table->integer('coverage')->nullable();
            $table->integer('reach')->nullable();
            $table->integer('conversion')->nullable();
            $table->integer('disqualified')->nullable();
            $table->integer('ruled_out')->nullable();
            $table->decimal('revenue', 10, 2)->nullable();
            $table->unsignedInteger('status_id');
            $table->text('applied_filters')->nullable();
            $table->dateTime('started_at')->nullable();
            $table->dateTime('finished_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boosts');
    }
}
