<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTirolezCompany extends Migration
{
      protected $companyName = 'Tirolez';
      protected $email = 'tirolez@souk.com.br';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            (new TirolezSeeder())->run($this->companyName, $this->email);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            $company = DB::table('companies')->where([
                'name' => $this->companyName
            ])->first();

            if (isset($company->id)) {
                DB::table('users')->where([
                    'email' => $this->email
                ])->delete();

                DB::table('company_settings')->where([
                    'company_id' => $company->id
                ])->delete();

                DB::table('company_payments')->where([
                    'company_id' => $company->id
                ])->delete();

                DB::table('companies')->where([
                'name' => $this->companyName
                ])->delete();
            }
        }
    }
}
