<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('client_company_payment_id')
                ->references('id')
                ->on('client_company_payments');
            $table->unsignedInteger('client_company_payment_id')
                ->nullable()
                ->after('aceite_parcial');

            $table->foreign('client_card_id')->references('id')->on('client_cards');
            $table->unsignedInteger('client_card_id')
                ->after('aceite_parcial')->nullable();

            $table->integer('cc_months')
                ->after('aceite_parcial')->default(1);

            $table->float('tax_percent')
                ->after('aceite_parcial')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropForeign('pedidos_client_card_id_foreign');
            $table->dropColumn('client_card_id');

            $table->dropForeign('pedidos_client_company_payment_id_foreign');
            $table->dropColumn('client_company_payment_id');

            $table->dropColumn('cc_months');
            $table->dropColumn('tax_percent');
        });
    }
}
