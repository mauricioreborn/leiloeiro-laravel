<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientTableAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->unsignedInteger('confirm_sms')->nullable()->after('register_updated');
            $table->unsignedInteger('confirm_email')->nullable()->after('confirm_sms');

            $table->foreign('confirm_sms')->references('id')->on('status');
            $table->foreign('confirm_email')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropForeign('clientes_confirm_sms_foreign');
            $table->dropForeign('clientes_confirm_email_foreign');

            $table->dropColumn(['confirm_sms', 'confirm_email']);
        });
    }
}
