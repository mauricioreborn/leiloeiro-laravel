<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientListsCnpj extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_lists_cnpj', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_list_id');
            $table->string('cnpj');

            $table->foreign('client_list_id')->references('id')->on('client_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_lists_cnpj');
    }
}
