<?php
use App\Auth\User;
use App\Company\Company;
use Illuminate\Database\Migrations\Migration;

class CreateSoukBotUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $massaleve = Company::find(3);
        $nestle = Company::find(4);

        if($massaleve !== null) {
            $massaleve->users()->create([
                'name' => 'soukBot',
                'email' => 'soukbot.massaleve@souk.com.br',
                'password' => md5('S0uk@123'),
                'root' => 1,
                'price' => 0,
                'featured' => 1,
            ]);
        }

        if($nestle !== null) {
            $nestle->users()->create([
                'name' => 'soukBot',
                'email' => 'soukbot.nestle@souk.com.br',
                'password' => md5('S0uk@123'),
                'root' => 1,
                'price' => 0,
                'featured' => 1,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::where('email', 'soukbot.massaleve@souk.com.br')->delete();
        User::where('email', 'soukbot.nestle@souk.com.br')->delete();
    }
}
