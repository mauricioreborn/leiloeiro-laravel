<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProdutoUserTableChangeColumnProductCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produto_user', function (Blueprint $table) {
            $table->integer('produto_id')->length(11)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produto_user', function (Blueprint $table) {
            $table->unsignedInteger('produto_id')->length(10)->change();
        });
    }
}
