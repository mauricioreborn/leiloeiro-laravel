<?php

use App\Company\CompanySetting;
use Illuminate\Database\Migrations\Migration;

class CompanySeetingsStockReplicate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new CompanySettingsStockReplicateSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CompanySetting::where('setting', 'hasStockReplicate')->delete();
    }
}
