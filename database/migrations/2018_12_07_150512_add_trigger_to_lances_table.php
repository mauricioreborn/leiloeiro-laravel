<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTriggerToLancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER tgr_inserted_bids AFTER INSERT ON lances FOR EACH ROW
            BEGIN
                INSERT INTO processed_bids (bid_id) VALUES (NEW.id);
            END');

        DB::unprepared('CREATE TRIGGER tgr_updated_bids AFTER UPDATE ON lances FOR EACH ROW
            BEGIN
                INSERT INTO processed_bids (bid_id) VALUES (NEW.id);
            END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS  tgr_inserted_bids');
        DB::unprepared('DROP TRIGGER IF EXISTS  tgr_updated_bids');
    }
}
