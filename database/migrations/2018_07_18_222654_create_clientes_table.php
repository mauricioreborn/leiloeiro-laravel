<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('clientes')) {
            Schema::create('clientes', function (Blueprint $table){
                $table->increments('id');
                $table->string('cnpj', 25)->index();
                $table->string('password');
                $table->string('nome');
                $table->string('rede')->nullable();
                $table->boolean('status')->default(1)->index();
                $table->decimal('saldo', 20);
                $table->decimal('pedido_minimo', 10)->default(200.00);
                $table->text('endereco', 65535)->nullable();
                $table->string('uf', 2);
                $table->string('municipio', 120);
                $table->string('email', 200)->nullable();
                $table->string('telefone', 200)->nullable();
                $table->enum('email_verified', ['1', '0'])->nullable()->default('0');
                $table->timestamps();
            });
        }
    }


    /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('clientes');
	}

}
