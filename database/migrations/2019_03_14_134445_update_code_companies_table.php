<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCodeCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('companies')->where('id', 1)->update(['code' => 1]);

        DB::table('companies')->where('id', 3)->update(['code' => 6]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('companies')->where('id', 1)->update(['code' => null]);

        DB::table('companies')->where('id', 3)->update(['code' => null]);
    }
}
