<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClientConfirmationsTableAddIndependentClientId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_confirmations', function (Blueprint $table) {
            $table->integer('client_id')->unsigned()->nullable()->change();
            $table->integer('independent_client_id')->unsigned()->nullable()->after('client_id');

            $table->foreign('independent_client_id')
                ->references('id')
                ->on('clientes_independentes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_confirmations', function (Blueprint $table) {
            $table->dropForeign(['independent_client_id']);
            $table->dropColumn(['independent_client_id']);
        });
    }
}
