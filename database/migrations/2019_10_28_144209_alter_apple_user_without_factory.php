<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Bid\Bid;
use App\Core\Entities\Status;
use App\Client\ClientCompanyPayment;
use App\Payment\Payment;
use App\Company\CompanyPayment;
use App\LeadTime\LeadTime;

class AlterAppleUserWithoutFactory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            $client = Client::where(
                ['email' => 'renato.siroma@gmail.com', 'cnpj' => '54240606000123']
            )->first();

            $leadtimes = LeadTime::where([
                'cnpj' => '97747439000114',
            ])->whereIn('company_id', [1, 4])->get();

            if (!isset($client->id)) {
                $client = Client::create([
                    'nome' => 'E F PIZZARIA',
                    'email' => 'renato.siroma@gmail.com',
                    'cnpj' => '54240606000123',
                    'register_updated' => false,
                    'email_verified' => 1,
                    'state_registration' => null,
                    'password' => '123qwe',
                    'contact_name' => 'renato siroma',
                    'telefone' => '9238934851',
                    'confirm_sms' => null,
                    'confirm_email' => null,
                ]);


                $payment = Payment::where([
                    'type' => 'company_credit'
                ])->first();

                $companyPaymentSeara = CompanyPayment:: where([
                    'company_id' => 1,
                    'payment_id' => $payment->id
                ])->first();

                $companyPaymentNestle = CompanyPayment:: where([
                    'company_id' => 4,
                    'payment_id' => $payment->id
                ])->first();

                ClientCompany::create([
                    'client_code' => null,
                    'company_id' => 1,
                    'client_id' => $client->id,
                    'min_order' => 100,
                    'account_balance' => 1000,
                    'status' => 0,
                    'channel' => 'FOOD SERVICES REGIONAIS',
                    'address' => 'sem endereco',
                    'state' => 'SP',
                    'city' => 'SAO PAULO',
                    'track_1' => 1,
                    'track_2' => 1,
                    'track_3' => 1,
                    'track_4' => 1,
                    'typology' => 'BARES / LANCHONETES / CANTINAS',
                    'typology_code',
                    'created_at',
                    'updated_at'
                ]);

                ClientCompany::create([
                    'client_code' => null,
                    'company_id' => 4,
                    'client_id' => $client->id,
                    'min_order' => 100,
                    'account_balance' => 1000,
                    'status' => 0,
                    'channel' => 'FOOD SERVICES REGIONAIS',
                    'address' => 'sem endereco',
                    'state' => 'SP',
                    'city' => 'SAO PAULO',
                    'track_1' => 1,
                    'track_2' => 1,
                    'track_3' => 1,
                    'track_4' => 1,
                    'typology' => 'BARES / LANCHONETES / CANTINAS',
                    'typology_code',
                    'created_at',
                    'updated_at'
                ]);

                $clientPaymentSeara = ClientCompanyPayment::create([
                    'client_id' => $client->id,
                    'company_payment_id' => $companyPaymentSeara->id
                ]);

                $clientPaymentNestle = ClientCompanyPayment::create([
                    'client_id' => $client->id,
                    'company_payment_id' => $companyPaymentNestle->id
                ]);


                foreach ($leadtimes as $leadtime) {
                    Bid::create([
                        'date' => now()->toDateString(),
                        'product_code' => 238349,
                        'weeks' => 3,
                        'box_amount' => 2,
                        'weight' => 1,
                        'kg_price' => 30.00,
                        'price' => 243.00,
                        'partial_accept' => 0,
                        'duration' => now()->toDateString(),
                        'user_id' => null,
                        'accept_date',
                        'original_box_amount' => 2,
                        'sanctioned_at' => now()->toDateString(),
                        'client_id' => $client->id,
                        'company_id' => 1,
                        'status_id' => Status::type(Status::CANCELED)->id,
                        'status' => 0,
                        'origin_code' => $leadtime->code,
                        'client_company_payment_id' => $clientPaymentSeara->id,
                    ]);

                    Bid::create([
                        'date' => now()->toDateString(),
                        'product_code' => 238349,
                        'weeks' => 3,
                        'box_amount' => 2,
                        'weight' => 1,
                        'kg_price' => 30.00,
                        'price' => 243.00,
                        'partial_accept' => 0,
                        'duration' => now()->toDateString(),
                        'user_id' => null,
                        'accept_date',
                        'original_box_amount' => 2,
                        'sanctioned_at' => now()->toDateString(),
                        'client_id' => $client->id,
                        'company_id' => 4,
                        'status_id' => Status::type(Status::CANCELED)->id,
                        'status' => 0,
                        'origin_code' => $leadtime->code,
                        'client_company_payment_id' => $clientPaymentNestle->id,
                    ]);

                    LeadTime::create([
                        'cnpj' => $client->cnpj,
                        'code' => $leadtime->code,
                        'name' => $leadtime->name,
                        'days' => $leadtime->days,
                        'company_id' => $leadtime->company_id,
                        'prediction' => $leadtime->prediction,
                        'limit_hour' => $leadtime->limit_hour,
                        'monday' => $leadtime->monday,
                        'tuesday' => $leadtime->tuesday,
                        'wednesday' => $leadtime->wednesday,
                        'thursday' => $leadtime->thursday,
                        'friday' => $leadtime->friday,
                        'saturday' => $leadtime->saturday,
                        'sunday' => $leadtime->sunday,
                        'operates_saturday' => $leadtime->operates_saturday,
                        'operates_sunday' => $leadtime->operates_sunday,
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            $client = Client::where(
                [
                    'nome' => 'E F PIZZARIA',
                    'email' => 'renato.siroma@gmail.com',
                    'cnpj' => '54240606000123',
                    'register_updated' => false,
                    'email_verified' => 1,
                ])->first();

            if (isset($client->id)) {
                ClientCompanyPayment::where([
                    'client_id' => $client->id
                ])->delete();

                ClientCompany::where([
                    'client_id' => $client->id,
                ])->delete();

                LeadTime::where([
                    'cnpj' => $client->cnpj
                ])->delete();

                Bid::where([
                    'id_cliente' => $client->id,
                ])->delete();

                $client->update([
                    'email_verified' => 0,
                ]);
            }
        }
    }
}
