<?php

use Illuminate\Database\Migrations\Migration;
use App\Company\CompanyDivision;

class DropCompanyDivisionsIncorrectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        CompanyDivision::whereNotIn('company_id', [1,3])->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
