<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        if (!Schema::hasTable('lances')) {
            Schema::create('lances', function (Blueprint $table){
                $table->integer('id', true);
                $table->integer('id_cliente');
                $table->date('data');
                $table->integer('cod_origem');
                $table->integer('cod_produto');
                $table->integer('semanas');
                $table->integer('qtd_caixas');
                $table->float('total_kg', 10);
                $table->decimal('valor_kg', 10);
                $table->decimal('valor', 10);
                $table->integer('aceite_parcial');
                $table->date('duracao_lance');
                $table->integer('id_user')->nullable()->comment('ID ');
                $table->integer('status')->default(1);
                $table->date('data_aceite')->nullable();
                $table->integer('nao_lido')->default(1);
                $table->timestamps();
                $table->softDeletes();
                $table->index(['id_user', 'cod_produto'], 'id_user');
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lances');
	}

}
