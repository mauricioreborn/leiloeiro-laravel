<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyBidLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bid_logs', function (Blueprint $table) {
            $table->integer('bid_id')->change();
            $table->integer('user_id')->nullable()->change();
        });

        Schema::table('bid_logs', function (Blueprint $table) {
            $table->foreign('client_id')->references('id')->on('clientes');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('bid_id')->references('id')->on('lances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bid_logs', function (Blueprint $table) {
            $table->unsignedInteger('bid_id')->change();
            $table->unsignedInteger('user_id')->nullable()->change();
              $table->dropForeign(['client_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['bid_id']);
        });
    }
}
