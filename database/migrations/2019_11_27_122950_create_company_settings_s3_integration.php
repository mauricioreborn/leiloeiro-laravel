<?php

use App\Company\CompanySetting;
use Illuminate\Database\Migrations\Migration;

class CreateCompanySettingsS3Integration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new CompanySettingsOrderS3IntegrationSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CompanySetting::where('setting', 'hasOrderS3Integration')->delete();
    }
}
