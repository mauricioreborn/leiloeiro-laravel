<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id')
                ->foreign('campaigns_companies')
                ->references('id')
                ->on('companies');
            $table->unsignedInteger('user_id')
                ->foreign('campaign_users')
                ->references('id')
                ->on('users');
            $table->unsignedInteger('campaign_type_id')
                ->foreign('campaign_campaign_types')
                ->references('id')
                ->on('campaign_types');
            $table->longText('attributes')->nullable();
            $table->integer('estimated_clients')->nullable();
            $table->dateTime('scheduled_at');
            $table->dateTime('processed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
