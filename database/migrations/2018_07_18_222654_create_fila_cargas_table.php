<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFilaCargasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('fila_cargas')) {
            Schema::create('fila_cargas', function (Blueprint $table){
                $table->integer('id', true);
                $table->string('file_name');
                $table->text('file_url', 65535);
                $table->string('total_success');
                $table->string('total_error');
                $table->string('total_duplicates');
                $table->text('status', 65535);
                $table->string('file_size')->nullable();
                $table->integer('tipo_carga');
                $table->dateTime('started_at')->nullable();
                $table->dateTime('finished_at')->nullable();
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('fila_cargas');
	}

}
