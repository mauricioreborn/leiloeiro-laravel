<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Auth\User;
use Illuminate\Support\Str;

class AlterUsersTableInsertNewUserToMiddleware extends Migration
{
    protected  $email = 'webhooks@souk.com.br';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $name = 'webhooks';

        $company = 1;
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            $company = 2;
        }
            $user = User:: where(['email' => $this->email])->first();
        if (!isset($user->id)) {
            User::create([
                'name' => $name,
                'email' => $this->email,
                'password' => Str::random(32),
                'root' => 1,
                'company_id' => $company,
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User:: where(['email' => $this->email])->delete();
    }
}
