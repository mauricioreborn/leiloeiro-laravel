<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableClientesIndependentesForSimplifiedSignUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_independentes', function (Blueprint $table) {
            $table->string('nome')->nullable()->change();
            $table->unsignedInteger('confirm_sms')->nullable()->after('integrado');
            $table->foreign('confirm_sms')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes_independentes', function (Blueprint $table) {
            $table->dropForeign(['confirm_sms']);
            $table->dropColumn(['confirm_sms']);
        });
    }
}
