<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_companies', function (Blueprint $table) {
            $table->decimal('account_balance', 20)->after('company_id');
            $table->decimal('min_order', 10)->default(200.00)->after('company_id');
            $table->text('address', 65535)->after('company_id');
            $table->string('state', 2)->after('company_id');
            $table->string('city', 120)->after('company_id');
            $table->string('track_4', 3)->default(1)->after('company_id');
            $table->string('track_3', 3)->default(1)->after('company_id');
            $table->string('track_2', 3)->default(1)->after('company_id');
            $table->string('track_1', 3)->default(1)->after('company_id');
            $table->string('typology')->nullable()->after('company_id');
            $table->integer('typology_code')->nullable()->after('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_companies', function (Blueprint $table) {
            $table->dropColumn('account_balance');
            $table->dropColumn('min_order');
            $table->dropColumn('state');
            $table->dropColumn('city');
            $table->dropColumn('address');
            $table->dropColumn('track_4');
            $table->dropColumn('track_3');
            $table->dropColumn('track_2');
            $table->dropColumn('track_1');
            $table->dropColumn('typology');
            $table->dropColumn('typology_code');
        });
    }
}
