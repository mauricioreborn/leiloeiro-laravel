<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLancesAddColumnStatusLance extends Migration
{
    public function up()
    {
        Schema::table('lances', function (Blueprint $table) {
            if (!Schema::hasColumn('lances', 'bid_rejection_status_id')) {
                $table->integer('bid_rejection_status_id')->after('status')->default(0);
            }
        });
    }

    public function down()
    {
        Schema::table('lances', function (Blueprint $table) {
            $table->dropColumn('bid_rejection_status_id');
        });
    }
}
