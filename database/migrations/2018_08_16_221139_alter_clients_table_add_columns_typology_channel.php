<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientsTableAddColumnsTypologyChannel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {

            if (!Schema::hasColumn('clientes', 'tipo')) {
                $table->string('tipo')->nullable();
            }

            if (!Schema::hasColumn('clientes', 'canal')) {
                $table->string('canal')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropColumn(['tipo', 'canal']);
        });
    }
}
