<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VwOrigem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_origem;");

        DB::statement("CREATE 
                VIEW `vw_origem` AS
                    SELECT 
                        `leadtime`.`cod_origem` AS `cod_origem`,
                        `leadtime`.`nome` AS `nome`,
                        `leadtime`.`company_id` AS `company_id`
                    FROM
                        `leadtime`
                    GROUP BY `leadtime`.`cod_origem` , `leadtime`.`nome`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_origem;");
    }
}
