<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFefoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('fefo')) {
            Schema::create('fefo', function (Blueprint $table){
                $table->integer('id', true);
                $table->date('date');
                $table->integer('semanas');
                $table->integer('cod_origem');
                $table->string('codigo_produto', 50)->index('codigo_produto');
                $table->decimal('volume', 10)->index('volume');
                $table->decimal('valor_atual', 10);
                $table->decimal('valor_cheio', 10);
                $table->decimal('valor_min', 10)->nullable();
                $table->integer('valor_alterado_admin')->default(0);
                $table->date('vencimento')->nullable();
                $table->integer('destaque')->default(0);
                $table->integer('cd_empresa');
                $table->integer('cd_faixa_fefo');
                $table->string('ds_chave_seara', 50)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('fefo');
	}

}
