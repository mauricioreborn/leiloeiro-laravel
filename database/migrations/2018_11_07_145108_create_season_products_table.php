<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('season_id')->foreign('season_id')->references('id')->on('season_type');
            $table->unsignedInteger('product_code')->foreign('product_code')->references('codigo')->on('produtos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('season_products');
    }
}
