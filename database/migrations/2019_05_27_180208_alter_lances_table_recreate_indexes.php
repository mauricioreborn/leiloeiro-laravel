<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLancesTableRecreateIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            DB::statement("ALTER TABLE lances DROP INDEX id_user");

            Schema::table('lances', function (Blueprint $table) {
                $table->index('data');
                $table->index('cod_origem');
                $table->index('cod_produto');
                $table->index('semanas');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lances', function (Blueprint $table) {
            $table->dropIndex(['data']);
            $table->dropIndex(['cod_origem']);
            $table->dropIndex(['cod_produto']);
            $table->dropIndex(['semanas']);
        });

        Schema::table('lances', function (Blueprint $table) {
            DB::statement("CREATE INDEX id_user ON lances (id_user);");
        });
    }
}
