<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarrinhoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('carrinho')) {
            Schema::create('carrinho', function (Blueprint $table){
                $table->integer('id', true);
                $table->integer('id_cliente');
                $table->date('data');
                $table->integer('status')->default(1);
                $table->dateTime('created_at');
                $table->softDeletes();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //Schema::drop('carrinho');
	}

}
