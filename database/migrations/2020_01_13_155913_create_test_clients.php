<?php
use App\Bid\Bid;
use App\Bid\BidLog;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\ClientCompanies\ClientCompany;
use App\Company\CompanyPayment;
use App\Core\Entities\EntityHistory;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Payment\Payment;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateTestClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(env('APP_ENV') === 'testing') {
            return;
        }

        $this->createSearaClient();
        $this->createTirolezClient();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(env('APP_ENV') === 'testing') {
            return;
        }

        $this->dropClient('98746233000132');
        $this->dropClient('53816944000106');
    }

    protected function createSearaClient()
    {
        // Cliente de testes com acesso Seara/MassaLeve
        $client = Client::where('email', 'seara@souk.com.br')
            ->where('cnpj', '98746233000132')
            ->first();

        if($client !== null) {
            return;
        }

        $client = Client::create([
            'nome' => 'SEARA/MASSALEVE SANDBOX',
            'email' => 'seara@souk.com.br',
            'cnpj' => '98746233000132',
            'register_updated' => false,
            'email_verified' => 1,
            'state_registration' => null,
            'password' => '123qwe',
            'contact_name' => 'Renato Siroma',
            'telefone' => '11952336062',
            'confirm_sms' => 10,
            'confirm_email' => 10,
        ]);

        $clientCompanyPayload = [
            'client_code' => null,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 10000,
            'status' => 1,
            'channel' => 'FOOD SERVICES REGIONAIS',
            'address' => 'sem endereco',
            'state' => 'SP',
            'city' => 'SAO PAULO',
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'typology' => 'BARES / LANCHONETES / CANTINAS',
        ];

        ClientCompany::create(array_merge($clientCompanyPayload, ['company_id' => 1]));
        ClientCompany::create(array_merge($clientCompanyPayload, ['company_id' => 3]));

        $payment = Payment::where('type', 'company_credit')->first();

        $companyPaymentSeara = CompanyPayment:: where([
            'company_id' => 1,
            'payment_id' => $payment->id
        ])->first();

        ClientCompanyPayment::create([
            'client_id' => $client->id,
            'company_payment_id' => $companyPaymentSeara->id
        ]);

        $companyPaymentMassaLeve = CompanyPayment:: where([
            'company_id' => 3,
            'payment_id' => $payment->id
        ])->first();

        ClientCompanyPayment::create([
            'client_id' => $client->id,
            'company_payment_id' => $companyPaymentMassaLeve->id
        ]);

        $leadtimes = LeadTime::where('cnpj', '97747439000114')->whereIn('company_id', [1, 3])->get();

        foreach ($leadtimes as $leadtime) {
            $newLeadtime = $leadtime->replicate();
            $newLeadtime->cnpj = $client->cnpj;

            $newLeadtime->save();
        }
    }

    protected function createTirolezClient()
    {
        // Cliente de testes com acesso Tirolez
        $client = Client::where('email', 'tirolez@souk.com.br')
            ->where('cnpj', '53816944000106')
            ->first();

        if($client !== null) {
            return;
        }

        $client = Client::create([
            'nome' => 'TIROLEZ SANDBOX',
            'email' => 'tirolez@souk.com.br',
            'cnpj' => '53816944000106',
            'register_updated' => false,
            'email_verified' => 1,
            'state_registration' => null,
            'password' => '123qwe',
            'contact_name' => 'Renato Siroma',
            'telefone' => '11952336062',
            'confirm_sms' => 10,
            'confirm_email' => 10,
        ]);

        $clientCompanyPayload = [
            'client_code' => null,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 10000,
            'status' => 1,
            'address' => 'sem endereco',
            'state' => 'SP',
            'city' => 'SAO PAULO',
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
        ];

        ClientCompany::create(array_merge($clientCompanyPayload, ['company_id' => 5]));

        $payment = Payment::where('type', 'company_credit')->first();

        $companyPaymentTirolez = CompanyPayment:: where([
            'company_id' => 1,
            'payment_id' => $payment->id
        ])->first();

        ClientCompanyPayment::create([
            'client_id' => $client->id,
            'company_payment_id' => $companyPaymentTirolez->id
        ]);

        $leadtimes = LeadTime::where('cnpj', '97747439000114')->whereIn('company_id', [1])->get();

        foreach ($leadtimes as $leadtime) {
            $newLeadtime = $leadtime->replicate();
            $newLeadtime->cnpj = $client->cnpj;
            $newLeadtime->company_id = 5;
            $newLeadtime->code = 666;

            $newLeadtime->save();
        }
    }

    protected function dropClient($cnpj)
    {
        $client = Client::where('cnpj', $cnpj)->first();

        if($client === null) {
            return;
        }

        EntityHistory::where('client_id', $client->id)->delete();

        $orders = Order::where('client_id', $client->id)->pluck('id')->toArray();

        if(count($orders) > 0) {
            OrderProduct::withTrashed()->whereIn('pedido_id', $orders)->forceDelete();
            Order::where('client_id', $client->id)->delete();
        }

        $bids = Bid::withTrashed()->where('client_id', $client->id)->pluck('id')->toArray();

        if(count($bids) > 0) {
            BidLog::withTrashed()->where('client_id', $client->id)->forceDelete();
            DB::table('processed_bids')->whereIn('bid_id', $bids)->delete();
            Bid::withTrashed()->where('client_id', $client->id)->forceDelete();
        }

        ClientCompany::where('client_id', $client->id)->forceDelete();
        ClientCompanyPayment::withTrashed()->where('client_id', $client->id)->forceDelete();
        LeadTime::withTrashed()->where('cnpj', $cnpj)->forceDelete();

        $client->delete();
    }
}
