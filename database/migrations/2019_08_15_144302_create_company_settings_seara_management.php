<?php

use Illuminate\Database\Migrations\Migration;

class CreateCompanySettingsSearaManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new CompanySettingsManagementRequestsSeeder())->run();
    }
}
