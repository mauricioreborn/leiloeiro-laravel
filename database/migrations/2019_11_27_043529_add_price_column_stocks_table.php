<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceColumnStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->float('price', 10, 2)->after('batch')->nullable();
            $table->float('min_price', 10, 2)->after('price')->nullable();
            $table->float('original_price', 10, 2)->after('min_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stocks', function ($table) {
            if (Schema::hasColumn('stocks', 'price')) {
                $table->dropColumn('price');
            }

            if (Schema::hasColumn('stocks', 'min_price')) {
                $table->dropColumn('min_price');
            }

            if (Schema::hasColumn('stocks', 'original_price')) {
                $table->dropColumn('original_price');
            }
        });
    }
}
