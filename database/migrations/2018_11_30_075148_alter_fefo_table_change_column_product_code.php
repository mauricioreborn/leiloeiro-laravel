<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFefoTableChangeColumnProductCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fefo', function (Blueprint $table) {
            $table->integer('codigo_produto')->length(11)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fefo', function (Blueprint $table) {
            $table->string('codigo_produto', 50)->change();
        });
    }
}
