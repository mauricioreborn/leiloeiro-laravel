<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDataCompanyNestleRules extends Migration
{
    protected $old = "<h2>Disponibilidade de Estoque</h2> Todas as modalidades de vendas (Carrinho ou Pregão Eletrônico - Lance) estão sujeitas à disponibilidade de estoque no dia do embarque ou entrega. <br> <h2>Prazo de Pagamento e Entregas, Pedido Mínimo, Endereço de Entrega e Devoluções</h2> Todas as regras relacionadas a: prazo de pagamento e entregas, pedido mínimo, endereço de entregas e devoluções obedecem às instruções e particularidades de cada indústria. Consulte o suporte souk pelo email suporte@souk.com.br. <br> <h2>Produtos com peso variável</h2> Alguns produtos, devido a sua natureza, possuem peso variável pela dificuldade de padronização – exemplo cortes in-natura podem variar em gramas o peso da caixa de embarque. As ofertas seguem conforme especificação da indústria.";
    protected $new = "<h2>Disponibilidade de Estoque</h2> Todas as modalidades de vendas (Carrinho ou Pregão Eletrônico - Lance) estão sujeitas à disponibilidade de estoque no dia do embarque ou entrega. <br> <h2>Prazo de Pagamento e Entregas, Pedido Mínimo, Endereço de Entrega e Devoluções</h2> Todas as regras relacionadas a: prazo de pagamento e entregas, pedido mínimo, endereço de entregas e devoluções obedecem às instruções e particularidades de cada indústria. Caso haja alguma dúvida entre contato no e-mail suporte@souk.com.br ou via Whatsapp no número (11) 95233-6062.";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('companies')){
            DB::table('companies')->where('id', '=', 4)->update(['rules' => $this->new, 'updated_at' => Carbon::now()]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('companies')){
            DB::table('companies')->where('id', '=', 4)->update(['rules' => $this->old, 'updated_at' => Carbon::now()]);
        }
    }
}
