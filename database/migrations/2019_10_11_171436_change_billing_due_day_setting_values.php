<?php
use App\Company\CompanySetting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBillingDueDaySettingValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seara = CompanySetting::where('setting', 'billingDueDay')->where('company_id', 1)->first();
        $massaleve = CompanySetting::where('setting', 'billingDueDay')->where('company_id', 3)->first();
        $nestle = CompanySetting::where('setting', 'billingDueDay')->where('company_id', 4)->first();

        if($seara !== null) {
            $seara->update(['value' => 28]);
        }
        if($massaleve !== null) {
            $massaleve->update(['value' => 28]);
        }
        if($nestle !== null) {
            $nestle->update(['value' => 14]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
