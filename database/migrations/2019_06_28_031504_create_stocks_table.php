<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('status_id');
            $table->integer('product_id');
            $table->unsignedInteger('file_queue_id')->nullable();
            $table->integer('origin_code');
            $table->float('volume', 10, 2);
            $table->string('batch')->nullable();
            $table->dateTime('fabricated_at');
            $table->dateTime('expired_at');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('status_id')->references('id')->on('status');
            $table->foreign('product_id')->references('id')->on('produtos');
            $table->foreign('file_queue_id')->references('id')->on('file_queues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
