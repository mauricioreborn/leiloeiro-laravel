<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AlterDataCompanyNestleBid extends Migration
{
    protected $old = "<h2>Volume Parcial</h2> No intervalo de tempo que existe entre o envio até a contemplação do seu lance as disponibilidades dos produtos podem variar. <br> Caso você não aceite receber um “Volume Parcial”, após a validação do preço, seu lance só estará disponível para faturamento quando houver a disponibilidade total dos produtos que você deseja. <hr> <h2>Quantidade de Caixas e Pedido Mínimo</h2> O valor de pedido mínimo reflete diretamente na quantidade de caixas que deverá ser comprada a cada lance. Para algumas indústrias, este valor mínimo de faturamento é exigido para a modalidade de entrega com frete grátis. <hr> <h2>Validade do Lance</h2> Caso a validade do lance seja marcado como “durante o dia de hoje” ele estará disponível para analise até às 18h para os gestores do pregão eletrônico. <br> Caso a validade do lance no pregão eletrônico seja selecionada através do calendário, as datas apresentadas para a duração do seu lance levarão em consideração as datas de embarques possíveis para as grades de entregas na sua região. Seu lance ficará disponível de acordo com a data escolhida até ser aceito ou rejeitado por um gestor da indústria. <br> Você pode cancelar ou editar um lance a qualquer momento até ele ser aceito ou rejeitado.";
    protected $new =  "<h2>Volume Parcial</h2> No intervalo de tempo que existe entre o envio até a contemplação do seu lance as disponibilidades dos produtos podem variar. <br> Caso você não aceite receber um “Volume Parcial”, após a validação do preço, seu lance só estará disponível para faturamento quando houver a disponibilidade total dos produtos que você deseja. <hr> <h2>Quantidade de Caixas e Pedido Mínimo</h2> O valor de pedido mínimo reflete diretamente na quantidade de caixas que deverá ser comprada a cada lance. Para algumas indústrias, este valor mínimo de faturamento é exigido para a modalidade de entrega com frete grátis. <hr> <h2>Validade do Lance</h2> Caso a validade do lance seja marcado como “durante o dia de hoje” ele estará disponível para analise até às 18h para os gestores do pregão eletrônico. <br> Caso a validade do lance no pregão eletrônico seja selecionada através do calendário, as datas apresentadas para a duração do seu lance levarão em consideração as datas de embarques possíveis para as grades de entregas na sua região. Seu lance ficará disponível de acordo com a data escolhida até ser aceito ou rejeitado por um gestor da indústria. <br> Você pode cancelar ou editar um lance a qualquer momento até ele ser aceito ou rejeitado.<hr> <small>Se ainda houver alguma dúvida relacionada ao funcionamento dos lances, fique a vontade para entrar em contato conosco pelo e-mail suporte@souk.com.br ou via Whatsapp no número (11) 95233-6062.</small>";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('companies')){
            DB::table('companies')->where('id', '=', 4)->update(['bid' => $this->new, 'updated_at' => Carbon::now()]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('companies')){
            DB::table('companies')->where('id', '=', 4)->update(['bid' => $this->old, 'updated_at' => Carbon::now()]);
        }
    }
}
