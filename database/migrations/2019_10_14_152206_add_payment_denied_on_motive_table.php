<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Core\Entities\RejectionMotive;

class AddPaymentDeniedOnMotiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new RejectionMotiveSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RejectionMotive::find(RejectionMotive::type(RejectionMotive::PAYMENT_DENIED)->id)->delete();
    }
}
