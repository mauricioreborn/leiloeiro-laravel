<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Fefo\FefoPriceBoost;

class RemoveImpulsionadosOnBoostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $date = now()->toDateString();
        FefoPriceBoost::whereRaw("finish_date > {$date}")->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $date = now()->toDateString();
        FefoPriceBoost::whereRaw("finish_date > {$date}")->withTrashed()->restore();
    }
}
