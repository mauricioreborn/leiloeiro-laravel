<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lances', function (Blueprint $table) {
            $table->unsignedInteger('rejection_motive_id')->nullable()->after('status');
            $table->foreign('rejection_motive_id')->references('id')->on('rejection_motive');

            if (!Schema::hasColumn('lances', 'status_id')) {
                $table->unsignedInteger('status_id')->nullable()->after('status');
                $table->foreign('status_id')->references('id')->on('status');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lances', function (Blueprint $table) {
            $table->dropForeign('lances_rejection_motive_id_foreign');
            $table->dropForeign('lances_status_id_foreign');
            $table->dropColumn(['rejection_motive_id', 'status_id']);
        });
    }
}
