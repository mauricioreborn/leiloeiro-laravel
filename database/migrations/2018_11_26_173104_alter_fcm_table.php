<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFcmTable extends Migration
{

    /**
     * Method to update fcm token param
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fcm_token', function (Blueprint $table) {
            $schema = Schema::getConnection()->getDoctrineSchemaManager()->listTableDetails($table->getTable());

            if (array_key_exists('fcm_token_token_unique', $schema->getIndexes())) {
                $table->dropUnique(['token']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fcm_token', function (Blueprint $table) {
            $schema = Schema::getConnection()->getDoctrineSchemaManager()->listTableDetails($table->getTable());

            if (!array_key_exists('fcm_token_token_unique', $schema->getIndexes())) {
                $table->unique(['token']);
            }
        });
    }
}
