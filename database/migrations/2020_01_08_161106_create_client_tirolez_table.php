<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTirolezTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_tirolez', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->integer('type');
            $table->integer('modal_view_counter')->nullable();
            $table->dateTime('last_updated_at');
            $table->dateTime('banner_start_at')->nullable();
            $table->dateTime('banner_finish_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_id')->references('id')->on('clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_tirolez');
    }
}
