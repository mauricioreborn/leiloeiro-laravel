<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBidTableAddCardPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lances', function (Blueprint $table) {
            $table->integer('cc_months')
                ->after('nao_lido')->default(1);

            $table->float('tax_percent')
                ->after('cc_months')->default(0);

            $table->foreign('client_company_payment_id')
                ->references('id')
                ->on('client_company_payments');
            $table->unsignedInteger('client_company_payment_id')
                ->nullable()
                ->after('tax_percent');

            $table->foreign('client_card_id')->references('id')->on('client_cards');
            $table->unsignedInteger('client_card_id')
                ->after('client_company_payment_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lances', function (Blueprint $table) {
            $table->dropForeign('lances_client_card_id_foreign');
            $table->dropColumn('client_card_id');

            $table->dropForeign('lances_client_company_payment_id_foreign');
            $table->dropColumn('client_company_payment_id');

            $table->dropColumn('cc_months');
            $table->dropColumn('tax_percent');
        });
    }
}
