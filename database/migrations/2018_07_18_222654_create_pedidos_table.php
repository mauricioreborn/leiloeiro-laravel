<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        if (!Schema::hasTable('pedidos')) {
            Schema::create('pedidos', function (Blueprint $table){
                $table->integer('id', true);
                $table->string('codigo_pedido', 50)->nullable();
                $table->integer('id_cliente')->index('id_cliente');
                $table->date('data')->index('data');
                $table->integer('lance_id')->nullable();
                $table->integer('status')->default(1);
                $table->string('motivo_cancelamento', 244)->nullable();
                $table->integer('aceite_parcial')->default(0);
                $table->date('data_embarque')->nullable();
                $table->date('data_entrega')->nullable();
                $table->integer('nao_lido')->default(1);
                $table->integer('integrado')->default(0);
                $table->dateTime('created_at')->nullable();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('pedidos');
	}

}
