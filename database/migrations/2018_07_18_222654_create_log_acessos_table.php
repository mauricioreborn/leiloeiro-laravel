<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogAcessosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('log_acessos')) {
            Schema::create('log_acessos', function (Blueprint $table){
                $table->integer('id', true);
                $table->string('client_id', 25)->nullable();
                $table->string('client_independente_id', 25)->nullable();
                $table->string('route', 50)->nullable();
                $table->string('agent', 250)->nullable();
                $table->string('versao_app', 50)->nullable();
                $table->text('post', 65535)->nullable();
                $table->text('get', 65535)->nullable();
                $table->text('headers', 65535)->nullable();
                $table->string('return_status', 50)->nullable();
                $table->text('return_object', 65535)->nullable();
                $table->string('ip', 25)->nullable();
                $table->integer('integrado')->default(0);
                $table->dateTime('created_at')->nullable();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('log_acessos');
	}

}
