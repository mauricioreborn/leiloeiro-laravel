<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddForeignKeysToClienteVendedorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cliente_vendedor', function(Blueprint $table)
		{
            $foreign = DB::table("information_schema.TABLE_CONSTRAINTS")
                ->select('*')
                ->where(['CONSTRAINT_SCHEMA' => 'souk'])
                ->get();

            if(!$foreign->where('CONSTRAINT_NAME', 'cliente_vendedor_cliente_id_foreign')->toArray()){
                $table->foreign('cliente_id')->references('id')->on('clientes')->onUpdate('RESTRICT')->onDelete('CASCADE');
            }

            if(!$foreign->where('CONSTRAINT_NAME', 'cliente_vendedor_vendedor_id_foreign')->toArray()){
                $table->foreign('vendedor_id')->references('id')->on('vendedores')->onUpdate('RESTRICT')->onDelete('CASCADE');
            }
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cliente_vendedor', function(Blueprint $table)
		{
			$table->dropForeign('cliente_vendedor_cliente_id_foreign');
			$table->dropForeign('cliente_vendedor_vendedor_id_foreign');
		});
	}
}
