<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('client_card_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->string('type');
            $table->string('invoice_id')->nullable();
            $table->string('transaction_number')->nullable();
            $table->decimal('total', 10);
            $table->decimal('tax', 10)->nullable();
            $table->decimal('commission', 10)->nullable();
            $table->decimal('advance_fee', 10)->nullable();
            $table->decimal('paid', 10)->nullable();
            $table->decimal('overpaid', 10)->nullable();
            $table->date('due_date')->nullable();
            $table->dateTimeTz('paid_at')->nullable();
            $table->dateTimeTz('authorized_at')->nullable();
            $table->dateTimeTz('expired_at')->nullable();
            $table->dateTimeTz('refunded_at')->nullable();
            $table->dateTimeTz('chargeback_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('order_id')->references('id')->on('pedidos');
            $table->foreign('status_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
