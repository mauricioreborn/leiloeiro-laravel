<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoostsOriginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boost_origins', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('boost_id');
            $table->integer('origin_code');
            $table->decimal('price', 10, 2);

            $table->foreign('boost_id')->references('id')->on('boosts');
            $table->index('origin_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boost_origins');
    }
}
