<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFefoPriceBoostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fefo_price_boosts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fefo_price_id');
            $table->unsignedInteger('client_id');
            $table->timestamp('start_date');
            $table->timestamp('finish_date');
            $table->timestamp('last_view')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fefo_price_id')->references('id')->on('fefo_prices');
            $table->foreign('client_id')->references('id')->on('clientes');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fefo_price_boost');
    }
}
