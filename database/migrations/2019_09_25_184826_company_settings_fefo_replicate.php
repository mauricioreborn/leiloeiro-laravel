<?php

use App\Company\CompanySetting;
use Illuminate\Database\Migrations\Migration;

class CompanySettingsFefoReplicate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new CompanySettingsFefoReplicateSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CompanySetting::where('setting', 'hasFefoReplicate')->delete();
    }
}
