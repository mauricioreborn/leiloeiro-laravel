<?php

use App\Company\Company;
use App\Company\CompanySetting;
use App\Company\Services\CompanySettingService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBidMinAcceptablePercentCompanySettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $companies = Company::get();

        $companies->each(function ($company) {
            (new CompanySettingService())->createBidMinAcceptablePercent($company);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CompanySetting::where('setting', 'bidMinAcceptablePercent')->delete();
    }
}
