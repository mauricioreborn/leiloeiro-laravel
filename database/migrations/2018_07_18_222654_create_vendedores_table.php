<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVendedoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('vendedores')) {
            Schema::create('vendedores', function (Blueprint $table){
                $table->increments('id');
                $table->string('nome', 200)->nullable();
                $table->string('email', 120)->index();
                $table->string('telefone', 50)->nullable();
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('vendedores');
	}

}
