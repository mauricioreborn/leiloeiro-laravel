<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClienteVendedorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('cliente_vendedor')) {
            Schema::create('cliente_vendedor', function (Blueprint $table){
                $table->increments('id');
                $table->integer('cliente_id')->unsigned()->index();
                $table->integer('vendedor_id')->unsigned()->index();
                $table->timestamps();
                $table->index(['cliente_id', 'vendedor_id']);
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('cliente_vendedor');
	}

}
