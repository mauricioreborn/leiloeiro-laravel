<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAddInfoToCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->text('cart')->nullable()->after('name');
            $table->text('bid')->nullable()->after('cart');
            $table->text('rules')->nullable()->after('bid');
            $table->text('aboult')->nullable()->after('rules');
            $table->text('policy')->nullable()->after('aboult');
            $table->text('information')->nullable()->after('policy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('cart');
            $table->dropColumn('bid');
            $table->dropColumn('rules');
            $table->dropColumn('aboult');
            $table->dropColumn('policy');
            $table->dropColumn('information');
        });
    }
}
