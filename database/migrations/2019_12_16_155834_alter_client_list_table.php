<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_lists', function (Blueprint $table) {
            $table->integer('duplicate_rows')->default(0)->after('invalid_rows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_lists', function (Blueprint $table) {
            $table->dropColumn('duplicate_rows');
        });
    }
}
