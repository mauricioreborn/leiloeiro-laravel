<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOriginalBoxAmountOnOrderTable extends Migration
{
    /**
     * Method to insert new field
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos_produtos', function ($table) {
            $table->integer('original_box_amount')->after('qtd_caixas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos_produtos', function ($table) {
            $table->dropColumn('original_box_amount');
        });
    }
}
