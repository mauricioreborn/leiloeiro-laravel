<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FaixaClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            if( !Schema::hasColumn('faixa_1', 'faixa_2','faixa_3','faixa_4')){
                $table->string('faixa_4', 3)->default(1)->after('email_verified')->nullable();
                $table->string('faixa_3', 3)->default(1)->after('email_verified')->nullable();
                $table->string('faixa_2', 3)->default(1)->after('email_verified')->nullable();
                $table->string('faixa_1', 3)->default(1)->after('email_verified')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropColumn('faixa_1');
            $table->dropColumn('faixa_2');
            $table->dropColumn('faixa_3');
            $table->dropColumn('faixa_4');
        });
    }
}
