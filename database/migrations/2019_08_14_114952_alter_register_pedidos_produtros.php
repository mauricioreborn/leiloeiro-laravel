<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRegisterPedidosProdutros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('pedidos_produtos')) {
            DB::table('pedidos_produtos')->where('status_final_descricao', '=', 'CANCELED')->update(
                ['status_final_descricao' => 'CANCELADO']
            );
        }
    }
}
