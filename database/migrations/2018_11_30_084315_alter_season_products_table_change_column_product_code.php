<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSeasonProductsTableChangeColumnProductCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('season_products', function (Blueprint $table) {
            $table->integer('product_code')->length(11)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('season_products', function (Blueprint $table) {
            $table->integer('product_code')->length(10)->change();
        });
    }
}
