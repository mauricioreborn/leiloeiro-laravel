<?php

use Illuminate\Database\Migrations\Migration;
use App\Company\Company;
use App\Auth\User;

class CreateSoukBootTirolez extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tirolez = Company::find(5);

        if($tirolez !== null) {
            $tirolez->users()->create([
                'name' => 'soukBot',
                'email' => 'soukbot.tirolez@souk.com.br',
                'password' => md5('S0uk@123'),
                'root' => 1,
                'price' => 0,
                'featured' => 1,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::where('email', 'soukbot.tirolez@souk.com.br')->delete();
    }
}
