<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientCardTableChangeDisplayNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_cards', function (Blueprint $table) {
            $table->string('display_number')->change();
            $table->string('payment_method_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_cards', function (Blueprint $table) {
            $table->integer('display_number')->change();
            $table->integer('payment_method_id')->change();
        });
    }
}
