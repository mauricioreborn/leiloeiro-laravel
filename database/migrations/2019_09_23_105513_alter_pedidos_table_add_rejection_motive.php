<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPedidosTableAddRejectionMotive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->unsignedInteger('rejection_motive_id')->nullable()->after('status_id');
            $table->foreign('rejection_motive_id')->references('id')->on('rejection_motive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropForeign('pedidos_rejection_motive_id_foreign');
            $table->dropColumn(['rejection_motive_id']);
        });
    }
}
