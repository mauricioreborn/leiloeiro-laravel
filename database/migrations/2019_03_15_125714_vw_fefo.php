<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VwFefo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_fefo;");

        DB::statement("CREATE 
                        VIEW `vw_fefo` AS
                            SELECT 
                                `ff`.`date` AS `data`,
                                `ff`.`semanas` AS `semanas_venc`,
                                `ff`.`volume` AS `volume`,
                                `ff`.`valor_atual` AS `valor_atual`,
                                `ff`.`valor_cheio` AS `valor_cheio`,
                                `ff`.`valor_min` AS `valor_min`,
                                `ff`.`cd_empresa` AS `cd_empresa`,
                                `ff`.`codigo_produto` AS `codigo_produto`,
                                `ff`.`faixa_fefo` AS `faixa_fefo`,
                                `ff`.`id` AS `id_fefo`,
                                NOW() AS `data_carga`,
                                `origem`.`cod_origem` AS `cod_origem`,
                                `ff`.`destaque` AS `destaque`
                            FROM
                                (`fefo` `ff`
                                JOIN `leadtime` `origem` ON ((`origem`.`cod_origem` = `ff`.`cod_origem`)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_fefo;");
    }
}
