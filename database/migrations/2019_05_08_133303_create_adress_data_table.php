<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdressDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes_independentes', function (Blueprint $table) {
            $table->string('zipcode')->nullable();
            $table->integer('address_number')->nullable();
            $table->string('neighborhood')->nullable();
            $table->string('address_complement')->nullable();
            $table->string('social_reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adress_data');
    }
}
