<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFefoPriceBoostsDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fefo_price_boosts', function (Blueprint $table) {
            $table->dateTime('start_date')->nullable()->default(null)->change();
            $table->dateTime('finish_date')->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fefo_price_boosts', function (Blueprint $table) {
            $table->timestamp('start_date')->change();
            $table->timestamp('finish_date')->change();
        });
    }
}
