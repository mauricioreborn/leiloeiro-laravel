<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesIndependentesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('clientes_independentes')) {
            Schema::create('clientes_independentes', function (Blueprint $table){
                $table->increments('id');
                $table->string('cnpj', 25)->index('clientes_cnpj_index');
                $table->string('password');
                $table->string('nome');
                $table->string('rede')->nullable();
                $table->boolean('status')->default(1)->index('clientes_status_index');
                $table->decimal('saldo', 10)->nullable();
                $table->text('endereco', 65535)->nullable();
                $table->string('uf', 2)->nullable();
                $table->string('municipio', 120)->nullable();
                $table->string('email', 200)->nullable();
                $table->string('telefone', 200)->nullable();
                $table->integer('integrado')->default(0);
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('clientes_independentes');
	}

}
