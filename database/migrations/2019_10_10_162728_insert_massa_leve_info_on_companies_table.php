<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Company\Company;

class InsertMassaLeveInfoOnCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seara = Company::find(1);
        if (isset($seara->id)) {
            $massaLeve = Company::find(3);

            if(isset($massaLeve->id)) {
                $massaLeve->update([
                    'cart' => $seara->cart,
                    'bid' => $seara->bid,
                    'rules' => $seara->rules,
                    'aboult' => $seara->aboult
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
