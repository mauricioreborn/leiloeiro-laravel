<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('produtos')) {
            Schema::create('produtos', function (Blueprint $table){
                $table->integer('id', true);
                $table->string('codigo', 50)->index('codigo');
                $table->integer('divisao_empresarial')->nullable();
                $table->string('nome');
                $table->string('foto');
                $table->integer('foto_check')->default(0);
                $table->string('area');
                $table->string('familia');
                $table->string('marca');
                $table->string('submarca');
                $table->string('medida');
                $table->float('peso_caixa', 10);
                $table->string('gramatura', 50);
                $table->string('unidades_caixa', 50);
                $table->string('exibicao_app', 50);
                $table->timestamps();
                $table->softDeletes();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('produtos');
	}

}
