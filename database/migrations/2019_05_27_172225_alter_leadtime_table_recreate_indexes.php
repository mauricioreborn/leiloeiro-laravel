<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLeadtimeTableRecreateIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
            DB::statement("ALTER TABLE leadtime DROP INDEX origem");

            Schema::table('leadtime', function (Blueprint $table) {
                $table->dropIndex('cnpj');
                $table->dropIndex('cod_origem');
            });

            Schema::table('leadtime', function (Blueprint $table) {
                $table->index('cnpj');
                $table->index('cod_origem');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leadtime', function (Blueprint $table) {
            DB::statement("CREATE INDEX origem ON leadtime (cod_origem);");
            DB::statement("CREATE INDEX cod_origem ON leadtime (cod_origem);");
            DB::statement("CREATE INDEX cnpj ON leadtime (cnpj);");
        });
    }
}
