<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('campaign_id')
                ->foreign('campaign_clients_campaigns')
                ->references('id')
                ->on('campaigns');
            $table->unsignedInteger('client_id')
                ->foreign('campaign_clients_clients')
                ->references('id')
                ->on('clients');
            $table->unsignedInteger('notification_id')
                ->foreign('campaign_clients_notifications')
                ->references('id')
                ->on('notifications')
                ->nullable();
            $table->date('date');
            $table->dateTime('send_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_clients');
    }
}
