<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLancesAddForeignKeyOnBidRejectionStatusId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('lances')->where('bid_rejection_status_id', 0)->update(['bid_rejection_status_id' => null]);

        Schema::table('lances', function (Blueprint $table) {
            $table->integer('bid_rejection_status_id')
                  ->unsigned()
                  ->nullable()
                  ->default(null)
                  ->change();

            $table->index('bid_rejection_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lances', function (Blueprint $table) {
            $table->index(['state']);
        });

        DB::table('lances')->whereNull('bid_rejection_status_id')->update(['bid_rejection_status_id' => 0]);
    }
}
