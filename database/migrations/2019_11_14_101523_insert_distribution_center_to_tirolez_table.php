<?php

use Illuminate\Database\Migrations\Migration;
use App\DistributionCenter\DistributionCenter;
use App\Company\Company;

class InsertDistributionCenterToTirolezTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $company = Company::find(5);

        if (isset($company->id)) {
            $distribution = DistributionCenter::where([
                'company_id' => $company->id
            ])->first();

            if(!isset($distribution->id)) {
                DistributionCenter::create([
                    'company_id' => $company->id,
                    'code' => 666,
                    'name' => 'CD SAO PAULO',
                    'state' => 'SP',
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $company = Company::find(5);

        if (isset($company->id)) {
            DistributionCenter::where([
                'company_id' => $company->id
            ])->forceDelete();
        }
    }
}