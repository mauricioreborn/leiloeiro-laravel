<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarrinhoProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        if (!Schema::hasTable('carrinho_produtos')) {

            Schema::create('carrinho_produtos', function(Blueprint $table)
            {
                $table->integer('id', true);
                $table->integer('carrinho_id');
                $table->integer('cod_produto');
                $table->integer('cod_origem');
                $table->integer('semanas');
                $table->integer('qtd_caixas');
                $table->float('total_kg', 10);
                $table->decimal('valor_kg', 10);
                $table->decimal('valor', 10);
                $table->integer('status')->default(1);
                $table->dateTime('created_at')->nullable();
                $table->softDeletes();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //Schema::drop('carrinho_produtos');
	}

}
