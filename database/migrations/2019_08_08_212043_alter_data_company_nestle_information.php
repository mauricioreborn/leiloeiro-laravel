<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AlterDataCompanyNestleInformation extends Migration
{

    protected $old = "<h2>Produtos Oferecidos</h2> A Souk como um marketplace se caracteriza como um intermediador de vendas B2B estabelecendo uma conexão entre indústrias e varejo. Todos os produtos da plataforma são oferecidos diretamente e exclusivamente pelas indústrias em caráter de oferta especial, em função da data de validade dos produtos. <hr> <h2>Logística e Entrega</h2> Seus produtos serão entregues de acordo com a disponibilidade logística das indústrias. São as mesmas datas que você já conhece! Sendo assim, você deverá receber seus produtos nos dias que normalmente recebe suas compras quando a venda é realizada por um vendedor ou representante da indústria. <hr> <h2>Pregão Eletrônico - Lances </h2> Os lances de preços são avaliados por times de gestores das indústrias que analisam as intenções de compra do pregão eletrônico antes de aceitar ou rejeitar lance. <br> A Souk prioriza os maiores preços e não o volume para enviar a listagem de lances para as indústrias.";

    protected $new = "<h2>Produtos Oferecidos</h2> A Souk como um marketplace se caracteriza como um intermediador de vendas B2B estabelecendo uma conexão entre indústrias e varejo. Todos os produtos da plataforma são oferecidos diretamente e exclusivamente pelas indústrias em caráter de oferta especial, em função da data de validade dos produtos. <hr> <h2>Logística e Entrega</h2> Seus produtos serão entregues de acordo com a disponibilidade logística das indústrias. Se você já tiver relacionamento com a indústria as datas serão as mesmas que você já conhece, se não você pode conferir a sua grade logística acessando Menu > Sobre a grade de embarque. <hr> <h2>Pregão Eletrônico - Lances </h2> Os lances de preços são avaliados por times de gestores das indústrias que analisam as intenções de compra do pregão eletrônico antes de aceitar ou rejeitar lance. <br> A Souk prioriza os maiores preços e não o volume para enviar a listagem de lances para as indústrias.";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('companies')){
            DB::table('companies')->where('id', '=', 4)->update(['information' => $this->new, 'updated_at' => Carbon::now()]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('companies')){
            DB::table('companies')->where('id', '=', 4)->update(['information' => $this->old, 'updated_at' => Carbon::now()]);
        }
    }
}
