<?php

use Illuminate\Database\Migrations\Migration;
use App\Products\Product;

class AddPriceIncreaseProductsTirolez extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Product::where('company_id', 5)->update(['price_increase' => 7.00]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Product::where('company_id', 5)->update(['price_increase' => 0]);
    }
}
