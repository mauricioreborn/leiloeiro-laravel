<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VwPedido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_pedido;");

        DB::statement("CREATE 
                    VIEW `vw_pedido` AS
                        SELECT 
                            `pp`.`semanas` AS `semanas_vencimento`,
                            `pp`.`qtd_caixas` AS `qtd_caixas`,
                            `pp`.`total_kg` AS `total_kg`,
                            `pp`.`valor_kg` AS `valor_kg`,
                            `pp`.`valor` AS `valor`,
                            `pp`.`data_embarque` AS `data_embarque`,
                            `pp`.`data_entrega` AS `data_entrega`,
                            `pp`.`cd_empresa` AS `cd_empresa`,
                            `pp`.`status_final_id` AS `status_final_id`,
                            `pp`.`status_final_descricao` AS `status_final_descricao`,
                            `pp`.`CD_MOTIVO_CCL_PEDIDO` AS `cd_motico_ccl_pedido`,
                            `pp`.`DS_MOTIVO_CCL_PEDIDO` AS `ds_motico_ccl_pedido`,
                            `pp`.`status` AS `status`,
                            `p`.`lance_id` AS `lance_id`,
                            (CASE
                                WHEN ISNULL(`p`.`lance_id`) THEN 'Carrinho'
                                ELSE 'Lance'
                            END) AS `lance_ou_carrinho`,
                            `p`.`status` AS `status_pedido`,
                            `p`.`motivo_cancelamento` AS `motivo_cancelamento_pedido`,
                            `p`.`aceite_parcial` AS `aceite_parcial_pedido`,
                            SUBSTR(`pp`.`cd_faixa_fefo`,
                                (LENGTH(`pp`.`cd_faixa_fefo`) - 2),
                                LENGTH(`pp`.`cd_faixa_fefo`)) AS `cd_faixa_fefo`,
                            NOW() AS `data_carga`,
                            `p`.`id` AS `pedido_id`,
                            1 AS `qtd`,
                            `pp`.`cod_origem` AS `cod_origem`,
                            `prod`.`codigo` AS `codigo_produto`,
                            `p`.`company_id` AS `company_id`,
                            `p`.`data` AS `data_pedido`,
                            `p`.`id_cliente` AS `id_cliente`
                        FROM
                            (((`pedidos_produtos` `pp`
                            JOIN `produtos` `prod` ON ((`pp`.`produto_id` = `prod`.`codigo`)))
                            JOIN `leadtime` `origem` ON ((`origem`.`cod_origem` = `pp`.`cod_origem`)))
                            JOIN `pedidos` `p` ON ((`pp`.`pedido_id` = `p`.`id`)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_pedido;");
    }
}
