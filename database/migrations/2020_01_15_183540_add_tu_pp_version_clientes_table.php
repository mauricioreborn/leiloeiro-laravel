<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTuPpVersionClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->string('registration_ip')->after('confirm_email')->nullable();
            $table->string('terms_of_use_version')->after('registration_ip')->nullable();
            $table->string('privacy_policy_version')->after('terms_of_use_version')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropColumn('registration_ip');
            $table->dropColumn('terms_of_use_version');
            $table->dropColumn('privacy_policy_version');
        });
    }
}
