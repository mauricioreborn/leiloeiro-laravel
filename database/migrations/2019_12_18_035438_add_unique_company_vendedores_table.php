<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueCompanyVendedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendedores', function (Blueprint $table) {
            $schema = Schema::getConnection()->getDoctrineSchemaManager()->listTableDetails($table->getTable());

            if (array_key_exists('vendedores_email_unique', $schema->getIndexes())) {
                $table->dropUnique(['email']);
            }

            if (!array_key_exists('vendedores_email_company_id_unique', $schema->getIndexes())) {
                $table->unique(['email', 'company_id']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendedores', function (Blueprint $table) {
            $table->dropUnique(['email', 'company_id']);
            $table->unique(['email']);
        });
    }
}
