<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFefoLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('fefo_log')) {
            Schema::create('fefo_log', function (Blueprint $table){
                $table->integer('id', true);
                $table->integer('semanas');
                $table->integer('cod_origem');
                $table->integer('codigo_produto');
                $table->float('valor', 10);
                $table->integer('id_user');
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('fefo_log');
	}

}
