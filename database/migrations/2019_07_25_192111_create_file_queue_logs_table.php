<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileQueueLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_queue_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('file_queue_id');
            $table->longText('messages');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('file_queue_id')->references('id')->on('file_queues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_queue_logs');
    }
}
