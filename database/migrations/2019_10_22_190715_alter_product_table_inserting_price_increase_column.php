<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Products\Product;

class AlterProductTableInsertingPriceIncreaseColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produtos', function ($table) {
            if (!Schema::hasColumn('produtos', 'price_increase')) {
                $table->boolean('price_increase')->after('exibicao_app')->default(false);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produtos', function ($table) {
            if (Schema::hasColumn('produtos', 'price_increase')) {
                $table->dropColumn('price_increase');
            }
        });
    }


}
