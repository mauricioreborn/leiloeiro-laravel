<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNotificationTableFixColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('notification_origin', 'notification_origins');

        Schema::table('notification_origins', function (Blueprint $table) {
            $table->string('day_period')->nullable();
            $table->date('scheduled_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('notification_origins', 'notification_origin');

        Schema::table('notification_origin', function (Blueprint $table) {
            $table->dropColumn(['day_period', 'scheduled_at', 'created_at', 'updated_at']);
            $table->dropSoftDeletes();
        });
    }
}
