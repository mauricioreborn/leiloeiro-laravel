<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeTrackProductTypologies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {

            DB::table('product_typologies')
                ->where(['company_id' => 1])
                ->whereIn('state', ["MT", "MS"])
                ->whereIn('product_code', function ($query) {
                    $query->select('codigo')
                        ->from('produtos')
                        ->whereRaw('company_id = 1')
                        ->whereRaw('divisao_empresarial = 15');
                })
                ->update([
                    'track_1' => 2,
                    'track_2' => 2,
                    'track_3' => 2,
                    'track_4' => 2,
                    'updated_at' => now()
                ]);
        }
    }
}
