<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertIntoClientCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('client_companies')->truncate();

        DB::statement("INSERT INTO client_companies (client_id, company_id, status, typology_code, typology, track_1,
        track_2, track_3, track_4, city, state, address, min_order , account_balance, created_at, updated_at)
        SELECT id , 1, status, typology_code, tipo, faixa_1, faixa_2, faixa_3, faixa_4, municipio, uf, endereco,
        pedido_minimo, saldo, NOW(), NOW() FROM clientes;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
