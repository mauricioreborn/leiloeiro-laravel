<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRangePriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('range_prices', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->change();
            $table->foreign('company_id')->references('id')->on('companies')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('range_prices', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
        });
    }
}
