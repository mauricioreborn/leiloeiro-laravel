<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DisablePrePaidTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('company_payments')){
            DB::table('company_payments')->where(['payment_id' => 1])->update(['deleted_at' => now()]);
        }
    }

    public function down()
    {
        if(Schema::hasTable('company_payments')){
            DB::table('company_payments')->where(['payment_id' => 1])->update(['deleted_at' => null]);
        }
    }
}
