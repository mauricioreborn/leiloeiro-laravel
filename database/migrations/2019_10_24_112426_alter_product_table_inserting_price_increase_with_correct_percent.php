<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Products\Product;

class AlterProductTableInsertingPriceIncreaseWithCorrectPercent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Product::whereIn('codigo', $this->getInNatura())->update([
            'price_increase' => 6.00
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Product::whereIn('codigo', $this->getInNatura())->update([
            'price_increase' => true
        ]);
    }

    public function getInNatura()
    {
        return [
            201509,
            201619,
            201629,
            201639,
            727466,
            798380,
            798398,
            798371,
            208649,
            208659,
            978990,
            958557,
            958565,
            958573,
            958581,
            958590,
            958603,
            958611,
            14290,
            14567,
            15105,
            15423,
            18163,
            18244,
            40237,
            197696,
            197718,
            197726,
            197742,
            220359,
            228719,
            229848,
            229859,
            229879,
            229889,
            229909,
            229929,
            229939,
            229959,
            229979,
            792306,
            926922,
            929883,
            952117,
            985503,
            986445,
            220219,
            220229,
            220239,
            220249,
            221069,
            221079,
            685658,
            685666,
            685674,
            685690,
            686166,
            686549,
            928313,
            205099,
            205108,
            797235
        ];
    }
}
