<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PushPorCd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_origin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('origin_code')->index('origin_code');
            $table->string('subject')->nullable();
            $table->string('message');
            $table->timestamp('processed_at')->nullable();
            $table->integer('total_sent')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_origin');
    }
}
