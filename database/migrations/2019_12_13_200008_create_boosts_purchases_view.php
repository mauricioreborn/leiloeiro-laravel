<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoostsPurchasesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_boosts_purchases;");

        DB::statement("create view vw_boosts_purchases as
            select `x`.`purchase_id`   AS `purchase_id`,
                   `x`.`product_code`  AS `product_code`,
                   `x`.`weeks`         AS `weeks`,
                   `x`.`data`          AS `data`,
                   `x`.`status_id`     AS `status_id`,
                   `x`.`company_id`    AS `company_id`,
                   `x`.`typology_code` AS `typology_code`,
                   `x`.`cnpj`          AS `cnpj`,
                   `x`.`purchase_type` AS `purchase_type`
            from ((select `l`.`id`             AS `purchase_id`,
                          `l`.`cod_produto`    AS `product_code`,
                          `l`.`semanas`        AS `weeks`,
                          `l`.`data`           AS `data`,
                          `l`.`status_id`      AS `status_id`,
                          `l`.`company_id`     AS `company_id`,
                          `cc`.`typology_code` AS `typology_code`,
                          `c`.`cnpj`           AS `cnpj`,
                          'lances'             AS `purchase_type`
                   from ((`lances` `l` join `clientes` `c` on ((`c`.`id` = `l`.`id_cliente`)))
                            join `client_companies` `cc` on ((`cc`.`client_id` = `c`.`id`)))
                   where (`cc`.`company_id` = `l`.`company_id`))
                  union all
                  (select `pp`.`pedido_id`     AS `purchase_id`,
                          `pp`.`produto_id`    AS `product_code`,
                          `pp`.`semanas`       AS `weeks`,
                          `p`.`data`           AS `data`,
                          `pp`.`status_id`     AS `status_id`,
                          `p`.`company_id`     AS `company_id`,
                          `cc`.`typology_code` AS `typology_code`,
                          `c`.`cnpj`           AS `cnpj`,
                          'carrinho'           AS `purchase_type`
                   from (((`pedidos_produtos` `pp` join `pedidos` `p` on ((`pp`.`pedido_id` = `p`.`id`))) join `clientes` `c` on ((`c`.`id` = `p`.`id_cliente`)))
                            left join `client_companies` `cc` on ((`cc`.`client_id` = `c`.`id`)))
                   where ((`cc`.`company_id` = `p`.`company_id`) and isnull(`p`.`lance_id`)))) `x`
            order by `x`.`purchase_id` desc;
            
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_boosts_purchases;");
    }
}
