<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogEmailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('log_emails')) {
            Schema::create('log_emails', function (Blueprint $table){
                $table->integer('id', true);
                $table->string('tela');
                $table->string('assunto');
                $table->text('mensagem');
                $table->string('md5_mensagem', 100)->nullable();
                $table->text('emails');
                $table->timestamps();
                $table->softDeletes();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('log_emails');
	}

}
