<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeReadFlagToTimestampOnLogEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_emails', function (Blueprint $table) {
            $table->dateTime('read_at')->nullable()->after('read');
        });

        DB::statement('UPDATE log_emails SET `read_at`=`updated_at` WHERE `read`=1');

        Schema::table('log_emails', function (Blueprint $table) {
            $table->dropColumn('read');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_emails', function (Blueprint $table) {
            $table->boolean('read')->after('emails')->default(false);
        });

        DB::statement('UPDATE log_emails SET `read`=1 WHERE `read_at` IS NOT NULL');

        Schema::table('log_emails', function (Blueprint $table) {
            $table->dropColumn('read_at');
        });
    }
}
