<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientCompanyPaymentsTableAddUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_company_payments', function (Blueprint $table) {
            $table->unique(['client_id', 'company_payment_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_company_payments', function (Blueprint $table) {
            $table->dropUnique(['client_id', 'company_payment_id']);
        });
    }
}
