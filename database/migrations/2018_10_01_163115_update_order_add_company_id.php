<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderAddCompanyId extends Migration
{
    public function up()
    {
        Schema::table('pedidos', function($table) {
            $table->unsignedInteger('company_id')->foreign('company_id')->references('id')->on('companies')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function($table) {
            $table->dropColumn('company_id');
        });
    }
}
