<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLeadtimeTableInsertingLogisticInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leadtime', function (Blueprint $table) {
            $table->string('logistic_information')->after('opera_domingo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leadtime', function ($table) {
            if (Schema::hasColumn('file_queues', 'logistic_information')) {
                $table->dropColumn('logistic_information');
            }
        });
    }
}
