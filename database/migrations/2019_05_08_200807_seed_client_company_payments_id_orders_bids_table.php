<?php

use App\Bid\Bid;
use App\Order\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedClientCompanyPaymentsIdOrdersBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new PopulatePaymentInfoSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Order::whereNotNull('client_company_payment_id')->update(['client_company_payment_id' =>  null]);
        Bid::whereNotNull('client_company_payment_id')->update(['client_company_payment_id' =>  null]);
    }
}
