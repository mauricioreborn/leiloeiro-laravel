<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRangePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('range_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_code')->index('product_code');
            $table->integer('origin_code')->index('origin_code');
            $table->integer('track')->index('track');
            $table->float('approval_percentage');
            $table->float('reprove_percentage');
            $table->integer('company_id')
                ->references('id')->on('companies')->index('company_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('range_prices');
    }
}
