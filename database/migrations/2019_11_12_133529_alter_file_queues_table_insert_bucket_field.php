<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFileQueuesTableInsertBucketField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('file_queues', function (Blueprint $table) {
            $table->string('bucket')->after('original_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('file_queues', function ($table) {
            if (Schema::hasColumn('file_queues', 'bucket')) {
                $table->dropColumn('bucket');
            }
        });
    }
}
