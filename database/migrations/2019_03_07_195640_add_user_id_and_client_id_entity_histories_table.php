<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdAndClientIdEntityHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entity_histories', function (Blueprint $table) {
            $table->unsignedInteger('client_id')->after('id')->nullable();
            $table->integer('user_id')->after('id')->nullable();

            $table->foreign('client_id')->references('id')->on('clientes');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_histories', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->dropForeign(['user_id']);

            $table->dropColumn('client_id');
            $table->dropColumn('user_id');
        });
    }
}
