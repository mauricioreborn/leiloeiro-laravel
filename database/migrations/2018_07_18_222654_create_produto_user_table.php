<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutoUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('produto_user')) {
            Schema::create('produto_user', function (Blueprint $table){
                $table->increments('id');
                $table->integer('produto_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->timestamps();
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('produto_user');
	}

}
