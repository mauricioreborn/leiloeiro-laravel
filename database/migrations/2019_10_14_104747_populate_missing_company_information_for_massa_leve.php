<?php
use App\Company\Company;
use Illuminate\Database\Migrations\Migration;

class PopulateMissingCompanyInformationForMassaLeve extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seara = Company::find(1);
        $massaLeve = Company::find(3);

        if($seara !== null && $massaLeve !== null) {
            $massaLeve->update([
                'cart' => $seara->cart,
                'bid' => $seara->bid,
                'rules' => $seara->rules,
                'aboult' => $seara->aboult,
                'policy' => $seara->policy,
                'information' => $seara->information,
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
