<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VwLance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_lance;");

        DB::statement("CREATE 
                        VIEW `vw_lance` AS
                            SELECT 
                                `lan`.`semanas` AS `semanas_venc`,
                                `pr`.`medida` AS `medida_produto`,
                                `pr`.`unidades_caixa` AS `qtde_por_caixa`,
                                `pr`.`peso_caixa` AS `peso_caixa`,
                                `lan`.`qtd_caixas` AS `qtd_caixas`,
                                `lan`.`total_kg` AS `total_kg_lance`,
                                `lan`.`valor_kg` AS `valor_kg`,
                                `lan`.`valor` AS `valor_total_lance`,
                                `lan`.`aceite_parcial` AS `aceite_parcial`,
                                `lan`.`status` AS `status`,
                                (CASE
                                    WHEN (`lan`.`status` = 1) THEN 'Aberto'
                                    WHEN (`lan`.`status` = 0) THEN 'Cancelado'
                                    WHEN (`lan`.`status` = 2) THEN 'Aceito'
                                END) AS `descricao_status`,
                                `lan`.`data_aceite` AS `data_aceite`,
                                `lan`.`data` AS `data_lance`,
                                NOW() AS `data_carga`,
                                `lan`.`id` AS `id_lance`,
                                `lan`.`id_cliente` AS `id_cliente`,
                                `lan`.`cod_produto` AS `cod_produto`,
                                `lan`.`company_id` AS `company_id`,
                                `lan`.`cod_origem` AS `cod_origem`
                            FROM
                                ((`lances` `lan`
                                JOIN `clientes` `cl` ON ((`lan`.`id_cliente` = `cl`.`id`)))
                                JOIN `produtos` `pr` ON ((`lan`.`cod_produto` = `pr`.`codigo`)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_lance;");
    }
}
