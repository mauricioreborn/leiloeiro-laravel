<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoostsClientListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boosts_client_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('boost_id');
            $table->unsignedInteger('client_list_id');

            $table->foreign('boost_id')->references('id')->on('boosts');
            $table->foreign('client_list_id')->references('id')->on('client_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boosts_client_lists');
    }
}
