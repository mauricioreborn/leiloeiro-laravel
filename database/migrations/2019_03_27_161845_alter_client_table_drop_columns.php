<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientTableDropColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            // $table->dropColumn('saldo');
            // $table->dropColumn('pedido_minimo');
            // $table->dropColumn('endereco');
            // $table->dropColumn('uf');
            // $table->dropColumn('municipio');
            // $table->dropColumn('faixa_1');
            // $table->dropColumn('faixa_2');
            // $table->dropColumn('faixa_3');
            // $table->dropColumn('faixa_4');
            // $table->dropColumn('status');
            // $table->dropColumn('tipo');
            // $table->dropColumn('typology_code');
            // $table->dropColumn('rede');
            // $table->dropColumn('canal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
