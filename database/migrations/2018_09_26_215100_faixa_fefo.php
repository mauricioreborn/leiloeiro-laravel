<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FaixaFefo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fefo', function (Blueprint $table) {
            $table->string('faixa_fefo', 3)->after('cd_faixa_fefo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fefo', function (Blueprint $table) {
            $table->dropColumn('faixa_fefo');
        });
    }
}
