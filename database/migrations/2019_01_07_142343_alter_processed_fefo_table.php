<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterProcessedFefoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processed_fefos', function (Blueprint $table) {
            $table->renameColumn('schedule_at', 'scheduled_at');
        });

        Schema::table('processed_fefos', function (Blueprint $table) {
            $table->dateTime('scheduled_at')->default(null)->change();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_fefos', function (Blueprint $table) {
            $table->renameColumn('scheduled_at', 'schedule_at');
        });

        Schema::table('processed_fefos', function (Blueprint $table) {
            $table->dateTime('schedule_at')->useCurrent()->change();
        });
    }
}
