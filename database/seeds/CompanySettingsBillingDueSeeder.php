<?php

use App\Company\Company;
use App\Company\Services\CompanySettingService;
use Illuminate\Database\Seeder;

class CompanySettingsBillingDueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = new CompanySettingService();

        $companies = Company::withTrashed()->get();

        $companies->each(function ($company) use ($service) {
            $billingDueDay = $company->name !== 'Nestlé Iogurtes' ? 18 : 28;
            $service->createBillingDueDay($company, $billingDueDay);
        });
    }
}