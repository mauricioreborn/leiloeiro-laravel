<?php

use App\Auth\User;
use App\Bid\Bid;
use App\Client\Client;
use App\Client\Seller;
use App\Company\Company;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Products\Product;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyName = 'Massa leve';
        $email = 'massaleve@souk.com.br';
        $password = '12345';

        $company = Company::where('name', 'Massa leve')->first();

        if (!$company) {
            $company = factory(Company::class)->create([
                'name' => 'Massa leve',
                'cart' => __('Mobile/login.texto_carrinho'),
                'bid' => __('Mobile/login.texto_lance'),
                'rules' => __('Mobile/login.texto_regras'),
                'aboult' => __('Mobile/login.texto_sobre'),
                'policy' => __('Mobile/login.texto_politica'),
                'information' => __('Mobile/login.texto_informacoes'),
            ]);
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            $user = User::create([
                'name' => $companyName,
                'email' => $email,
                'password' => $password,
                'root' => 1,
                'company_id' => $company->id,
            ]);
        }

        $product = factory(Product::class)->create([
            'company_id' => $company->id,
        ]);

        $client = factory(Client::class)->create([
            'min_order' => 5
        ]);

        $seller = factory(Seller::class)->create([
            'company_id' => $company->id
        ]);

        $client->seller()->save($seller);
        $client->companies()->sync($company);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1, 688, $company->id);

        $product = factory(Product::class)->create([
            'weight' => 5,
            'company_id' => $company->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => today()->toDateString(),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
            'company_id' => $company->id
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status' => '2',
            'company_id' => $company->id,
        ]);

        $order = factory(Order::class)->create([
            'status' => '2',
            'client_id' => $client->id,
            'bid_id' => $bid->id,
            'company_id' => $company->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'product_id' => $fefo->product_code,
            'origin_code' => $fefo->leadtime_code,
            'status' => '1',
            'final_status_id' => '3',
        ]);

        $order = factory(Order::class)->create([
            'status' => '2',
            'client_id' => $client->id,
            'company_id' => $company->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'product_id' => $fefo->product_code,
            'origin_code' => $fefo->leadtime_code,
            'status' => '1',
            'final_status_id' => '3',
        ]);
    }

    /**
     * create LeadTime
     *
     * @param  string $cnpj      CNPJ
     * @param  string $name      Name
     * @param  int    $operate   Operate
     * @param  string $code      code
     * @param  string $companyId Company Id
     *
     * @return App\LeadTime\LeadTime
     */
    protected function createLeadTime(string $cnpj, string $name, int $operate, $code, $companyId)
    {
        $params = [
            'name' => $name,
            'cnpj' => $cnpj,
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => $operate,
            'tuesday' => $operate,
            'wednesday' => $operate,
            'thursday' => $operate,
            'friday' => $operate,
            'saturday' => $operate,
            'sunday' => $operate,
            'operates_saturday' => $operate,
            'operates_sunday' => $operate,
            'company_id' => $companyId ?? $this->company->id
        ];

        if ($code) {
            $params['code'] = $code;
        }

        return factory(LeadTime::class)->create($params);
    }
}
