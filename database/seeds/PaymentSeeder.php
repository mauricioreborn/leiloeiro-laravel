<?php

use Illuminate\Database\Seeder;

use App\Payment\Payment;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $creditCard = Payment::firstOrNew([
            'type' => Payment::CREDIT_CARD
        ]);
        $creditCard->name = 'Cartão de crédito';
        $creditCard->save();

        $billet = Payment::firstOrNew([
            'type' => Payment::BILLET
        ]);
        $billet->name = 'Boleto pŕe-pago';
        $billet->save();

        $companyCredit = Payment::firstOrNew([
            'type' => Payment::COMPANY_CREDIT
        ]);
        $companyCredit->name = 'Boleto pós-pago';
        $companyCredit->save();
    }
}
