<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AddSearaOnCompaniesTable extends Seeder
{
    public function run()
    {
        $seara = DB::table('companies')->where(['name' => 'seara'])->first();

        if (!isset($seara->id)) {
            DB::table('companies')->insert([
                'name' => 'seara',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}