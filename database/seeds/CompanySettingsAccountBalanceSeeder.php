<?php

use App\Company\Company;
use App\Company\Services\CompanySettingService;
use Illuminate\Database\Seeder;

class CompanySettingsAccountBalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = new CompanySettingService();

        $companies = Company::withTrashed()->get();

        $companies->each(function ($company) use ($service) {
            $value = ($company->id == 4) ?? false;

            $service->createReduceAccountBalanceCheckoutCart($company, $value);
            $service->createReduceAccountBalanceApproveBid($company, $value);

            $service->createAddAccountBalanceCancelOrder($company, $value);
        });
    }
}
