<?php

use App\Bid\Bid;
use App\Client\ClientCompanyPayment;
use App\Company\CompanyPayment;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Order\Order;
use App\Payment\Payment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PopulateBidStatusBackwardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusReference = [
            0 => Status::type(Status::CANCELED)->id,
            1 => Status::type(Status::PENDING)->id,
            2 => Status::type(Status::APPROVED)->id,
        ];

        foreach($statusReference as $status => $status_id) {
            DB::table('lances')->where('status', $status)->update(['status_id' => $status_id]);
        }

        $rejectionReference = [
            1 => RejectionMotive::type(RejectionMotive::EXPIRED)->id,
            2 => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id,
            3 => RejectionMotive::type(RejectionMotive::REMOVED_BY_USER)->id,
        ];

        foreach($rejectionReference as $bid_rejection_status_id => $rejection_motives_id) {
            DB::table('lances')->where('bid_rejection_status_id', $bid_rejection_status_id)
                ->update(['rejection_motives_id' => $rejection_motives_id]);
        }

        DB::table('lances')->where('bid_rejection_status_id', 0)
            ->where('status', 0)
            ->update(['rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id]);

        DB::table('lances')->whereNull('bid_rejection_status_id')
            ->where('status', 0)
            ->update(['rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id]);
    }
}
