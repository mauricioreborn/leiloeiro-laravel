<?php

use Illuminate\Database\Seeder;
use App\Auth\User;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Payment\Payment;

class TirolezSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($companyName, $email)
    {
        $password = '12345';

        $company = Company::where('name', $companyName)->first();

        if (!isset($company->id)) {
            $company = Company::create([
                'name' => $companyName,
                'code' => 5,
                'cart' => __('Mobile/login.texto_carrinho'),
                'bid' => __('Mobile/login.texto_lance'),
                'rules' => __('Mobile/login.texto_regras'),
                'aboult' => __('Mobile/login.texto_sobre'),
                'policy' => __('Mobile/login.texto_politica'),
                'information' => __('Mobile/login.texto_informacoes'),
            ]);
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            User::create([
                'name' => $companyName,
                'email' => $email,
                'password' => $password,
                'root' => 1,
                'company_id' => $company->id,
            ]);
        }

        $companyPayment = CompanyPayment::where([
            'company_id' => $company->id
        ])->first();

        if (!isset($companyPayment->id)) {
            $payment = Payment::where([
                'type' => 'company_credit'
            ])->first();

            CompanyPayment::create([
                'payment_id' => $payment->id,
                'company_id' => $company->id
            ]);
        }
    }
}
