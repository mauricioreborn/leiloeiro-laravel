<?php

use App\Company\Company;
use App\Company\Services\CompanySettingService;
use Illuminate\Database\Seeder;

class CompanySettingsHasSellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = new CompanySettingService();

        $companies = Company::withTrashed()->get();

        $companies->each(function ($company) use ($service) {
            $hasSeller = $company->name !== 'Nestlé DPA' ? 1 : 0;
            $service->createHasSeller($company, $hasSeller);
        });
    }
}
