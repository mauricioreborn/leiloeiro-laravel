<?php

use App\Company\Company;
use App\Company\Services\CompanySettingService;
use Illuminate\Database\Seeder;

class CompanySettingsAcceptsDevolutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = new CompanySettingService();

        $companies = Company::withTrashed()->get();

        $companies->each(function ($company) use ($service) {
            $acceptsDevolution = $company->id !== 4 ? 1 : 0;
            $service->createAcceptsDevolution($company, $acceptsDevolution);
        });
    }
}