<?php

use App\Core\Entities\RejectionMotive;
use Illuminate\Database\Seeder;

class RejectionMotiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            RejectionMotive::INVALID_FEFO,
            RejectionMotive::CUSTOMER_WITHOUT_BALANCE,
            RejectionMotive::MIN_ORDER,
            RejectionMotive::EXPIRED,
            RejectionMotive::PRICE_REJECTED,
            RejectionMotive::REMOVED_BY_USER,
            RejectionMotive::PAYMENT_DENIED,
            RejectionMotive::BEHAVIOUR_REJECTION,
        ];

        foreach ($status as $name) {
            RejectionMotive::firstOrCreate(['name' => $name]);
        }
    }
}
