<?php

use App\Core\Entities\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            Status::CREATED,
            Status::INTEGRATED,
            Status::PENDING,
            Status::PROCESSING,
            Status::PAID,
            Status::CANCELED,
            Status::PARTIALLY_PAID,
            Status::REFUNDED,
            Status::EXPIRED,
            Status::AUTHORIZED,
            Status::REJECTED,
            Status::DRAFT,
            Status::PAYMENT_IN_PROGRESS,
            Status::IN_PROTEST,
            Status::IN_ANALYSIS,
            Status::CHARGEBACK,
            Status::ACTIVE,
            Status::BLOCKED,
            Status::WITH_ERRORS,
            Status::PRICE_ACCEPTED,
            Status::EVALUATING_PRICE,
            Status::APPROVED,
            Status::SCHEDULED,
            Status::FINISHED,
            Status::IN_PROGRESS,
        ];

        foreach ($status as $name) {
            Status::firstOrCreate(['name' => $name]);
        }
    }
}
