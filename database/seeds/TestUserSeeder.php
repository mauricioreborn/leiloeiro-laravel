<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class TestUserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (config('app.env') === 'production') {
            Log::info('Aborting. This command should not be ran on production environment.');
            return;
        }

        $checkIfUserExists = User::where('email', 'testing@souk.com.br')->first();

        if ($checkIfUserExists === null) {
            User::create([
                'name'     => 'Test User',
                'email'    => 'testing@souk.com.br',
                'password' => md5('testpassword'),
            ]);
        }
    }
}
