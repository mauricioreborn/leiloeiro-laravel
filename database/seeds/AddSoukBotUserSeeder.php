<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AddSoukBotUserSeeder extends Seeder
{

    /**
     * Method to insert a souk bot
     *
     * @return null
     */
    public function run()
    {
        $soukBot = DB::table('users')->where(['id' => 27])->first();

        if (!isset($soukBot->id)) {
            DB::table('users')->insert([
                'id' => 27,
                'name' => 'soukBot',
                'email' => 'soukbot@souk.com.br',
                'password' => md5('S0uk@123'),
                'root' => 1,
                'price' => 0,
                'featured' => 1,
                'company_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}