<?php

use App\Client\Client;
use App\Client\ClientTirolez;
use Illuminate\Database\Seeder;

class ClientTirolezSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client1 = Client::where('cnpj', '97747439000114')->first();
        $client2 = Client::where('cnpj', '27127266000156')->first();

        if ($client1) {
            $company = factory(ClientTirolez::class)->create([
                'client_id' => $client1,
                'type' => ClientTirolez::TYPE['base_1'],
                'modal_view_counter' => 0,
                'last_updated_at' => now(),
                'banner_start_at' => null,
                'banner_finish_at' => null,
            ]);
        }

        if ($client2) {
            $company = factory(ClientTirolez::class)->create([
                'client_id' => $client2,
                'type' => ClientTirolez::TYPE['base_2'],
                'modal_view_counter' => 0,
                'last_updated_at' => now(),
                'banner_start_at' => null,
                'banner_finish_at' => null,
            ]);
        }
    }
}
