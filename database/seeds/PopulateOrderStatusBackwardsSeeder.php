<?php

use App\Bid\Bid;
use App\Client\ClientCompanyPayment;
use App\Company\CompanyPayment;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Order\Order;
use App\Payment\Payment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PopulateOrderStatusBackwardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedidos')
            ->whereNull('rejection_motives_id')
            ->where('motivo_cancelamento', 'LIKE', '%pedido minimo%')
            ->where('status_id', 6)
            ->update(['rejection_motives_id' => RejectionMotive::type(RejectionMotive::MIN_ORDER)->id]);

        DB::table('pedidos')
            ->whereNull('rejection_motives_id')
            ->where('motivo_cancelamento', 'LIKE', '%limite de credito%')
            ->where('status_id', 6)
            ->update(['rejection_motives_id' => RejectionMotive::type(RejectionMotive::CUSTOMER_WITHOUT_BALANCE)->id]);

        DB::table('pedidos')
            ->whereNull('rejection_motives_id')
            ->whereNotNull('motivo_cancelamento')
            ->where('status_id', 6)
            ->update(['rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id]);

        DB::table('pedidos')
            ->whereNull('rejection_motives_id')
            ->whereNull('motivo_cancelamento')
            ->where('status_id', 6)
            ->update(['rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id]);
    }
}
