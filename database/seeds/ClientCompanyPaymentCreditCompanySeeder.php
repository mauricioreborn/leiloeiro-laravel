<?php

use App\Company\Company;
use App\Company\CompanyPayment;
use App\Payment\Payment;
use App\Client\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class ClientCompanyPaymentCreditCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::with('clients')->withTrashed()->get();

        $companyCredit = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $companyPayments = CompanyPayment::where('payment_id', $companyCredit->id)->get();

        $payments = $companyPayments->mapWithKeys(function ($p) {
            return [$p->company_id => $p->id];
        });

        $companies->each(function ($company) use ($payments) {
            $companyPayment = array_get($payments, $company->id, null);

            $companyId = $company->id;

            $clients = (new Client())
                ->fromCompany($companyId)
                ->with([
                    'companyPayments' => function ($query) use ($companyId) {
                        $query->where('company_payments.company_id', '=', $companyId);
                    }
                ])->whereDoesntHave('companyPayments', function ($query) use ($companyId) {
                    $query->where('company_payments.company_id', '=', $companyId);
                })->get();

            if ($clients) {
                foreach ($clients as $client) {
                    $client = $client->clientCompanyPayments()->firstOrCreate([
                        'company_payment_id' => $companyPayment,
                    ]);

                    Log::info("Inserindo client_id: {$client->id} na company_id {$company->id}");
                }
            }
        });
    }
}
