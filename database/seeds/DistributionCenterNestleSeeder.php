<?php

use App\Company\Company;
use App\DistributionCenter\DistributionCenter;
use Illuminate\Database\Seeder;

class DistributionCenterNestleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nestle = Company::withTrashed()->where('name', 'Nestlé DPA')->first();

        if ($nestle) {
            $data = [
                [
                    'company_id' => $nestle->id,
                    'code' => '1304',
                    'name' => 'CD BELO HORIZONTE',
                    'state' => 'MG',
                ],
                [
                    'company_id' => $nestle->id,
                    'code' => '1305',
                    'name' => 'CD BRASILIA',
                    'state' => 'DF',
                ],
                [
                    'company_id' => $nestle->id,
                    'code' => '1308',
                    'name' => 'CD CURITIBA',
                    'state' => 'PR',
                ],
                [
                    'company_id' => $nestle->id,
                    'code' => '1311',
                    'name' => 'CD FORTALEZA',
                    'state' => 'CE',
                ],
                [
                    'company_id' => $nestle->id,
                    'code' => '1314',
                    'name' => 'CD PORTO ALEGRE',
                    'state' => 'RS',
                ],
                [
                    'company_id' => $nestle->id,
                    'code' => '1316',
                    'name' => 'CD RECIFE',
                    'state' => 'PE',
                ],
                [
                    'company_id' => $nestle->id,
                    'code' => '1317',
                    'name' => 'CD RIO DE JANEIRO',
                    'state' => 'RJ',
                ],
                [
                    'company_id' => $nestle->id,
                    'code' => '1319',
                    'name' => 'CD SALVADOR',
                    'state' => 'BA',
                ],
            ];

            foreach ($data as $distributionCenter) {
                DistributionCenter::firstOrCreate($distributionCenter);
            }
        }
    }
}
