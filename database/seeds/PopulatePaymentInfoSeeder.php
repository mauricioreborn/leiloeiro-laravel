<?php

use App\Bid\Bid;
use App\Client\ClientCompanyPayment;
use App\Company\CompanyPayment;
use App\Order\Order;
use App\Payment\Payment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class PopulatePaymentInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $billet = Payment::where('type', Payment::BILLET)->first();
        $companyBillet = CompanyPayment::where('payment_id', $billet->id)->get();

        ClientCompanyPayment::whereIn('company_payment_id', $companyBillet->pluck('id'))->delete();

        $orders = Order::with('client.clientCompanyPayments')->whereNull('client_company_payment_id')->get();

        $this->updateClientCompanyPayment($orders);

        $count = Order::whereNull('client_company_payment_id')->count();

        $bids = Bid::withTrashed()->with('client.clientCompanyPayments')->whereNull('client_company_payment_id')->get();

        $this->updateClientCompanyPayment($bids);

        $count = Bid::whereNull('client_company_payment_id')->count();
    }

    /**
     * update client company payment id
     *
     * @param  Collection $items Collection of items
     *
     * @return void
     */
    public function updateClientCompanyPayment($items)
    {
        $companyCredit = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $companyPayments = CompanyPayment::where('payment_id', $companyCredit->id)->get();

        $payments = $companyPayments->mapWithKeys(function ($p) {
            return [$p->company_id => $p->id];
        });

        $items->each(function ($item) use ($payments) {
            $companyPayment = array_get($payments, $item->company_id, null);

            if ($companyPayment) {
                $clientCompanyPayment = $item
                    ->client
                    ->clientCompanyPayments
                    ->firstWhere('company_payment_id', $companyPayment);

                if ($clientCompanyPayment) {
                    $item->client_company_payment_id = $clientCompanyPayment->id;

                    $item->save();

                    return;
                }

                return;
            }
        });
    }
}
