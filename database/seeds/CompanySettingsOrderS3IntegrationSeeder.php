<?php

use App\Company\Company;
use App\Company\Services\CompanySettingService;
use Illuminate\Database\Seeder;

class CompanySettingsOrderS3IntegrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = new CompanySettingService();
        $companies = Company::withTrashed()->get();

        $companies->each(function ($company) use ($service) {
            $sync = ($company->id == 5) ? 1 : 0;
            $service->createOrderS3Integration($company, $sync);
        });
    }
}