<?php
use App\Client\Client;
use App\Company\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TestDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $availableCompanies = ['Seara', 'Souk', 'Massa Leve', 'Nestlé Iogurtes', 'Tirolez'];

        foreach($availableCompanies as $company) {
            $company = Company::firstOrCreate(['name' => $company]);
            $company->code = $company->id;
            $company->save();
        }

        $client = Client::firstOrCreate([
            'cnpj' => 97747439000114,
            'nome' => 'Dummy Client',
            'email' => 'client@souk.com.br',
            'email_verified' => true,
            'telefone' => '11987654321',
        ]);

        $client->password = 'testpassword';
        $client->save();

        $companies = Company::all();

        foreach($companies as $company) {
            $companySlug = $company->name === 'Souk' ? '' : Str::slug($company->name) . '.';

            $testUser = $company->users()->firstOrCreate([
                'name' => 'DummyUser',
                'email' => "dummyuser@{$companySlug}souk.com.br",
            ]);
            $testUser->update(['password' => 'testpassword']);

            $company->users()->firstOrCreate([
                'name' => 'soukBot',
                'email' => "soukbot@{$companySlug}souk.com.br",
                'root' => 1,
            ]);

            $company->clients()->sync([$client->id]);
        }
    }
}
