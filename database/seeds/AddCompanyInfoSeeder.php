<?php

use App\Company\Company;
use Illuminate\Database\Seeder;

class AddCompanyInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::first();

        if ($company) {
            $company->cart = __('Mobile/login.texto_carrinho');
            $company->bid = __('Mobile/login.texto_lance');
            $company->rules = __('Mobile/login.texto_regras');
            $company->aboult = __('Mobile/login.texto_sobre');
            $company->policy = __('Mobile/login.texto_politica');
            $company->information = __('Mobile/login.texto_informacoes');

            $company->save();
        }
    }
}
