<?php

use App\Company\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AddChristmasSeason extends Seeder
{
    /**
     * Method to insert season type
     *
     * @return void
     */
    public function run()
    {
        $season = Db::table('season_types')->where(['name' => 'natalinos'])->first();
        $company = Company::first();

        if (!isset($season->id)) {
            DB::table('season_types')->insert([
                'name' => 'natalinos',
                'company_id' => $company->id,
                'start_season' => Carbon::now()->format('Y-m-d H:i:s'),
                'finish_season' => Carbon::now()->addMonths(2)->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}