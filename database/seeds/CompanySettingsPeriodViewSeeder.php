<?php

use App\Company\Company;
use App\Company\Services\CompanySettingService;
use Illuminate\Database\Seeder;

class CompanySettingsPeriodViewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = new CompanySettingService();

        $companies = Company::withTrashed()->get();

        $companies->each(function ($company) use ($service) {
            $type = $service::WEEKLY;

            if ($company->name == 'Nestlé DPA') {
                $type = $service::DAILY;
            }

            $service->createPeriodView($company, $type);
        });
    }
}
