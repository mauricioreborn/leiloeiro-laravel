<?php

use App\Company\Company;
use App\Payment\Payment;
use Illuminate\Database\Seeder;

class CompanyPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::withTrashed()->get();

        $payments = Payment::all();

        $companies->each(function ($company) use ($payments) {
            foreach ($payments as $payment) {
                $company->companyPayments()->firstOrCreate(['payment_id' => $payment->id]);
            }
        });
    }
}
