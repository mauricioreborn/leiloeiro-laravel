<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Season\SeasonType;

class AddCustomColorOnSeasonType extends Seeder
{
    /**
     * Method to insert season type
     *
     * @return void
     */
    public function run()
    {
        $seasonType = SeasonType::where(['name' => 'natalinos'])->first();

        if ($seasonType->id) {
            $customColors = factory(SeasonType::class)->make()->custom_colors;

            $seasonType->custom_colors = $customColors;

            $seasonType->save();
        }
    }
}
