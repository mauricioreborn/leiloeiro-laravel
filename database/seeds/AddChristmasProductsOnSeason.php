<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AddChristmasProductsOnSeason extends Seeder
{
    /**
     * Method to inset products
     *
     * @return void
     */
    public function run()
    {
        $season = Db::table('season_types')->where(['name' => 'natalinos'])->first();
        $products = [
            6696,
            7293,
            7307,
            7374,
            951137,
            951153,
            951226,
            951242,
            951250,
            951293,
            951307,
            951595,
            951609,
            951978,
            956317,
            124730,
            951331,
            951374,
            124061,
            217779,
            217789,
            217799,
            921351,
            950586,
            951145,
            951277,
            951285,
            951323,
            958816,
            217819,
            218069,
            688657,
            688673,
            688681,
            791814,
            921343,
        ];

        if (isset($season->id)) {
            foreach ($products as $product) {
                DB::table('season_products')->insert([
                    'product_code' => $product,
                    'season_id' => $season->id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }
        }
    }
}