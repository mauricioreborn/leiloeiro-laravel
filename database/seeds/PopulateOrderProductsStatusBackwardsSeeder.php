<?php

use App\Bid\Bid;
use App\Client\ClientCompanyPayment;
use App\Company\CompanyPayment;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Order\Order;
use App\Payment\Payment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PopulateOrderProductsStatusBackwardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedidos_produtos')
            ->whereNull('status_final_id')
            ->update(['status_id' => Status::type(Status::PENDING)->id]);

        DB::table('pedidos_produtos')
            ->whereIn('status_final_id', [1, 2])
            ->update(['status_id' => Status::type(Status::PENDING)->id]);

        DB::table('pedidos_produtos')
            ->where('status_final_id', 3)
            ->update(['status_id' => Status::type(Status::APPROVED)->id]);

        DB::table('pedidos_produtos')
            ->where('status_final_id', 4)
            ->update([
                'status_id' => Status::type(Status::CANCELED)->id,
                'rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id,
            ]);
    }
}
