<?php

use App\Campaign\CampaignType;
use Illuminate\Database\Seeder;

class CampaignTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('APP_ENV') != 'testing') {
            CampaignType::truncate();
        }
        CampaignType::create([
            'name' => 'Recompra',
            'uid' => 'repurchase',
            'description' => 'Enviamos o melhor preço de um produto já consumido pelo cliente.',
            'recurrence_days' => '7'
        ]);

        CampaignType::create([
            'name' => 'Primeira compra',
            'uid' => 'firstPurchase',
            'description' => 'O cliente receberá um push do produto mais consumido na tipologia dele
            O preço exibido será o menor preço negociado do produto.',
            'recurrence_days' => '7'
        ]);

        CampaignType::create([
            'name' => 'Destaque',
            'uid' => 'highlightFefo',
            'description' => 'Escolha um produto em destaque e iremos disparar um push para todos os clientes que 
            possuem este produto disponivel para o seu login.',
            'recurrence_days' => '1'
        ]);
    }
}
