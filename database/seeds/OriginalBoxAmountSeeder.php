<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OriginalBoxAmountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lances')
            ->whereNull('original_box_amount')
            ->update(['original_box_amount' => DB::raw('qtd_caixas')]);

        DB::table('pedidos_produtos')
            ->whereNull('original_box_amount')
            ->update(['original_box_amount' => DB::raw('qtd_caixas')]);
    }
}
