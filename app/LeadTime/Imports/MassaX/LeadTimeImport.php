<?php

namespace App\LeadTime\Imports\MassaX;

use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Illuminate\Support\Facades\Validator;

use App\ClientCompanies\ClientCompany;
use App\Core\Entities\Status;
use App\DistributionCenter\DistributionCenter;
use App\FileQueue\Services\FileQueueService;
use App\LeadTime\Services\LeadTimeService;
use Illuminate\Support\Collection;
use App\FileQueue\FileQueue;
use App\LeadTime\LeadTime;

class LeadTimeImport implements ToCollection, WithHeadingRow, WithCustomCsvSettings, WithEvents
{
    use Importable, RegistersEventListeners;

    protected $leadTimeService;
    protected $fileQueue;
    protected $leadtimeIds = [];
    protected $errors = [];
    protected $clientCodes = [];
    protected $distributionCenter;

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);

        $this->distributionCenter = DistributionCenter::where([
            'company_id' => $this->fileQueue->company_id
        ])->first();
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;
            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * get csv settings
     *
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }


    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     */
    public function collection(Collection $rows)
    {
        $errors = [];

        $validRows = $rows->filter(function ($row) {
            $emptyColumnsCount = 0;

            foreach ($row as $column => $value) {
                if (array_key_exists($column, $this->rules()) && !$value) {
                    $emptyColumnsCount++;
                }
            }
            return $emptyColumnsCount !== count(array_keys(self::rules()));
        });

        foreach ($validRows as $rowNumber => $row) {

            $validation = Validator::make($row->toArray(), self::rules());
            if ($validation->fails()) {
                $errors[$rowNumber + $this->headingRow() + 1] = $validation->errors()->getMessageBag()->all();
                continue;
            }

            $cnpj = sanitize_number($row['cnpj']);
            $name = strtoupper($row['nome']);
            $days = $row['dias'];
            $monday = $row['segunda'];
            $tuesday = $row['terca'];
            $wednesday = $row['quarta'];
            $thursday = $row['quinta'];
            $friday = $row['sexta'];
            $saturday = $row['sabado'];
            $sunday = $row['domingo'];

            LeadTime::updateOrCreate([
                    'cod_origem' => $this->distributionCenter->code,
                    'cnpj' => $cnpj,
                    'company_id' => $this->fileQueue->company_id
                ],[
                    'code' => $this->distributionCenter->code,
                    'name' => $name,
                    'cnpj' => $cnpj,
                    'days' => $days,
                    'prediction' => 9,
                    'limit_hour' => 1400,
                    'monday' => $monday,
                    'tuesday' => $tuesday,
                    'wednesday' => $wednesday,
                    'thursday' => $thursday,
                    'friday' => $friday,
                    'saturday' => $saturday,
                    'sunday' => $sunday,
                    'operates_saturday' => $saturday,
                    'operates_sunday' => $sunday,
                    'company_id' => $this->fileQueue->company_id,
                ]
            );
        }

        if (count($errors) > 0) {
            $this->logErrors($errors);
        }
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nome' => 'required',
            'cnpj' => 'required',
            'dias' => 'required',
            'segunda' => 'required',
            'terca' => 'required',
            'quarta' => 'required',
            'quinta' => 'required',
            'sexta' => 'required',
            'sabado' => 'required',
            'domingo' => 'required',
        ];
    }

    /**
     * @TODO move to abstract class
     * @param array $errors errors array
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function logErrors($errors, $withImplode = true)
    {
        $this->fileQueue->update([
            'status_id' => Status::type(Status::WITH_ERRORS)->id
        ]);

        $errorMessages = [];

        foreach ($errors as $row => $messages) {

            $message =  $withImplode == true ? implode(', ', $messages) : $messages;
            $message = "Linha {$row}: {$message}";
            $errorMessages[] = $message;
            Log::info($message);

        }
        return (new FileQueueService)->createLog($this->fileQueue, $errorMessages);
    }
}
