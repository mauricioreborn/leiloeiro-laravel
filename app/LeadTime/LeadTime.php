<?php

namespace App\LeadTime;

use App\Client\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class LeadTime extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    protected $table = 'leadtime';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'cod_origem',
        'nome',
        'cnpj',
        'dias',
        'visao_logistica',
        'hora_corte',
        'segunda',
        'terca',
        'quarta',
        'quinta',
        'sexta',
        'sabado',
        'domingo',
        'opera_sabado',
        'opera_domingo',
        'company_id',
        'logistic_information',

        //en
        'code',
        'name',
        'days',
        'prediction',
        'limit_hour',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
        'operates_saturday',
        'operates_sunday',
    ];


    // // legacy db mapping
    public $maps = [
        'code' => 'cod_origem',
        'name' => 'nome',
        'days' => 'dias',
        'prediction' => 'visao_logistica',
        'limit_hour' => 'hora_corte',
        'monday' => 'segunda',
        'tuesday' => 'terca',
        'wednesday' => 'quarta',
        'thursday' => 'quinta',
        'friday' => 'sexta',
        'saturday' => 'sabado',
        'sunday' => 'domingo',
        'operates_saturday' => 'opera_sabado',
        'operates_sunday' => 'opera_domingo',
    ];

    protected $hidden = [
        'cod_origem',
        'nome',
        'dias',
        'visao_logistica',
        'hora_corte',
        'segunda',
        'terca',
        'quarta',
        'quinta',
        'sexta',
        'sabado',
        'domingo',
        'opera_sabado',
        'opera_domingo',
    ];

    protected $appends = [
        'code',
        'name',
        'days',
        'prediction',
        'limit_hour',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
        'operates_saturday',
        'operates_sunday',
    ];

    /**
     * @param Builder $builder Query builder
     * @param string  $value   Value
     * @param string  $key     Key
     * @return Collection
     */
    public function scopeListed($builder, $value = 'name', $key = 'cod_origem')
    {
        return $builder->distinct()->pluck($value, $key);
    }

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }

    /**
     * Method to relates the leadtime class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'cnpj', 'cnpj');
    }
}
