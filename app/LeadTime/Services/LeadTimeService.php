<?php

namespace App\LeadTime\Services;

use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Core\Entities\Status;
use App\DistributionCenter\DistributionCenter;
use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\LeadTime\Http\Factories\Search;
use App\LeadTime\LeadTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class LeadTimeService
{
    protected $clientCodes = [];
    /**
     * get clients id by origin code
     *
     * @param Company $company Company entity
     * @param string $originCode origin code
     *
     * @return array
     */
    public function getClientsOriginCode(Company $company, string $originCode): array
    {
        return Leadtime::with('client')
            ->where('company_id', $company->id)
            ->where('cod_origem', $originCode)
            ->get()
            ->pluck('client.id')
            ->toArray();
    }

    public function getLeadTimeOrigins(Company $company)
    {
        return LeadTime::select(['cod_origem', 'nome'])
            ->where('company_id', $company->id)
            ->groupBy(['cod_origem', 'nome'])
            ->orderBy('nome')
            ->get();
    }

    /**
     * Check day passed is a week day or a holiday, then return nextWeekday or false
     * @param Carbon $date date
     * @return Carbon|false
     **/
    public function checkWeekDay(Carbon $date)
    {
        $response = false;

        while ($date->isHoliday() || $date->isWeekend()) {
            $response = $date->nextWeekday();
        }

        return $response;
    }

    /**
     * Method to get the logistics
     *
     * @param array $clientIds Ids to be searched
     * @param bool|int $codOrigem Where false is not searched
     * @param null $company The company_id
     * @param bool $shipment Aditional param
     * @return array|bool
     */
    public function getLogistica(array $clientIds, $codOrigem = false, $company = null, bool $shipment = false)
    {
        if (count($clientIds) <= 0) {
            return false;
        }

        $clientesCnpj = Client::find($clientIds)->pluck('id', 'cnpj');

        $query = Leadtime::select([
            'cnpj',
            'code',
            'name',
            'days',
            'prediction',
            'limit_hour',
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
            'operates_saturday',
            'operates_sunday',
        ])->whereIn('cnpj', $clientesCnpj->keys());

        if ($company) {
            $query->where('company_id', $company);
        }

        if ($codOrigem) {
            $query->where("cod_origem", $codOrigem);
        }

        $leadtimes = $query->get();

        $results = [];

        foreach ($leadtimes as $leadtime) {
            $shipmentDate = null;

            $nextDeliveryDay = $this->nextDeliveryDay($leadtime);

            $shipmentDate = array_get($nextDeliveryDay, 'shipment');
            $purchaseLimit = array_get($nextDeliveryDay, 'purchaseLimit');
            $logisticDate = array_get($nextDeliveryDay, 'logistic');

            $deliveryDate = $shipmentDate->copy()->addDays($leadtime->days);
            $daysToDelivery = now()->startOfDay()->diffInDays($deliveryDate);

            $result = [
                "aceita_compra" => array_get($nextDeliveryDay, 'accept'),
                "visao_logistica" => $logisticDate->toDateString(),
                "limite_compra" => $purchaseLimit->toDateTimeString(),
                "data_embarque" => $shipmentDate->toDateString(),
                "data_entrega" => $deliveryDate->toDateString(),
                "dias_ate_entrega" => $daysToDelivery,
                "semanas_subtrair" => ceil($daysToDelivery / 7),
                "hora_considerada" => now()->toTimeString(),
            ];

            if ($shipment) {
                $result['dias_embarque'] = [
                    (string) $leadtime->sunday,
                    (string) $leadtime->monday,
                    (string) $leadtime->tuesday,
                    (string) $leadtime->wednesday,
                    (string) $leadtime->thursday,
                    (string) $leadtime->friday,
                    (string) $leadtime->saturday,
                ];

                $result['dias_visao_logistica'] = [
                    (string) $this->canBuyAtNext($leadtime, Carbon::SUNDAY),
                    (string) $this->canBuyAtNext($leadtime, Carbon::MONDAY),
                    (string) $this->canBuyAtNext($leadtime, Carbon::TUESDAY),
                    (string) $this->canBuyAtNext($leadtime, Carbon::WEDNESDAY),
                    (string) $this->canBuyAtNext($leadtime, Carbon::THURSDAY),
                    (string) $this->canBuyAtNext($leadtime, Carbon::FRIDAY),
                    (string) $this->canBuyAtNext($leadtime, Carbon::SATURDAY),
                ];

                $result['prazo_entrega'] = $leadtime->days;
            }

            $results[$clientesCnpj[$leadtime->cnpj]] = $result;
        }

        $clientWithoutLeadtime = array_diff($clientIds, array_keys($results));

        foreach ($clientWithoutLeadtime as $clientId) {
            $results[$clientId] = $this->withoutLogistic($shipment);
        }
        return $results;
    }

    /**
     * verify if can buy in next weekday
     *
     * @param  Leadtime $leadtime LeadTime Entity
     * @param  int $weekday week day
     *
     * @return int
     */
    public function canBuyAtNext(Leadtime $leadtime, int $weekday): int
    {
        $day = now()->startOfDay()->next($weekday);

        if (now()->startOfDay()->dayOfWeek == $weekday) {
            $day = now();
        }

        return (integer) array_get($this->nextDeliveryDay($leadtime, $day), 'accept');
    }

    /**
     * get next delivery day
     *
     * @param Leadtime $leadtime Leadtime entity
     * @param Carbon|null $date Current Date
     *
     * @return array
     */
    public function nextDeliveryDay(Leadtime $leadtime, ?Carbon $date = null): array
    {
        $day = $date ?? now();

        $weekendDays = [];

        if (!$leadtime->operates_saturday) {
            $weekendDays[] = Carbon::SATURDAY;
        }
        if (!$leadtime->operates_sunday) {
            $weekendDays[] = Carbon::SUNDAY;
        }

        $defaultWeekendDays = Carbon::getWeekendDays();

        Carbon::setWeekendDays($weekendDays);

        $shipments = collect();

        if ($leadtime->sunday && $leadtime->operates_sunday) {
            $shipments->push($day->copy()->next(Carbon::SUNDAY));
        }

        if ($leadtime->monday) {
            $shipments->push($day->copy()->next(Carbon::MONDAY));
        }

        if ($leadtime->tuesday) {
            $shipments->push($day->copy()->next(Carbon::TUESDAY));
        }

        if ($leadtime->wednesday) {
            $shipments->push($day->copy()->next(Carbon::WEDNESDAY));
        }

        if ($leadtime->thursday) {
            $shipments->push($day->copy()->next(Carbon::THURSDAY));
        }

        if ($leadtime->friday) {
            $shipments->push($day->copy()->next(Carbon::FRIDAY));
        }

        if ($leadtime->saturday && $leadtime->operates_saturday) {
            $shipments->push($day->copy()->next(Carbon::SATURDAY));
        }

        $shipments = $shipments->sort();

        $hour = substr($leadtime->limit_hour, 0, 2);
        $minute = substr($leadtime->limit_hour, 2, 4);

        foreach ($shipments as $shipment) {
            $lastWeekDay = $shipment->copy()
                ->subBusinessDays()
                ->hour($hour)
                ->minute($minute)
                ->second(0);

            if ($lastWeekDay->lt($day)) {
                continue;
            }

            $shipmentDate = $shipment->copy();

            break;
        }

        if (!isset($shipmentDate)) {
            $shipmentDate = now()->addBusinessDays($leadtime->prediction + 2);

            if ($shipments->count()) {
                $shipmentDate = $shipments->first()->addWeek();
            }
        }
        $purchaseLimit = $shipmentDate->copy()
            ->subBusinessDays()
            ->hour($hour)
            ->minute($minute)
            ->second(0);

        $logisticDate = $purchaseLimit->copy()->subBusinessDays($leadtime->prediction)->addSecond();

        Carbon::setWeekendDays($defaultWeekendDays);

        return [
            'accept' => (bool) $day->between($logisticDate, $purchaseLimit),
            'shipment' => $shipmentDate,
            'purchaseLimit' => $purchaseLimit,
            'logistic' => $logisticDate,
        ];
    }

    /**
     * Generate logistic without leadtime
     *
     * @param bool $shipment Include shipment in array
     *
     * @return array
     */
    protected function withoutLogistic(bool $shipment): array
    {
        $logistic = [
            "aceita_compra" => false,
            "visao_logistica" => now()->addBusinessDays()->toDateString(),
            "limite_compra" => now()->addBusinessDays()->toDateTimeString(),
            "data_embarque" => now()->addBusinessDays(2)->toDateString(),
            "data_entrega" => now()->addBusinessDays(2)->toDateString(),
            "dias_ate_entrega" => 0,
            "semanas_subtrair" => 1,
            "hora_considerada" => now()->toTimeString(),
        ];

        if ($shipment) {
            $logistic = array_merge($logistic, [
                'dias_embarque' => ['0', '0', '0', '0', '0', '0', '0'],
                'dias_visao_logistica' => ['0', '0', '0', '0', '0', '0', '0'],
                'prazo_entrega' => 0,
            ]);
        }

        return $logistic;
    }

    /**
     * Method to process Leadtime File
     * @param FileQueue $fileQueue File queue object
     * @return array
     */
    public function processLeadtime(FileQueue $fileQueue)
    {
        $fileQueueService = new FileQueueService();
        $file = $fileQueueService->downloadFile($fileQueue->path, $fileQueue->original_name);

        if (is_readable($file)) {
            $row = 0;
            if (($handle = fopen($file, "r")) !== false) {
                while (($data = fgetcsv($handle, 10000, ";")) !== false) {
                    $row++;
                    if ($row == 1) {
                        continue;
                    }

                    if (count($data) < 11) {
                        $fileQueue->update([
                            Status::type(Status::REJECTED)->id
                        ]);
                        return false;
                    }

                    $cnpj = preg_replace('/[^0-9]/i', '', $data[0]);
                    $originCode = $data[1];
                    $name = $data[2];
                    $dias = 0;


                    $insertLeadtime['nome'] = $data[2];
                    $insertLeadtime['dias'] = $dias;
                    $insertLeadtime['visao_logistica'] = 9;
                    $insertLeadtime['segunda'] = $data[3];
                    $insertLeadtime['terca'] = 0;
                    $insertLeadtime['quarta'] = $data[5];
                    $insertLeadtime['quinta'] = $data[6];
                    $insertLeadtime['sexta'] = $data[7];
                    $insertLeadtime['sabado'] = $data[8];
                    $insertLeadtime['domingo'] = $data[9];
                    $insertLeadtime['hora_corte'] = 1800;
                    $insertLeadtime['opera_sabado'] = 0;
                    $insertLeadtime['opera_domingo'] = 0;


                    $leadtime = LeadTime::withTrashed()->updateOrCreate([
                        'cod_origem' => $originCode, 'cnpj' => $cnpj, 'company_id' => $fileQueue->company_id
                    ],
                        $insertLeadtime
                    );

                    $data[10] == 1 ? $leadtime->restore() : $leadtime->delete();

                }
                $fileQueueService->deleteFile($fileQueue->original_name);
            }
            $fileQueue->update([
                Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * Method to insert a leadtime
     * @param Company            $company             company object
     * @param string             $client_code         client code
     * @param DistributionCenter $distributionCenter  distribution center
     * @param string             $days                days
     * @param string             $monday              working monday
     * @param string             $tuesday             working tuesday
     * @param string             $wednesday           working wednesday
     * @param string             $thursday            working thursday
     * @param string             $friday              working friday
     * @param string             $saturday            working saturday
     * @param string             $sunday              working sunday
     * @param string             $logisticInformation when would like receive
     * @return bool
     */
    public function importLeadTime(
        Company $company,
        $client_code,
        DistributionCenter $distributionCenter,
        $days,
        $monday,
        $tuesday,
        $wednesday,
        $thursday,
        $friday,
        $saturday,
        $sunday,
        $logisticInformation
    ) {

        if (! array_key_exists($client_code, $this->clientCodes)) {
            $errorMessage =  __(
                'Integration/leadtime.client_code_not_found',
                ['client_code' => $client_code]
            );
            Log::error($errorMessage);
            return $errorMessage;
        }
        
        return LeadTime::updateOrCreate([
            'cod_origem' => $distributionCenter->code,
            'cnpj' => $this->clientCodes[$client_code]['cnpj'],
            'company_id' => $company->id
        ],[
            'code' => $distributionCenter->code,
            'name' => $distributionCenter->name,
            'cnpj' => $this->clientCodes[$client_code]['cnpj'],
            'days' => $days,
            'prediction' => 7,
            'limit_hour' => 1400,
            'monday' => $monday,
            'tuesday' => $tuesday,
            'wednesday' => $wednesday,
            'thursday' => $thursday,
            'friday' => $friday,
            'saturday' => $saturday,
            'sunday' => $sunday,
            'operates_saturday' => $saturday,
            'operates_sunday' => $sunday,
            'company_id' => $company->id,
            'logistic_information' => $logisticInformation
        ]);
    }

    /**
     * Method to get client codes of client companies by company
     * @param Company $company company object
     * @return void
     */
    public function getClientWithCodes(Company $company)
    {
        $clientCompanies = ClientCompany::where([
            'company_id' => $company->id,
        ])->whereNotNull('client_code')->get();

        foreach ($clientCompanies as $clientCompany) {
            $this->clientCodes[$clientCompany->client_code]['cnpj'] = $clientCompany->client->cnpj;
        }
    }

    /**
     * Method to get codes with client cnpj array
     * @param Company $company company
     * @param array   $cnpjs   cnpjs
     * @return array
     */
    public function getOriginCodeWithCnpj(Company $company, $cnpjs)
    {
        return Leadtime::where(['company_id' => $company->id])
            ->whereIn('cnpj', $cnpjs)
            ->select('code')
            ->get()
            ->pluck('code')
            ->unique()
            ->values()
            ->toArray();
    }
}
