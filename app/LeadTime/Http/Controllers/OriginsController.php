<?php

namespace App\LeadTime\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\LeadTime\Http\Resources\LeadTimeOriginResource;
use App\LeadTime\Services\LeadTimeService;
use Illuminate\Http\Request;

class OriginsController extends Controller
{
    public function __invoke(Request $request)
    {
        return LeadTimeOriginResource::collection(
            (new LeadTimeService)->getLeadTimeOrigins($request->user()->company)
        );
    }
}
