<?php

namespace App\LeadTime\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LeadTimeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' =>
                $this->collection->map(function ($leadTime) use ($request) {
                    return (new LeadTimeResource($leadTime))->toArray($request);
                })
        ];
    }
}
