<?php

namespace App\LeadTime\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LeadTimeOriginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'leadtime_origin',
            'attributes' => [
                'code' => $this->code,
                'name' => $this->name
            ]
        ];
    }
}
