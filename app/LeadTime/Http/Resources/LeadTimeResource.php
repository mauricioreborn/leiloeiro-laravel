<?php

namespace App\LeadTime\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LeadTimeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'leadTime',
            'id' => $this->id,
            'attributes' => [
                'code' => $this->code,
                'cnpj' => $this->cnpj,
                'name' => $this->name,
                'days' => $this->days,
                'prediction' => $this->prediction,
                'limit_hour' => $this->limit_hour,
                'monday' => $this->monday,
                'tuesday' => $this->tuesday,
                'wednesday' => $this->wednesday,
                'thursday' => $this->thursday,
                'friday' => $this->friday,
                'saturday' => $this->saturday,
                'sunday' => $this->sunday,
                'operates_saturday' => $this->operates_saturday,
                'logistic_information' => $this->logistic_information,
                'operates_sunday' => $this->operates_sunday,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ]
        ];
    }
}
