<?php

namespace App\LeadTime\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class LeadTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {
                return [
                    'code' => 'required|integer|min: 0',
                    'name' => 'required|string',
                    'days' => 'required|integer|min: 0',
                    'prediction' => 'required|integer|min: 0',
                    'limit_hour' => 'required|integer|min:0',
                    'monday' => 'required|boolean',
                    'tuesday' => 'required|boolean',
                    'wednesday' => 'required|boolean',
                    'thursday' => 'required|boolean',
                    'friday' => 'required|boolean',
                    'saturday' => 'required|boolean',
                    'sunday' => 'required|boolean',
                    'cnpj' => 'required',
                    'operates_saturday' => 'required|boolean',
                    'operates_sunday' => 'required|boolean',
                ];
            }

            case 'PUT': {
                return [
                    'code' => 'integer|min: 1',
                    'name' => 'string',
                    'days' => 'integer|min: 0',
                    'prediction' => 'integer|min: 0',
                    'limit_hour' => 'integer',
                    'monday' => 'boolean',
                    'tuesday' => 'boolean',
                    'wednesday' => 'boolean',
                    'thursday' => 'boolean',
                    'friday' => 'boolean',
                    'saturday' => 'boolean',
                    'sunday' => 'boolean',
                    'operates_saturday' => 'boolean',
                    'operates_sunday' => 'boolean'
                ];
            }
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
