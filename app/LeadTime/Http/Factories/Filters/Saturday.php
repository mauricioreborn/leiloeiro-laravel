<?php

namespace App\LeadTime\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Saturday extends AbstractFilter implements FilterInterface
{

    public function run($query, $daysValue)
    {
        return $query->where($this->maps['saturday'], $daysValue);
    }

}
