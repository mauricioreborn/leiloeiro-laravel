<?php

namespace App\LeadTime\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Friday extends AbstractFilter implements FilterInterface
{

    public function run($query, $daysValue)
    {
        return $query->where($this->maps['days'], $daysValue);
    }

}
