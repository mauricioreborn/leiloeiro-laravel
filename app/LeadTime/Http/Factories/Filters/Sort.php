<?php


namespace App\LeadTime\Http\Factories\Filters;


use App\Core\Contracts\FilterInterface;

class Sort extends AbstractFilter implements FilterInterface
{

    public function run($query, $sort)
    {
        $descending = $sort[0] === '-';
        $key = ltrim($sort, '-');

        return $query->orderBy($key, $descending ? 'desc' : 'asc');
    }
}
