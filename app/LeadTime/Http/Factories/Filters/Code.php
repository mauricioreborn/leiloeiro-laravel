<?php

namespace App\LeadTime\Http\Factories\Filters;


use App\Core\Contracts\FilterInterface;

class Code extends AbstractFilter implements FilterInterface
{

    public function run($query, $codeValue)
    {
        return $query->where($this->maps['code'], $codeValue);
    }
}
