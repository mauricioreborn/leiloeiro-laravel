<?php

namespace App\LeadTime\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Name extends AbstractFilter implements FilterInterface
{

    public function run($query, $nameValue)
    {
        return $query->where($this->maps['name'], 'LIKE', '%'.$nameValue.'%');
    }
}
