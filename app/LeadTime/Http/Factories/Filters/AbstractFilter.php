<?php

namespace App\LeadTime\Http\Factories\Filters;

use App\LeadTime\LeadTime;

abstract class AbstractFilter
{
    protected $maps;

    public function __construct()
    {
        $leadTime = new LeadTime();
        $this->maps = $leadTime->maps;
    }
}
