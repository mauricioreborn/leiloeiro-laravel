<?php

namespace App\LeadTime\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Limit_hour extends AbstractFilter implements FilterInterface
{

    public function run($query, $daysValue)
    {
        return $query->where($this->maps['days'], $daysValue);
    }

}
