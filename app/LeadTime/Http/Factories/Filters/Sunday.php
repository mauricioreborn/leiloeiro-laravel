<?php

namespace App\LeadTime\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Sunday extends AbstractFilter implements FilterInterface
{

    public function run($query, $daysValue)
    {
        return $query->where($this->maps['sunday'], $daysValue);
    }

}
