<?php

namespace App\LeadTime\Http\Factories;


use App\Core\Contracts\SearchInterface;
use App\LeadTime\LeadTime;

class Search implements SearchInterface
{

    private $query;

    private $namespace;

    public function __construct()
    {
        $this->namespace = "App\\LeadTime\\Http\\Factories\\Filters\\";
    }

    /**
     * Method to search by filters
     * @param array $filters
     * @return LeadTime
     */
    public function run(array $filters)
    {
        $this->query = new LeadTime();

        foreach ($filters as $className => $value) {
            $className = ucfirst($className);
            $class     = $this->namespace . $className;
            if (class_exists($class)) {
                $class       = new $class();
                $this->query = $class->run($this->query, $value);
            }
        }

        return $this->query;
    }

    public function execute()
    {

    }

}
