<?php

namespace App\LeadTime\Http\Validators;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LeadTimeValidator
{

    public function validateSearchFilters($fields)
    {
        $validatorRule = [
            'code' => 'integer',
            'cnpj' => 'string',
            'name' => 'string|min:1',
            'days' => 'integer|min:0',
            'prediction' => 'integer|min:0',
            'limit_hour' => 'integer|min:0',
            'monday' => 'boolean',
            'tuesday' => 'boolean',
            'wednesday' => 'boolean',
            'thursday' => 'boolean',
            'friday' => 'boolean',
            'saturday' => 'boolean',
            'sunday' => 'boolean',
            'operates_saturday' => 'boolean',
            'operates_sunday' => 'boolean',
            'limit' => 'integer|min:1',
            'order' => 'string',
            'sort' => 'string',
            'page' => 'integer'
        ];

        $validator = Validator::make($fields, $validatorRule);

        foreach ($fields as $fieldName => $fieldValue) {

            if (!isset($validatorRule[$fieldName])) {
                $validator->errors()->add($fieldName, "the field does not exist");
            }
        }

        if (count($validator->errors()) > 0) {
            throw new ValidationException($validator->errors(), $validator->errors());
        }
    }
}
