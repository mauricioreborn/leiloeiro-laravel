<?php

namespace App\Bid;

use App\Auth\User;
use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Company\Company;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Order\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Products\Product;
use App\Client\Client;
use \Carbon\Carbon;
use App\Core\Classes\Transactible;
use App\Mobile\Services\BidService;

class Bid extends Model
{
    use Eloquence, Mappable, SoftDeletes, Transactible;

    protected $table = 'lances';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_cliente',
        'data',
        'cod_origem',
        'cod_produto',
        'semanas',
        'qtd_caixas',
        'total_kg',
        'valor_kg',
        'valor',
        'aceite_parcial',
        'duracao_lance',
        'company_id',
        'nao_lido',

        //en
        'client_id',
        'date',
        'origin_code',
        'product_code',
        'weeks',
        'box_amount',
        'weight',
        'kg_price',
        'price',
        'partial_accept',
        'duration',
        'user_id',
        'accept_date',
        'original_box_amount',
        'client_company_payment_id',
        'client_card_id',
        'cc_months',
        'tax_percent',
        'status_id',
        'rejection_motives_id',
        'sanctioned_at',
    ];

    protected $hidden = [
        'id_cliente',
        'data',
        'cod_origem',
        'cod_produto',
        'semanas',
        'qtd_caixas',
        'total_kg',
        'valor_kg',
        'valor',
        'aceite_parcial',
        'duracao_lance',
        'id_user'
    ];

    protected $appends = [
        'client_id',
        'date',
        'origin_code',
        'product_code',
        'weeks',
        'box_amount',
        'weight',
        'kg_price',
        'price',
        'price_with_tax',
        'partial_accept',
        'duration',
        'status_description',
        'user_id',
        'custom_fields'
    ];

    // legacy db mapping
    protected $maps = [
        'client_id'      => 'id_cliente',
        'date'           => 'data',
        'origin_code'    => 'cod_origem',
        'product_code'   => 'cod_produto',
        'weeks'          => 'semanas',
        'box_amount'     => 'qtd_caixas',
        'weight'         => 'total_kg',
        'kg_price'       => 'valor_kg',
        'price'          => 'valor',
        'price_with_tax' => 'valor_com_taxa',
        'partial_accept' => 'aceite_parcial',
        'duration'       => 'duracao_lance',
        'user_id'        => 'id_user',
        'accept_date'    => 'data_aceite',
    ];

    /**
     * Accessors & Mutators
     **/

    /**
     * get status description Accessor
     * transform status from int to string
     * @return string
     **/
    public function getStatusDescriptionAttribute()
    {
        $status = (new BidService($this))->getBidStatus($this);

        return $status['label'];
    }

    /*
     * get custom fields Accessor
     * transform status from int to string
     * @return string
     **/
    public function getCustomFieldsAttribute()
    {
        $validate = now()->startOfDay()->diffInDays($this->duration, false);
        $statusApproved = Status::type(Status::APPROVED);

        $shelfLifeDays = '';
        $orderStatus = null;
        $orderProductTotal = null;

        if ($this->order) {
            $orderStatus = $this->order->status_id;
            $orderProductTotal = $this->order->orderProduct->first()->kg_total;
        }

        $isCancelled = $this->status_id === Status::type(Status::CANCELED)->id
           && $orderStatus === Status::type(Status::CANCELED)->id;
        $translateKey = $validate >= 0 ?
            trans_choice('Mobile/order.validate', $validate, ['days' => $validate]) :
            __('Mobile/order.expired');

        $shelfLifeDays = $isCancelled ? '' : $translateKey;

        return [
            'weight' => $this->weight.'Kg',
            'box_amount' => trans_choice('Mobile/order.boxes', $this->box_amount, ['box' => $this->box_amount]),
            'price' => formata_moeda($this->price_with_tax, false),
            'shelf_life' => Carbon::parse($this->duration)->format('d/m/Y'),
            'shelf_life_days' => $shelfLifeDays,
            'partial' => ($this->aceite_parcial) ? (($this->status_id == Status::type(Status::CANCELED)->id) ?
             formata_moeda($orderProductTotal, false).'Kg' : true) : false,
        ];
    }

    /**
     * Relationships
     **/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'cod_produto', 'codigo')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientCard()
    {
        return $this->belongsTo(ClientCard::class, 'client_card_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientCompanyPayment()
    {
        return $this->belongsTo(ClientCompanyPayment::class, 'client_company_payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function editedBy()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    /**
     * @param string $companyId companyId
     * @return null
     */
    public function origin(string $companyId = null)
    {
        $query = $this->client ? $this->client->origins->where('code', $this->origin_code) : null;

        if ($companyId && $query) {
            $query = $query->where('company_id', $companyId);
        }

        return $query ? $query->first() : null;
    }

    /**
     * @param string $companyId $companyId
     * @return mixed
     */
    public function fefo(string $companyId = null)
    {
        $query = $this->product ?
            $this->product->fefo
                ->where('weeks', $this->weeks)
                ->where('leadtime_code', $this->origin_code)
                ->where('date', $this->data) : null;

        if ($companyId && $query) {
            $query = $query->where('company_id', $companyId);
        }

        return $query ? $query->first() : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function order()
    {
        return $this->hasOne(Order::class, 'lance_id', 'id');
    }

    /**
     * Method to relates the bidLog class with bid class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bidLogs()
    {
        return $this->hasMany(BidLog::class);
    }

    /**
     * Company relation
     *
     * @return Company
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Scopes
     **/

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }

    /**
     * Get bid.status approved and order.pending
     * @param Builder $builder Query builder
     * @return Builder
     */
    public function scopePending($builder)
    {
        return $builder
            ->where('lances.status_id', Status::type(Status::PENDING)->id)
            ->leftJoin('pedidos', function ($query) {
                return $query->on('pedidos.lance_id', '=', 'lances.id');
            })
            ->orWhere([
                ['pedidos.status_id', '=', Status::type(Status::PENDING)->id],
                ['lances.status_id', '=', Status::type(Status::APPROVED)->id],
                ['pedidos.lance_id', '!=', null]
            ]);
    }

    /**
     * Get bid.status approved and order.pending
     * @param Builder $builder Query builder
     * @return Builder
     */
    public function scopeApproved($builder)
    {
        return $builder
            ->leftJoin('pedidos', function ($query) {
                return $query->on('pedidos.lance_id', '=', 'lances.id');
            })
            ->where('pedidos.status_id', Status::type(Status::APPROVED)->id)
            ->whereNotNull('pedidos.lance_id')
            ->whereNotNull('pedidos.data_embarque');
    }

    /**
     * Get bid.status approved and order.pending
     * @param Builder $builder Query builder
     * @return Builder
     */
    public function scopeCanceled($builder)
    {
        return $builder
            ->leftJoin('pedidos', function ($query) {
                return $query->on('pedidos.lance_id', '=', 'lances.id');
            })
            ->where('lances.status_id', Status::type(Status::CANCELED)->id)
            ->orWhere(function ($query) {
                return $query->where('lances.status_id', Status::type(Status::APPROVED)->id)
                            ->where('pedidos.status_id', Status::type(Status::CANCELED)->id)
                            ->whereNotNull('pedidos.lance_id');

            });
    }

    /**
     * Check is bid is partially billed. When original_box_amount is bigger then box_amount
     * @param Bid $bid Bid full price
     * @return boolean
     **/
    public function isPartiallyBilled()
    {
        $approved = Status::type(Status::APPROVED);
        $response = false;

        $hasProductPartiallyBilled = $this->order->orderProduct->filter(function ($value) {
            return $value->original_box_amount > $value->box_amount;
        });

        $boxChanged = $this->original_box_amount > $this->box_amount;

        if (($boxChanged || $hasProductPartiallyBilled->isNotEmpty()) && $this->order->status_id == $approved->id) {
            $response = true;
        }

        return $response;
    }

    /**
     * Check if bid is partially billed. When original_box_amount is bigger or less then box_amount
     * @param Bid $bid Bid full price
     * @return boolean
     **/
    public function isBilledAbove()
    {
        $approved = Status::type(Status::APPROVED);

        if($this->order->status_id !== $approved->id ) {
            return false;
        }

        $hasProductBilledAbove = $this->order->orderProduct->filter(function ($value) {
            return $value->original_box_amount < $value->box_amount;
        });

        return $hasProductBilledAbove->isNotEmpty();
    }

    /**
     * Get bid value with tax
     * @return mixed
     */
    public function getPriceWithTaxAttribute()
    {
        if($this->tax_percent  == 0){
            $this->price_with_tax = $this->price;
            return;
        }

        $percent = ($this->tax_percent / 100);

        $taxPrice = ($this->price + ($this->price * $percent)) * $percent;

        $this->price_with_tax = round($this->price + $taxPrice, 2);
    }

    /**
     * Return status
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * Return Rejection Motive
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rejectionMotives()
    {
        return $this->belongsTo(RejectionMotive::class);
    }
}
