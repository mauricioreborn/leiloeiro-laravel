<?php
namespace App\Bid\Services;

use App\Auth\User;
use App\Bid\Bid;
use App\Client\Client;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\Fefo\Services\FefoBidsService;
use App\Stock\Services\StockService;
use Illuminate\Support\Facades\Log;

class BehaviourApprovalService
{
    protected $process = 'behaviourAproval';

    public function getBids(User $soukBot, $date)
    {
        $bids = $this->getBidsWithInteraction($soukBot, $date);
        $response = [];

        foreach($bids as $bid) {
            if($bid->status_id === Status::type(Status::PENDING)->id) {
                continue;
            }

            $thresholdKey = "{$bid->origin_code}.{$bid->product_code}.{$bid->weeks}";
            $statusKey = $bid->status_id === Status::type(Status::CANCELED)->id ? 'rejected' : 'approved';

            if(!isset($response[$thresholdKey][$statusKey])) {
                $response[$thresholdKey][$statusKey] = (float) $bid->kg_price;
                continue;
            }

            if($bid->status_id === Status::type(Status::CANCELED)->id &&
               $response[$thresholdKey][$statusKey] > (float) $bid->kg_price) {
                continue;
            }

            if($bid->status_id === Status::type(Status::APPROVED)->id &&
               $response[$thresholdKey][$statusKey] < (float) $bid->kg_price) {
                continue;
            }

            $response[$thresholdKey][$statusKey] = (float) $bid->kg_price;
        }

        return $response;
    }

    protected function getBidsWithInteraction(User $soukBot, $date)
    {
        $clientExceptions = Client::whereIn('cnpj', config('souk.sandbox_clients'))->pluck('id')->toArray();

        return Bid::with('product')
            ->whereHas('product')
            ->whereIn('status_id', [
                Status::type(Status::CANCELED)->id,
                Status::type(Status::APPROVED)->id,
            ])
            ->where('date', $date)
            ->where('company_id', $soukBot->company_id)
            ->where('id_user', '!=', $soukBot->id)
            ->whereNotIn('client_id', $clientExceptions)
            ->get();
    }

    public function processFefoBids(User $soukBot, $key, $thresholds, $date)
    {
        [$origin, $product, $weeks] = explode('.', $key);

        $fefo = Fefo::where('leadtime_code', $origin)
            ->where('date', $date)
            ->where('product_code', $product)
            ->where('weeks', $weeks)
            ->first();

        if($fefo === null) {
            Log::info("{$this->process}: Fefo não encontrado para chave: {$key}");
            return;
        }

        if(isset($thresholds['rejected'])) {
            $this->rejectBids($soukBot, $fefo, $thresholds['rejected'], $date);
        }

        if(isset($thresholds['approved'])) {
            $this->approveBids($soukBot, $fefo, $thresholds['approved']);
        }
    }

    protected function rejectBids(User $soukBot, Fefo $fefo, $treshold, $date): void
    {
        $key = "{$fefo->leadtime_code}.{$fefo->product_code}.{$fefo->weeks}";

        $bids = Bid::with('product')
            ->whereHas('product')
            ->where('origin_code', $fefo->leadtime_code)
            ->where('status_id', Status::type(Status::PENDING)->id)
            ->where('date', $date)
            ->where('product_code', $fefo->product_code)
            ->where('weeks', $fefo->weeks)
            ->where('kg_price', '<=', $treshold)
            ->get();

        if($bids->count() === 0) {
            return;
        }

        // phpcs:ignore
        Log::info("{$this->process}: {$key}: {$bids->count()} lances encontrados abaixo de R$ {$treshold}");

        $bidIds = $bids->pluck('id')->toArray();

        $motiveId = RejectionMotive::type(RejectionMotive::BEHAVIOUR_REJECTION)->id;
        (new FefoBidsService())->disapprove($fefo, $bidIds, $motiveId, $soukBot->id);

        $ids = implode(', ', $bidIds);
        // phpcs:ignore
        Log::info("{$this->process}: {$key}: Os lances de IDs {$ids} foram rejeitados por comportamento");
    }

    protected function approveBids(User $soukBot, Fefo $fefo, $treshold): void
    {
        $key = "{$fefo->leadtime_code}.{$fefo->product_code}.{$fefo->weeks}";

        $bids = (new FefoBidsService())->getEligibleBids($fefo, $treshold);

        if($bids->count() === 0) {
            return;
        }

        // phpcs:ignore
        Log::info("{$this->process}: {$key}: {$bids->count()} lances encontrados acima de R$ {$treshold}");


        $approvedBidsIds = (new FefoBidsService())
            ->approve(
                $fefo,
                $bids->pluck('id')->toArray(),
                $soukBot->id
            );

        $ids = implode(', ', $approvedBidsIds);
        // phpcs:ignore
        Log::info("{$this->process}: {$key}: Os lances de IDs {$ids} foram aceitos por comportamento");
    }
}