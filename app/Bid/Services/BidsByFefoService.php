<?php
namespace App\Bid\Services;

use App\Bid\Bid;
use App\Company\Company;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\LeadTime\Services\LeadTimeService;

class BidsByFefoService
{
    public $bids;

    protected $company;

    protected $date;

    protected $filters;

    protected $eligibleOnly = true;

    protected $origins = [];

    protected $fefo = [];

    /**
     * BidsByFefoService constructor.
     * @param Company $company
     * @param null    $date Date
     * @param array   $filters
     */
    public function __construct(Company $company, $date = null, $filters = [])
    {
        $this->company = $company;
        $this->setDate($date);
        $this->filters = $filters;
    }

    /**
     * @return $this
     */
    public function fetch(): self
    {
        $this->fetchOrigins();
        $this->fetchBids();
        $this->fetchFefo();
        $this->verifyFefoExists();

        return $this;
    }

    /**
     * @return void
     */
    protected function fetchOrigins(): void
    {
        $this->origins = LeadTime::fromCompany($this->company->id)->listed();
    }

    /**
     * @return void
     */
    protected function fetchBids(): void
    {
        $query = Bid::with('order.orderProduct', 'product.users')
            ->fromCompany($this->company->id)
            ->where('duration', '>=', $this->date)
            ->where('status_id', Status::type(Status::PENDING)->id);
        if (isset($this->filters['product_code'])) {
            $query->where('product_code', $this->filters['product_code']);
        }
        $this->bids = $query->orderBy('kg_price', 'desc')->get();
    }

    /**
     * @return void
     */
    protected function fetchFefo(): void
    {
        $fefoKeys = $this->bids->map(function ($bid) {
            return "{$bid->cod_origem}.{$bid->cod_produto}.{$bid->semanas}";
        })->unique();
        $this->fefo = Fefo::fromCompany($this->company->id)->where('date', $this->date)->where(function ($query) use (
                $fefoKeys
            ) {
                $fefoKeys->map(function ($item) {
                    list($originCode, $productCode, $weeks) = explode('.', $item);

                    return [
                        ['semanas', '=', $weeks],
                        ['cod_origem', '=', $originCode],
                        ['codigo_produto', '=', $productCode],
                    ];
                })->each(function ($item) use (&$query) {
                    return $query->orWhere($item);
                });

                return $query;
            })->orderBy('volume', 'desc')->get()->mapWithKeys(function ($fefo) {
                return ["{$fefo['cod_origem']}.{$fefo['codigo_produto']}.{$fefo['semanas']}" => $fefo];
            });
    }

    /**
     * @return void
     */
    protected function verifyFefoExists(): void
    {
        foreach ($this->bids as $key => $bid) {
            $fefoKey = "{$bid->origin_code}.{$bid->product_code}.{$bid->weeks}";
            if (!isset($this->fefo[$fefoKey]) || $this->fefo[$fefoKey]->volume < 1) {
                $this->bids->forget($key);
            }
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function group(): \Illuminate\Support\Collection
    {
        $bids = $this->bids;
        $response = [];
        foreach ($bids as $bid) {
            if ($this->eligibleOnly && $bid->ineligible_for_logistics) {
                unset($bid);
                continue;
            }
            $slug = "{$bid->weeks}-{$bid->cod_origem}-{$bid->product_code}";
            if (!isset($response[$slug])) {
                $response[$slug] = $bid->toArray();
                $response[$slug]['bids_volume'] = 0;
                $response[$slug]['bids_kg_price'] = 0;
                $response[$slug]['bids_price'] = 0;
                $response[$slug]['bids'] = 0;
                $response[$slug]['client_ids'] = [];
            }
            $response[$slug]['bids_volume'] += $bid->weight;
            $response[$slug]['bids_price'] += (float) $bid->price;
            $response[$slug]['bids_kg_price'] = ($response[$slug]['bids_price'] / $response[$slug]['bids_volume']);
            $response[$slug]['bids']++;
            $response[$slug]['client_ids'][] = $bid->client_id;
        }
        foreach ($response as $key => $bid) {
            $response[$key]['clients'] = collect($bid['client_ids'])->unique()->count();
        }
        $list = [];
        foreach ($response as $bid) {
            $list[] = $bid;
        }

        return collect($list);
    }

    /**
     * @return $this
     */
    public function normalize(): self
    {
        foreach ($this->bids as $bid) {
            $fefoKey = "{$bid->origin_code}.{$bid->product_code}.{$bid->weeks}";
            $bid->product_type = $bid->product->type;
            $bid->product_name = $bid->product->name;
            $bid->product_picture = env('PRODUCT_IMAGES_BASE_URL') . 'souk-company/' . $this->company->id . '/' .
                                    $bid->product->picture;
            $bid->product_seller = null;
            if (isset($bid->product->users) && count($bid->product->users) > 0) {
                $bid->product_seller = $bid->product->users->first()->name;
            }
            if (isset($this->origins[$bid->origin_code])) {
                $bid->cd = $this->origins[$bid->origin_code];
            }
            if (isset($this->fefo[$fefoKey])) {
                $bid->fefo_id = $this->fefo[$fefoKey]['id'];
                $bid->fefo_volume = $this->fefo[$fefoKey]['volume'];
                $bid->fefo_selling_price = $this->fefo[$fefoKey]['selling_price'];
                $bid->fefo_min_price = $this->fefo[$fefoKey]['min_price'];
                $bid->fefo_featured = $this->fefo[$fefoKey]['highlight_app'];
                $bid->faixa_fefo = $this->fefo[$fefoKey]['faixa_fefo'];
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function validateMinimumAcceptablePrice(): self
    {
        $acceptablePercent = $this->company->settings->get('bidMinAcceptablePercent');
        foreach ($this->bids as $bid) {
            if ($bid->status_id !== Status::type(Status::PENDING)->id) {
                continue;
            }
            $bid->minimum_acceptable_price = round($bid->fefo_min_price -
                                                   ($bid->fefo_min_price * $acceptablePercent / 100), 2);
            if ($this->eligibleOnly && (float) $bid->kg_price < (float) $bid->minimum_acceptable_price) {
                $bid->ineligible_for_price = true;
                $bid->ineligible = true;
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function validateFefoVolumes(): self
    {
        $fefoVolumes = [];
        foreach ($this->bids as $key => $bid) {
            if ($bid->status_id !== Status::type(Status::PENDING)->id) {
                continue;
            }
            $slug = "{$bid->weeks}-{$bid->cod_origem}-{$bid->product_code}";
            if (!isset($fefoVolumes[$slug])) {
                $fefoVolumes[$slug] = 0;
            }
            $fefoVolumes[$slug] += $bid->weight;
            if (!$bid->partial_accept && $bid->fefo_volume < $fefoVolumes[$slug]) {
                $bid->can_purchase = null;
                $bid->ineligible_for_volume = true;
                $bid->ineligible = true;
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function validateLogistics(): self
    {
        $leadtimeService = new LeadTimeService();
        $logistics = $leadtimeService->getLogistica($this->bids->pluck('client_id')->unique()->toArray(), false,
            $this->company->id);
        foreach ($this->bids as $bid) {
            if ($bid->status_id !== Status::type(Status::PENDING)->id) {
                continue;
            }
            if (!isset($logistics[$bid->client_id])) {
                $bid->logistics = null;
                $bid->can_purchase = false;
                $bid->ineligible_for_logistics = true;
                $bid->ineligible = true;
                continue;
            }
            $bid->logistics = $logistics[$bid->client_id];
            $bid->can_purchase = $logistics[$bid->client_id]['aceita_compra'];
            if (!$bid->can_purchase) {
                $bid->ineligible_for_logistics = true;
                $bid->ineligible = true;
            }
        }

        return $this;
    }

    /**
     * @param null $date Date
     * @return $this
     */
    public function setDate($date = null): self
    {
        $this->date = $date ?: now()->toDateString();

        return $this;
    }

    /**
     * @param bool $eligibleOnly eligibleOnly param
     * @return $this
     */
    public function setEligibleOnly(bool $eligibleOnly): self
    {
        $this->eligibleOnly = $eligibleOnly;

        return $this;
    }
}
