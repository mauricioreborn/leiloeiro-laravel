<?php

namespace App\Bid\Services;

use App\Bid\Bid;
use App\Client\Services\TransactionalEmailsService;
use App\Company\Company;
use App\Core\Classes\CrudService;
use App\Core\Classes\Search;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\Fefo\Services\FefoBidsService;
use App\LeadTime\Services\LeadTimeService;
use App\Notification\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class BidService extends CrudService
{
    protected $entity = Bid::class;

    protected $module = 'Bid';

    /**
     * @param integer $companyId Company ID
     * @param array   $filters   Filters
     * @return mixed
     */
    public function search($companyId, $filters = [])
    {
        $searchInstance = new Search($this->module, $this->newQuery()->with('order.orderProduct'));

        $query = $searchInstance->run($filters);

        $query->fromCompany($companyId);

        return $query->paginate($filters['limit'] ?? 15);
    }

    /**
     * @param Company $company Company entity
     * @param array   $filters Filters
     * @return mixed
     */
    public function list(Company $company, $filters = [])
    {
        $include = array_get($filters, 'include', '');

        $include = explode(',', $include);

        $includeOrigin = (in_array('origin', $include));
        $includeFefo = (in_array('fefo', $include));

        if ($includeOrigin || $includeFefo) {
            $include = array_diff($include, ['origin', 'fefo']);
            $include = array_unique(array_merge(['client', 'product'], $include));

            $filters['include'] = implode(',', $include);
        }

        $bids = $this->search($company->id, $filters);

        foreach ($bids as $bid) {
            if ($includeOrigin) {
                $bid->getOrigin = $bid->origin($company->id);
            }

            if ($includeFefo) {
                $bid->getFefo = $bid->fefo($company->id);
            }
        }

        return $bids;
    }

    /**
     * bid statistics with filters
     *
     * @param Company $company Company entity
     * @param array   $filters Filters
     *
     * @return array
     */
    public function bidStatistics(Company $company, array $filters = []): array
    {
        $filters['sort'] = '-created_at';

        $bids = $this->search($company->id, $filters)->getCollection();

        $kgPrices = $bids->pluck('kg_price');
        $weights = $bids->pluck('weight');

        $mode = $kgPrices->mode();

        return [
            'min_price' => $kgPrices->min() ?? 0,
            'max_price' => $kgPrices->max() ?? 0,
            'average_price' => $kgPrices->avg() ?? 0,
            'mode_price' => isset($mode) ? reset($mode) : 0,
            'min_volume' => $weights->min() ?? 0,
            'max_volume' => $weights->max() ?? 0,
            'average_volume' => $weights->avg() ?? 0,
        ];
    }

    /**
     * Method to get consolidate order history (with status > 0) per day
     * @param string     $company company_id
     * @param Collection $filters Filters
     * @return Collection
     */
    public function stats(Company $company, $filters): Collection
    {
        $date         = $filters->has('date') ? $filters->get('date') : now()->toDateString();
        $eligibleOnly = ($filters->has('eligibleOnly') && $filters->get('eligibleOnly') === 'true');

        $bids = (new BidsByFefoService($company, $date))
            ->setEligibleOnly($eligibleOnly)
            ->fetch()
            ->normalize()
            ->validateLogistics()
            ->validateMinimumAcceptablePrice()
            ->validateFefoVolumes()
            ->bids;

        $open = $bids->reject(function ($bid) use ($eligibleOnly) {
            return !$bid->can_purchase || ($eligibleOnly && $bid->ineligible);
        });

        $accepted = Bid::select(['total_kg'])
            ->fromCompany($company->id)
            ->whereDate('accept_date', '=', $date)
            ->where('status_id', Status::type(Status::APPROVED)->id)
            ->sum('total_kg');

        return collect([
            'bids' => [
                'open' => $open->count(),
            ],
            'weight' => [
                'open' => $open->sum('weight') ?? 0,
                'accepted' => $accepted,
            ],
        ]);
    }

    /**
     * @param Fefo  $fefo       Fefo instance
     * @param array $conditions Conditions
     * @return mixed
     */
    public function findByCriteria($fefo, $conditions)
    {
        $company = Company::find($fefo->company_id);

        $bids = $this->newQuery()
            ->with(['client' => function ($query) use ($fefo) {
                return $query->withCount(['order' => function ($query) use ($fefo) {
                    return $query->where('company_id', $fefo->company_id);
                }]);
            }, 'product', 'order.orderProduct'])
            ->where($conditions)
            ->whereDate('duration', '>=', Carbon::today())
            ->orderBy('kg_price', 'desc')
            ->get();

        $logistics = (new LeadTimeService())->getLogistica(
            $bids->pluck('client_id')->unique()->toArray(),
            null,
            $fefo->company_id
        );

        $totalVolume = 0;

        $acceptablePercent = $fefo->company->settings->get('bidMinAcceptablePercent');
        $minAcceptablePrice = round($fefo->min_price - ($fefo->min_price * $acceptablePercent / 100), 2);

        foreach ($bids as $bid) {
            $bid->logistics = $logistics[$bid->client_id];
            $bid->can_purchase = $logistics[$bid->client_id]['aceita_compra'];
            $bid->client->clientCompany = $bid->client->setClientCompany($company);

            $bid->original_weight = $bid->weight;
            $bid->minAcceptPrice = true;

            if ($bid->kg_price < $minAcceptablePrice) {
                $bid->can_purchase = false;
                $bid->minAcceptPrice = false;
            }

            if (($totalVolume + $bid->weight) > $fefo->volume) {
                $bid->can_purchase = false;
                $bid->ineligibleVolume = true;
            }

            $volumeAvailable =  $fefo->volume - $totalVolume;

            if ($bid->partial_accept && ($bid->ineligibleVolume) && ($bid->minAcceptPrice) && $volumeAvailable > 0) {
                $minClientVolumeOrder = $bid->client->min_order/$bid->kg_price;

                if ($volumeAvailable >= $minClientVolumeOrder && $volumeAvailable >= $bid->product->weight) {
                    $bid->weight = floor($volumeAvailable/$bid->product->weight) * $bid->product->weight;
                    $bid->price = number_format($bid->weight * $bid->kg_price, 2, '.', '');
                    $bid->box_amount = $bid->weight / $bid->product->weight;
                    $bid->can_purchase = true;
                    $bid->partial_purchase = true;
                }
            }

            if ($bid->can_purchase) {
                $totalVolume += $bid->weight;
            }

            $discountPercentage = $this->calcDiscountPercentage($fefo->max_price, $bid->kg_price);

            $bid->discount_percentage = "{$discountPercentage}%";
            $bid->original_price = $fefo->max_price;

            $labels = [];

            if($bid->client->order_count === 0) {
                $labels[] = [
                    'type' => 'first_purchase',
                    'content' => __('fefoBids.labels.first_purchase'),
                ];
            }

            $bid->labels = $labels;
        }

        return $bids;
    }

    /**
     * @param Fefo $fefo Fefo instance
     * @return void
     */
    public function approveBidsForFefo($fefo)
    {
        $bids = (new FefoBidsService())->getEligibleBids($fefo, $fefo->selling_price);

        $ids = $bids->pluck('id');

        Bid::whereIn('id', $ids)->update(
            [
                'status_id' => Status::type(Status::APPROVED)->id,
                'id_user' => auth()->user()->id ?? null,
                'nao_lido' => 1,
                'data_aceite' => now()->toDateString(),
            ]
        );

        foreach($bids as $bid) {
            (new TransactionalEmailsService)->sendBidEmail($bid->fresh());
        }
    }

    /**
     * List ineligible Bids with fefo, when bids.kg_price is smaller tem 30% of fefo.min_price
     * @param Company $company
     * @param string  $date
     * @return Collection
     */
    public function listIneligibleBids(Company $company, $date = null): Collection
    {
        $date = $date ?: now()->toDateString();

        $filters['date'] = $date;

        $bids = (new BidsByFefoService($company, $date))
            ->fetch()
            ->normalize()
            ->validateLogistics()
            ->validateMinimumAcceptablePrice()
            ->validateFefoVolumes()
            ->bids;

        return $bids->filter(static function ($bid) {
            return $bid->ineligible && $bid->ineligible_for_price;
        });
    }

    /**
     * @param array $bidIds Array of bid ids to disapprove
     */
    public function disapprove($bidIds)
    {
        $bids = Bid::with('product.users')->whereIn('id', $bidIds)->get();

        Bid::whereIn('id', $bidIds)->update([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id,
            'id_user' => auth()->user()->id ?? null,
            'nao_lido' => 1,
            'data_aceite' => now()->toDateString(),
        ]);

        $bids->each(function ($bid) {
            // if (!$bid->product->canBeUpdatedByUser()) {
            //     throw new UnauthorizedException();
            // }

            (new NotificationService())->rejectedBid(
                $bid->client_id,
                $bid->id,
                $bid->product->name,
                $bid->kg_price
            );

            (new TransactionalEmailsService)->sendBidEmail($bid->fresh());
        });

        return Bid::whereIn('id', $bidIds)->update([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id,
            'id_user' => auth()->user()->id ?? null,
            'nao_lido' => 1,
            'data_aceite' => now()->toDateString(),
        ]);
    }

    public function findBid($id, $relations = [], $shouldThrowException = true, ?Company $company  = null)
    {
        if (in_array('logs', $relations)) {
            $relations = array_values(array_diff($relations, ['logs']));

            $logs = (new BidLogService())->getLogsByBid($id);
        }

        $includeOrigin = (in_array('origin', $relations));
        $includeFefo = (in_array('fefo', $relations));

        if ($includeOrigin || $includeFefo) {
            $relations = array_diff($relations, ['origin', 'fefo']);
        }

        $query = parent::find($id, $relations, $shouldThrowException);

        if ($query->relationLoaded('client')) {
            $query->client->companies = $query->client->companies->where('id', $company->id);
            $query->client->clientCompany = $query->client->setClientCompany($company);
        }

        if (isset($logs)) {
            $query->logs = $logs;
        }

        if ($includeOrigin) {
            $query->getOrigin = $query->origin($company->id);
        }

        if ($includeFefo) {
            $query->getFefo = $query->fefo($company->id);
        }

        return $query;
    }

    /**
     * Check for expired bids, update their status and notify clients.
     * @return mixed
     * @throws \Exception
     */
    public function expire()
    {
        $expired = $this->newQuery()
            ->join('produtos', function ($join) {
                $join->on('lances.cod_produto', '=', 'produtos.codigo');
                $join->on('lances.company_id', '=', 'produtos.company_id');
            })
            ->select('lances.id AS lance_id', 'client_id', 'cod_produto', 'lances.duracao_lance')
            ->where('lances.status_id', '=', Status::type(Status::PENDING)->id)
            ->where('duracao_lance', '<', now()->format('Y-m-d'))
            ->get();

        $ids = $expired->pluck('lance_id')->toArray();

        if(count($ids) === 0) {
            return false;
        }

        $idsChunk = array_chunk($ids, 1000);

        foreach($idsChunk as $id){
            $this->newQuery()->whereIn('id', $id)->update([
                'status_id' => Status::type(Status::CANCELED)->id,
                'rejection_motives_id' => RejectionMotive::type(RejectionMotive::EXPIRED)->id,
            ]);
        }

        foreach($expired as $bid) {
            (new NotificationService())->expiredBid(
                $bid->lance_id,
                $bid->id_cliente,
                $bid->product->nome,
                Carbon::createFromFormat('Y-m-d', $bid->duracao_lance)->format('d/m/y')
            );

            $instance = $this->find($bid->lance_id, ['client.companies', 'product'], false);
            (new TransactionalEmailsService)->sendBidEmail($instance);

        }
    }

    /**
     * @param float $fefoFullPrice Fefo full price
     * @param float $bidPrice      Fefo bid price
     * @return string
     */
    public function calcDiscountPercentage($fefoFullPrice, $bidPrice): string
    {
        if($fefoFullPrice === 0 || $bidPrice === 0) {
            return '0.00';
        }

        $difference = $fefoFullPrice - $bidPrice;
        $discount = ($difference * 100) / $fefoFullPrice;

        return number_format($discount, 2, '.', '');
    }
}
