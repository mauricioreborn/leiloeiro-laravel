<?php

namespace App\Bid\Services;

use App\Bid\Bid;
use App\Bid\BidLog;
use App\Mobile\Exceptions\MobileException;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class BidLogService
{

    /**
     * Method to save a bid log
     *
     * @param array $bidLogParams parameters to save a bid
     *
     * @return mixed
     */
    public function saveBidLog(array $bidLogParams)
    {
        try {
            $bid = new BidLog();

            $bidLog['bid_id'] = $bidLogParams['bid_id'];

            if (isset($bidLogParams['user_id'])) {
                $bidLog['user_id'] = $bidLogParams['user_id'];
            }

            if (isset($bidLogParams['client_id'])) {
                $bidLog['client_id'] = $bidLogParams['client_id'];
            }

            $bidLog['box_amount'] = $bidLogParams ['box_amount'];

            return $bid->create($bidLog);

        } catch (Exception $e) {
            Log::critical($e->getMessage());
            return false;
        }
    }

    /**
     * get logs by bid ID
     *
     * @param int $bidId Bid ID
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getLogsByBid($bidId): Collection
    {
        $bid = Bid::with(['bidLogs', 'bidLogs.client', 'bidLogs.user'])->find($bidId);

        $bid->bidLogs->each(function ($bidLog) use ($bid) {
            $bidLog->weight = ($bid->weight / $bid->box_amount) * $bidLog->box_amount;
            $bidLog->price = number_format($bidLog->weight * $bid->kg_price, 2);
            $bidLog->responsible = ($bidLog->client) ? $bidLog->client->name : $bidLog->user->name;
        });

        return $bid->bidLogs;
    }
}