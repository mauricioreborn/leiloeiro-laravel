<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'bids'], function () {
        Route::get('/fefo', 'BidsByFefoController@list');
        Route::get('/ineligible', 'BidController@ineligible');
        Route::put('/disapprove', 'BidController@disapprove');

        Route::get('/', 'BidController@list');
        Route::get('/stats', 'BidController@stats');
        Route::get('/{id}', 'BidController@show');
    });
});
