<?php

namespace App\Bid\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BidStatsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'date_format:Y-m-d',
            'product_code' => 'integer|required_with_all:origin_code|required_with_all:weeks',
            'origin_code' => 'integer|required_with_all:product_code|required_with_all:weeks',
            'weeks' => 'integer|required_with_all:product_code|required_with_all:origin_code',
        ];
    }
}
