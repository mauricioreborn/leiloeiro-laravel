<?php

namespace App\Bid\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
                return [
                    'client_id'      => 'required|integer|min:1',
                    'date'           => 'required|date_format:Y-m-d',
                    'origin_code'    => 'required|integer',
                    'product_code'   => 'required|integer',
                    'weeks'          => 'required|integer',
                    'box_amount'     => 'required|numeric',
                    'kg_price'       => 'required|numeric',
                    'weight'         => 'required|numeric',
                    'price'          => 'required|numeric',
                    'partial_accept' => 'required|numeric',
                    'duration'       => 'required|date_format:Y-m-d',
                    'user_id'        => 'required|integer',
                    'status_id'      => 'required|exists:status,id',
                    'accept_date'    => 'bool',
                ];

            case 'PUT':
                return [
                    'client_id'      => 'integer|min:1',
                    'date'           => 'date_format:Y-m-d',
                    'origin_code'    => 'integer',
                    'product_code'   => 'integer',
                    'weeks'          => 'integer',
                    'box_amount'     => 'numeric',
                    'kg_price'       => 'numeric',
                    'weight'         => 'numeric',
                    'price'          => 'numeric',
                    'partial_accept' => 'numeric',
                    'duration'       => 'date_format:Y-m-d',
                    'user_id'        => 'integer',
                    'status_id'      => '',
                    'accept_date'    => 'bool',
                ];
        }
    }
}
