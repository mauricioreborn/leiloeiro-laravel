<?php

namespace App\Bid\Http\Resources;

use App\Auth\Http\Resources\UserResource;
use App\Client\Http\Resources\ClientResource;
use App\Fefo\Http\Resources\FefoResource;
use App\LeadTime\Http\Resources\LeadTimeResource;
use App\Order\Http\Resources\OrderResource;
use App\Products\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BidLogResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'bid_log',
            'id' => $this->id,
            'attributes' => [
                'price' => $this->price,
                'box_amount' => $this->box_amount,
                'weight' => $this->weight,
                'responsible' => $this->responsible,
                'created_at' => $this->created_at,
            ],
        ];
    }
}
