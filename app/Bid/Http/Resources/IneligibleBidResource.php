<?php

namespace App\Bid\Http\Resources;

use App\Auth\Http\Resources\UserResource;
use App\Client\Http\Resources\ClientResource;
use App\Fefo\Http\Resources\FefoResource;
use App\LeadTime\Http\Resources\LeadTimeResource;
use App\Order\Http\Resources\OrderResource;
use App\Products\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class IneligibleBidResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type'       => 'bid',
            'id'         => $this->id,
            'attributes' => [
                'client_id'               => $this->client_id,
                'date'                    => $this->date,
                'origin_code'             => $this->origin_code,
                'product_code'            => $this->product_code,
                'cd'                      => $this->cd,
                'weeks'                   => $this->weeks,
                'weight'                  => $this->weight,
                'kg_price'                => $this->kg_price,
                'price'                   => $this->price,
                'minimum_acceptable_price' => $this->minimum_acceptable_price,
            ],
        ];
    }
}
