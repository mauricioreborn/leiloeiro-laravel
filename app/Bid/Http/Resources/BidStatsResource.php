<?php

namespace App\Bid\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BidStatsResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'type' => 'bid_stats',
            'attributes' => [
                'bids' => [
                    'open' => $this['bids']['open'],
                ],
                'weight' => [
                    'open' => number_format($this['weight']['open'], 0, ',', '.'),
                    'accepted' => number_format($this['weight']['accepted'], 0, ',', '.'),
                ],
            ],
        ];
    }

}
