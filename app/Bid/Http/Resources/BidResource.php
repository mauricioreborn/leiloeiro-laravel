<?php

namespace App\Bid\Http\Resources;

use App\Auth\Http\Resources\UserResource;
use App\Client\Http\Resources\ClientResource;
use App\Fefo\Http\Resources\FefoResource;
use App\LeadTime\Http\Resources\LeadTimeResource;
use App\Order\Http\Resources\OrderResource;
use App\Products\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BidResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $company    = auth()->user()->company;
        $periodView = $company !== null ? $company->settings['periodView'] : false;
        $periodLabel = $periodView ? $this->weeks . ' ' .
                                     trans_choice("companySetting.{$periodView}", $this->weeks) : false;

        $response = [
            'type' => 'bid',
            'id' => $this->id,
            'attributes' => [
                'validade' => $periodLabel?: null,
                'client_id' => $this->client_id,
                'date' => $this->date,
                'origin_code' => $this->origin_code,
                'product_code' => $this->product_code,
                'weeks' => $this->weeks,
                'box_amount' => $this->box_amount,
                'weight' => $this->weight,
                'kg_price' => (float) $this->kg_price,
                'price' => (float) $this->price,
                'partial_accept' => $this->partial_accept,
                'duration' => $this->duration,
                'user_id' => $this->user_id,
                'status_id' => $this->status_id,
                'rejection_motives_id' => $this->rejection_motives_id,
                'status_description' => $this->status_description,
                'status_message' => $this->status_message,
                'status_class' => $this->status_class,
                'accept_date' => $this->accept_date,
                'custom_fields' => $this->custom_fields,
                'created_at' => $this->created_at,
                'unread' => $this->nao_lido,
                'status_description' => $this->status_description,
            ],
        ];

        $response['relationships'] = [
            'client' => new ClientResource($this->whenLoaded('client')),
            'product' => new ProductResource($this->whenLoaded('product')),
            'order' => new OrderResource($this->whenLoaded('order')),
            'editedBy' => new UserResource($this->whenLoaded('editedBy')),
            'logs' => BidLogResource::collection($this->when($this->logs, $this->logs)),
            'origin' => new LeadTimeResource($this->when($this->getOrigin, $this->getOrigin)),
            'fefo' => new FefoResource($this->when($this->getFefo, $this->getFefo)),
        ];

        return $response;
    }
}
