<?php

namespace App\Bid\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BidFefoResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        $company    = auth()->user()->company;
        $periodView = $company !== null ? $company->settings['periodView'] : false;
        $periodLabel = $periodView ? $this['weeks'] . ' ' . trans_choice("companySetting.{$periodView}", $this['weeks']) : false;

        return [
            'type'       => 'bids_fefo',
            'attributes' => [
                'validade'                  => $periodLabel?: null,
                'weeks'                     => $this['weeks'],
                'product_code'              => $this['product_code'],
                'product_name'              => $this['product_name'],
                'product_picture'           => $this['product_picture'],
                'product_type'              => $this['product_type'],
                'product_seller'            => $this['product_seller'],
                'origin_code'               => $this['origin_code'],
                'cd'                        => $this['cd'],
                'faixa_fefo'                => (string) $this['faixa_fefo'],
                'fefo_id'                   => $this['fefo_id'],
                'fefo_volume'               => (float) number_format($this['fefo_volume'], 2, '.', ''),
                'fefo_selling_price'        => (float) number_format($this['fefo_selling_price'], 2, '.', ''),
                'fefo_min_price'            => (float) number_format($this['fefo_min_price'], 2, '.', ''),
                'fefo_min_acceptable_price' => (float) number_format($this['minimum_acceptable_price'], 2, '.', ''),
                'fefo_featured'             => (bool) $this['fefo_featured'],
                'bids_volume'               => (float) number_format($this['bids_volume'], 2, '.', ''),
                'bids_kg_price'             => (float) number_format($this['bids_kg_price'], 2, '.', ''),
                'bids_price'                => (float) number_format($this['bids_price'], 2, '.', ''),
                'bids'                      => $this['bids'],
                'clients'                   => $this['clients'],
                'client_ids'                => $this['client_ids'],
            ],
        ];
    }
}
