<?php

namespace App\Bid\Http\Validators;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class BidValidator
{

    public function validateSearchFilters($fields)
    {
        $validatorRule = [
            'id'                      => 'integer|min:1',
            'client_id'               => 'integer|min:1',
            'date'                    => 'date_format:Y-m-d',
            'origin_code'             => 'integer',
            'product_code'            => 'integer',
            'weeks'                   => 'integer',
            'box_amount'              => 'numeric',
            'kg_price'                => 'numeric',
            'total_kg'                => 'numeric',
            'price'                   => 'numeric',
            'partial_accept'          => 'numeric',
            'duration'                => 'date_format:Y-m-d',
            'user_id'                 => 'integer',
            'status'                  => 'integer',
            'bid_rejection_status_id' => 'integer',
            'accept_date'             => 'bool',
            'limit'                   => 'integer',
            'sort'                    => 'string',
            'include'                 => 'string',
        ];

        $validator = Validator::make($fields, $validatorRule);

        foreach ($fields as $fieldName => $fieldValue) {

            if (!isset($validatorRule[$fieldName])) {
                $validator->errors()->add($fieldName, "the field does not exist");
            }
        }

        if (count($validator->errors()) > 0) {
            throw new ValidationException($validator->errors(), $validator->errors());
        }
    }
}
