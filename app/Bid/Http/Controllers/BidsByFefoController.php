<?php

namespace App\Bid\Http\Controllers;

use App\Bid\Http\Resources\BidFefoResource;
use App\Bid\Services\BidsByFefoService;
use App\Core\Entities\Status;
use App\Core\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BidsByFefoController extends Controller
{
    /**
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     */
    public function list(Request $request): AnonymousResourceCollection
    {
        $date = $request->has('date') ? $request->date : now()->toDateString();
        $eligibleOnly = $request->has('eligibleOnly') ? $request->eligibleOnly : true;

        $bids = (new BidsByFefoService(auth()->user()->company, $date, $request->except(['date', 'eligibleOnly'])))
            ->setEligibleOnly($eligibleOnly)
            ->fetch()
            ->normalize()
            ->validateLogistics()
            ->validateMinimumAcceptablePrice()
            ->validateFefoVolumes()
            ->group();

        return BidFefoResource::collection($bids);
    }
}
