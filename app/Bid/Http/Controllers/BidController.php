<?php

namespace App\Bid\Http\Controllers;

use App\Bid\Http\Requests\BidRequest;
use App\Bid\Http\Requests\BidStatsRequest;
use App\Bid\Http\Resources\BidResource;
use App\Bid\Http\Resources\BidStatsResource;
use App\Bid\Http\Resources\IneligibleBidResource;
use App\Bid\Services\BidService;
use App\Core\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;
use Predis\Connection\ConnectionException;

/**
 * Class BidController
 * @package App\Bid\Http\Controllers
 */
class BidController extends Controller
{

    /**
     * @var BidService
     */
    protected $bidService;

    /**
     * BidController constructor.
     * @param BidService $bidService Bid Service
     */
    public function __construct(BidService $bidService)
    {
        $this->bidService = $bidService;
    }

    /**
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     */
    public function list(Request $request): AnonymousResourceCollection
    {
        $collection = $this->bidService->list(auth()->user()->company, $request->all());

        return BidResource::collection($collection);
    }

    public function show(Request $request, $id): BidResource
    {
        $relations = $request->has('include') ? explode(',', $request->get('include')) : [];
        $shouldThrowException = true;

        return new BidResource($this->bidService->findBid(
            $id,
            $relations,
            $shouldThrowException,
            $request->user()->company
        ));
    }

    /**
     * Method to get stats on bid
     * @param BidStatsRequest $request Request object
     * @return BidStatsResource
     */
    public function stats(BidStatsRequest $request): BidStatsResource
    {
        $stats = $this->bidService->stats($request->user()->company, collect($request->all()));
        return new BidStatsResource($stats);
    }

    /**
     * List invalid bids with margin of 30%
     * @param Request $request Request
     * @return AnonymousResourceCollection
     **/
    public function ineligible(Request $request): AnonymousResourceCollection
    {
        $date = $request->has('date') ? $request->date : now()->toDateString();

        $bids = $this->bidService->listIneligibleBids($request->user()->company, $date);

        return IneligibleBidResource::collection($bids);
    }

    /**
     * @param Request $request Request object
     * @return array
     */
    public function disapprove(Request $request)
    {
        $this->bidService->disapprove($request->get('bid_ids'));

        return [
            'status' => 200,
            'title' => __('Mobile/bid.bids.disapprove.success')
        ];
    }
}
