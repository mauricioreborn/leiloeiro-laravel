<?php


namespace App\Bid\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Product_code implements FilterInterface
{
    /**
    * weeks
    *
    * @param query  $query Query
    * @param string $value string
    *
    * @return query
    */
    public function run($query, $value)
    {
        return $query->where('product_code', $value);
    }
}
