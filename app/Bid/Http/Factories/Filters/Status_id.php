<?php


namespace App\Bid\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Status_id implements FilterInterface
{
    public function run($query, $value)
    {
        if(is_array($value)) {
            return $query->whereIn('status_id', $value);
        }
        
        return $query->where('status_id', $value);
    }
}
