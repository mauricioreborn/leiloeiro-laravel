<?php

namespace App\Bid\Http\Factories\Filters;


use App\Core\Contracts\FilterInterface;

class Id implements FilterInterface
{

    public function run($query, $value)
    {
        return $query->where('id', $value);
    }
}
