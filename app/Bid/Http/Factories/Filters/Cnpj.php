<?php

namespace App\Bid\Http\Factories\Filters;


use App\Client\Client;
use App\Core\Contracts\FilterInterface;

class Cnpj implements FilterInterface
{

    public function run($query, $value)
    {
        $value = sanitize_number($value);
        $ids = Client::where('cnpj', 'LIKE', "%{$value}%")
            ->pluck('id')
            ->toArray();

        return $query->whereIn('id_cliente', $ids);
    }
}
