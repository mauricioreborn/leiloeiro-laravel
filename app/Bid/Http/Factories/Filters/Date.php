<?php


namespace App\Bid\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Carbon\Carbon;

class Date implements FilterInterface
{

    public function run($query, $date)
    {
        $date = Carbon::parse($date);

        return $query->where('date', $date);
    }
}
