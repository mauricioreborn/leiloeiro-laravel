<?php

namespace App\Bid\Http\Factories\Filters;


use App\Core\Contracts\FilterInterface;

class Product implements FilterInterface
{

    public function run($query, $value)
    {
        $codes = \App\Products\Product::where('name', 'LIKE', "%{$value}%")->pluck('code')->toArray();

        return $query->whereIn('product_code', $codes);
    }
}
