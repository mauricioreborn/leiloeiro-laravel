<?php


namespace App\Bid\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Kg_price implements FilterInterface
{

    public function run($query, $value)
    {
        return $query->where('kg_price', $value);
    }
}
