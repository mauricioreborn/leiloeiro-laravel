<?php


namespace App\Bid\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Weeks implements FilterInterface
{
    /**
    * weeks
    *
    * @param query  $query Query
    * @param string $value string
    *
    * @return query
    */
    public function run($query, $value)
    {
        return $query->where('weeks', $value);
    }
}
