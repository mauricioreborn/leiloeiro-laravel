<?php

namespace App\Bid\Http\Factories\Filters;


use App\Core\Contracts\FilterInterface;
use App\Order\Order;

class Order_code implements FilterInterface
{

    public function run($query, $value)
    {
        $ids = Order::where('order_code', 'LIKE', "%{$value}%")
            ->where('lance_id', '<>', null)
            ->pluck('lance_id')
            ->toArray();

        return $query->whereIn('id', $ids);
    }
}
