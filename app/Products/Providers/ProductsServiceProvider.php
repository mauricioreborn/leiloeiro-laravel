<?php

namespace App\Products\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

use App\Products\Product;
use App\Products\Http\Policies\ProductCompanyPolicy;
use App\Products\Http\Policies\ProductActionPolicy;

class ProductsServiceProvider extends ServiceProvider
{

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Products\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Gate::policy(Product::class, ProductCompanyPolicy::class);
        Gate::policy(Product::class, ProductActionPolicy::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware(['api', 'validateJwt', 'sentry'])
            ->namespace($this->namespace)
            ->group(base_path('app/Products/Http/routes.php'));
    }
}
