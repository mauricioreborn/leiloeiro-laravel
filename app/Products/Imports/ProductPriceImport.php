<?php

namespace App\Products\Imports;

use App\Core\Entities\Status;
use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\Products\Services\ProductPriceService;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Validators\Failure;

class ProductPriceImport implements
    ToModel,
    WithHeadingRow,
    WithCustomCsvSettings,
    WithValidation,
    SkipsOnFailure,
    WithEvents
{
    use Importable, SkipsFailures, RegistersEventListeners;

    protected $productPriceService;
    protected $fileQueue;
    protected $errors = [];

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);

        $this->productPriceService = new ProductPriceService();
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;

            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     */
    public function model(array $row)
    {
        $sku = array_get($row, 'sku');
        $state = array_get($row, 'uf_destino');
        $price = floatval(str_replace(',', '.', str_replace('.', '', array_get($row, 'repasse_tab'))));

        $this->productPriceService->processProductPrice($this->fileQueue->company, $sku, $state, $price);
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        $companyId = $this->fileQueue->company_id;

        return [
            'uf_destino' => 'required',
            'sku' => "required|exists:produtos,codigo,deleted_at,NULL,company_id,{$companyId}",
            'repasse_tab' => 'required',
        ];
    }

    /**
     * get csv settings
     *
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';',
            'input_encoding' => 'UTF-8',
            'enclosure' => '"',
        ];
    }
}
