<?php

namespace App\Products\Imports;

use App\Core\Entities\Status;
use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\Products\Services\ProductService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Validators\Failure;

class ProductsImport implements
    ToModel,
    WithHeadingRow,
    WithCustomCsvSettings,
    WithValidation,
    SkipsOnFailure,
    WithEvents
{
    protected $productService;
    protected $fileQueue;
    protected $errors = [];

    use Importable, SkipsFailures, RegistersEventListeners;

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);

        $this->productService = new ProductService();
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;

            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     */
    public function model(array $row)
    {
        $this->productService->processProduct($row, $this->fileQueue->company);
    }

    /**
     * get csv settings
     *
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'descricao' => 'required',
            'status_sap' => 'required',
            'codigo' => 'required',
            'categoria' => 'required',
            'empresa' => 'required',
            'peso_bruto_caixa' => 'required',
            'peso_bruto_unidade' => 'required',
            'fator_de_conversao_caixa' => 'required'
        ];
    }

    /**
     * validation messages
     *
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            'descricao.required' => 'O campo descricao é obrigatório.',
            'status_sap.required' => 'O campo status_sap é obrigatório.',
            'codigo.required' => 'O campo codigo é obrigatório.',
            'categoria.required' => 'O campo categoria é obrigatório.',
            'empresa.required' => 'O campo empresa é obrigatório.',
            'peso_bruto_caixa.required' => 'O campo peso_bruto_caixa é obrigatório.',
            'peso_bruto_unidade.required' => 'O campo peso_bruto_unidade é obrigatório.',
            'fator_de_conversao_caixa.required' => 'O campo fator_de_conversao_caixa é obrigatório.'
        ];
    }
}
