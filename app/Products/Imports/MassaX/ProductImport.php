<?php

namespace App\Products\Imports\MassaX;

use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;

use App\Core\Entities\Status;
use App\FileQueue\Services\FileQueueService;
use App\Products\Services\ProductService;
use App\FileQueue\FileQueue;

class ProductImport implements ToCollection, WithHeadingRow, WithCustomCsvSettings, WithEvents
{
    use Importable, RegistersEventListeners;

    protected $fileQueue;
    protected $productService;
    protected $errors = [];

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);
        $this->productService = new ProductService;
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) === static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;
            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * get csv settings
     *
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }


    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     */
    public function collection(Collection $rows)
    {
        $errors = [];

        $validRows = $rows->filter(function ($row) {
            $emptyColumnsCount = 0;

            foreach ($row as $column => $value) {
                if (array_key_exists($column, $this->rules()) && !$value) {
                    $emptyColumnsCount++;
                }
            }
            return $emptyColumnsCount !== count(array_keys(self::rules()));
        });

        foreach ($validRows as $rowNumber => $row) {

            $validation = Validator::make($row->toArray(), self::rules());

            if ($validation->fails()) {
                $errors[$rowNumber + $this->headingRow() + 1] = $validation->errors()->getMessageBag()->all();
                continue;
            }

            $importData = $this->normalizeRow($row->toArray());
            // dd($importData);
            if(!$importData['weight']) {
                $errors[$rowNumber + $this->headingRow() + 1] = [
                    __('import.invalid_weight')
                ];
                continue;
            }

            $this->productService->integrateProduct($this->fileQueue->company, $importData);
        }

        if (count($errors) > 0) {
            $this->logErrors($errors, true, $rows->count());
        }
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'codigo' => 'required',
            'nome_c_variante' => 'required',
            'area_supermercado' => 'required',
            'familia' => 'required',
            'marca' => 'required',
            'submarca' => 'required',
            'unidade_de_medida' => 'required',
            'peso_caixa_em_kg' => 'required',
            'gramatura' => 'required',
            'unidades_caixa' => 'required',
            'exibicao_app' => 'required',
        ];
    }

    /**
     * @param array $errors errors array
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function logErrors($errors, $withImplode = true, $totalRows)
    {
        $this->fileQueue->update([
            'status_id' => Status::type(Status::WITH_ERRORS)->id
        ]);

        $errorMessages = [];

        foreach ($errors as $row => $messages) {

            $message =  $withImplode == true ? implode(', ', $messages) : $messages;
            $message = "Linha {$row}: {$message}";
            $errorMessages[] = $message;
            Log::info($message);

        }

        if(($totalRows * 0.1) <= count($errors)){
            Log::channel('slack')->error("Import de Produtos Tirolez com quantidade excessiva de erros (>10%)");
        }

        return (new FileQueueService)->createLog($this->fileQueue, $errorMessages);
    }

    protected function normalizeRow($row)
    {
        foreach($row as $index => $field) {
            $row[$index] = trim($field);
        }

        $row['codigo'] = ltrim($row['codigo'], 0);

        return [
            'company_id' => $this->fileQueue->company_id,
            'company_division' => 98,
            'name' => $row['nome_c_variante'],
            'code' => $row['codigo'],
            'category' => $row['area_supermercado'],
            'type' => $row['familia'],
            'brand' => $row['marca'],
            'subbrand' => $row['submarca'],
            'unit' => $row['unidade_de_medida'],
            'weight' => $row['peso_caixa_em_kg'],
            'weight_piece' => $row['gramatura'],
            'pieces' => $row['unidades_caixa'],
            'foto' => "{$row['codigo']}.png",
            'app_exhibition' => $row['exibicao_app'],
            'price_increase' => null,
        ];
    }

    protected function getProductWeight($row)
    {
        if($row['peso_base'] > 0) {
            return $row['peso_base'];
        }

        if($row['peso_min'] === 0 || $row['peso_max'] === 0) {
            return false;
        }

        return ($row['peso_min'] + $row['peso_max']) / 2;
    }

    protected function getAppExhibitionText($row)
    {
        $row['gramatura'] *= 1000;

        if($row['peso_base'] > 0) {
            return "{$row['peso_base']}kg({$row['unidade_caixa']}x{$row['gramatura']}g)";
        }

        return "{$row['peso_min']}kg - {$row['peso_max']}kg({$row['unidade_caixa']}x{$row['gramatura']}g)";
    }

}
