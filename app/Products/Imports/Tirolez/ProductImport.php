<?php

namespace App\Products\Imports\Tirolez;

use App\Core\Entities\Status;
use App\FileQueue\Services\FileQueueService;
use App\Products\Services\ProductService;
use Illuminate\Support\Collection;
use App\FileQueue\FileQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Illuminate\Support\Facades\Validator;

class ProductImport implements ToCollection, WithHeadingRow, WithCustomCsvSettings, WithEvents
{
    use Importable, RegistersEventListeners;

    protected $fileQueue;
    protected $productService;
    protected $errors = [];

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);
        $this->productService = new ProductService;
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) === static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;
            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * get csv settings
     *
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }


    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     */
    public function collection(Collection $rows)
    {
        $errors = [];

        $validRows = $rows->filter(function ($row) {
            $emptyColumnsCount = 0;

            foreach ($row as $column => $value) {
                if (array_key_exists($column, $this->rules()) && !$value) {
                    $emptyColumnsCount++;
                }
            }
            return $emptyColumnsCount !== count(array_keys(self::rules()));
        });

        foreach ($validRows as $rowNumber => $row) {

            $validation = Validator::make($row->toArray(), self::rules());

            if ($validation->fails()) {
                $errors[$rowNumber + $this->headingRow() + 1] = $validation->errors()->getMessageBag()->all();
                continue;
            }

            $importData = $this->normalizeRow($row->toArray());

            if(!$importData['weight']) {
                $errors[$rowNumber + $this->headingRow() + 1] = [
                    __('import.invalid_weight')
                ];
                continue;
            }

            $this->productService->integrateProduct($this->fileQueue->company, $importData);
        }

        if (count($errors) > 0) {
            $this->logErrors($errors, true, $rows->count());
        }
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'codigo' => 'required',
            'nome_cvariate' => 'required',
            'segmento' => 'required',
            'familia' => 'required',
            'marca' => 'required',
            'submarca' => 'required',
            'unidade_de_medida' => 'required',
            'peso_min' => 'required',
            'peso_max' => 'required',
            'peso_base' => 'required',
            'gramatura' => 'required',
            'unidade_caixa' => 'required',
        ];
    }

    /**
     * @param array $errors errors array
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function logErrors($errors, $withImplode = true, $totalRows)
    {
        $this->fileQueue->update([
            'status_id' => Status::type(Status::WITH_ERRORS)->id
        ]);

        $errorMessages = [];

        foreach ($errors as $row => $messages) {

            $message =  $withImplode == true ? implode(', ', $messages) : $messages;
            $message = "Linha {$row}: {$message}";
            $errorMessages[] = $message;
            Log::info($message);

        }

        if(($totalRows * 0.1) <= count($errors)){
            Log::channel('slack')->error("Import de Produtos Tirolez com quantidade excessiva de erros (>10%)");
        }

        return (new FileQueueService)->createLog($this->fileQueue, $errorMessages);
    }

    protected function normalizeRow($row)
    {
        foreach($row as $index => $field) {
            $row[$index] = trim($field);
        }

        $row['codigo'] = ltrim($row['codigo'], 0);

        return [
            'company_id' => $this->fileQueue->company_id,
            'company_division' => 99,
            'name' => $this->getNormalizedName($row['nome_cvariate']),
            'code' => $row['codigo'],
            'category' => $row['segmento'],
            'type' => $row['familia'],
            'brand' => $row['marca'],
            'subbrand' => $row['submarca'],
            'unit' => $row['unidade_de_medida'],
            'weight' => $this->getProductWeight($row),
            'weight_piece' => $row['gramatura'],
            'pieces' => $row['unidade_caixa'],
            'foto' => "{$row['codigo']}.png",
            'app_exhibition' => $this->getAppExhibitionText($row),
            'price_increase' => 7.00,
        ];
    }

    protected function getProductWeight($row)
    {
        if($row['peso_base'] > 0) {
            return $row['peso_base'];
        }

        if($row['peso_min'] === 0 || $row['peso_max'] === 0) {
            return false;
        }

        return ($row['peso_min'] + $row['peso_max']) / 2;
    }

    protected function getAppExhibitionText($row)
    {
        $row['gramatura'] *= 1000;

        if($row['peso_base'] > 0) {
            return "{$row['peso_base']}kg({$row['unidade_caixa']}x{$row['gramatura']}g)";
        }

        return "{$row['peso_min']}kg - {$row['peso_max']}kg({$row['unidade_caixa']}x{$row['gramatura']}g)";
    }

    protected function getNormalizedName($productName)
    {
        $normalizedNames = [
            'BRIE (FRACAO CARTUCHO 125G)10UN' => 'Brie Fração 125g Cartucho',
            'BRIE FORMA (PECA INTEIRA 1,0KG)02UN' => 'Brie Forma 1,0Kg  Peça Inteira',
            'CAMEMBERT (COM CARTUCHO 125G)10UN' => 'Camembnert 125g Com Cartucho',
            'COALHO (ABRE-FACIL 220G) 24UN' => 'Coalho 220g Abre Fácil',
            'COTTAGE (PT 200G)16UN' => 'Cottage 200g',
            'COTTAGE (PT 400G)12UN' => 'Cottage 400g',
            'COTTAGE DUO GOIABA (PT 100G)16UN' => 'Cottage Duo Goiaba 100g',
            'COTTAGE DUO MACA/CANELA (PT 100G)16UN' => 'Cottage Duo Maça/Canela 100g',
            'COTTAGE DUO MORANGO (PT 100G)16UN' => 'Cottage Duo Morango 100g',
            'COTTAGE Z.LAC(PT 200GR)12UN' => 'Cottage Zero Lactose 100g',
            'CREME DE LEITE (GARRAFA 1,01KG) 12UN' => 'Creme de Leite 1,01Kg ',
            'CREME DE LEITE (GARRAFA 500G)12UN' => 'Creme de Leite 500g',
            'CREME DE LEITE(BOMBONA 5,0KG) 02UN' => 'Creme de Leite 5.0Kg',
            'CREME DE RICOTA (BALDE 3,6 KG)04UN' => 'Creme de Ricota 3,6Kg',
            'CREME DE RICOTA (PT 200G) 16UN' => 'Creme de Ricota 200g',
            'CREME DE RICOTA LIGHT (PT 200G) 16UN' => 'Creme de Ricota Light 200g',
            'CREME DE RICOTA Z.LAC (PT150G)18UN' => 'Creme de Ricota 150g Zero Lactose',
            'CREME MINAS FRESCAL (PT 200G) 16UN' => 'Creme Minas Frescal 200g',
            'CREME MINAS FRESCAL LIGHT (PT 200G) 16UN' => 'Creme Minas Frescal 200g Light',
            'CREME MINAS FRESCAL Z.LAC(PT 130G)18UN' => 'Creme Minas Frescal 130g Zero Lactose',
            'EDAM (1,8KG)04UN' => 'Edam 1,8Kg',
            'EDAM (FRACAO ABRE-FACIL 180G) 20UN' => 'Edam Fração 180g Abre Fácil',
            'EMMENTAL (13,0KG)01UN' => 'Emmental 13Kg',
            'EMMENTAL (FRACAO ABRE-FACIL 190G) 18UN' => 'Emmental Fração 190g Abre Fácil',
            'ESTEPE (7,0KG)01UN' => 'Estepe 7Kg',
            'ESTEPE (FRACAO ABRE-FACIL 233G) 18UN' => 'Estepe Fração 233g Abre Fácil',
            'FONDUE (CART. 400G)12UN' => 'Fondue 400g Cartucho',
            'FONDUE (POUCHE 250G)20UN' => 'Fondue 250g Pouche',
            'GORGONZOLA (3,0KG)02UN' => 'Gorgonzola 3,0Kg',
            'GORGONZOLA (FRACAO AL.200G)32UN' => 'Gorgonzola Fração 200g AL',
            'GOUDA (3,0KG)02UN' => 'Gouda 3Kg',
            'GOUDA (FRACAO ABRE-FACIL 188G) 18UN' => 'Gouda Fração 188g Abre Fácil',
            'GRUYERE (13,0KG)01UN' => 'Gruyere 13Kg',
            'GRUYERE (FRACAO ABRE-FACIL 190G) 20UN' => 'Gruyere Fração 190g Abre Fácil',
            'IOGURTE BAUNILHA NUTRI+WHEY20G (GFA 250G)12UN' => 'Iogurte Bau 250g Nutri +Whey20g',
            'IOGURTE MORANGO NUTRI+WHEY20G (GFA 250G)12UN' => 'Iogurte Mor 250g Nutri +Whey20g',
            'IOGURTE TRAD. NUTRI+WHEY20G (GFA 250G)12UN' => 'Iogurte Trad 250g Nutri+Whey20g',
            'MANTEIGA COM SAL (POTE 500G)12UN' => 'Manteiga com Sal 500g',
            'MANTEIGA COM SAL (PT 200G)12UN' => 'Manteiga com Sal 200g',
            'MANTEIGA COMUM (CAIXA 5,0KG)01UN' => 'Manteiga Comum 5Kg Caixa',
            'MANTEIGA SEM SAL (PT 200G)12UN' => 'Manteiga sem Sal 200g',
            'MANTEIGA SEM SAL (PT 500G)12UN' => 'Manteiga sem Sal 500g',
            'MINAS FRESCAL (3,0KG)04UN' => 'Minas Frescal 3kg',
            'MINAS FRESCAL (ABRE-FACIL 270G)24UN' => 'Minas Frescal 270g Abre Fácil',
            'MINAS FRESCAL (ABRE-FACIL 500G)16UN' => 'Minas Frescal 500g Abre Fácil',
            'MINAS FRESCAL LIGHT (ABRE-FACIL 270G)24UN' => 'Minas Frescal 270g Light Abre Fácil',
            'MINAS FRESCAL Z.LAC(ABRE-FACIL 270G)16UN' => 'Minas Frescal 270g Zero Lac Abre Fácil',
            'MINAS MEIA CURA (600G)12UN' => 'Minas Meia Cura 600g',
            'MINAS PADRAO (4,0KG)04UN' => 'Minas Padrão 4Kg',
            'MINAS PADRAO (FRACAO 500G)16UN' => 'Minas Padrão Fração 500g',
            'MINAS PADRAO LIGHT (FRACAO 350G)12UN' => 'Minas Padrão Light 350g Fração',
            'MINAS PADRAO LIGHT Z.LAC(FRACAO 350G)12UN' => 'Minas Padrão Light 350g Z. Lac Abre Fácil',
            'MUSSARELA (2,0KG)06UN' => 'Mussarela 2Kg',
            'MUSSARELA (3,6KG)06UN' => 'Mussarela 3,6Kg',
            'MUSSARELA (4,0KG)2UN' => 'Mussarela 4,0Kg',
            'MUSSARELA (ABRE-FACIL 380G) 24UN' => 'Mussarela 380g Abre Fácil',
            'MUSSARELA (FATIADO 150G)20UN' => 'Mussarela 150g Fatiado',
            'MUSSARELA LIGHT (3,6KG)04UN' => 'Mussarela Light 3,6Kg',
            'MUSSARELA LIGHT (ABRE-FACIL 380G) 24UN' => 'Mussarela Light 380g Abre Fácil',
            'MUSSARELA LIGHT (FATIADO 150G)20UN' => 'Mussarela Light 150g Fatiada',
            'MUSSARELA LIGHT Z.LAC (ABRE-FACIL 380G) 24UN' => 'Mussarela Light 380g Z. Lac Abre Fácil',
            'MUSSARELA LIGHT Z.LAC(3,6KG)4UN' => 'Mussarela Light 3,6Kg Zero Lactose',
            'MUSSARELA LIGHT Z.LAC(FATIADO 150G)20UN' => 'Mussarela Light 150g Z. Lac Fatiada',
            'PARMESAO (7,0KG)01UN' => 'Parmesão 7Kg',
            'PARMESAO (FRACAO 245G)30UN' => 'Parmesão 245g Fração',
            'PARMESAO MONTANHES (7,0KG)01UN' => 'Parmesão Montanhês 7Kg',
            'PARMESAO RALADO (100G)60UN' => 'Parmesão Ralado 100g',
            'PARMESAO RALADO (50G)60UN' => 'Parmesão Ralado 50g',
            'PRATO (2,0KG)06UN' => 'Prato 2Kg',
            'PRATO (3,6KG)2UN' => 'Prato 3,6Kg',
            'PRATO (ABRE-FACIL 380G) 24UN' => 'Prato 380g Abre Fácil',
            'PRATO (FATIADO 150G)20UN' => 'Prato 150g Fatiado',
            'PRATO ESFERICO (1,8KG)04UN' => 'Prato Esférico 1,8Kg',
            'PRATO ESFERICO (FRACAO ABRE-FACIL 180G) 20UN' => 'Prato Esférico 180g Fração Abre Fácil',
            'PRATO LIGHT (ABRE-FACIL 380G) 24UN' => 'Prato Light 380g Abre Fácil',
            'Prato Light Fatiado 150g 20Un' => 'Prato Light Fatiado 150g 20Un',
            'PRATO LIGHT Z.LAC (ABRE-FACIL 380G) 24UN' => 'Prato Light 380g Z. Lac Abre Fácil',
            'PRATO LIGHT Z.LAC(FATIADO 150G)20UN' => 'Prato Light 150g Z. Lac Fatiado',
            'PROVOLONE (10,0KG)01UN' => 'Provolone 10Kg',
            'PROVOLONE (20,0KG)01UN' => 'Provolone 20Kg',
            'PROVOLONE (5,0KG)01UN' => 'Provolone 5Kg',
            'PROVOLONE (CRY 5,0KG)01UN' => 'Provolone 5Kg Cry',
            'Provolone 750g' => 'Provolone 750g',
            'Provolone Fração 360g' => 'Provolone 360g Fração',
            'PROVOLONETE (335G)15UN' => 'Provolonete 335g',
            'QUEIJO MINI BABYBEL (110G)12UN' => 'Queijo Mini 110g Babybel',
            'QUEIJO MINI BABYBEL LIGHT (100G)12UN' => 'Queijo Mini Light 100g Babybel',
            'Prato 3,6Kg' => 'Prato 3,6Kg',
            'Prato Light 3,6Kg' => 'Prato Light 3,6Kg',
            'Prato Light 3,6Kg Zero Lactose' => 'Prato Light 3,6Kg Zero Lactose',
            'QUEIJO PROC.SABOR CHEDDAR (BISNAGA 1,5KG)10UN' => 'Queijo Proc.1,5Kg Sabor Cheddar',
            'QUEIJO PROC.VACA QUE RI (128G)48UN' => 'Queijo Proc. 128g Vaca Que Ri',
            'QUEIJO PROC.VACA QUE RI LIGHT (140G)24UN' => 'Queijo Proc. Light 140g Vaca Que Ri ',
            'QUEIJO PROC.VACA QUE RI PALITOS (140G)12UN' => 'Queijo Proc. 140g Palitos Vaca Que Ri',
            'REINO (CRY 1,8KG)04UN' => 'Reino 1,8Kg Cry',
            'REINO (FRACAO 180G)20UN' => 'Reino 180g Fração',
            'REINO (FRACAO ABRE-FACIL 180G) 20UN' => 'Reino 180g Fração Abre Fácil',
            'REINO (LATA 600G)12UN' => 'Reino 600g Lata',
            'REINO (LATA 900G)04UN' => 'Reino 900g Lata',
            'REQUEIJAO (BALDE 3,6KG)04UN' => 'Requeijão Balde 3,6Kg',
            'REQUEIJAO (BISNAGA 1,8KG)08UN' => 'Requeijão 1,8Kg',
            'REQUEIJAO (COPO 200G)24UN' => 'Requeijão 200g',
            'REQUEIJAO (POUCHE 250G)20UN' => 'Requeijão 250g',
            'REQUEIJAO (POUCHE 400G)18UN' => 'Requeijão 400g',
            'REQUEIJAO (SACO 10,0KG)02UN' => 'Requeijão 10kg Saco',
            'REQUEIJAO LIGHT (COPO 200G)12UN' => 'Requeijão Light 200g',
            'REQUEIJAO LIGHT (POUCHE 250G)20UN' => 'Requeijão Light 250g',
            'REQUEIJAO LIGHT FONT FIBR (CP200G)12UN' => 'Requeijão Light 200g Fonte de Fibras',
            'REQUEIJAO LIGHT NUTRI+ FIBRAS (200G) 12UN' => 'Requeijão Light 200g Nutri+ Fibras',
            'REQUEIJAO LIGHT Z.LAC(COPO 200GR)12UN' => 'Requeijão Light 200g Zero Lactose',
            'RICOTA (3,0KG)08UN' => 'Ricota 3Kg',
            'RICOTA (500G)12UN' => 'Ricota 500g',
        ];

        if(array_key_exists($productName, $normalizedNames)) {
            return $normalizedNames[$productName];
        }

        return $productName;
    }
}
