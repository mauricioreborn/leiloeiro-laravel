<?php

namespace App\Products\Services;

use App\Company\Company;
use App\Core\Classes\CrudService;
use App\Products\Product;

class ProductService extends CrudService
{
    protected $entity = Product::class;

    protected $module = 'Products';

    protected $model;

    /**
     * Constructor.
     * @return void
     */
    public function __construct()
    {
        $this->model = new Product;
    }

    /**
     * Method to get product by sku
     * @param integer $sku     product code
     * @param Company $company company object
     * @return mixed
     */
    public function getProductBySku($sku, Company $company)
    {
        return Product::where([
            'codigo' => $sku,
            'company_id' => $company->id
        ])->first();
    }

    /**
     * Process Products
     **/
    /**
     * Process Products
     * @param array   $fields  fields
     * @param Company $company Company entity
     *
     * @return null|Product
     */
    public function processProduct($fields, Company $company)
    {
        $pieces = str_replace(',','.', $fields['fator_de_conversao_caixa']);
        $weightPiece = str_replace(',','.', $fields['peso_liquido_unidade']) / 1000;

        $weight = $pieces * $weightPiece;

        $data = [
            'company_id' => $company->id,
            'company_division' => 99,
            'name' => $fields['descricao'],
            'code' => $fields['codigo'],
            'category' => $fields['categoria'],
            'type' => $fields['categoria'],
            'brand' => $fields['empresa'],
            'subbrand' => $fields['empresa'],
            'unit' => 'KG',
            'weight' => $weight,
            'weight_piece' => $weightPiece,
            'pieces' => $pieces,
            'foto' => $fields["codigo"].'.png',
            // phpcs:ignore
            'app_exhibition' => "{$weight}kg({$pieces}x{$fields['peso_liquido_unidade']}g)",
        ];
        
        $product = $this->model->withTrashed()
            ->where('code', $fields['codigo'])
            ->where('company_id', $company->id)
            ->first();

        if(stristr($fields['status_sap'], 'ativo')) {
            if($product) {
                $product->restore();
            }

            $product = $this->model->updateOrCreate(
                ['codigo' => $fields['codigo'], 'company_id' => $company->id],
                $data
            );
        } else {
            if(isset($product->id)) {
                $product->delete();
            }
        }

        return $product;
    }

    public function updateManagers(Product $product, $ids)
    {
        return $product->users()->sync($ids);
    }

    public function categories(Company $company)
    {
        return $this->newQuery()
            ->fromCompany($company->id)
            ->groupBy('area')
            ->pluck('area');
    }

    public function integrateProduct(Company $company, $fields)
    {
        $product = $company->products()->withTrashed()->where('code', $fields['code'])->first();

        if($product === null) {
            return $company->products()->create($fields);
        }

        if($product->trashed()) {
            $product->restore();
        }

        $product->update($fields);

        return $product->fresh();
    }

    /**
     * Method to get product codes by company
     *
     * @param Company $company company object
     * @return array
     */
    public function getProductCodes(Company $company): array
    {
        return Product::where(['company_id' => $company->id])->pluck('codigo')->toArray();
    }
}
