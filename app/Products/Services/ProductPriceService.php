<?php
namespace App\Products\Services;

use App\Company\Company;
use App\DistributionCenter\Services\DistributionCenterService;
use App\Products\Product;
use App\Products\ProductPrice;

class ProductPriceService
{
    /**
     * process product price
     *
     * @param Company $company Company entity
     * @param string  $sku     Sku
     * @param string  $state   state
     * @param float   $price   price
     *
     * @return App\Products\ProductPrice
     */
    public function processProductPrice(Company $company, string $sku, string $state, float $price): ProductPrice
    {
        $product = (new ProductService())->getProductBySku($sku, $company);

        $attributes = [
            'product_id' => $product->id,
            'state' => $state,
        ];

        return ProductPrice::updateOrCreate($attributes, ['price' => $price]);
    }

    /**
     * get product price from distribution center
     *
     * @param  Product $product Product entity
     * @param  string  $code    Code of distribution center
     *
     * @return null|ProductPrice
     */
    public function getProductPrice(Product $product, string $code): ?ProductPrice
    {
        $distributionCenter = (new DistributionCenterService())->getUfCode($product->company, $code);

        if ($distributionCenter) {
            return $product->productPrices->where('state', $distributionCenter->state)->first();
        }

        return null;
    }
}
