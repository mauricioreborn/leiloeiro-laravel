<?php

namespace App\Products;

use App\Auth\User;
use App\Bid\Bid;
use App\Company\Company;
use App\Core\Classes\Transactible;
use App\Fefo\Fefo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Storage;

class Product extends Model
{
    use Eloquence, Mappable, SoftDeletes, Transactible;

    protected $table = 'produtos';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'codigo',
        'divisao_empresarial',
        'nome',
        'foto',
        'foto_check',
        'area',
        'familia',
        'marca',
        'submarca',
        'medida',
        'peso_caixa',
        'gramatura',
        'unidades_caixa',
        'exibicao_app',
        'company_id',
        'price_increase',

        // en
        'name',
        'code',
        'picture',
        'category',
        'type',
        'brand',
        'subbrand',
        'unit',
        'weight',
        'weight_piece',
        'pieces',
        'app_exhibition',
        'min_billing',
        'avg_price',
        'company_division',
        'behaviour_approval',
    ];

    // // legacy db mapping
    protected $maps = [
        'name' => 'nome',
        'code' => 'codigo',
        'picture' => 'foto',
        'category' => 'area',
        'type' => 'familia',
        'brand' => 'marca',
        'subbrand' => 'submarca', // XXX(rochacon) review this mapping
        'unit' => 'medida',
        'weight' => 'peso_caixa',
        'weight_piece' => 'gramatura',
        'pieces' => 'unidades_caixa',
        'app_exhibition' => 'exibicao_app',
        'company_division' => 'divisao_empresarial'
    ];

    protected $hidden = [
        'codigo',
        'divisao_empresarial',
        'nome',
        'foto',
        'foto_check',
        'area',
        'familia',
        'marca',
        'submarca',
        'medida',
        'peso_caixa',
        'gramatura',
        'unidades_caixa',
        'exibicao_app',
    ];

    protected $appends = [
        'name',
        'code',
        'picture',
        'picture_url',
        'category',
        'type',
        'brand',
        'subbrand',
        'unit',
        'weight',
        'weight_piece',
        'pieces',
        'app_exhibition',
        'company_division'
    ];

    protected $casts = [
        'gramatura' => 'float(10,2)',
        'peso_caixa' => 'float(10,2)',
        'unidades_caixa' => 'integer',
    ];

    /**
     * @return string
     */
    public function getPictureUrlAttribute()
    {
        return env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$this->company_id. '/'.$this->picture;
    }

    public function getCodeFormattedAttribute()
    {
        return str_pad($this->codigo, 5,'0', STR_PAD_LEFT);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fefo()
    {
        return $this->hasMany(Fefo::class, 'codigo_produto', 'codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productPrices()
    {
        return $this->hasMany(ProductPrice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'produto_user', 'produto_id', 'user_id');
    }

    /**
     *  @TODO: Remove this method and change all places to use ProductActionPolicy
     * @return bool
     */
    public function canBeUpdatedByUser()
    {
        $user = auth()->user();

        return $user->root || ($user->price && (bool) $this->users->find($user->id));
    }

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return mixed
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }
}
