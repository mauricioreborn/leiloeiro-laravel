<?php

namespace App\Products\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'product',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'code' => $this->code,
                'picture_url' => $this->picture_url,
                'category' => $this->category,
                'type' => $this->type,
                'brand' => $this->brand,
                'subbrand' => $this->subbrand,
                'unit' => $this->unit,
                'weight' => $this->weight,
                'weight_piece' => $this->weight_piece,
                'pieces' => $this->pieces,
                'price_increase' => $this->price_increase,
                'min_billing' => $this->min_billing,
                'avg_price' => $this->avg_price,
                'behaviour_approval' => $this->behaviour_approval,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
            'relationships' => [
                'user' => $this->whenLoaded('users'),
            ],
        ];
    }
}
