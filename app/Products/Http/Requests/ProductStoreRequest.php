<?php

namespace App\Products\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to cast confirm password
     *
     * @return Request
     */
    protected function prepareForValidation()
    {
        $companyId = (auth()->user()) ? auth()->user()->company_id : 1;

        return $this->replace(array_merge(
            $this->all(),
            [
                'company_id' => $companyId
            ]
        ));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unit' => 'required|in:KG,L',
            'name' => 'required',
            'code' => 'required|unique:produtos,codigo,NULL,id,deleted_at,NULL',
            'category' => 'required|string',
            'type' => 'required|string',
            'brand' => 'required|string',
            'subbrand' => 'required|string',
            'weight' => 'required|numeric|min:0.1',
            'weight_piece' => 'required|numeric|min:0.1',
            'pieces' => 'required|integer|min:1',
            'picture' => 'required|min:1',
            'app_exhibition' => 'required|string',
            'min_billing' => 'nullable|numeric',
            'avg_price' => 'nullable|numeric'
        ];
    }
}
