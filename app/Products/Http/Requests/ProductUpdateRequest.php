<?php

namespace App\Products\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to cast confirm password
     *
     * @return Request
     */
    protected function prepareForValidation()
    {
        $companyId = (auth()->user()) ? auth()->user()->company_id : 1;

        return $this->replace(array_merge(
            $this->all(),
            [
                'company_id' => $companyId
            ]
        ));
    }

    /**
     * Get the validation rules that apply to the request.
     * @param Request $request request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'picture' => 'string|min:1',
            'behaviour_approval' => 'required|boolean',
            'managers' => 'array',
            'managers.*' => 'integer',
        ];
    }
}
