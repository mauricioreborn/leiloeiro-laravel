<?php

namespace App\Products\Http\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Auth\User;
use App\Products\Product;

class ProductCompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Authorize user to access route
     * @param User    user    $user    User object
     * @param Product product $product Company object
     * @return boolean
     **/
    public function userAccess(User $user, Product $product)
    {
        return (bool) ($product->company_id === $user->company_id);
    }
}
