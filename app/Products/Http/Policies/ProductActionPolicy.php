<?php

namespace App\Products\Http\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Auth\User;
use App\Products\Product;

class ProductActionPolicy
{
    use HandlesAuthorization;

    /**
     * Authorize user to save or update
     * @param User $user User object
     * @return boolean
     **/
    public function save(User $user)
    {
        return $user->root;
    }

    /**
     * Authorize user to save or update
     * @param User    $user    User object
     * @param Product $product Product object
     * @return boolean
     **/
    public function update(User $user, Product $product)
    {
        if ($user->email === 'soukbot@souk.com.br') {
            return true;
        }

        return $user->root || ($user->price && (bool) $product->users->find($user->id));
    }

    /**
     * Authorize user to save or update
     * @param User    $user    User object
     * @return boolean
     **/
    public function updateManagers(User $user)
    {
        return $user->root === 1;
    }
}
