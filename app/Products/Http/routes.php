<?php

Route::group(['prefix' => 'v1/products'], function () {
    Route::get('/categories', 'ProductCategoriesController');

    Route::get('/', 'ProductController@list');
    Route::put('/{product}', 'ProductController@update');
});
