<?php

namespace App\Products\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Behaviour_approval implements FilterInterface
{
    public function run($query, $value)
    {
        return $query->where('behaviour_approval', $value);
    }
}
