<?php

namespace App\Products\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Sub_brand implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $value Search value
     * @return Builder
     */
    public function run($query, $value)
    {
        return $query->where('subbrand', 'LIKE', "%{$value}%");
    }
}
