<?php

namespace App\Products\Http\Controllers;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Request;

use App\Core\Http\Controllers\Controller;

use App\Products\Http\Resources\ProductResource;
use App\Products\Product;
use App\Products\Http\Requests\ProductStoreRequest;
use App\Products\Http\Requests\ProductUpdateRequest;
use App\Products\Services\ProductService;
use App\Company\Company;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function list(Request $request) : AnonymousResourceCollection
    {
        $company = (auth()->user()) ? auth()->user()->company : Company::find(1);

        $collection = $this->productService->search($request->all(), $company);

        return ProductResource::collection($collection);
    }

    public function update(ProductUpdateRequest $request, Product $product): ProductResource
    {
        $this->authorize('update', $product);

        if($request->has('managers')) {
            $this->authorize('updateManagers', Product::class);
            $this->productService->updateManagers($product, $request->managers);
        }

        $this->productService->update($product, $request->only(['picture', 'behaviour_approval']));

        return new ProductResource($this->productService->find($product->id, ['users']));
    }
}
