<?php

namespace App\Products\Http\Controllers;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Request;

use App\Core\Http\Controllers\Controller;

use App\Products\Http\Resources\ProductResource;
use App\Products\Product;
use App\Products\Http\Requests\ProductStoreRequest;
use App\Products\Http\Requests\ProductUpdateRequest;
use App\Products\Services\ProductService;
use App\Company\Company;
use Illuminate\Http\Response;

class ProductCategoriesController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function __invoke()
    {
        return new Response([
            'type' => 'product_categories',
            'data' => (new ProductService)->categories(auth()->user()->company)
        ]);
    }
}
