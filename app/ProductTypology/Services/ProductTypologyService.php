<?php

namespace App\ProductTypology\Services;

use App\Client\Client;
use App\Company\Company;
use App\ProductTypology\ProductTypology;

class ProductTypologyService
{
    /**
     * Get products ID of client typology
     *
     * @param Client  $client  Client entity
     * @param Company $company Company entity
     *
     * @return Illuminate\Database\Eloquent\Collection | boolean
     */
    public function getProducts(Client $client, Company $company)
    {
        $client->clientCompany = $client->setClientCompany($company);

        $productTypologies = ProductTypology::where('company_id', $company->id)
            ->where('typology_code', $client->clientCompany->typology_code)
            ->where('state', $client->clientCompany->state)
            ->get();

        if ($productTypologies->isNotEmpty()) {
            return $productTypologies->filter(function ($productTypology) use ($client) {
                $productTypology->tracks = array_values(array_intersect(
                    $client->clientCompany->tracks,
                    $productTypology->tracks
                ));

                return count($productTypology->tracks);
            });
        }

        return false;
    }
}
