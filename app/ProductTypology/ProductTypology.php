<?php

namespace App\ProductTypology;

use App\Company\Company;
use App\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class ProductTypology extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    protected $fillable = [
        'company_id',
        'product_code',
        'typology_code',
        'state',
        'track_1',
        'track_2',
        'track_3',
        'track_4',
    ];

    protected $appends = [
        'tracks'
    ];

    /**
     * Method to set a Tracks attribute
     *
     *
     * @return array with current faixa
     */
    public function getTracksAttribute(): array
    {
        $tracks = [];

        if ($this->track_1 == 1) {
            $tracks[] = "001";
        }
        if ($this->track_2 == 1) {
            $tracks[] = "002";
        }
        if ($this->track_3 == 1) {
            $tracks[] = "003";
        }
        if ($this->track_4 == 1) {
            $tracks[] = "004";
        }

        return  $this->attributes['tracks'] ?? $tracks;
    }

    /**
     * Company relation
     *
     * @return App\Company\Company
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Product relation
     *
     * @return App\Product\Product
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_code', 'codigo');
    }
}
