<?php

Route::group(['prefix' => 'v1'], function (){
    Route::group(['prefix' => 'logs'], function (){
        Route::get('/emails', 'LogController@emails');
    });
});
