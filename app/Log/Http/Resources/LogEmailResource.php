<?php
namespace App\Log\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LogEmailResource extends JsonResource
{

    /**
     * @param Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'log_email',
            'id'         => $this->id,
            'attributes' => [
                'emails' => $this->emails,
                'screen' => $this->page,
                'subject' => $this->subject,
                'message' => $this->message,
                'created_at' => $this->created_at->format('d/m/Y H:i:s')
            ]
        ];
    }
}
