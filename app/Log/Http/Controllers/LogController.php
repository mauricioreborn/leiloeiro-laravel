<?php

namespace App\Log\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Log\Http\Requests\LogEmailRequest;
use App\Log\Http\Resources\LogEmailResource;
use App\Log\Services\LogEmailService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class LogController
 * @package App\Log\Http\Controllers
 */
class LogController extends Controller
{

    /**
     * @var LogEmailService
     */
    protected $logEmailService;

    /**
     * LogController constructor.
     * @param LogEmailService $logEmailService Service class
     */
    public function __construct(LogEmailService $logEmailService)
    {
        $this->logEmailService = $logEmailService;
    }


    /**
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     */
    public function emails(Request $request): AnonymousResourceCollection
    {
        $request->merge([
            'company_id' => $request->user()->company_id
        ]);

        $logs = $this->logEmailService->search($request->all());

        return LogEmailResource::collection($logs);
    }
}
