<?php

namespace App\Log\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Illuminate\Database\Query\Builder;

class Company_id implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('company_id', $param);
    }
}
