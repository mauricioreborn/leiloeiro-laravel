<?php

namespace App\Log;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogWebHook extends Model
{
    use SoftDeletes;

    protected $table = 'log_web_hooks';

    protected $fillable = [
        'data',
        'event',
        'data_id',
        'data_status',
    ];
}
