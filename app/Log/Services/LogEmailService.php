<?php

namespace App\Log\Services;

use App\Core\Classes\CrudService;
use App\Log\LogEmail;
use Illuminate\Support\Collection;

class LogEmailService extends CrudService
{

    protected $entity = LogEmail::class;

    protected $module = 'Log';

}