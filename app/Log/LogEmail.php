<?php

namespace App\Log;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class LogEmail extends Model
{
    use Eloquence, Mappable;

    protected $table = 'log_emails';

    protected $fillable = [
        'tela',
        'assunto',
        'mensagem',
        'md5_mensagem',
        'emails',
        'read_at',

        //en
        'page',
        'subject',
        'message',
        'md5_message',
        'company_id',
    ];

    protected $hidden = [
        'tela',
        'assunto',
        'mensagem',
        'md5_mensagem',
    ];

    public $maps = [
        'page' => 'tela',
        'subject' => 'assunto',
        'message' => 'mensagem',
        'md5_message' => 'md5_mensagem',
    ];

    protected $appends = [
        'page',
        'subject',
        'message',
        'md5_message'
    ];
}
