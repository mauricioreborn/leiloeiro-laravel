<?php

namespace App\Log;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class LogAccess extends Model
{
    use Eloquence, Mappable;

    protected $table = 'log_acessos';

    public $timestamps = false;

    protected $fillable = [
        'client_id',
        'client_independente_id',
        'route',
        'agent',
        'versao_app',
        'post',
        'get',
        'headers',
        'return_status',
        'return_object',
        'ip',
        'integrado',
    ];
}
