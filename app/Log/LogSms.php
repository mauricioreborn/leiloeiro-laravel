<?php
namespace App\Log;

use Illuminate\Database\Eloquent\Model;

class LogSms extends Model
{
    protected $table = 'log_sms';

    protected $fillable = [
        'phone_number',
        'message',
        'message_sent',
        'errors',
    ];
}