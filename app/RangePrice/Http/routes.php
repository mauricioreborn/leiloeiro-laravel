<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'range_price'], function () {

        Route::middleware(['ValidateServiceAccountJwt'])->group(function () {
            Route::put('approve/fefo/{fefo}', 'RangePriceController@approveFefo');
        });

        Route::middleware(['validateJwt', 'sentry'])->group(function () {
            Route::get('/', 'RangePriceController@list');
            Route::put('/', 'RangePriceController@update');
        });
    });
});
