<?php
namespace App\RangePrice\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

use App\Core\Http\Controllers\Controller;
use App\Fefo\Fefo;
use App\RangePrice\Services\RangePriceService;
use App\RangePrice\Http\Requests\RangePriceRequest;
use App\RangePrice\Http\Resources\RangePriceResource;

class RangePriceController extends Controller
{
    protected $rangePriceService;

    /**
     * Constructor
     *
     * @return void
     **/
    public function __construct()
    {
        $this->rangePriceService = new RangePriceService();
    }

    /**
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     */
    public function list(Request $request) : AnonymousResourceCollection
    {
        $collection = $this->rangePriceService->search(auth()->user()->company_id, $request->all());

        return RangePriceResource::collection($collection);
    }

    /**
     * @param RangePriceRequest $request Request object
     * @return array
     */
    public function update(RangePriceRequest $request)
    {
        $this->rangePriceService->updateMany($request->all());

        return response()->json([], 204);
    }

    /**
     * Get users of campaign
     *
     * @param Fefo $fefo Fefo Entity
     *
     * @return CampaignResource
     */
    public function approveFefo(Fefo $fefo)
    {
        return $this->rangePriceService->approveBidElegibleFefo($fefo);
    }
}
