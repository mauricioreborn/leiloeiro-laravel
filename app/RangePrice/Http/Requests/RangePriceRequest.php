<?php

namespace App\RangePrice\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RangePriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'product_code' => 'required|integer',
                    'origin_code' => 'required|integer',
                    'track' => 'required|integer',
                    'approval_percentage' => 'required|numeric',
                    'reprove_percentage' => 'required|numeric|gt:approval_percentage',
                ];
            case 'PUT':
                return [
                    'approval_percentage.*' => 'required|numeric|min: 0.1',
                    'reprove_percentage.*' => 'required|numeric|min: 0.1|gt:approval_percentage',
                ];
        }
    }
}
