<?php
namespace App\RangePrice\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RangePriceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'range_price',
            'id' => $this->id,
            'attributes' => [
                'product_code' => $this->product_code,
                'product_name' => $this->product_name,
                'product_category' => $this->product_category,
                'origin_code' => $this->origin_code,
                'origin_name' => $this->origin_name,
                'track' => $this->track,
                'approval_percentage' => $this->approval_percentage,
                'reprove_percentage' => $this->reprove_percentage,
            ],
        ];
    }
}
