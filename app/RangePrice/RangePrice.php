<?php
namespace App\RangePrice;

use App\Core\Classes\Transactible;
use App\Products\Product;
use App\LeadTime\LeadTime;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class RangePrice extends Model
{
    use Eloquence, Mappable, SoftDeletes, Transactible;

    protected $table = 'range_prices';

    protected $fillable = [
        'product_code',
        'origin_code',
        'track',
        'approval_percentage',
        'reprove_percentage',
        'company_id',
    ];

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_code', 'codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leadtime()
    {
        return $this->belongsTo(LeadTime::class, 'origin_code', 'cod_origem')->groupBy('cod_origem');
    }
}
