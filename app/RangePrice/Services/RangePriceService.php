<?php
namespace App\RangePrice\Services;

use App\Bid\Bid;
use App\Bid\Services\BidService;
use App\Company\Company;
use App\Core\Classes\CrudService;
use App\Fefo\Fefo;
use App\Fefo\Services\FefoBidsService;
use App\LeadTime\LeadTime;
use App\Products\Product;
use App\RangePrice\RangePrice;
use InvalidArgumentException;

class RangePriceService extends CrudService
{
    const UNLEGIBLE = 'unlegible';
    const ACCEPT = 'accept';
    const REJECT = 'reject';

    protected $rangePrice;

    protected $entity = RangePrice::class;

    protected $module = 'RangePrice';

    /**
     * @param integer $companyId Company ID
     * @param array   $filters   Filters
     * @return mixed
     */
    public function search($companyId, $filters = [])
    {
        $availableOriginsQuery = LeadTime::fromCompany($companyId);

        if(isset($filters['origin_code'])) {
            $availableOriginsQuery->where(function ($query) use($filters) {
                return $query->where('cod_origem', $filters['origin_code'])
                    ->orWhere('nome', 'LIKE', "%{$filters['origin_code']}%");
            });
        }

        $availableOrigins = $availableOriginsQuery->listed();

        $query = $this->newQuery()->select([
                'id',
                'product_code',
                'origin_code',
                'track',
                'approval_percentage',
                'reprove_percentage',
                'produtos.nome as product_name',
                'produtos.area as product_category',
            ])
            ->join('produtos', 'range_prices.product_code', 'produtos.codigo')
            ->where('range_prices.company_id', $companyId)
            ->whereNotNull('produtos.id')
            ->whereIn('origin_code', $availableOrigins->keys())
            ->groupBy('range_prices.id');

        if(isset($filters['id'])) {
            $query->where('range_prices.id', $filters['id']);
        }

        if(isset($filters['product_code'])) {
            $query->where('range_prices.product_code', $filters['product_code']);
        }

        if(isset($filters['product_name'])) {
            $query->where('produtos.nome', 'LIKE', "%{$filters['product_name']}%");
        }

        if(isset($filters['category'])) {
            $query->where('produtos.area', 'LIKE', "%{$filters['category']}%");
        }

        if(isset($filters['track'])) {
            $query->where('range_prices.track', $filters['track']);
        }

        $paginator = $query->paginate($filters['limit'] ?? 15);

        $paginator->getCollection()->transform(function ($rangePrice) use ($availableOrigins) {
            $rangePrice->origin_name = $availableOrigins[$rangePrice->origin_code];
            return $rangePrice;
        });

        return $paginator;
    }

    /**
     * create or find range price
     *
     * @param Company $company    Company entity
     * @param Product $product    Product entity
     * @param string  $originCode Origin code
     * @param string  $track      Track of fefo
     *
     * @return App\RangePrice\RangePrice
     */
    public function createRangePrice(Company $company, Product $product, string $originCode, string $track)
    {
        $attributes = [
            'product_code' => $product->code,
            'origin_code' => $originCode,
            'track' => $track,
            'company_id' => $company->id
        ];

        $data = [
            'approval_percentage' => $this->approvalPercentage($company, $product, $track),
            'reprove_percentage' => $this->reprovePercentage($company, $product),
        ];

        return RangePrice::updateOrCreate($attributes, $data);
    }

    /**
     * get approval percentage of track
     *
     * @param Company $company company
     * @param Product $product product
     * @param string $track    track
     *
     * @return float
     */
    public function approvalPercentage(Company $company, Product $product, string $track): float
    {
        $approvalPercentage = 0;

        if($company->id != 5) {
            if ($track == '002') {
                $approvalPercentage = 5.88;
            }

            if ($track == '003') {
                $approvalPercentage = 9.09;
            }
        }else if($company->id === 5 && (float)$product->price_increase > 0){
            $approvalPercentage = (float)$product->price_increase - 1;
        }

        return $approvalPercentage;
    }

    /**
     * get reprove percentage
     *
     * @param Company $company
     * @param Product $product
     * @return float
     */
    public function reprovePercentage(Company $company, Product $product): float
    {
        $reprovePercentage = 0;

        if($company->id != 5){
            $reprovePercentage =  100;
        }else if($company->id === 5 && (float)$product->price_increase > 0){
            $reprovePercentage = (float)$product->price_increase - 1;
        }

        return $reprovePercentage;
    }

    /**
     * Update multiple data
     *
     * @param array $data data
     * @return array
     **/
    public function updateMany($data)
    {
        $rangePrice = new RangePrice();

        $result = [];

        foreach ($data as $value) {
            if ($value['approval_percentage'] >  $value['reprove_percentage']) {
                throw new InvalidArgumentException(
                    __('rangeprice.approval_percentage_gt_reject', [
                        'approval_percentage' =>  $value['approval_percentage'],
                        'reprove_percentage' => $value['reprove_percentage']
                    ])
                );
            }
            $result = $rangePrice->findOrFail($value['id'])->update([
                'approval_percentage' => $value['approval_percentage'],
                'reprove_percentage' => $value['reprove_percentage'],
            ]);
        }

        return $result;
    }

    /**
     * approve bids with fefo range price
     *
     * @param  Fefo $fefo Fefo Entity
     *
     * @return array
     */
    public function approveBidElegibleFefo(Fefo $fefo) :array
    {
        $bidIds = [];

        $this->rangePrice = RangePrice::where('product_code', $fefo->product_code)
            ->where('origin_code', $fefo->leadtime_code)
            ->where('track', (int) $fefo->faixa_fefo)
            ->where('company_id', $fefo->company_id)
            ->first();

        if ($this->rangePrice) {
            $bids = (new FefoBidsService())->getFefoBids($fefo);

            foreach ($bids as $bid) {
                $elegible = $this->elegibleBid($bid, $fefo->selling_price);

                $bidIds[$elegible][] = $bid->id;
            }

            if (isset($bidIds[$this::ACCEPT])) {
                (new FefoBidsService())->approve($fefo, $bidIds[$this::ACCEPT]);
            }

            if (isset($bidIds[$this::REJECT])) {
                (new BidService())->disapprove($bidIds[$this::REJECT]);
            }
        }

        return $bidIds;
    }

    /**
     * calculate elegible bid range price
     *
     * @param  Bid    $bid   Bid Entity
     * @param  string $price Fefo price
     *
     * @return string
     */
    protected function elegibleBid(Bid $bid, $price) :string
    {
        $logistics = $bid->logistics;

        if ($logistics['aceita_compra']) {
            $approvePrice = round($price * (1 - ($this->rangePrice->approval_percentage / 100)), 2);
            $reprovePrice = round($price * (1 - ($this->rangePrice->reprove_percentage / 100)), 2);

            if ($bid->can_purchase) {
                if ($bid->kg_price >= $approvePrice && $this->rangePrice->approval_percentage > 0) {
                    return $this::ACCEPT;
                }
            }

            if ($bid->kg_price  < $reprovePrice && $this->rangePrice->reprove_percentage > 0) {
                return $this::REJECT;
            }
        }

        return $this::UNLEGIBLE;
    }
}
