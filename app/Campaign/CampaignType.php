<?php

namespace App\Campaign;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class CampaignType extends Model
{
    use Eloquence, Mappable;

    protected $table = 'campaign_types';

    protected $fillable = [
        'name',
        'uid',
        'recurrence_days',
    ];

    /**
     * Campaign relation
     *
     * @return Company
     */
    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }
}
