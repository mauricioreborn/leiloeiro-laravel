<?php

namespace App\Campaign\Services;

use App\Campaign\Campaign;
use App\Campaign\CampaignClient;
use App\Campaign\CampaignType;
use App\Client\Client;
use App\LeadTime\LeadTime;
use App\Mobile\Exceptions\MobileException;
use App\Mobile\Services\ProductService;
use App\Order\OrderProduct;
use App\Fefo\Fefo;
use App\Order\Order;
use App\Company\Company;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Core\Classes\CrudService;
use InvalidArgumentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CampaignTypeService extends CrudService
{
    protected $entity = CampaignType::class;

    protected $module = 'Campaign';
    /**
     * Get client message of campaign
     *
     * @param CampaignClient $campaignClient CampaignClient Entity
     *
     * @return array
     */
    public function client(CampaignClient $campaignClient)//: array
    {
        $service = $campaignClient->campaign->campaignType->uid;

        return $this->$service($campaignClient);
    }

    /**
     * Get repurchase client message
     *
     * @param CampaignClient $campaignClient CampaignClient Entity
     *
     * @return array
     */
    public function repurchase(CampaignClient $campaignClient): array
    {
        $productService = new ProductService();
        $company = new Company();
        $date = Carbon::now()->format('Y-m-d');
        $dateSub7Days = Carbon::now()->subDays(7)->format('Y-m-d');

        //Product that client had bought more
        $clientProducts =
            OrderProduct::selectRaw('COUNT(pedidos_produtos.produto_id) as buy_amount, pedidos_produtos.*')
            ->join('pedidos', 'pedidos.id', '=', 'pedidos_produtos.pedido_id')
            ->where('pedidos.company_id', $campaignClient->campaign->company_id)
            ->where('pedidos.id_cliente', $campaignClient->client_id)
            ->groupBy('pedidos_produtos.produto_id')
            ->orderBy('buy_amount', 'DESC')
            ->orderBy('pedidos_produtos.created_at', 'DESC')
            ->get();

        $company = $company->find($campaignClient->campaign->company_id);

        //Products avaliable for client
        $productsAvaliable = $productService->getAreasProducts(
            $campaignClient->client,
            $company,
            ['date' => $date, 'orderBy' => 'preco']
        );

        $productsAvaliable = collect($productsAvaliable['produtos']);

        //sort by cheapper product of week
        $productsAvaliable = $productsAvaliable->sortBy('precoMin');

        if ($productsAvaliable->isEmpty()) {
            throw new ModelNotFoundException;
        }

        $bestWeekProductValue = $clientProducts->map(function ($value, $key) use ($productsAvaliable) {

            $match = $productsAvaliable->firstWhere('codigo_produto', $value->produto_id);

            if(!empty($match)) {
                return $match;
            }

            return false;
        })->filter(function ($value, $key) {
            return $value;
        })->first();

        if (empty($bestWeekProductValue)) {
            throw new ModelNotFoundException;
        }

        return [
            'title' => __('notification.campaign.repurchase.title',
                [
                    'product_name' => $bestWeekProductValue['nome']
                ]
            ),
            'message' => __('notification.campaign.repurchase.message',
                [
                    'price' =>
                        formata_moeda($bestWeekProductValue['precoMin']).'/'.$bestWeekProductValue['padraoMedida']
                ]),
        ];
    }

    /**
     * Get first Purchase client message
     *
     * @param CampaignClient $campaignClient CampaignClient Entity
     *
     * @return array
     */
    public function firstPurchase(CampaignClient $campaignClient): array
    {
        $productService = new ProductService();
        $client = Client::find($campaignClient->client_id);
        $origin = LeadTime::select('code')->where('cnpj', $client->cnpj)->first();
        $date = date('Y-m-d');

        $campaign = Campaign::where('id', $campaignClient->campaign_id)->first();
        $campaignClient = $client->setClientCompany($campaign->company);

        $mostBoughtTypology = DB::table('pedidos_produtos')
            ->selectRaw('pedidos_produtos.produto_id, count(pedidos_produtos.id) as total_order')
            ->join('pedidos', 'pedidos.id', '=', 'pedidos_produtos.pedido_id')
            ->join('produtos', 'produtos.codigo', '=', 'pedidos_produtos.produto_id')
            ->whereRaw("pedidos.id_cliente
                IN (SELECT client_id from client_companies where typology = '{$campaignClient->typology}' and
                company_id = '{$campaignClient->company_id}'
                )")
            ->groupBy('pedidos_produtos.produto_id')
            ->orderByDesc('total_order')
            ->get();

        //Products avaliable for client
        $productsAvaliable = $productService->getAreasProducts(
            $client,
            $campaign->company,
            ['date' => $date, 'orderBy' => 'preco']
        );

        $productsAvaliable = collect($productsAvaliable['produtos']);

        //sort by cheapper product of week
        $productsAvaliable = $productsAvaliable->sortBy('precoMin');

        if ($productsAvaliable->isEmpty()) {
            throw new ModelNotFoundException;
        }

        $bestWeekProductValue = $mostBoughtTypology->map(function ($value, $key) use ($productsAvaliable) {

            $match = $productsAvaliable->firstWhere('codigo_produto', $value->produto_id);

            if(!empty($match)) {
                return $match;
            }

            return false;
        })->filter(function ($value, $key) {
            return $value;
        })->first();

        if (empty($bestWeekProductValue)) {
            throw new ModelNotFoundException;
        }

        return [
            'title' => __('notification.campaign.first_purchase.title',
                [
                    'company_name' => strtoupper($campaign->company->name),
                    'product_name' => $bestWeekProductValue['nome']
                ]
            ),
            'message' => __('notification.campaign.first_purchase.message',
                [
                    'product_name' => $bestWeekProductValue['nome'],
                    'price' =>
                        formata_moeda($bestWeekProductValue['precoMin']).'/'.$bestWeekProductValue['padraoMedida']
                ]
            ),
        ];
    }

    /**
     * Get highlight Fefo client message
     *
     * @param CampaignClient $campaignClient CampaignClient Entity
     *
     * @return array
     */
    public function highlightFefo(CampaignClient $campaignClient): array
    {
        $campaign = $campaignClient->campaign;

        $fefo = Fefo::selectRaw('fefo.id, fefo.valor_atual, fefo.codigo_produto, produtos.nome, produtos.medida')
            ->whereRaw('fefo.volume >= produtos.peso_caixa')
            ->join('produtos', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('produtos.deleted_at');
            })
            ->where([
                ['fefo.id', '=', $campaign->attributes['fefo_id']],
                ['fefo.date', '=', today()->toDateString()],
                ['fefo.volume', '>', 0]
            ])
            ->firstOrFail();
        
        return [
            'title' => __('notification.campaign.highlight_fefo.title', [
                'product_name' => $fefo->nome,
                'price' => formata_moeda($fefo->valor_atual).'/'.$fefo->medida
            ]),
            'message' => __('notification.campaign.highlight_fefo.message')
        ];
    }
}
