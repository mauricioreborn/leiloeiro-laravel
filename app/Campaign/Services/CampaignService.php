<?php

namespace App\Campaign\Services;

use App\Auth\User;
use App\Campaign\Campaign;
use App\Campaign\CampaignType;
use App\Client\Client;
use App\Company\Company;
use App\Core\Classes\CrudService;
use App\Core\Classes\Search;
use App\Core\Exceptions\ActionNotAllowedException;
use App\Fefo\Fefo;
use App\LeadTime\Services\LeadTimeService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class CampaignService
 * @package App\Campaign\Services
 */
class CampaignService extends CrudService
{
    protected $entity = Campaign::class;

    protected $module = 'Campaign';


    /**
     * @param integer $companyId Company ID
     * @param array   $filters   Filters
     * @return mixed
     */
    public function search($companyId, $filters = [])
    {
        $searchInstance = new Search($this->module, $this->newQuery());

        $query = $searchInstance->run($filters);

        $query->fromCompany($companyId);

        return $query->paginate($filters['limit'] ?? 15);
    }

    /**
     * Create Campaign in company
     *
     * @param User   $user         User Entity
     * @param string $campaignType Campaign Type uid
     * @param string $scheduledAt  Scheduled At
     * @param array  $attributes   Optional attributes
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createCompanyCampaign(User $user, $campaignType, $scheduledAt, array $attributes = []): Model
    {
        $data = [
            'campaign_type_id' => CampaignType::where('uid', $campaignType)->first()->id,
            'user_id' => $user->id,
            'company_id' => $user->company_id,
            'scheduled_at' => $scheduledAt
        ];

        if ($campaignType === 'highlightFefo') {
            $data['attributes'] = ['fefo_id' => array_get($attributes, 'fefo_id')];
        }

        $model = $this->factory($data);

        $this->save($model);

        return $model;
    }

    /**
     * Delete unprocessed campaign
     *
     * @param  Campaign $campaign Campaign Entity
     *
     * @return bool
     *
     * @throws Exception
     */
    public function deleteCampaign(Campaign $campaign): bool
    {
        if ($campaign->processed_at !== null) {
            throw new ActionNotAllowedException(__('auth.access_denied'));
        }

        return $campaign->delete();
    }

    /**
     * Get clients of campaign
     *
     * @param Campaign $campaign Campaign Entity
     *
     * @return array
     */
    public function clients(Campaign $campaign): array
    {
        $service = $campaign->campaignType->uid;

        return $this->$service($campaign);
    }

    /**
     * Get estimate clients of campaign
     *
     * @param CampaignType $campaignType Campaign Type Entity
     * @param Company      $company      Company Entity
     * @param array        $attributes   Attributes
     *
     * @return array
     */
    public function estimate(CampaignType $campaignType, Company $company, array $attributes = []): array
    {
        $type = ucfirst($campaignType->uid);
        $service = "estimate{$type}";

        return $this->$service($company->id, $attributes);
    }

    /**
     * Get campaign 'repurchase' clients
     *
     * @param Campaign $campaign Campaign Entity
     *
     * @return array
     */
    protected function repurchase(Campaign $campaign): array
    {
        return $this->estimateRepurchase($campaign->company_id);
    }

    /**
     * Estimate Repurchase clients
     *
     * @param string $companyId  Company ID
     * @param array  $attributes attributes
     * @param int    $subDays    days to sub of today
     *
     * @return array
     */
    protected function estimateRepurchase(
        string $companyId,
        array $attributes = [],
        $subDays = 30,
        $alreadyBought = false
    ): array {
        $date = now()->format('Y-m-d');
        $dateSubDays = now()->subDays($subDays)->format('Y-m-d');
        $clientIds = [];

        //get clients that had buy between today and 30 days ago
        $clientsBoughtThisMonth = DB::table('clientes')
            ->select('clientes.id')
            ->join('pedidos', 'pedidos.id_cliente', '=', 'clientes.id')
            ->where('email_verified', 1)
            ->whereBetween('pedidos.data', [$dateSubDays, $date])
            ->where('pedidos.company_id', $companyId)
            ->groupBy('clientes.id')
            ->get();

        $clientsBoughtThisMonth = $clientsBoughtThisMonth->pluck('id')->toArray();

        $elegibleClients = DB::table('clientes')
            ->select('clientes.id')
            ->join('pedidos', 'pedidos.id_cliente', '=', 'clientes.id')
            ->join('client_companies', function ($query) {
                return $query->on('client_companies.client_id', '=', 'clientes.id')
                    ->where('client_companies.status', 1)
                    ->where('client_companies.account_balance', '>', 0)
                    ->whereRaw('client_companies.account_balance > client_companies.min_order')
                    ->whereNull('client_companies.deleted_at');
            })
            ->where('email_verified', 1)
            ->where('pedidos.company_id', $companyId)
            ->where('client_companies.company_id', $companyId)
            ->groupBy('clientes.id');

        $elegibleClients = $alreadyBought == true ?
            $elegibleClients->whereIn('clientes.id', $clientsBoughtThisMonth):
            $elegibleClients->whereNotIn('clientes.id', $clientsBoughtThisMonth);

        $elegibleClients = $elegibleClients->get();

            $clientIds = $elegibleClients->pluck('id');

        // //get clients with logistic
        $logistics =
            (new LeadTimeService())->getLogistica(
                $clientIds->toArray(),
                false,
                $companyId
            );

        $clientIds = $clientIds->filter(function ($clientId) use ($logistics) {
            if (isset($logistics[$clientId])) {
                $logistic = $logistics[$clientId];
            }

            if (isset($logistic)) {
                if ($logistic['aceita_compra'] == true) {
                    return $clientId;
                }
            }
        })->toArray();

        return [
            'clientIds' => $clientIds
        ];
    }

    /**
     * Get campaign 'firstPurchase' clients
     *
     * @param Campaign $campaign Campaign Entity
     *
     * @return array
     */
    protected function firstPurchase(Campaign $campaign): array
    {
        return $this->estimateFirstPurchase($campaign->company_id);
    }

    /**
     * Estimate FirstPurchase clients
     *
     * @param string $companyId  Company ID
     * @param array  $attributes attributes
     *
     * @return array
     */
    protected function estimateFirstPurchase(string $companyId, array $attributes = []): array
    {
        $data = DB::table('clientes')
            ->select('clientes.id')
            ->join('client_companies', function ($query) {
                return $query->on('client_companies.client_id', '=', 'clientes.id')
                    ->where('client_companies.status', 1)
                    ->where('client_companies.account_balance', '>', 0)
                    ->whereRaw('client_companies.account_balance > client_companies.min_order')
                    ->whereNull('client_companies.deleted_at');
            })
            ->whereRaw('clientes.id not in (SELECT id_cliente FROM pedidos)')
            ->where('client_companies.company_id', $companyId)
            ->where('email_verified', 1)
            ->where('client_companies.company_id', $companyId)
            ->get();

        $logistics = (
        new LeadTimeService())->getLogistica($data->pluck('id')
                ->unique()->toArray(),
                false,
                $companyId
            );

        $canBuy = [];

        $canBuy = $data->filter(function ($item) use ($logistics, $canBuy) {
          if (isset($logistics[$item->id])) {
            if ($logistics[$item->id]['aceita_compra'] == true) {
                return $item->id;
            }
          }
        });

        return [
            'clientIds' => $canBuy->pluck('id')->unique()->toArray(),
        ];
    }

    /**
     * Get campaign 'highlightFefo' clients
     *
     * @param Campaign $campaign Campaign Entity
     *
     * @return array
     */
    protected function highlightFefo(Campaign $campaign): array
    {
        return $this->estimateHighlightFefo($campaign->company_id, ['fefo_id' => $campaign->attributes['fefo_id']]);
    }

    /**
     * Estimate FirstPurchase clients
     *
     * @param string $companyId  Company ID
     * @param array  $attributes attributes
     *
     * @return array
     */
    protected function estimateHighlightFefo(string $companyId, array $attributes = []): array
    {
        $clientIds = [];

        $fefo = Fefo::whereRaw('fefo.volume >= produtos.peso_caixa')
            ->join('produtos', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('produtos.deleted_at');
            })
            ->where([
                ['fefo.id', '=', $attributes['fefo_id']],
                ['fefo.date', '=', today()->toDateString()],
                ['fefo.volume', '>', 0],
                ['fefo.destaque', '=', 1]
            ])
            ->first();

        if ($fefo) {
            $clients = Client::selectRaw('clientes.id')
                ->join('client_companies', function ($query) {
                    return $query->on('client_companies.client_id', '=', 'clientes.id')
                        ->where('client_companies.status', 1)
                        ->where('client_companies.account_balance', '>', 0)
                        ->whereRaw('client_companies.account_balance > client_companies.min_order')
                        ->whereNull('client_companies.deleted_at');
                })
                ->join('leadtime', function ($query) {
                    return $query->on('clientes.cnpj', '=', 'leadtime.cnpj')
                        ->whereNull('leadtime.deleted_at');
                })
                ->where([
                    ['client_companies.company_id', '=', $companyId],
                    ['clientes.email_verified', '=', 1],
                    ['leadtime.cod_origem', '=', $fefo->leadtime_code],
                ]);

            if ($fefo->faixa_fefo == 1) {
                $clients->where('faixa_1', true);
            }

            if ($fefo->faixa_fefo == 2) {
                $clients->where('faixa_2', true);
            }

            if ($fefo->faixa_fefo == 3) {
                $clients->where('faixa_3', true);
            }

            if ($fefo->faixa_fefo == 4) {
                $clients->where('faixa_4', true);
            }

            $clientIds = $clients->pluck('id');

            $logistics = (new LeadTimeService())->getLogistica(
                $clientIds->toArray(),
                $fefo->leadtime_code,
                $companyId
            );

            $clientIds = $clientIds->filter(function ($clientId) use ($logistics) {
                $logistic = $logistics[$clientId];

                if (isset($logistic)) {
                    if ($logistic['aceita_compra'] == true) {
                        return $clientId;
                    }
                }
            })->toArray();
        }

        return [
            'clientIds' => $clientIds,
        ];
    }

    /**
     * Method to get clients that bought between date
     * @param Company $company    company object
     * @param array   $attributes additional attributes
     * @param int     $subDays    days to sub of today
     * @return array
     */
    public function getClientsThatBought(Company $company, $attributes = [],  $subDays = 30)
    {
        return $this->estimateRepurchase($company->id,$attributes, $subDays, $alreadyBought = true);
    }
}
