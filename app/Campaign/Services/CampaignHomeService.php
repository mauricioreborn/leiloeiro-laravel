<?php
namespace App\Campaign\Services;

use App\Client\Client;
use App\Client\Services\ClientService;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Jobs\Campaign\CreateDailySuggestionJob;
use App\Jobs\Campaign\CreateDailyTypologyJob;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Products\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CampaignHomeService
{
    public function createDailyTypologyCampaign(Company $company): void
    {
        Log::info("[CAMPANHA][TIPOLOGIA][{$company->id}] Buscando tipologias para a company");
        $typologies = ClientCompany::where('company_id', $company->id)
            ->whereNotNull('typology')
            ->where('typology', '!=', '')
            ->groupBy('typology')
            ->pluck('typology', 'typology_code');

        if($typologies->count() === 0) {
            Log::info("[CAMPANHA][TIPOLOGIA][{$company->id}] Nenhuma tipologia encontrada para company");
            return;
        }

        Log::info("[CAMPANHA][TIPOLOGIA][{$company->id}] {$typologies->count()} tipologias encontradas");

        Log::info("[CAMPANHA][TIPOLOGIA][{$company->id}] Buscando origens para a company");

        $originCodes = LeadTime::select('cod_origem')
            ->where('company_id', $company->id)
            ->groupBy('cod_origem')
            ->pluck('cod_origem');

        if($originCodes->count() === 0) {
            Log::info("[CAMPANHA][TIPOLOGIA][{$company->id}] Nenhum CD encontrado para company");
            return;
        }

        Log::info("[CAMPANHA][TIPOLOGIA][{$company->id}] {$originCodes->count()} CDs encontrados");

        foreach($typologies as $typologyCode => $typology) {
            dispatch(new CreateDailyTypologyJob($company->id, $typologyCode, $typology, $originCodes->toArray()));
        }
    }

    public function getTopProductsByOrigin($companyId, $typologyCode, $typology, $originCodes = [])
    {
        if(count($originCodes) === 0) {
            return;
        }

        Log::info("[CAMPANHA][TIPOLOGIA][{$companyId}][{$typology}] Buscando clientes para tipologia");
        $clientIds = ClientCompany::where('company_id', $companyId)
            ->where('typology_code', $typologyCode)
            ->where('status', 1)
            ->pluck('client_id');

        if($clientIds->count() === 0) {
            //phpcs:ignore
            Log::info("[CAMPANHA][TIPOLOGIA][{$companyId}][{$typology}] Nenhum cliente ativo encontrado para tipologia");
            return;
        }

        //phpcs:ignore
        Log::info("[CAMPANHA][TIPOLOGIA][{$companyId}][{$typology}] {$clientIds->count()} clientes encontrados para tipologia");


        foreach ($originCodes as $originCode) {
            //phpcs:ignore
            Log::info("[CAMPANHA][TIPOLOGIA][{$companyId}][{$typology}][{$originCode}] contabilizando produtos comprados para tipologia e CD");
            //phpcs:ignore
            $productIds = OrderProduct::selectRaw('produtos.area, pedidos_produtos.produto_id, count(pedidos_produtos.id) as total_order')
                ->join('pedidos', 'pedidos.id', '=', 'pedidos_produtos.pedido_id')
                ->join('produtos', 'produtos.codigo', '=', 'pedidos_produtos.produto_id')
                ->whereIn('pedidos.id_cliente', $clientIds)
                ->where('cod_origem', $originCode)
                ->where('pedidos.company_id', $companyId)
                ->where('produtos.company_id', $companyId)
                ->groupBy('pedidos_produtos.produto_id')
                ->orderByDesc('total_order')
                ->pluck('product_id')
                ->toArray();

            //phpcs:ignore
            Log::info("[CAMPANHA][TIPOLOGIA][{$companyId}][{$typology}][{$originCode}] gerando cache para tipologia e CD");
            $expiresAt = now()->addDay();
            if (count($productIds) > 0) {
                Cache::add(
                    "campaign:home:typology:{$companyId}:{$originCode}.{$typology}", $productIds, $expiresAt
                );
            }
        }
    }

    /**
     * Method to get products by Categories
     * @param Company $company    company object
     * @param array   $categories categories to search
     * @param bool    $random     is random products
     * @param array $exceptionProducts
     * @return mixed
     */
    public function getProductsByCategories(
        Company $company,
        array $categories,
        array $exceptionProducts = []
    ) {
        $products = Product::whereIn('area', $categories)
            ->where('company_id', $company->id);

        if ($exceptionProducts) {
            $products = $products->whereNotIn('codigo', $exceptionProducts);
        }
        return $products->get();
    }

    /**
     * Method to create a campaign with suggested products
     */
    public function createCacheToSuggestedCampaign()
    {
        $campaignService = new CampaignService();
        $clientService = new ClientService();
        $companies = Company::all();
        $expiresAt = now()->addDay();
        foreach ($companies as $company) {
            $clientIds = $campaignService->getClientsThatBought($company, [], 90);
            if (count($clientIds['clientIds']) > 0) {
                foreach ($clientIds['clientIds'] as $clientId) {
                    $client = Client::find($clientId);
                    $products = $clientService->getProductCategoryBought($client, $company);
                    if ($products->count() > 0) {
                        $categories = $products->pluck('area');
                        $productsByCategory = $this->getProductsByCategories($company, $categories->toArray());
                        Cache::add("campaign:home:suggested:{$company->id}:{$client->id}",
                            $productsByCategory->pluck('code')->toArray(), $expiresAt);
                    }
                }
            }
        }
    }

    public function createDailySuggestionCampaign(Company $company): void
    {
        //phpcs:ignore
        Log::info("[CAMPANHA][SUGESTOES_DO_DIA][{$company->id}] Buscando clientes com pedidos nos últimos 90 dias");

        // Pegando clientes que compraram nos últimos 90 dias
        $clientIds = Order::fromCompany($company->id)
            ->whereBetween('date', [
                now()->subDays(90)->toDateString(),
                now()->toDateString(),
            ])
            ->groupBy('id_cliente')
            ->pluck('client_id');

        foreach($clientIds as $clientId) {
            dispatch(new CreateDailySuggestionJob($company->id, $clientId));
        }
    }

    public function createDailySuggestionForClient(Company $company, Client $client): void
    {
        $expiresAt = now()->addDay();
        $companyProducts = $company->products;
        $companyProductCategories = $companyProducts->pluck('area', 'codigo')->toArray();

        $clientOrders = $client->order()->with('orderProduct')->fromCompany($company->id)
            ->whereBetween('date', [
                now()->subDays(90)->toDateString(),
                now()->toDateString(),
            ])
            ->get();


        if(count($clientOrders) === 0) {
            Log::info("[CAMPANHA][SUGESTOES_DO_DIA][{$company->id}][{$client->id}] Nenhum pedido encontrado");
            return;
        }

        Log::info("[CAMPANHA][SUGESTOES_DO_DIA][{$company->id}][{$client->id}] Gerando cache para cliente");
        $productCategories = [];

        foreach($clientOrders as $order) {
            foreach($order->orderProduct as $orderProduct) {
                if(isset($companyProductCategories[$orderProduct->product_id])) {
                    $productCategories[] = $companyProductCategories[$orderProduct->product_id];
                }
            }
        }

        $clientProducts = $companyProducts->whereIn('area', $productCategories);
        $codes = $clientProducts->pluck('code')->toArray();

        Cache::add("campaign:home:suggested:{$company->id}:{$client->id}", $codes, $expiresAt);

        Log::info("[CAMPANHA][SUGESTOES_DO_DIA][{$company->id}][{$client->id}] Cache gerado para cliente");

        $json = json_encode($codes);
        Log::info("[CAMPANHA][SUGESTOES_DO_DIA][{$company->id}][{$client->id}] {$json}");
    }
}