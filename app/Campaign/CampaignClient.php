<?php

namespace App\Campaign;

use App\Client\Client;
use App\Company\Company;
use App\Notification\Notification;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class CampaignClient extends Model
{
    use Eloquence, Mappable;

    protected $table = 'campaign_clients';

    protected $fillable = [
        'campaign_id',
        'client_id',
        'notification_id',
        'date',
        'send_at',
    ];

    /**
     * Company relation
     *
     * @return Company
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * Client relation
     *
     * @return Client
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Notification relation
     *
     * @return Notification
     */
    public function notification()
    {
        return $this->belongsTo(Notification::class);
    }
}
