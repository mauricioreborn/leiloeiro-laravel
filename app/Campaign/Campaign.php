<?php

namespace App\Campaign;

use App\Auth\User;
use App\Company\Company;
use App\Fefo\Fefo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class Campaign extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    protected $table = 'campaigns';

    protected $fillable = [
        'company_id',
        'user_id',
        'campaign_type_id',
        'attributes',
        'estimated_clients',
        'scheduled_at',
        'processed_at',
    ];

    protected $appends = [
        'fefo_id'
    ];

    protected $casts = [
        'attributes' => 'array'
    ];

    /**
     * Company relation
     *
     * @return Company
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * User relation
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * CampaignType relation
     *
     * @return User
     */
    public function campaignType()
    {
        return $this->belongsTo(CampaignType::class);
    }

    /**
     * CampaignType relation
     *
     * @return User
     */
    public function fefo()
    {
        return $this->belongsTo(Fefo::class);
    }

    /**
     * Campaign Clients relation
     *
     * @return CampaignClient
     */
    public function campaignClients()
    {
        return $this->hasMany(CampaignClient::class);
    }

    /**
     * Campaign Clients relation
     *
     * @return CampaignClient
     */
    public function getFefoIdAttribute()
    {
        return array_get($this['attributes'], 'fefo_id');
    }

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }
}
