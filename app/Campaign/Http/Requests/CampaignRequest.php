<?php

namespace App\Campaign\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class CampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('allowCampaigns', $this->user()->company);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|exists:campaign_types,uid',
            'scheduled_at' => 'required|date|date_format:Y-m-d H:i:s|after:now',
            'fefo_id' => 'required_if:type,highlightFefo|exists:fefo,id',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'type.required' => __('campaign.type.required'),
            'type.exists' => __('campaign.type.exists'),
            'scheduled_at.required' => __('campaign.scheduled_at.required'),
            'scheduled_at.date' => __('campaign.scheduled_at.date'),
            'scheduled_at.date_format' => __('campaign.scheduled_at.date_format'),
            'scheduled_at.after' => __('campaign.scheduled_at.after'),
            'fefo_id.required_if' => __('campaign.fefo_id.required_if'),
        ];
    }
}
