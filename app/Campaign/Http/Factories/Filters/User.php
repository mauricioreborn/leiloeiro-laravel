<?php

namespace App\Campaign\Http\Factories\Filters;

use App\Auth\User as UserModel;
use App\Core\Contracts\FilterInterface;

class User implements FilterInterface
{

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query Query builder
     * @param mixed                                 $value Value
     * @return mixed
     */
    public function run($query, $value)
    {
        $userIds = UserModel::where('name', 'LIKE', "%{$value}%")->pluck('id');

        return $query->whereIn('user_id', $userIds);
    }
}
