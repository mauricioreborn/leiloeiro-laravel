<?php

namespace App\Campaign\Http\Factories\Filters;

use App\Campaign\CampaignType;
use App\Core\Contracts\FilterInterface;

class Campaign_type implements FilterInterface
{

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query Query builder
     * @param mixed                                 $value Value
     * @return mixed
     */
    public function run($query, $value)
    {
        $campaignTypeIds = CampaignType::where('name', 'LIKE', "%{$value}%")->pluck('id');

        return $query->whereIn('campaign_type_id', $campaignTypeIds);
    }
}
