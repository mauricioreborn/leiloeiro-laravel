<?php

namespace App\Campaign\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Carbon\Carbon;

class Scheduled_at implements FilterInterface
{

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query Query builder
     * @param mixed                                 $value Value
     * @return mixed
     */
    public function run($query, $value)
    {
        $date = Carbon::parse($value);

        return $query->whereDate('scheduled_at', '=', $date);
    }
}
