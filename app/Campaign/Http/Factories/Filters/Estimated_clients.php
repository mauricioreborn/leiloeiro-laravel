<?php

namespace App\Campaign\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Estimated_clients implements FilterInterface
{

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query Query builder
     * @param mixed                                 $value Value
     * @return mixed
     */
    public function run($query, $value)
    {
        return $query->where('estimated_clients', 'LIKE', "%{$value}%");
    }
}
