<?php

namespace App\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignTypesResource extends JsonResource
{
    /**
     * CampaignClientsResource
     * @param  Request $request Request entity
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'uid' => $this->uid
        ];
    }
}