<?php

namespace App\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CampaignTypesCollection extends ResourceCollection
{

    /**
     * @param \Illuminate\Http\Request $request request params
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' =>
                $this->collection->map(function ($campaignType) use ($request) {
                    return (new CampaignTypesResource($campaignType))->toArray($request);
                })
        ];
    }
}