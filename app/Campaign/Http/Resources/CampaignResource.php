<?php

namespace App\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignResource extends JsonResource
{

    /**
     * CampaignClientsResource
     * @param  Request $request Request entity
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'campaign',
            'id'         => $this->id,
            'attributes' => [
                'estimated_clients' => $this->estimated_clients,
                'scheduled_at'      => $this->scheduled_at,
                'processed_at'      => $this->processed_at,
            ],
            'relationships' => [
                'user' => $this->whenLoaded('user'),
                'campaign_type' => $this->whenLoaded('campaignType'),
                'fefo' => $this->whenLoaded('fefo'),
            ],
        ];
    }
}
