<?php
namespace App\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignEstimateClientsResource extends JsonResource
{
    /**
     * CampaignClientsResource
     * @param  Request $request Request entity
     * @return array
     */
    public function toArray($request)
    {
        return [
            'total' => count($this['clientIds']),
        ];
    }
}
