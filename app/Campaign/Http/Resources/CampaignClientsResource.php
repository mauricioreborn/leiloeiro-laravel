<?php
namespace App\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignClientsResource extends JsonResource
{
    /**
     * CampaignClientsResource
     * @param  Request $request Request entity
     * @return array
     */
    public function toArray($request)
    {
        return [
            'total' => count($this['clientIds']),
            'clientIds' => $this['clientIds'],
        ];
    }
}
