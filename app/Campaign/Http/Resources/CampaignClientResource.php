<?php
namespace App\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignClientResource extends JsonResource
{
    /**
     * CampaignClientsResource
     * @param  Request $request Request entity
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this['title'],
            'message' => $this['message'],
        ];
    }
}