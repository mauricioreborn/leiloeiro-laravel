<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'campaign'], function () {
        Route::group(['middleware' => ['ValidateServiceAccountJwt']], function () {
            Route::get('/{campaign}/clients', 'CampaignController@clients');
            Route::get('/client/{campaign_client}', 'CampaignController@client');
        });

        Route::group(['middleware' => ['validateJwt', 'sentry']], function () {
            Route::get('/', 'CampaignController@list');
            Route::get('/type/{campaign_type}/estimate', 'CampaignController@estimate');
            Route::post('/', 'CampaignController@store');
            Route::delete('/{campaign}', 'CampaignController@destroy');


            Route::group(['prefix' => 'types'], function () {
                Route::get('/', 'CampaignController@getCampaignType');
            });
        });
    });
});
