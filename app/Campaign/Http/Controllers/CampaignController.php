<?php

namespace App\Campaign\Http\Controllers;

use App\Campaign\Campaign;
use App\Campaign\CampaignClient;
use App\Campaign\CampaignType;
use App\Campaign\Http\Requests\CampaignEstimateRequest;
use App\Campaign\Http\Requests\CampaignRequest;
use App\Campaign\Http\Resources\CampaignClientResource;
use App\Campaign\Http\Resources\CampaignClientsResource;
use App\Campaign\Http\Resources\CampaignResource;
use App\Campaign\Http\Resources\CampaignTypesCollection;
use App\Campaign\Http\Resources\CampaignEstimateClientsResource;
use App\Campaign\Services\CampaignService;
use App\Campaign\Services\CampaignTypeService;
use App\Core\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\UnauthorizedException;

class CampaignController extends Controller
{

    protected $service;


    /**
     * CampaignController constructor.
     * @param CampaignService $service CampaignService
     */
    public function __construct(CampaignService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function list(Request $request): AnonymousResourceCollection
    {
        $this->authorize('allowCampaigns', $request->user()->company);

        $collection = $this->service->search(auth()->user()->company_id, $request->all());

        return CampaignResource::collection($collection);
    }

    /**
     * @param CampaignRequest $request Request object
     * @return CampaignResource
     */
    public function store(CampaignRequest $request): CampaignResource
    {
        $campaign = $this->service->createCompanyCampaign(
            $request->user(),
            $request->get('type'),
            $request->get('scheduled_at'),
            $request->toArray()
        );

        return new CampaignResource($campaign);
    }

    /**
     * Delete unprocessed campaign
     *
     * @param Campaign $campaign Campaign entity
     *
     * @param Request  $request Request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(Campaign $campaign, Request $request)
    {
        $this->authorize('allowCampaigns', $request->user()->company);
        $this->authorize('userAccess', $campaign->company);

        $this->service->deleteCampaign($campaign);

        return new JsonResponse(null, 204);
    }

    /**
     * Get users of campaign
     *
     * @param Request  $request  Request
     * @param Campaign $campaign Campaign Entity
     *
     * @return CampaignClientsResource
     * @throws AuthorizationException
     */
    public function clients(Request $request, Campaign $campaign)
    {
        return new CampaignClientsResource((new CampaignService())->clients($campaign));
    }

    /**
     * Get client message of campaign
     *
     * @param Request        $request        Request
     * @param CampaignClient $campaignClient Campaign Client Entity
     *
     * @return CampaignClientResource
     * @throws AuthorizationException
     */
    public function client(Request $request, CampaignClient $campaignClient)
    {
        return new CampaignClientResource((new CampaignTypeService())->client($campaignClient));
    }

    /**
     * Method to get campaign types
     *
     * @param Request $request Request
     * @return CampaignTypesCollection
     * @throws AuthorizationException
     */
    public function getCampaignType(Request $request)
    {
        return new CampaignTypesCollection((new CampaignTypeService())->search([]));
    }

    /**
     * Get estimate clients of campaign
     *
     * @param CampaignEstimateRequest $request      Request
     * @param CampaignType            $campaignType Campaign Type Entity
     *
     * @return CampaignEstimateClientsResource
     */
    public function estimate(CampaignEstimateRequest $request, CampaignType $campaignType)
    {
        return new CampaignEstimateClientsResource((new CampaignService())->estimate(
            $campaignType,
            auth()->user()->company,
            $request->toArray()
        ));
    }
}
