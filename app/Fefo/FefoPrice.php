<?php

namespace App\Fefo;

use App\Company\Company;
use App\Core\Classes\Transactible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FefoPrice extends Model
{
    use SoftDeletes, Transactible;

    protected $fillable = [
        'company_id',
        'weeks',
        'leadtime_code',
        'product_code',
        'selling_price',
        'expired_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'weeks' => 'integer',
        'leadtime_code' => 'integer',
        'product_code' => 'integer',
        'selling_price' => 'float',
        'expired_at' => 'date',
    ];

    /**
     * company Relationship
     *
     * @return Company
     **/
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
