<?php

namespace App\Fefo\Http\Policies;

use App\Auth\User;
use App\Client\Client;
use App\Fefo\Fefo;
use Illuminate\Auth\Access\HandlesAuthorization;

class FefoCompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Authorize user to access route
     *
     * @param User user $user User object
     * @param Fefo fefo $fefo Company object
     *
     * @return boolean
     **/
    public function userAccess(User $user, Fefo $fefo): bool
    {
        return $user->company_id == $fefo->company_id ? true : false ;
    }

    /**
     * Authorize user to access route
     *
     * @param Client $client Client entity
     * @param Fefo   $fefo   Company entity
     *
     * @return boolean
     **/
    public function clientAccess(Client $client, Fefo $fefo): bool
    {
        return in_array($fefo->company_id, $client->companies->pluck('id')->toArray());
    }
}
