<?php

namespace App\Fefo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class FefoPriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'weeks' => 'required|integer|min: 1',
            'leadtime_code' => 'required|integer|min: 1',
            'product_code' => 'required|integer|min: 1',
        ];
    }
}
