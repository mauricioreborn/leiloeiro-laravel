<?php

namespace App\Fefo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFeaturedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->root === 1 || auth()->user()->featured === 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'highlight_app' => 'required|integer'
        ];
    }
}
