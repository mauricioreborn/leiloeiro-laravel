<?php

namespace App\Fefo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class FefoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date_format:Y-m-d',
            'weeks' => 'required|integer|min: 1',
            'leadtime_code' => 'required|integer|min: 1',
            'product_code' => 'required|integer|min: 1',
            'selling_price' => 'required|between:0,99.99|min: 0.1',
            'max_price' => 'required|between:0,99.99|min: 0.1',
            'min_price' => 'required|between:0,99.99|min: 0.1',
            'changed_admin_price' => 'required|between:0,99.99|min: 0.1',
            'best_before' => 'date_format:Y-m-d',
            'highlight_app' => 'required|boolean',
            'volume' => 'required|between:0,99.99|min: 0.1',
            'cd_empresa' => 'required|integer|min: 1',
            'cd_faixa_fefo' => 'required|integer|min:1',
            'ds_chave_seara' => 'required|string',
        ];
    }
}
