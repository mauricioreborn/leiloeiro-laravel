<?php

namespace App\Fefo\Http\Resources;

use App\Client\Http\Resources\ClientResource;
use App\LeadTime\Http\Resources\LeadTimeResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FefoBidsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        $company    = auth()->user()->company;
        $periodView = $company !== null ? $company->settings['periodView'] : false;
        $periodLabel = $periodView ?
            $this->weeks . ' ' . trans_choice("companySetting.{$periodView}", $this->weeks) : false;

        return [
            'type'          => 'fefo_bid',
            'id'            => $this->id,
            'attributes' => [
                'validade' => $periodLabel?: null,
                'client_id' => $this->client_id,
                'date' => $this->date,
                'origin_code' => $this->origin_code,
                'product_code' => $this->product_code,
                'weeks' => $this->weeks,
                'box_amount' => $this->box_amount,
                'original_weight' => $this->original_weight,
                'weight' => $this->weight,
                'kg_price' => $this->kg_price,
                'price' => $this->price,
                'partial_accept' => $this->partial_accept,
                'duration' => $this->duration,
                'user_id' => $this->user_id,
                'status_id' => $this->status_id,
                'rejection_motives_id' => $this->rejection_motives_id,
                'accept_date' => $this->accept_date,
                'can_purchase' => $this->can_purchase,
                'original_box_amount' => $this->original_box_amount,
                'partial_purchase' => $this->partial_purchase ?? false,
                'discount_percentage' => $this->discount_percentage,
                'original_price' => $this->original_price,
            ],
            'relationships' => [
                'client' => new ClientResource($this->client),
                'origin' => new LeadTimeResource($this->client->origins()->where('code', $this->origin_code)->first()),
                'logistics' => $this->logistics,
                'labels' => $this->labels,
            ],
        ];
    }
}
