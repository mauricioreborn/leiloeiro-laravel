<?php

namespace App\Fefo\Http\Resources;


use App\Bid\Http\Resources\BidResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FefoUpdateWithBidsResource extends JsonResource{


    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'type' => 'bids_for_auto_approval',
            'bids' => BidResource::collection($this['bids']),
            'stats' => $this['stats'],
        ];
    }

}
