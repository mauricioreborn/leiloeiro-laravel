<?php
namespace App\Fefo\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FefoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'fefo',
            'id' => $this->id,
            'attributes' => [
                'validade' => $this->periodLabel,
                'date' => $this->date,
                'weeks' => $this->weeks,
                'leadtime_code' => $this->leadtime_code,
                'origin' => $this->origin,
                'product_code' => $this->product_code,
                'product_name' => $this->product_name,
                'product_type' => $this->product_type,
                'volume' => $this->volume,
                'selling_price' => $this->selling_price,
                'max_price' => $this->max_price,
                'min_price' => $this->min_price,
                'highlight_app' => $this->highlight_app,
                'faixa_fefo' => $this->faixa_fefo,
                'product' => $this->product,
            ],
        ];
    }
}
