<?php

namespace App\Fefo\Http\Resources;

use App\Client\Http\Resources\ClientResource;
use App\LeadTime\Http\Resources\LeadTimeResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FefoStatisticsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'fefo_statistics',
            'attributes' => [
                'price' => [
                    'min' => (float) array_get($this, 'min_price', 0),
                    'max' => (float) array_get($this, 'max_price', 0),
                    'average' => (float) bcdiv(array_get($this, 'average_price', 0), 1, 2),
                    'mode' => (float) array_get($this, 'mode_price', 0),
                ],
                'volume' => [
                    'min' => (float) array_get($this, 'min_volume', 0),
                    'max' => (float) array_get($this, 'max_volume', 0),
                    'average' => (float) bcdiv(array_get($this, 'average_volume', 0), 1, 2),
                ],
            ],
        ];
    }
}
