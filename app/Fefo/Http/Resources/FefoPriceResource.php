<?php

namespace App\Fefo\Http\Resources;

use App\LeadTime\Http\Resources\LeadTimeResource;
use App\Products\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FefoPriceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'fefo_price',
            'id' => $this->id,
            'attributes'    => [
                'weeks' => $this->weeks,
                'leadtime_code' => $this->leadtime_code,
                'product_code' => $this->product_code,
                'selling_price' => $this->selling_price,
                'expired_at' => $this->expired_at->toDateString()
            ],
            'relationships' => [
                'company' => new ProductResource($this->whenLoaded('company')),
            ],
        ];
    }
}
