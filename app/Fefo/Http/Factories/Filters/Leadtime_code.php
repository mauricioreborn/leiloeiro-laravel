<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Leadtime_code implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('leadtime_code', $param);
    }
}
