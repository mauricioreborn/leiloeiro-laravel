<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Sum implements FilterInterface
{
    /**
     * @param Builder $query    Query builder
     * @param mixed   $sumValue sumValue
     * @return Builder
     */
    public function run($query, $sumValue)
    {
        return $query->sum($sumValue);
    }
}
