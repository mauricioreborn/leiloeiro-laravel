<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Carbon\Carbon;

class Faixa_fefo implements FilterInterface
{

    /**
     * @param Builder $query Query builder
     * @param mixed   $param Param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('faixa_fefo', $param);
    }
}
