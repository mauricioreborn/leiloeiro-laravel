<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Selling_price implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('selling_price', $param);
    }
}
