<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Carbon\Carbon;

class Date implements FilterInterface
{

    /**
     * @param Builder $query Query builder
     * @param mixed   $date  Search date
     * @return Builder
     */
    public function run($query, $date)
    {
        $date = Carbon::parse($date);

        return $query->whereDate('date', '=', $date);
    }
}
