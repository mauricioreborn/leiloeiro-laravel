<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use App\Products\Product;
use Illuminate\Database\Query\Builder;

class Product_name implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        $products = Product::where('name', 'LIKE', "%{$param}%")->pluck('code');

        return $query->whereIn('product_code', $products);
    }
}
