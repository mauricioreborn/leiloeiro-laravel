<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Product_code implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('product_code', $param);
    }
}
