<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Volume implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('volume', $param);
    }
}
