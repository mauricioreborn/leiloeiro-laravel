<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use App\LeadTime\LeadTime;
use Illuminate\Support\Facades\DB;

class Origin implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        $origins = DB::table('leadtime')->distinct()->where('nome', 'LIKE', "%{$param}%")->pluck('cod_origem');

        return $query->whereIn('leadtime_code', $origins);
    }
}
