<?php

namespace App\Fefo\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Carbon\Carbon;

class Batch implements FilterInterface
{

    /**
     * @param Builder $query Query builder
     * @param mixed   $param Value
     *
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('ds_chave_seara', '=', $param);
    }
}
