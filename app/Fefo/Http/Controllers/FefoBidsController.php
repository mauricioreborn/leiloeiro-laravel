<?php

namespace App\Fefo\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Fefo\Fefo;
use App\Fefo\Http\Requests\FefoBidsApproveRequest;
use App\Fefo\Http\Resources\FefoBidsResource;
use App\Fefo\Services\FefoBidsService;
use App\Fefo\Services\FefoService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FefoBidsController extends Controller
{
    protected $fefoService;

    protected $fefoBidsService;

    /**
     * FefoBidsController constructor.
     *
     * @param FefoService     $fefoService     FefoService
     * @param FefoBidsService $fefoBidsService FefoBidsService
     *
     * @return void
     */
    public function __construct(FefoService $fefoService, FefoBidsService $fefoBidsService)
    {
        $this->fefoService = $fefoService;
        $this->fefoBidsService = $fefoBidsService;
    }

    /**
     * Method to get list of bids by fefo id
     *
     * @param integer $id Fefo ID
     *
     * @return AnonymousResourceCollection
     */
    public function list($id)
    {
        $fefo = $this->fefoService->find($id);
        $list = $this->fefoBidsService->getFefoBids($fefo);

        return FefoBidsResource::collection($list);
    }

    /**
     * Approve bids by fefo
     *
     * @param FefoBidsApproveRequest $request Request object
     * @param Fefo                   $fefo    Fefo entity
     *
     * @return array
     */
    public function approve(FefoBidsApproveRequest $request, Fefo $fefo)
    {
        $bids = $this->fefoBidsService->approve($fefo, $request->get('bid_ids'));

        return [
            'status' => 200,
            'bids' => $bids
        ];
    }

    /**
     * Approve bids by fefo
     *
     * @param FefoBidsApproveRequest $request Request object
     * @param Fefo                   $fefo    Fefo entity
     *
     * @return array
     */
    public function disapprove(FefoBidsApproveRequest $request, Fefo $fefo)
    {
        $bids = $this->fefoBidsService->disapprove($fefo, $request->get('bid_ids'));

        return [
            'status' => 200,
            'bids' => $bids
        ];
    }
}
