<?php

namespace App\Fefo\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Fefo\Fefo;
use App\Fefo\Http\Requests\FefoRequest;
use App\Fefo\Http\Requests\FefoStatisticsRequest;
use App\Fefo\Http\Requests\UpdateFeaturedRequest;
use App\Fefo\Http\Requests\UpdateFefoRequest;
use App\Fefo\Http\Resources\FefoResource;
use App\Fefo\Http\Resources\FefoStatisticsResource;
use App\Fefo\Http\Resources\FefoUpdateWithBidsResource;
use App\Fefo\Services\FefoBidsService;
use App\Fefo\Services\FefoService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use http\Exception\RuntimeException;

class FefoController extends Controller
{
    protected $fefoService;

    protected $fefoBidService;

    /**
     * FefoController constructor.
     * @param FefoService     $fefo           Service class
     * @param FefoBidsService $fefoBidService Service class
     */
    public function __construct(FefoService $fefo, FefoBidsService $fefoBidService)
    {
        $this->fefoService = $fefo;
        $this->fefoBidService = $fefoBidService;
    }

    /**
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     */
    public function list(Request $request)
    {
        $filters = $request->all();
        $filters['date'] = now()->toDateString();

        $collection = $this->fefoService->search($filters, auth()->user()->company);

        return FefoResource::collection($collection);
    }

    /**
     * Show Fefo
     * @param Request $request Request instance
     * @param integer $id      Fefo ID
     * @return FefoResource
     *
     * @throws AuthorizationException
     */
    public function show(Request $request, Fefo $fefo)
    {
        $this->authorize('userAccess', $fefo);
        $relations = $request->has('include') ? explode(',', $request->get('include')) : [];
        $fefo = $this->fefoService->find($fefo->id, $relations);

        $this->authorize('userAccess', $fefo);

        return new FefoResource($fefo);
    }

    /**
     * Statistics Fefo
     * @param Request $request Request instance
     * @param Fefo    $fefo    Fefo entity
     *
     * @return FefoStatisticsResource
     */
    public function statistics(FefoStatisticsRequest $request, Fefo $fefo)
    {
        $statistics = $this->fefoBidService->statistics($fefo, $request->limit, $request->status_id);

        return new FefoStatisticsResource($statistics);
    }

    /**
     * @param UpdateFefoRequest $request Request object
     * @param integer           $id      Fefo id
     * @return FefoResource|FefoUpdateWithBidsResource
     * @throws AuthorizationException
     * @throws RuntimeException
     */
    public function update(UpdateFefoRequest $request, $id)
    {
        $fefo = $this->fefoService->find($id);
        $price = $request->get('price');
        $date = $request->get('date');

        // Verify user permission
        if(!$fefo->canBeUpdatedByUser()) {
            throw new AuthorizationException('You do not have the necessary permissions to proceed with this action.');
        }

        // Verify valid price
        $this->fefoService->validatePrice($fefo, $price);

        // Get list of eligible bids for auto approval
        // Get stats based on bids returned
        if(!$request->has('confirm_update')) {
            $eligibleBids = $this->fefoBidService->checkForEligibleBids($fefo, $price);

            if($eligibleBids['hasEligibleBids']) {
                return new FefoUpdateWithBidsResource($eligibleBids);
            }
        }

        // Save fefo if confirmed or no eligible bids are found
        if(!$this->fefoService->update($fefo, ['selling_price' => $price], $date)) {
            throw new RuntimeException('An error has ocurred. Your action was not completed.');
        }

        return new FefoResource($fefo->fresh());
    }

    /**
     * Method to get stats on fefo
     * @param Request $request Request object
     * @return array
     */
    public function stats(Request $request)
    {
        $fefoService = new FefoService();
        return $fefoService->stats($request->user()->company_id);
    }

    /**
     * @param UpdateFeaturedRequest $request Request object
     * @param integer               $id      Fefo id
     * @return FefoResource
     */
    public function featured(UpdateFeaturedRequest $request, $id)
    {
        $fefo = $this->fefoService->find($id);

        $this->fefoService->toggleFeatured($fefo, $request->get('highlight_app'));

        return new FefoResource($fefo->fresh('product', 'leadtime'));
    }
}
