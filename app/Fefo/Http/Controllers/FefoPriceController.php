<?php

namespace App\Fefo\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Fefo\Http\Requests\FefoPriceRequest;
use App\Fefo\Http\Resources\FefoPriceResource;
use App\Fefo\Services\FefoPriceService;

class FefoPriceController extends Controller
{
    protected $service;

    /**
     * FefoPriceController constructor.
     *
     * @param FefoPriceService $service FefoPrice service
     */
    public function __construct(FefoPriceService $service)
    {
        $this->service = $service;
    }

    /**
     * Show fefo price
     *
     * @param Request $request Request object
     *
     * @return AnonymousResourceCollection
     */
    public function show(FefoPriceRequest $request)
    {
        $fefoPrice = $this->service->show(
            auth()->user()->company,
            $request->get('weeks'),
            $request->get('leadtime_code'),
            $request->get('product_code')
        );

        return new FefoPriceResource($fefoPrice);
    }
}
