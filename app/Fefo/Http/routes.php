<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => ['validateJwt', 'sentry']], function () {
        Route::group(['prefix' => 'fefo_price'], function () {
            Route::get('/', 'FefoPriceController@show');
        });

        Route::group(['prefix' => 'fefo'], function () {
            Route::get('/stats/volume', 'FefoController@stats');

            Route::get('/', 'FefoController@list');
            Route::get('/{fefo}', 'FefoController@show');
            Route::put('/{id}', 'FefoController@update');
            Route::put('/{id}/featured', 'FefoController@featured');
            Route::get('/{id}/bids', 'FefoBidsController@list');
            Route::get('/{fefo}/statistics', 'FefoController@statistics');
            Route::put('/{fefo}/bids/approve', 'FefoBidsController@approve');
            Route::put('/{fefo}/bids/disapprove', 'FefoBidsController@disapprove');
        });
    });

    Route::group(['prefix' => 'service', 'middleware' => 'ValidateServiceAccountJwt'], function () {
        Route::group(['prefix' => 'fefo'], function () {
            Route::put('/{fefo}/bids/approve', 'FefoBidsController@approve');
        });
    });
});