<?php

namespace App\Fefo;

use App\Company\Company;
use App\Core\Classes\Transactible;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class Fefo extends Model
{

    use Eloquence, Mappable, SoftDeletes, Transactible;

    protected $table = 'fefo';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates    = ['deleted_at'];

    protected $fillable = [
        'company_id',
        'semanas',
        'cod_origem',
        'codigo_produto',
        'volume',
        'valor_atual',
        'valor_cheio',
        'valor_min',
        'valor_alterado_admin',
        'vencimento',
        'destaque',
        'cd_empresa',
        'faixa_fefo',
        'cd_faixa_fefo',
        'ds_chave_seara',
        'date',

        //en
        'weeks',
        'leadtime_code',
        'product_code',
        'selling_price',
        'max_price',
        'min_price',
        'changed_admin_price',
        'best_before',
        'highlight_app'
    ];

    protected $hidden = [
        'semanas',
        'cod_origem',
        'codigo_produto',
        'valor_atual',
        'valor_cheio',
        'valor_min',
        'valor_alterado_admin',
        'vencimento',
        'destaque',
    ];

    protected $appends  = [
        'weeks',
        'leadtime_code',
        'product_code',
        'selling_price',
        'max_price',
        'min_price',
        'changed_admin_price',
        'best_before',
        'highlight_app',
    ];

    // // legacy db mapping
    protected $maps   = [
        'weeks'               => 'semanas',
        'leadtime_code'       => 'cod_origem',
        'product_code'        => 'codigo_produto',
        'selling_price'       => 'valor_atual',
        'max_price'           => 'valor_cheio',
        'min_price'           => 'valor_min',
        'changed_admin_price' => 'valor_alterado_admin',
        'best_before'         => 'vencimento',
        'highlight_app'       => 'destaque',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'codigo_produto', 'codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leadtime()
    {
        return $this->belongsTo(LeadTime::class, 'cod_origem', 'cod_origem')->groupBy('cod_origem');
    }

    /**
     * Company relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @param Builder $query Query builder
     * @return Builder
     */
    public function scopeTodayOrEarlier($query)
    {
        return $query->where('date', '<=', today()->toDateString());
    }

    /**
     * @return Builder
     */
    public function scopeToday()
    {
        return $this->where('date', date('Y-m-d'));
    }

    /**
     * @return Builder
     */
    public function scopeYesterday()
    {
        return $this->where('date', '=', today()->subDay(1)->toDateString());
    }

    /**
     * @return bool
     */
    public function canBeUpdatedByUser()
    {
        $user = auth()->user();

        return $user->root || ($user->price && (bool) $this->product->users->find($user->id));
    }

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }
}
