<?php

namespace App\Fefo;

use App\Client\Client;
use App\Company\Company;
use App\Core\Classes\Transactible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FefoPriceBoost extends Model
{
    use SoftDeletes, Transactible;

    protected $fillable = [
        'fefo_price_id',
        'client_id',
        'start_date',
        'finish_date',
        'expired_at',
        'last_view'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * company Relationship
     *
     * @return Company
     **/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * company Relationship
     *
     * @return Company
     **/
    public function fefoPrice()
    {
        return $this->belongsTo(FefoPrice::class, 'fefo_price_id');
    }
}