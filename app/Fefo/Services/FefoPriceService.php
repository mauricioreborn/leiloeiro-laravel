<?php

namespace App\Fefo\Services;

use App\Company\Company;
use App\Fefo\FefoPrice;

class FefoPriceService
{
    /**
     * create fefo price
     *
     * @param Company $company      Company Entity
     * @param int     $weeks        Week value
     * @param int     $leadtimeCode Leadtime code
     * @param int     $productCode  Product code
     * @param float   $sellingPrice Selling price
     * @param string  $expiredAt    Date of expired
     *
     * @return FefoPrice|bool
     */
    public function create(
        Company $company,
        int $weeks,
        int $leadtimeCode,
        int $productCode,
        float $sellingPrice,
        string $expiredAt = null
    ) {
        $fefoPrice = FefoPrice::where('company_id', $company->id)
            ->where('weeks', $weeks)
            ->where('leadtime_code', $leadtimeCode)
            ->where('product_code', $productCode)
            ->where('expired_at', '>=', now()->toDateString())
            ->first();

        if ($fefoPrice) {
            if (!$expiredAt) {
                $fefoPrice->delete();

                return false;
            }

            $fefoPrice->selling_price = $sellingPrice;
            $fefoPrice->expired_at = $expiredAt;

            $fefoPrice->save();

            return $fefoPrice->fresh();
        } else {
            if ($expiredAt) {
                return FefoPrice::create([
                    'company_id' => $company->id,
                    'weeks' => $weeks,
                    'leadtime_code' => $leadtimeCode,
                    'product_code' => $productCode,
                    'selling_price' => $sellingPrice,
                    'expired_at' => $expiredAt,
                ]);
            }
        }

        return false;
    }

    /**
     * show fefo price by fefo params
     *
     * @param Company $company      Company Entity
     * @param int     $weeks        Week value
     * @param int     $leadtimeCode Leadtime code
     * @param int     $productCode  Product code
     *
     * @return FefoPrice|bool
     */
    public function show(Company $company, int $weeks, int $leadtimeCode, int $productCode)
    {
        return $this->search($company, $weeks, $leadtimeCode, $productCode)->firstOrFail();
    }

    /**
     * search fefo price by fefo params
     *
     * @param Company $company      Company Entity
     * @param int     $weeks        Week value
     * @param int     $leadtimeCode Leadtime code
     * @param int     $productCode  Product code
     *
     * @return Builder
     */
    public function search(Company $company, int $weeks, int $leadtimeCode, int $productCode)
    {
        return FefoPrice::where('company_id', $company->id)
            ->where('weeks', $weeks)
            ->where('leadtime_code', $leadtimeCode)
            ->where('product_code', $productCode)
            ->where('expired_at', '>=', now()->toDateString());
    }
}