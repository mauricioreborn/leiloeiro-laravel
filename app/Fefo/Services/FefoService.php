<?php

namespace App\Fefo\Services;

use App\Bid\Services\BidService;
use App\Company\Company;
use App\Company\CompanySetting;
use App\Company\Services\CompanySettingService;
use App\LeadTime\LeadTime;
use App\ProcessedFefo\Service\ProcessedFefoService;
use App\Core\Classes\CrudService;
use App\Core\Classes\Search;
use App\Fefo\Fefo;
use App\Products\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FefoService extends CrudService
{
    protected $entity = Fefo::class;

    protected $module = 'Fefo';

    /**
     * @param array        $filters Filters
     * @param Company|null $company
     * @return mixed
     */
    public function search($filters, Company $company = null)
    {
        $availableOriginsQuery = LeadTime::fromCompany($company->id);

        if(isset($filters['origin'])) {
            $filters['origin_code'] = $filters['origin'];
        }

        if(isset($filters['origin_code'])) {
            $availableOriginsQuery->where(function ($query) use ($filters) {
                return $query->where('cod_origem', $filters['origin_code'])
                    ->orWhere('nome', 'LIKE', "%{$filters['origin_code']}%");
            });
        }

        $availableOrigins = $availableOriginsQuery->listed();

        $availableProductsQuery = Product::fromCompany($company->id);

        if(isset($filters['product_code'])) {
            $availableProductsQuery->where('code', $filters['product_code']);
        }

        if(isset($filters['product_name'])) {
            $availableProductsQuery->where('name', 'LIKE', "%{$filters['product_name']}%");
        }

        if(isset($filters['product_type'])) {
            $availableProductsQuery->where('type', 'LIKE', "%{$filters['product_type']}%");
        }

        $availableProducts = $availableProductsQuery->get();

        $columns = [
            'id',
            'date',
            'weeks',
            'leadtime_code',
            'product_code',
            'selling_price',
            'max_price',
            'min_price',
            'volume',
            'highlight_app',
            'faixa_fefo',
        ];

        $query = $this->newQuery()->select($columns)
            ->where('fefo.company_id', $company->id)
            ->whereIn('leadtime_code', $availableOrigins->keys())
            ->whereIn('product_code', $availableProducts->pluck('code')->toArray())
            ->groupBy('fefo.id');

        if(isset($filters['date'])) {
            $query->where('fefo.date', $filters['date']);
        }

        if(isset($filters['weeks'])) {
            $query->where('fefo.semanas', $filters['weeks']);
        }

        if(isset($filters['faixa_fefo'])) {
            $query->where('fefo.faixa_fefo', $filters['faixa_fefo']);
        }

        if(isset($filters['volume'])) {
            $filters['volume'] = preg_replace("/[^0-9]/", "", $filters['volume']);
            $query->where('volume', 'LIKE', "%{$filters['volume']}%");
        }

        if(isset($filters['selling_price'])) {
            $filters['selling_price'] = preg_replace("/[^0-9]/", "", $filters['selling_price']);
            $query->where('selling_price', 'LIKE', "%{$filters['selling_price']}%");
        }

        if(isset($filters['max_price'])) {
            $filters['max_price'] = preg_replace("/[^0-9]/", "", $filters['max_price']);
            $query->where('max_price', 'LIKE', "%{$filters['max_price']}%");
        }

        if(isset($filters['featured']) && $filters['featured']) {
            $query->where('highlight_app', true);
        }

        if(isset($filters['sort'])){
            $direction = strpos($filters['sort'], '-') === false ? 'asc' : 'desc';
            $column    = ltrim($filters['sort'], '-');

            if(in_array($column, $columns)){
                $query->orderBy($column, $direction);
            }
        }

        $paginator = $query->paginate($filters['limit'] ?? 15);
        $periodView = $company->settings['periodView'];

        $paginator->getCollection()
            ->transform(function ($fefo) use ($availableOrigins, $availableProducts, $periodView) {
                $fefo->origin = $availableOrigins[$fefo->leadtime_code];
                $fefo->periodLabel = $fefo->weeks . ' ' . trans_choice("companySetting.{$periodView}", $fefo->weeks);

                $product = $availableProducts->where('code', $fefo->product_code)->first();

                $fefo->product = $product->only([
                    'name', 'type', 'pieces', 'unit', 'weight_piece', 'brand', 'picture_url'
                ]);

                $fefo->product_name = $product->name;
                $fefo->product_type = $product->type;

                return $fefo;
            });

        return $paginator;
    }

    /**
     * @param array         $filters   Filters
     * @param interger|bool $limit     Limit
     * @param integer       $companyId Company ID
     *
     * @return mixed
     */
    public function searchWithoutPagination($filters, $limit = false, $companyId = null)
    {
        $searchInstance = new Search($this->module, $this->newQuery());

        $query = $searchInstance->run($filters);

        if ($companyId) {
            $query->fromCompany($companyId);
        }

        if($limit) {
            $query->take($limit);
        }

        return $query->get();
    }

    /**
     * @param array $params Params
     * @return Fefo
     */
    public function createFefo(array $params)
    {
        $fefo = new Fefo();

        $searchFields = [
            'company_id' => array_get($params, 'company_id'),
            'date' => array_get($params, 'date'),
            'semanas' =>  array_get($params, 'weeks'),
            'cod_origem' =>  array_get($params, 'leadtime_code'),
            'codigo_produto' => array_get($params, 'product_code'),
        ];

        $fefo = Fefo::withTrashed()->where($searchFields)->first();

        if ($fefo) {
            $fefo->restore();
            $fefo->update($params);

            return $fefo->fresh();
        }

        return $this->create($params);
    }

    /**
     * Method to get total products
     * @param integer $company Company Id
     * @return array
     */
    public function stats($company)
    {
        $fefo = new Fefo();
        $fefoData = $fefo->scopeToday()->join('produtos', 'codigo_produto', 'codigo')
            ->where('produtos.company_id', $company)
            ->whereNull('produtos.deleted_at')->get();
        $product = new Product();
        $produtos = $product->get();

        $query = "select distinct nome as name, cod_origem as code from leadtime where company_id = $company and deleted_at is not null";
        $leadTimeDatas = DB::select($query);

        $dados = array_reverse($fefoData->toArray());

        $total_volume = 0;

        $tabela_familias = [];
        $familias = [];

        $tabela_origens = [];
        $origens = [];

        $tmp_tabela_semanas = [];
        $tabela_semanas = [];

          if ($produtos) {
              foreach ($produtos as $produto) {
                  $tabela_familias[$produto->type] = 0;
                  $familias[$produto->code] = $produto->type;
              }
          }


        if ($leadTimeDatas) {
            foreach ($leadTimeDatas as $origem) {
                $tabela_origens[$origem->name] = 0;
                $origens[$origem->code] = $origem->name;
            }
        }

        foreach ($dados as $row) {
            /* Total de volume do DIA */
            $total_volume += $row['volume'];

            /* Total de volume por CLASSE */
            if (isset($familias[$row['product_code']])) {
                $tabela_familias[$familias[$row['product_code']]] += $row['volume'];
            }

            /* Total de volume por ORIGENS */
            if (isset($origens[$row['leadtime_code']])) {
                $tabela_origens[$origens[$row['leadtime_code']]] += $row['volume'];
            }


            if (!isset($tmp_tabela_semanas[$row['weeks']])) {
                $tmp_tabela_semanas[$row['weeks']] = $row['volume'];
            } else {
                $tmp_tabela_semanas[$row['weeks']] += $row['volume'];
            }
        }


        arsort($tmp_tabela_semanas);
        $contador = 0;
        $restante_semanas = 100;

        foreach ($tmp_tabela_semanas as $semanas => $volume) {
            if ($volume == 0) {
                unset($tmp_tabela_semanas[$semanas]);
                continue;
            }

            if ($contador < 4) {
                $companySettingService = new CompanySettingService();
                $slug = $semanas . ' ' . $companySettingService->getPeriodView(Company::find($company), $semanas);

                $tabela_semanas[$slug] = round((($volume / $total_volume) * 100));

                $restante_semanas -= $tabela_semanas[$slug];
            }

            $contador++;
        }

        arsort($tabela_familias);
        $contador = 0;
        $restante_familias = $total_volume;
        foreach ($tabela_familias as $key => &$row) {
            if ($row == 0) {
                unset($tabela_familias[$key]);
                continue;
            }

            if ($contador < 4) {
                $restante_familias -= $row;
            } else {
                unset($tabela_familias[$key]);
            }

            $contador++;
        }

        arsort($tabela_origens);

        $contador = 0;
        $restante_origens = $total_volume;
        foreach ($tabela_origens as $key => &$row) {
            if ($row == 0) {
                unset($tabela_origens[$key]);
                continue;
            }

            if ($contador < 4) {
                $restante_origens -= $row;
            } else {
                unset($tabela_origens[$key]);
            }

            $contador++;
        }

        // descomenta que vai dar pau .

        foreach ($tabela_familias as $key => $value) {
            $tabela_familias[$key] = number_format((float) $value,0,',','.');
        }

        foreach ($tabela_origens as $key => $value) {
            $tabela_origens[$key] = number_format((float) $value,0,',','.');
        }

        $array = array(
            "total" => round($total_volume, 3),
            "tabela_familias" => $tabela_familias,
            "tabela_origens" => $tabela_origens,
            "tabela_semanas" => $tabela_semanas,
            "restante_semanas" => round($restante_semanas),
            "restante_familias" => number_format($restante_familias,0,',','.'),
            "restante_origens" => number_format($restante_origens,0,',','.')
        );

        return $array;
    }

    /**
     * @param Fefo  $fefo  Fefo instance
     * @param float $price Price
     * @return void
     */
    public function validatePrice($fefo, $price): void
    {
        $minPrice           = $fefo->min_price;
        $minAcceptablePrice = round($minPrice - ($minPrice * 0.3), 2);

        if($price < $minAcceptablePrice) {
            throw new \InvalidArgumentException(__('Fefo/errors.price_too_low', [
                'minPrice' => number_format($minAcceptablePrice, 2, ',', '.')
            ]));
        }
    }

    /**
     * @param mixed  $model    id|Model instance
     * @param array  $data     Data
     * @param string $validate Validate of price
     *
     * @return bool
     */
    public function update($model, array $data = [], string $validate = null): bool
    {
        if (!$model instanceof Model) {
            $model = $this->find($model);
        }

        $this->setModelData($model, $data);

        $model->changed_admin_price = 1;

        $this->save($model);

        $processedFefoService = new ProcessedFefoService();

        $processedFefoService->processedFefo($model->id, now());

        $model = $model->fresh();


        (new FefoPriceService())->create(
            $model->company,
            $model->weeks,
            $model->leadtime_code,
            $model->product_code,
            $model->selling_price,
            $validate
        );

        return true;
    }

    /**
     * update fefo without range price bot
     *
     * @param Fefo  $fefo Model instance
     * @param array $data Data
     *
     * @return Fefo
     */
    public function updateFefo(Fefo $fefo, array $data): Fefo
    {
        $fefo->update($data);

        return $fefo->fresh();
    }

    /**
     * @param mixed   $model id|Model instance
     * @param boolean $state Featured state
     * @return bool
     */
    public function toggleFeatured($model, $state): bool
    {
        if (!$model instanceof Model) {
            $model = $this->find($model);
        }

        $model->highlight_app = $state;

        return $this->save($model);
    }

    /**
     * decrement volume Fefo
     *
     * @param Fefo  $fefo   Fefo entity
     * @param float $weight weight volume
     *
     * @return Fefo
     */
    public function decrementVolume(Fefo $fefo, float $weight): Fefo
    {
        $fefo->decrement('volume', $weight);

        return $fefo;
    }

    public function replicate()
    {
        $companies = Company::with('companySettings')
            ->whereHas('companySettings', function ($query) {
                return $query->where(['setting' => 'hasFefoReplicate', 'value' => 1]);
            })
            ->get()
            ->pluck('id');

        $fefo = new Fefo();

        if($companies){

            foreach($companies as $companyId){

                $fefoData = $fefo->scopeYesterday()->fromCompany($companyId)
                    ->select("company_id" , "date", "volume", "faixa_fefo", "weeks", "leadtime_code", "product_code",
                        "selling_price", "max_price", "min_price", "best_before","highlight_app",
                        "cd_faixa_fefo", "cd_empresa", "ds_chave_seara")
                    ->where('cd_faixa_fefo', '<>', 0)
                    ->where('cd_empresa', '<>', 0)
                    ->get()
                    ->toArray();

                if($fefoData){

                    Log::info("Replicando ".count($fefoData)." FEFOS para Company_id ".$companyId);

                    foreach($fefoData as $fefoRow){

                        $fefoRow['date'] = today()->toDateString();
                        $fefoRow['changed_admin_price'] = 0;

                        $this->createFefo($fefoRow);
                    }
                }
            }
        }
    }

    /**
     * @param Stock $stock Stock Model
     * @return Fefo
     */
    public function searchFefoOrderWeeks($stock)
    {
        return Fefo::where('weeks', '>=', $stock->period)
            ->where('company_id', '=', $stock->company->id)
            ->where('cod_origem', '=', $stock->origin_code)
            ->where('codigo_produto', '=', $stock->product->code)
            ->where('ds_chave_seara', '=', $stock->batch)
            ->orderBy('weeks', 'asc')
            ->first();
    }
}
