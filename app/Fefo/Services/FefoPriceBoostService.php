<?php

namespace App\Fefo\Services;

use App\Client\Client;
use App\Company\Company;
use App\Fefo\Fefo;
use App\Fefo\FefoPrice;
use App\Fefo\FefoPriceBoost;

class FefoPriceBoostService
{

    /**
     * Method to create a fefo price boost also to update fefo and fefo price
     * entities with fefo price boost values
     * @param FefoPrice  $fefoPrice  fefo price object
     * @param Client     $client     client object
     * @param \DateTime  $startDate  start dt fefo price campaign
     * @param \DateTime  $finishDate finish at fefo price campaign
     * @return bool|object
     */
    public function createFefoPriceBoostWithRules(
        FefoPrice $fefoPrice,
        Client $client,
        $startDate,
        $finishDate
    )
    {
        $fefoPriceBoost = FefoPriceBoost::
        where('fefo_price_id', $fefoPrice->id)
            ->where('client_id', $client->id)
            ->where('start_date', $startDate)
            ->first();

        $today = now()->toDateString();
        if ($fefoPriceBoost) {
            if ($today > $finishDate) {
                $fefoPriceBoost->delete();
                return false;
            }

            $fefoPriceBoost->finish_date = $finishDate;
            $fefoPriceBoost->save();

            return $fefoPriceBoost->fresh();
        } else {
            if ($finishDate) {
                return FefoPriceBoost::create([
                    'client_id' => $client->id,
                    'fefo_price_id' => $fefoPrice->id,
                    'start_date' => $startDate,
                    'finish_date' => $finishDate,
                    '$lastView' => null
                ]);
            }
        }
    }

    /**
     * Method to get mobile fefo price boost
     * @param Client  $client client object
     * @param Company $company company entity
     * @return array|bool
     */
    public function getMobileFefoPriceBoost(Client $client, Company $company)
    {
        $fefoPriceBostArray = false;

        $fefoPriceBosts = FefoPriceBoost::where([
                'client_id' => $client->id,
            ])
            ->whereRaw('? between start_date and finish_date', [now()->toDateTimeString()])
            ->orderBy('last_view')
            ->groupBy('fefo_price_id')
            ->with(['fefoPrice' => function ($query) use ($company) {
                $query->where('fefo_prices.company_id', '=', $company->id);
            }])
            ->get();

        foreach ($fefoPriceBosts as $fefoPriceBost) {
            $fefoPrice = $fefoPriceBost->fefoPrice;

            if (!isset($fefoPrice->id)) {
                continue;
            }

            $fefo = Fefo::where([
                    'semanas' => $fefoPrice->weeks,
                    'codigo_produto' => $fefoPrice->product_code,
                    'cod_origem' => $fefoPrice->leadtime_code,
                    'company_id' => $company->id,
                    'date' => now()->toDateString()
                ])
                ->orderBy('valor_atual', 'asc')->first();

            if (!isset($fefo->id)) {
                continue;
            }
            $discount_percentage = floor(
                ($fefo->max_price - $fefo->selling_price) / $fefo->max_price * 100
            );

            $unred = false;

            if ($fefoPriceBost->last_view == null) {
                $fefoPriceBost->update([
                    'last_view' => now()
                ]);
                $unred = true;
            }

            $fefoPriceBostArray[] =  [
                'title' => __('Mobile/boost.title'),
                'codigo_produto' => $fefo->id,
                'empresa' => $company->name,
                'img' => $fefo->product->picture_url,
                'msgPreco' => __('Mobile/boost.priceMessage'),
                'nome' => $fefo->product->name,
                'padraoMedida' => $fefo->product->unit,
                'precoAtual' => formata_moeda($fefo->selling_price),
                'precoAtualUn' => unitPrice($fefo->selling_price, $fefo->product->weight_piece),
                'precoMin' => formata_moeda($fefo->min_price),
                'unidadeMedida' => getUnit($fefo->selling_price, $fefo->product->weight_piece, $fefo->product->unit, true),
                'msgDesconto' => "$discount_percentage%",
                'msgPromo' => __('Mobile/boost.descontMessage'),
                'unread' => $unred,
            ];
        }

        return $fefoPriceBostArray;
    }
}