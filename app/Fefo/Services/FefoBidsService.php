<?php

namespace App\Fefo\Services;

use App\Bid\Bid;
use App\Bid\Services\BidLogService;
use App\Bid\Services\BidService;
use App\Client\Services\ClientService;
use App\Client\Services\TransactionalEmailsService;
use App\Company\Company;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\Notification\Services\NotificationService;
use App\Order\Events\OrderCanceledEvent;
use App\Order\Services\OrderService;
use Illuminate\Support\Collection;

/**
 * Class FefoBidsService
 * @package App\Fefo\Services
 */
class FefoBidsService
{
    protected $orderService;

    /**
     * Class constructor method.
     */
    public function __construct()
    {
        $this->orderService = new OrderService();
    }

    /**
     * @param Fefo  $fefo  Fefo model object
     * @param float $price Price provided by user
     * @return array
     */
    public function checkForEligibleBids($fefo, $price)
    {
        $bids = $this->getEligibleBids($fefo, $price);

        $finalPrice  = $bids->sum('price');
        $finalWeight = $bids->sum('weight');

        $kgPrice = $finalPrice > 0 && $finalPrice > 0 ? round($finalPrice / $finalWeight, 2) : 0;

        return [
            'hasEligibleBids' => $bids->count() > 0,
            'bids'            => $bids,
            'stats'           => [
                'weight'   => $finalWeight,
                'price'    => $finalPrice,
                'kg_price' => $kgPrice,
                'clients'  => $bids->pluck('client_id')->unique()->count(),
            ],
        ];
    }

    /**
     * @param Fefo  $fefo  Fefo model object
     * @param float $price Price provided by user
     * @return Collection
     */
    public function getEligibleBids($fefo, $price): Collection
    {
        $bids = $this->getFefoBids($fefo)
            ->reject(function ($bid) use ($price) {
                return $bid->kg_price < $price;
            });

        return $bids->where('can_purchase', true);
    }

    /**
     * @param Fefo $fefo Fefo model object
     * @return Collection
     */
    public function getFefoBids($fefo): Collection
    {
        return (new BidService)->findByCriteria($fefo, [
            'cod_produto' => $fefo->product_code,
            'cod_origem'  => $fefo->leadtime_code,
            'semanas'     => $fefo->weeks,
            'company_id'  => $fefo->company_id,
            'status_id'   => Status::type(Status::PENDING)->id,
        ]);
    }

    /**
     * Fefo bid statistics
     *
     * @param Fefo         $fefo   Fefo entity
     * @param integer      $limit  Fefo entity
     * @param string|array $status Fefo entity
     *
     * @return array
     */
    public function statistics(Fefo $fefo, $limit, $statusId): array
    {
        return (new BidService)->bidStatistics($fefo->company, [
            'weeks' => $fefo->weeks,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'limit' => $limit,
            'status_id' => $statusId,
        ]);
    }

    /**
     * Approve bids by fefo id
     *
     * @param  Fefo  $fefo   Fefo Entity
     * @param  array $bidIds Array of bid ids
     *
     * @return array
     */
    public function approve(Fefo $fefo, $bidIds, $userId = null): array
    {
        $bids = $this->getFefoBids($fefo);

        if($userId === null) {
            $userId = auth()->user()->id ?? null;
        }

        $isBidCanceled = false;

        $ids = [];

        foreach ($bids as $bid) {
            if (! $bid->can_purchase) {
                continue;
            }

            if (!in_array($bid->id, $bidIds, true)) {
                break;
            }

            $reduceVolumeFefo = array_get($bid->company->settings, 'reduceVolumeFefoApproveBid', false);

            if ($reduceVolumeFefo && !$fefo->decrement('volume', $bid->weight)) {
                break;
            }

            $params = [
                'status_id' => Status::type(Status::APPROVED)->id,
                'id_user' => $userId,
                'nao_lido' => 1,
                'data_aceite' => now()->toDateString(),
                'sanctioned_at' => now(),
            ];

            if ($bid->partial_purchase) {
                $params['total_kg'] = $bid->weight;
                $params['valor'] = $bid->price;
                $params['qtd_caixas'] = $bid->box_amount;

                $bidLogParams = [
                    'user_id' => auth()->user()->id ?? null,
                    'box_amount' => $bid->box_amount,
                    'bid_id' => $bid->id
                ];

                (new BidLogService())->saveBidLog($bidLogParams);
            }

            if (Bid::where('id', $bid->id)->update($params)) {
                $ids[] = $bid->id;
            }

            $createdOrder = $this->createOrderForApprovedBid($fefo, $bid->id);

            $clientCompany = $createdOrder->client->setClientCompany($createdOrder->company);

            $reject = Status::type(Status::REJECTED);

            if ($this->orderService->isValidMinOrder($createdOrder) === false) {
                $this->orderService->cancelOrder(
                    $createdOrder,
                    RejectionMotive::type(RejectionMotive::MIN_ORDER),
                    $reject->id
                );

                event(new OrderCanceledEvent($createdOrder));

                $isBidCanceled = true;
            }

            if ($this->orderService->isValidAccountBallance($createdOrder) === false) {

                $this->orderService->cancelOrder(
                    $createdOrder,
                    RejectionMotive::type(RejectionMotive::CUSTOMER_WITHOUT_BALANCE),
                    $reject->id
                );

                event(new OrderCanceledEvent($createdOrder));

                $isBidCanceled = true;
            }

            $reduceAccountBalance = array_get($bid->company->settings, 'reduceAccountBalanceApproveBid', false);

            if ($reduceAccountBalance) {
                (new ClientService())->decrementAccountBalance(
                    $createdOrder->company,
                    $createdOrder->client,
                    $bid->price
                );
            }

            if($isBidCanceled === false) {
                (new NotificationService)->approvedBid(
                    $bid->client_id,
                    $bid->id,
                    $bid->product->name,
                    $bid->kg_price,
                    calcDiscount($fefo->max_price, $bid->kg_price)
                );

                (new TransactionalEmailsService)->sendBidEmail($bid->fresh());
            }

        }

        return $ids;
    }

    /**
     * @param \App\Fefo\Fefo $fefo  Fefo
     * @param integer        $bidId Bid ID
     * @return mixed
     * @throws \Exception
     */
    public function createOrderForApprovedBid(Fefo $fefo, $bidId)
    {
        $bid = Bid::find($bidId);

        if($bid->data_aceite === null) {
            return;
        }

        $order = [
            'client_id' => $bid->client_id,
            'date' => now()->format('Y-m-d'),
            'bid_id' => $bid->id,
            'status_id' => Status::type(Status::PENDING)->id,
            'partial_accept' => $bid->partial_accept,
            'unread' => 0,
            'company_id' => $fefo->company_id,
            'client_company_payment_id' => $bid->client_company_payment_id,
            'client_card_id' => $bid->client_card_id,
            'cc_months' => $bid->cc_months,
            'tax_percent' => $bid->tax_percent,
            'order_product' => [
                [
                    'product_id' => $bid->product_code,
                    'origin_code' => $bid->origin_code,
                    'weeks' => $bid->weeks,
                    'qtd_caixas' => $bid->box_amount,
                    'original_box_amount' => $bid->box_amount,
                    'kg_total' => $bid->weight,
                    'kg_price' => $bid->kg_price,
                    'price' => $bid->price,
                    'status_id' => Status::type(Status::PENDING)->id,
                    'cd_empresa' => $fefo->cd_empresa,
                    'cd_faixa_fefo' => $fefo->cd_faixa_fefo,
                    'ds_chave_seara' => $fefo->ds_chave_seara,
                ]
            ]
        ];

        $company = Company::find($fefo->company_id);

        return (new OrderService())->createOrder($order, $company);
    }

    /**
     * Disapprove bids by fefo id
     *
     * @param  Fefo  $fefo   Fefo Entity
     * @param  array $bidIds Array of bid ids
     *
     * @return bool
     */
    public function disapprove(Fefo $fefo, $bidIds, $motiveId = null, $userId = null): array
    {
        $bids = $this->getFefoBids($fefo)->reverse();

        if($userId === null) {
            $userId = auth()->user()->id ?? null;
        }

        if($motiveId === null) {
            $motiveId = RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id;
        }

        $ids = [];

        foreach ($bids as $bid) {
            if (!in_array($bid->id, $bidIds)) {
                break;
            }
            $params = [
                'status_id' => Status::type(Status::CANCELED)->id,
                'rejection_motives_id' => $motiveId,
                'id_user' => $userId,
                'nao_lido' => 1,
                'data_aceite' => now()->toDateString(),
                'sanctioned_at' => now(),
            ];

            $update = Bid::where('id', $bid->id)->update($params);

            if ($update) {
                $ids[] = $bid->id;

                (new NotificationService())->rejectedBid(
                    $bid->client_id,
                    $bid->id,
                    $bid->product->name,
                    $bid->kg_price
                );

                (new TransactionalEmailsService)->sendBidEmail($bid->fresh());
            }
        }

        return $ids;
    }
}
