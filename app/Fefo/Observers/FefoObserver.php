<?php

namespace App\Fefo\Observers;

use App\Bid\Services\BidService;
use App\Fefo\Fefo;

class FefoObserver
{
    /**
     * Handle the fefo "created" event.
     *
     * @param  Fefo  $fefo
     * @return void
     */
    public function created(Fefo $fefo)
    {
        //
    }

    /**
     * Handle the fefo "updated" event.
     *
     * @param  Fefo  $fefo
     * @return void
     */
    public function updated(Fefo $fefo)
    {
        (new BidService())->approveBidsForFefo($fefo);
    }

    /**
     * Handle the fefo "deleted" event.
     *
     * @param  Fefo  $fefo
     * @return void
     */
    public function deleted(Fefo $fefo)
    {
        //
    }

    /**
     * Handle the fefo "restored" event.
     *
     * @param  Fefo  $fefo
     * @return void
     */
    public function restored(Fefo $fefo)
    {
        //
    }

    /**
     * Handle the fefo "force deleted" event.
     *
     * @param  Fefo  $fefo
     * @return void
     */
    public function forceDeleted(Fefo $fefo)
    {
        //
    }
}
