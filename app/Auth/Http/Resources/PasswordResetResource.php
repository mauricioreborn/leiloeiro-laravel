<?php

namespace App\Auth\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PasswordResetResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'password_reset',
            'attributes' => [
                'email' => $this['email'],
            ],
        ];
    }
}
