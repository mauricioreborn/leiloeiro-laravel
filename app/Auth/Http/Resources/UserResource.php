<?php

namespace App\Auth\Http\Resources;

use App\Company\Http\Resources\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param request $request \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'user',
            'id'         => $this->id,
            'attributes' => [
                'name'  => $this->name,
                'email' => $this->email,
                'root' => $this->root,
                'price' => $this->price,
                'featured' => $this->featured,
                'company_id' => $this->company_id,
                'deleted_at' => $this->deleted_at,
            ],
            'relationships' => [
                'companies' => new CompanyResource($this->whenLoaded('company')),
            ],
        ];
    }
}
