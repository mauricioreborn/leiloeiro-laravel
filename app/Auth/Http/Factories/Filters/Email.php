<?php

namespace App\Auth\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Sofa\Eloquence\Query\Builder;

class Email implements FilterInterface
{

    /**
     * @param Builder $query QueryBuilder instance
     * @param mixed   $value search param
     * @return Builder
     */
    public function run($query, $value)
    {
        return $query->where('email', 'LIKE', "%{$value}%");
    }
}
