<?php

namespace App\Auth\Http\Controllers;

use Illuminate\Auth\AuthenticationException;
use App\Auth\User;
use App\Auth\Services\AuthService;
use App\Auth\Http\Requests\LoginRequest;
use App\Auth\Http\Requests\UpdateUserRequest;
use App\Core\Http\Controllers\Controller;
use App\Auth\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    protected $user;
    protected $authService;

    /**
     * AuthController constructor.
     * @param user        $user        User instance
     * @param authService $authService AuthService instance
     */
    public function __construct(User $user, AuthService $authService)
    {
        $this->user = $user;
        $this->authService = $authService;
    }

    /**
     * Authenticates a user.
     * @param request $request LoginRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *   path="/api/v1/login",
     *   tags={"Auth"},
     *   summary="Authenticates a user",
     *   description="Method for authenticating a user in the api",
     *   operationId="login",
     *   @SWG\Parameter(
     *         name="username",
     *         in="formData",
     *         description="User's email",
     *         required=true,
     *         type="string"
     *   ),
     *   @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="User's password",
     *         required=true,
     *         type="string"
     *   ),
     *   @SWG\Response(
     *          response=200,
     *          description="The user exists and was authenticated",
     *          ref="$/responses/Json",
     *          @SWG\Schema(
     *              type="string"
     *          )
     *    ),
     *   @SWG\Response(
     *          response=422,
     *          description="Validator response",
     *          ref="$/responses/Json",
     *          @SWG\Schema(
     *              type="string"
     *          )
     *    ),
     *   @SWG\Response(response=401, description="Unauthorized."),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     * @throws AuthenticationException
     */
    public function login(LoginRequest $request)
    {
        return response()->json($this->authService->loginUser($request->username, $request->password), 200);
    }

    /**
     * Verifies whether a user is authenticated and has a valid token.
     *
     * @return UserResource|JsonResponse
     *
     * @SWG\Get(
     *   path="/api/v1/me",
     *   tags={"Auth"},
     *   summary="Validates a token",
     *   description="Method for validating a token in the api",
     *   operationId="me",
     *  @SWG\Response(
     *          response=200,
     *          description="The user exists and was authenticated",
     *          ref="$/responses/Json",
     *          @SWG\Schema(
     *              type="string"
     *          )
     *   ),
     *  @SWG\Response(
     *          response=204,
     *          description="The user exists and was authenticated",
     *          ref="$/responses/Json",
     *          @SWG\Schema(
     *              type="string"
     *          )
     *   ),
     *   @SWG\Response(response=401, description="User not authenticated"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   security={{"authorization":{"write:all", "read:all"}}}
     * )
     */
    public function check()
    {
        if(request()->method() === 'HEAD') {
            return response()->json('user has been logged', 204);
        }

        return new UserResource(auth()->user());
    }

    public function updateUser(UpdateUserRequest $request) : UserResource
    {
        $user = $request->user();
        return new UserResource( $this->authService->updateUser($user,$request->all()) );
    }
}
