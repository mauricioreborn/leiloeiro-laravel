<?php

namespace App\Auth\Http\Controllers;

use App\Auth\Http\Requests\ResetPasswordRequest;
use App\Auth\Http\Requests\UpdatePasswordRequest;
use App\Auth\Http\Requests\VerifyTokenRequest;
use App\Auth\Http\Resources\PasswordResetResource;
use App\Auth\Services\PasswordResetService;
use App\Core\Http\Controllers\Controller;
use Illuminate\Http\Response;

class PasswordResetController extends Controller
{
    protected $passwordResetService;

    /**
     * PasswordResetController constructor.
     * @param passwordResetService $passwordResetService
     */
    public function __construct(PasswordResetService $passwordResetService)
    {
        $this->passwordResetService = $passwordResetService;
    }

    public function reset(ResetPasswordRequest $request)
    {
        $this->passwordResetService->reset($request->email);

        return new Response([], 204);
    }

    public function verifyToken(VerifyTokenRequest $request)
    {
        return new PasswordResetResource($this->passwordResetService->verifyToken($request->token));
    }

    public function update(UpdatePasswordRequest $request)
    {
        $this->passwordResetService
            ->changePassword($request->token, $request->email, $request->password);

        return new Response([], 204);
    }
}
