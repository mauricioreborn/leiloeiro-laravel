<?php


namespace App\Auth\Http\Controllers;

use App\Auth\Http\Resources\UserListResource;
use App\Auth\User;
use App\Core\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Auth\Services\UserService;
use App\Auth\Http\Resources\UserResource;
use App\Auth\Http\Requests\UserRequest;
use App\Company\Company;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class UserController extends Controller
{
    protected $userService;

    /**
     * @param UserService $userService UserService instance
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Lists all users
     * @param Request $request request instance
     * @return AnonymousResourceCollection
     */
    public function list(Request $request): AnonymousResourceCollection
    {
        $this->authorize('isRoot', User::class);
        $collection = $this->userService->search($request->all(), auth()->user()->company);
        return UserResource::collection($collection);
    }

    /**
     * Creates a new user
     * @param UserRequest $request request instance
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $response = new UserResource(
            $this->userService->createUser($request->all(), auth()->user()->company)
        );

        return response([
            'data' => $response,
            'message' => __('users.created'),
        ], 201);
    }

    /**
     * Returns the user with the given ID
     * @param User $user used to search a user
     * @return UserResource
     */
    public function show(User $user)
    {
        $this->authorize('isRoot', User::class);
        $this->authorize('sameCompany', $user);
        return new UserResource($user);
    }

    /**
     * Updates a user
     * @param User        $user    user instance
     * @param UserRequest $request form body
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(User $user, UserRequest $request)
    {
        $this->userService->update($user, $request->all());

        return response([
            'data' => new UserResource($this->userService->find($user->id)),
            'message' => __('users.updated'),
        ]);
    }

    /**
     * Removes a user
     * @param User $user user instance
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user): JsonResponse
    {
        $this->authorize('isRoot', User::class);
        $this->authorize('sameCompany', $user);
        return response()->json($this->userService->delete($user), 204);
    }

    public function restore($id)
    {
        $user = $this->userService->findWithTrashed($id);

        if(!$user) {
            throw new ResourceNotFoundException(__('users.notFound'));
        }

        $this->authorize('isRoot', User::class);
        $this->authorize('sameCompany', $user);

        return new UserResource($this->userService->restore($user));
    }

    public function listById()
    {
        return $this->userService->listById(auth()->user()->company);
    }
}
