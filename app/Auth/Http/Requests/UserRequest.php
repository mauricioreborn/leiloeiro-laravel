<?php

namespace App\Auth\Http\Requests;

use App\Auth\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        $isRoot = $this->user()->can('isRoot', User::class);

        if($isRoot && $this->user !== null) {
            return $this->user()->can('sameCompany', $this->user);
        }

        return $isRoot;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $userId = $this->user ? $this->user->id : null;

        return [
            'name' => 'required',
            'email' => "required|email|unique:users,email,{$userId}",
            'password' => 'required|confirmed',
            'root' => 'required|boolean',
            'price' => 'required|boolean',
            'featured' => 'required|boolean',
        ];
    }
}
