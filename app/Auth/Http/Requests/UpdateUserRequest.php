<?php

namespace App\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to cast confirm password
     *
     * @return Request
     */
    protected function prepareForValidation()
    {
        if (isset($this->password_confirmation)) {
            return $this->replace(array_merge(
                $this->all(),
                [
                    'confirm_password' => $this->password_confirmation
                ]
            ));
        }
    }

    /**
     * Validation Rules
     * @param Request $request Request
     * @return array
     **/
    public function rules(Request $request)
    {
        $user = $request->user();

        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'string',
            'confirm_password' => 'required_with:password|same:password'
        ];
    }
}
