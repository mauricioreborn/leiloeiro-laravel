<?php

namespace App\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginClientRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'cnpj'     => 'required',
            'password' => 'required',
        ];
    }
}
