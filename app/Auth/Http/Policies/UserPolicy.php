<?php

namespace App\Auth\Http\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Auth\User;
use App\Products\Product;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Authorize user to access route
     * @param User $loggedUser Logged user
     * @param User $user       User from request
     * @return boolean
     */
    public function sameCompany(User $loggedUser, User $user)
    {
        return (bool) ($loggedUser->company_id === $user->company_id);
    }

    /**
     * Authorize user with root access
     * @param User $user Logged user
     * @return boolean
     */
    public function isRoot(User $user)
    {
        return (bool) $user->root;
    }

    /**
     * Authorize user with can_refund access
     * @param User $user Logged user
     * @return boolean
     */
    public function canRefund(User $user)
    {
        return (bool) $user->can_refund;
    }
}
