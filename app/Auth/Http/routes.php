<?php

Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', 'AuthController@login');

    Route::group(['prefix' => 'password'], function () {
        Route::put('/', 'PasswordResetController@update');
        Route::post('/reset', 'PasswordResetController@reset');
        Route::post('/verify-token', 'PasswordResetController@verifyToken');
    });

    Route::group(['middleware' => ['validateJwt', 'sentry']], function () {
        Route::get('/me', 'AuthController@check');

        Route::group(['prefix' => 'user'], function () {
            Route::put('update', 'AuthController@updateUser');
        });
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'UserController@list');
            Route::post('/', 'UserController@store');
            Route::get('/list', 'UserController@listById');
            Route::get('/{user}', 'UserController@show');
            Route::put('/{user}', 'UserController@update');
            Route::delete('/{user}', 'UserController@destroy');
            Route::post('/{id}/restore', 'UserController@restore');
        });
    });
});
