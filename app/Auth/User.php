<?php

namespace App\Auth;

use App\Company\Company;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Products\Product;
use Sofa\Eloquence\Eloquence;

class User extends Authenticatable
{
    use Eloquence, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'company_id',
        'root',
        'price',
        'featured',
        'can_refund'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'date' => ['created_at', 'updated_at', 'deleted_at']
    ];

    /**
     * Relationships
     **/

    /**
     * Company Relationship
     * @return Company
     **/
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'produto_user', 'user_id', 'produto_id');
    }

    /**
     * Acessors
     **/

     /**
      * Password Acessor
      * @param value $value Password value
      * @return md5 password
      **/
    public function getPasswordAttribute($value)
    {
        return md5($value);
    }

    /**
     * Mutators
     **/

    /**
     * Password Mutator
     * @param value $value Password password
     * @return void
     **/
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = md5($value);
    }

    /**
     * @param Builder $builder   Query builder
     * @param integer $companyId Company id
     * @return mixed
     */
    public function scopeFromCompany($builder, $companyId)
    {
        return $builder->where('company_id', $companyId);
    }
}
