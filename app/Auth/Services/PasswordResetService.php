<?php

namespace App\Auth\Services;

use App\Auth\User;
use App\Client\Notifications\PasswordResetNotification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PasswordResetService
{
    public function reset(string $email): void
    {
        $user = (new UserService)->findBy('email', $email);

        if($user === null) {
            // phpcs:ignore
            Log::info("Houve uma tentativa de recuperação de senha para um usuário inexistente utilizando o email: {$email}");
            return;
        }

        $this->resetUserPassword($user);

        $user->notify(new PasswordResetNotification);
    }

    protected function resetUserPassword(User $user):void
    {
        $user->password = Hash::make($user->password);
        $user->reset_token = Str::random(60);

        $user->save();
    }

    public function verifyToken($token)
    {
        $user = (new UserService)->findBy('reset_token', $token);

        if($user === null) {
            throw new ModelNotFoundException(__('auth.invalid_password_reset_token'));
        }

        return $user->only('email');
    }

    public function changePassword($token, $email, $password)
    {
        $user = (new UserService)->findBy('reset_token', $token);

        if($user === null || $user->email !== $email) {
            throw new ModelNotFoundException(__('auth.invalid_password_reset_token'));
        }

        $user->password = $password;
        $user->reset_token = null;

        $user->save();
    }
}
