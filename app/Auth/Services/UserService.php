<?php

namespace App\Auth\Services;

use App\Auth\User;
use App\Core\Classes\CrudService;
use App\Company\Company;
use Illuminate\Database\Eloquent\Model;

class UserService extends CrudService
{
    protected $entity = User::class;

    protected $module = 'Auth';

    /**
     * Creates a new user within the given company
     * @param array   $form    client fields
     * @param Company $company company
     * @return Model
     */
    public function createUser($form, Company $company): Model
    {
        return $this->create(array_merge($form, ['company_id' => $company->id]));
    }

    public function findWithTrashed($id)
    {
        return $this->newQuery()->withTrashed()->find($id);
    }

    public function listById(Company $company)
    {
        return $this->newQuery()
            ->where('company_id', $company->id)
            ->pluck('name', 'id');
    }
}
