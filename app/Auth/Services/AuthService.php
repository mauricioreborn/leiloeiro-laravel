<?php

namespace App\Auth\Services;

use App\Auth\User;
use App\Client\Client;
use App\Client\IndependentClient;
use App\Company\Company;
use App\Core\Helpers\JWT;
use App\Core\Http\Controllers\Controller;
use App\Log\LogEmail;
use App\Mobile\Mail\NewClient;
use App\Mobile\Mail\NewIndependentClient;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Mail;
use App\Mobile\Services\MobileService;

class AuthService
{
    /**
     * Client Login
     * @param cnpj     $cnpj     Client CNPJ
     * @param password $password Password
     * @return LoginResource
     */
    public function loginClient($cnpj, $password)
    {
        $clientModel = new Client();
        $mobileService = new MobileService();

        $client = $clientModel->where(
            [
                'cnpj' => $cnpj, 'password' => md5($password)
            ]
        )->with(['seller', 'companies'])->first();

        if ($client === null) {
            throw new AuthenticationException;
        }

        $companyAmount = $client->companies->count();

        $client = $mobileService->checkToken($client->id);

        $expiration = Carbon::now()->addDays(60);
        $token = JWT::encodeClient($client->id, $expiration, $companyAmount);

        return [
            'client' => $client, 'authorization_token' => $token, 'expires_at' => $expiration,
        ];
    }

    /**
     * Method to login an client
     * @param email    $email    email
     * @param password $password Password
     * @return mixed
     * @throws AuthenticationException
     */
    public function loginUser($email, $password)
    {
        $user = User::where('email', $email)->where('password', md5($password))->first();
        if (!isset($user->id)) {
            throw new AuthenticationException;
        }
        $expiration = Carbon::now()->addDay();
        $token = JWT::encodeUser($user->id, $expiration);

        $company = new Company();
        $companyData = $user->company->toArray();

        $return['data'] = [
            'authorization_token' => $token,
            'expires_at'          => $expiration,
            'company' => $companyData

        ];

        return $return;
    }

    /**
     * Method to register an client
     * @param registerData $registerData registerData
     * @return array
     */
    public function registerClient(array $registerData)
    {
        $clientModel = new Client();
        $status = false;

        $telefone = isset($registerData['telefone']) ? $registerData['telefone'] : null;
        $clientData = false;
        $cnpj = isset($registerData['cnpj']) ? $registerData['cnpj'] : false;

        $errors = [];
        if ($cnpj !== false) {
            $cnpj = $this->formataCnpj($cnpj);
            $where_user = [
                'cnpj' => $cnpj,
            ];
            $clientData = $clientModel->where($where_user)->first();
        }

        $independentClient = IndependentClient::where([
            'cnpj' => $cnpj,
            'integrado' => 0
        ])->first();

        if (isset($independentClient->id)) {
            $status = true;
            $errors[] = __('Mobile/client.independent_client_cnpj_exist');
        }

        if ($clientData) {
            if ($clientData->email_verified == 1) {
                $errors[] = "O CNPJ já consta em nossa base, faça o login.";
            } elseif (empty($errors)) {
                $new_password = generatePassword(rand(12, 100));
                $update = [
                    "name" => $registerData['nome'],
                    "email" => $registerData['email'],
                    "telefone" => $telefone,
                    "password" => $new_password,
                ];


                if ($clientData->update($update)) {
                    $messages = array();
                    $messages[] = "Cadastro realizado com sucesso!";
                    $params = [
                        'new_password' => $new_password,
                        'email' => $update['email'],
                        'to' => $update['email'],
                        'cnpj' => $cnpj,
                        'name' => $update['name'],
                    ];

                    $ClientEmail = new NewClient($params);
                    $ClientEmail->build();
                    Mail::send($ClientEmail);

                    (new LogEmail())->create([
                        'page' => "Cadastro de Cliente",
                        'subject' => "Contato Souk | Cadastro realizado",
                        'message' => $ClientEmail->render(),
                        "emails" =>  $clientData->email
                    ]);

                    return [
                        'status' => true,
                        'message' => __('auth.client.register_with_success')
                    ];
                }
            }
        }

        if (!empty($errors)) {
           return [
               'status' => $status,
               'message' => $errors
           ];
        }

        $new_password = generatePassword(rand(12, 100));

        $insert = [
            "nome" => $registerData['nome'],
            "email" => $registerData['email'],
            "telefone" => $telefone,
            "password" => md5($new_password),
            "cnpj" => $cnpj
        ];

        $insert = $this->addAdditionalParams($insert, $registerData);

        $independentClient = new IndependentClient();
        $independentClient->create($insert);
        $messages = __('auth.independent_client.register_with_success');

        $params = [
            'new_password' => $new_password,
            'email' => $insert['email'],
            'to' => $insert['email'],
            'cnpj' => $insert['cnpj'],
            'name' => $insert['nome'],
        ];

        $independentEmail = new NewIndependentClient($params);
        $independentEmail->build();
        Mail::send($independentEmail);

        (new LogEmail())->create([
            'page' => "Cadastro de Cliente",
            'subject' => "Contato Souk | Cadastro realizado",
            'message' => $independentEmail->render(),
            "emails" =>  $insert['email']
        ]);

        return [
            'status' => true,
            'message' => $messages,
            'msg' => $messages
        ];
    }

    /**
     * Update user informations
     * @param User  user           $user           User instance
     * @param array fieldsToUpdate $fieldsToUpdate [name, email, password]
     * @return User $user
     **/
    public function updateUser(User $user, array $fieldsToUpdate)
    {
        $user->update($fieldsToUpdate);

        return $user;
    }

    /**
     * Method to format CNPJ
     * @param str $str CNPJ
     * @return string
     */
    public function formataCnpj($str)
    {
        $str = preg_replace('/\D/', '', $str);
        $str = str_pad(preg_replace('[^0-9]', '', $str), 14, '0', STR_PAD_LEFT);

        return $str;
    }

    /**
     * Method to check additional parameters
     *
     * @param array $independentClient independent client params
     * @param array $request           request with sent data
     * @return array with independent clients params
     */
    public function addAdditionalParams(array $independentClient, array $registerData)
    {

        if (isset($registerData['address_complement'])) {
            $independentClient['address_complement'] = $registerData['address_complement'];
        }
        
        if (isset($registerData['address'])) {
            $independentClient['endereco'] = $registerData['address'];
        }

        if (isset($registerData['state'])) {
            $independentClient['uf'] = $registerData['state'];
        }

        if (isset($registerData['zipcode'])) {
            $independentClient['zipcode'] = $registerData['zipcode'];
        }

        if (isset($registerData['city'])) {
            $independentClient['municipio'] = $registerData['city'];
        }

        if (isset($registerData['address_number'])) {
            $independentClient['address_number'] = $registerData['address_number'];
        }

        if (isset($registerData['neighborhood'])) {
            $independentClient['neighborhood'] = $registerData['neighborhood'];
        }

        if (isset($registerData['razao'])) {
            $independentClient['social_reason'] = $registerData['razao'];
        }

        return $independentClient;
    }
}
