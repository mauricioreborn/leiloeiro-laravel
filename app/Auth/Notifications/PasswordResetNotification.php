<?php

namespace App\Client\Notifications;

use App\Auth\User;
use App\Core\Mail\LoggableMail;
use App\Log\LogEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class PasswordResetNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $email = (new LoggableMail(
            'mail.user_password_reset',
            $this->getViewData($notifiable),
            'Contato Souk | Recuperar Senha'
        )
        )->to($notifiable->email);

        (new LogEmail())->create([
            'page' => 'Recuperar Senha - Painel Web',
            'subject' => 'Contato Souk | Recuperar Senha',
            'message' => $email->build()->render(),
            'emails' =>  $notifiable->email,
            'company_id' => $notifiable->company_id,
        ]);

        Log::info("Email de recuperação de senha enviado para {$notifiable->email}");

        return $email;
    }

    protected function getViewData(User $user)
    {
        return [
            'name' => $user->name,
            'email' => $user->email,
            'url' => config('app.penha_url') . "/alterar-senha?token={$user->reset_token}",
        ];
    }
}
