<?php
namespace App\Payment\Services;

use App\Company\Company;
use App\Payment\MonthlyReport;

class PaymentReportsService
{
    /**
     * Method to get all monthly reports by company_id
     * @param Company $company company object
     * @return mixed
     */
    public function getMonthlyReports(Company $company)
    {
        return MonthlyReport::where('company_id', $company->id)->with('status')->get();
    }
}