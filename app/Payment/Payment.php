<?php

namespace App\Payment;

use App\Company\CompanyPayment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Payment extends Model
{
    use SoftDeletes;

    const CREDIT_CARD = 'credit_card';
    const BILLET = 'billet';
    const COMPANY_CREDIT = 'company_credit';

    protected $fillable = [
        'name',
        'type'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * CompanyPayment Relationship
     * @return Payment
     **/
    public function companyPayment()
    {
        return $this->hasMany(CompanyPayment::class);
    }

    /**
     * Query Scope where Payment is Credit Card
     * @param Builder $query Query builder
     * @return mixed
     **/
    public function scopeIsCreditCard($query)
    {
        return $query->where('type', $this::CREDIT_CARD);
    }

    /**
     * Query Scope where Payment is Credit Card
     * @param Builder $query Query builder
     * @return mixed
     **/
    public function scopeIsCompanyCredit($query)
    {
        return $query->where('type', $this::COMPANY_CREDIT);
    }

    /**
     * get payment is a credit card type
     *
     * @return bool
     */
    public function getIsCreditCardAttribute(): bool
    {
        return $this->type == $this::CREDIT_CARD;
    }

    /**
     * get payment is a company credit type
     *
     * @return bool
     */
    public function getIsCompanyCreditAttribute(): bool
    {
        return $this->type == $this::COMPANY_CREDIT;
    }

    /**
     * get payment is a company credit type
     *
     * @return array
     */
    public function getImageAttribute(): array
    {
        return [
            'default' => env('PRODUCT_IMAGES_BASE_URL').'souk-company/payment/types/'.$this->type.'.svg',
            'png' => env('PRODUCT_IMAGES_BASE_URL').'souk-company/payment/types/'.$this->type.'.png',
            'white' => env('PRODUCT_IMAGES_BASE_URL').'souk-company/payment/types/'.$this->type.'_white.svg'
        ];
    }
}
