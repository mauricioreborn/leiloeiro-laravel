<?php

namespace App\Payment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Core\Entities\Status;
use Illuminate\Support\Facades\Storage;

class MonthlyReport extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'total',
        'month',
        'year',
        'company_id',
        'status_id',
        'url'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Company relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * status relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    /**
     * Method to set storage on url
     * @param string $url url of file
     * @return mixed
     */
    public function getUrlAttribute($url)
    {
        return Storage::temporaryUrl(
            $url, now()->addHour()
        );
    }
}