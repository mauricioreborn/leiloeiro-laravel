<?php

namespace App\Payment\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyPaymentResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'company_payment',
            'id' => $this->id,
            'attributes' => [
                'company_id' => $this->company_id,
                'payment_id' => $this->payment_id,
            ],
            'relationships' => [
                'payments' => new PaymentResource($this->whenLoaded('payment')),
            ],
        ];
    }
}
