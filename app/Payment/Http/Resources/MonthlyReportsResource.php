<?php
namespace App\Payment\Http\Resources;

use App\Core\Http\Resources\StatusResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MonthlyReportsResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'monthly_reports',
            'id' => $this->id,
            'attributes' => [
                'total' => $this->total,
                'month' => $this->month,
                'year' => $this->year,
                'url' => $this->url,
            ],
            'relationships' => [
                'status' => new StatusResource($this->whenLoaded('status')),
            ],
        ];
    }
}
