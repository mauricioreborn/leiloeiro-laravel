<?php

namespace App\Payment\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientCompanyPaymentResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'client_company_payment',
            'id' => $this->id,
            'attributes' => [
                'enable' => $this->enable ?? false,
                'message' => $this->message ?? false,
            ],
            'relationships' => [
                'payments' => $this->when(
                    $this->relationLoaded('companyPayment'),
                    function () {
                        return new PaymentResource($this->companyPayment->payment);
                    }
                ),
            ],
        ];
    }
}
