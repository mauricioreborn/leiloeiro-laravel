<?php
namespace App\Payment\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UpdateClientPaymentResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'payment',
            'attributes' => [
                'attached' => $this['attached'],
                'detached' => $this['detached'],
                'updated' => $this['updated']
            ]
        ];
    }
}