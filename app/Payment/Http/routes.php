<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => ['validateJwt', 'sentry']], function () {
        Route::group(['prefix' => 'payments'], function () {
            Route::get('/', 'PaymentController@list');
            Route::put('client/{client}', 'PaymentController@updateClientPayments');
            Route::get('/reports', 'PaymentReportsController@list');
        });
    });
    Route::group(['prefix' => 'report', 'middleware' => ['validateJwt', 'sentry']], function () {
        Route::post('payments/monthly', 'PaymentController@getMonthlyReports');
    });
});
