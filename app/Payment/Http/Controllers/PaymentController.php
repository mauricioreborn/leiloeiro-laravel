<?php

namespace App\Payment\Http\Controllers;

use App\Client\Client;
use App\Client\Services\ClientCompanyPaymentService;
use App\Company\Services\CompanyPaymentService;
use App\Core\Http\Controllers\Controller;
use App\Jobs\PaymentReportsJob;
use App\Payment\Http\Requests\UpdateClientPaymentRequest;
use App\Payment\Http\Resources\CompanyPaymentResource;
use App\Payment\Http\Resources\UpdateClientPaymentResource;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $companyPaymentService;
    protected $clientPaymentService;

    /**
     * PaymentController constructor.
     * @param CompanyPaymentService       $companyPayment       Service class
     * @param ClientCompanyPaymentService $clientPaymentService Service class
     * @return null
     */
    public function __construct(
        CompanyPaymentService $companyPayment,
        ClientCompanyPaymentService $clientPaymentService
    ) {
        $this->companyPaymentService = $companyPayment;
        $this->clientPaymentService = $clientPaymentService;
    }

    /**
     * Method to list company payments
     * @param Request $request request params
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function list(Request $request)
    {
        return CompanyPaymentResource::collection($this->companyPaymentService->list(auth()->user()->company));
    }

    /**
     * Method to update a company payments on client
     * @param Request $request request parameters
     * @param Client  $client  client object
     * @return array
     */
    public function updateClientPayments(UpdateClientPaymentRequest $request, Client $client)
    {
        return new UpdateClientPaymentResource(
            $this->clientPaymentService->syncCompanyPayments(
                $client,
                auth()->user()->company,
                $request->company_payment_ids
            )
        );
    }

    /**
     * Method to dispatch job to generate a payment reports
     * @param Request $request request params
     * @return array
     */
    public function getMonthlyReports(Request $request)
    {
        PaymentReportsJob::dispatch($request->user()->company_id);
        return [
            'status' => 200,
            'message' => 'report has been scheduled'
        ];
    }
}
