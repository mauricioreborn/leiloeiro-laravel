<?php

namespace App\Payment\Http\Controllers;

use App\Company\Company;
use App\Core\Http\Controllers\Controller;
use App\Payment\Http\Resources\MonthlyReportsResource;
use App\Payment\Services\PaymentReportsService;
use Illuminate\Http\Request;

class PaymentReportsController extends Controller
{
    protected $paymentReportsService;

    /**
     * Construct method of payment report
     * PaymentReportsController constructor.
     * @param PaymentReportsService $paymentReportsService service of controller
     */
    public function __construct(PaymentReportsService $paymentReportsService)
    {
        $this->paymentReportsService = $paymentReportsService;
    }

    /**
     * Method to list company payments
     * @param Request $request request params
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function list(Request $request)
    {
        return MonthlyReportsResource::collection(
            $this->paymentReportsService->getMonthlyReports(Company::find($request->user()->company_id))
        );
    }
}
