<?php

namespace App\Uuid;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait
use Illuminate\Database\Eloquent\SoftDeletes;

class Uuid extends Model
{
    use Eloquence, Mappable;

    protected $table = 'uuid';

    public $timestamps = true;

    protected $fillable = [
        'client_id',
        'uuid',
    ];
}