<?php

namespace App\Boost;

use App\Bid\Bid;
use App\Boost\Relations\BelongsToProductRelation;
use App\Client\Client;
use App\Company\Company;
use App\Core\Entities\Status;
use App\Order\Order;
use Illuminate\Database\Eloquent\Model;

class BoostsPurchasesView extends Model
{
    protected $table = 'vw_boosts_purchases';

    public function client()
    {
        return $this->belongsTo(Client::class, 'cnpj', 'cnpj');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function product()
    {
        return (new BelongsToProductRelation($this))->withDefault();
    }
}