<?php
namespace App\Boost\Events;

use App\Boost\ClientList;
use Illuminate\Queue\SerializesModels;

class CacheClientList
{
    use SerializesModels;

    public $clientList;

    /**
     * ClientListCreated constructor.
     * @param $clientList
     */
    public function __construct(ClientList $clientList)
    {
        $this->clientList = $clientList;
    }
}