<?php
namespace App\Boost\Listeners;

use App\Boost\Events\CacheClientList;
use App\Boost\Services\ClientListCacheService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ClientListSubscriber implements ShouldQueue
{
    use InteractsWithQueue;

    public function refreshCache($event)
    {
        (new ClientListCacheService($event->clientList))->cache();
    }

    public function subscribe($events)
    {
        $events->listen(
            CacheClientList::class,
            'App\Boost\Listeners\ClientListSubscriber@refreshCache'
        );
    }
}