<?php
namespace App\Boost\Classes;

use App\Boost\Boost;
use App\Company\Company;
use App\Core\Contracts\SearchInterface;
use App\Products\Product;
use Carbon\Carbon;
use Sofa\Eloquence\Builder;

class BoostSearch implements SearchInterface
{
    private $query;
    protected $company;

    public function __construct(Company $company, Builder $query, $relations = [])
    {
        $this->query = $query->with($relations);
        $this->company = $company;
    }

    public function run(array $filters)
    {
        foreach($filters as $filter => $value) {
            $methodName = 'applyFilter' . ucfirst(camel_case($filter));
            if(!method_exists($this, $methodName)) {
                continue;
            }

            $this->$methodName($value);
        }

        return $this->query;
    }

    public function applyFilterSort($column)
    {
        $direction = $column[0] === '-' ? 'desc' : 'asc';
        $column = ltrim($column, '-');

        if(!in_array($column, (new Boost)->getFillable(), true)) {
            return;
        }

        $this->query->orderBy($column, $direction);
    }

    public function applyFilterStartedAt($date)
    {
        try {
            $this->query->whereDate('started_at', '=', Carbon::parse($date));
        } catch (\Exception $e) {
            return;
        }
    }

    public function applyFilterFinishedAt($date)
    {
        try {
            $this->query->whereDate('finished_at', '=', Carbon::parse($date));
        } catch (\Exception $e) {
            return;
        }
    }

    public function applyFilterRevenue($value)
    {
        $this->query->where('revenue', 'LIKE', "{$value}%");
    }

    public function applyFilterTitle($value)
    {
        $this->query->where('title', 'LIKE', "%{$value}%");
    }

    public function applyFilterProductName($value)
    {
        $products = Product::fromCompany($this->company->id)->where('name', 'LIKE', "%{$value}%")->pluck('code');
        $this->query->whereIn('product_code', $products);
    }

    public function applyFilterStatusId($value)
    {
        $this->query->where('status_id', $value);
    }

    public function applyFilterWeeks($value)
    {
        $this->query->where('weeks', $value);
    }
}
