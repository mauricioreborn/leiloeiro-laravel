<?php

namespace App\Boost;

use Illuminate\Database\Eloquent\Model;

class ClientListCnpj extends Model
{

    protected $table = 'client_lists_cnpj';

    protected $fillable = [
        'client_list_id',
        'cnpj'
    ];

    public $timestamps = false;

    /**
     * ClientList relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientList()
    {
        return $this->belongsTo(ClientList::class);
    }
}