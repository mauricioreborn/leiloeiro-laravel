<?php


namespace App\Boost\Http\Imports;


class ClientListImport
{
    public function import($file)
    {
        $valid = $invalid = [];
        $duplicate = 0;

        if (($handle = fopen($file, 'r')) !== false) {

            $row = 0;

            while (($dataRow = fgetcsv($handle, 1000, ';')) !== false) {

                $row++;

                if($row == 1){
                    continue;
                }

                if(validCnpj($dataRow[0])){

                    if(isset($valid[$dataRow[0]])){
                        $duplicate++;
                        continue;
                    }

                    $valid[$dataRow[0]] = ['cnpj' => $dataRow[0]];
                }else{
                    $invalid[] = $dataRow[0];
                }
            }
        }

        return [
            'valid' => collect($valid)->unique()->toArray(),
            'invalid' => $invalid,
            'duplicate' => $duplicate,
        ];
    }
}