<?php

namespace App\Boost\Http\Requests;

use App\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class BoostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('isRoot', User::class);
    }

    /**
     * Method to validate params
     * @param Request $request request
     * @return array
     */
    public function rules(Request $request)
    {
        return [];
    }

    /**
     * Method to change messages
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
