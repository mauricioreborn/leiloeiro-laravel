<?php

namespace App\Boost\Http\Requests;

use App\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ClientListUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('isRoot', User::class);
    }

    /**
     * Method to validate paClientListRequestrams
     * @param Request $request request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name' => 'required|string',
            'file' => 'required|mimes:csv,txt|max:5000',
        ];
    }
}