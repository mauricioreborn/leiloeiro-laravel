<?php

namespace App\Boost\Http\Requests;

use App\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ApplyFiltersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can('isRoot', User::class);
    }

    /**
     * Method to validate params
     * @param Request $request request
     * @return array
     */
    public function rules(): array
    {
        return [
            'use_filters' => 'required|in:yes,no',
            'purchase_history' => 'required_unless:use_filters,no|in:bids,orders,bids_and_orders,bids_or_orders',
            'purchases_start' => 'required_unless:use_filters,no|date',
            'purchases_finish' => 'required_unless:use_filters,no|date|after:purchases_start',
            'typology_codes' => 'required_unless:use_filters,no|array|min:1',
            'already_purchased' => 'required_unless:use_filters,no|string|in:yes,no',
            'product_condition' => 'required_unless:use_filters,no|string|in:all,one',
            'product_codes' => 'required_unless:use_filters,no|array|min:1',
        ];
    }
}
