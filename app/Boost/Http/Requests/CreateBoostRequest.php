<?php

namespace App\Boost\Http\Requests;

use App\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use LaravelLegends\PtBrValidator\Validator;

class CreateBoostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('isRoot', User::class);
    }

    /**
     * Method to validate params
     * @param Request $request request
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'title' => 'required',
            'description' => 'required',
            'email' => 'required|boolean',
            'push' => 'required|boolean',
            'sms' => 'required|boolean',
            'whatsapp' => 'required|boolean',
            'started_at' => 'required|date',
            'finished_at' => 'required|date',
            'client_lists' => 'required|array|min:1',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('boosts.validation.name_required'),
            'title.required' => __('boosts.validation.title_required'),
            'description.required' => __('boosts.validation.description_required'),
            'started_at.required' => __('boosts.validation.started_at_required'),
            'finished_at.required' => __('boosts.validation.finished_at_required'),
            'client_lists.required' => __('boosts.validation.client_lists_required'),
            'client_lists.min' => __('boosts.validation.client_lists_min'),
        ];
    }

    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
            if (!$this->validateChannels()) {
                $validator->errors()->add('channel', __('boosts.validation.channel_required'));
            }
        });
    }

    public function validateChannels(): bool
    {
        $email = $this->email;
        $push = $this->push;
        $sms = $this->sms;
        $whatsapp = $this->whatsapp;

        return (bool)$email || (bool)$push || (bool)$sms || (bool)$whatsapp;
    }
}
