<?php

namespace App\Boost\Http\Requests;

use App\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use LaravelLegends\PtBrValidator\Validator;

class FinishBoostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('isRoot', User::class);
    }

    /**
     * Method to validate params
     * @param Request $request request
     * @return array
     */
    public function rules()
    {
        return [
            'draft' => 'required',
            'product_code' => 'required',
            'origins' => 'required|array|min:1',
            'origins.*.price' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'product_code.required' => __('boosts.validation.product_code_required'),
            'origins.required' => __('boosts.validation.origins_required'),
            'origins.min' => __('boosts.validation.origins_min'),
            'origins.*.price.required' => __('boosts.validation.origins_price_required'),
        ];
    }
}
