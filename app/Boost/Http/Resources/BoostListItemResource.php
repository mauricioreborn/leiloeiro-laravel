<?php

namespace App\Boost\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BoostListItemResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'boost',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'description' => $this->description,
                'product_name' => $this->product->name,
                'weeks' => $this->weeks,
                'coverage' => $this->coverage,
                'started_at' => $this->started_at->toDateString(),
                'finished_at' => $this->finished_at->toDateString(),
                'revenue' => $this->revenue,
                'status_id' => $this->status_id,
                'status' => [
                    'label' => __("boosts.status.{$this->status->name}.label"),
                    'bg_class' => __("boosts.status.{$this->status->name}.bg_class"),
                    'text_class' => __("boosts.status.{$this->status->name}.text_class"),
                ],
            ],
        ];
    }
}
