<?php


namespace App\Boost\Http\Resources;

use App\Auth\Http\Resources\UserResource;
use App\Mobile\Http\Resources\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientListResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => 'clientList',
            'attributes' => [
                'name' => $this->name,
                'rows' => $this->rows,
                'imported_rows' => $this->imported_rows,
                'invalid_rows' => $this->invalid_rows,
                'duplicate_rows' => $this->duplicate_rows,
                'user_id' => $this->user_id,
                'user_name' => $this->user->name,
                'company_id' => $this->company_id,
                'created_at' => $this->created_at->toDateString(),
            ],
        ];
    }
}