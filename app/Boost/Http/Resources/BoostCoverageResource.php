<?php

namespace App\Boost\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


/**
 * @property mixed coverage
 * @property mixed disqualified
 * @property mixed ruled_out
 */
class BoostCoverageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'boosts_coverage',
            'attributes' => [
                'coverage' => $this->coverage,
                'disqualified' => $this->disqualified,
                'ruled_out' => $this->ruled_out,
            ],
        ];
    }
}
