<?php

namespace App\Boost\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed applied_filters
 * @property mixed revenue
 * @property mixed ruled_out
 * @property mixed disqualified
 * @property mixed conversion
 * @property mixed reach
 * @property mixed coverage
 * @property mixed weeks
 * @property mixed product_code
 * @property mixed clientLists
 * @property mixed status
 * @property mixed status_id
 * @property mixed finished_at
 * @property mixed started_at
 * @property mixed whatsapp
 * @property mixed push
 * @property mixed email
 * @property mixed description
 * @property mixed title
 * @property mixed name
 * @property mixed id
 */
class BoostResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'boost',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'title' => $this->title,
                'description' => $this->description,
                'email' => (bool) $this->email,
                'push' => (bool) $this->push,
                'sms' => (bool) $this->push,
                'whatsapp' => (bool) $this->whatsapp,
                'started_at' => $this->started_at->toDateString(),
                'finished_at' => $this->finished_at->toDateString(),
                'status_id' => $this->status_id,
                'status' => [
                    'label' => __("boosts.status.{$this->status->name}.label"),
                    'bg_class' => __("boosts.status.{$this->status->name}.bg_class"),
                    'text_class' => __("boosts.status.{$this->status->name}.text_class"),
                ],
                'client_lists' => $this->whenLoaded('clientLists', $this->clientLists),
                'product_code' => $this->when($this->product_code !== null, $this->product_code),
                'product' => $this->whenLoaded('product'),
                'weeks' => $this->when($this->weeks !== null, $this->weeks),
                'coverage' => $this->when($this->coverage !== null, $this->coverage),
                'reach' => $this->when($this->reach !== null, $this->reach),
                'conversion' => $this->when($this->conversion !== null, $this->conversion),
                'disqualified' => $this->when($this->disqualified !== null, $this->disqualified),
                'ruled_out' => $this->when($this->ruled_out !== null, $this->ruled_out),
                'revenue' => $this->when($this->revenue !== null, $this->revenue),
                'applied_filters' => $this->when($this->applied_filters !== null, $this->applied_filters),
                'origins' => $this->whenLoaded('origins'),
            ],
        ];
    }
}
