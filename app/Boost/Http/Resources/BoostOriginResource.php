<?php

namespace App\Boost\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BoostOriginResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'boost_origins',
            'attributes' => [
                'origin_code' => $this['origin_code'],
                'origin_name' => $this['origin_name'],
                'selling_price' => $this['selling_price'],
                'clients' => $this['clients'],
            ],
        ];
    }
}
