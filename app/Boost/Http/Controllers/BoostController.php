<?php

namespace App\Boost\Http\Controllers;

use App\Auth\User;
use App\Boost\Http\Requests\ApplyFiltersRequest;
use App\Boost\Http\Requests\CreateBoostRequest;
use App\Boost\Http\Resources\BoostCoverageResource;
use App\Boost\Http\Resources\BoostOriginResource;
use App\Boost\Http\Requests\FinishBoostRequest;
use App\Boost\Http\Resources\BoostResource;
use App\Boost\Services\BoostService;
use App\Boost\Http\Resources\BoostListItemResource;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BoostController
{
    use AuthorizesRequests;

    protected $boostService;

    public function __construct(BoostService $boostService)
    {
        $this->boostService = $boostService;
    }

    public function lists(Request $request)
    {
        $this->authorize('isRoot', User::class);

        $collection = $this->boostService->search($request->all(), auth()->user()->company);
        return BoostListItemResource::collection($collection);
    }

    public function create(CreateBoostRequest $request)
    {
        $payload = $request->all();
        $payload['company_id'] = $request->user()->company_id;
        $payload['started_at'] = Carbon::parse($request->started_at);
        $payload['finished_at'] = Carbon::parse($request->finished_at);

        $newBoost = $this->boostService->create($payload);
        return new BoostResource($newBoost);
    }

    public function update(CreateBoostRequest $request, $id)
    {
        $payload = $request->all();
        $payload['company_id'] = $request->user()->company_id;
        $payload['started_at'] = Carbon::parse($request->started_at);
        $payload['finished_at'] = Carbon::parse($request->finished_at);

        $this->boostService->update($id, $payload);
        return new BoostResource($this->boostService->find($id, ['clientLists', 'product', 'status']));
    }

    public function show($id)
    {
        $this->authorize('isRoot', User::class);

        return new BoostResource(
            $this->boostService->find($id, ['clientLists', 'product', 'status', 'origins.origin'])
        );
    }

    public function applyFilters(ApplyFiltersRequest $request, $id)
    {
        $boost = $this->boostService->find($id, ['clientLists', 'product', 'status']);
        return new BoostResource($this->boostService->applyFilters($boost, $request->all()));
    }

    public function coverage(Request $request, $id)
    {
        $this->authorize('isRoot', User::class);

        $boost = $this->boostService->find($id, ['clientLists']);
        return new BoostCoverageResource($this->boostService->calcCoverage($boost, $request->all()));
    }

    public function products($id)
    {
        $this->authorize('isRoot', User::class);

        $boost = $this->boostService->find($id);

        return new Response([
            'data' => [
                'type' => 'boost_products',
                'list' => $this->boostService->getProductList($boost),
            ],
        ]);
    }

    public function origins(Request $request, $id)
    {
        $this->authorize('isRoot', User::class);

        $boost = $this->boostService->find($id);

        return BoostOriginResource::collection(
            $this->boostService->getOriginList($boost, $request->product_code, $request->weeks)
        );
    }

    public function finish(FinishBoostRequest $request, $id)
    {
        $boost = $this->boostService->find($id);
        return new BoostResource($this->boostService->finish($boost, $request->all()));
    }
}
