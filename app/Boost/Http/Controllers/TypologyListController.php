<?php

namespace App\Boost\Http\Controllers;

use App\Boost\Services\TypologyListService;
use Illuminate\Http\Response;

class TypologyListController
{
    public function __invoke()
    {
        return new Response([
            'data' => [
                'type' => 'typology_list',
                'items' => (new TypologyListService())->list(auth()->user()->company),
            ],
        ]);
    }
}
