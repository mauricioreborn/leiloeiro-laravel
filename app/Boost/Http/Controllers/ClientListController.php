<?php

namespace App\Boost\Http\Controllers;

use App\Auth\User;
use App\Boost\Http\Requests\ClientListUploadRequest;
use App\Boost\Services\ClientListService;
use App\Boost\Http\Resources\ClientListResource;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ClientListController
{
    use AuthorizesRequests;

    protected $clientListService;

    /**
     * ClientListController constructor.
     * @param ClientListService $clientListService
     */
    public function __construct(ClientListService $clientListService)
    {
        $this->clientListService = $clientListService;
    }

    public function lists(Request $request)
    {
        $this->authorize('isRoot', User::class);

        $collection = $this->clientListService->list(auth()->user()->company, $request->all());
        return ClientListResource::collection($collection);
    }

    public function upload(ClientListUploadRequest $request)
    {
        $collection  = $this->clientListService->uploadList($request->all(), $request->user());
        return new ClientListResource($collection);
    }

    public function remove($id)
    {
        $this->authorize('isRoot', User::class);

        $this->clientListService->delete($id);
        return new JsonResponse([], 204);
    }
}
