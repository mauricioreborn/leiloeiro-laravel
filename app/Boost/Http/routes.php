<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'client_lists'], function () {
        Route::post('/', 'ClientListController@upload');
        Route::get('/', 'ClientListController@lists');
        Route::delete('/{id}', 'ClientListController@remove');
    });

    Route::group(['prefix' => 'boosts'], function () {
        Route::get('/', 'BoostController@lists');
        Route::post('/', 'BoostController@create');
        Route::put('/{id}/filters', 'BoostController@applyFilters');
        Route::get('/{id}', 'BoostController@show');
        Route::put('/{id}', 'BoostController@update');
        Route::get('/{id}/coverage', 'BoostController@coverage');
        Route::get('/{id}/products', 'BoostController@products');
        Route::get('/{id}/origins', 'BoostController@origins');
        Route::put('/{id}/finish', 'BoostController@finish');
    });

    Route::get('/typologies/list', 'TypologyListController');
});
