<?php
namespace App\Boost\Relations;

use App\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BelongsToProductRelation extends BelongsTo
{

    public function __construct(Model $parent)
    {
        parent::__construct(
            Product::query(),
            $parent,
            'product_code',
            'codigo',
            'product'
        );
    }

    public function addConstraints()
    {
        parent::addConstraints();

        $this->query->withTrashed();
    }

    public function addEagerConstraints(array $models)
    {
        if(count($models) > 0) {
            $this->query->fromCompany($models[0]->company_id);
        }

        parent::addEagerConstraints($models);
    }

}