<?php

namespace App\Boost;

use App\LeadTime\LeadTime;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class BoostOrigin extends Model
{
    use Eloquence;

    protected $fillable = [
        'boost_id',
        'origin_code',
        'price'
    ];

    public $timestamps = false;

    /**
     * Boost relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function boost()
    {
        return $this->belongsTo(Boost::class);
    }

    public function origin()
    {
        return $this->hasOne(LeadTime::class, 'cod_origem', 'origin_code')
            ->groupBy('cod_origem');
    }
}