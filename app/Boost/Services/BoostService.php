<?php

namespace App\Boost\Services;

use App\Boost\Boost;
use App\Boost\BoostsPurchasesView;
use App\Boost\BoostOrigin;
use App\Boost\Classes\BoostSearch;
use App\Company\Company;
use App\Core\Classes\CrudService;
use App\Core\Entities\Status;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Illuminate\Database\Eloquent\Model;
use App\LeadTime\Services\LeadTimeService;
use Illuminate\Support\Facades\DB;

class BoostService extends CrudService
{
    protected $entity = Boost::class;

    protected $module = 'Boost';

    protected $leadTimeService;

    public function __construct()
    {
        $this->leadTimeService = new LeadTimeService();
    }

    public function search($filters, Company $company = null)
    {
        return (new BoostSearch($company, $this->newQuery()->fromCompany($company->id), ['product', 'status']))
            ->run($filters)
            ->paginate($filters['limit'] ?? 15);
    }

    public function create(array $data = []): Model
    {
        $data['status_id'] = Status::type(Status::DRAFT)->id;
        $newBoost = parent::create($data);

        $newBoost->clientLists()->sync($data['client_lists']);

        return $newBoost->load('clientLists', 'product');
    }

    public function update($boost, array $data = []): bool
    {
        $boost = parent::find($boost);

        $data['status_id'] = Status::type(Status::DRAFT)->id;
        $boost->clientLists()->sync($data['client_lists']);

        return parent::update($boost, $data);
    }

    public function applyFilters(Boost $boost, $filters = [])
    {
        $coverage = (new BoostCoverageService($boost, $filters))
            ->validateFilters()
            ->setActiveClients()
            ->calc()
            ->stats();

        $boost->disqualified = $coverage['disqualified'];
        $boost->coverage = $coverage['coverage'];
        $boost->ruled_out = $coverage['ruled_out'];
        $boost->applied_filters = $this->prepareFilters($boost, $filters);

        $boost->save();

        return $boost->fresh();
    }

    /**
     * @param Boost $boost
     * @param array $filters
     * @return object
     */
    public function calcCoverage(Boost $boost, $filters = [])
    {
        $coverageStats = (new BoostCoverageService($boost, $filters))
            ->validateFilters()
            ->setActiveClients()
            ->calc()
            ->stats();

        return (object) $coverageStats;
    }

    /**
     * @param Boost $boost
     * @return array
     */
    public function getProductList(Boost $boost): array
    {
        $parsedFilters = json_decode($boost->applied_filters, true, 512, JSON_THROW_ON_ERROR);
        $originCodes = (new BoostCoverageService($boost, $parsedFilters))
            ->validateFilters()
            ->setActiveClients()
            ->calc()
            ->originCodes();

        return DB::table('fefo')
            ->join('produtos', 'fefo.codigo_produto', '=', 'produtos.codigo')
            ->select(DB::RAW("CONCAT(codigo_produto, '-', semanas) as hash"), 'nome')
            ->where('date', now()->toDateString())
            ->where('produtos.company_id', '=', $boost->company_id)
            ->where('fefo.company_id', '=', $boost->company_id)
            ->whereIn('cod_origem', $originCodes)
            ->groupBy('hash')
            ->orderBy('nome')
            ->get()
            ->pluck('nome', 'hash')
            ->toArray();
    }

    public function getOriginList(Boost $boost, $productCode, $weeks)
    {
        $parsedFilters = json_decode($boost->applied_filters, true, 512, JSON_THROW_ON_ERROR);
        $coverage = (new BoostCoverageService($boost, $parsedFilters))
            ->validateFilters()
            ->setActiveClients()
            ->calc();

        $fefosAvailable = DB::table('fefo')
            ->select('cod_origem', 'valor_atual')
            ->where('date', now()->toDateString())
            ->where('company_id', $boost->company_id)
            ->where('codigo_produto', $productCode)
            ->where('semanas', $weeks)
            ->whereIn('cod_origem', $coverage->originCodes())
            ->pluck('valor_atual', 'cod_origem');

        $origins = LeadTime::whereIn('cod_origem', $fefosAvailable->keys())
            ->groupBy('cod_origem')
            ->pluck('nome', 'cod_origem');

        $response = [];
        $clientsByOrigin = $coverage->clientsByOrigin();
        foreach($fefosAvailable as $originCode => $price) {
            $response[] = [
                'origin_code' => $originCode,
                'origin_name' => $origins[$originCode],
                'selling_price' => (float) $price,
                'clients' => count($clientsByOrigin[$originCode]),
            ];
        }

        return collect($response);
    }

    /**
     * @param $boost
     * @param $data
     * @return mixed
     */
    public function finish($boost, $data)
    {
        $parsedFilters = json_decode($boost->applied_filters, true, 512, JSON_THROW_ON_ERROR);
        $parsedFilters['origin_codes'] = collect($data['origins'])->pluck('origin_code');
        $coverage = (new BoostCoverageService($boost, $parsedFilters))
            ->validateFilters()
            ->setActiveClients()
            ->calc()
            ->stats();

        if($data['draft'] === false){
            $boost->status_id = Status::type(Status::IN_PROGRESS)->id;
        }
        $boost->coverage = $coverage['coverage'];
        $boost->ruled_out = $coverage['ruled_out'];
        $boost->product_code = $data['product_code'];
        $boost->weeks = $data['weeks'];
        $boost->save();

        $boost->origins()->delete();
        $boost->origins()->saveMany( $this->saveOrigins($data['origins']));

        return $boost->load(['product', 'origins']);
    }

    /**
     * @param $originsToSave
     * @return array
     */
    public function saveOrigins($originsToSave)
    {
        $origins = [];

        foreach($originsToSave as $toSave){
            $origins[] = new BoostOrigin($toSave);
        }

        return $origins;
    }

    /**
     * @param Boost $boost
     * @param       $filters
     * @return false|string
     */
    protected function prepareFilters(Boost $boost, $filters)
    {
        $filters['products'] = Product::whereIn('code', $filters['product_codes'])
            ->where('company_id', $boost->company_id)
            ->pluck('name');

        $filters['typologies'] = (new TypologyListService)
            ->list($boost->company, $filters['typology_codes'])
            ->values()
            ->toArray();

        return json_encode($filters, JSON_THROW_ON_ERROR, 512);
    }
}
