<?php
namespace App\Boost\Services;

use App\Boost\ClientList;
use App\Boost\Events\CacheClientList;
use App\Company\Company;
use App\Boost\Http\Imports\ClientListImport;
use App\Core\Classes\CrudService;
use Illuminate\Support\Facades\Storage;

class ClientListService extends CrudService
{
    protected $entity = ClientList::class;

    protected $module = 'Boost';

    public function list(Company $company, $filters = [])
    {
        $query = $this->newQuery()->with('user')->fromCompany($company->id);

        if(isset($filters['sort'])) {
            $direction = $filters['sort'][0] === '-' ? 'desc' : 'asc';
            $column    = ltrim($filters['sort'], '-');

            if($column === 'user_name') {
                $column = 'user_id';
            }

            $query->orderBy($column, $direction);
        }

        return $query->paginate($filters['limit'] ?? 10);
    }

    public function uploadList($request, $user)
    {
        $fileToImport = $request['file'];

        $fileUrl = Storage::url(Storage::cloud()->putFileAs('', $fileToImport, $fileToImport->getClientOriginalName()));

        $cnpjs = (new ClientListImport)->import($fileToImport);

        $insertData = [
            'name' => $request['name'],
            'company_id' => $user->company->id,
            'user_id' => $user->id ,
            'file_name' => $fileToImport->getClientOriginalName(),
            'file_url' => $fileUrl,
            'rows' => count($cnpjs['valid']) + count($cnpjs['invalid']),
            'imported_rows' => count($cnpjs['valid']),
            'invalid_rows' => count($cnpjs['invalid']),
            'duplicate_rows' => $cnpjs['duplicate'],
        ];

        $clientList = (new ClientList())->create($insertData);

        $clientList->listsCnpj()->createMany($cnpjs['valid']);

        event(new CacheClientList($clientList));

        return $clientList;
    }
}
