<?php
namespace App\Boost\Services;

use App\Boost\Boost;
use App\Boost\BoostsPurchasesView;
use App\LeadTime\LeadTime;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

class BoostCoverageService
{
    protected $boost;
    protected $filters;
    protected $coverageList = [];
    protected $stats = [];
    protected $activeClients = [];
    protected $cnpjList = [];

    /**
     * BoostCoverageService constructor.
     * @param Boost $boost
     * @param array $filters
     */
    public function __construct(Boost $boost, $filters = [])
    {
        $this->boost = $boost;
        $this->filters = collect($filters);
    }

    public function calc(): object
    {
        if(!$this->filters->has('use_filters') || $this->filters['use_filters'] === 'no') {
            $this->coverageList = $this->cnpjList;

            $this->stats['coverage'] = count($this->cnpjList);
            $this->stats['ruled_out'] = 0;

            return $this;
        }

        $coverage = $this->getCoverage();

        if($this->filters->has('origin_codes')) {
            $coverage = LeadTime::whereIn('code', $this->filters['origin_codes'])
                ->where('company_id', $this->boost->company_id)
                ->whereIn('cnpj', $coverage)
                ->pluck('cnpj')
                ->toArray();
        }

        $coverageCount = count($coverage);
        $this->stats['coverage'] = $coverageCount;

        $ruledOut = count($this->cnpjList) - $coverageCount;
        $this->stats['ruled_out'] = $ruledOut < 0 ? 0 : $ruledOut;

        $this->coverageList = $coverage;

        return $this;
    }

    public function getCoverage()
    {
        $purchases = $this->getPurchaseHistory();

        // Movendo validação se é só lance/carrinho para PHP porque a query quebra devido as collations diferentes das
        // tabelas do banco
        if ($this->filters->get('purchase_history') === 'bids') {
            $purchases = $purchases->where('purchase_type', 'lances');
        }

        if ($this->filters->get('purchase_history') === 'orders') {
            $purchases = $purchases->where('purchase_type', 'carrinho');
        }

        $purchasesByClient = $this->groupPurchasesByClient($purchases);

        // Filtro pede apenas clientes sem compra de todos os produtos indicados, a query já trás isso, portanto não
        // precisa continuar nenhuma validação.
        if($this->filters->get('already_purchased') === 'no' &&
           $this->filters->get('product_condition') === 'all') {
            return collect($this->cnpjList)->filter(function($cnpj) use($purchasesByClient) {
                return !array_key_exists($cnpj, $purchasesByClient);
            });
        }

        // Filtro pede apenas clientes sem compra de um os produtos indicados, a query já trás isso, portanto não
        // precisa continuar nenhuma validação.
        if($this->filters->get('already_purchased') === 'no' &&
           $this->filters->get('product_condition') === 'one') {
            return collect($this->cnpjList)->filter(function($cnpj) use($purchasesByClient) {
                return !array_key_exists($cnpj, $purchasesByClient)
                       || array_keys($purchasesByClient[$cnpj]) == $this->filters->get('product_codes');
            });
        }

        // Filtro pede apenas clientes com pelo menos uma compra de qualquer tipo para qualquer produto selecionado,
        // portanto não precisa continuar nenhuma validação.
        if($this->filters->get('already_purchased') === 'yes'
           && $this->filters->get('purchase_history') === 'bids_or_orders'
           && $this->filters->get('product_condition') === 'one') {
            return array_keys($purchasesByClient);
        }

        // Filtro pede apenas clientes com pelo menos uma compra de qualquer tipo para todos os produtos selecionados,
        // porém apenas um produto foi selecionado, sendo desnecessária qualquer validação adicional.
        if($this->filters->get('already_purchased') === 'yes'
           && $this->filters->get('purchase_history') === 'bids_or_orders'
           && $this->filters->get('product_condition') === 'all'
           && count($this->filters->get('product_codes')) === 1) {
            return array_keys($purchasesByClient);
        }

        $qualifiedClients = [];

        foreach($purchasesByClient as $cnpj => $purchases) {
            if($this->filters->get('purchase_history') === 'bids_and_orders'
               && $this->filters->get('product_condition') === 'one') {
                $hasValidPurchases = array_filter($purchases, function($purchase) {
                    return $purchase['lances'] > 0 && $purchase['carrinho'] > 0;
                });

                if(count($hasValidPurchases) > 0) {
                    $qualifiedClients[] = $cnpj;
                }

                continue;
            }

            if($this->filters->get('purchase_history') === 'bids_and_orders'
               && $this->filters->get('product_condition') === 'all') {
                $hasValidPurchases = array_filter($purchases, function($purchase) {
                    return $purchase['lances'] > 0 && $purchase['carrinho'] > 0;
                });

                if(count($hasValidPurchases) > 1
                   && array_keys($hasValidPurchases) == $this->filters->get('product_codes')) {
                    $qualifiedClients[] = $cnpj;
                }

                continue;
            }

            // Se chegou até aqui é porque o filtro de purchase_history é ou "bids" ou "orders", e o filtro de
            // already_purchased é "no"
            if($this->filters->get('product_condition') === 'one') {
                $qualifiedClients[] = $cnpj;
                continue;
            }

            if(array_keys($purchases) == $this->filters->get('product_codes')) {
                $qualifiedClients[] = $cnpj;
            }

            continue;
        }

        return $qualifiedClients;
    }

    public function validateFilters(): self
    {
        Validator::make($this->filters->toArray(), [
            'purchase_history' => 'in:bids,orders,bids_and_orders,bids_or_orders',
            'purchases_start' => 'date',
            'purchases_finish' => 'date|after:purchases_start',
            'typology_codes' => 'array|min:1',
            'already_purchased' => 'string|in:yes,no',
            'product_condition' => 'string|in:all,one',
            'product_codes' => 'array|min:1',
        ])->validate();

        return $this;
    }

    public function setActiveClients(): self
    {
        $total = 0;
        $totalQualified = 0;
        $clients = [];

        foreach($this->boost->clientLists as $clientList) {
            $clientListCacheService = (new ClientListCacheService($clientList))->setCacheNamespace();

            $cnpjList = $clientListCacheService->getList();

            if($cnpjList === null || count($cnpjList) === 0) {
                continue;
            }

            $total += count($cnpjList);

            $clientListCacheService->cacheClientsIds();

            $clientStatusList = $clientListCacheService->cacheClientStatus();

            $activeClients = collect($clientStatusList)
                ->where('status', 1)
                ->filter(function ($client) {
                    return (float)$client->account_balance > (float)$client->min_order
                        && $client->company_id === $this->boost->company_id;
                });

            $clients[] = $activeClients->all();

            $totalQualified += $activeClients->count();
        }

        $this->stats['total'] = $total;
        $this->stats['qualified'] = $totalQualified;
        $this->stats['disqualified'] = $total - $totalQualified;

        $this->activeClients = array_flatten($clients);
        $this->cnpjList = collect($this->activeClients)->pluck('cnpj')->toArray();

        return $this;
    }

    protected function getPurchaseHistory()
    {
        $query = BoostsPurchasesView::query()->where('company_id', $this->boost->company_id);

        if($this->filters->has('purchases_start')) {
            $query->whereDate('data', '>=', $this->filters->get('purchases_start'));
        }

        if($this->filters->has('purchases_finish')) {
            $query->whereDate('data', '<=', $this->filters->get('purchases_finish'));
        }

        if($this->filters->has('typology_codes')) {
            $query->whereIn('typology_code', $this->filters->get('typology_codes'));
        }

        if($this->filters->has('product_codes')) {
            $query->whereIn('product_code', $this->filters->get('product_codes'));
        }

        return $query->get();
    }

    protected function groupPurchasesByClient($purchases): array
    {
        $clientPurchases = [];

        foreach ($purchases as $purchase) {
            if (!isset($clientPurchases[$purchase->cnpj])) {
                $clientPurchases[$purchase->cnpj] = [];
            }
            if (!isset($clientPurchases[$purchase->cnpj][$purchase->product_code])) {
                $clientPurchases[$purchase->cnpj][$purchase->product_code] = [
                    'lances' => 0,
                    'carrinho' => 0,
                ];
            }
            $clientPurchases[$purchase->cnpj][$purchase->product_code][$purchase->purchase_type]++;
        }

        return $clientPurchases;
    }

    public function coverageList(): array
    {
        return $this->coverageList;
    }

    public function stats(): array
    {
        return $this->stats;
    }

    public function activeClients(): array
    {
        return $this->activeClients;
    }

    public function cnpjList(): array
    {
        return $this->cnpjList;
    }

    public function originCodes()
    {
        return collect($this->activeClients)
            ->filter(function($client) {
                return in_array($client->cnpj, $this->coverageList);
            })
            ->pluck('cod_origem')
            ->unique()
            ->values()
            ->toArray();
    }

    public function clientsByOrigin()
    {
        return collect($this->activeClients)
            ->filter(function($client) {
                return in_array($client->cnpj, $this->coverageList);
            })
            ->groupBy('cod_origem')
            ->toArray();
    }
}