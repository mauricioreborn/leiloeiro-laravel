<?php
namespace App\Boost\Services;

use App\Boost\ClientList;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClientListCacheService
{
    protected $clientList;
    protected $cacheNamespace;
    protected $cnpjList;

    /**
     * ClientListCacheService constructor.
     * @param $clientList
     */
    public function __construct(ClientList $clientList)
    {
        $this->clientList = $clientList;
    }

    public function cache()
    {
        Log::info("[ClientListCache] Gerando cache para lista de clientes ID {$this->clientList->id}");

        $this->setCacheNamespace();
        $this->resetCurrentCache();
        $this->getList();

        $this->cacheClientsIds();
        $this->cacheClientStatus();
    }

    public function setCacheNamespace(): self
    {
        $this->cacheNamespace = "clientLists:{$this->clientList->id}";

        return $this;
    }

    protected function resetCurrentCache(): void
    {
        if(Cache::has("{$this->cacheNamespace}:cnpj")) {
            Cache::forget("{$this->cacheNamespace}:cnpj");
            Cache::forget("{$this->cacheNamespace}:clients");
            Cache::forget("{$this->cacheNamespace}:clientsStatus");
        }
    }

    public function cacheClientsIds()
    {
        return Cache::rememberForever("{$this->cacheNamespace}:clients", function() {
            return DB::table('clientes')
                ->select('cnpj', 'id')
                ->whereIn('cnpj', $this->cnpjList)
                ->get()
                ->toArray();
        });
    }

    public function cacheClientStatus()
    {
        return Cache::rememberForever("{$this->cacheNamespace}:clientsStatus", function() {
            return DB::table('client_companies')
                ->select('client_id', 'client_companies.id', 'status', 'min_order', 'account_balance', 'typology_code', 'client_companies.company_id', 'clientes.cnpj', 'cod_origem')
                ->join('clientes', 'client_companies.client_id', '=', 'clientes.id')
                ->join('leadtime', function($join) {
                    return $join->on('clientes.cnpj', '=', 'leadtime.cnpj')
                        ->where('client_companies.company_id', '=', $this->clientList->company_id);
                })
                ->whereIn('client_id', collect(Cache::get("{$this->cacheNamespace}:clients"))->pluck('id'))
                ->where('client_companies.company_id', $this->clientList->company_id)
                ->whereNull('leadtime.deleted_at')
                ->whereNull('client_companies.deleted_at')
                ->groupBy('clientes.cnpj')
                ->get()
                ->toArray();
        });
    }

    public function getList()
    {
        $this->setCacheNamespace();

        return Cache::rememberForever("{$this->cacheNamespace}:cnpj", function() {
            return $this->setCnpjList()->cnpjList;
        });
    }

    protected function setCnpjList(): self
    {
        $this->cnpjList = $this->clientList->listsCnpj()->pluck('cnpj')->toArray();

        return $this;
    }
}