<?php
namespace App\Boost\Services;

use App\ClientCompanies\ClientCompany;
use App\Company\Company;

class TypologyListService
{
    public function list(Company $company, $codes = [])
    {
        $query = ClientCompany::where('company_id', $company->id)
            ->whereNotNull('typology')
            ->whereNotNull('typology_code')
            ->where('typology', '!=', '');

        if(count($codes) > 0) {
            $query->whereIn('typology_code', $codes);
        }

        return $query->pluck('typology', 'typology_code');
    }
}