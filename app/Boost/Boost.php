<?php

namespace App\Boost;

use App\Boost\Relations\BelongsToProductRelation;
use App\Company\Company;
use App\Core\Classes\Transactible;
use App\Core\Entities\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class Boost extends Model
{
    use Eloquence, SoftDeletes, Transactible;

    protected $fillable = [
        'company_id',
        'name',
        'whatsapp',
        'push',
        'email',
        'sms',
        'title',
        'description',
        'product_code',
        'weeks',
        'coverage',
        'reach',
        'conversion',
        'disqualified',
        'ruled_out',
        'revenue',
        'status_id',
        'applied_filters',
        'started_at',
        'finished_at',
    ];

    protected $dates = [
        'started_at',
        'finished_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'boolean' => ['email', 'push', 'sms', 'whatsapp'],
        'json' => 'applied_filters',
    ];

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }

    /**
     * Company relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Status relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * ClientList relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clientLists()
    {
        return $this->belongsToMany(ClientList::class, 'boosts_client_lists');
    }

    /**
     * ClientList relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function origins()
    {
        return $this->hasMany(BoostOrigin::class);
    }

    public function product()
    {
        return (new BelongsToProductRelation($this));
    }
}