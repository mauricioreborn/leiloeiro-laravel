<?php

namespace App\Boost;

use App\Auth\User;
use App\Company\Company;
use App\Core\Classes\Transactible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class ClientList extends Model
{
    use Eloquence, SoftDeletes, Transactible;

    protected $fillable = [
        'company_id',
        'user_id',
        'name',
        'file_name',
        'file_url',
        'rows',
        'imported_rows',
        'invalid_rows',
        'duplicate_rows',
    ];

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }

    /**
     * Company relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * User relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * ClientListCnpj relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listsCnpj()
    {
        return $this->hasMany(ClientListCnpj::class);
    }

    /**
     * Boosts relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function boosts()
    {
        return $this->belongsToMany(Boost::class, 'boosts_client_lists');
    }
}