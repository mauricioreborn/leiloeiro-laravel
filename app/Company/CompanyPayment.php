<?php

namespace App\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Payment\Payment;
use App\Client\ClientCompanyPayment;

class CompanyPayment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'payment_id',
        'company_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @return BelongsTo
     */
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    /**
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return HasMany
     */
    public function clientCompanyPayments()
    {
        return $this->hasMany(ClientCompanyPayment::class);
    }
}
