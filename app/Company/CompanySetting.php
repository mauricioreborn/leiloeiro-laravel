<?php

namespace App\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanySetting extends Model
{
    protected $fillable = [
        'setting',
        'value',
        'company_id',
    ];

    /**
     * Company relationship
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
