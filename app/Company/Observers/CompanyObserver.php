<?php

namespace App\Company\Observers;

use App\Company\Company;
use App\Company\Services\CompanySettingService;

class CompanyObserver
{
    /**
     * Handle the company "created" event.
     *
     * @param Company $company Company entity
     *
     * @return void
     */
    public function created(Company $company)
    {
        (new CompanySettingService())->createHasSeller($company);
        (new CompanySettingService())->createHasManagementRequests($company);
        (new CompanySettingService())->createAddAccountBalanceCancelOrder($company);
        (new CompanySettingService())->createAddVolumeFefoCancelOrder($company);
        (new CompanySettingService())->createBidMinAcceptablePercent($company);
        (new CompanySettingService())->createPeriodView($company);
        (new CompanySettingService())->createReduceAccountBalanceApproveBid($company);
        (new CompanySettingService())->createReduceAccountBalanceCheckoutCart($company);
        (new CompanySettingService())->createReduceVolumeFefoApproveBid($company);
        (new CompanySettingService())->createReduceVolumeFefoCheckoutCart($company);
        (new CompanySettingService())->createStockReplicate($company);
        (new CompanySettingService())->createAcceptsDevolution($company);
        (new CompanySettingService())->createBillingDueDay($company);
        (new CompanySettingService())->createFefoReplicate($company);
        (new CompanySettingService())->createOrderS3Integration($company);
    }
}
