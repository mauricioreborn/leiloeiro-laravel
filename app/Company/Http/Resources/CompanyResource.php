<?php

namespace App\Company\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;
use App\Client\Http\Resources\ClientCompanyResource;
use App\Company\Http\Resources\CompanySettingResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'company',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'code' => $this->code,
                'cart' => $this->cart,
                'bid' => $this->bid,
                'rules' => $this->rules,
                'aboult' => $this->aboult,
                'policy' => $this->policy,
                'information' => $this->information,
                'image' => $this->image,
                'settings' => $this->settings
            ],
            'relationships' => [
                'clientCompanies' => $this->whenPivotLoadedAs('clientCompany', 'client_companies', function () {
                    return new ClientCompanyResource($this->clientCompany);
                })
            ],
        ];
    }
}
