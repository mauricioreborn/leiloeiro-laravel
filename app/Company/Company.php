<?php

namespace App\Company;

use App\Auth\User;
use App\Boost\Boost;
use App\Boost\ClientList;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\DistributionCenter\DistributionCenter;
use App\Fefo\FefoPriceBoost;
use App\Products\Product;
use App\Stock\Stock;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'code',
        'cart',
        'bid',
        'rules',
        'aboult',
        'policy',
        'information',
        'image',
        'product_info',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $appends = [
        'settings'
    ];

    /**
     * users Relationship
     * @return User
     **/
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * CompanySettings relationship
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function companySettings()
    {
        return $this->hasMany(CompanySetting::class);
    }

    /**
     * clients Relationship
     * @return Client
     **/
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_companies')
            ->as('clientCompany')
            ->withPivot((new ClientCompany())->getFillable())
            ->using(ClientCompany::class);
    }

    /**
     * CompanyPayment Relationship
     * @return CompanyPayment
     **/
     public function companyPayments()
     {
       return $this->hasMany(CompanyPayment::class);
     }

    /**
     * Company settings attribute
     *
     * @return array
     **/
    public function getSettingsAttribute()
    {
        return $this->companySettings->mapWithKeys(function ($companySetting) {
            return [$companySetting->setting => $companySetting->value];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companyDivisions()
    {
        return $this->hasMany(CompanyDivision::class);
    }

    /**
     * DistributionCenter relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function distributionCenters()
    {
        return $this->hasMany(DistributionCenter::class);
    }

    /**
     * Method to relates the bids class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fefoPriceBoost()
    {
        return $this->hasMany(FefoPriceBoost::class);
    }

    /**
     * Method to relates the stock with company
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * ClientList relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientLists()
    {
        return $this->hasMany(ClientList::class);
    }

    /**
     * Boosts relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boosts()
    {
        return $this->hasMany(Boost::class);
    }
}
