<?php

namespace App\Company;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyDivision extends Model
{
    use  SoftDeletes;

    protected $fillable = [
        'division',
        'company_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * company Relationship
     *
     * @return Company
     **/
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
