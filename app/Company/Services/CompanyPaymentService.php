<?php
namespace App\Company\Services;

use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Payment\Payment;
use App\Core\Classes\CrudService;
use App\Core\Classes\Search;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;

class CompanyPaymentService
{
    /**
     * Method to get all company payments
     * @param Company $company company object
     * @return mixed
     */
    public function list(Company $company)
    {
        return CompanyPayment::where('company_id', $company->id)->with('payment')->get();
    }

    /**
     * Method to check company clients
     * @param Client  $client         client object
     * @param Company $company        company object
     * @param array   $clientPayments client payments params
     * @return null
     */
    public function checkCompanyClients(Client $client, Company $company, array $clientPayments)
    {
        ClientCompany::where([
            'client_id' => $client->id,
            'company_id' => $company->id
        ])->firstOrFail();

        $companyPayments = $this->list($company);

        foreach ($clientPayments as $clientPayment) {
            if (!in_array($clientPayment, $companyPayments->pluck('id')->toArray())) {
                throw new InvalidArgumentException(
                    __('Payment/errors.company_payment_not_allowed', ['company_payment_id' => $clientPayment])
                );
            }
        }
    }

    /**
     * Method do filter Client's companies that accept credit card
     * @param Client $client client object
     * @return Illuminate\Support\Collection
     */
    public function companiesAcceptCreditCard(Client $client)
    {
        $creditCardPayment = (new Payment())->isCreditCard()->first();

        $companiesAcceptCreditCard = $client->companies->filter(function ($company) use ($creditCardPayment) {
            return $company->companyPayments->where('payment_id', $creditCardPayment->id)->first();
        });

        return $companiesAcceptCreditCard;
    }
}
