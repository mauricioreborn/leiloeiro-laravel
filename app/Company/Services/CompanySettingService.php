<?php

namespace App\Company\Services;

use App\Company\Company;
use App\Company\CompanySetting;

class CompanySettingService
{
    const DAILY = 'daily';
    const WEEKLY = 'weekly';
    const MONTHLY = 'monthly';

    const PERIOD_TYPES = [
        self::DAILY,
        self::WEEKLY,
        self::MONTHLY
    ];

    /**
     * create company setting
     *
     * @param Company $company Company Entity
     * @param string  $setting Setting key
     * @param string  $value   value of setting
     *
     * @return CompanySetting
     */
    public function create(Company $company, string $setting, string $value): CompanySetting
    {
        $companySetting = CompanySetting::firstOrNew([
            'setting' => $setting,
            'company_id' => $company->id
        ]);

        $companySetting->value = $value;

        $companySetting->save();

        return $companySetting->fresh();
    }

    /**
     * create `bid min acceptable price percent` setting
     *
     * @param Company $company Company entity
     * @param string  $default Default value: 30
     *
     * @return CompanySetting
     */
    public function createBidMinAcceptablePercent(Company $company, $default = '30'): CompanySetting
    {
        return $this->create($company, 'bidMinAcceptablePercent', $default);
    }

    /**
     * create `period view` setting
     *
     * @param Company $company Company entity
     *
     * @param string  $default Default value: 30
     *
     * @return CompanySetting
     */
    public function createPeriodView(Company $company, $default = self::WEEKLY): CompanySetting
    {
        return $this->create($company, 'periodView', $default);
    }

    /**
     * get `period view` setting
     *
     * @param Company $company Company entity
     * @param integer $count   Counter
     *
     * @return string
     */
    public function getPeriodView(Company $company, $count = 0): string
    {
        $periodView = $company->companySettings->where('setting', 'periodView')->first();

        return trans_choice("companySetting.{$periodView->value}", $count);
    }

    /**
     * create `has_seller` setting
     *
     * @param Company $company Company entity
     * @param boolean $default Default value: false
     *
     * @return CompanySetting
     */
    public function createHasSeller(Company $company, $default = false): CompanySetting
    {
        return $this->create($company, 'hasSeller', $default);
    }

    /**
     * Method to create expiration billing day
     *
     * @param Company $company Company entity
     * @param bool    $default Default value: false
     * @return CompanySetting
     */
    public function createBillingDueDay(Company $company, $default = false): CompanySetting
    {
        return $this->create($company, 'billingDueDay', $default);
    }

    /**
     * create `Reduce Account Balance Checkout Cart` setting
     *
     * @param Company $company Company entity
     * @param bool    $default Default value: true
     *
     * @return CompanySetting
     */
    public function createReduceAccountBalanceCheckoutCart(Company $company, bool $default = true): CompanySetting
    {
        return $this->create($company, 'reduceAccountBalanceCheckoutCart', (string) $default ?: 0);
    }

    /**
     * create `Reduce Account Balance Approve Bid` setting
     *
     * @param Company $company Company entity
     * @param bool    $default Default value: true
     *
     * @return CompanySetting
     */
    public function createReduceAccountBalanceApproveBid(Company $company, bool $default = true): CompanySetting
    {
        return $this->create($company, 'reduceAccountBalanceApproveBid', (string) $default ?: 0);
    }

    /**
     * create `Add Account Balance Cancel Order` setting
     *
     * @param Company $company Company entity
     * @param bool    $default Default value: true
     *
     * @return CompanySetting
     */
    public function createAddAccountBalanceCancelOrder(Company $company, bool $default = true): CompanySetting
    {
        return $this->create($company, 'addAccountBalanceCancelOrder', (string) $default ?: 0);
    }

    /**
     * create `Reduce Volume Fefo Checkout Cart` setting
     *
     * @param Company $company Company entity
     * @param bool    $default Default value: true
     *
     * @return CompanySetting
     */
    public function createReduceVolumeFefoCheckoutCart(Company $company, bool $default = true): CompanySetting
    {
        return $this->create($company, 'reduceVolumeFefoCheckoutCart', (string) $default ?: 0);
    }

    /**
     * create `Reduce Volume Fefo Approve Bid` setting
     *
     * @param Company $company Company entity
     * @param bool    $default Default value: true
     *
     * @return CompanySetting
     */
    public function createReduceVolumeFefoApproveBid(Company $company, bool $default = true): CompanySetting
    {
        return $this->create($company, 'reduceVolumeFefoApproveBid', (string) $default ?: 0);
    }

    /**
     * create `Add Volume Fefo Cancel Order` setting
     *
     * @param Company $company Company entity
     * @param bool    $default Default value: true
     *
     * @return CompanySetting
     */
    public function createAddVolumeFefoCancelOrder(Company $company, bool $default = true): CompanySetting
    {
        return $this->create($company, 'addVolumeFefoCancelOrder', (string) $default ?: 0);
    }

    /**
     * create `has Management Requests` setting
     *
     * @param Company $company Company entity
     * @param boolean $default Default value: false
     *
     * @return CompanySetting
     */
    public function createHasManagementRequests(Company $company, $default = false): CompanySetting
    {
        return $this->create($company, 'hasManagementRequests', $default);
    }

    /**
     * create Fefo Sync seeting
     *
     * @param Company $company
     * @param bool    $default
     * @return CompanySetting
     */
    public function createFefoReplicate(Company $company, $default = false): CompanySetting
    {
        return $this->create($company, 'hasFefoReplicate', $default);
    }

    public function createAcceptsDevolution(Company $company, $default = false): CompanySetting
    {
        return $this->create($company, 'acceptsDevolution', $default);
    }

    /**
     * create Stock Sync seeting
     *
     * @param Company $company
     * @param bool    $default
     * @return CompanySetting
     */
    public function createStockReplicate(Company $company, $default = false): CompanySetting
    {
        return $this->create($company, 'hasStockReplicate', $default);
    }

    /**
     * Create OrderS3Integration
     *
     * @param Company $company
     * @param bool    $default
     * @return CompanySetting
     */
    public function createOrderS3Integration(Company $company, $default = false): CompanySetting
    {
        return $this->create($company, 'hasOrderS3Integration', $default);
    }
}
