<?php

namespace App\Iugu;

use App\Client\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IuguClient extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'client_id',
        'iugu_id'
    ];

    /**
     * Client relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
