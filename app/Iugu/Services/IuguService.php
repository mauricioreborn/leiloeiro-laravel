<?php

namespace App\Iugu\Services;

use App\Client\Client;
use App\Company\Company;
use Illuminate\Support\Facades\Log;

class IuguService extends IuguApiService
{
    /**
     * get iugu account ID
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->getApiId();
    }

    /**
     * Get clients
     *
     * @param string $query Query search param
     *
     * @return array
     */
    public function clients(string $query = null)
    {
        $fetch =  $this->fetch('get', 'customers?query=' . $query);

        if (!array_get($fetch, 'status', false)) {
            return false;
        }

        return array_get($fetch, 'body', []);
    }

    /**
     * Create client
     *
     * @param Client $client Client entity
     *
     * @return array
     */
    public function createClient(Client $client)
    {
        if (!$client->iuguClient) {
            $clients = $this->clients($client->id);

            if (array_get($clients, 'totalItems')) {
                $items = array_get($clients, 'items');

                $body = array_first($items);
            } else {
                $fetch = $this->fetch('post', 'customers', [
                    'email' => $client->email,
                    'name' => $client->name,
                    'cpf_cnpj' => $client->cnpj,
                    'custom_variables' => [
                        [ 'name' => 'client_id', 'value' => $client->id ]
                    ]
                ]);

                if (!array_get($fetch, 'status')) {
                    return false;
                }

                $body = array_get($fetch, 'body', []);
            }

            $iuguId = array_get($body, 'id');

            if ($iuguId) {
                $client->iuguClient()->create(['iugu_id' => $iuguId]);
            }
        }

        return $client->iuguClient()->first();
    }

    /**
     * Get client payment methods
     *
     * @param Client $client Client entity
     *
     * @return array
     */
    public function listPaymentMethods(Client $client)
    {
        if ($client->iuguClient) {
            return $this->fetch('get', 'customers/' .$client->iuguClient->iugu_id. '/payment_methods');
        }

        return [];
    }

    /**
     * Create payment method
     *
     * @param Client $client  Client entity
     * @param string $name    Card name
     * @param string $token   Token from mobile
     * @param bool   $default Default payment method?
     *
     * @return array | bool
     */
    public function createPaymentMethods(Client $client, string $name, string $token, bool $default = false)
    {
        $iuguClient = $client->iuguClient;

        if (!$iuguClient) {
            $iuguClient = $this->createClient($client);
        }

        $fetch = $this->fetch('post', 'customers/' .$iuguClient->iugu_id. '/payment_methods', [
            'description' => $name,
            'token' => $token,
            'set_as_default' => $default,
        ]);

        if (array_get($fetch, 'status', false)) {
            return array_get($fetch, 'body', []);
        }

        return false;
    }

    /**
     * update description of payment method
     *
     * @param Client $client Client entity
     * @param string $id     Payment method ID
     * @param string $name   Card name
     *
     * @return array | bool
     */
    public function updatePaymentMethods(Client $client, string $id, string $name)
    {
        if ($client->iuguClient) {
            $fetch = $this->fetch('put', 'customers/' .$client->iuguClient->iugu_id. '/payment_methods/' .$id, [
                'description' => $name,
            ]);

            if (array_get($fetch, 'status', false)) {
                return array_get($fetch, 'body', []);
            }
        }

        return false;
    }

    /**
     * Create charge
     *
     * @param  Client $client          Client entity
     * @param  string $paymentMethodId Payment ID
     * @param  string $email           email
     * @param  array  $items           items
     * @param  string $orderId         orderId
     * @param  string $months          Months
     *
     * @return bool|array
     */
    public function createCharge(
        Client $client,
        string $paymentMethodId,
        string $email,
        array $items = [],
        string $orderId,
        string $months = '1'
    ) {
        $iuguClient = $client->iuguClient;

        if ($iuguClient) {
            $fetch = $this->fetch('post', 'charge', [
                'customer_payment_method_id' => $paymentMethodId,
                'customer_id' => $client->iuguClient->iugu_id,
                'email' => $email,
                'items' => $items,
                'order_id' => $orderId,
                'months' => $months,
            ]);

            Log::info($fetch);

            if (array_get($fetch, 'status', false)) {
                return array_get($fetch, 'body', []);
            }
        }

        return false;
    }

    /**
     * Cancel charge
     *
     * @param string $orderId Order Id
     *
     * @return bool|array
     */
    public function cancelCharge(string $orderId)
    {
        $fetch = $this->fetch('put', "invoices/{$orderId}/cancel");
        Log::info($fetch);

        if (array_get($fetch, 'status', false)) {
            return array_get($fetch, 'body', []);
        }

        return false;
    }

    /**
     * Capture charge
     *
     * @param string $orderId Order ID
     *
     * @return bool|array
     */
    public function captureCharge(string $orderId)
    {
        $fetch = $this->fetch('post', "invoices/{$orderId}/capture");
        Log::info($fetch);

        if (array_get($fetch, 'status', false)) {
            return array_get($fetch, 'body', []);
        }

        return false;
    }

    /**
     * Show invoice
     *
     * @param string $invoice Invoice ID
     *
     * @return array
     */
    public function showInvoice(string $invoice)
    {
        $fetch = $this->fetch('get', 'invoices/' .$invoice);

        if (array_get($fetch, 'status', false)) {
            return array_get($fetch, 'body', []);
        }

        return false;
    }

    /**
     * Refund charge
     *
     * @param string $orderId Order ID
     *
     * @return bool|array
     */
    public function refundCharge(string $orderId)
    {
        $fetch = $this->fetch('post', "invoices/{$orderId}/refund");
        Log::info($fetch);

        if (array_get($fetch, 'status', false)) {
            return array_get($fetch, 'body', []);
        }

        return false;
    }
}
