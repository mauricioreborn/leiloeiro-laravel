<?php

namespace App\Iugu\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;

class IuguApiService
{
    /**
     * Get API url
     *
     * @return string iugu api base url
     */
    protected function getApiUrl(): string
    {
        return env('IUGU_API');
    }

    /**
     * Get API ID
     *
     * @return string iugu api token
     */
    protected function getApiId(): string
    {
        return env('IUGU_ID');
    }

    /**
     * Get API token
     *
     * @return string iugu api token
     */
    protected function getApiKey(): string
    {
        return env('IUGU_TOKEN');
    }

    /**
     * Get API headers
     *
     * @return array request headers
     */
    protected function getHeaders(): array
    {
        return [
            'Accept' => 'application/json',
            'Accept-Charset' => 'utf-8',
            'Accept-Language' => 'pt-br;q=0.9,pt-BR',
            'Content-Type' => 'application/json'
        ];
    }

    /**
     * Fetch API request
     *
     * @param string $method Http method
     * @param string $uri    URI request
     * @param array  $body   Body
     *
     * @return array request headers
     */
    protected function fetch(string $method, string $uri, array $body = [])
    {
        $payload = [
            'headers' => $this->getHeaders(),
            'auth' => [$this->getApiKey(), '']
        ];

        if ($body) {
            $payload['json'] = $body;
        }

        try {
            $fetch = (new Client())->request(
                $method,
                $this->getApiUrl() . $uri,
                $payload
            );

            return [
                'status' => true,
                'code' => $fetch->getStatusCode(),
                'body' => json_decode($fetch->getBody(), true)
            ];
        } catch (GuzzleException $e) {
            return $this->throwException($e);
        } catch (ClientException $e) {
            return $this->throwException($e);
        }
    }

    /**
     * throw Exception
     *
     * @param  Exception $exception Exception
     *
     * @return array
     */
    protected function throwException($exception)
    {
        return [
            'status' => false,
            'code' => $exception->getCode(),
            'body' => json_decode($exception->getResponse()->getBody()->getContents(), true)
        ];
    }
}
