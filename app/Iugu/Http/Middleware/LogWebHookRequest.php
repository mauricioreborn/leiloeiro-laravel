<?php

namespace App\Iugu\Http\Middleware;

use App\Log\LogWebHook;
use Closure;

class LogWebHookRequest
{
    /**
    * Handle an incoming request.
    *
    * @param Illuminate\Http\Request $request Request
    * @param Closure                 $next    Next
    *
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        LogWebHook::create([
            'data' => json_encode($request->all()),
            'event' => $request->event,
            'data_id' => '',
            'data_status' => '',
        ]);

        $request = $next($request);

        return $request;
    }
}
