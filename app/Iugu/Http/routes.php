<?php

Route::group(['prefix' => 'v1'], function () {
    // Route::group(['middleware' => 'validateJwt'], function () {
        Route::group(['prefix' => 'iugu'], function () {
            Route::group(['prefix' => 'webhooks', 'middleware' => 'logWebHookRequest'], function () {
                Route::post('/', 'WebHookController@fired');
                Route::post('/log', 'WebHookController@log');
            });
        });
    // });
});
