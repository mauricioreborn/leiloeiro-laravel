<?php

namespace App\Iugu\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Invoice\Services\InvoiceService;
use Illuminate\Http\Request;

class WebHookController extends Controller
{
    protected $service;

    /**
     * __construct
     * @param IuguService $service IuguService
     */
    public function __construct(InvoiceService $service)
    {
        $this->service = $service;
    }

    /**
     * Log web hooks
     *
     * @param  Request $request Request
     *
     * @return array
     */
    public function log(Request $request)
    {
        return [
            'ok'
        ];
    }

    /**
     * Fired all events
     *
     * @param  Request $request Request
     *
     * @return array
     */
    public function fired(Request $request)
    {
        $events = [
            'invoice.created',
            'invoice.status_changed',
            'invoice.refund',
            'invoice.payment_failed',
            'invoice.dunning_action',
            'invoice.due',
            'invoice.installment_released',
            'invoice.released'
        ];

        if (in_array($request->event, $events)) {
            $this->service->eventFired($request->event, $request->data);
        }

        return [
            'ok'
        ];
    }
}
