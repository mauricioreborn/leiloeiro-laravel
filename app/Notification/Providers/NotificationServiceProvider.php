<?php

namespace App\Notification\Providers;

use App\Notification\Notification;
use App\Notification\Observers\NotificationObserver;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Notification::observe(NotificationObserver::class);

        parent::boot();
    }
}
