<?php

namespace App\Notification\Observers;

use App\Jobs\NotificationJob;
use App\Notification\Notification;

class NotificationObserver
{
    /**
     * Handle the notification "created" event.
     *
     * @param Notification $notification Notification entity
     *
     * @return void
     */
    public function created(Notification $notification)
    {
        //
    }
}
