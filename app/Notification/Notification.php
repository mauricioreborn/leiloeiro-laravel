<?php

namespace App\Notification;

use App\Client\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    protected $table = 'notifications';

    const generic = '0';

    const rejectedBid = '1';

    protected $fillable = [
        'title',
        'type',
        'message',
        'attributes',
        'client_id',
        'success',
        'failure',
        'send_at',
    ];

    protected $dates = [
        'send_at',
    ];

    /**
     * Client relation
     *
     * @return Client
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
