<?php

namespace App\Notification\Services;

use App\Client\Client;
use App\Core\Entities\Status;
use App\Jobs\NotificationJob;
use App\Notification\Notification;
use App\Order\Order;
use App\Core\Entities\RejectionMotive;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Exception;

class NotificationService
{
    /**
     * Send rejected bid push notification
     *
     * @param integer $client_id Client ID
     * @param integer $bid       Bid ID $data
     * @param string  $product   Product name
     * @param string  $price     Product price
     *
     * @return mixed
     */
    public function rejectedBid($client_id, $bid, $product, $price)
    {
        $attributes = json_encode(['route' => 'VisualizarLancePage', 'resource' => $bid]);
        $notification = $this->createNotification(
            $client_id,
            __('rejectionMotive.bid.price_rejected.push.title', ['bid' => $bid]),
            __('rejectionMotive.bid.price_rejected.push.message',
                [
                    'product' => $product,
                    'price' => price_br($price)
                ]
            ),
            $attributes,
            Notification::rejectedBid
        );

        if ($client_id == 205669) {
            NotificationJob::dispatch($notification->id);
        }

        return $notification;
    }

    /**
     * @param integer $bidId       Bid ID
     * @param integer $clientId    Client ID
     * @param string  $productName Product Name
     * @return Notification
     */
    public function expiredBid($bidId, $clientId, $productName, $expiredAt): Notification
    {
        $title = __('rejectionMotive.bid.expired.push.title', ['bid' => $bidId]);
        $message = __('rejectionMotive.bid.expired.push.message', [
            'product' => $productName,
            'expired_at' => $expiredAt
        ]);
        $attributes = json_encode(['route' => 'VisualizarLancePage', 'resource' => $bidId]);

        return $this->createNotification($clientId, $title, $message, $attributes);
    }

    /**
     * @param integer              $clientId    Client ID
     * @param integer              $bidId       Bid ID
     * @param string               $productName Product Name
     * @param RejectionMotive|null $rejectionMotive
     * @return Notification
     */
    public function canceledBid($clientId, $bidId, $productName, RejectionMotive $rejectionMotive): Notification
    {
        $title = __("rejectionMotive.bid.{$rejectionMotive->name}.push.title", ['bid' => $bidId]);
        $message = __("rejectionMotive.bid.{$rejectionMotive->name}.push.message", ['product' => $productName]);
        $attributes = json_encode(['route' => 'VisualizarLancePage', 'resource' => $bidId]);

        return $this->createNotification($clientId, $title, $message, $attributes);
    }

    /**
     * @param integer $clientId    Client ID
     * @param integer $bidId       Bid ID
     * @param string  $productName Product Name
     * @param         $date
     * @return Notification
     */
    public function finishedBid($clientId, $bidId, $productName, $date): Notification
    {
        $title = __('status.order_bid.approved.push.title', ['bid' => $bidId]);
        $message = __('status.order_bid.approved.push.message', ['product' => $productName, 'date' => $date]);
        $attributes = json_encode(['route' => 'VisualizarLancePage', 'resource' => $bidId]);

        return $this->createNotification($clientId, $title, $message, $attributes);
    }

    public function partiallyBilledBid($clientId, $bidId, $productName, $date): Notification
    {
        $title = __('status.order_bid.partially_billed.push.title', ['bid' => $bidId]);
        $message = __('status.order_bid.partially_billed.push.message', ['product' => $productName, 'date' => $date]);
        $attributes = json_encode(['route' => 'VisualizarLancePage', 'resource' => $bidId]);

        return $this->createNotification($clientId, $title, $message, $attributes);
    }

    public function billedAboveBid($clientId, $bidId, $productName, $date)
    {
        $title = __('status.order_bid.billed_above.push.title', ['bid' => $bidId]);
        $message = __('status.order_bid.billed_above.push.message', ['product' => $productName, 'date' => $date]);

        return $this->createNotification($clientId, $title, $message);
    }

    public function approvedBid($clientId, $bidId, $productName, $price, $discount)
    {
        $title = __('status.order_bid.pending.push.title', ['bid' => $bidId]);
        $message = __('status.order_bid.pending.push.message', [
            'product' => $productName,
            'price' => price_br($price),
            'discount' => $discount,
        ]);
        $attributes = json_encode(['route' => 'VisualizarLancePage', 'resource' => $bidId]);

        return $this->createNotification($clientId, $title, $message, $attributes);
    }

    /**
     * Create notification
     *
     * @param integer $clientId Client ID
     * @param string  $title    Title
     * @param string  $message  Message
     * @param int     $type     Type
     *
     * @param null    $attributes
     * @return Notification
     */
    protected function createNotification($clientId, $title, $message, $attributes = null, $type = 0): Notification
    {
        return Notification::firstOrCreate([
            'type' => $type,
            'client_id' => $clientId,
            'title' => $title,
            'message' => $message,
            'attributes' => $attributes,
        ]);
    }

    /**
     * Method to create a notification by product boost entity
     *
     * @param Client $client  client object
     * @param string $title   notification title
     * @param string $message notification message
     * @return Notification
     */
    public function createNotificationByProductBoost(Client $client, $title, $message): Notification
    {
        return $this->createNotification($client->id, $title, $message);
    }

    /**
     * Method to create a notification for all approved order itens
     * @param integer $clientId client indentifier
     * @param Order   $orderId  order object
     * @return mixed
     */
    public function allApprovedOrder($clientId, $orderId)
    {
        $attributes = json_encode(['route' => 'PedidoPage', 'resource' => $orderId]);
        return $this->createNotification(
            $clientId,
            __('status.order.approved.push.title', ['order_id' => $orderId]),
            __('status.order.approved.push.message'),
            $attributes
        );
    }

    public function partiallyBilledOrder($clientId, $orderId): Notification
    {
        $attributes = json_encode(['route' => 'PedidoPage', 'resource' => $orderId]);
        return $this->createNotification(
            $clientId,
            __('status.order.partially_billed.push.title', ['order_id' => $orderId]),
            __('status.order.partially_billed.push.message'),
            $attributes
        );
    }

    /**
     * Method to create a notification for all canceled order itens
     * @param integer $clientId client indentifier
     * @param Order   $orderId  order object
     * @return mixed
     */
    public function allCanceledOrder($clientId, $orderId)
    {
        $attributes = json_encode(['route' => 'PedidoPage', 'resource' => $orderId]);
        return $this->createNotification(
            $clientId,
            __('rejectionMotive.order.canceled.push.title', ['order_id' => $orderId]),
            __('rejectionMotive.order.canceled.push.message'),
            $attributes
        );
    }

    /**
     * Send Push notification
     *
     * @param string  $client_id Client ID
     * @param string  $title     Title of message
     * @param string  $message   Message
     * @param mixed   $data      PayloadDataBuilder
     * @param integer $time      Time in seconds
     * @param string  $sound     Sound
     * @return array
     */
    public function sendFCM($client_id, $title, $message, $data = null, int $time = 2419200, string $sound = 'default')
    {
        $tokens = $this->setToken($client_id);

        if (count($tokens)) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive($time);

            $notificationBuilder = new PayloadNotificationBuilder($title);

            $notificationBuilder->setBody($message)->setSound($sound);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();

            if ($data !== null) {
                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(json_decode($data, true));
                $data = $dataBuilder->build();
            }

            try {
                $downstreamResponse = FCM::sendTo(
                    $tokens,
                    $option,
                    $notification,
                    $data
                );

                $success = $downstreamResponse->numberSuccess();
                $failure = $downstreamResponse->numberFailure();
            } catch (Exception $e) {
                Log::warning($e->getMessage());
            }
        }

        return [
            'success' => $success ?? 0,
            'failure' => $failure ?? 0,
        ];
    }

    /**
     * Set Client Token
     *
     * @param integer $client_id Client ID
     *
     * @return array
     */
    protected function setToken($client_id): array
    {
        $client = DB::table('clientes')->where("id", $client_id)->first();

        $tokens = [];

        if ($client) {
            if ($client->fcm_token) {
                $tokens[] = $client->fcm_token;
            }

            $fcm_tokens = DB::table('fcm_token')
                    ->where("client_id", $client_id)
                    ->whereNull('deleted_at')
                    ->pluck('token')
                    ->toArray();

            if (count($fcm_tokens)) {
                $tokens = array_unique(array_merge($tokens, $fcm_tokens));
            }
        }

        return $tokens;
    }
}
