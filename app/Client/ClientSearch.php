<?php

namespace App\Client;

use App\Client\Client;
use App\Company\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class ClientSearch extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'client_id',
        'company_id',
        'search',
        'response',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'response' => 'array',
    ];

    /**
    * client Relationship
    * @return Client
    **/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
    * status relationship
    * @return Client
    **/
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
