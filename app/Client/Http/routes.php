<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'clients'], function () {
        Route::get('/', 'ClientController@list');
        Route::post('/', 'ClientController@store');
        Route::get('/{id}', 'ClientController@show');
        Route::put('/{id}', 'ClientController@update');
        Route::delete('/{id}', 'ClientController@destroy');
    });

    Route::group(['prefix' => 'mobile/client'], function () {
        Route::get('/credit_cards', 'ClientCardController@list');
        Route::post('/credit_cards', 'ClientCardController@post');
    });
});
