<?php

namespace App\Client\Http\Factories\Filters;

use App\ClientCompanies\ClientCompany;
use App\Core\Contracts\FilterInterface;

class Min_order extends AbstractFilter implements FilterInterface
{
    public function run($query, $min_order)
    {
        return $query->whereHas('companies', function ($query) use ($min_order) {
            return $query->where('client_companies.min_order', 'LIKE', '%'.$min_order.'%');
        });
    }
}