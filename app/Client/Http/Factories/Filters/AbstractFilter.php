<?php

namespace App\Client\Http\Factories\Filters;

use App\Client\Client;

abstract class AbstractFilter
{
    protected $maps;

    public function __construct()
    {
        $client = new Client();
        $this->maps = $client->maps;
    }
}
