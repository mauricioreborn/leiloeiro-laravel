<?php

namespace App\Client\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Name extends AbstractFilter implements FilterInterface
{
    public function run($query, $name)
    {
        return $query->where('name', 'LIKE', '%'.$name.'%');
    }
}
