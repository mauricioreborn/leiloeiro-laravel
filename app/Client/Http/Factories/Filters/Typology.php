<?php

namespace App\Client\Http\Factories\Filters;

use App\ClientCompanies\ClientCompany;
use App\Core\Contracts\FilterInterface;

class Typology extends AbstractFilter implements FilterInterface
{
    public function run($query, $typology)
    {
        return $query->whereHas('companies', function ($query) use ($typology) {
            return $query->where('client_companies.typology', 'LIKE', '%'.$typology.'%');
        });
    }
}
