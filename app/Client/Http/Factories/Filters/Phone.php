<?php

namespace App\Client\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Phone extends AbstractFilter implements FilterInterface
{
    public function run($query, $phone)
    {
        return $query->where('phone', 'LIKE', '%'.$phone.'%');
    }
}
