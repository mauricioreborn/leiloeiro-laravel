<?php

namespace App\Client\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Email extends AbstractFilter implements FilterInterface
{
    public function run($query, $email)
    {
        return $query->where('email', 'LIKE', '%'.$email.'%');
    }
}
