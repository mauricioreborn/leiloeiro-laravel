<?php

namespace App\Client\Http\Factories\Filters;

use App\ClientCompanies\ClientCompany;
use App\Core\Contracts\FilterInterface;

class Account_balance extends AbstractFilter implements FilterInterface
{
    public function run($query, $value)
    {
        return $query->whereHas('companies', function ($query) use ($value) {
            return $query->where('client_companies.account_balance', 'LIKE', '%'.$value.'%');
        });
    }
}
