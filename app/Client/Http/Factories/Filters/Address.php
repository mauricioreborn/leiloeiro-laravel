<?php

namespace App\Client\Http\Factories\Filters;

use App\ClientCompanies\ClientCompany;
use App\Core\Contracts\FilterInterface;

class Address extends AbstractFilter implements FilterInterface
{
    public function run($query, $address)
    {
        return $query->whereHas('companies', function ($query) use ($address) {
            return $query->where('client_companies.address', 'LIKE', '%'.$address.'%');
        });
    }
}
