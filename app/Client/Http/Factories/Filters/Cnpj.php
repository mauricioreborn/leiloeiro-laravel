<?php

namespace App\Client\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Cnpj extends AbstractFilter implements FilterInterface
{
    public function run($query, $cnpj)
    {
        $cnpj = sanitize_number($cnpj);
        return $query->where('cnpj', 'LIKE', "%{$cnpj}%");
    }
}
