<?php

namespace App\Client\Http\Factories\Filters;

use App\ClientCompanies\ClientCompany;
use App\Core\Contracts\FilterInterface;

class State extends AbstractFilter implements FilterInterface
{
    public function run($query, $state)
    {
        return $query->whereHas('companies', function ($query) use($state) {
            return $query->where('client_companies.state', $state);
        });
    }
}
