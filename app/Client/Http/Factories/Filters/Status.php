<?php

namespace App\Client\Http\Factories\Filters;

use App\ClientCompanies\ClientCompany;
use App\Core\Contracts\FilterInterface;

class Status extends AbstractFilter implements FilterInterface
{
    public function run($query, $status)
    {
        return $query->whereHas('companies', function ($query) use ($status) {
            return $query->where('client_companies.status', (int) $status);
        });
    }
}
