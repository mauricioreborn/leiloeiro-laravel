<?php

namespace App\Client\Http\Factories\Filters;

use App\ClientCompanies\ClientCompany;
use App\Core\Contracts\FilterInterface;

class City extends AbstractFilter implements FilterInterface
{

    public function run($query, $city)
    {
        return $query->whereHas('companies', function ($query) use ($city) {
            return $query->where('client_companies.city', 'LIKE', '%'.$city.'%');
        });
    }
}
