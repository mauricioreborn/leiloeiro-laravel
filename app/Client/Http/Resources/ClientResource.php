<?php

namespace App\Client\Http\Resources;

use App\LeadTime\Http\Resources\LeadTimeResource;
use App\Order\Http\Resources\OrderResource;
use App\Payment\Http\Resources\CompanyPaymentResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Client\Http\Resources\SellerResource;
use App\Company\Http\Resources\CompanyResource;

class ClientResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'client',
            'id' => $this->id,
            'attributes' => [
                'cnpj' => $this->cnpj,
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'verified_mail' => (int) $this->verified_mail,
                'created_at' => $this->created_at->toDateTimeString(),
                'register_updated' => $this->register_updated,
            ],
            'relationships' => [
                'sellers' => SellerResource::collection($this->whenLoaded('seller')),
                'orders'  => OrderResource::collection($this->whenLoaded('order')),
                'origins' => LeadTimeResource::collection($this->whenLoaded('origins')),
                'companies' => CompanyResource::collection($this->whenLoaded('companies')),
                'companyPayments' => CompanyPaymentResource::collection($this->whenLoaded('companyPayments')),
            ],
        ];
    }
}
