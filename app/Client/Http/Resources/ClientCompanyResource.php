<?php

namespace App\Client\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientCompanyResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'client_companies',
            'id' => $this->id,
            'attributes' => [
                'client_id' => $this->client_id,
                'client_code' => $this->client_code,
                'company_id' => $this->company_id,
                'status' => $this->status,
                'account_balance' => $this->account_balance,
                'channel' => $this->channel,
                'min_order' => $this->min_order,
                'address' => $this->address,
                'state' => $this->state,
                'city' => $this->city,
                'track_1' => $this->track_1,
                'track_2' => $this->track_2,
                'track_3' => $this->track_3,
                'track_4' => $this->track_4,
                'typology' => $this->typology,
                'typology_code' => $this->typology_code,
            ],
        ];
    }
}
