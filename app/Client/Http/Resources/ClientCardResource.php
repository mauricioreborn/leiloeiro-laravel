<?php

namespace App\Client\Http\Resources;

use App\Client\Http\Resources\ClientResource;
use App\Core\Http\Resources\StatusResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientCardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'client_card',
            'id' => $this->id,
            'attributes' => [
                'payment_method_id' => $this->payment_method_id,
                'description' => $this->description,
                'holder_name' => $this->holder_name,
                'display_number' => $this->display_number,
                'brand' => $this->brand,
                'bin' => $this->bin,
                'month' => $this->month,
                'year' => $this->year,
                'cc_accepted_in' => $this->cc_accepted_in,
                'default' => $this->default,
                'image' => $this->image,
                'enable' => $this->enable,
                'label' => $this->label,
            ],
            'relationships' => [
                'client' => new ClientResource($this->whenLoaded('client')),
                'status' => new StatusResource($this->whenLoaded('status')),
                'quotas' => $this->when($this->quotas, function () {
                    return $this->quotas;
                }),
            ],
        ];
    }
}
