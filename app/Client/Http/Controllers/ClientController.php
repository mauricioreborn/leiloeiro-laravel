<?php


namespace App\Client\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client\Services\ClientService;
use App\Client\Http\Resources\ClientResource;
use App\Client\Http\Requests\ClientRequest;
use App\Company\Company;

class ClientController extends Controller
{
    protected $clientService;

    /**
     * Set Client Service
     *
     * ClientController constructor.
     *
     * @param ClientService $clientService Used to set the client service
     */
    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * Method to list a client
     *
     * ClientController constructor.
     *
     * @param Request $request used to search a client
     *
     * @return collection with a specifically user
     */
    public function list(Request $request)
    {
        $company = (auth()->user()) ? auth()->user()->company : Company::find(1);

        $collection = $this->clientService->search($request->all(), $company);

        return ClientResource::collection($collection);
    }

    /**
     * Method to save a client
     *
     * ClientController constructor.
     *
     * @param ClientRequest $request used to create a client
     *
     * @return array with a specifically response
     */
    public function store(ClientRequest $request)
    {
        $company = (auth()->user()) ? auth()->user()->company : Company::find(1);

        $response = new ClientResource(
            $this->clientService->createClient($request->except('company_id', 'client_id'), $company)
        );

        return response(['data' => $response], 201);
    }

    /**
     * Method to show a client
     *
     * ClientController constructor.
     *
     * @param integer $id      used to search a client
     * @param Company $company company
     *
     * @return ClientResource with a specifically response
     */
    public function show($id, Company $company)
    {
        $company = (auth()->user()) ? auth()->user()->company : Company::find(1);

        return new ClientResource($this->clientService->show($id, $company));
    }

    /**
     * Method to update a client
     *
     * @param int           $id      identifier of client
     * @param ClientRequest $request params to updated
     *
     * @return ClientResource with total results
     */
    public function update($id, ClientRequest $request)
    {
        $company = (auth()->user()) ? auth()->user()->company : Company::find(1);

        $this->clientService->updateClient($id, $request->except('company_id', 'client_id', 'cnpj'), $company);

        return new ClientResource($this->clientService->show($id, $company));
    }

    /**
     * Method to delete a client
     *
     * ClientController constructor.
     *
     * @param integer $id used to search a client
     *
     * @return array with a specifically response
     */
    public function destroy($id)
    {
        $company = (auth()->user()) ? auth()->user()->company : Company::find(1);

        $this->clientService->destroy($id, $company);

        return response()->json([], 204);
    }
}
