<?php

namespace App\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to prepare for validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj)
            ]
        ));
    }

    /**
     * Method to validate params
     * @param Request $request request
     * @return array
     */
    public function rules(Request $request)
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'cnpj' => 'required|cnpj|unique:clientes,cnpj',
                    'password' => 'required',
                    'name' => 'required|string',
                    'status' => 'required|integer|max:1',
                    'account_balance' => 'required|numeric',
                    'min_order' => 'required|numeric|min: 0.1',
                    'address' => 'required|string',
                    'state' => 'required|string|max:2',
                    'city' => 'required|string',
                    'email' => 'nullable|email',
                    'phone' => 'nullable',
                    'typology' => 'nullable|string',
                    'typology_code' => 'integer',
                    'track_1' => 'integer',
                    'track_2' => 'integer',
                    'track_3' => 'integer',
                    'track_4' => 'integer',
                    'seller.*.name' => 'string',
                    'seller.*.email' => 'required|email',
                    'seller.*.phone' => 'nullable',
                ];

            case 'PUT':
                return [
                    'cnpj' => 'cnpj|unique:clientes,cnpj,'.$request->id,
                    'name' => 'string',
                    'status' => 'nullable|integer|max:1',
                    'account_balance' => 'numeric',
                    'min_order' => 'numeric|min: 0.1',
                    'address' => 'string',
                    'state' => 'string|max:2',
                    'city' => 'string|max:120',
                    'email' => 'nullable|email',
                    'typology' => 'nullable|string',
                    'typology_code' => 'integer',
                    'track_1' => 'integer',
                    'track_2' => 'integer',
                    'track_3' => 'integer',
                    'track_4' => 'integer',
                    'seller.*.name' => 'nullable|string',
                    'seller.*.email' => 'required|email',
                    'seller.*.phone' => 'nullable',
                ];
        }
    }

    /**
     * Method to change messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cnpj' => 'The :attribute is a invalid format'
        ];
    }
}
