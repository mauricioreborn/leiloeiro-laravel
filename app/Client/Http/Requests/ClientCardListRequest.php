<?php

namespace App\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ClientCardListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to validate params
     * @param Request $request request
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'quotaPrice' => 'numeric',
        ];
    }
}
