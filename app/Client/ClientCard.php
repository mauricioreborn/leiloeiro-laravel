<?php

namespace App\Client;

use App\Core\Classes\Transactible;
use App\Core\Entities\Status;
use App\Invoice\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class ClientCard extends Model
{
    use Transactible, SoftDeletes;

    protected $fillable = [
        'status_id',
        'payment_method_id',
        'client_id',
        'description',
        'holder_name',
        'display_number',
        'brand',
        'bin',
        'month',
        'year',
        'default',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'default' => 'boolean',
    ];

    protected $appends = [
        'enable',
    ];

    /**
    * client Relationship
    * @return Client
    **/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
    * status relationship
    * @return Client
    **/
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
    * invoice relationship
    * @return Client
    **/
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * scope to Authorized status
     *
     * @param Builder $builder Builder
     *
     * @return Builder
     */
    public function scopeToAuthorized($builder)
    {
        $this->status_id = Status::type(Status::AUTHORIZED)->id;

        return $this;
    }

    /**
     * scope to Authorized status
     *
     * @param Builder $builder Builder
     *
     * @return Builder
     */
    public function scopeToRejected($builder)
    {
        $this->status_id = Status::type(Status::REJECTED)->id;

        return $this;
    }

    /**
     * scope to Authorized status
     *
     * @param Builder $builder Builder
     *
     * @return Builder
     */
    public function getEnableAttribute(): bool
    {
        return (bool) ($this->status_id !== Status::type(Status::REJECTED)->id);
    }

    /**
     * get Branch image url
     *
     * @return String
     */
    public function getImageAttribute(): string
    {
        $brand = str_replace("'", "", $this->brand);
        $brand = str_replace(" ", "", $brand);
        $brand = strtolower($brand);

        return env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/payment/brands/'.$brand.'.png';
    }

    public function getLabelAttribute()
    {
        return $this->enable ? false : __('Payment/clientCard.rejected');
    }
}
