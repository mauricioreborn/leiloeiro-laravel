<?php

namespace App\Client;

use App\Core\Classes\Transactible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientTirolez extends Model
{
    use Transactible, SoftDeletes;

    const TYPE = [
        'base_1' => 1,
        'base_2' => 2
    ];

    protected $table = 'client_tirolez';

    protected $dates = [
        'last_updated_at',
        'banner_start_at',
        'banner_finish_at',
    ];

    protected $fillable = [
        'client_id',
        'type',
        'modal_view_counter',
        'last_updated_at',
        'banner_start_at',
        'banner_finish_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
    * client Relationship
    * @return Client
    **/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
