<?php
namespace App\Client;

use App\Core\Helpers\SanitizeHelper;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class SellerClient extends Model
{
    use Eloquence, Mappable;

    protected $table = 'cliente_vendedor';

    protected $fillable = [
        'cliente_id',
        'vendedor_id',

        //en
        'client_id',
        'seller_id',
    ];

    protected $hidden = [
        'cliente_id',
        'vendedor_id',
    ];

    protected $appends = [
        'client_id',
        'seller_id',
    ];

    public $maps = [
        'seller_id' => 'vendedor_id',
        'client_id' => 'cliente_id',
    ];


    /**
     * Set Clients relation
     * @return App\Client\Client
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_id')->withTimestamps();
    }

    /**
     * Set Company relation
     * @return App\Company\Company
     */
    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id');
    }
}