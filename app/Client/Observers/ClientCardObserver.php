<?php

namespace App\Notification\Observers;

use App\Client\ClientCard;
use App\Client\Services\ClientCardService;
use App\Core\Entities\Status;
use App\Invoice\Services\InvoiceService;

class ClientCardObserver
{
    /**
     * Handle the ClientCard "created" event.
     *
     * @param ClientCard $clientCard ClientCard entity
     *
     * @return void
     */
    public function created(ClientCard $clientCard)
    {
        $pending = Status::type(Status::PENDING);

        if ($clientCard->status_id === $pending->id) {
            (new InvoiceService)->createAuthorization($clientCard);
        }

        (new ClientCardService())->setDefault($clientCard);
    }
}
