<?php

namespace App\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company\CompanyPayment;

class ClientCompanyPayment extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'client_id',
      'company_payment_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

  /**
   * client Relationship
   * @return Client
   **/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

  /**
   * client Relationship
   * @return Client
   **/
    public function companyPayment()
    {
        return $this->belongsTo(CompanyPayment::class);
    }
}
