<?php

namespace App\Client\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LoggableMail extends Mailable
{
    use SerializesModels;

    public function __construct(string $view, array $viewData, string $subject)
    {
        $this->view = $view;
        $this->viewData = $viewData;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->view)
            ->with($this->viewData)
            ->subject($this->subject);
    }
}
