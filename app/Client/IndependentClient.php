<?php


namespace App\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class IndependentClient extends Model
{
    use Eloquence, Mappable, Notifiable;

    protected $table = 'clientes_independentes';

    protected $fillable = [
        'cnpj',
        'state_registration',
        'password',
        'nome',
        'contact_name',
        'rede',
        'status',
        'saldo',
        'endereco',
        'uf',
        'municipio',
        'email',
        'telefone',
        'integrado',
        'zipcode',
        'address_number',
        'neighborhood',
        'address_complement',
        'social_reason',
        'registration_ip',
        'terms_of_use_version',
        'privacy_policy_version',

        //en
        'name',
        'chain',
        'account_balance',
        'address',
        'state',
        'city',
        'phone',
    ];

    protected $hidden = [
        'password',
        'nome',
        'rede',
        'status',
        'saldo',
        'endereco',
        'uf',
        'municipio',
        'email',
        'telefone',
    ];

    protected $appends  = [
        'name',
        'chain',
        'account_balance',
        'address',
        'state',
        'city',
        'phone',
    ];

    public    $maps     = [
        'name' => 'nome',
        'chain' => 'rede',
        'account_balance' => 'saldo',
        'address' => 'endereco',
        'state' => 'uf',
        'city' => 'municipio',
        'phone' => 'telefone',
    ];

    protected $casts    = [
        'saldo'         => 'float',
    ];

    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = sanitize_number($value);
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['telefone'] = sanitize_number($value);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = md5($value);
    }

    public function order()
    {
        return $this->hasMany(Order::class, 'id_cliente');
    }

    public function seller()
    {
        return $this->belongsToMany(Seller::class, 'cliente_vendedor', 'cliente_id', 'vendedor_id')->withTimestamps();
    }

    public function origins()
    {
        return $this->hasMany(LeadTime::class, 'cnpj', 'cnpj');
    }

    public function confirmations()
    {
        return $this->hasMany(ClientConfirmation::class, 'independent_client_id');
    }
}