<?php

namespace App\Client;

use App\Client\Client;
use App\Company\Company;
use App\Core\Helpers\SanitizeHelper;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class Seller extends Model
{
    use Eloquence, Mappable;

    protected $table = 'vendedores';

    protected $fillable = [
        'nome',
        'email',
        'telefone',
        'company_id',

        //en
        'name',
        'phone',
    ];

    protected $hidden = [
        'nome',
        'telefone',
    ];

    protected $appends = [
        'name',
        'phone',
    ];

    public $maps = [
        'name' => 'nome',
        'phone' => 'telefone',
    ];

    /**
     * Set phone attribute
     * @param value $value Phone
     * @return void
     */
    public function setTelefoneAttribute($value)
    {
        $this->attributes['telefone'] = sanitize_number($value);
    }

    /**
     * Set Clients relation
     * @return App\Client\Client
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'cliente_vendedor')->withTimestamps();
    }

    /**
     * Set Company relation
     * @return App\Company\Company
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
