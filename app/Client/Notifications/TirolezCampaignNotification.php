<?php

namespace App\Client\Notifications;

use App\Client\Mail\LoggableMail;
use App\Core\Channels\SmsChannel;
use App\Core\Messages\SmsMessage;
use App\Log\LogEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Notifications\Notification;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Log;

class TirolezCampaignNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $tries = 3;
    protected $channel;
    protected $payload;

    /**
     * @param mixed $channel default channel
     * @return void
     */
    public function __construct($payload = null, $channel = null)
    {
        $this->channel = ($channel) ? [$channel] : [SmsChannel::class];
        $this->payload = $payload;
    }

    /**
     * @param mixed $notifiable Notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->channel;
    }

    /**
     * @param mixed $notifiable Notifiable
     * @return SmsMessage
     */
    public function toSms($notifiable)
    {
        return new SmsMessage(__($this->payload['messageKey']));
    }

    /**
     * @param mixed $notifiable Notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $email = (new LoggableMail($this->payload['view'], $this->payload['viewData'], $this->payload['subject']))
            ->to($notifiable->email);

        (new LogEmail())->create([
            'page' => 'Emails Campanha Tirolez',
            'subject' => $this->payload['subject'],
            'message' => $email->build()->render(),
            'md5_mensagem' => $this->payload['md5_mensagem'],
            'emails' =>  $notifiable->email,
        ]);

        return $email;
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        Log::error($exception);
    }
}
