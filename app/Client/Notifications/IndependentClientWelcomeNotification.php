<?php

namespace App\Client\Notifications;

use App\Log\LogEmail;
use App\Mobile\Mail\IndependentClientWelcomeMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class IndependentClientWelcomeNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $params;

    /**
     * Create a new notification instance.
     *
     * @param  array $param Params to send notify
     *
     * @return void
     */
    public function __construct(array $params = [])
    {
        $this->params = $params;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return IndependentClientWelcomeMail
     */
    public function toMail($notifiable)
    {
        $email = new IndependentClientWelcomeMail($this->params);

        (new LogEmail())->create([
            'page' => 'Cadastro de Cliente',
            'subject' => 'Contato Souk | Cadastro realizado',
            'message' => $email->build()->render(),
            'emails' =>  array_get($this->params, 'email')
        ]);

        return $email;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
