<?php
namespace App\Client\Notifications;

use App\Client\Mail\LoggableMail;
use App\Log\LogEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class TransactionalEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $tries = 2;

    protected $view;
    protected $viewData;
    protected $subject;

    public function __construct(string $view, array $viewData, string $subject) {
        $this->view = $view;
        $this->viewData = $viewData;
        $this->subject = $subject;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $email = (new LoggableMail($this->view, $this->viewData, "Contato Souk | {$this->subject}"))
            ->to($notifiable->email);

        $email_md5 = md5($email->build()->render());

        (new LogEmail())->create([
            'page' => 'Emails transacionais',
            'subject' => "Contato Souk | {$this->subject}",
            'message' => $email->build()->render(),
            'md5_mensagem' => $email_md5,
            'emails' =>  $notifiable->email,
        ]);

        return $email;
    }

    public function failed(\Exception $exception)
    {
        Log::error($exception);
    }
}
