<?php

namespace App\Client\Notifications;

use App\Core\Channels\SmsChannel;
use App\Core\Messages\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Notifications\Notification;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Log;

class MobileConfirmationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $tries = 3;

    protected $channel;

    /**
     * @param mixed $channel default channel
     * @return void
     */
    public function __construct($channel = null)
    {
        $this->channel = ($channel) ? [$channel] : [SmsChannel::class];
    }

    /**
     * @param mixed $notifiable Notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->channel;
    }

    /**
     * @param mixed $notifiable Notifiable
     * @return SmsMessage
     */
    public function toSms($notifiable)
    {
        return new SmsMessage(__('Mobile/clientConfirmation.sms.notification', ['code' => $notifiable->code]));
    }

    /**
     * @param mixed $notifiable Notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $now = now();
        $url = config('app.url').'/api/v1/public/clients/confirm_mail/?code='.$notifiable->code;

        $viewData = [
            'name' => $notifiable->client->name,
            'url' => $url,
        ];

        $send = (new MailMessage())
            ->bcc('renato@souk.com.br')
            ->view('mail.email_confirmation', $viewData)
            ->subject('Contato Souk | Confirmação de email');

        return $send;
    }



    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        Log::error($exception);
    }
}
