<?php

namespace App\Client\Traits;

use App\Core\Entities\Status;

/**
 * Trait ConfirmationSteps
 * @package App\Client\Traits
 */
trait ConfirmationSteps
{

    /**
     * @return bool
     */
    public function isStepOne()
    {
        return is_null($this->confirm_sms) && is_null($this->confirm_email);
    }

    /**
     * @return bool
     */
    public function isStepTwo()
    {
        return $this->confirm_sms == Status::type(Status::PENDING)->id;
    }

    /**
     * @return bool
     */
    public function isStepThree()
    {
        return $this->confirm_sms == Status::type(Status::AUTHORIZED)->id && is_null($this->confirm_email);
    }

    /**
     * @return bool
     */
    public function isStepFour()
    {
        return $this->confirm_email == Status::type(Status::PENDING)->id;
    }
}