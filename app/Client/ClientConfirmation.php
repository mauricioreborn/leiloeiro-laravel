<?php
namespace App\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class ClientConfirmation extends Model
{
    use SoftDeletes, Notifiable;

    const TYPES = [
        'sms' => 'sms',
        'mail' => 'mail'
    ];

    protected $fillable = [
        'client_id',
        'type',
        'value',
        'code',
        'confirmed_at'
    ];

    protected $dates = [
        'confirmed_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return string
     */
    public function routeNotificationForSms()
    {
        return "+55{$this->value}";
    }

    /**
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->value;
    }

    /**
     * @param Builder $query query
     * @return string
     */
    public function scopeIsSms($query)
    {
        return $query->where('type', $this::TYPES['sms']);
    }

    public function independentClient()
    {
        return $this->belongsTo(IndependentClient::class, 'independent_client_id');
    }
}
