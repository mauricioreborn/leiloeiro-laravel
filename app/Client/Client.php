<?php

namespace App\Client;

use App\Bid\Bid;
use App\Client\ClientTirolez;
use App\ClientCompanies\ClientCompany;
use App\Client\Traits\ConfirmationSteps;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Fefo\FefoPriceBoost;
use App\Iugu\IuguClient;
use App\LeadTime\LeadTime;
use App\Order\Order;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class Client extends Model implements Authenticatable
{
    use Eloquence, Mappable, AuthenticableTrait, ConfirmationSteps, Notifiable;

    protected $table = 'clientes';

    protected $connection ='mysql';

    protected $fillable = [
        'cnpj',
        'state_registration',
        'password',
        'nome',
        'contact_name',
        'email',
        'telefone',
        'email_verified',
        'register_updated',
        'confirm_sms',
        'confirm_email',
        'registration_ip',
        'terms_of_use_version',
        'privacy_policy_version',

        //en
        'name',
        'phone',
        'verified_mail',
    ];

    protected $hidden = [
        'password',
        'nome',
        'telefone',
    ];

    protected $appends = [
        'name',
        'phone',
        'verified_mail',
    ];

    public $maps = [
        'name' => 'nome',
        'phone' => 'telefone',
        'verified_mail' => 'email_verified',
    ];

    protected $casts = [
        'register_updated' => 'array'
    ];

    /**
     * Method to set a Cnpj attribute
     *
     * @param string $value cnpj value
     *
     * @return null
     */
    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = sanitize_number($value);
    }

    /**
     * Method to set a telefone attribute
     *
     * @param string $value telefone
     *
     * @return null
     */
    public function setTelefoneAttribute($value)
    {
        $this->attributes['telefone'] = sanitize_number($value);
    }

    /**
     * Method to set a Password attribute
     *
     * @param string $value password
     *
     * @return null
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = md5($value);
    }

    /**
     * Method to relates the order class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order()
    {
        return $this->hasMany(Order::class, 'id_cliente');
    }

    /**
     * Method to relates the Seller class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function seller()
    {
        return $this->belongsToMany(Seller::class, 'cliente_vendedor', 'cliente_id', 'vendedor_id')->withTimestamps();
    }

    /**
     * Method to relates the leadtime class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function origins()
    {
        return $this->hasMany(LeadTime::class, 'cnpj', 'cnpj');
    }

    /**
     * Method to relates the bids class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bids()
    {
        return $this->hasMany(Bid::class, 'id_cliente');
    }

    /**
     * Method to relates the bids class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fefoPriceBoost()
    {
        return $this->hasMany(FefoPriceBoost::class, 'client_id');
    }

    /**
     * iuguClient relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function iuguClient()
    {
        return $this->hasOne(IuguClient::class);
    }

    /**
     * Method to relates  a Company class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function companies()
    {
        return $this->belongsToMany(Company::class, 'client_companies')
            ->as('clientCompany')
            ->withPivot((new ClientCompany())->getFillable())
            ->whereNull('client_companies.deleted_at')
            ->using(ClientCompany::class);
    }

    /**
     * ClientCompanyPayment relationships
     *
     * @return ClientCompanyPayment
     */
    public function clientCompanyPayments()
    {
        return $this->hasMany(ClientCompanyPayment::class);
    }

    /**
     * Method to get company from payments
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function companyPayments()
    {
        return $this->belongsToMany(CompanyPayment::class, 'client_company_payments')
            ->whereNull('client_company_payments.deleted_at')
            ->withTimestamps();
    }

    /**
     * ClientCard relationships
     *
     * @return ClientCard
     */
    public function clientCards()
    {
        return $this->hasMany(ClientCard::class);
    }

    /**
    * client Relationship
    * @return Client
    **/
    public function clientSearches()
    {
        return $this->hasMany(ClientSearch::class);
    }

    /**
     * @param Builder $builder   Query builder
     * @param integer $companyId Company id
     * @return mixed
     */
    public function scopeFromCompany($builder, $companyId)
    {
        return $builder
            ->with([
                'companies' => function ($query) use ($companyId) {
                    $query->where('companies.id', '=', $companyId);
                }
            ])->whereHas('companies', function ($query) use ($companyId) {
                $query->where('companies.id', '=', $companyId);
            });
    }

    /**
     * @param Builder $builder   Query builder
     * @param integer $companyId Company id
     * @return mixed
     */
    public function scopeFromCompanyPayments($builder, $companyId)
    {
        return $builder
            ->with([
                'companyPayments' => function ($query) use ($companyId) {
                    $query->where('company_payments.company_id', '=', $companyId);
                }
            ]);
    }

    /**
     * Get client company from company entity
     *
     * @param Company $company Company entity in companies relationships
     *
     * @return ClientCompany|null
     */
    public function setClientCompany(Company $company)
    {
        $company = $this->companies->find($company);

        if ($company) {
            return $company->clientCompany;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function confirmations()
    {
        return $this->hasMany(ClientConfirmation::class, 'client_id');
    }

    /**
    * client Relationship
    * @return Client
    **/
    public function clientTirolez()
    {
        return $this->hasOne(ClientTirolez::class);
    }

    public function routeNotificationForSms()
    {
        return "+55{$this->phone}";
    }
}
