<?php

namespace App\Client\Providers;

use App\Client\ClientCard;
use App\Core\Policies\ClientCardPolicy;
use App\Notification\Observers\ClientCardObserver;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;

class ClientServiceProvider extends ServiceProvider
{

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Client\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        ClientCard::observe(ClientCardObserver::class);

        Gate::policy(ClientCard::class, ClientCardPolicy::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware(['api', 'validateJwt', 'sentry'])
            ->namespace($this->namespace)
            ->group(base_path('app/Client/Http/routes.php'));
    }
}
