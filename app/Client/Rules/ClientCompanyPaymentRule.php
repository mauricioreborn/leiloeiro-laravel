<?php

namespace App\Client\Rules;

use App\Client\ClientCompanyPayment;
use Illuminate\Contracts\Validation\Rule;

class ClientCompanyPaymentRule implements Rule
{
    protected $companyId;

    protected $clientId;

    /**
     * Create a new rule instance.
     *
     * @param string $companyId Company ID
     * @param string $clientId  Client ID
     *
     * @return void
     */
    public function __construct($companyId, $clientId)
    {
        $this->companyId = $companyId;
        $this->clientId = $clientId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute Attribute name
     * @param mixed  $value     Value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $clientCompanyPayment = ClientCompanyPayment::with('companyPayment')
            ->where('id', $value)
            ->where('client_id', $this->clientId)
            ->first();

        if (!$clientCompanyPayment) {
            return false;
        }

        return $clientCompanyPayment->companyPayment->company_id == $this->companyId;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Payment/errors.client_company_payment_invalid');
    }
}
