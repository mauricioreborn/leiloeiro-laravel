<?php

namespace App\Client\Rules;

use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Payment\Payment;
use Illuminate\Contracts\Validation\Rule;

class ClientCardRule implements Rule
{
    protected $clientCompanyPaymentId;

    protected $clientId;

    /**
     * Create a new rule instance.
     *
     * @param string $clientCompanyPaymentId Client Company Payment ID
     * @param string $clientId               Client ID
     *
     * @return void
     */
    public function __construct($clientCompanyPaymentId, $clientId)
    {
        $this->clientCompanyPaymentId = $clientCompanyPaymentId;
        $this->clientId = $clientId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute Attribute name
     * @param mixed  $value     Value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!$this->clientCompanyPaymentId || !$value) {
            return true;
        }

        if ($this->clientCompanyPaymentId) {
            $clientCompanyPayment = ClientCompanyPayment::with('companyPayment.payment')
                ->find($this->clientCompanyPaymentId);

            if ($clientCompanyPayment) {
                $paymentMethod = $clientCompanyPayment->companyPayment->payment->type;

                if ($paymentMethod == Payment::CREDIT_CARD) {
                    $clientCard = ClientCard::where('client_id', $this->clientId)->where('id', $value)->first();

                    return $clientCard && $clientCard->enable;
                }
            }
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Payment/errors.credit_card');
    }
}
