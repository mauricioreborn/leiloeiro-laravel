<?php

namespace App\Client\Services;

use App\Client\Client;
use App\Company\Company;

class ClientSearchService
{
    /**
     * Create client card
     *
     * @param Client $client Client entity
     * @param string $name   Name of card
     * @param string $token  Token IUGU
     *
     * @return ClientCard|boolean
     **/
    public function createClientSearch(Company $company, Client $client, string $search, array $response)
    {
        return $client->clientSearches()->create([
            'company_id' => $company->id,
            'search' => $search,
            'response' => $response
        ]);
    }
}
