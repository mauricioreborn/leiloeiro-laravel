<?php

namespace App\Client\Services;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\Seller;
use App\Company\Company;
use App\Core\Classes\CrudService;
use App\Core\Classes\Search;
use App\Order\OrderProduct;
use Illuminate\Support\Facades\DB;

class ClientService extends CrudService
{
    protected $entity = Client::class;

    protected $module = 'Client';

    /**
     * @param array   $filters filters
     * @param Company $company company
     * @return mixed
     */
    public function search($filters, Company $company = null)
    {
        $searchInstance = new Search($this->module, $this->newQuery());
        $query = $searchInstance->run($filters);
        $query->fromCompany($company->id);
        $query->fromCompanyPayments($company->id);
        return $query->paginate($filters['limit'] ?? 15);
    }

    /**
     * Method to insert a new client
     * @param array   $clientFields client fields
     * @param Company $company      company
     * @return mixed
     */
    public function createClient($clientFields, Company $company)
    {
        $clientModel = new Client();
        $sellerModel = new Seller();

        $client = $clientModel->create($clientFields);

        $client->companies()->attach($company->id, $clientFields);

        if (isset($clientFields['seller'])) {
            $this->insertOrUpdateSeller($client, $sellerModel, $clientFields);
        }

        return $this->show($client->id, $company);
    }

    /**
     * Method to get a client by id
     * @param int     $id      client identifier
     * @param Company $company company
     * @return mixed
     */
    public function show($id, Company $company)
    {
        $client = new Client();

        $client = $client->with('seller', 'companyPayments.payment')->fromCompany($company->id)->findOrFail($id);

        return $client;
    }

    /**
     * Method to update a client
     * @param int     $clientId     client identifier
     * @param array   $clientFields fields to update
     * @param Company $company      company
     * @return mixed
     */
    public function updateClient($clientId, array $clientFields = [], Company $company): bool
    {
        $clientModel = new Client();
        $sellerModel = new Seller();

        $client = $clientModel->findOrFail($clientId);

        $client->update($clientFields);

        $client->companies()->updateExistingPivot($company->id, $clientFields);

        if (isset($clientFields['seller'])) {
            $this->insertOrUpdateSeller($client, $sellerModel, $clientFields);
        }

        return true;
    }

    /**
     * Method to remove a client
     * @param int     $id      client id
     * @param Company $company company
     * @return bool
     */
    public function destroy($id, Company $company)
    {
        $client = $this->find($id);

        $client->companies()->updateExistingPivot($company->id, ['status' => 0]);

        return $this->save($client);
    }

    /**
     * Method to Insert or update a Seller, syncing pivot table
     * @param object $clientAction (update or insert)
     * @param object $sellerModel  model of seller
     * @param array  $clientFields client parameters
     *
     * @return void
     **/
    protected function insertOrUpdateSeller($clientAction, $sellerModel, $clientFields)
    {
        $sellers_id = [];

        foreach ($clientFields['seller'] as $seller) {

            $checkIfSellerExists = $sellerModel->where('email', $seller['email'])->first();

            if($checkIfSellerExists) {

                $sellerModel->findOrFail($checkIfSellerExists->id)->update($seller);

                $sellers_id[] = $checkIfSellerExists->id;

            } else {

                $newClientSeller = $clientAction->seller()->create($seller);

                $sellers_id[] = $newClientSeller->id;
            }
        }

        $clientAction->seller()->sync($sellers_id);
    }

    /**
     * Method to check if an client should be updated
     * @param Company $company company object
     * @param Client  $client  client object
     * @return array
     */
    public function getRegisterUpdate(Company $company, Client $client)
    {
        $registerUpdated = false;
        $waitingApproval = false;

            if ($client->register_updated === false) {
                $registerUpdated = true;
            }

            if ($client->companies()->where(['company_id' => $company->id,
            'status' => 0])->first()) {
            $waitingApproval = true;
            }

        return [
            'registerUpdated' => $registerUpdated,
            'waitingApproval' => $waitingApproval
        ];

    }

    /**
     * decrement account balance value
     *
     * @param Company $company   Company entity
     * @param Client  $client    Client entity
     * @param float   $decrement Value to decrement
     *
     * @return ClientCompany
     */
    public function decrementAccountBalance(Company $company, Client $client, float $decrement): ClientCompany
    {
        $clientCompany = $client->setClientCompany($company);

        $clientCompany->decrement('account_balance', $decrement);

        return $clientCompany;
    }

    /**
     * Method to get product categories has Bought
     * @param Client $client   client object
     * @param Company $company company object
     * @return OrderProduct
     */
    public function getProductCategoryBought(Client $client, Company $company)
    {
        return
            OrderProduct::selectRaw(
                'COUNT(pedidos_produtos.produto_id) as buy_amount, produtos.area, pedidos_produtos.*'
            )
                ->join('pedidos', 'pedidos.id', '=', 'pedidos_produtos.pedido_id')
                ->join('produtos', 'produtos.codigo', 'pedidos_produtos.produto_id')
                ->where('pedidos.company_id', $company->id)
                ->where('pedidos.id_cliente',$client->id)
                ->where('produtos.company_id',$company->id)
                ->groupBy('pedidos_produtos.produto_id')
                ->orderBy('buy_amount', 'DESC')
                ->orderBy('pedidos_produtos.created_at', 'DESC')
                ->get();
    }

    /**
     * @param Client  $client Client
     * @param Company $company Company
     * @return boolean
     */
    public function hasAlreadyBought(Company $company, Client $client)
    {
        $clientCompany = $client->setClientCompany($company);

        if($clientCompany) {
            return $clientCompany->first_bid_at || $clientCompany->first_cart_at;
        }

        return false;
    }
}
