<?php

namespace App\Client\Services;

use App\Client\Client;
use App\Client\ClientCard;
use App\Client\Services\ClientCompanyPaymentService;
use App\Company\Services\CompanyPaymentService;
use App\Core\Entities\Status;
use App\Iugu\Services\IuguService;
use App\Mobile\Services\BidService;
use App\Mobile\Services\OrderService;
use Illuminate\Support\Collection;

class ClientCardService
{
    protected $iuguService;

    /**
     * Constructor
     **/
    public function __construct()
    {
        $this->iuguService = resolve(IuguService::class);
    }

    /**
     * Create client payment method
     *
     * @param Client $client     Client entity
     * @param float  $quotaPrice Amount
     *
     * @return object
     **/
    public function list(Client $client, $quotaPrice = null)
    {
        $clientCards = $client->clientCards()->with('status')->get();

        $companiesAcceptCreditCard = (new CompanyPaymentService())->companiesAcceptCreditCard($client);

        $clientCards->each(function ($clientCard) use ($companiesAcceptCreditCard) {
            return $clientCard->cc_accepted_in = trans_choice(
                'Payment/infos.cc_accepted_in',
                $companiesAcceptCreditCard->count(),
                [
                    'companies' => $companiesAcceptCreditCard->implode('name', ', ')
                ]
            );
        });

        if ($quotaPrice) {
            foreach ($clientCards as $clientCard) {
                $clientCard->quotas = $this->calculateQuotas($clientCard, $quotaPrice);
            }
        }

        return $clientCards;
    }

    /**
     * calculate quotas price of card
     *
     * @param  ClientCard $clientCard ClientCard
     * @param  float      $quotaPrice Amount
     *
     * @return Collection
     */
    protected function calculateQuotas(ClientCard $clientCard, $quotaPrice): Collection
    {
        $quota = 1;
        $maxQuotas = 12;

        $quotasTax = [
            'default' => [
                1 => 2.51,
                2 => 3.21,
                3 => 3.21,
                4 => 3.21,
                5 => 3.21,
                6 => 3.21,
                7 => 3.55,
                8 => 3.55,
                9 => 3.55,
                10 => 3.55,
                11 => 3.55,
                12 => 3.55
            ],
            'amex' => [
                1 => 4.60,
                2 => 4.60,
                3 => 4.60,
                4 => 4.60,
                5 => 4.60,
                6 => 4.60,
                7 => 4.60,
                8 => 4.60,
                9 => 4.60,
                10 => 4.60,
                11 => 4.60,
                12 => 4.60
            ]
        ];

        $quotas = collect();

        $cardType = ($clientCard->brand == 'American Express') ? 'amex' : 'default';

        $clientCompanyPaymentService = new ClientCompanyPaymentService();

        while ($quota <= $maxQuotas) {
            $totalPrice = $clientCompanyPaymentService->calculateTaxTotal($quotaPrice, $quotasTax[$cardType][$quota]);

            $price = formata_moeda(($totalPrice / $quota), false);

            $quotas->push([
                'quota' => $quota,
                'total' => number_format($totalPrice, 2),
                'tax' => $quotasTax[$cardType][$quota],
                'message' => __('Payment/clientCard.quota', [
                    'quota' => $quota,
                    'price' => $price,
                    'tax' => $quotasTax[$cardType][$quota],
                ])
            ]);

            $quota++;
        }

        return $quotas;
    }

    /**
     * Get quota value
     *
     * @param ClientCard $clientCard Client Card entity
     * @param float      $quotaPrice Price
     * @param integer    $quota      Quota number
     *
     * @return array
     */
    public function calculateQuotaValue(ClientCard $clientCard, float $quotaPrice, int $quota): array
    {
        return $this->calculateQuotas($clientCard, $quotaPrice)->first(function ($item) use ($quota) {
            return $item['quota'] === $quota;
        });
    }

    /**
     * Create client card
     *
     * @param Client $client Client entity
     * @param string $name   Name of card
     * @param string $token  Token IUGU
     *
     * @return ClientCard|boolean
     **/
    public function create(Client $client, string $name, string $token)
    {
        $response = $this->iuguService->createPaymentMethods($client, $name, $token, true);

        if ($response) {
            $displayNumber = explode('-', $response['data']['display_number']);

            return $client->clientCards()->create([
                'status_id' => Status::type(Status::PENDING)->id,
                'payment_method_id' => $response['id'],
                'description' => $response['description'],
                'holder_name' => $response['data']['holder_name'],
                'display_number' => end($displayNumber),
                'brand' => $response['data']['brand'],
                'bin' => $response['data']['bin'],
                'month' => $response['data']['month'],
                'year' => $response['data']['year'],
            ])->fresh(['status']);
        }

        return false;
    }

    /**
     * set default card of client, update others cards to default 0
     *
     * @param ClientCard $clientCard ClientCard entity
     *
     * @return App\Client\ClientCard
     */
    public function setDefault(ClientCard $clientCard): ClientCard
    {
        $clientCard->client->clientCards()->update(['default' => false]);

        $clientCard->default = true;
        $clientCard->save();

        return $clientCard;
    }

    public function delete(ClientCard $clientCard)
    {
        $fields = [
            'status' => 'pending',
            'client_card_id' => $clientCard->id
        ];

        $bids = (new BidService())->list($clientCard->client, $fields);

        $orders = (new OrderService())->getClientOrdersByStatus(
            $clientCard->client->id,
            Status::type(Status::PENDING),
            $fields
        );

        if (!$bids->count() && !$orders->count()) {
            $clientCard->delete();

            return $clientCard;
        }

        return false;
    }
}
