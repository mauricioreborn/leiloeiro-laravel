<?php

namespace App\Client\Services;

use App\Client\Client;
use App\Company\Company;

use Illuminate\Support\Collection;

class ClientTirolezService
{
    const ACTIVE_STATES = [
        'SP',
        'RJ',
    ];

    const MODAL_LIMIT = 3;

    public function hasReachedLimitViews(Client $client)
    {
        if ($client->clientTirolez) {
            if ($client->clientTirolez->modal_view_counter >= $this::MODAL_LIMIT) {
                return true;
            }
        }

        return false;
    }

    public function hasModalAlreadySeenToday(Client $client)
    {
        if ($client->clientTirolez) {
            if ($client->clientTirolez->last_updated_at->format('dmy') == now()->format('dmy')) {
                return true;
            }
        }

        return false;
    }

    public function isActiveCampaign(Client $client)
    {
        if($client->clientTirolez) {
            $start_at = $client->clientTirolez->banner_start_at;
            $finish_at = $client->clientTirolez->banner_finish_at;

            if(now() > $start_at && now() < $finish_at) {
                return true;
            }
        }

        return false;
    }

    public function getClientState(Company $company, Client $client)
    {
        $clientCompany = $client->setClientCompany($company);

        if($clientCompany){
            return $clientCompany->state;
        }
    }
}
