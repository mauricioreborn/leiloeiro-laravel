<?php

namespace App\Client\Services;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Company\Services\CompanyPaymentService;
use App\Core\Classes\CrudService;
use App\Core\Classes\Search;
use App\Mobile\Exceptions\MobileException;
use App\Payment\Payment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClientCompanyPaymentService
{
    protected $creditCardLimit = 2000;

    /**
     * Method to sync client company payments
     *
     * @param Client  $client          client object
     * @param Company $company         company object
     * @param array   $companyPayments company payments array
     *
     * @return array
     */
    public function syncCompanyPayments(Client $client, Company $company, array $companyPayments)
    {
        $attached = [];
        $detached = [];

        (new CompanyPaymentService())->checkCompanyClients($client, $company, $companyPayments);

        $payments = $company->companyPayments;

        $clientCompanyPayments = ClientCompanyPayment::withTrashed()
            ->where('client_id', $client->id)
            ->whereIn('company_payment_id', $payments->pluck('id'))
            ->get();

        foreach ($clientCompanyPayments as $clientCompanyPayment) {
            $enable = in_array($clientCompanyPayment->company_payment_id, $companyPayments);

            if (!$enable) {
                if (!$clientCompanyPayment->trashed()) {
                    $detached[] = $clientCompanyPayment->id;

                    $clientCompanyPayment->delete();
                }
            } else {
                if ($clientCompanyPayment->trashed()) {
                    $attached[] = $clientCompanyPayment->id;

                    $clientCompanyPayment->restore();
                }
            }
        }

        $inserts = array_unique(
            array_diff($companyPayments, $clientCompanyPayments->pluck('company_payment_id')->toArray())
        );

        foreach ($inserts as $companyPaymentId) {
            $attached[] = $companyPaymentId;

            $client->clientCompanyPayments()->create([
                'company_payment_id' => $companyPaymentId
            ]);
        }

        return [
            'attached' => $attached,
            'detached' => $detached,
            'updated' => [],
        ];
    }


    /**
     * Method to get client payments
     *
     * @param Company $company company object
     * @param Client  $client  client object
     * @return mixed
     */
    public function getClientPayments(Company $company, Client $client)
    {
        $companyPayments = CompanyPayment::where('company_id', $company->id)->get();

        return ClientCompanyPayment::whereIn('company_payment_id', $companyPayments->pluck('id'))
            ->where('client_id', $client->id)
            ->with('companyPayment.payment')
            ->get();
    }

    public function validateClientCompanyPayment(Company $company, Client $client, $price, $clientCompanyPaymentId)
    {
        $clientCompanyPayment = ClientCompanyPayment::find($clientCompanyPaymentId);

        $clientEnabledPayments = $this->getClientEnabledPayments($company, $client, $price);

        foreach ($clientEnabledPayments as $clientEnabledPayment) {
            if ($clientCompanyPayment->id == $clientEnabledPayment->id && !$clientEnabledPayment->enable) {
                throw new MobileException(false, $clientEnabledPayment->message);
            }
        }

        return true;
    }

    public function getClientEnabledPayments(Company $company, Client $client, float $price)
    {
        $clientCompanyPayments = $this->getClientPayments($company, $client);
        $creditCardEnabledWithCompany = $this->creditCardEnabledWithCompany($company, $client);

        $clientCompany = $client->setClientCompany($company);

        foreach ($clientCompanyPayments as $clientCompanyPayment) {
            $isCompanyCredit = $clientCompanyPayment->companyPayment->payment->isCompanyCredit;
            $isCreditCard = $clientCompanyPayment->companyPayment->payment->isCreditCard;

            $clientCompanyPayment->enable = true;

            if ($creditCardEnabledWithCompany && $isCompanyCredit && $clientCompany->account_balance < $price) {

                $clientCompanyPayment->enable = false;

                $limit =  formata_moeda($price - $clientCompany->account_balance, false);
                $clientCompanyPayment->message = __('Mobile/payment.companyCreditDisable', ['limit' => $limit]);
            }

            if ($isCreditCard && $price > $this->creditCardLimit) {
                $clientCompanyPayment->enable = false;

                $limit = formata_moeda($this->creditCardLimit, false);
                $clientCompanyPayment->message = __('Mobile/payment.creditCardDisable', ['limit' => $limit]);
            }
        }

        return $clientCompanyPayments;
    }

    /**
     * Method to verify if client has credit_card
     * @param Company $company company object
     * @param Client  $client  client object
     * @return mixed
     */
    public function creditCardEnabledWithCompany(Company $company, Client $client)
    {
        $clientCompanies = $this->getClientPayments($company,$client );

        $types = $clientCompanies->pluck('companyPayment.payment.type');

        return $this->creditCardInTypes($types->toArray());
    }

    /**
     * Check if client have a credit card enable
     *
     * @param  Client $client Client entity
     *
     * @return boolean
     */
    public function creditCardEnabled(Client $client): bool
    {
        $clientCompanyPayments = $client->clientCompanyPayments()->with('companyPayment.payment')->get();

        $types = $clientCompanyPayments->pluck('companyPayment.payment.type');

        return $this->creditCardInTypes($types->toArray());
    }

    private function creditCardInTypes(array $types)
    {
        return in_array(Payment::CREDIT_CARD, $types);
    }

    /**
     * get client payment `company_credit` of company
     *
     * @param Company $company Company entity
     * @param Client  $client  Client entity
     *
     * @return getClientCompanyPayment
     */
    public function getClientCompanyPaymentCredit(Company $company, Client $client)
    {
        $payment = (new Payment())->isCompanyCredit()->first();

        return $this->getClientCompanyPayment($company, $payment, $client);
    }

    /**
     * find or create client company payment by company and payment
     *
     * @param Company $company Company entity
     * @param Payment $payment Payment entity
     * @param Client  $client  Client entity
     *
     * @return boolean|ClientCompanyPayment
     */
    public function getClientCompanyPayment(Company $company, Payment $payment, Client $client)
    {
        $companyPayments = (new CompanyPaymentService())->list($company);

        $paymentCompany = $companyPayments->where('payment_id', $payment->id)->first();

        if ($paymentCompany) {
            $clientCompanyPayment = ClientCompanyPayment::
                where([
                    'client_id' => $client->id,
                    'company_payment_id' => $paymentCompany->id
                ])
                ->first();

            if (!$clientCompanyPayment) {
                Log::error("WARNING: CLIENT: {$client->id} WITHOUT COMPANY_CREDIT PAYMENT");

                $clientCompanyPayment = $this->create($client, $paymentCompany);
            }

            return $clientCompanyPayment;
        }

        return false;
    }

    /**
     * create or restore client company payment
     * @param Client         $client         Client entity
     * @param CompanyPayment $companyPayment Company Payment entity
     *
     * @return ClientCompanyPayment
     */
    public function create(Client $client, CompanyPayment $companyPayment)
    {
        $clientCompanyPayments = ClientCompanyPayment::withTrashed()
            ->where('client_id', $client->id)
            ->where('company_payment_id', $companyPayment->id)
            ->first();

        $response = false;

        if ($clientCompanyPayments === null) {
            $response = ClientCompanyPayment::create([
                'client_id' => $client->id,
                'company_payment_id' => $companyPayment->id
            ]);
        } else {
            if ($clientCompanyPayments->trashed()) {
                $clientCompanyPayments->restore();
                $response = $clientCompanyPayments->fresh();
            }
        }

        return $response;
    }

    /**
     * Calculate total of with tax correction
     *
     * @param float $total total
     * @param float $tax   tax
     *
     * @return float
     */
    public function calculateTaxTotal(float $total, float $tax)
    {
        $percent = ($tax / 100);

        $taxPrice = ($total + ($total * $percent)) * $percent;
        return round($total + $taxPrice, 2);
    }
}
