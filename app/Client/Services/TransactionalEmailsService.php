<?php
namespace App\Client\Services;

use App\Bid\Bid;
use App\Client\Notifications\TransactionalEmailNotification;
use App\Company\Company;
use App\Core\Entities\Status;
use App\Mobile\Services\BidService;
use App\Order\Order;
use App\Order\OrderProduct;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use App\Client\Mail\LoggableMail;
use App\Log\LogEmail;

class TransactionalEmailsService
{
    public function getPendingOrders(Company $company)
    {
        $orderIds = OrderProduct::with('order')
            ->whereHas('order', function ($query) use ($company) {
                return $query
                    ->where('company_id', $company->id);
            })
            ->where('email_sent', 0)
            ->where('status_id', '!=', Status::type(Status::PENDING)->id)
            ->pluck('pedido_id');

        return Order::with([
                'orderProduct.product',
                'orderProduct.status',
                'orderProduct.rejectionMotives',
                'client' => function ($query) use ($company) {
                    return $query->with([
                        'companies' => function ($query) use ($company) {
                            return $query->where('client_companies.company_id', $company->id);
                        },
                    ])->whereNotNull('email');
                },
                'bid.status',
                'bid.rejectionMotives',
            ])
            ->where('status_id', '!=', Status::type(Status::PENDING)->id)
            ->whereIn('id', $orderIds)
            ->get();
    }

    public function sendBidEmail(Bid $bid)
    {
        if (!$bid->client->email) {
            Log::error("transactionalEmails: Cliente de CNPJ {$bid->client->cnpj} não possui email cadastrado.");
            return;
        }

        $emailPayload = $this->buildEmailDataForBid($bid);
        $view = $this->getEmailView('bid');
        $subject = $this->getEmailSubject('bid', $bid->id);

        if (!$emailPayload) {
            return;
        }

        // @TODO remove this from here
        $email = (new LoggableMail($view, $emailPayload, "Contato Souk | {$subject}"));
        $email_md5 = md5($email->build()->render());
        $checkMd5 = (new LogEmail())->where('md5_mensagem', $email_md5)->count();

        if($checkMd5 <= 0) {
            $bid->client->notify(new TransactionalEmailNotification($view, $emailPayload, $subject));
        } else {
            Log::info('Duplicated email');
        }

        if($bid->order) {
            foreach ($bid->order->orderProduct as $orderProduct) {
                $orderProduct->update(['email_sent' => 1]);
            }
        }
    }

    public function sendOrderEmail(Order $order)
    {
        if (!$order->client || !$order->client->email) {
            Log::error("transactionalEmails: Cliente do pedido {$order->id} não possui email cadastrado.");
            return;
        }

        $emailPayload = $this->buildEmailDataForOrder($order);
        $view = $this->getEmailView('order');
        $subject = $this->getEmailSubject('order', $order->id);

        if (!$emailPayload) {
            return;
        }

        $order->client->notify(new TransactionalEmailNotification($view, $emailPayload, $subject));

        foreach ($order->orderProduct as $orderProduct) {
            $orderProduct->update(['email_sent' => 1]);
        }
    }

    protected function buildEmailDataForBid(Bid $bid)
    {
        $company = $bid->client->companies()->where('companies.id', $bid->company_id)->first();

        $canceled = Status::type(Status::CANCELED);

        if ($company === null) {
            Log::error("transactionalEmails: Não foi possível encontrar o relacionamento de clientCompany para o cliente {$bid->client->cnpj}");
            return false;
        }

        $status = (new BidService($bid))->getBidStatus($bid);

        if (!isset($status['email'])) {
            Log::error("Informações de status não encontradas para lance {$bid->id}");
            return false;
        }

        $statusColor = $status['email']['color'];
        $statusLabel = $status['email']['label'];
        $statusLabelDescription = $status['label'];
        $statusDescription = $status['email']['description'];

        $paymentDetails = $this->getPaymentDetails($bid);

        $payload = [
            'name' => $bid->client->name,
            'bidDate' => Carbon::createFromFormat('Y-m-d', $bid->date),
            'bidExpirationDate' => Carbon::createFromFormat('Y-m-d', $bid->duration),
            'deliveryDate' => $bid->order && $bid->order->delivery_date !== null ?
                Carbon::createFromFormat('Y-m-d', $bid->order->delivery_date)
                : false,
            'bidId' => $bid->id,
            'companyCode' => $bid->order ? $bid->order->order_code : false,
            'status' => $statusColor,
            'statusLabel' => $statusLabel,
            'statusDescription' => $statusDescription,
            'statusLabelDescription' => $statusLabelDescription,
            'address' => $company->clientCompany->address,
            'city' => $company->clientCompany->city,
            'state' => $company->clientCompany->state,
            'products' => [],
            'total' => 0,
            'showNestleWarning' => false,
            'paymentMethod' => $paymentDetails['paymentMethod'],
            'paymentMethodIcon' => $paymentDetails['paymentMethodIcon'],
            'paymentMethodLabel' => $paymentDetails['paymentMethodLabel'],
            'creditCardBrandIcon' => $paymentDetails['creditCardBrandIcon'],
            'creditCardDescription' => $paymentDetails['creditCardDescription'],
        ];

        $i=0;

        if($bid->order) {
            foreach ($bid->order->orderProduct as $orderProduct) {
                $payload['products'][$i] = [
                    'name' => $orderProduct->product->name,
                    'volume' => $orderProduct->kg_total,
                    'kg_price' => $orderProduct->kg_price,
                    'price' => $orderProduct->price,
                ];

                if($orderProduct->status_id == Status::type(Status::CANCELED)->id) {
                    $payload['products'][$i]['price'] = 0;
                } else {
                    $payload['total'] += (float) $orderProduct->price;
                }

                $i++;
            }
        } else {
            $payload['products'][$i] = [
                'name' => $bid->product->name,
                'volume' => $bid->weight,
                'kg_price' => $bid->kg_price,
                'price' => $bid->price,
            ];

            $payload['total'] = (float) $bid->price;

            if($bid->status_id == Status::type(Status::CANCELED)->id) {
                $payload['products'][$i]['price'] = 0;
                $payload['total'] = 0;
            }
        }

        if ($bid->company_id === 4) {
            $payload['showNestleWarning'] = true;
        }

        return $payload;
    }

    protected function buildEmailDataForOrder(Order $order)
    {
        $company = $order->client->companies()->where('companies.id', $order->company_id)->first();
        if ($company === null) {
            Log::error("transactionalEmails: Não foi possível encontrar o relacionamento de clientCompany para o cliente {$order->client->cnpj}");

            return false;
        }

        $status = $this->getOrderStatus($order);
        if (!$status) {
            Log::error("Informações de status não encontradas para lance {$order->bid->id}");

            return false;
        }
        $statusColor = $status['color'];
        $statusLabel = $status['label'];
        $statusDescription = $status['description'];

        $paymentDetails = $this->getPaymentDetails($order);

        $payload = [
            'name' => $order->client->name,
            'orderDate' => Carbon::createFromFormat('Y-m-d', $order->date),
            'orderCode' => $order->id,
            'companyCode' => $order->order_code,
            'status' => $statusColor,
            'statusLabel' => $statusLabel,
            'statusDescription' => $statusDescription,
            'address' => $company->clientCompany->address,
            'city' => $company->clientCompany->city,
            'state' => $company->clientCompany->state,
            'total' => 0,
            'showNestleWarning' => false,
            'paymentMethod' => $paymentDetails['paymentMethod'],
            'paymentMethodIcon' => $paymentDetails['paymentMethodIcon'],
            'paymentMethodLabel' => $paymentDetails['paymentMethodLabel'],
            'creditCardBrandIcon' => $paymentDetails['creditCardBrandIcon'],
            'creditCardDescription' => $paymentDetails['creditCardDescription'],
        ];

        $i = 0;
        foreach ($order->orderProduct as $orderProduct) {
            $status = $orderProduct->status()->first();

            $langKey = $status->id === Status::type(Status::CANCELED)->id ? 'rejectionMotive' : 'status';

            $payload['products'][$i] = [
                'status' => __("{$langKey}.order.{$orderProduct->status()->first()->name}.class"),
                'statusLabel' => __("{$langKey}.order.{$orderProduct->status()->first()->name}.label"),
                'name' => $orderProduct->product->name,
                'volume' => $orderProduct->kg_total,
                'kg_price' => $orderProduct->kg_price,
                'price' => $orderProduct->price,
            ];

            if($orderProduct->status_id == Status::type(Status::CANCELED)->id) {
                $payload['products'][$i]['price'] = 0;
            }
            $i++;
        }

        $payload['total'] = $order->orderProduct()->notCanceled()->sum('valor');

        if ($order->company_id === 4) {
            $payload['showNestleWarning'] = true;
        }

        return $payload;
    }

    protected function getPaymentDetails($model): array
    {
        $payment = $model->clientCompanyPayment->companyPayment->payment;

        $response = [
            'paymentMethod' => $payment->type,
            'paymentMethodIcon' => $payment->image['png'],
            'paymentMethodLabel' => $payment->name,
            'creditCardBrandIcon' => null,
            'creditCardDescription' => null,
        ];

        if($payment->type === 'company_credit') {
            $response['paymentMethodLabel'] .= ' - ' . $model->company->name;
        }

        if($payment->type === 'credit_card' && $model->client_card_id !== null) {
            $response['paymentMethodLabel'] .= " em {$model->cc_months}x";
            $response['creditCardBrandIcon'] = $model->clientCard->image;
            $response['creditCardDescription'] = $model->clientCard->description;
        }

        return $response;
    }

    protected function getEmailView($type = 'order'): string
    {
        return "mail.transactional.{$type}";
    }

    protected function getEmailSubject($type, $id): string
    {
        return __("transactionalEmails.{$type}", ['id' => $id]);
    }

    protected function getOrderStatus(Order $order): array
    {
        $statusName = $order->status()->first()->name;
        if($order->isPartiallyBilled()) {
            return [
                'color' => __('status.order.partially_billed.class'),
                'label' => __('status.order.partially_billed.label'),
                'description' => __('status.order.partially_billed.email'),
            ];
        }

        if ($order->status_id === Status::type(Status::CANCELED)->id) {
            return [
                'color' => __('rejectionMotive.order.canceled.class'),
                'label' => __('rejectionMotive.order.canceled.label'),
                'description' => __('rejectionMotive.order.canceled.email'),
            ];
        }

        return [
            'color' => __("status.order.{$statusName}.class"),
            'label' => __("status.order.{$statusName}.label"),
            'description' => __("status.order.{$statusName}.email"),
        ];
    }

    public function checkEmailHasAlreadyBeenSent($md5)
    {
        return (new LogEmail())->where('md5_mensagem', $md5)->count();
    }
}
