<?php

namespace App\Client\Imports;

use App\ClientsCompanies\Services\ClientsCompaniesService;
use App\Core\Entities\Status;
use App\FileQueue\Services\FileQueueService;
use Illuminate\Support\Collection;
use App\FileQueue\FileQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Illuminate\Support\Facades\Validator;

class ClientImport implements ToCollection, WithHeadingRow, WithEvents, WithMultipleSheets
{
    use Importable, RegistersEventListeners;

    protected $clientCompanyService;
    protected $fileQueue;
    protected $clientIds = [];
    protected $errors = [];

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);

        $this->clientCompanyService  = new ClientsCompaniesService();

    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;

            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     */
    public function collection(Collection $rows)
    {
        $errors = [];
        $validRows = $rows->filter(function ($row) {
            $emptyColumnsCount = 0;

            foreach($row as $column => $value) {
                if(array_key_exists($column, $this->rules()) && !$value) {
                    $emptyColumnsCount++;
                }
            }
            return $emptyColumnsCount !== count(array_keys(self::rules()));
        });

        foreach($validRows as $rowNumber => $row) {
            $validation = Validator::make($row->toArray(), self::rules());
            if ($validation->fails()) {
                $errors[$rowNumber + $this->headingRow() + 1] = $validation->errors()->getMessageBag()->all();
                continue;
            }

            $client_code = array_get($row, 'conta_de_credito');
            $currentBalance = array_get($row, 'disponivel_dif_entre_limite_e_div_a_receber');
            $overdueInvoice = array_get($row, 'faturas_vencidas');

            $client = $this->clientCompanyService->processAccountBalance(
                $this->fileQueue,
                $client_code,
                $currentBalance,
                $overdueInvoice
            );
            $this->clientIds[] = $client->id;
        }

        if(count($errors) > 0) {
            $this->logErrors($errors);
        }
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        $companyId = $this->fileQueue->company_id;

        return [
            'conta_de_credito' => "required|exists:client_companies,client_code,deleted_at,NULL,company_id,{$companyId}",
            'cliente' => 'required',
            'faturas_vencidas' => 'required',
            'limite' => 'required',
            'div_a_receber' => 'required',
            'disponivel_dif_entre_limite_e_div_a_receber' => 'required',
            'status' => 'required'
        ];
    }

    /**
     * @TODO move to abstract class
     * @param array $errors errors array
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function logErrors($errors)
    {
        $this->fileQueue->update([
            'status_id' => Status::type(Status::WITH_ERRORS)->id
        ]);

        $errorMessages = [];

        foreach($errors as $row => $messages) {
            $message = implode(', ', $messages);
            $message = "Linha {$row}: {$message}";
            $errorMessages[] = $message;
            Log::info($message);

        }
        return (new FileQueueService)->createLog($this->fileQueue, $errorMessages);
    }

    public function sheets(): array
    {
        return [
            '0' => $this,
        ];
    }
}
