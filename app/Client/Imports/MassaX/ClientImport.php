<?php
namespace App\Client\Imports\MassaX;

use App\ClientsCompanies\Services\ClientsCompaniesService;
use App\Core\Entities\Status;
use App\FileQueue\Services\FileQueueService;
use Illuminate\Support\Collection;
use App\FileQueue\FileQueue;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Illuminate\Support\Facades\Validator;

class ClientImport implements ToCollection, WithHeadingRow, WithEvents, WithCustomCsvSettings
{
    use Importable, RegistersEventListeners;

    protected $clientCompanyService;
    protected $fileQueue;
    protected $clientIds = [];
    protected $errors = [];

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);

        $this->clientCompanyService = new ClientsCompaniesService();

    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;

            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     */
    public function collection(Collection $rows)
    {
        $errors = [];

        $validRows = $rows->filter(function ($row) {
            $emptyColumnsCount = 0;

            foreach ($row as $column => $value) {
                if (array_key_exists($column, $this->rules()) && !$value) {
                    $emptyColumnsCount++;
                }
            }
            return $emptyColumnsCount !== count(array_keys(self::rules()));
        });

        foreach ($validRows as $rowNumber => $row) {
            $validation = Validator::make($row->toArray(), self::rules());
            if ($validation->fails()) {
                $errors[$rowNumber + $this->headingRow() + 1] = $validation->errors()->getMessageBag()->all();
                continue;
            }

            $row = collect($this->normalizeData($row));

            $clientData = $row->only('cnpj', 'name', 'password', 'email_verified')->toArray();

            $clientCompanyData = $row->only(
                'min_order',
                'account_balance',
                'client_code',
                'address',
                'state',
                'city',
                'zip_code',
                'status',
                'company_id',
                'typology',
                'channel',
                'deleted_at'
            )->toArray();

            $sellerData = $row->only('seller_name', 'email', 'phone', 'company_id')->toArray();
            $sellerData['name'] = $sellerData['seller_name'];

            $this->clientCompanyService->processCreateClientCompanies(
                $this->fileQueue->company, $clientData, $clientCompanyData, $sellerData
            );
        }

        if (count($errors) > 0) {
            (new FileQueueService)->createImportLog($this->fileQueue, $errors);
        }
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'cnpj' => 'required',
            'nome_fantasia' => 'required',
            'status' => 'required',
            'saldo' => 'required',
            'pedido_minimo' => 'required',
            'endereco' => 'required',
            'cep' => 'required',
            'uf' => 'required',
            'municipio' => 'required',
            'vendedor' => 'required',
            'email_vendedor' => 'required',
            'telefone_vendedor' => 'required',
            'canal' => 'required'
        ];
    }

    /**
     * get csv settings
     *
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    protected function normalizeData($row)
    {
        return [
            'company_id' => $this->fileQueue->company->id,
            'cnpj' => sanitize_number($row['cnpj']),
            'client_code' => null,
            'name' => $row['nome_fantasia'],
            'typology' => $row['rede'],
            'channel' => $row['canal'],
            'password' => Str::random(32),
            'email_verified' => 0,
            'min_order' => $row['pedido_minimo'],
            'account_balance' => $row['saldo'],
            'address' => $row['endereco'],
            'state' => $row['uf'],
            'city' => $row['municipio'],
            'zip_code' => $row['cep'],
            'status' => $row['status'] === 'ATIVO' && $row['saldo'] > 0,
            'seller_name' => $row['vendedor'],
            'email' => $row['email_vendedor'],
            'phone' => $row['telefone_vendedor'],
            'deleted_at' => null,
        ];
    }
}
