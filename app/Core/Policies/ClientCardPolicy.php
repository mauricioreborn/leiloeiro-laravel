<?php

namespace App\Core\Policies;

use App\Client\Client;
use App\Client\ClientCard;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientCardPolicy
{
    use HandlesAuthorization;

    /**
     * Verify client have card access
     *
     * @param Client     $client     Client entity
     * @param ClientCard $clientCard ClientCard entity
     *
     * @return boolean
     */
    public function clientAccess(Client $client, ClientCard $clientCard)
    {
        return $client->id === $clientCard->client_id;
    }
}
