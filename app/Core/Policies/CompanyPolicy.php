<?php

namespace App\Core\Policies;

use App\Auth\User;
use App\Client\Client;
use App\Company\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Verify client company access
     *
     * @param Client  $client  Client entity
     * @param Company $company Company entity
     *
     * @return [type]           [description]
     */
    public function clientAccess(Client $client, Company $company)
    {
        return (bool) $client->companies->find($company) ?? true;
    }

    /**
     * Verify client company access
     *
     * @param User    $user    User entity
     * @param Company $company Company entity
     *
     * @return boolean
     */
    public function userAccess(User $user, Company $company)
    {
        return $user->company_id == $company->id;
    }

    /**
     * Verify client company management request
     *
     * @param User    $user    User entity
     * @param Company $company Company entity
     *
     * @return boolean
     */
    public function companyManagementRequest(User $user, Company $company)
    {
        $hasManagementRequests = $user->company->companySettings->where('setting', 'hasManagementRequests')->first();
        return $hasManagementRequests ? $hasManagementRequests->value : false;
    }

    /**
     * Validates that only company with name=Seara can manage campaigns
     *
     * @param User    $user    User entity
     * @param Company $company Company entity
     *
     * @return boolean
     */
    public function allowCampaigns(User $user, Company $company)
    {
        return true;
    }
}
