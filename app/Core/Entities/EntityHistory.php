<?php

namespace App\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class EntityHistory extends Model
{
    protected $fillable = [
        'table',
        'user_id',
        'client_id',
        'parent_id',
        'status',
        'fields',
    ];

    protected $casts = [
        'fields' => 'array'
    ];
}
