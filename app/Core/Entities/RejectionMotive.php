<?php

namespace App\Core\Entities;

use App\Core\Traits\Cacheable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RejectionMotive extends Model
{
    use Cacheable, SoftDeletes;

    const INVALID_FEFO = 'invalid_fefo';
    const CUSTOMER_WITHOUT_BALANCE = 'customer_without_balance';
    const MIN_ORDER = 'min_order';
    const EXPIRED = 'expired';
    const PRICE_REJECTED = 'price_rejected';
    const REMOVED_BY_USER = 'removed_by_user';
    const PAYMENT_DENIED = 'payment_denied';
    const BEHAVIOUR_REJECTION = 'behaviour_rejection';

    protected $table = 'rejection_motives';

    protected $fillable = [
        'name'
    ];

    /**
     * Get by type
     *
     * @param Builder $builder Builder
     * @param string  $name    Name type
     *
     * @return $this
     */
    public function scopeType($builder, $name)
    {
        return $builder->where('name', $name)->first();
    }

    public static function getMotives()
    {
        $constants = (new \ReflectionClass(static::class))->getConstants();

        return array_except($constants, ['UPDATED_AT', 'CREATED_AT']);
    }
}
