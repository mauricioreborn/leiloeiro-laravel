<?php

namespace App\Core\Entities;

use App\Core\Traits\Cacheable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    use Cacheable, SoftDeletes;

    const CREATED = 'created';
    const INTEGRATED = 'integrated';
    const PENDING = 'pending';
    const PROCESSING = 'processing';
    const PAID = 'paid';
    const CANCELED = 'canceled';
    const PARTIALLY_PAID = 'partially_paid';
    const REFUNDED = 'refunded';
    const EXPIRED = 'expired';
    const AUTHORIZED = 'authorized';
    const REJECTED = 'rejected';
    const DRAFT = 'draft';
    const PAYMENT_IN_PROGRESS = 'payment_in_progress';
    const IN_PROTEST = 'in_protest';
    const IN_ANALYSIS = 'in_analysis';
    const CHARGEBACK = 'chargeback';
    const ACTIVE = 'active';
    const BLOCKED = 'blocked';
    const WITH_ERRORS = 'with_errors';
    const PRICE_ACCEPTED = 'price_accepted';
    const EVALUATING_PRICE = 'evaluating_price';
    const APPROVED = 'approved';
    const SCHEDULED = 'scheduled';
    const FINISHED = 'finished';
    const IN_PROGRESS = 'in_progress';

    protected $table = 'status';

    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'displayName'
    ];

    /**
     * get display name
     *
     * @return CampaignClient
     */
    public function getDisplayNameAttribute()
    {
        return __("status.{$this->name}");
    }

    /**
     * Get by type
     *
     * @param Builder $builder Builder
     * @param string  $name    Name type
     *
     * @return $this
     */
    public function scopeType($builder, $name)
    {
        return $builder->where(['name' => $name])->first();
    }

    public static function getStatuses()
    {
        $constants = (new \ReflectionClass(static::class))->getConstants();

        return array_except($constants, ['UPDATED_AT', 'CREATED_AT']);
    }
}
