<?php

namespace App\Core\Exceptions\Traits;

use App\Core\Exceptions\ActionNotAllowedException;
use App\Core\Exceptions\InvalidFilterException;
use Exception;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use InvalidArgumentException;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

use App\Mobile\Exceptions\MobileValidationException;
use App\Mobile\Exceptions\MobileWithArrayValidationException;
use App\Mobile\Exceptions\MobileException;

trait RestExceptionHandlerTrait
{
    public $acceptedExceptions = [
        ModelNotFoundException::class => [
            'code'   => 'resource_not_found',
            'title'  => 'Resource not found.',
            'status' => 404,
        ],
        ValidationException::class => [],
        MobileValidationException::class => [],
        MobileWithArrayValidationException::class => [],
        MobileException::class => [],
        MethodNotAllowedHttpException::class => [
            'code'   => 'unauthenticated',
            'title'  => 'Unauthenticated.',
            'status' => 401,'unauthorized'
        ],
        NotFoundHttpException::class => [
            'code'   => 'route_not_found',
            'title'  => 'Route not found.',
            'status' => 404,
        ],
        AuthenticationException::class => [
            'code'   => 'unauthorized',
            'title'  => 'Unauthorized.',
            'status' => 401,
        ],
        ThrottleRequestsException::class => [
            'code'   => 'unauthorized',
            'title'  => 'Unauthorized.',
            'status' => 401,
        ],
        InvalidFilterException::class => [],
        AuthorizationException::class => [
            'code'   => 'unauthorized',
            'title'  => 'Unauthorized.',
            'status' => 403,
        ],
        InvalidArgumentException::class => [],
        ActionNotAllowedException::class => [],
    ];

    protected $exceptionsWithCustomHandler = [
        ValidationException::class => 'handleValidationException',
        MobileValidationException::class => 'handleMobileValidationException',
        MobileWithArrayValidationException::class => 'handleMobileWithArrayValidationException',
        MobileException::class => 'handleMobileException',
        InvalidFilterException::class => 'handleInvalidFilterException',
        InvalidArgumentException::class => 'handleInvalidArgumentException',
        ActionNotAllowedException::class  => 'handleActionNotAllowedException',
        ThrottleRequestsException::class => 'handleTooManyRequestsHttpException'
    ];

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Exception $exception Exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException($exception)
    {
        if(array_key_exists(get_class($exception), $this->acceptedExceptions)) {

            if(array_key_exists(get_class($exception), $this->exceptionsWithCustomHandler)) {
                return $this->{$this->exceptionsWithCustomHandler[get_class($exception)]}($exception);
            }

            return $this->jsonResponse(
                [
                    [
                        'code'   => __("auth.".$this->acceptedExceptions[get_class($exception)]['code'].'.code'),
                        'title'  => __("auth.".$this->acceptedExceptions[get_class($exception)]['code'].'.title'),
                        'status' => $this->acceptedExceptions[get_class($exception)]['status'],
                    ],
                ], $this->acceptedExceptions[get_class($exception)]['status']
            );
        }

        return $this->jsonResponse('Internal Error', 500);
    }

    /**
     * Returns json response.
     *
     * @param array $errors     Errors
     * @param int   $statusCode Status code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse($errors = [], $statusCode = 404)
    {
        return response()->json(
            [
                'errors' => $errors,
            ], $statusCode
        );
    }

    /**
     * @param array $exception Exception
     * @return \Illuminate\Http\JsonResponse
     **/
    protected function handleValidationException($exception)
    {
        $errors = collect($exception->errors())->map(
            function ($error, $key) {
                return [
                    'source'      => ['parameter' => $key],
                    'description' => $error,
                    'status'      => 422,
                ];
            }
        );

        return $this->jsonResponse($errors->values()->all(), 422);
    }

    /**
     * @param array $exception Exception
     * @return \Illuminate\Http\JsonResponse
     **/
    protected function handleMobileValidationException($exception)
    {

        $errors = collect($exception->errors())->map(
            function ($error, $key) {
                return [
                    'status'  => false,
                    'msg'     => $error[0],
                    'message' => $error[0],
                ];
            }
        );

        return response()->json($errors->values()->first(), 200);
    }

    /**
     * @param array $exception Exception
     * @return \Illuminate\Http\JsonResponse
     **/
    protected function handleMobileWithArrayValidationException($exception)
    {
        $errors = collect($exception->errors())->map(
            function ($error, $key) {
                return [
                    'status'  => false,
                    'msg'     => $error[0],
                    'message' => [$error[0]],
                ];
            }
        );

        return response()->json($errors->values()->first(), 200);
    }

    /**
     * @param Exception $exception Exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function handleMobileException($exception)
    {
        return response()->json($exception->errors());
    }

    /**
     * @param Exception $exception Exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function handleInvalidFilterException($exception)
    {
        $response = [
            'status'      => 400,
            'description' => $exception->getMessage(),
        ];

        return response()->json(['errors' => [$response]], 400);
    }

    /**
     * @param Exception $exception Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleInvalidArgumentException($exception)
    {
        return response()->json(
            [
                'errors' => [
                    [
                        'status'      => 400,
                        'description' => $exception->getMessage(),
                    ],
                ],
            ], 400
        );
    }

    /**
     * @param Exception $exception Exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function handleActionNotAllowedException($exception)
    {
        return response()->json(['error' => $exception->getMessage()], $exception->getCode());
    }

    public function handleTooManyRequestsHttpException($exception)
    {
        return response()->json(
             [
                 'status'  => false,
                 'msg'     => __(
                     "auth.to_many_requests.title",
                     ['time' => $exception->getHeaders()['Retry-After']]
                 ),
                 'message' => __(
                     "auth.to_many_requests.title",
                     ['time' => $exception->getHeaders()['Retry-After']]
                 ),
             ],
            200,
            [
               'Retry-After' =>  $exception->getHeaders()['Retry-After']
            ]
        );
    }
}
