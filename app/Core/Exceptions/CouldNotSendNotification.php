<?php
namespace App\Core\Exceptions;

class CouldNotSendNotification extends \Exception
{
    /**
     * @param string $message Message
     * @return CouldNotSendNotification
     */
    public static function serviceRespondedWithAnError($message = '')
    {
        return new static('Could not send message.' . $message);
    }
}