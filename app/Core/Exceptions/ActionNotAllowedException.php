<?php

namespace App\Core\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ActionNotAllowedException extends \Exception
{

    /**
     * ActionNotAllowedException constructor.
     * @param string|null $message Exception Message
     */
    public function __construct(string $message = null)
    {
        parent::__construct($message, 403);
    }
}
