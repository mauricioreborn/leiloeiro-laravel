<?php

namespace App\Core\Exceptions;


use Symfony\Component\HttpKernel\Exception\HttpException;

class InvalidFilterException extends HttpException
{

    public function __construct(string $message = null)
    {
        parent::__construct(400, $message);
    }
}
