<?php

namespace App\Core\Exceptions;

use App\Core\Exceptions\Traits\RestExceptionHandlerTrait;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use InvalidArgumentException;

class Handler extends ExceptionHandler
{
    use RestExceptionHandlerTrait;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        InvalidArgumentException::class,
        ActionNotAllowedException::class,
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception Exception object
     *
     * @return void
     */
    public function report(Exception $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request   $request   Request object
     * @param Exception $exception Exception object
     *
     * @return Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        if (env('APP_DEBUG')) {
            return parent::render($request, $exception);
        }

        return $this->getJsonResponseForException($exception);
    }
}
