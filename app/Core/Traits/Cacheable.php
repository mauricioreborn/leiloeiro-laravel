<?php

namespace App\Core\Traits;

use App\Core\Classes\CachedBuilder;

/**
 * Trait Cacheable
 *
 * @package  App\Core\Traits
 */
trait Cacheable
{
    protected function newBaseQueryBuilder()
    {
        $connection = $this->getConnection();

        return new CachedBuilder(
            $connection, $connection->getQueryGrammar(), $connection->getPostProcessor(), $this
        );
    }
}