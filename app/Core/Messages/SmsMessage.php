<?php
namespace App\Core\Messages;

class SmsMessage
{
    public $type = '';

    public $phoneNumber = '';

    public $endpoint = '';

    public $message = '';

    public $subject = '';

    public $messageStructure = 'string';

    /**
     * @param string $message Message content
     */
    public function __construct($message = '')
    {
        $this->message = $message;
    }

    /**
     * Set the Phone Number.
     *
     * @param string $phoneNumber Phone number
     * @return $this
     */
    public function phoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Set the Type.
     *
     * @param string $type Type
     * @return $this
     */
    public function type($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the endpoint.
     *
     * @param string $endpoint Endpoint
     * @return $this
     */
    public function endpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Set the message content.
     *
     * @param string $message Message
     * @return $this
     */
    public function message($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Set the subject.
     *
     * @param string $subject Subject
     * @return $this
     */
    public function subject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Set the Message Structure.
     *
     * @param string $messageStructure MessageStructure
     * @return $this
     */
    public function messageStructure($messageStructure)
    {
        $this->messageStructure = $messageStructure;

        return $this;
    }
}