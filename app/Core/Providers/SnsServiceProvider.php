<?php
namespace App\Core\Providers;

use App\Core\Channels\SmsChannel;
use Aws\Sns\SnsClient;
use Illuminate\Support\ServiceProvider;

class SnsServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        config('aws.sns');
        $this->bindClient();
    }

    /**
     * @return void
     */
    protected function bindClient()
    {
        $this->app->when(SmsChannel::class)->needs(SnsClient::class)
            ->give(function () {
                return $this->snsInstance();
            });
    }

    /**
     * @return SnsClient
     */
    protected function snsInstance()
    {
        $config = config('services.sns');
        $snsConfig = [
            'credentials' => [
                'key' => $config['key'],
                'secret' => $config['secret'],
            ],
            'version' => 'latest',
            'region' => $config['region'],
        ];

        return new SnsClient($snsConfig);
    }
}