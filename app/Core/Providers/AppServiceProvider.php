<?php

namespace App\Core\Providers;

use App\Company\Company;
use App\Company\Observers\CompanyObserver;
use App\Core\Helpers\Zipcode;
use App\Core\Policies\CompanyPolicy;
use Illuminate\Foundation\Http\Events\RequestHandled;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Mockery;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        DB::connection()->disableQueryLog();

        Route::model('company', Company::class);
        Gate::policy(Company::class, CompanyPolicy::class);
        Company::observe(CompanyObserver::class);

        if ($this->app->environment('testing')) {
            $this->app->instance(Zipcode::class, Mockery::mock(Zipcode::class)->makePartial());
        }

        $this->enableQueryLogging();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function enableQueryLogging(): void
    {
        if(env('DEV_SQL_DEBUG', false) === false) {
            return;
        }

        DB::connection()->enableQueryLog();
        Event::listen(RequestHandled::class, function (RequestHandled $event) {
            $queries = DB::getQueryLog();
            $debug = [];
            $total = 0;
            if (!empty($queries)) {
                foreach ($queries as $query) {
                    $debug[] = "[{$query['time']}ms]: {$query['query']}";
                    $total += $query['time'];
                    //$debug[] = "[{$query['time']}ms]: " . vsprintf(str_replace('?', '%s', $query['query']), $query['bindings']);
                }
                Log::channel('query_logging')->info([
                    'path' => $event->request->url(),
                    'total_queries' => count($debug),
                    'queries' => $debug,
                    'total' => $total
                ]);
            }
        });
    }
}
