<?php

namespace App\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Carbon;
use Yasumi\Yasumi;

class CarbonMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $holidays = Yasumi::create('Brazil', Carbon::now()->format('Y'));

        $holidaysDates = array_values($holidays->getHolidayDates());

        Carbon::macro('isHoliday', function () use ($holidaysDates) {
            return in_array($this->format('Y-m-d'), $holidaysDates);
        });

        /**
         * increments business days from the instance
         *
         * @param int  $days       Increment number of business days, default 1
         * @param bool $increments Increments flag, default true
         *
         * @return static
         */
        Carbon::macro('addBusinessDays', function ($days = 1, $increments = true) {
            for ($day = 0; $day < $days;) {
                if ($increments) {
                    $this->addDay();
                } else {
                    $this->subDay();
                }

                if (!$this->isWeekend()) {
                    $day++;
                }
            }

            return $this;
        });

        /**
         * Remove business days from the instance
         *
         * @param int $days Remove number of business days, default 1
         *
         * @return static
         */
        Carbon::macro('subBusinessDays', function ($days = 1) {
            return $this->addBusinessDays($days, false);
        });

        /**
         * Check if the date is valid
         *
         * @param int|string $year  Year
         * @param int        $month Month
         * @param int        $day   Day
         *
         * @return DateFormat
         */
        Carbon::macro('checkDate', function ($year, $month = null, $day = null) {

            if($day === null){
                [$year, $month, $day] = explode('-', $year);
            }

            return checkdate($month, $day, $year);
        });
    }
}
