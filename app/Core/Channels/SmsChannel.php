<?php
namespace App\Core\Channels;

use App\Core\Exceptions\CouldNotSendNotification;
use App\Log\LogSms;
use Aws\Sns\Exception\SnsException;
use Aws\Sns\SnsClient;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class SmsChannel
{
    /**
     * @var SnsClient
     */
    protected $snsClient;

    /**
     * @param SnsClient $snsClient SnsClient instance
     */
    public function __construct(SnsClient $snsClient)
    {
        $this->snsClient = $snsClient;
    }

    /**
     * Send the given notification.
     *
     * @param mixed        $notifiable   Notifiable object
     * @param Notification $notification Notification
     *
     * @return void
     *
     * @throws CouldNotSendNotification
     */
    public function send($notifiable, Notification $notification)
    {
        Log::info("[SMS][{$notifiable->id}] Iniciado tentativa de envio de SMS");
        $message = $notification->toSms($notifiable);
        $message->phoneNumber = $message->phoneNumber ?: $notifiable->routeNotificationForSms();

        if (!$message->phoneNumber || !$message->message) {
            Log::info("[SMS][{$notifiable->id}] número ou mensagem inválido");
            return;
        }

        $payload = [
            'PhoneNumber' => $message->phoneNumber,
            'MessageStructure' => $message->messageStructure,
            'Message' => $message->message,
        ];

        try {
            $this->configureSMS();

            Log::info("[SMS][{$notifiable->id}] payload montado: " . json_encode($payload));

            $response = $this->snsClient->publish($payload);
            $response = $response->toArray();

            Log::info("[SMS][{$notifiable->id}] tentativa de envio de SMS respondido com: " . json_encode($response));

            if ($response['@metadata']['statusCode'] !== 200) {
                $this->createErrorLog(
                    $notifiable,
                    $payload['PhoneNumber'],
                    $payload['Message'],
                    json_encode($response['@metadata'])
                );
                throw CouldNotSendNotification::serviceRespondedWithAnError();
            }

            $this->createLog($notifiable, $payload['PhoneNumber'], $payload['Message']);
        } catch(SnsException $exception) {
            $this->createErrorLog(
                $notifiable,
                $payload['PhoneNumber'],
                $payload['Message'],
                $exception->getAwsErrorMessage()
            );
            throw $exception;
        }

    }

    /**
     * @return void
     */
    protected function  configureSMS()
    {
        $config = config('aws.sns');
        if (!$config || !array_key_exists('sms', $config)) {
            return;
        }

        $smsConfig = [];
        foreach ($config['sms'] as $conf => $value) {
            if (!$value) {
                continue;
            }
            $smsConfig[ucfirst($conf)] = $value;
        }

        if (count($smsConfig) < 1) {
            return;
        }

        $this->snsClient->setSMSAttributes([
            'attributes' => $smsConfig,
        ]);
    }

    /**
     * @param        $notifiable
     * @param string $phoneNumber phone number
     * @param string $message     message
     *
     * @return void
     */
    public function createLog($notifiable, $phoneNumber, $message)
    {
        Log::info("[SMS][{$notifiable->id}] tentativa de envio de SMS bem sucedido.");

        LogSms::create([
            'phone_number' => $phoneNumber,
            'message' => $message,
            'message_sent' => true,
        ]);
    }

    /**
     * @param        $notifiable
     * @param string $phoneNumber phone number
     * @param string $message     message
     * @param string $errors      errors
     *
     * @return void
     */
    public function createErrorLog($notifiable, $phoneNumber, $message, $errors)
    {
        Log::info("[SMS][{$notifiable->id}] tentativa de envio de SMS falhou.");
        LogSms::create([
            'phone_number' => $phoneNumber,
            'message' => $message,
            'message_sent' => false,
            'errors' => $errors,
        ]);
    }
}