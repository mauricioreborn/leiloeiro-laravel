<?php

namespace App\Core\Console;

use App\Jobs\BehaviourApprovalJob;
use App\Jobs\FefoReplicateJob;
use App\Jobs\Campaign\ProductsByTypologyJob;
use App\Jobs\Campaign\SuggestedProductsJob;
use App\Jobs\NotificationBusinessHoursJob;
use App\Jobs\PaymentReportsJob;
use App\Jobs\ProcessTransactionalEmailsJob;
use App\Jobs\RefreshClientListsCacheJob;
use App\Jobs\StockReplicateJob;
use App\Jobs\UpdateExpiredBidsJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule Schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->job(new PaymentReportsJob(1))->cron('0 5 * * *');
        $schedule->job(new PaymentReportsJob(3))->cron('5 5 * * *');
        $schedule->job(new UpdateExpiredBidsJob)->cron('0 */2 * * *');
        $schedule->job(new NotificationBusinessHoursJob)->dailyAt('08:05');
        $schedule->job(new FefoReplicateJob())->dailyAt('00:05');
        $schedule->job(new ProductsByTypologyJob())->cron('0 5 * * *');
        $schedule->job(new SuggestedProductsJob())->cron('0 6 * * *');
        $schedule->job(new StockReplicateJob())->dailyAt('00:01');
       $schedule->job(new ProcessTransactionalEmailsJob())
           ->environments(['production'])
           ->everyFiveMinutes();

        $schedule->job(new BehaviourApprovalJob())
            ->environments(['production'])
            ->everyTenMinutes()
            ->between('18:00', '23:59');
        $schedule->job(new RefreshClientListsCacheJob)->everyThirtyMinutes();

        if(app()->environment() == "qa"){
            $schedule->command('replicate:dataQa')->dailyAt('06:30');
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
    }
}
