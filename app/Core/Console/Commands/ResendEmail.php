<?php

namespace App\Core\Console\Commands;

use App\Client\Client;
use App\Client\Notifications\MobileConfirmationNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;
use App\Notification\Notification as AppNotification;
use Log;

class ResendEmail extends Command
{
    protected $signature = 'souk:resend-email';
    protected $description = '';

    public function handle()
    {
        $ClientId = [
            202885,
            184306,
            348540,
            335760,
            344572,
            194469,
            229706,
            199939,
            229449,
            179878,
            289701,
            329635,
            194085,
            205830,
            184699,
            191861,
            195757,
            203379,
            291876,
            276610,
            293567,
            335060,
            283912,
            257695,
            281369,
            253068,
            263620,
            276045,
            268993,
            265966,
            262803,
            261538,
            278708,
            210959,
            207812,
            206197,
            209631,
            209826,
            207509,
            327609,
            211101,
            209846,
            319322,
            306684,
            273105,
            288123,
            281438,
            255419,
            284997,
            255118,
            282795,
            244489,
            243641,
            296822,
            352775,
            244333,
            241029,
            236413,
            330893,
            330499,
            238218,
        ];
        $clients = Client::where('email_verified', '=', 1)->whereIn('id', $ClientId)->get();

        $progressBar = $this->output->createProgressBar($clients->count());

        foreach ($clients as $client){

            if($client->confirm_email != 3) {
                continue;
            }

            $confirmation = $client->confirmations()->where('type', '=', 'mail')->latest()->first();

            if(!$confirmation){
                $this->error("Confirmation não encontrada para o $client->id");
                Log::error("$this->signature Confirmation não encontrada para o $client->id");
                continue;
            }

            sleep(1);

            Log::info("$this->signature reenviando email para o cliente $client->id");

            Notification::send($confirmation, new MobileConfirmationNotification('mail'));

            Log::info("$this->signature enviando push para o cliente $client->id");

            AppNotification::create([
                'type' => 0,
                'client_id' => $client->id,
                'title' => 'Verificação de email',
                //@phpcs:ignore
                'message' => "Tudo certo para verificar seu e-mail! Enviamos agora um novo e-mail de confirmação para você."
            ]);

            $progressBar->advance();
        }

        $progressBar->finish();

        $this->info("Finalizado!");
    }
}


