<?php

namespace App\Core\Console\Commands;

use App\Client\ClientTirolez;
use App\Client\Notifications\TirolezCampaignNotification;
use App\Client\Services\ClientService;
use App\Client\Services\ClientTirolezService;
use App\Client\Services\TransactionalEmailsService;
use App\Company\Company;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notification\Notification as PushNotification;

class TirolezCampaign extends Command
{
    protected $signature = "souk:campaignTirolez";
    protected $description = "Send email, sms and push notification to Tirolez clients";

    protected $campaignDays;
    protected $services;
    protected $campaignDayService;
    protected $tirolezClients;
    protected $bar;
    protected $uf;
    protected $selectService;
    protected $selectDay;
    protected $selectUf;

    protected $clientTirolezService;
    protected $totalReject = 0;
    protected $totalSend = 0;

    public function __construct()
    {
        parent::__construct();

        $this->campaignDays = ["d0" => "D + 0", "d1" => "D + 1", "d3" => "D + 3", "d8" => "D + 8"];
        $this->services = ["email" => "EMAIL", "sms" => "SMS", "push" => "PUSH"];
        $this->uf = [1 => "SP/RJ", 2 => "MG"];
        $this->campaignDayService = [
            "d0" => ["email", "push", "sms"],
            "d1" => ["push"],
            "d3" => ["email", "push"],
            "d8" => ["email", "push"],
        ];

        $this->clientTirolezService = new ClientTirolezService();
    }

    public function handle(): void
    {
        $this->info("{$this->signature} inicio:". now()->toDateTimeString());

        $this->selectDay = $this->choice('Qual o dia da campanha?', $this->campaignDays);

        $this->selectService = $this->choice('Qual o serviço será disparado?', $this->services);

        if(!in_array($this->selectService, $this->campaignDayService[$this->selectDay])){
            $this->error("Serviço não disponivel para o dia solicitado");
            return;
        }

//        $this->selectUf = $this->choice('Qual a UF serviço será disparado?', $this->uf);

        $this->tirolezClients = ClientTirolez::with('client')->get();

        $this->exec();

        $this->info(PHP_EOL."{$this->signature}: {$this->selectService} finalizado: ". now()->toDateTimeString());
        $this->warn("Enviado para {$this->totalSend} e não enviado para {$this->totalReject} por já ter convertido");
    }

    public function progressBar(): void
    {
        $this->bar = $this->output->createProgressBar($this->tirolezClients->count());
        $this->bar->start();
    }

    public function exec(): void
    {
        $this->progressBar();

        $selectService = $this->selectService;
        $company = Company::find(5);
//        $this->selectUf = explode('/', $this->selectUf);

        foreach($this->tirolezClients as $tClient) {

            $checkAlreadyBought = (new ClientService())->hasAlreadyBought($company, $tClient->client);
            $clientState = (new ClientTirolezService())->getClientState($company, $tClient->client);

//            if($clientState === false || !in_array($clientState, $this->selectUf)){
//                phpcs:ignore
//                Log::info("{$this->signature}: Cliente de CNPJ {$tClient->client->cnpj} uf divergente da execução.");
//                continue;
//            }

            if($checkAlreadyBought === true){
                $this->totalReject++;
            }else{
                $this->$selectService($tClient);
                $this->totalSend++;
            }

            $this->bar->advance();
        }

        $this->bar->finish();
    }

    public function email(ClientTirolez $clientTirolez): void
    {
        if (is_null($clientTirolez->client->email)) {
            Log::error("{$this->signature}: Cliente de CNPJ {$clientTirolez->client->cnpj} não possui email cadastrado.");
            return;
        }

        $view = "mail.campaign.tirolez_{$this->selectDay}";
        $subject = __("Campaign/tirolez.{$this->selectDay}.{$this->selectService}.subject");

        $linkMd5 = md5($subject."-".$clientTirolez->client->cnpj);
        $apiUrl = config('APP_URL')."/api/v1/emailopened?hash={$linkMd5}";
        $checkSend = (new TransactionalEmailsService())->checkEmailHasAlreadyBeenSent($linkMd5);

        if($checkSend > 0){
            Log::info("{$this->signature}: Email duplicado para o Cliente {$clientTirolez->client->cnpj}");
            return;
        }

        $slug = "Campaign/tirolez.{$this->selectDay}.{$this->selectService}.slug.base_{$clientTirolez->type}";
        $link = __("Campaign/tirolez.{$this->selectDay}.{$this->selectService}.link.base_{$clientTirolez->type}");

        $payload = [
            'subject' => $subject,
            'view' => $view,
            'viewData' => [
                'slug' => $slug,
                'link' => $link,
                'tracking_url' => $apiUrl,
            ],
            'md5_mensagem' => $linkMd5,
        ];

        $clientTirolez->client->notify(new TirolezCampaignNotification($payload, 'mail'));
    }

    public function sms($tClient): void
    {
        if (is_null($tClient->client->phone)) {
            Log::error("{$this->signature}: Cliente de CNPJ {$tClient->client->cnpj} não telefone cadastrado.");
            return;
        }

        $payload = ['messageKey' => "Campaign/tirolez.{$this->selectDay}.{$this->selectService}.base_{$tClient->type}"];
        $tClient->client->notify(new TirolezCampaignNotification($payload));
    }

    public function push($tClient): void
    {
        $key = "Campaign/tirolez.{$this->selectDay}.{$this->selectService}.base_{$tClient->type}";
        $title = __($key.".title");
        $pushMessage = __($key.".message");
        $attributes = json_encode([
            'route' => 'HomePage',
            'company_id' => 5,
            'campaign' => __($key.".campaign"),
        ]);

        PushNotification::create([
            'type' => 0,
            'client_id' => $tClient->client->id,
            'title' => $title,
            'message' => $pushMessage,
            'attributes' => $attributes,
        ]);
    }
}