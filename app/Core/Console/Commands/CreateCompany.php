<?php

namespace App\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

use App\Auth\User;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Payment\Payment;
use App\DistributionCenter\DistributionCenter;

class CreateCompany extends Command
{
    protected $signature = "souk:createCompany";
    protected $description = "Create a new company";

    protected $fields;
    protected $companyName;
    protected $code;
    protected $userEmail;
    protected $userPassword;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $this->info("{$this->signature} inicio:". now()->toDateTimeString());

        $this->companyName = $this->ask('Qual o nome da company?', $this->companyName);
        $this->code = $this->ask('Qual o código da empresa?', $this->code);
        $this->userEmail = $this->ask('Qual o e-mail para o usuário do painel?', $this->userEmail);
        $this->userPassword = $this->secret('Qual a senha para o usuário do painel?', $this->userPassword);

        $this->fields = [
            'companyName' => $this->companyName,
            'code' => $this->code,
            'email' => $this->userEmail,
            'password' => $this->userPassword,
        ];

        $validation = Validator::make($this->fields, $this->rules());
        if($validation->fails()) {
            $errors = $validation->errors()->getMessageBag()->all();

            $errorMessages = [];

            foreach($errors as $messages) {
                Log::error($messages);
            }

            Log::error('Empresa não criada');
            exit;
        }

        $this->exec();

        //phpcs:ignore
        $this->info(PHP_EOL."{$this->signature}: A empresa {$this->companyName} foi criada com sucesso! ". now()->toDateTimeString());
    }

    public function exec(): void
    {
        $company = Company::where('name', $this->companyName)->first();

        if (!isset($company->id)) {
            $company = Company::create([
                'name' => $this->companyName,
                'code' => $this->code,
                'cart' => __('Mobile/login.texto_carrinho'),
                'bid' => __('Mobile/login.texto_lance'),
                'rules' => __('Mobile/login.texto_regras'),
                'aboult' => __('Mobile/login.texto_sobre'),
                'policy' => __('Mobile/login.texto_politica'),
                'information' => __('Mobile/login.texto_informacoes'),
            ]);
        }

        $user = User::where('email', $this->userEmail)->first();

        if (!$user) {
            User::create([
                'name' => $this->companyName,
                'email' => $this->userEmail,
                'password' => $this->userPassword,
                'root' => 1,
                'company_id' => $company->id,
            ]);
        }

        $companyPayment = CompanyPayment::where([
            'company_id' => $company->id
        ])->first();

        if (!isset($companyPayment->id)) {
            $payment = Payment::where([
                'type' => 'company_credit'
            ])->first();

            CompanyPayment::create([
                'payment_id' => $payment->id,
                'company_id' => $company->id
            ]);
        }

        DistributionCenter::create([
            'code' => $company->code,
            'name' => 'CD SAO PAULO',
            'company_id' => $company->id
        ]);
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'companyName' => "required|unique:companies,name",
            'code' => 'required|unique:companies,code',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ];
    }
}
