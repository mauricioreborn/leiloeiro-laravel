<?php

namespace App\Core\Console\Commands;

use App\Core\Entities\Status;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Log;

class ReplicateDataQa extends Command
{
    protected $signature = 'replicate:dataQa';
    protected $description = 'Replicate data (Bids and FEFO) in QA database';

    public function handle()
    {
        if(app()->environment() !== 'production'){

            $today = Carbon::now();

            $lastRegisterFefo = DB::table('fefo')
                ->where('date', '<>', $today->copy()->format('Y-m-d'))
                ->orderBy('date', 'desc')->limit(1)->first();

            if($lastRegisterFefo){

                $last = Carbon::createFromFormat('Y-m-d', $lastRegisterFefo->date);

                DB::table('fefo')->where('date', '=', $last->format('Y-m-d'))
                    ->update(['date' => $today->copy()->format('Y-m-d')]);
            }

            $lastRegisterBids = DB::table('lances')
                ->where('data', '<>', $today->copy()->format('Y-m-d'))
                ->orderBy('data', 'desc')->limit(1)
                ->first();

            if($lastRegisterBids){

                $last = Carbon::createFromFormat('Y-m-d', $lastRegisterBids->data);

                DB::table('lances')->where('data', '=', $last->format('Y-m-d'))
                    ->update([
                        'data' => $today->copy()->format('Y-m-d'),
                        'duracao_lance' =>  $today->copy()->addDay(1)->format('Y-m-d'),
                        'status_id' => Status::type(Status::PENDING)->id
                    ]);
            }
        }
    }
}
