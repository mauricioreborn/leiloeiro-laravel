<?php

namespace App\Core\Console\Commands;

use App\Jobs\Ping;
use Illuminate\Console\Command;
use Log;

class BolotinhaInit extends Command
{
    protected $signature = 'bolotinha:fix';

    protected $description = 'Fix bolotinha';

    public function handle()
    {
        Log::info("Seed orders");
        $this->call('db:seed', ['--class' => 'PopulateOrderStatusBackwardsSeeder']);
    }
}
