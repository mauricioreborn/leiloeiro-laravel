<?php

namespace App\Core\Console\Commands;

use App\Client\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FirstPurchase extends Command
{
    protected $signature = 'souk:go-horse-first-purchase';
    protected $description = '';

    public function handle()
    {
        if (!$this->confirm('tem certeza?')) {
            $this->info("$this->signature comando não executado");

            return false;
        }

        $clients = DB::table('pedidos')->select('id_cliente')->groupBy('id_cliente')->pluck('id_cliente');

        $progressBar = $this->output->createProgressBar(count($clients));

        foreach ($clients as $clientId){

            Log::info("$this->signature atualizando primeira compra para o cliente $clientId");

            $orders = Client::find($clientId)->order()->get()->groupBy('company_id');

            foreach($orders as $companyId => $companyOrders) {
                $firstOrder = $companyOrders->first();
                $firstOrder->first_purchase = true;
                $firstOrder->save();
            }

            $progressBar->advance();
        }

        $progressBar->finish();

        $this->info("Finalizado!");
    }
}