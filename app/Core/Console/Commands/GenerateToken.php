<?php

namespace App\Core\Console\Commands;

use App\Auth\User;
use App\Core\Helpers\JWT;
use Illuminate\Console\Command;

class GenerateToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'souk:generate-token {owner}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a JWT token to be used for other Souk applications.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $owner = $this->argument('owner');

        if ($owner != 'webhooks') {
            $user = User::where(['email' => 'soukbot@souk.com.br'])->first();

            if (isset($user->id)) {
                $jwt = JWT::encodeSA($user->id);

            } else {
                $jwt = JWT::encodeSA($owner);
            }
            $this->info('Your token is:');
            $this->comment($jwt);
        } else {
            $jwt = JWT::encodeSA($owner);
            $this->info('Your token is:');
            $this->comment($jwt);
        }
    }
}
