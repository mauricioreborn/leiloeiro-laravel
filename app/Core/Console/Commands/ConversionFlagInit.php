<?php

namespace App\Core\Console\Commands;

use App\ClientCompanies\ClientCompany;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ConversionFlagInit extends Command
{
    protected $signature = 'souk:conversionFlagInit';
    protected $description = 'Command to get client first order and bid';

    public function handle()
    {
        $this->info("{$this->signature}: Inicio: ".now()->toDateTimeString());

        $clientCompananies = ClientCompany::all();

        $bar = $this->output->createProgressBar($clientCompananies->count());
        $bar->start();

        foreach($clientCompananies as $clientCompany){

            $where = [
                'id_cliente' => $clientCompany->client_id,
                'company_id' => $clientCompany->company_id,
            ];

            $bid = DB::table('lances')
                ->where($where)
                ->select('created_at')
                ->orderBy('id', 'asc')
                ->first();

            $order = DB::table('pedidos')
                ->where($where)
                ->whereNull('lance_id')
                ->select('created_at')
                ->orderBy('id', 'asc')
                ->first();

            if($bid){
                $clientCompany->first_bid_at  = $bid->created_at;
            }

            if($order){
                $clientCompany->first_cart_at = $order->created_at;
            }

            $clientCompany->save();

            $bar->advance();
        }

        $bar->finish();
        $this->info("{$this->signature}: Fim: ".now()->toDateTimeString());
    }
}