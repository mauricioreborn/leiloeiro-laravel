<?php

namespace App\Core\Console\Commands;

use App\Jobs\Ping;
use Illuminate\Console\Command;
use Log;

class DispatchPing extends Command
{
    protected $signature = 'souk:dispatch-ping';
    protected $description = 'Dispatch ping job';

    public function handle()
    {
        Ping::dispatch();
        Log::info("Dispatched Ping job");
    }
}
