<?php

namespace App\Core\Console\Commands;

use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\FileQueue\Services\FileQueueService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ImportBlacklistTirolez extends Command
{
    protected $signature = 'tirolez:blacklist';
    protected $bar;

    public function handle()
    {
        $this->log('PROCESSO INCIADO');

        $list = $this->getCnpjList();
        $clientsIds = $this->getClientsIds($list);

        $this->bar = $this->output->createProgressBar(count($clientsIds));
        $this->bar->start();

        foreach($clientsIds as $clientId) {
            $this->log("Atualizando cliente {$clientId}");

            ClientCompany::updateOrCreate([
                'client_id' => $clientId,
                'company_id' => 5,
            ], [
                'status' => 0,
                'deleted_at' => now(),
            ]);

            $this->bar->advance();
        }
        $this->bar->finish();

        $this->log('PROCESSO FINALIZADO');
    }

    protected function getCnpjList()
    {
        $this->log('Iniciando leitura de arquivo CSV');

        $valid = $invalid = [];
        $total = $duplicated = 0;

        (new FileQueueService())->downloadFile("https://souk-company.s3.amazonaws.com/TIROLEZ_BLACKLIST.csv",
            'TIROLEZ_BLACKLIST.csv');

        if (($handle = fopen(storage_path('app/TIROLEZ_BLACKLIST.csv'), 'r')) !== false) {
            $row = 0;

            while (($dataRow = fgetcsv($handle, 1000, ';')) !== false) {
                $row++;

                if($row === 1){
                    continue;
                }

                $total++;
                $cnpj = str_pad(
                    preg_replace(
                        '[^0-9]',
                        '',
                        (int) preg_replace('/\D/', '', $dataRow[0])
                    ),
                    14,
                    '0',
                    STR_PAD_LEFT
                );

                if(validCnpj($cnpj)){
                    if(isset($valid[$cnpj])){
                        $duplicated++;
                        continue;
                    }

                    $valid[] = $cnpj;
                }else{
                    $invalid[] = $cnpj;
                }
            }
        }

        $this->log('Finalizado leitura de arquivo CSV');

        if(count($invalid) > 0) {
            $invalidCnpj = implode(', ', $invalid);
            $this->log("CNPJs inválidos: {$invalidCnpj}");
        }

        if($duplicated > 0) {
            $this->log("CNPJs duplicados: {$duplicated}");
        }

        if(($total * 0.1) <= count($invalid)){
            Log::channel('slack')->error('Lista blacklist Tirolez com quantidade excessiva de erros (>10%)');
        }

        return $valid;
    }

    protected function getClientsIds($list)
    {
        $this->log('Buscando ID dos clientes');
        return Client::whereIn('cnpj', $list)->pluck('id');
    }

    protected function log($message)
    {
        Log::info("{$this->signature}: $message");
    }
}


