<?php


namespace App\Core\Console\Commands;

use App\Client\Mail\LoggableMail;
use App\Core\Entities\Status;
use App\Log\LogEmail;
use App\Notification\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AnnualSummary extends Command
{
    protected $signature = "souk:annual-summary {onlyPush?}";
    protected $description  = "Envio de email";

    protected $clients;
    protected $generalEconomy = 26000000.00;
    protected $generalVolume = "10.000";

    public function handle()
    {
        $this->info("Inicio ".now()->toDateTimeString());

        $onlyPush = $this->argument('onlyPush');

        $this->getEligibleClients();

        if($onlyPush == "send-push"){
            $this->sendPush();
        }else{
            $this->sendEmail();
        }

        $this->info("Fim ".now()->toDateTimeString());
    }

    function getEligibleClients()
    {
        $this->clients  = DB::table('clientes')
            ->where('confirm_email', 10)
            ->whereNotNull('email')
            ->select('id', 'nome', 'contact_name', 'email')
            ->get();

        $this->clients = collect($this->clients)->map(function($x){ return (array)$x; })->toArray();

        $orders =  DB::table('pedidos_produtos')
            ->join('pedidos', 'pedido_id', '=', 'pedidos.id')
            ->where('pedidos.status_id', Status::type(Status::APPROVED)->id)
            ->where('pedidos_produtos.status_id', Status::type(Status::APPROVED)->id)
            ->whereYear('data', '=', now()->format('Y'))
            ->select('produto_id AS product_code', 'total_kg', 'valor_kg', 'valor', 'pedido_id', 'lance_id',
                'data', 'company_id', 'cod_origem', 'semanas', 'id_cliente', 'cd_faixa_fefo')
            ->get();

        $orders = collect($orders)->map(function($x){ return (array)$x; })->toArray();

        $fefos = DB::table('fefo')
                ->whereYear('date', '=', now()->format('Y'))
                ->select('id', 'company_id', 'date', 'semanas', 'cod_origem', 'codigo_produto', 'valor_cheio',
                    'cd_faixa_fefo')
                ->get();

        $fefos = collect($fefos)->map(function($x){ return (array)$x; })->toArray();

        $products = DB::table('produtos')
                ->select('codigo', 'nome', 'foto', 'marca', 'company_id')
                ->get();

        $products = collect($products)->map(function($x){ return (array)$x; })->toArray();

        $productsParse = [];

        foreach($products as $prod){
            $hashProd = $prod['company_id']."_".$prod['codigo'];
            $productsParse[$hashProd] = $prod;
        }

        $fefoMaxValue = [];

        foreach ($fefos as $fefo){

            $hash = $fefo['company_id']."_".
                $fefo['cod_origem']."_".
                $fefo['date']."_".
                $fefo['codigo_produto']."_".
                $fefo['semanas']."_".
                $fefo['cd_faixa_fefo'];

            $fefoMaxValue[$hash] = (float) $fefo['valor_cheio'];
        }

        $ordersByClient = [];

        foreach($orders as $order){

            if(!isset($ordersByClient[$order['id_cliente']])){
                $ordersByClient[$order['id_cliente']] = [];
            }

            $ordersByClient[$order['id_cliente']][] = $order;
        }

        foreach($this->clients as $k => &$client){

            $client['withPurchase'] = false;

            if(isset($ordersByClient[$client['id']])){

                $client['withPurchase'] = true;
                $client['orders'] = 0;
                $totalOrders = [];
                $totalBids = [];
                $client['volume'] = 0;
                $client['bids'] = 0;
                $client['economy'] = 0;
                $client['generalEconomy'] = $this->generalEconomy;
                $client['products'] = [];

                foreach($ordersByClient[$client['id']] as $orderClient){

                    $hashOrderProduct = $orderClient['company_id']."_".$orderClient['product_code'];

                    if(!isset($client['products'][$hashOrderProduct])){
                        $client['products'][$hashOrderProduct] = $productsParse[$hashOrderProduct];
                        $client['products'][$hashOrderProduct]['volume'] = 0;
                    }

                    if(!is_null($orderClient['lance_id'])){
                        $totalBids[] = $orderClient['lance_id'];
                    }else{
                        $totalOrders[] = $orderClient['pedido_id'];
                    }

                    $client['volume'] += $orderClient['total_kg'];
                    $client['products'][$hashOrderProduct]['volume'] += $orderClient['total_kg'];

                    $hashFefo = $orderClient['company_id']."_".
                        $orderClient['cod_origem']."_".
                        $orderClient['data']."_".
                        $orderClient['product_code']."_".
                        $orderClient['semanas']."_".
                        $orderClient['cd_faixa_fefo'];

                    $fullValue = $fefoMaxValue[$hashFefo] * $orderClient['total_kg'];

                    $client['economy'] += $fullValue - (float) $orderClient['valor'];
                }

                $client['products'] = collect($client['products'])->sortByDesc('volume')
                    ->take(3)
                    ->toArray();

                $client['bids'] = count(array_unique($totalBids));
                $client['orders'] = count(array_unique($totalOrders));
            }
        }
    }

    function sendEmail()
    {
        $subject = 'Contato Souk | É hora de celebrar as conquistas de 2019!';

        foreach($this->clients as $client){

            if($client['withPurchase'] === false) {

                $view = "mail.notices.souk_yearly_review";

                $payload = [
                    'name' => $client['contact_name'] ?: $client['nome'],
                    'volume' => $this->generalVolume,
                    'economy' => price_br($this->generalEconomy),
                    //phpcs:ignore
                    'download_android' => "https://play.google.com/store/apps/details?id=intermediacoes.souk&referrer=utm_source%3D2019-email-fim-de-ano%26utm_medium%3De-mail%26utm_content%3DE-mail%2520de%2520fim%2520de%2520ano%25202019%26utm_campaign%3DFim%2520de%2520Ano%2520-%25202019",
                    //phpcs:ignore
                    'download_ios' => "https://apps.apple.com/app/apple-store/id1369376242?pt=119023613&ct=Email%20fim%20de%20ano%202019&mt=8",
                    'tracking_url' => '',
                ];
            }else{

                $view = "mail.notices.client_yearly_review";

                $payload = [
                    'name' => $client['contact_name'] ?: $client['nome'],
                    'orders' => $client['orders'],
                    'volume' => $client['volume'],
                    'bids' => $client['bids'],
                    //phpcs:ignore
                    'economy' => ($client['economy'] == 0) ? price_br($this->generalEconomy) : price_br($client['economy']),
                    //phpcs:ignore
                    'nps_url' => "https://pt.surveymonkey.com/r/pesquisa-satisfacao-final-de-ano?client_id={$client['id']}",
                    'tracking_url' => '',
                    'products' => [],
                ];

                $count = 1;

                foreach($client['products'] as $product){

                    $payload['products'][] = [
                        'ranking' => $count,
                        'name' => $product['nome'],
                        //phpcs:ignore
                        'picture_url' => env('PRODUCT_IMAGES_BASE_URL')."souk-company/{$product['company_id']}/{$product['foto']}",
                        'brand' => $product['marca'],
                        'volume' => $product['volume'],
                    ];

                    $count++;
                }
            }

            $renderEmail = (new LoggableMail($view, $payload, $subject))->to($client['email']);
            $emailMd5 = md5($renderEmail->build()->render());
            $checkSend = (new LogEmail())->where('md5_mensagem', $emailMd5)->count();

            if($checkSend > 0){
                continue;
            }

            $payload['tracking_url'] = "https://api.souk.com.br/api/v1/emailopened?hash={$emailMd5}";

            Mail::send((new LoggableMail($view, $payload, $subject))->to($client['email']));

            (new LogEmail())->create([
                'page' => "Emails transacionais fim de ano|{$client['id']}",
                'subject' => $subject,
                'message' => $renderEmail->build()->render(),
                'emails' =>  $client['email'],
                'md5_mensagem' => $emailMd5
            ]);
        }
    }

    function sendPush()
    {
        $logEmail = DB::table('log_emails')
            ->where('tela', "like", "%Emails transacionais fim de ano%")
            ->where('read', '=', 0)
            ->select('tela')
            ->get();

        $sendPush = [];

        foreach($logEmail as $log){
            $data = explode('|', $log->tela);
            $sendPush[] = end($data);
        }

        foreach($this->clients as $client){

            if(!in_array($client['id'], $sendPush)){
                continue;
            }

            if($client['withPurchase'] === false) {

                $title = "2019 o ano de conquistas!";
                //phpcs:ignore
                $pushMessage = "Te enviamos um e-mail com as novidades para 2020. Faça como outras 2000 empresas que economizam com a SOUK, vem crescer com a gente! Acesse o e-mail para saber mais.";
            }else{

                $title = "É hora de celebrarmos o ano de 2019!";
                //phpcs:ignore
                $pushMessage = "Te enviamos um e-mail com um resumo da nossa parceria em 2019. Tem novidade vindo por aí. Vamos crescer juntos em 2020! Acesse o e-mail e confira!";
            }

            Notification::create([
                'type' => 0,
                'client_id' => $client['id'],
                'title' => $title,
                'message' => $pushMessage,
                'attributes' => '',
            ]);
        }
    }
}