<?php

namespace App\Core\Console\Commands;

use App\Jobs\BehaviourApprovalJob;
use Illuminate\Console\Command;

class BehaviourApproval extends Command
{
    protected $signature = 'souk:behaviour-approval';
    protected $description = 'Approve or reject bids based on previous users daily interactions';

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        dispatch(new BehaviourApprovalJob());
    }
}
