<?php

namespace App\Core\Console\Commands;

use Illuminate\Console\Command;
use App\Notification\Notification as AppNotification;
use Log;

class SendPush extends Command
{
    protected $signature = 'souk:go-horse-notification';
    protected $description = '';

    public function handle()
    {
        if (!$this->confirm('tem certeza?')) {
            $this->info("$this->signature comando não executado");

            return false;
        }

        $ClientId = [
            183545,
            185118,
            186749,
            187261,
            188962,
            190921,
            191167,
            191672,
            191898,
            198204,
            198358,
            204063,
            204344,
            207812,
            217376,
            229598,
            235802,
            236262,
            236354,
            237996,
            239621,
            241455,
            241493,
            241965,
            242167,
            256326,
            260001,
            260872,
            262994,
            266134,
            271668,
            277554,
            277570,
            285183,
            290061,
            292954,
            299110,
            308210,
            322938,
            324218,
            324579,
            325100,
            329695,
            348509,
            352758,
            355704,
        ];


        $progressBar = $this->output->createProgressBar(count($ClientId));

        foreach ($ClientId as $clientId){

            Log::info("$this->signature enviando push para o cliente $clientId");

            AppNotification::create([
                'type' => 0,
                'client_id' => $clientId,
                'title' => 'Problemas nos pedidos via carrinho! :(',
                //@phpcs:ignore
                'message' => "Desculpe-nos mas entre 01 e 16/10 tivemos problemas no envio para a Seara. Já resolvemos, mas infelizmente os pedidos serão cancelados."
            ]);

            $progressBar->advance();
        }

        $progressBar->finish();

        $this->info("Finalizado!");
    }
}