<?php

namespace App\Core\Console\Commands;

use App\Client\Client;
use App\Notification\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ComunicacaoStatus extends Command
{
    protected $signature = 'souk:comunicao-status';
    protected $description = 'Command to send email and push users about new status';

    public function handle()
    {
        $timestamp = now();
        Log::info("[{$this->signature}][{$timestamp}] Iniciando processo.");

        $clients = Client::whereNotNull('email')
            ->where('email_verified', 1)
            ->where('confirm_email', 10)
            ->select('id', 'name', 'contact_name', 'email')
            ->get()
            ->toArray();

        $timestamp = now();
        $total = count($clients);
        Log::info("[{$this->signature}][{$timestamp}] Enviando email e push para {$total} clientes.");

        foreach($clients as $client) {
            $timestamp = now();
            Log::info("[{$this->signature}][{$timestamp}] Enviando email para cliente {$client['id']}");
            Mail::send(
                'mail.notices.new_status',
                ['name' => $client['contact_name'] ?? $client['name']],
                function ($message) use($client) {
                    $message->to($client['email']);
                    $message->subject('Contato Souk | Alteramos alguns de nossos status de pedidos e lances. Confira!');
                });

            $timestamp = now();
            Log::info("[{$this->signature}][{$timestamp}] Enfileirando push para cliente {$client['id']}");
            Notification::create([
                'type' => 0,
                'client_id' => $client['id'],
                'title' => 'Novidades nos status',
                'message' => 'Alteramos alguns de nossos status de pedidos e lances. Enviamos para você um e-mail explicando melhor. Acesse e confira!',
            ]);
        }

        $timestamp = now();
        Log::info("[{$this->signature}][{$timestamp}] Processo finalizado.");
    }
}
