<?php

namespace App\Core\Console\Commands;

use App\Client\Client;
use App\Client\Mail\LoggableMail;
use App\Client\Notifications\MobileConfirmationNotification;
use App\Log\LogEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use App\Notification\Notification as AppNotification;
use Log;

class Email extends Command
{
    protected $signature = 'souk:email';

    public function handle()
    {
        $payload = [
            'tracking_url' => "https://api.souk.com.br/api/v1/emailopened?hash=olar",
            'link' => "https://api.souk.com.br/api/v1/emailopened?hash=olar",
            'duration' => '05/02/2020',
            'product' => [
                'name' => 'Presunto Cozido Fino & Solto 200g',
                'image_url' => 'https://s3.amazonaws.com/souk-company/1/5487.png',
                'company' => 'Seara',
                'shelf_life' => '6 Semanas',
                'discount_percentage' => 40,
                'price' => 'R$ 3,50/Un',
                'kg_price' => 'R$ 6,70/Kg',
            ],
        ];

        Mail::send((new LoggableMail('mail.campaign.boost', $payload, 'Email Impulsionamento')
        )->to([
            'tiago@souk.com.br',
            'jpedro.prado@gmail.com',
        ]));

    }
}


