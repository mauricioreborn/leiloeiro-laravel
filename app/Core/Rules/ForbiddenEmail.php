<?php

namespace App\Core\Rules;

use Illuminate\Contracts\Validation\Rule;

class ForbiddenEmail implements Rule
{
    protected $forbiddenDomains = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->forbiddenDomains = ['@seara.com.br'];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $email = strtolower($value);

        return !in_array(substr($email, strpos($email, '@')), $this->forbiddenDomains, true);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.forbiddenemail');
    }
}
