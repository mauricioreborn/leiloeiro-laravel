<?php

namespace App\Core\Contracts;


interface FilterInterface
{
    public function run($query, $param);
}
