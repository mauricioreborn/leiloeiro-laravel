<?php

namespace App\Core\Contracts;


interface  SearchInterface
{
    public function run(array $filters);
}
