<?php

namespace App\Core\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'status',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'displayName' => $this->displayName,
            ],
        ];
    }
}
