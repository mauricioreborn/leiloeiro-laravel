<?php

namespace App\Core\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request Request
     * @param  \Closure                 $next    Closure next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('Content-Language')) {
            App::setLocale($request->header('Content-Language'));
        }

        return $next($request);
    }
}