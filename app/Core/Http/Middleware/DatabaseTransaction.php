<?php

namespace App\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class DatabaseTransaction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request Request
     * @param  \Closure                 $next    Closure next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next)
    {
        return DB::transaction(function ($transaction) use ($request, $next) {
            $response = $next($request);

            if($response->exception || ($response instanceof Response && $response->getStatusCode() > 399))
            {
                if (app('env') != "testing") {
                    $transaction->rollback();
                }
            }

            return $response;
        });
    }
}


