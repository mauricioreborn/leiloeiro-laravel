<?php

namespace App\Core\Http\Middleware;

use App\Auth\User;
use App\Core\Helpers\JWT;
use Closure;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class ValidateServiceAccountJwt
{
    const SERVICE_ACCOUNT_TYPE = JWT::SERVICE_ACCOUNT_TYPE;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request Request
     * @param  \Closure                 $next    Closure next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasHeader('Authorization') || empty($request->header('Authorization'))) {
            throw new AuthenticationException;
        }

        $authorization = explode(' ', $request->header('Authorization'));

        if (count($authorization) < 2) {
            throw new AuthenticationException;
        }

        $jwt = $authorization[1];

        try {
            $token = JWT::decode($jwt);

            if ($token->uid === 'helipa') {
                $user = User::where(['email' => 'soukbot@souk.com.br'])->first();
            } else if ($token->uid === 'webhooks') {
                $user = User::where(['email' => 'webhooks@souk.com.br'])->first();
            } else {
                $user = User::where(['id' => $token->uid, 'root' => 1, 'email' => 'soukbot@souk.com.br'])->first();
            }

            if (isset($user->id)) {
                Auth::setUser($user);
            }
        } catch (Exception $e) {
            throw new AuthenticationException;
        }

        if ($token->type != ValidateServiceAccountJwt::SERVICE_ACCOUNT_TYPE || !isset($user->id)) {
            throw new AuthenticationException;
        }

        return $next($request);
    }
}
