<?php

namespace App\Core\Http\Middleware;

use Illuminate\Auth\AuthenticationException;
use Exception;

use App\Core\Helpers\JWT;
use App\Client\Client;
use App\Auth\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class ValidateJwt
{
    // TODO: remove JWT::SERVICE_ACCOUNT_TYPE
    const VALID_TYPES = [JWT::SERVICE_ACCOUNT_TYPE, JWT::USER_TYPE, JWT::CLIENT_TYPE];

    /**
     * Handle an incoming request.
     * @param Request $request Request
     * @param Closure $next    Next request
     *
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasHeader('Authorization') || empty($request->header('Authorization'))) {
            throw new AuthenticationException;
        }

        $authorization = explode(' ', $request->header('Authorization'));
        if (count($authorization) < 2) {
            throw new AuthenticationException;
        }
        $jwt = $authorization[1];

        try {
            $token = JWT::decode($jwt);
        } catch (Exception $e) {
            throw new AuthenticationException;
        }

        if (!isset($token->type) || !isset($token->uid) || !in_array($token->type, ValidateJwt::VALID_TYPES)) {
            throw new AuthenticationException;
        }

        switch($token->type){
            case JWT::USER_TYPE:
                $user = User::with(['company.companySettings'])->findOrFail($token->uid);

                Auth::setUser($user);

                if(null === $user){
                    throw new AuthenticationException;
                }
                break;

            case JWT::CLIENT_TYPE:
                $client = Client::with('company', 'company.companySettings')->findOrFail($token->uid);

                Auth::setUser($client);

                if(null === $client){
                    throw new AuthenticationException;
                }
                break;
        }

        return $next($request);
    }
}
