<?php

namespace App\Core\Http\Controllers;

use Illuminate\Support\Facades\DB;

class HealthCheckController extends Controller
{
    public function index()
    {
        $return = DB::select("SELECT NOW();");
        return response()->json(['health' => true]);
    }

    public function ready()
    {
        $return = DB::select("SHOW GLOBAL VARIABLES LIKE 'innodb_read_only'");

        if ($return[0]->Value != 'OFF') {
            return response()->json([
                'errors' => 'read only is true',
                'health' => false
            ], 503);

        }
        return response()->json(['health' => true]);
    }

}
