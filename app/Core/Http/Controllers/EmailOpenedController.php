<?php

namespace App\Core\Http\Controllers;

use App\Log\LogEmail;
use Illuminate\Http\Request;

class EmailOpenedController
{
    public function __invoke(Request $request)
    {
        $hash = $request->hash;

        if($hash) {
            $email = LogEmail::where(['md5_mensagem' => $hash])->whereNull('read_at')->first();

            if($email !== null) {
                $email->update(['read_at' => now()]);
            }
        }

        //phpcs:ignore
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
    }
}
