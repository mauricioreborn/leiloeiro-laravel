<?php

namespace App\Core\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


/**
 * @SWG\Swagger(
 *   basePath="/",
 *   schemes={"https","http"},
 *   produces={"application/json"},
 *   consumes={"application/json"},
 *   host="localhost"
 * ),
 * @SWG\SecurityScheme(
 *   securityDefinition="authorization",
 *   type="oauth2",
 *   authorizationUrl="localhost/login",
 *   flow="accessCode",
 *   scopes={
 *     "read:all": "read all endpoints",
 *     "write:all": "write on all endpoints"
 *   }
 * )
 */

/**
 * @SWG\Info(
 *   version="1.0.0",
 *   title="Example of using references in swagger-php",
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}

/** @SWG\Response(
 *      response="Json",
 *      description="the basic response"
 * )
 */


