<?php
//phpcs:ignoreFile

use App\Mobile\Exceptions\MobileException;

/**
 * Sanitize an string to number by removing special characters.
 * Ex.: cnpj, phone, etc
 * @param string $value
 * @return string $value
 */

function sanitize_number($value)
{
    return preg_replace("/[^0-9]/", "", $value);
}

function price($value)
{
    return number_format($value, 2, '.', ',');
}

function price_br($value) {
    return number_format($value, 2, ',', '.');
}

function validate_phone($value) {
    $phoneNumber = preg_replace('/[^\d+]/', '', $value);

    if(!preg_match('/^\d{11}$/', $phoneNumber)) {
        throw new MobileException(400, __('Mobile/clientConfirmation.sms.errors.invalid_number'));
    }

    return $phoneNumber;
}

function formatPhone($value)
{
    if (!$value) {
        return false;
    }

    $ddd = substr($value, 0, 2);
    $fone1 = substr($value, 2, 5);
    $fone2 = substr($value, 7);

    return "({$ddd}) {$fone1}-{$fone2}";
}

/**
 * format date RFC3339 Extented to DateTime
 *
 * @param string $string Date Rfc3339 Extented formated
 *
 * @return string
 */
function rfc3339ExtentedToDateTime(?string $string)
{
    if ($string) {
        return now()->parse($string)->toDateTimeString();
    }

    return $string;
}

function format_cnpj($str): string {
    $str = preg_replace('/\D/', '', $str);
    $str = str_pad(preg_replace('[^0-9]', '', $str), 14, '0', STR_PAD_LEFT);

    return $str;
}

function maskCnpj($str): string {
    return mask((string) $str, '##.###.###/####-##');
}

function mask($val, $mask) {
    $maskared = '';

    $k = 0;
    for ($i = 0; $i<=strlen($mask)-1; $i++) {

    if($mask[$i] == '#') {
        if(isset($val[$k])) {
            $maskared .= $val[$k++];
        }
    } else {
        if(isset($mask[$i]))
            $maskared .= $mask[$i];
        }
    }

    return $maskared;
}

function calcDiscount(float $full, float $withDiscount) {
    if($full === 0 || $withDiscount === 0) {
        return 0;
    }

    return ceil((1-($withDiscount / $full))*100);
}

function validCnpj($cnpj)
{
    $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);

    if (strlen($cnpj) != 14){
        return false;
    }

    if (preg_match('/(\d)\1{13}/', $cnpj)){
        return false;
    }

    for ($i = 0, $j = 5, $sum = 0; $i < 12; $i++){
        $sum += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }

    $rest = $sum % 11;

    if ($cnpj{12} != ($rest < 2 ? 0 : 11 - $rest)){
        return false;
    }

    for ($i = 0, $j = 6, $sum = 0; $i < 13; $i++){
        $sum += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }

    $rest = $sum % 11;

    return $cnpj{13} == ($rest < 2 ? 0 : 11 - $rest);
}

function sanitizeCategory($category, $reverse = false)
{
    $array = [
        'INTEIRO/CARCACA AVES' => 'INTEIRO OU CARCACA AVES',
        'INTEIRO/CARCACA AVES TR' => 'INTEIRO OU CARCACA AVES TR',
        'KIBE/ALMONDEGA' => 'KIBE E ALMONDEGA',
    ];

    if ($reverse) {
        $array = array_flip($array);
    }

    if (isset($array[$category])) {
        $category = $array[$category];
    }

    return $category;
}

function getIp(): string
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function removeWhiteSpace($string) : string
{
    return str_replace(' ', '', $string);
}
