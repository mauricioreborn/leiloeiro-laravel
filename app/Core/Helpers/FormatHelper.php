<?php

namespace App\Core\Helpers;

use Illuminate\Support\Carbon;

class FormatHelper
{
    /**
     * Method to format volume
     * @param integer $value value param
     * @return string
     */
    static function volume($value)
    {
        return number_format($value, 0, ',', '.');
    }

    /**
     * Method to format value to price
     * @param integer $value price value
     * @return float
     */
    static function price($value)
    {
        return (float) number_format($value, 2, '.', '');
    }

    /**
     * Method to format a currency
     * @param integer $value      value of currency format
     * @param bool    $identifier indentifier variable
     * @return string
     */
     static function currencyFormat($value, $identifier = true)
     {
         if ($identifier) {
             return 'R$ ' . number_format($value, 2, ',', '.');
         }
         return number_format($value, 2, ',', '.');
     }

    /**
     * @param mixed  $value  Value
     * @param string $format Format
     * @return \Carbon\Carbon
     * @throws \Exception
     */
    static function transformDate($value, $format = 'd/m/Y')
    {
        try{
            return Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        }catch(\ErrorException $e){
            return Carbon::createFromFormat($format, $value);
        }
    }

    /**
     * @param float $value Value
     * @return float|int
     */
    static function percent($value)
    {
        return 100 * floatval(str_replace(',', '.', str_replace('%', '', $value)));
    }

    /**
     * Convert version string to int
     * @param Version $version version
     * @return int
     */
    static function convertAppVersion($version): int
    {
        $versionFinal = 0;

        if(is_array($version)){
            return $versionFinal;
        }

        $version = explode('.', $version);

        if(count($version) > 3){
            array_pop($version);
        }

        foreach($version as $key => &$ver){
            $versionFinal .= $ver;
        }

        return intval($versionFinal);
    }
}
