<?php

namespace App\Core\Helpers;

use Carbon\Carbon;
use Firebase\JWT\JWT as _JWT;

class JWT
{

    /**
     * JWT internal types
     */
    const SERVICE_ACCOUNT_TYPE  = 'service-account';
    const USER_TYPE             = 'user';
    const CLIENT_TYPE           = 'client';

    /**
     * Decode a JWT token.
     */
    public static function decode($token) {
        return _JWT::decode($token, config('app.jwt_key'), ['HS256']);
    }

    /**
     * Encode a JWT Token.
     */
    public static function encode($owner, $expiration, $type, $companyAmount = null) {
        $token = [
            "iat"  => Carbon::now()->timestamp,
            "iss"  => config('app.url'),
            "type" => $type,
            "uid"  => $owner,
        ];

        if ($companyAmount) {
            $token['companyAmount'] = $companyAmount;
        }

        if ($expiration !== null) {
            $token['exp'] = $expiration->timestamp;
        }

        return _JWT::encode($token, config('app.jwt_key'));
    }

    /**
     * Encode an User JWT Token.
     */
    public static function encodeUser($userId, $expiration = null) {
        if ($expiration === null) {
            $expiration = Carbon::now()->addDay();
        }
        return JWT::encode($userId, $expiration, JWT::USER_TYPE);
    }

    /**
     * Encode an Client JWT Token.
     */
    public static function encodeClient($userId, $expiration = null, $companyAmount = null) {
        if ($expiration === null) {
            $expiration = Carbon::now()->addDay();
        }
        return JWT::encode($userId, $expiration, JWT::CLIENT_TYPE, $companyAmount);
    }

    /**
     * Encode a Service Account JWT Token.
     */
    public static function encodeSA($owner) {
        return JWT::encode($owner, null, JWT::SERVICE_ACCOUNT_TYPE);
    }

}
