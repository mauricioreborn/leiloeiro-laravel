<?php

namespace App\Core\Helpers;

use App\Mobile\Exceptions\MobileException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Zipcode
{
    /**
     * Get zipcode data
     *
     * @param string|integer $zipcode Zipcode: "012000-010", 012000010
     *
     * @return array
     */
    public function get($value): array
    {
        $zipcode = str_replace(["-", "–"], '', $value);

        $cached = Cache::get("zipcode:{$zipcode}");

        if (is_null($cached)) {
            $body = $this->fetch($zipcode);

            if (array_get($body, 'erro')) {
                Log::error("VIACEP: {$zipcode} - NOT FOUND");

                return [];
            }

            $cached = [
                'address' => array_get($body, 'logradouro'),
                'zipcode' => array_get($body, 'cep'),
                'address_complement' => array_get($body, 'complemento'),
                'neighborhood' => array_get($body, 'bairro'),
                'city' => array_get($body, 'localidade'),
                'state' => array_get($body, 'uf'),
            ];

            Cache::forever("zipcode:{$zipcode}", $cached);
        }

        return $cached;
    }

    /**
     * Get API url
     *
     * @return string iugu api base url
     */
    protected function getApiUrl(): string
    {
        return 'https://viacep.com.br/ws/';
    }

    /**
     * Get API headers
     *
     * @return array request headers
     */
    protected function getHeaders(): array
    {
        return [
            'Accept' => 'application/json',
            'Accept-Charset' => 'utf-8',
            'Accept-Language' => 'pt-br;q=0.9,pt-BR',
            'Content-Type' => 'application/json'
        ];
    }

    /**
     * Fetch API request
     *
     * @param string|integer $zipcode Zipcode: "012000-010", 012000010
     *
     * @return array request headers
     */
    protected function fetch($zipcode): array
    {
        $payload = [
            'headers' => $this->getHeaders()
        ];

        try {
            $fetch = (new Client())->request(
                'GET',
                $this->getApiUrl() . $zipcode . '/json',
                $payload
            );

            return json_decode($fetch->getBody(), true);
        } catch (GuzzleException $e) {
            return $this->throwException($e);
        } catch (ClientException $e) {
            return $this->throwException($e);
        }
    }

    /**
     * throw Exception
     *
     * @param  Exception $exception Exception
     *
     * @return array
     */
    protected function throwException($exception)
    {
        $body = json_decode($exception->getResponse()->getBody()->getContents(), true);

        Log::error("VIACEP: {$exception->getCode()} - {$body}");

        return [];
    }
}
