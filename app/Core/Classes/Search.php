<?php

namespace App\Core\Classes;


use App\Core\Contracts\SearchInterface;
use App\Core\Exceptions\InvalidFilterException;
use Sofa\Eloquence\Builder;

/**
 * Class Search
 * @package App\Core\Classes
 */
class Search implements SearchInterface
{

    /**
     * @var Builder
     */
    private $query;

    /**
     * @var string
     */
    private $namespace;

    /**
     * @var array
     */
    static protected $reservedFilters = ['include', 'sort', 'limit', 'page'];

    /**
     * Search constructor.
     * @param         $module
     * @param Builder $query
     */
    public function __construct($module, Builder $query)
    {
        $this->query     = $query;
        $this->namespace = "App\\$module\\Http\\Factories\\Filters\\";
    }

    /**
     * Method to search by filters
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function run(array $filters)
    {
        foreach ($filters as $filter => $value) {
            if (\in_array($filter, self::$reservedFilters, true)) {
                if (\method_exists($this, $filter)) {
                    $this->query = $this->$filter($this->query, $value);
                }

                continue;
            }

            $filterClass = $this->namespace . ucfirst($filter);
            if (!class_exists($filterClass)) {
                throw new InvalidFilterException("Filter '{$filter}' not available for this request.");
            }

            $this->query = (new $filterClass())->run($this->query, $value);
        }

        return $this->query;
    }

    /**
     * @param Builder $query
     * @param         $value
     * @return Builder
     */
    public function include(Builder $query, $value)
    {
        return $query->with(explode(',', $value));
    }

    /**
     * @param $column
     * @return Builder
     */
    public function sort(Builder $query, $column)
    {
        $direction = $column[0] === '-' ? 'desc' : 'asc';
        $column    = ltrim($column, '-');

        return $query->orderBy($column, $direction);
    }
}
