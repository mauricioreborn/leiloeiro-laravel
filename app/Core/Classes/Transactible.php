<?php

namespace App\Core\Classes;

use App\Core\Entities\EntityHistory;
use Illuminate\Database\Eloquent\SoftDeletes;

trait Transactible
{
    protected $transactible = true;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            $model->_history($model->getNewStatus());
        });

        static::updated(function ($model) {
            $model->_history($model->getUpdatedStatus());
        });

        static::deleted(function ($model) {
            $model->_history($model->getDeletedStatus());
        });

        static::restored(function ($model) {
            $model->_history($model->getRestoredStatus());
        });
    }

    /**
     * Event dispatcher transaction history of model
     *
     * @param string $status Status
     *
     * @return void
     */
    private function _history($status)
    {
        if ($this->transactible) {
            $this->transaction($status);
        }
    }

    /**
     * Create transaction history of model
     *
     * @param string $status Status
     *
     * @return void
     */
    protected function transaction($status)
    {
        $except = [
            $this->getKeyName(),
            $this->getCreatedAtColumn(),
            $this->getUpdatedAtColumn()
        ];

        if ($this->soft_deleting) {
            array_push($except, $this->getDeletedAtColumn());
        }

        $entityHistory = [
            'table' => $this->getTable(),
            'parent_id' => $this->id,
            'status' => $status,
            'fields' => array_except($this->getAttributes(), $except),
        ];

        if (auth()->user()) {
            $class = strtolower(class_basename(auth()->user()));

            $entityHistory["{$class}_id"] = auth()->user()->id;
        }

        $history = EntityHistory::create($entityHistory);
    }

    /**
     * Get soft deleting Attribute
     *
     * @return bool
     */
    public function getSoftDeletingAttribute()
    {
        return in_array(SoftDeletes::class, class_uses($this)) && ! $this->forceDeleting;
    }

    /**
     * Get the name of the "NEW" status.
     *
     * @return string
     */
    public function getNewStatus()
    {
        return defined('static::NEW_STATUS') ? static::NEW_STATUS : 'new';
    }

    /**
     * Get the name of the "UPDATED" status.
     *
     * @return string
     */
    public function getUpdatedStatus()
    {
        return defined('static::UPDATED_STATUS') ? static::UPDATED_STATUS : 'updated';
    }

    /**
     * Get the name of the "DELETED" status.
     *
     * @return string
     */
    public function getDeletedStatus()
    {
        return defined('static::DELETED_STATUS') ? static::DELETED_STATUS : 'deleted';
    }

    /**
     * Get the name of the "RESTORED" status.
     *
     * @return string
     */
    public function getRestoredStatus()
    {
        return defined('static::RESTORED_STATUS') ? static::RESTORED_STATUS : 'restored';
    }
}
