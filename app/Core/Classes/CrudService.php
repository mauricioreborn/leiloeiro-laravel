<?php

namespace App\Core\Classes;


use Illuminate\Database\Eloquent\Model;

/**
 * Class CrudService
 * @package App\Core\Classes
 */
abstract class CrudService extends BaseService
{

    /**
     * @param  array $data
     *
     * @return Model
     */
    public function create(array $data = []): Model
    {
        $model = $this->factory($data);

        $this->save($model);

        return $model;
    }

    /**
     * Updates model data using $data.
     * The sequence performs the Model update.
     *
     * @param  mixed $model
     * @param  array $data
     *
     * @return boolean
     */
    public function update($model, array $data = []): bool
    {
        if (!$model instanceof Model) {
            $model = $this->find($model);
        }

        $this->setModelData($model, $data);

        return $this->save($model);
    }

    /**
     * Performs the save method of a model.
     * The goal is to allow the implementation of your business logic
     * before the command.
     *
     * @param  Model $model
     *
     * @return boolean
     */
    public function save($model)
    {
        return $model->save();
    }

    /**
     * Runs the delete command model.
     * The goal is to allow the implementation of your business logic
     * before the command.
     *
     * @param  mixed $model
     *
     * @return bool
     * @throws \Exception
     */
    public function delete($model): bool
    {
        if (!$model instanceof Model) {
            $model = $this->find($model);
        }

        return $model->delete();
    }

    /**
     * @param $model
     * @return mixed
     */
    public function restore($model)
    {
        if (!$model instanceof Model) {
            $model = $this->newQuery()->withTrashed()->find($model);
        }

        $model->restore();

        return $model->fresh();
    }

    /**
     * @param $model
     * @return bool|null
     */
    public function forceDelete($model): ? bool
    {
        if (!$model instanceof Model) {
            $model = $this->find($model);
        }

        return $model->forceDelete();
    }

    /**
     * Creates a model object with the $data information.
     *
     * @param  array $data
     *
     * @return Model
     */
    public function factory(array $data = []): Model
    {
        $model = $this->newQuery()->getModel()->newInstance();

        $this->setModelData($model, $data);

        return $model;
    }

    /**
     * @param Model $model
     * @param array $data
     */
    public function setModelData($model, array $data): void
    {
        $model->fill($data);
    }
}
