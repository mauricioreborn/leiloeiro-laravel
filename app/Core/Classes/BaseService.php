<?php

namespace App\Core\Classes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Company\Company;

/**
 * Class BaseService
 * @package App\Core\Classes
 */
abstract class BaseService
{
    /**
     * @var
     */
    protected $entity;

    /**
     * @var
     */
    protected $module;

    /**
     * @return mixed
     */
    protected function newQuery()
    {
        return (new $this->entity)->newQuery();
    }

    /**
     * Set entity Model
     * @param Model $entity entity
     * @return void
     **/
    protected function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param array   $filters filters
     * @param Company $company company
     * @return mixed
     */
    public function search($filters, ?Company $company = null)
    {
        $searchInstance = new Search($this->module, $this->newQuery());

        $query = $searchInstance->run($filters);

        if ($company != null) {
            $query->fromCompany($company->id);
        }

        return $query->paginate($filters['limit'] ?? 15);
    }

    /**
     * @param array $filters filters
     * @param bool  $limit   limit
     * @return mixed
     */
    public function searchWithoutPagination($filters, $limit = false)
    {
        $searchInstance = new Search($this->module, $this->newQuery());

        $query = $searchInstance->run($filters);

        if($limit) {
            $query->take($limit);
        }

        return $query->get();
    }

    /**
     * Returns all the records for the given Model. No pagination or sortable options.
     * @return mixed
     */
    public function all()
    {
        return (new $this->entity)->all();
    }

    /**
     * @param string $value value
     * @param string $key   key
     *
     * @return Collection
     */
    public function lists($value, $key)
    {
        return $this->newQuery()->pluck($value, $key);
    }

    /**
     * Retrieves a record by its id.
     * If $shouldThrowException is true, fires ModelNotFoundException.
     *
     * @param int     $id                   id
     * @param array   $relations            relations
     * @param boolean $shouldThrowException shouldThrowException
     *
     * @return Model
     */
    public function find($id, $relations = [], $shouldThrowException = true)
    {
        $query = $this->newQuery()->with($relations);

        if ($shouldThrowException) {
            return $query->findOrFail($id);
        }

        return $query->find($id);
    }

    /**
     * Rretrieves a entity by a specific column.
     *
     * @param string $column column
     * @param string $key    key
     *
     * @return Model|Collection
     */
    public function findBy($column, $key)
    {
        $query = $this->newQuery();

        $query->where($column, $key);

        return $query->first();
    }

    /**
     * Returns the latest result of a given table ordered by its id.
     * TODO: change to creation date and fix every table to have this column.
     * @return Collection
     */
    public function latest(): Collection
    {
        return $this->newQuery()->orderBy('id', 'desc')->first();
    }
}
