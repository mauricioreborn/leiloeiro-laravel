<?php
namespace App\Core\Classes;

use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use Faker\Provider\Base;

class SoukFakerProvider extends Base
{
    public function rejectionMotive($strict = [])
    {
        $motives = RejectionMotive::getMotives();

        if(count($strict) > 0) {
            $motives = array_filter($motives, function ($motive) use ($strict) {
                return in_array($motive, $strict, true);
            });
        }

        return $this->generator->randomElement($motives);
    }

    public function manyRejectionMotives($amount)
    {
        return $this->generator->randomElements(RejectionMotive::getMotives(), $amount);
    }

    public function status($strict = [])
    {
        $statuses = Status::getStatuses();

        if(count($strict) > 0) {
            $statuses = array_filter($statuses, function ($status) use ($strict) {
                return in_array($status, $strict, true);
            });
        }

        return $this->generator->randomElement($statuses);
    }

    public function manyStatuses($amount)
    {
        return $this->generator->randomElements(Status::getStatuses(), $amount);
    }
}