<?php

namespace App\Season;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeasonProduct extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    protected $fillable = [
        'id',
        'season_id',
        'product_code'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seasonType()
    {
        return $this->belongsTo(SeasonType::class, 'season_id');
    }
}
