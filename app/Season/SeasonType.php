<?php

namespace App\Season;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeasonType extends Model
{
    use Eloquence, Mappable,  SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'company_id',
        'start_season',
        'finish_season',
        'custom_colors'
    ];

    protected $casts = [
        'custom_colors' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function seasonProduct()
    {
        return $this->hasMany(SeasonProduct::class, 'season_id', 'id');
    }
}
