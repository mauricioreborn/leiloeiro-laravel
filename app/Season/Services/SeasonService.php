<?php

namespace App\Season\Services;

use Carbon\Carbon;

use App\Mobile\Exceptions\MobileException;
use App\Season\SeasonType;

class SeasonService
{
    /**
     * Method to get Products By a specific season
     * @param int $seasonId session id
     * @return array
     */
    public function getProductsBySeason(int $seasonId)
    {
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $seasonProducts = SeasonType::Select(
            'season_products.product_code',
            'season_types.name',
            'season_types.id'
        )->where('season_types.id', $seasonId)
            ->where('season_types.finish_season', '>=', $date)
            ->where('season_types.start_season', '<=', $date)
            ->join('season_products', 'season_types.id', '=', 'season_products.season_id')
            ->get();

        $products = [];
        foreach ($seasonProducts->toArray() as $key => $product) {
            $products[$product['name']][] = $product['product_code'];
        }
        return $products;
    }

    /**
     * Method to get all season products
     *
     * @param int $companyId company id
     * @return array
     */
    public function getAllSeasonProducts(int $companyId, $orderBy = 'name')
    {
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $seasonProducts = SeasonType::Select('season_products.product_code', 'season_types.name', 'season_types.id')
            ->where('company_id', $companyId)
            ->where('season_types.finish_season', '>=', $date)
            ->where('season_types.start_season', '<=', $date)
            ->join('season_products', 'season_types.id', '=', 'season_products.season_id')
            ->whereNull('season_products.deleted_at')
            ->orderBy("season_types.{$orderBy}")
            ->get();

        $products = [];
        foreach ($seasonProducts->toArray() as $key => $product) {
            $products[$product['name']][] = $product['product_code'];
        }
        return $products;
    }

    /**
     * Method to return season_colors
     *
     * @param string $seasonName color name
     * @param int    $companyId  company id
     * @return mixed
     */
    public function getCustomColorBySeasonName(string $seasonName, int $companyId = null)
    {
        $seasonColors = SeasonType::select('id', 'custom_colors')
            ->where('name', $seasonName);

        if ($companyId) {
            $seasonColors = $seasonColors->where('company_id', $companyId);
        }

        $seasonColors = $seasonColors->first();

        if (!isset($seasonColors->id)) {
            throw new MobileException(false, __('season.season_name_not_found'));
        }
        return $seasonColors->custom_colors;
    }
}
