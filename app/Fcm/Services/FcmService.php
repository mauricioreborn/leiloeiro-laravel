<?php


namespace App\Fcm\Services;

use App\Fcm\Fcm;

class FcmService
{
    /**
     * Method to update a Fcm Token
     * @param string $clientId Client ID
     * @param string $token    Token
     * @return bool
     */
    public function updateOrCreateFcm(string $clientId, string $token)
    {
        $fcmModel = new Fcm();

        $fcms = $fcmModel->where([
            'token' => $token
        ])->get();

        foreach ($fcms as $fcm) {
            if ($fcm->client_id != $clientId) {
                $fcm->delete();
            } else {
                $fcm->update();
            }
        }

        $fcms = $fcmModel->where([
            'token' => $token
        ])->get();
        
        if (count($fcms) == 0) {
           $fcmModel->create([
                'client_id' => $clientId,
                'token' => $token
            ]);
        }
        return true;
    }
}