<?php


namespace App\Fcm;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait
use Illuminate\Database\Eloquent\SoftDeletes;

class Fcm extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    protected $table = 'fcm_token';

    public $timestamps = true;

    protected $fillable = [
        'client_id',
        'token',
    ];
}