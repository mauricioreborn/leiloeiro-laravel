<?php

namespace App\QueueLoad\Services;

use App\Core\Classes\CrudService;
use App\QueueLoad\QueueLoad;

class QueueLoadService extends CrudService
{
    protected $entity = QueueLoad::class;
    protected $module = 'QueueLoad';
}
