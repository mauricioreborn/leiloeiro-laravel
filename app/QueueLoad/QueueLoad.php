<?php

namespace App\QueueLoad;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait

class QueueLoad extends Model
{
    use Eloquence, Mappable;

    protected $table = 'fila_cargas';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'file_name',
        'file_url',
        'total_success',
        'total_error',
        'total_duplicates',
        'status',
        'file_size',
        'file_type',
        'started_at',
        'finished_at'
    ];

    protected $appends = [
        'file_type',
    ];

    protected $hidden = [
        'tipo_carga',
    ];

    // // legacy db mapping
    protected $maps = [
        'file_type' => 'tipo_carga'
    ];
}
