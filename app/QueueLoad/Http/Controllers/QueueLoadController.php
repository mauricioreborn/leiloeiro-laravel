<?php

namespace App\QueueLoad\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\QueueLoad\Http\Requests\QueueLoadRequest;
use App\QueueLoad\Http\Resources\QueueLoadCollection;
use App\QueueLoad\Http\Resources\QueueLoadResource;
use App\QueueLoad\Services\QueueLoadService;
use App\QueueLoad\Http\Validators\QueueLoadValidator;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class QueueLoadController extends Controller
{

    protected $queueLoadService;

    /**
     * QueueLoadController constructor.
     * @param QueueLoadService $queueLoadService Service class
     */
    public function __construct(QueueLoadService $queueLoadService)
    {
        $this->queueLoadService = $queueLoadService;

    }

    /**
     * Method to get all queueLoads
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     */
    public function list(Request $request)
    {
        $collection = $this->queueLoadService->search($request->all());

        return QueueLoadResource::collection($collection);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  QueueLoadRequest $request Request object
     * @return QueueLoadResource
     */
    public function store(QueueLoadRequest $request)
    {
        return new QueueLoadResource($this->queueLoadService->create($request->all()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  QueueLoadRequest $request Request object
     * @param  integer          $id      Queueload ID
     * @return QueueLoadResource
     */
    public function update(QueueLoadRequest $request, $id)
    {
        $model = $this->queueLoadService->find($id);

        $this->queueLoadService->update($model, $request->all());

        return new QueueLoadResource($model->fresh());
    }
}
