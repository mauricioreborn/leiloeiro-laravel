<?php

Route::group(['prefix' => 'v1'], function (){
    Route::group(['prefix' => 'queueload'], function (){
        Route::get('/', 'QueueLoadController@list');
        Route::post('/', 'QueueLoadController@store');
        Route::put('/{id}', 'QueueLoadController@update');
    });
});
