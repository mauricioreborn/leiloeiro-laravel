<?php

namespace App\QueueLoad\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QueueLoadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'queueload',
            'id' => $this->id,
            'attributes' => [
                'file_name' => $this->file_name,
                'file_url'=>$this->file_url,
                'total_success'=>$this->total_success,
                'total_error'=>$this->total_error,
                'total_duplicates'=>$this->total_duplicates,
                'status'=>$this->status,
                'file_size'=>$this->file_size,
                'file_type'=>$this->file_type,
                'started_at'=>$this->started_at,
                'finished_at'=>$this->finished_at,
                'created_at'=>$this->created_at->toDateTimeString(),
            ]
        ];
    }
}
