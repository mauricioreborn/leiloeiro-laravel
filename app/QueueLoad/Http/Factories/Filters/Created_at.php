<?php

namespace App\QueueLoad\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;

class Created_at implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        $date = Carbon::createFromFormat('Y-m-d', $param);

        return $query->whereDate('created_at', '=', $date->format('Y-m-d'));
    }
}
