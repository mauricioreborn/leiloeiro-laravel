<?php

namespace App\QueueLoad\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Illuminate\Database\Query\Builder;

class Total_duplicates implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('total_duplicates', $param);
    }
}
