<?php

namespace App\QueueLoad\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use Illuminate\Database\Query\Builder;

class File_url implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->where('file_url', 'LIKE', '%'.$param.'%');
    }
}
