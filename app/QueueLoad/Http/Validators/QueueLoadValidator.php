<?php

namespace App\QueueLoad\Http\Validators;

use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;


class QueueLoadValidator
{
    public function validateSearchFilters($fields)
    {
        $validatorRule = [
            'file_name' => 'string|min:2',
            'file_url'=>'string|min:2',
            'total_success'=>'integer|min: 0',
            'total_error'=>'integer|min: 0',
            'total_duplicates'=>'integer|min: 0',
            'status'=>'string|min:0',
            'file_size'=>'integer|min:0',
            'file_type'=>'integer',
            'started_at'=>'date_format:Y-m-d H:i:s',
            'finished_at'=>'date_format:Y-m-d H:i:s',
            'limit' => 'integer|min:1',
        ];

        $validator = Validator::make($fields, $validatorRule);

        foreach ($fields as $fieldName => $fieldValue) {

            if (!isset($validatorRule[$fieldName])) {
                $validator->errors()->add($fieldName, "the field does not exist");
            }
        }

        if (count($validator->errors()) > 0) {
            throw new ValidationException($validator->errors(), $validator->errors());
        }
    }
}
