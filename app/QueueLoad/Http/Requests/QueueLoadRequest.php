<?php

namespace App\QueueLoad\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class QueueLoadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {
                return [
                    'file_name' => 'required|string|min:2',
                    'file_url'=>'required|string|min:2',
                    'total_success'=>'required|integer|min: 0',
                    'total_error'=>'required|integer|min: 0',
                    'total_duplicates'=>'required|integer|min: 0',
                    'status'=>'required|string|min:0',
                    'file_size'=>'required|integer|min:0',
                    'file_type'=>'required|integer',
                    'started_at'=>'date_format:Y-m-d H:i:s',
                    'finished_at'=>'date_format:Y-m-d H:i:s',
                ];
            }

            case 'PUT': {
                return [
                    'file_name' => 'string|min:2',
                    'file_url'=>'string|min:2',
                    'total_success'=>'integer|min: 0',
                    'total_error'=>'integer|min: 0',
                    'total_duplicates'=>'integer|min: 0',
                    'status'=>'string|min:0',
                    'file_size'=>'integer|min:0',
                    'file_type'=>'integer',
                    'started_at'=>'date_format:Y-m-d H:i:s',
                    'finished_at'=>'date_format:Y-m-d H:i:s',
                ];
            }
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
