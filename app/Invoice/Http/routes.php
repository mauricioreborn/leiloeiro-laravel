<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'invoices', 'middleware' => ['ValidateServiceAccountJwt']], function () {
        Route::put('/order/{order}/cancel', 'InvoiceController@cancel');
        Route::put('/order/{order}/capture', 'InvoiceController@capture');
        Route::put('/order/{order}/reopenOrder', 'InvoiceController@reopenOrder');
    });
});
