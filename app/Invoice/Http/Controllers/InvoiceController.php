<?php

namespace App\Invoice\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Invoice\Services\InvoiceService;
use App\Jobs\CaptureInvoiceJob;
use App\Order\Order;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * PaymentController constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Method to list company payments
     *
     * @param Order $order Order entity
     *
     * @return array
     */
    public function cancel(Order $order)
    {
        (new InvoiceService)->cancelOrder($order);

        return ['ok'];
    }

    /**
     * Method to update a company payments on client
     *
     * @param Order $order Order entity
     *
     * @return array
     */
    public function capture(Order $order)
    {
        (new InvoiceService)->captureOrder($order);

        return ['ok'];
    }

    /**
     * Method to reopen a order
     * @param Order $order order
     * @return array
     */
    public function reopenOrder(Order $order)
    {
        (new InvoiceService)->reopenOrder($order);

        return ['ok'];
    }
}
