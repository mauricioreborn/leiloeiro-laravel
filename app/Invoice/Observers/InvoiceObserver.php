<?php

namespace App\Invoice\Observers;

use App\Core\Entities\Status;
use App\Invoice\Invoice;
use App\Invoice\Services\InvoiceService;
use App\Jobs\IntegrateInvoiceJob;

class InvoiceObserver
{
    /**
     * Handle the Invoice "created" event.
     *
     * @param Invoice $invoice Invoice entity
     *
     * @return void
     */
    public function created(Invoice $invoice)
    {
        $created = Status::type(Status::CREATED);

        if ($invoice->status_id === $created->id) {
            IntegrateInvoiceJob::dispatch($invoice->id);
        }
    }
}
