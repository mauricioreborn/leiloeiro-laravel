<?php

namespace App\Invoice;

use App\Client\Client;
use App\Client\ClientCard;
use App\Core\Classes\Transactible;
use App\Core\Entities\Status;
use App\Order\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    const AUTHORIZATION = 'authorization';
    const ORDER = 'order';

    use Transactible, SoftDeletes;

    protected $fillable = [
        'id',
        'status_id',
        'client_card_id',
        'order_id',
        'type',
        'capture',
        'invoice_id',
        'transaction_number',
        'total',
        'months',
        'tax',
        'commission',
        'advance_fee',
        'paid',
        'overpaid',
        'due_date',
        'paid_at',
        'authorized_at',
        'expired_at',
        'refunded_at',
        'chargeback_at',
    ];

    /**
    * status relationship
    *
    * @return Status
    **/
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
    * invoice relationship
    *
    * @return ClientCard
    **/
    public function clientCard()
    {
        return $this->belongsTo(ClientCard::class);
    }

    /**
    * order relationship
    *
    * @return Order
    **/
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * scope authorization
     *
     * @param Builder $builder Builder
     *
     * @return Builder
     */
    public function scopeTypeAuthorization($builder)
    {
        return $builder->where('type', $this::AUTHORIZATION);
    }

    /**
     * scope ORDER
     *
     * @param Builder $builder Builder
     *
     * @return Builder
     */
    public function scopeTypeOrder($builder)
    {
        return $builder->where('type', $this::ORDER);
    }

    /**
     * return if invoice can be cancellable
     *
     * @return bool
     */
    public function cancellable(): bool
    {
        return in_array($this->status->name, [
            Status::INTEGRATED,
            Status::PENDING,
            Status::AUTHORIZED,
            Status::IN_ANALYSIS
        ]);
    }

    /**
     * return if invoice can be cancellable
     *
     * @return bool
     */
    public function refundable(): bool
    {
        return in_array($this->status->name, [
            Status::PAID,
        ]);
    }
}
