<?php

namespace App\Invoice\Services;

use App\Client\ClientCard;
use App\Client\Services\ClientCompanyPaymentService;
use App\Company\Company;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Invoice\Invoice;
use App\Iugu\Services\IuguService;
use App\Jobs\CancelInvoiceJob;
use App\Jobs\CaptureInvoiceJob;
use App\Jobs\SyncInvoiceJob;
use App\Order\Order;
use App\Order\Services\OrderService;
use App\Payment\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class InvoiceService
{
    /**
     * Constructor
     **/
    public function __construct()
    {
        $this->iuguService = resolve(IuguService::class);
    }

    /**
     * create authorization invoice to validate client card
     *
     * @param  ClientCard $clientCard ClientCard entity
     *
     * @return App\Invoice\Invoice
     */
    public function createAuthorization(ClientCard $clientCard): Invoice
    {
        return Invoice::create([
            'status_id' => Status::type(Status::CREATED)->id,
            'client_card_id' => $clientCard->id,
            'type' => Invoice::AUTHORIZATION,
            'total' => 1.00
        ]);
    }

    /**
     * create order invoice
     *
     * @param Order $order   Order entity
     * @param float $total   Value to reserve
     * @param bool  $capture Capture invoice immediately
     *
     * @return App\Invoice\Invoice
     */
    public function reserve(Order $order, float $total, bool $capture = false)
    {
        return Invoice::create([
            'status_id' => Status::type(Status::CREATED)->id,
            'client_card_id' => $order->client_card_id,
            'order_id' => $order->id,
            'type' => Invoice::ORDER,
            'months' => $order->cc_months,
            'total' => $total,
            'capture' => (int) $capture,
        ]);
    }

    /**
     * calculate order total and call reserve
     *
     * @param Order $order   Order entity
     * @param bool  $capture Capture invoice immediately
     *
     * @return self::reserve
     */
    public function calculateReserve(Order $order, bool $capture = false)
    {
        $orderTotal = (new OrderService)->calculatePriceOrder($order);

        $total = (new ClientCompanyPaymentService())->calculateTaxTotal(
            $orderTotal,
            $order->tax_percent
        );

        $invoiceTotal = (float) $order->invoices()
            ->where('status_id', '!=', Status::type(Status::CANCELED)->id)
            ->sum('total');

        if ($invoiceTotal !== $total) {
            if ($invoiceTotal > $total) {
                $this->cancelOrder($order);
            }

            if ($invoiceTotal < $total) {
                $total = $total - $invoiceTotal;
            }

            return $this->reserve($order, $total, $capture);
        }

        return $this->reserve($order, $total, $capture);
    }

    /**
     * integrate invoice
     *
     * @param Invoice $invoice Invoice entity
     *
     * @return App\Invoice\Invoice
     */
    public function integrate(Invoice $invoice)
    {
        Log::info($invoice);
        $clientCard = $invoice->clientCard()->withTrashed()->first();

        if ($invoice->type === Invoice::AUTHORIZATION) {
            $client = $clientCard->client;
            $items = [
                [
                    'description' => 'Valor simbolico',
                    'quantity' => 1,
                    'price_cents' => (int) ($invoice->total * 100)
                ]
            ];
        } else {
            $client = $invoice->order->client;
            $items = [
                [
                    'description' => "Pedido #{$invoice->order_id}",
                    'quantity' => 1,
                    'price_cents' => (int) ($invoice->total * 100)
                ]
            ];
        }

        $token = $clientCard->payment_method_id;

        $response = $this->iuguService->createCharge(
            $client,
            $token,
            $client->email,
            $items,
            $invoice->id,
            $invoice->months
        );

        $success = false;

        if ($response) {
            $success = $response['success'];

            $invoice->invoice_id = $response['invoice_id'];
        }

        Log::info($response);

        $invoice->save();

        /**
         * TODO: Need be called from webhooks invoice status.changed
         */
        if ($invoice->type === Invoice::AUTHORIZATION) {
            $clientCard = $invoice->clientCard()->withTrashed()->first();

            if ($success) {
                $clientCard->toAuthorized();

                CancelInvoiceJob::dispatch($invoice->id);
            } else {
                $clientCard->toRejected();
            }

            $clientCard->save();
        }

        if ($invoice->type === Invoice::ORDER) {
            $order = $invoice->order;

            Log::info($invoice->capture);
            Log::info($success);

            if ($success) {
                $statusId = Status::type(Status::PAID)->id;

                (new OrderService)->update($order, [ 'payment_status_id' => $statusId ]);

                if ($invoice->capture) {
                    CaptureInvoiceJob::dispatch($invoice->id);
                }
            } else {
                $statusId = Status::type(Status::REJECTED)->id;

                (new OrderService)->cancelOrder(
                    $order,
                    RejectionMotive::type(RejectionMotive::PAYMENT_DENIED),
                    $statusId
                );
            }

        }

        // NotificationJob::dispatch($invoice->id);

        return $success;
    }

    /**
     * Fire event to cancel invoice
     *
     * @param  Invoice $invoice Invoice entity
     *
     * @return void
     */
    public function cancel(Invoice $invoice)
    {
        Log::info("INVOICE: $invoice->id - REQUEST CANCEL");

        if ($invoice->cancellable()) {
            if ($invoice->invoice_id) {
                $response = $this->iuguService->cancelCharge($invoice->invoice_id);
            }

            $status = Status::type(Status::CANCELED);
        }

        if ($invoice->refundable()) {
            if ($invoice->invoice_id) {
                $response = $this->iuguService->refundCharge($invoice->invoice_id);
            }

            $status = Status::type(Status::REFUNDED);
        }

        if (isset($response)) {
            $invoice->status_id = $status->id;

            $invoice->save();

            Log::info("INVOICE: $invoice->id - $status->type");
        }
    }

    /**
     * Fire event to capture invoice
     *
     * @param  Invoice $invoice Invoice entity
     *
     * @return void
     */
    public function capture(Invoice $invoice)
    {
        Log::info("INVOICE: $invoice->id - REQUEST CAPTURE");

        if ($invoice->type === Invoice::ORDER) {
            $response = $this->iuguService->captureCharge($invoice->invoice_id);

            if ($response) {
                $invoice->status_id = Status::type($response['status'])->id;

                $invoice->save();

                Log::info("INVOICE: $invoice->id - PAID");
            }
        } else {
            Log::info("INVOICE: $invoice->id - cannot capture invoice because is a authorization");
        }
    }

    /**
     * 'cancel' invoice by order
     *
     * @param  Order $order Order entity
     *
     * @return bool
     */
    public function cancelOrder(Order $order)
    {
        return $this->fireEventOrder($order, 'cancel');
    }

    /**
     * 'capture' invoice by order
     *
     * @param  Order $order Order entity
     *
     * @return bool
     */
    public function captureOrder(Order $order)
    {
        if ($order->clientCompanyPayment->companyPayment->payment->type === Payment::CREDIT_CARD) {
            $orderTotal = (new OrderService)->calculatePriceOrder($order);

            $total = (new ClientCompanyPaymentService())->calculateTaxTotal(
                $orderTotal,
                $order->tax_percent
            );

            $invoiceTotal = (float) $order->invoices()
                ->where('status_id', '!=', Status::type(Status::CANCELED)->id)
                ->sum('total');

            if ($invoiceTotal > $total) {
                $this->cancelOrder($order);

                $this->reserve($order, $total, true);
            }

            if ($invoiceTotal < $total) {
                $notPaid = $total - $invoiceTotal;
                $this->reserve($order, $notPaid, true);

                Log::info('reservado: '.$notPaid);

                $this->fireEventOrder($order, 'capture');
            }

            if ($invoiceTotal === $total) {
                Log::info('vamos so capturar');

                $this->fireEventOrder($order, 'capture');
            }

            Log::info($total);
            Log::info($invoiceTotal);

            return true;
        }

        return false;
    }

    /**
     * Reopen a order
     * @param Order $order order
     * @return bool
     */
    public function reopenOrder(Order $order)
    {
        if ($order->clientCompanyPayment->companyPayment->payment->type === Payment::CREDIT_CARD) {
            $this->calculateReserve($order);
        }

        return false;
    }

    /**
     * Dispatch job to capture or cancel invoice by order
     *
     * @param  Order  $order Order entity
     * @param  string $event Events: capture, cancel
     *
     * @return bool
     */
    protected function fireEventOrder(Order $order, string $event)
    {
        $invoices = $order->invoices()->with('status')->get();

        foreach ($invoices as $invoice) {
            if ($invoice->status->name != Status::CANCELED) {
                if ($event == 'capture') {
                    CaptureInvoiceJob::dispatch($invoice->id);
                }

                if ($event == 'cancel') {
                    CancelInvoiceJob::dispatch($invoice->id);
                }
            }
        }

        return true;
    }

    /**
     * Method to sync invoices
     * @param string $invoiceId id of iugu invoice
     * @return null
     */
    public function syncInvoice(string $invoiceId)
    {
        $response = $this->iuguService->showInvoice($invoiceId);

        if ($response) {
            $invoice = Invoice::find($response['order_id']);

            if ($invoice) {
                $order = $invoice->order;

                $status = Status::type($response['status']);

                $invoice->status_id = $status->id;
                $invoice->invoice_id = $response['id'];
                $invoice->transaction_number = $response['transaction_number'];
                $invoice->total = $response['total_cents'] / 100;
                $invoice->tax = $response['taxes_paid_cents'] / 100;
                $invoice->commission = $response['commission_cents'] / 100;
                $invoice->advance_fee = $response['advance_fee_cents'] / 100;
                $invoice->paid = $response['total_paid_cents'] / 100;
                $invoice->overpaid = $response['overpaid_cents'] / 100;
                $invoice->due_date = $response['due_date'];
                $invoice->paid_at = rfc3339ExtentedToDateTime($response['paid_at']);
                $invoice->authorized_at = rfc3339ExtentedToDateTime($response['authorized_at_iso']);
                $invoice->expired_at = rfc3339ExtentedToDateTime($response['expired_at_iso']);
                $invoice->refunded_at = rfc3339ExtentedToDateTime($response['refunded_at_iso']);
                $invoice->canceled_at = rfc3339ExtentedToDateTime($response['canceled_at_iso']);
                $invoice->protested_at = rfc3339ExtentedToDateTime($response['protested_at_iso']);
                $invoice->chargeback_at = rfc3339ExtentedToDateTime($response['chargeback_at_iso']);

                $invoice->save();

                if ($status->name == Status::CANCELED) {
                    $rejected = Status::type(Status::REJECTED);

                    # TODO: Identify when reserve expired
                    /*if ($order) {
                        (new OrderService)->update($order, [ 'payment_status_id' => $rejected->id ]);

                        if ($order->isOpen) {
                            $this->calculateReserve($order);
                        }
                    }*/
                }

                return $invoice;
            }
        }
    }

    /**
     * process invoice event
     *
     * @param string $event Event fired
     * @param array  $data  Data
     *
     * @return bool
     */
    public function eventFired(string $event, $data): bool
    {
        $invoiceEvents = [
            'invoice.created',
            'invoice.status_changed',
            'invoice.refund',
            'invoice.payment_failed',
            'invoice.dunning_action',
            'invoice.due',
            'invoice.installment_released',
            'invoice.released'
        ];

        if (in_array($event, $invoiceEvents)) {
            SyncInvoiceJob::dispatch($data['id']);
        }

        return true;
    }

    /**
     * Method to get invoices from range date and company_id
     * @param Carbon  $start_at  start at date
     * @param Carbon  $finish_at finish at date
     * @param Company $company   company identifier
     * @return mixed
     */
    public function getInvoiceByRangeDateAndCompany(Carbon $start_at, Carbon $finish_at, Company $company)
    {
        return Invoice::whereBetween(
            'paid_at',
            [$start_at->toDateString(), $finish_at->toDateString()]
        )->where('type', 'order')
            ->whereNotNull('client_card_id')
            ->whereHas('Order', function ($query) use ($company) {
                $query->where('company_id', $company->id);
            })->get();
    }

    /**
     * Get total values
     * @param Order $order order object
     * @return mixed
     */
    public function getTotalByOrderId(Order $order)
    {
        return Invoice::where('order_id', $order->id)->sum('total');
    }
}
