<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;

class PaymentReportsExport implements FromArray
{
    use Exportable;

    protected $paymentReports = [];

    /**
     * Construct method of PaymentReports
     * PaymentReportsExport constructor.
     * @param array $paymentReports payment reports data
     */
    public function __construct(array $paymentReports)
    {
        $this->paymentReports = $paymentReports;

    }

    /**
     * Method to return an array
     * @return array
     */
    public function array() : array
    {
        return $this->paymentReports;
    }
}