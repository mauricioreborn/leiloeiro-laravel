<?php

namespace App\ClientsCompanies\Services;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\Seller;
use App\Client\SellerClient;
use App\Client\Services\ClientCompanyPaymentService;
use App\Client\Services\ClientService;
use App\Company\Company;
use App\Core\Entities\Status;
use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ClientsCompaniesService
{
    protected $totalColumns = 6;

    /**
     * Method to process account balance file job
     * @param FileQueue $fileQueue file queue object
     * @return bool
     */
    public function processAccountBalance(
        FileQueue $fileQueue,
        $client_code,
        $currentBalance,
        $overdueInvoice
    ) {
        $status = 1;

        $clientCompany = ClientCompany::where([
            'client_code' => $client_code,
            'company_id' => $fileQueue->company_id
        ])->first();

        $client = $clientCompany->client;

        if (!isset($client->id)) {
            $fileQueue->update([
                Status::type(Status::WITH_ERRORS)->id
            ]);
        }

        if ($currentBalance < $clientCompany->min_order) {
            $status = 0;
        }

        if ($currentBalance == 0 || $overdueInvoice > 0) {
            $status = 0;
        }

        $clientCompany->update([
            'account_balance' => $currentBalance,
            'status' => $status,
            'client_code' => $client_code
        ]);

        return $client;
    }

    /**
     * Method to get client a client companies
     * @param Company $company Company object
     * @param array   $clientData
     * @param array   $clientCompanyData
     * @param array   $sellerData
     * @param bool    $blocked
     * @return
     */
    public function processCreateClientCompanies(
        Company $company,
        $clientData = [],
        $clientCompanyData = [],
        $sellerData = []
    ) {
        // Pega o primeiro cliente referente ao CNPJ informado ou cria um novo registro
        $client = Client::firstOrCreate(['cnpj' => $clientData['cnpj']], $clientData);

        // Atualiza o primeiro cliente com as condições passadas ou cria um novo registro
        $clientCompany = ClientCompany::updateOrCreate([
            'company_id' => $company->id,
            'client_id' => $client->id
        ], array_merge($clientCompanyData, ['client_id' => $client->id]));

        // Atualiza o primeiro vendedor com as condições passadas ou cria um novo registro
        $seller = Seller::updateOrCreate(
            ['email' => $sellerData['email'], 'company_id' => $company->id],
            $sellerData
        );

        // Synca o vendedor com o cliente sem remover os outros que já estão ligados
        $client->seller()->syncWithoutDetaching([$seller->id]);

        (new ClientCompanyPaymentService())->getClientCompanyPaymentCredit($company, $client);

        return $clientCompany;
    }

    public function setCheckoutAt(ClientCompany $clientCompany, string $type = 'bid'): bool
    {
        $type = ($type != 'cart') ? 'bid' : $type;

        $label = "first_{$type}_at";

        if (!isset($clientCompany->$label)) {
            $clientCompany->$label = now();

            return $clientCompany->save();
        }

        return false;
    }
}
