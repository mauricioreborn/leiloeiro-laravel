<?php

namespace App\ClientCompanies;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Sofa\Eloquence\Eloquence;
use App\Company\Company;
use App\Client\Client;

class ClientCompany extends Pivot
{
    use Eloquence;

    protected $table = 'client_companies';

    protected $fillable = [
        'id', //necessary on pivot class
        'client_id',
        'client_code',
        'company_id',
        'status',
        'account_balance',
        'payment_conditions',
        'channel',
        'min_order',
        'address',
        'zip_code',
        'state',
        'city',
        'track_1',
        'track_2',
        'track_3',
        'track_4',
        'typology',
        'typology_code',
        'first_bid_at',
        'first_cart_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * Method to get tracks
     *
     * @return array
     */
    public function getTracksAttribute(): array
    {
        $tracks = [];

        if ($this->track_1 == 1) {
            $tracks[] = "001";
        }
        if ($this->track_2 == 1) {
            $tracks[] = "002";
        }
        if ($this->track_3 == 1) {
            $tracks[] = "003";
        }
        if ($this->track_4 == 1) {
            $tracks[] = "004";
        }
        return  $tracks;
    }

    /**
     * Method to relates the order class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Method to relates  a Company class with client class
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'client_companies');
    }
}
