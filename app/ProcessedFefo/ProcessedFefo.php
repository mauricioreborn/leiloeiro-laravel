<?php
namespace App\ProcessedFefo;

use App\Fefo\Fefo;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcessedFefo extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    protected $table = 'processed_fefos';

    protected $fillable = [
        'fefo_id',
        'scheduled_at',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fefo()
    {
        return $this->belongsTo(Fefo::class, 'fefo_id');
    }
}
