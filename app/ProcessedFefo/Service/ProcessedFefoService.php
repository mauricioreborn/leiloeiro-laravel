<?php
namespace App\ProcessedFefo\Service;

use App\ProcessedFefo\ProcessedFefo;
use Illuminate\Support\Carbon;

class ProcessedFefoService
{

    /**
     * Method to add an fefo to be processed
     *
     * @param int  $fefoId      fefo identifier
     * @param date $schedule_at schedule at
     * @return mixed
     */
    public function processedFefo($fefoId, $schedule_at = null)
    {
        if (!$schedule_at) {
            $startBusinessDay = now()->hour(7)->minute(0)->second(0);

            $endBusinessDay = now()->hour(18)->minute(0)->second(0);

            $endDay = now()->hour(21)->minute(0)->second(0);

            if (now()->between($startBusinessDay, $endBusinessDay, true)) {
                $schedule_at = now()->addMinutes(rand(10, 20));
            }

            if (now()->lt($startBusinessDay)) {
                $schedule_at = $startBusinessDay->copy()->addMinutes(rand(10, 20));
            }

            if (now()->gt($endBusinessDay)) {
                $schedule_at = now()->addMinutes(rand(30, 60));
            }

            if (now()->gt($endDay)) {
                $schedule_at = $startBusinessDay->copy()->addDay()->addMinutes(rand(10, 20));
            }
        }

        $processedFefo = ProcessedFefo::where([
            'fefo_id' => $fefoId
        ])->first();

        if (!isset($processedFefo->id)) {
            $processedFefo = ProcessedFefo::create([
                'fefo_id' => $fefoId,
                'scheduled_at' => $schedule_at
            ]);
        }
        return $processedFefo->id;
    }
}
