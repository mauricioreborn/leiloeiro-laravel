<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderShowResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'produtos' => $this->products,
            'status_pedido' => $this->status,
            'status_message' => $this->status_message,
            'status_class' => $this->status_class,
            'aprovado' => $this->approved,
            'pedido_id' => (string) $this->id,
            'dataPedido' => $this->date,
            'valorTotal' => $this->total_price,
            'endereco' => $this->address,
            'cancelado' => $this->when((!is_null($this->canceled)), $this->canceled),
            'motivoCancelamento' => $this->when((!is_null($this->canceled)), $this->cancellation_reason),
            'payment' => $this->payment,
            'paymentIcon' => $this->paymentIcon,
            'paymentQuota' => $this->paymentQuota ?? false,
            'totalProducts' => $this->totalProducts,
            'totalProductsAnalyzed' => $this->totalProductsAnalyzed,
        ];
    }
}
