<?php

namespace App\Mobile\Http\Resources;

use App\Mobile\Http\Resources\SellerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'access_token' => (string) $this['access_token'] ?: null,
            'aceita_compra' => (boolean) $this['aceita_compra'] ?: false,
            'ativo' => (boolean) $this['ativo'] ?: false,
            'client_id' => (string) $this['client_id'] ?: null,
            'cnpj' => (string) $this['cnpj'] ?: null,
            'endereco' => (string) $this['endereco'] ?: null,
            'phone' => (string) formatPhone($this['phone']),
            'email' => (string) $this['email'],
            'confirm_sms' => $this['confirm_sms'],
            'confirm_email' => $this['confirm_email'],
            'first_login' => (boolean) $this['first_login'] ?: false,
            'first_login_day' => (boolean) $this['first_login_day'] ?: false,
            'name' => (string) $this['name'] ?: null,
            'nome' => (string) $this['name'] ?: null,
            'novos_lances' => (string) $this['novos_lances'],
            'novos_pedidos' => (string) $this['novos_pedidos'],
            'status' => true,
            'total_lances' => (string) $this['total_lances'],
            'total_pedidos' => (string) $this['total_pedidos'],
            'vendedores' => SellerResource::collection($this['vendedores']),
            'forceupdate' => false,
            'cc_enabled' => (boolean) array_get($this, 'cc_enabled', false)
        ];
    }
}
