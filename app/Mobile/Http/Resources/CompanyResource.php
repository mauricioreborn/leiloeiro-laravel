<?php

namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        $newest = __("Mobile/company.$this->id.newest") === "false" ? false : true;
        $days = $this->companySettings->where('setting', 'billingDueDay' )->first();
        return [
            'type' => 'company',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'image' => $this->image,
                'quantity' => $this->quantity,
                'cart' => $this->cart_label,
                'bid' => $this->bid,
                'rules' => $this->rules,
                'aboult' => $this->aboult,
                'policy' => $this->policy,
                'information' => $this->information,
                'product_info' => $this->product_info,
                'payment' => __('Mobile/company.payment'),
                'payment_reserve_info' => __('Payment/infos.payment_reserve'),
                'image_color' => __("Mobile/company.$this->id.image_color"),
                'image_background' => __("Mobile/company.$this->id.image_background"),
                'image_products' => __("Mobile/company.$this->id.image_products"),
                'discount' => __("Mobile/company.$this->id.discount"),
                'payment_methods' => __('Mobile/company.payment_methods',[
                    'days' => $days->value,
                    'company' => $this->name
                ]),
                'conditions_rates' => __('Mobile/company.conditions_rates'),
                'payment_cards_info' => __('Mobile/company.payment_cards_info'),
                'newest' =>  $newest,
            ],
        ];
    }
}
