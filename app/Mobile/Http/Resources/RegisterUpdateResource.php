<?php

namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RegisterUpdateResource extends JsonResource
{
    /**
     * Method to format array
     * @param  array $request request array
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'register_updated',
            'attributes' => [
                'register_updated' => $this['registerUpdated'],
                'waiting_approval' => $this['waitingApproval']
                ]
            ];
    }
}