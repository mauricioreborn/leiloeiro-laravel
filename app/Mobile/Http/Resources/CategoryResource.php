<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'custom_view' => $this['custom_view'] ?? false,
            'area' => strtoupper($this['nome']),
            'total_produtos' => (string) $this['total_produtos']
        ];
    }
}
