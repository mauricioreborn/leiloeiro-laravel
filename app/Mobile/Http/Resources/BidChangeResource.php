<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BidChangeResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'dataEntrega' => (string) $this['dataEntrega'] ?: null,
            'dimensaoCaixa' => (string) $this['dimensaoCaixa'] ?: null,
            'empresa' => (string) $this['empresa'] ?: null,
            'gramatura' => $this['gramatura'] ?: null,
            'img' => (string) $this['img'] ?: null,
            'kgCaixas' => (string) $this['kgCaixas'] ?: null,
            'lanceAceiteParcial' => (boolean) $this['lanceAceiteParcial'] ?: false,
            'lanceDuracaoLance' => (string) $this['lanceDuracaoLance'] ?: null,
            'lanceQtdCaixas' => (string) $this['lanceQtdCaixas'] ?: null,
            'lanceTotalKg' => (string) $this['lanceTotalKg'] ?: null,
            'lanceValor' => (string) $this['lanceValor'] ?: null,
            'lanceValorKg' => (string) $this['lanceValorKg'] ?: null,
            'lance_id' => (string) $this['lance_id'] ?: null,
            'nome' => (string) $this['nome'] ?: null,
            'padraoMedida' => (string) $this['padraoMedida'] ?: null,
            'precoKg' => (string) $this['precoKg'] ?: null,
            'precoUn' => (string) $this['precoUn'] ?: null,
            'quantidadeCaixas' => $this['quantidadeCaixas'] ?: null,
            'quantidadeUn' => $this['quantidadeUn'] ?: null,
            'amountLabel' => $this['amountLabel'] ?: null,
            'textoFinal' => $this['textoFinal'] ?: null,
            'unidadeMedida' => (string) $this['unidadeMedida'] ?: null,
            'validade' => (string) $this['validade'] ?: null,
            'validadeData' => (string) $this['validadeData'] ?: null,
            'valorTotal' => (string) $this['valorTotal'] ?: null,
        ];
    }
}
