<?php

namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SellerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'company' => $this->when($this->company, function () {
                return [
                    'name' => $this->company->name,
                    'image' => $this->company->image,
                ];
            })
        ];
    }
}
