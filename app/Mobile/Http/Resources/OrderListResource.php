<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderListResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'aprovados' => $this->when(count($this['aprovados']), array_get($this, 'aprovados')),
            'pendentes' => $this->when(count($this['pendentes']), array_get($this, 'pendentes')),
            'reprovados' => $this->when(count($this['reprovados']), array_get($this, 'reprovados')),
        ];
    }
}
