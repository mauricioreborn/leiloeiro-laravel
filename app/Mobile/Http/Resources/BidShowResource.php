<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BidShowResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'status_lance' => $this['status_lance'],
            'status_message' => $this['status_message'],
            'status_class' => $this['status_class'],
            'can_edit' => $this['can_edit'],
            'lance_id' => $this['lance_id'],
            'dataPedido' => $this['dataPedido'],
            'validade' => $this['validade'],
            'dataEmbarque' => $this['dataEmbarque'],
            'dataEntrega' => $this['dataEntrega'],
            'validadeLance' => $this['validadeLance'],
            'valorTotal' => $this['valorTotal'],
            'endereco' => $this['endereco'],
            'aceitaParcial' => $this['aceitaParcial'],
            'validadeDias' => $this['validadeDias'],
            'produtos' => array_get($this, 'produtos'),
            'payment' => array_get($this, 'payment'),
            'paymentIcon' => array_get($this, 'paymentIcon', false),
            'paymentQuota' => array_get($this, 'paymentQuota', false),
            'textoFinal' => $this['textoFinal'] ?: false,
        ];
    }
}
