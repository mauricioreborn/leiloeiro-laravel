<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductListResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'msg' => $this->when(isset($this['msg']), array_get($this, 'msg')),
            'showOrder' => $this['showOrder'],
            'showFilter' => $this['showFilter'],
            'custom_colors' => $this->when(
                isset($this['custom_colors']), array_get($this, 'custom_colors')
            ),
            'nome' => (!empty($this['nome'])) ? ucfirst($this['nome']) : false,
            'total_produtos' => $this['total_produtos'],
            'valor_min' => $this->when(isset($this['valor_min']), array_get($this, 'valor_min')),
            'valor_max' => $this->when(isset($this['valor_max']), array_get($this, 'valor_max')),
            'valor_min_exibicao' => $this->when(isset(
                $this['valor_min_exibicao']
            ),
                array_get($this, 'valor_min_exibicao')
            ),
            'valor_max_exibicao' => $this->when(
                isset($this['valor_max_exibicao']), array_get($this, 'valor_max_exibicao')
            ),
            'classes' => $this->when(isset($this['classes']), array_get($this, 'classes')),
            'semanas' => $this->when(isset($this['semanas']), array_get($this, 'semanas')),
            'produtos' => $this['produtos'],
        ];
    }
}
