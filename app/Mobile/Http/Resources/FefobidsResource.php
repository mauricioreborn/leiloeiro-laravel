<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class FefobidsResource extends JsonResource
{

    /**
     * Method to load data
     * @param \Illuminate\Http\Request $request parameters of array
     * @return array
     */
    public function toArray($request): array
    {
        $response = [
            'type' => 'bid',
            'id' => $this->id,
            'attributes' => [
                'date' => Carbon::parse($this->date)->format('d/m'),
                'kg_price' => formata_moeda($this->kg_price),
                'price' => formata_moeda($this->price),
                'unitPrice' => (string) $this->unitPrice ?: null,
                'volume' => (string) $this->volume ?: null,
            ],
        ];

        return $response;
    }
}