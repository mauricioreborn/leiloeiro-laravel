<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BidAddResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'nome' => (string) $this['nome'] ?: null,
            'empresa' => (string) $this['empresa'] ?: null,
            'img' => (string) $this['img'] ?: null,
            'kgCaixas' => (string) $this['kgCaixas'] ?: null,
            'dimensaoCaixa' => (string) $this['dimensaoCaixa'] ?: null,
            'precoKg' => (string) $this['precoKg'] ?: null,
            'quantidadeCaixas' => $this['quantidadeCaixas'] ?: null,
            'validade' => (string) $this['validade'] ?: null,
            'validadeData' => (string) $this['validadeData'] ?: null,
            'dataEntrega' => (string) $this['dataEntrega'] ?: null,
            'valorTotal' => (string) $this['valorTotal'] ?: null,
            'textoFinal' => $this['textoFinal'] ?: null,
            'precoUn' => (string) $this['precoUn'] ?: null,
            'unidadeMedida' => (string) $this['unidadeMedida'] ?: null,
            'padraoMedida' => (string) $this['padraoMedida'] ?: null,
            'gramatura' => $this['gramatura'] ?: null,
            'quantidadeUn' => $this['quantidadeUn'] ?: null,
            'amountLabel' => $this['amountLabel'] ?: null,
            'lanceCodOrigem' => (string) $this['lanceCodOrigem'] ?: null,
            'lanceCodProduto' => (string) $this['lanceCodProduto'] ?: null,
            'lanceSemanas' => (string) $this['lanceSemanas'] ?: null,
            'lanceQtdCaixas' => (string) $this['lanceQtdCaixas'] ?: null,
            'lanceTotalKg' => (string) $this['lanceTotalKg'] ?: null,
            'lanceValorKg' => (string) $this['lanceValorKg'] ?: null,
            'lanceValor' => (string) $this['lanceValor'] ?: null,
            'lanceAceiteParcial' => (boolean) $this['lanceAceiteParcial'] ?: false,
            'lanceDuracaoLance' => (string) $this['lanceDuracaoLance'] ?: null
        ];
    }
}
