<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientConfirmationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'title' => $this['title'],
            'message' => $this['message'],
        ];
    }
}