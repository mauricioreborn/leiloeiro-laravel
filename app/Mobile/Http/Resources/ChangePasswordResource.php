<?php


namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChangePasswordResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        $returnArray = [
            'status' => $this['status'],
            'message' => $this['message'],
            'msg' => $this['message']
        ];

        if (isset($this['email_verified'])) {
            $returnArray['email_verified'] = $this['email_verified'];
        }
        return $returnArray;
    }
}
