<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BidListResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'pendentes' => count($this['pendentes']) ? array_get($this, 'pendentes') : false,
            'aprovados' => count($this['aprovados']) ? array_get($this, 'aprovados') : false,
            'cancelados' => count($this['cancelados']) ? array_get($this, 'cancelados') : false,
        ];
    }
}
