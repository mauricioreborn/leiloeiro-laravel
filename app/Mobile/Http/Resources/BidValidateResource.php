<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BidValidateResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => (boolean) true,
            'precoAtual' => $this['precoAtual'] ?: null,
            'precoAtualComparacao' => (string) $this['precoAtualComparacao'] ?: null,
            'quantidadeMinima' =>  $this['quantidadeMinima'],
            'valorMinimo' => (string) $this['valorMinimo'] ?: null,
            'pesoCaixa' => (string) $this['pesoCaixa'] ?: null,
            'disponivelExibicao' => (string) $this['disponivelExibicao'] ?: null,
            'kgDisponivel' => (string) $this['kgDisponivel'] ?: null,
            'caixasDisponivel' => (int) $this['caixasDisponivel'] ?: null,
            'nextWeekday' => (string) $this['nextWeekday'] ?: false,
            'visaoLogistica' => (boolean) $this['visaoLogistica'] ?: false,
            'dataVisaoLogistica' => (string) $this['dataVisaoLogistica'] ?: null,
            'prazoMaxPedido' => (string) $this['prazoMaxPedido'] ?: null,
            'precoAtualUn' => (string) $this['precoAtualUn'] ?: null,
            'unidadeMedida' => (string) $this['unidadeMedida'] ?: null,
            'padraoMedida' => (string) $this['padraoMedida'] ?: null,
            'gramatura' => $this['gramatura'] ?: null,
        ];
    }
}
