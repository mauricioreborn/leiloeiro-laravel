<?php


namespace App\Mobile\Http\Resources\SignUp;

use Illuminate\Http\Resources\Json\JsonResource;

class ZipcodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'zipcode',
            'attributes' => [
                'address' => array_get($this, 'address'),
                'zipcode' => array_get($this, 'zipcode'),
                'address_complement' => array_get($this, 'address_complement'),
                'neighborhood' => array_get($this, 'neighborhood'),
                'city' => array_get($this, 'city'),
                'state' => array_get($this, 'state'),
            ]
        ];
    }
}
