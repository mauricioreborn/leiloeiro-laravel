<?php
namespace App\Mobile\Http\Resources\SignUp;

use Illuminate\Http\Resources\Json\JsonResource;

class FirstStepResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => (boolean) $this['status'],
            'message' => $this['message'],
            'next_step' => $this['next_step'],
            'phone' => $this['phone'],
            'client_type' => $this['client_type'],
        ];
    }
}
