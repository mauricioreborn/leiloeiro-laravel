<?php
namespace App\Mobile\Http\Resources\SignUp;

use Illuminate\Http\Resources\Json\JsonResource;

class PhoneVerificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => (boolean) $this['status'],
            'message' => $this['message'],
            'phone' => $this['phone'],
        ];
    }
}
