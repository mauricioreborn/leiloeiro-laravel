<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductShowResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => true,
            'produto' => [
                'fefo_id' => array_get($this, 'fefo_id'),
                'codigo_produto' => (string) array_get($this, 'codigo_produto'),
                'img' => array_get($this, 'img'),
                'nome' => array_get($this, 'nome'),
                'empresa' => array_get($this, 'empresa'),
                'caixas' => array_get($this, 'caixas'),
                'pesoCaixa' => array_get($this, 'pesoCaixa'),
                'dimensaoCaixa' => array_get($this, 'dimensaoCaixa'),
                'validade' => array_get($this, 'validade'),
                'disponivel' => array_get($this, 'disponivel'),
                'preco' => (float) array_get($this, 'preco'),
                'precoAtual' => array_get($this, 'precoAtual'),
                'precoAtualUn' => array_get($this, 'precoAtualUn'),
                'unidadeMedida' => array_get($this, 'unidadeMedida'),
                'padraoMedida' => array_get($this, 'padraoMedida'),
                'gramatura' => array_get($this, 'gramatura'),
                'aceita_compra' => array_get($this, 'aceita_compra'),
                'msg_logistica' => array_get($this, 'msg_logistica'),
                'percentual_desconto' => array_get($this, 'percentual_desconto'),
                'label' => array_get($this, 'label'),
            ],
            'lances' => array_get($this, 'lances'),
            'total_produtos_carrinho' => array_get($this, 'total_produtos_carrinho'),
        ];
    }
}
