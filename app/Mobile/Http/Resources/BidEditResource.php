<?php
namespace App\Mobile\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BidEditResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status' => (boolean) true,
            'precoAtual' => $this['precoAtual'] ?: null,
            'precoAtualComparacao' => (string) $this['precoAtualComparacao'] ?: null,
            'quantidadeMinima' =>  $this['quantidadeMinima'],
            'valorMinimo' => (string) $this['valorMinimo'] ?: null,
            'pesoCaixa' => (string) $this['pesoCaixa'] ?: null,
            'disponivelExibicao' => (string) $this['disponivelExibicao'] ?: null,
            'kgDisponivel' => (string) $this['kgDisponivel'] ?: null,
            'caixasDisponivel' => (int) $this['caixasDisponivel'] ?: null,
            'lancePreco' => (string) $this['lancePreco'] ?: null,
            'lanceCaixas' => (string) $this['lanceCaixas'] ?: null,
            'aceitaParcial' => (boolean) $this['aceitaParcial'] ?: false,
            'dataVisaoLogistica' => (string) $this['dataVisaoLogistica'] ?: null,
            'prazoMaxPedido' => (string) $this['prazoMaxPedido'] ?: null,
            'validadeData' => (string) $this['validadeData'] ?: null,
            'validadeHoje' => (boolean) $this['validadeHoje'] ?: false,
            'precoAtualUn' => (string) $this['precoAtualUn'] ?: false,
            'precoUn' => (string) $this['precoUn'] ?: false,
            'unidadeMedida' => (string) $this['unidadeMedida'] ?: null,
            'padraoMedida' => (string) $this['padraoMedida'] ?: null,
            'gramatura' => $this['gramatura'] ?: false,
            'quantidadeUn' => $this['quantidadeUn'] ?: false,
            'amountLabel' => $this['amountLabel'] ?: null,
        ];
    }
}
