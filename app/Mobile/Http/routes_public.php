<?php

Route::group(['prefix' => 'clients'], function () {
    Route::get('/confirm_mail', 'ClientConfirmationController@confirmMail');
});
