<?php

namespace App\Mobile\Http\Middleware;

use App\Company\Company;
use App\Core\Helpers\FormatHelper;
use App\Mobile\Exceptions\MobileException;
use Closure;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;

class MobileAuthorization
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request $request
    * @param  \Closure                 $next
    *
    * @return mixed
    * @throws \Exception
    */
    public function handle($request, Closure $next)
    {
        Route::model('company', Company::class, function($value) {
            throw new MobileException(false, __('auth.access_denied'), ['token_valido' => false]);
        });

        Gate::after(function ($user, $ability, $result, $arguments) {
            if(!$result)
                throw new MobileException(false, __('auth.access_denied'));
        });

        $request->merge(['version' => FormatHelper::convertAppVersion($request->version)]);

        $request = $next($request);

        return $request;
    }
}
