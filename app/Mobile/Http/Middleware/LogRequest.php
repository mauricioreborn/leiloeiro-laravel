<?php

namespace App\Mobile\Http\Middleware;

use App\Log\LogAccess;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LogRequest
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request $request
    * @param  \Closure                 $next
    *
    * @return mixed
    * @throws \Exception
    */
    public function handle($request, Closure $next)
    {
        $start = microtime(true);

        $appVersion = $request->input('app_versao') ?? $request->input('version');

        $log = LogAccess::create([
            'client_id' => '',
            'client_independente_id' => '',
            'route' => $request->path(),
            'agent' => json_encode($request->header('User-Agent')),
            'versao_app' => $appVersion ?? 0,
            'post' => $request->isMethod('post') || $request->isMethod('put') ? json_encode($this->filtered($request->all())) : null,
            'get' => $request->isMethod('get') ? json_encode($this->filtered($request->all())) : null,
            'headers' => json_encode($request->headers->all()),
            'return_status' => '',
            'return_object' => '',
            'ip' => getIp(),
        ]);

        $request = $next($request);

        $client =  Auth::getUser();

        if ($client) {
            $log->client_id = $client->id;
        }

        $log->return_status = $request->getStatusCode();
        $log->return_object = $request->getContent();
        $log->execution_time = round((microtime(true) - $start) * 1000);
        $log->execution_time;
        $log->save();

        return $request;
    }

    protected function filtered($params)
    {
        if(isset($params['password']))
            $params['password'] = 'FILTERED';

        return $params;
    }
}
