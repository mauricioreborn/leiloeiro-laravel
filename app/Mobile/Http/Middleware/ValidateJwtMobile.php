<?php

namespace App\Mobile\Http\Middleware;

use App\Auth\User;
use App\Client\Client;
use App\Core\Helpers\JWT;
use App\Mobile\Exceptions\MobileException;
use App\Mobile\Services\MobileService;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class ValidateJwtMobile
{

    const VALID_TYPES = [JWT::SERVICE_ACCOUNT_TYPE, JWT::USER_TYPE, JWT::CLIENT_TYPE];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request next
     * @param  \Closure                 $next    next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next)
    {

        if ((!$request->has('access_token') || empty($request->get('access_token'))) &&
            (!$request->hasHeader('Authorization') || empty($request->header('Authorization')))) {
            throw new MobileException(false, __('auth.failed'), ['reason' => 'expired']);
        }

        if ($request->hasHeader('Authorization')) {
            $authorization = explode(' ', $request->header('Authorization'));
            if (count($authorization) < 2) {
                throw new MobileException(false, __('auth.failed'), ['reason' => 'expired']);
            }
            $jwt = $authorization[1];
        } else {
            $jwt = $request->get('access_token');
        }

        try {
            $token = JWT::decode($jwt);
        } catch (\UnexpectedValueException $e) {
            throw new MobileException(false, __('auth.failed'), ['reason' => 'expired']);
        }

        if (!isset($token->type) || !isset($token->uid) || !in_array($token->type, self::VALID_TYPES)) {
            throw new AuthenticationException;
        }

        switch ($token->type) {
            case JWT::USER_TYPE:
                $user = User::findOrFail($token->uid);

                if (null === $user) {
                    throw new MobileException(false, __('auth.failed'));
                }
                break;
            case JWT::CLIENT_TYPE:
                $mobileService = new MobileService();
                $client = $mobileService->checkToken($token->uid);

                $companyAmount = $client->companies->count();

                // TODO: Remover essa validação quando persistirmos o JWT
//                if (app('env') != 'testing') {
//                    if ($companyAmount > 1) {
//                        if (!isset($token->companyAmount) || $companyAmount != $token->companyAmount) {
//                            throw new MobileException(
//                                false,
//                                __('auth.failed'),
//                                ['reason' => 'expired', 'token_valido' => false]
//                            );
//                        }
//                    }
//
//                    if (isset($token->companyAmount) && $companyAmount != $token->companyAmount) {
//                        throw new MobileException(
//                            false,
//                            __('auth.failed'),
//                            ['reason' => 'expired', 'token_valido' => false]
//                        );
//                    }
//                }
                // FIM TODO

                Auth::setUser($client);
                break;
        }

        return $next($request);
    }
}
