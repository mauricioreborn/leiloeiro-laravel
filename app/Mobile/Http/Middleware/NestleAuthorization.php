<?php
namespace App\Mobile\Http\Middleware;

use App\Mobile\Exceptions\MobileException;
use App\Mobile\Http\Resources\RegisterUpdateResource;
use Closure;


class NestleAuthorization
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        /**
         * TODO: will be removed when all companies can access register updated
         */

        if($request->company->id != 4) {

            $register_updated = [
                'registerUpdated' => false,
                'waitingApproval' => false,
            ];

            return new RegisterUpdateResource($register_updated);

        }

        $request = $next($request);

        return $request;
    }
}