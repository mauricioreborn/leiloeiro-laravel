<?php

namespace App\Mobile\Http\Controllers;

use App\Mobile\Http\Requests\ProductShowRequest;
use App\Mobile\Http\Resources\ProductListResource;
use App\Mobile\Http\Resources\ProductShowResource;
use App\Mobile\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends MobileController
{

    /**
     * ProductController constructor.
     *
     * @param ProductService $productService product service
     *
     * @return void
     */
    public function __construct(ProductService $productService)
    {
        parent::__construct();
        $this->productService = $productService;
    }

    /**
     * Method to get home products
     *
     * @param Request $request request parameters
     *
     * @return ProductListResource
     */
    public function index(Request $request)
    {
        $products = $this->productService->getAreasProducts(
            $request->user(),
            $request->company,
            [
                'category' => $request->categoria_id,
                'type' => $request->filtro_classes,
                'weeks' => $request->filtro_semanas,
                'min_selling_price' => $request->filtro_vmin,
                'max_selling_price' => $request->filtro_vmax,
                'search' => $request->search_terms,
                'orderBy' => $request->ordenacao
            ]
        );

        return new ProductListResource($products);
    }

    /**
     * Method to get fefo hightLights
     *
     * @param Request $request request params
     *
     * @return ProductListResource
     */
    public function highLightFefo(Request $request)
    {
        $products = $this->productService->getAreasHighlightFefo(
            $request->user(),
            $request->company,
            $request->custom_view
        );
        return new ProductListResource($products);
    }

    /**
     * Method to get product information
     *
     * @param ProductShowRequest $request parameters
     *
     * @return ProductShowResource
     */
    public function show(ProductShowRequest $request): ProductShowResource
    {
        $product = $this->productService->show(
            $request->user(),
            $request->company,
            $request->produto_id,
            [
                'code' => $request->codigo_produto,
                'validate' => $request->validade,
                'origin_code' => $request->codigo_origem,
            ]
        );

        return new ProductShowResource($product);
    }
}
