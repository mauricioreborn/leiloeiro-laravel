<?php
namespace App\Mobile\Http\Controllers;

use App\Mobile\Http\Requests\SendConfirmationRequest;
use App\Mobile\Http\Requests\ConfirmSmsRequest;
use App\Mobile\Http\Requests\ConfirmMailRequest;
use App\Mobile\Http\Resources\ClientConfirmationResource;
use App\Mobile\Services\ClientService;
use Log;

class ClientConfirmationController extends MobileController
{
    protected $clientService;

    /**
     * @param ClientService $clientService ClientService instance
     */
    public function __construct(ClientService $clientService)
    {
        parent::__construct();
        $this->clientService = $clientService;
    }

    /**
     * @param SendConfirmationRequest $request Request instance
     * @return ClientConfirmationResource
     */
    public function send(SendConfirmationRequest $request)
    {
        return new ClientConfirmationResource(
            $this->clientService->issueConfirmation(
                $request->user(),
                $request->get('type'),
                $request->get('value')
            )
        );
    }

    /**
     * @param Request $request Request
     * @return ClientConfirmationResource
     */
    public function confirmSms(ConfirmSmsRequest $request)
    {
        return new ClientConfirmationResource($this->clientService->confirmSms(
            auth()->user(),
            $request->get('type'),
            $request->get('code')
        ));
    }

    /**
     * @param Request $request Request
     * @return ClientConfirmationResource
     */
    public function confirmMail(ConfirmMailRequest $request)
    {
        try {
            $response = new ClientConfirmationResource($this->clientService->confirmMail(
                $request->get('code')
            ));

            return redirect()->away(config('app.confirmation_mail.success'));
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->away(config('app.confirmation_mail.error'));
        }

    }
}
