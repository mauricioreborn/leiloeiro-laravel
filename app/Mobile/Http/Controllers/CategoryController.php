<?php

namespace App\Mobile\Http\Controllers;

use App\Mobile\Http\Resources\CategoryResource;
use App\Mobile\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends MobileController
{

    /**
     * @param CategoryService $categoryService default service
     *
     * @return void
    **/
    public function __construct(CategoryService $categoryService)
    {
        parent::__construct();
        $this->categoryService = $categoryService;
    }

    /**
     * Method to get categories
     *
     * @param Request $request sent params
     *
     * @return array
     */
    public function index(Request $request)
    {
        $categories = $this->categoryService->categoriesProductCount($request->user(), $request->company);

        return [
            'status' => true,
            'areas' => CategoryResource::collection($categories)
        ];
    }
}