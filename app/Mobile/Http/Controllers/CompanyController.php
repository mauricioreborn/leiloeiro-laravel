<?php

namespace App\Mobile\Http\Controllers;

use App\Mobile\Http\Resources\CompanyResource;
use App\Mobile\Services\CompanyService;
use Illuminate\Http\Request;

class CompanyController extends MobileController
{
    protected $companyService;

    /**
     * CompanyController constructor.
     * @param CompanyService $companyService Used to set the CompanyService
     */
    public function __construct(CompanyService $companyService)
    {
        parent::__construct();
        $this->companyService = $companyService;
    }

    /**
     * Client company list.
     * @param Request $request Used to set the request
     * @return CompanyResource
     */
    public function list(Request $request)
    {
        $client = $request->user();

        $client = $this->companyService->listByClientId($client);

        return CompanyResource::collection($client);
    }
}
