<?php

namespace App\Mobile\Http\Controllers;

use App\Mobile\Http\Requests\Cart\AddCartRequest;
use App\Mobile\Http\Requests\Cart\UpdateProductCartRequest;
use App\Mobile\Http\Requests\Cart\RemoveProductRequest;
use Illuminate\Http\Request;
use App\Mobile\Services\CartService;

class CartController extends MobileController
{

    protected $cartService;


    /**
     * KartController constructor.
     * @param CartService $cartService Service default
     * @return null
     */
    public function __construct(CartService $cartService)
    {
        parent::__construct();
        $this->cartService = $cartService;
    }

    /**
     * Method to list a cart
     *
     * @param Request $request params to list
     * @return array with cart
     */
    public function list(Request $request)
    {
        return $this->cartService->list($request->user(), $request->company);
    }

    /**
     * Method to add a new cart
     *
     * @param AddCartRequest $request params to create a cart
     * @return array created cart
     */
    public function addCart(AddCartRequest $request)
    {
        return $this->cartService->addCart(
            $request->user(),
            $request->company,
            $request->produto_id,
            $request->qtd_caixas
        );
    }

    /**
     * Method to update a cart
     *
     * @param UpdateProductCartRequest $request request params
     * @return array with method result
     */
    public function update(UpdateProductCartRequest $request)
    {
        return  $this->cartService->updateCart(
            $request->user(),
            $request->company,
            $request->produto_id,
            $request->qtd_caixas
        );
    }

    /**
     * Method to remove a item of cart
     * @param RemoveProductRequest $request cart id
     * @return array with method response
     */
    public function delete(RemoveProductRequest $request)
    {
        return  $this->cartService->removeProduct(
            $request->get('produto_id'),
            $request->user()->id,
            $request->company->id
        );
    }
}
