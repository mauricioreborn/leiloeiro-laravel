<?php

namespace App\Mobile\Http\Controllers;

use App\Core\Http\Controllers\Controller;

class MobileController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        \App::setLocale('pt-BR');
    }

    /**
     * Error without return
     *
     * @return void
     */
    public function error()
    {
        //
    }
}
