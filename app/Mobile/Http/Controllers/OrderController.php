<?php

namespace App\Mobile\Http\Controllers;

use App\Mobile\Http\Requests\OrderShowRequest;
use App\Mobile\Http\Requests\OrderFinishRequest;
use App\Mobile\Http\Resources\OrderListResource;
use App\Mobile\Http\Resources\OrderShowResource;
use App\Mobile\Http\Resources\OrderFinishResource;
use App\Mobile\Services\CartService;
use App\Mobile\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends MobileController
{

    protected $cartService;

    /**
     * Constructor
     * @param OrderService $orderService orderService
     * @param CartService  $cartService  cartService
     * @return void
    **/
    public function __construct(OrderService $orderService, CartService $cartService)
    {
        parent::__construct();

        $this->orderService = $orderService;
        $this->cartService = $cartService;
    }

    /**
     * Client orders list
     * @param Request $request request
     * @return OrderListResource
    **/
    public function clientOrders(Request $request)
    {
        $orders = $this->orderService->getClientOrders($request->user()->id);

        return new OrderListResource($orders);
    }

    /**
     * Client orders list
     * @param OrderShowRequest $request request
     * @return OrderShowResource
    **/
    public function show(OrderShowRequest $request)
    {
        $order = $this->orderService->show($request->pedido_id,  $request->company);

        return new OrderShowResource($order);
    }

    /**
     * Finish order from cart
     * @param Request $request request
     * @return array
    **/
    public function finish(OrderFinishRequest $request)
    {
        $response = $this->cartService->finish(
            $request->user(),
            [
                'aceita_parcial' => $request->aceita_parcial,
                'client_company_payment_id' => $request->client_company_payment_id,
                'client_card_id' => $request->client_card_id,
                'cc_months' => $request->cc_months,
            ],
            $request->company
        );

        return new OrderFinishResource($response);
    }
}
