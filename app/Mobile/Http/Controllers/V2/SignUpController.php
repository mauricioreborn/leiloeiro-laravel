<?php
namespace App\Mobile\Http\Controllers\V2;

use App\Core\Helpers\Zipcode;
use App\Mobile\Http\Controllers\MobileController;
use App\Mobile\Http\Requests\V2\SignUp\ContactInformationRequest;
use App\Mobile\Http\Requests\V2\SignUp\FirstStepRequest;
use App\Mobile\Http\Requests\V2\SignUp\PhoneValidationRequest;
use App\Mobile\Http\Requests\V2\SignUp\PhoneVerificationRequest;
use App\Mobile\Http\Requests\V2\SignUp\RegistrationRequest;
use App\Mobile\Http\Requests\V2\SignUp\ZipcodeVerificationRequest;
use App\Mobile\Http\Resources\SignUp\FirstStepResource;
use App\Mobile\Http\Resources\SignUp\PhoneValidationResource;
use App\Mobile\Http\Resources\SignUp\PhoneVerificationResource;
use App\Mobile\Http\Resources\SignUp\ZipcodeResource;
use App\Mobile\Services\SignUpService;
use Illuminate\Support\Facades\Log;

class SignUpController extends MobileController
{
    protected $zipcode;
    protected $signUpService;

    public function __construct(SignUpService $signUpService)
    {
        parent::__construct();
        $this->zipcode = resolve(Zipcode::class);
        $this->signUpService = $signUpService;
    }

    public function firstStep(FirstStepRequest $request)
    {
        return new FirstStepResource(
            $this->signUpService->verifyPreviousRegistrations($request->cnpj)
        );
    }

    public function phoneVerification(PhoneVerificationRequest $request)
    {
        return new PhoneVerificationResource(
            $this->signUpService->sendPhoneConfirmationCode(
                $request->cnpj,
                $request->phone
            )
        );
    }

    public function phoneValidation(PhoneValidationRequest $request)
    {
        return new PhoneValidationResource(
            $this->signUpService->confirmClientPhone(
                $request->cnpj,
                $request->validation_code
            )
        );
    }

    public function zipcodeVerification(ZipcodeVerificationRequest $request)
    {
        return new ZipcodeResource($this->zipcode->get($request->zipcode));
    }

    public function registration(RegistrationRequest $request)
    {
        return new FirstStepResource(
            $this->signUpService->registration(
                $request->cnpj,
                [
                    'social_reason' => $request->social_reason,
                    'state_registration' => $request->state_registration,
                    'zipcode' => $request->zipcode,
                    'address' => $request->address,
                    'state' => $request->state,
                    'city' => $request->city,
                    'address_number' => $request->address_number,
                    'neighborhood' => $request->neighborhood,
                    'address_complement' => $request->address_complement,
                ]
            )
        );
    }

    public function updateContactInformation(ContactInformationRequest $request)
    {
        return new FirstStepResource(
            $this->signUpService->updateContactInformation(
                $request->cnpj,
                $request->email,
                $request->contact_name,
                getIp()
            )
        );
    }
}
