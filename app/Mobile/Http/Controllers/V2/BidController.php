<?php

namespace App\Mobile\Http\Controllers\V2;

use Illuminate\Http\Request;

use App\Mobile\Http\Controllers\MobileController;
use App\Mobile\Services\BidService;
use App\Bid\Http\Resources\BidResource;

class BidController extends MobileController
{

    protected $bidService;

    /**
     * BidController constructor.
     * @param bidService $bidService bidService
     * @return void
     */
    public function __construct(BidService $bidService)
    {
        parent::__construct();
        $this->bidService = $bidService;
    }

    /**
     * show
     * @param Request $request request
     * @return array
     **/
    public function list(Request $request)
    {
        $bid = $this->bidService->list($request->user(), $request->all());

        return BidResource::collection($bid);
    }

    /**
     * show
     * @param Request $request request
     * @return array
     **/
    public function counterList(Request $request)
    {
        $bid = $this->bidService->counterList($request->user());
        return $bid;
    }
}
