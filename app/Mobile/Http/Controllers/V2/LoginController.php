<?php

namespace App\Mobile\Http\Controllers\V2;

use Illuminate\Http\Request;
use App\Mobile\Http\Controllers\MobileController;
use App\Mobile\Http\Resources\ChangePasswordResource;
use App\Auth\Services\AuthService;
use App\Mobile\Http\Requests\V2\RegisterRequest;

class LoginController extends MobileController
{
    /**
     * Register client
     * @param RegisterRequest $request Used to set the request
     * @return ChangePasswordResource
     */
    public function register(RegisterRequest $request): ChangePasswordResource
    {
        $authService = new AuthService();
        return new ChangePasswordResource($authService->registerClient($request->all()));
    }
}
