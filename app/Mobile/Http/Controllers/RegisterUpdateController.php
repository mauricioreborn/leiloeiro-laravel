<?php
namespace App\Mobile\Http\Controllers;

use App\Client\Services\ClientService;
use App\Mobile\Http\Resources\RegisterUpdateResource;
use Symfony\Component\HttpFoundation\Request;
use App\Company\Company;

class RegisterUpdateController extends MobileController
{
    /**
     * Method to show a register updated
     * @param Request $request request object
     * @param Company $company company object
     * @return RegisterUpdateResource resource
     */
    public function show(Request $request, Company $company)
    {
        $clientService = new ClientService();
        return new RegisterUpdateResource($clientService->getRegisterUpdate($company, $request->user()));
    }
}