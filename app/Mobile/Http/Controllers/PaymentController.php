<?php
namespace App\Mobile\Http\Controllers;

use App\Client\Services\ClientCompanyPaymentService;
use App\Company\Company;
use App\Payment\Http\Resources\ClientCompanyPaymentResource;
use Symfony\Component\HttpFoundation\Request;

class PaymentController extends MobileController
{
    /**
     * Method to get client payments
     * @param Request $request request to get user
     * @param Company $company company object
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function list(Request $request, Company $company)
    {
        $price = $request->price ?? 0;

        return ClientCompanyPaymentResource::collection(
            (new ClientCompanyPaymentService())->getClientEnabledPayments($company, $request->user(), $price)
        );
    }
}