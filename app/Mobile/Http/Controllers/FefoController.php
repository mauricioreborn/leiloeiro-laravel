<?php

namespace App\Mobile\Http\Controllers;

use App\Company\Company;
use App\Fefo\Fefo;
use App\Mobile\Http\Resources\FefobidsResource;
use App\Mobile\Services\FefoService;
use Illuminate\Http\Request;
use App\Fefo\Http\Policies\FefoCompanyPolicy;

class FefoController extends MobileController
{

    protected $fefoService;

    /**
     * FefoController constructor.
     * @param fefoService $fefoService bidService
     * @return void
     */
    public function __construct(FefoService $fefoService)
    {
        parent::__construct();
        $this->fefoService = $fefoService;
    }

    /**
     * Method to get bids by fefo
     * @param Company $company logged company
     * @param Fefo    $fefo    fefo identifier
     * @param Request $request request parameters
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getBidsByFefo(Company $company, Fefo $fefo, Request $request)
    {
        $this->authorize('clientAccess', $fefo);
        $bids = $this->fefoService->getBidsByFefo($fefo, $company);
        return FefobidsResource::collection($bids);
    }
}