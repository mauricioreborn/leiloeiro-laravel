<?php

namespace App\Mobile\Http\Controllers;

use App\Client\Services\ClientCompanyPaymentService;
use App\Fcm\Services\FcmService;
use App\Fefo\Services\FefoPriceBoostService;
use App\Mobile\Http\Requests\HomeRequest;
use App\Mobile\Services\BannerService;
use App\Mobile\Services\HomeService;
use App\Uuid\Services\UuidService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Predis\Connection\ConnectionException;

class HomeController extends MobileController
{
    protected $homeService;

    /**
     * HomeController constructor.
     *
     * @param HomeService $homeService generic service
     *
     * @return void
     */
    public function __construct(HomeService $homeService)
    {
        parent::__construct();
        $this->homeService = $homeService;
    }

    /**
     * Method to get home
     *
     * @param HomeRequest $request send params
     *
     * @return array data with params
     */
    public function index(HomeRequest $request)
    {
        if (isset($request->token_fcm)) {
            (new FcmService())->updateOrCreateFcm($request->user()->id, $request->token_fcm);
        }

        if (isset($request->uuid)) {
            (new UuidService())->updateOrCreateUuid($request->user()->id, $request->uuid);
        }

        $user = $request->user();
        $company = $request->company;

        $productsByCategory = $this->homeService->getHome($user, $company);

        $clientCompanyPaymentService = new ClientCompanyPaymentService();

        $cc_enabled = $clientCompanyPaymentService->creditCardEnabledWithCompany(
            $company,
            $user
        );

        $bannerService = new BannerService();
        $fefoPriceBoostService = new FefoPriceBoostService();

        $boosts = $fefoPriceBoostService->getMobileFefoPriceBoost($user, $company, $productsByCategory);

        $boost = $boosts != false ? $boosts: false;

        $bannerPromo = $bannerService->getPromoBanner($company, $user);

        $banner = $bannerService->getHomeBanner($company, $user, $request->version, $request->platform);

        $forceregister = false;

        if ($user->register_updated === false) {
            $forceregister = true;
        }

        return [
            'status' => true,
            'forceupdate' => false,
            'forceregister'=> $forceregister,
            'banner' => $banner,
            'product_boost' =>$boost,
            'banner_promo' => $bannerPromo,
            'categorias' => $productsByCategory
        ];
    }

    /**
     * Method to getLogistica
     *
     * @param Request $request request with params
     *
     * @return array data with logistics info
     */
    public function logisticaInfo(Request $request)
    {
        return $this->homeService->logisticaInfo($request->user(), $request->company->id);
    }
}
