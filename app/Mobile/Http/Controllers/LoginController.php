<?php

namespace App\Mobile\Http\Controllers;

use App\Mobile\Http\Requests\ChangePasswordRequest;
use App\Mobile\Http\Requests\RegisterUpdatedRequest;
use App\Mobile\Http\Requests\ResetPasswordRequest;

use App\Mobile\Http\Requests\LoginClientRequest;
use App\Uuid\Services\UuidService;
use Illuminate\Http\Request;
use App\Mobile\Http\Resources\ChangePasswordResource;

use App\Mobile\Http\Requests\AccessFirstRequest;
use App\Mobile\Http\Resources\AccessFirstResource;
use App\Auth\Services\AuthService;
use App\Client\Client;
use App\Mobile\Http\Requests\RegisterRequest;

use App\Mobile\Http\Resources\LoginResource;
use App\Mobile\Services\LoginService;
use App\Fcm\Services\FcmService;

class LoginController extends MobileController
{
    protected $loginService;

    /**
     * AuthClientController constructor.
     * @param LoginService $loginService Used to set the LoginService
     */
    public function __construct(LoginService $loginService)
    {
        parent::__construct();
        $this->loginService = $loginService;
    }

    /**
     * Client login.
     * @param LoginClientRequest $request Used to set the request
     * @return LoginResource
     */
    public function login(LoginClientRequest $request): LoginResource
    {
        $response = $this->loginService->login($request->cnpj, $request->password);

        if (isset($request->token_fcm)) {
            $fmcService = new FcmService();
            $fmcService->updateOrCreateFcm($response['client_id'], $request->token_fcm);

            if (isset($request->uuid)) {
                $uuIdService = new UuidService();
                $uuIdService->updateOrCreateUuid($response['client_id'], $request->uuid);
            }
        }

        return new LoginResource($response);
    }

    /**
     * Client check.
     * @param CheckClientRequest $request Used to set the request
     * @return LoginResource
     */
    public function check(Request $request): LoginResource
    {
        $client = $request->user();
        $token = $request->bearerToken() ?? $request->access_token;
        $response = $this->loginService->check($client, $token);

        return new LoginResource($response);
    }

    /**
     * Change client password
     * @param ChangePasswordRequest $request Used to set the request
     * @return ChangePasswordResource
     */
    public function changePassword(ChangePasswordRequest $request): ChangePasswordResource
    {
        $client = $request->user();

        return new ChangePasswordResource(
            $this->loginService->changePassword($client, $request->password,$request->new_password)
        );
    }

    /**
     * Reset client password
     * @param ResetPasswordRequest $request Used to set the request
     * @return ChangePasswordResource
     */
    public function resetPassword(ResetPasswordRequest $request): ChangePasswordResource
    {
        return new ChangePasswordResource($this->loginService->resetPassword($request->cnpj));
    }

    /**
     * First client access
     * @param AccessFirstRequest $request Used to set the request
     * @return AccessFirstResource
     */
    public function accessFirst(AccessFirstRequest $request): AccessFirstResource
    {
        $client = $request->user();
        $response = $this->loginService->accessFirst($client, $request->email, $request->telefone);
        return new AccessFirstResource($response);
    }

    /**
     * Register client
     * @param RegisterRequest $request Used to set the request
     * @return ChangePasswordResource
     */
    public function register(RegisterRequest $request): ChangePasswordResource
    {
        $authService = new AuthService();
        return new ChangePasswordResource($authService->registerClient($request->all()));
    }

    public function registerUpdated(RegisterUpdatedRequest $request)
    {
        $client = $request->user();
        return $this->loginService->registerUpdated($client, $request->company, $request);
    }
}
