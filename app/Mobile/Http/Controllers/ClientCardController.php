<?php

namespace App\Mobile\Http\Controllers;

use App\Client\ClientCard;
use App\Client\Http\Requests\ClientCardListRequest;
use App\Client\Http\Requests\ClientCardRequest;
use App\Client\Http\Resources\ClientCardResource;
use App\Client\Services\ClientCardService;
use App\Core\Http\Controllers\Controller;
use App\Mobile\Exceptions\MobileException;
use App\Mobile\Http\Requests\ClientCardDeleteRequest;
use Illuminate\Http\Request;

class ClientCardController extends Controller
{
    protected $clientCardService;

    /***
     * ClientCardController constructor.
     */
    public function __construct()
    {
        $this->clientCardService = new ClientCardService();
    }

    /**
     * Get Credit Cards
     * @param Request $request request
     * @return array
     **/
    public function list(ClientCardListRequest $request)
    {
        return ClientCardResource::collection($this->clientCardService->list(auth()->user(), $request->quotaPrice));
    }

    /**
     * Create a credit card for client
     * @param ClientCardRequest $request request
     * @return array
     **/
    public function post(ClientCardRequest $request)
    {
        $response = $this->clientCardService->create(
            auth()->user(),
            $request->name,
            $request->token
        );

        if ($response) {
            return new ClientCardResource($response);
        }

        throw new MobileException('false', __('Payment/errors.credit_card'));
    }

    /**
     * Delete client card
     *
     * @param ClientCard        $clientCard ClientCard entity
     * @param ClientCardRequest $request    Request
     *
     * @return ClientCardResource
     **/
    public function delete(ClientCard $clientCard, ClientCardDeleteRequest $request)
    {
        $response = $this->clientCardService->delete($clientCard);

        if ($response) {
            return new ClientCardResource($response);
        }

        throw new MobileException(false, __('Payment/clientCard.invoice_pending'));
    }
}
