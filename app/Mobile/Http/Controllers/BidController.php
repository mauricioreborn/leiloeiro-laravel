<?php

namespace App\Mobile\Http\Controllers;

use App\Mobile\Http\Requests\BidValidateRequest;
use App\Mobile\Http\Requests\BidAddRequest;
use App\Mobile\Http\Requests\BidChangeRequest;
use App\Mobile\Http\Requests\BidEditRequest;
use App\Mobile\Http\Requests\BidConfirmRequest;
use App\Mobile\Http\Requests\BidUpdateRequest;
use App\Mobile\Http\Requests\ShowBidRequest;
use App\Mobile\Http\Resources\BidValidateResource;
use App\Mobile\Http\Resources\BidAddResource;
use App\Mobile\Http\Resources\BidChangeResource;
use App\Mobile\Http\Resources\BidEditResource;
use App\Mobile\Http\Resources\BidListResource;
use App\Mobile\Http\Resources\BidShowResource;
use App\Mobile\Http\Resources\BidConfirmResource;
use App\Mobile\Services\BidService;

use Illuminate\Http\Request;

class BidController extends MobileController
{

    protected $bidService;

    /**
     * BidController constructor.
     * @param bidService $bidService bidService
     * @return void
     */
    public function __construct(BidService $bidService)
    {
        parent::__construct();
        $this->bidService = $bidService;
    }

    /**
     * Bid validate
     * @param BidValidateRequest $request request
     * @return BidValidateResource
     **/
    public function bidValidate(BidValidateRequest $request)
    {
        $client = $request->user();
        return new BidValidateResource($this->bidService->bidValidate($client, $request->fefo_id, $request->company));
    }

    /**
     * Add bid
     * @param BidAddRequest $request request
     * @return BidAddResource
     **/
    public function addBid(BidAddRequest $request)
    {
        $client = $request->user();

        return new BidAddResource($this->bidService->addBid(
            $client,
            $request->fefo_id,
            $request->preco,
            $request->quantidade,
            $request->tipoLance,
            $request->aceitaParcial,
            $request->dataEscolhida,
            $request->company
        ));
    }

    /**
     * Change bid
     * @param BidChangeRequest $request request
     * @return BidChangeResource
     **/
    public function changeBid(BidChangeRequest $request)
    {
        $client = $request->user();

        return new BidChangeResource($this->bidService->changeBid(
            $client,
            $request->lance_id,
            $request->preco,
            $request->quantidade,
            $request->tipoLance,
            $request->aceitaParcial,
            $request->dataEscolhida,
            $request->company
        ));
    }

    /**
     * Change bid
     * @param BidConfirmRequest $request request
     * @return array
     **/
    public function confirmBid(BidConfirmRequest $request)
    {
        $client = $request->user();

        $response = $this->bidService->confirmBid(
            $client,
            $request->cod_origem,
            $request->cod_produto,
            $request->aceite_parcial,
            $request->duracao_lance,
            $request->qtd_caixas,
            $request->semanas,
            $request->total_kg,
            $request->valor,
            $request->valor_kg,
            [
                'client_company_payment_id' => $request->client_company_payment_id,
                'client_card_id' => $request->client_card_id,
                'cc_months' => $request->cc_months,
            ],
            $request->company
        );

        return new BidConfirmResource($response);
    }

    /**
     * Edit bid
     * @param BidEditRequest $request request
     * @return BidEditResource
     **/
    public function editBid(BidEditRequest $request)
    {
        $client = $request->user();

        return new BidEditResource($this->bidService->editBid($client, $request->lance_id, $request->company));
    }

    /**
     * Update bid
     * @param BidUpdateRequest $request request
     * @return array
     **/
    public function updateBid(BidUpdateRequest $request)
    {
        $client = $request->user();
        
        $response = $this->bidService->updateBid(
            $client,
            $request->aceite_parcial,
            $request->duracao_lance,
            $request->lance_id,
            $request->qtd_caixas,
            $request->total_kg,
            $request->valor,
            $request->valor_kg,
            [
                'client_company_payment_id' => $request->client_company_payment_id,
                'client_card_id' => $request->client_card_id,
                'cc_months' => $request->cc_months ?? 1,
            ]
        );

        return new BidConfirmResource($response);
    }

    /**
     * List client bids
     * @param Request $request request
     * @return array
     **/
    public function listClientBids(Request $request): BidListResource
    {
        $bids = $this->bidService->listBidsByClientId($request->user()->id);

        return new BidListResource($bids);
    }

    /**
     * show
     * @param Request $request request
     * @return array
     **/
    public function show(ShowBidRequest $request): BidShowResource
    {
        $bid = $this->bidService->showClientBid($request->lance_id, $request->company);

        return new BidShowResource($bid);
    }

    /**
     * Remove bid
     * @param BidEditRequest $request request
     * @return array
     **/
    public function removeBid(BidEditRequest $request)
    {
        $client = $request->user();

        $response = $this->bidService->removeBid($client, $request->lance_id, $request->company);

        return $response;
    }
}
