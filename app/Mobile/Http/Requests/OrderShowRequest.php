<?php

namespace App\Mobile\Http\Requests;

use Illuminate\Support\Facades\Gate;

class OrderShowRequest extends MobileRequest
{
    /**
     * Method to validate params
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pedido_id' => "required|exists:pedidos,id,id_cliente,{$this->user()->id}",
        ];
    }
}
