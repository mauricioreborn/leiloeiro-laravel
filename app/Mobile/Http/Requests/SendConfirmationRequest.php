<?php

namespace App\Mobile\Http\Requests;

use App\Client\ClientConfirmation;
use App\Core\Rules\ForbiddenEmail;

class SendConfirmationRequest extends MobileRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type' => 'required|in:sms,mail',
            'value' => 'required',
        ];

        if($this->type === ClientConfirmation::TYPES['mail']) {
            $rules['value'] = ['required', 'confirmed', 'email', new ForbiddenEmail()];
        }

        return $rules;
    }

    /**
     * @return array
     **/
    public function messages()
    {
        $messages = [];

        if($this->type === ClientConfirmation::TYPES['mail']) {
            $messages = [
                'value.required' => 'O campo e-mail é obrigatório',
                'value.email' => 'Oops! Por favor digite um e-mail válido',
                'value_confirmation.required' => 'O campo confirmação de e-mail é obrigatório',
                'value.confirmed' => 'Oops! Os e-mails digitados não são iguais, verifique.',
            ];
        }

        return $messages;
    }
}
