<?php

namespace App\Mobile\Http\Requests;

use Illuminate\Support\Facades\Gate;

class ProductShowRequest extends MobileRequest
{
    public function rules()
    {
        return [
            'produto_id' => "required_without:codigo_produto,validade,codigo_origem",
            'codigo_produto' => "required_without:produto_id",
            'validade' => "required_without:produto_id",
            'codigo_origem' => "required_without:produto_id",
        ];
    }
}
