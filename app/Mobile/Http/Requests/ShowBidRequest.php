<?php

namespace App\Mobile\Http\Requests;

use Illuminate\Support\Facades\Gate;

class ShowBidRequest extends MobileRequest
{
    /**
     * Method to check rules on a bid request
     * @return array
     */
    public function rules()
    {
        return [
            'lance_id' => "required|exists:lances,id,id_cliente,{$this->user()->id}",
        ];
    }
}
