<?php

namespace App\Mobile\Http\Requests;

class ConfirmSmsRequest extends MobileRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:sms,mail',
            'code' => 'required'
        ];
    }
}
