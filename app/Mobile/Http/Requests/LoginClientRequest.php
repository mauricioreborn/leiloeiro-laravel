<?php

namespace App\Mobile\Http\Requests;

class LoginClientRequest extends MobileRequest
{
    /**
     * Method to prepare for validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj)
            ]
        ));
    }

    /**
     * Method to validate params
     *
     * @return array with success or failed
     */
    public function rules()
    {
        return [
            'cnpj'     => 'required|cnpj',
            'password' => 'required',
            'token_fcm' => 'max:191',
            'uuid' => 'max:191',
        ];
    }
}
