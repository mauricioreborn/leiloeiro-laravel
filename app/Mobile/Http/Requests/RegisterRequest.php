<?php

namespace App\Mobile\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use App\Mobile\Exceptions\MobileWithArrayValidationException;

class RegisterRequest extends MobileRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to prepare for validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj)
            ]
        ));
    }

    /**
     * Method to validate params
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|string',
            'email' => ['required','email',Rule::unique('clientes', 'email')->where(function ($query) {
                return $query->where('email_verified',  1);
            }),Rule::unique('clientes_independentes', 'email')->where(function ($query) {
                return $query->where('integrado',  0);
            })],
            'cnpj' => ['required','cnpj',Rule::unique('clientes', 'cnpj')->where(function ($query) {
                return $query->where('email_verified',  1);
            }), 'unique:clientes_independentes,cnpj'
                ],
            'telefone' => 'sometimes|required',
            
        ];
    }

    /**
     * Method to cast params
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cnpj' => __('auth.cnpj_invalid'),
            'email.unique' => __('auth.register.invalid_email'),
            'cnpj.exists' => __('auth.register.cnpj_exists'),
        ];
    }

    /**
     * Method throw new exception when failed validation
     * @param Illuminate\Contracts\Validation\Validator $validator validator
     * @return array
     */
    protected function failedValidation(Validator $validator)
    {
        throw new MobileWithArrayValidationException($validator);
    }
}
