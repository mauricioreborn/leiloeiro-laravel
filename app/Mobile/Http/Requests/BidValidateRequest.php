<?php

namespace App\Mobile\Http\Requests;

class BidValidateRequest extends MobileRequest
{
    public function rules()
    {
        return [
            'client_id' => 'integer',
            'fefo_id' => 'required|integer',
        ];
    }
}
