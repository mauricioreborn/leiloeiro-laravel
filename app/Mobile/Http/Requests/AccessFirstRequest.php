<?php


namespace App\Mobile\Http\Requests;


class AccessFirstRequest extends MobileRequest
{
    public function rules()
    {
        return [
            'client_id' => 'integer',
            'email' => 'required|email',
            'telefone' => 'required|integer',

        ];
    }
}
