<?php

namespace App\Mobile\Http\Requests;

class ConfirmMailRequest extends MobileRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required'
        ];
    }
}
