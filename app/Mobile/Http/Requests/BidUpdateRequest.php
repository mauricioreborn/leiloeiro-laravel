<?php

namespace App\Mobile\Http\Requests;

use App\Client\Rules\ClientCardRule;
use App\Client\Rules\ClientCompanyPaymentRule;

class BidUpdateRequest extends MobileRequest
{
    public function rules()
    {
        $clientCompanyPaymentRule = new ClientCompanyPaymentRule($this->company->id, $this->user()->id);
        $clientCardRule = new ClientCardRule($this->client_company_payment_id, $this->user()->id);

        return [
            'client_id' => 'integer',
            'lance_id' => 'required|integer',
            'aceite_parcial' => 'nullable',
            'duracao_lance' => 'date_format:Y-m-d|after_or_equal:today',
            'qtd_caixas' => 'required|integer',
            'total_kg' => 'required|numeric',
            'valor' => 'required|numeric',
            'valor_kg' => 'required|numeric',
            'client_company_payment_id' => $clientCompanyPaymentRule,
            'client_card_id' => $clientCardRule,
            'cc_months' => 'integer|required_with:client_card_id|between:1,12',
        ];
    }

    public function messages()
    {
        return [
            'duracao_lance.after_or_equal' => __('validation.after_or_equal.today')
        ];
    }
}
