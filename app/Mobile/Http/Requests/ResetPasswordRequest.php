<?php

namespace App\Mobile\Http\Requests;

class ResetPasswordRequest extends MobileRequest
{
    public function rules(){
        return [
            'cnpj' => 'required|cnpj|exists:clientes',
        ];
    }
}
