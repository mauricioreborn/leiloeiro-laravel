<?php
namespace App\Mobile\Http\Requests;

use App\Core\Rules\ForbiddenEmail;

class RegisterUpdatedRequest extends MobileRequest
{
    /**
     * Method to validate params
     *
     * @return array with fails
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'phone' => 'required',
            'email' => ['required', 'email', new ForbiddenEmail()],
            'ie' => 'required',
            'version' =>  'required'
        ];
    }
}