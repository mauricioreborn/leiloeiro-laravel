<?php

namespace App\Mobile\Http\Requests;

class HomeRequest extends MobileRequest
{
    /**
     * Method to validate params
     *
     * @return array with fails
     */
    public function rules()
    {
        return [
            'client_id' => 'integer',
            'token_fcm' => 'max:191',
            'uuid' => 'max:191'
        ];
    }
}
