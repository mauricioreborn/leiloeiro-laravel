<?php

namespace App\Mobile\Http\Requests;

class BidAddRequest extends MobileRequest
{
    /**
     * Rules
     * @return array
     **/
    public function rules()
    {
        return [
            'client_id' => 'integer',
            'fefo_id' => 'required|integer',
            'preco' => 'required|numeric',
            'quantidade' => 'required|integer',
            'tipoLance' => 'required|integer',
            'dataEscolhida' => 'date_format:d/m/Y|required_if:tipoLance,2|nullable|after:yesterday'
        ];
    }

    /**
     * Messages
     * @return array
     **/
    public function messages()
    {
        return [
            'dataEscolhida.after' => __('validation.after.yesterday')
        ];
    }
}
