<?php
namespace App\Mobile\Http\Requests\V2;

use App\Core\Rules\ForbiddenEmail;
use Illuminate\Validation\Rule;
use App\Mobile\Http\Requests\MobileRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Mobile\Exceptions\MobileWithArrayValidationException;

class RegisterRequest extends MobileRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to prepare for validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj)
            ]
        ));
    }

    /**
     * Method to validate request parameters
     *
     * @return array with validation
     */
    public function rules()
    {
        return [
            'nome' => 'required|string',
            'email' => ['required','email',new ForbiddenEmail(),Rule::unique('clientes', 'email')->where(function ($query) {
                return $query->where('email_verified',  1);
            })],
            'cnpj' => ['required','cnpj',Rule::unique('clientes', 'cnpj')->where(function ($query) {
                return $query->where('email_verified',  1);
            })],
            'telefone' => 'sometimes|required',
            'confirm_email' => 'required|email|same:email',
            'address_address' => 'sometimes|min:3',
            'zipcode' => 'sometimes|min:8',
            'city' => 'sometimes|min:2',
            'address_number' => 'sometimes|integer',
            'neighborhood' => 'sometimes|string',
            'social_reason' => 'sometimes|min:3',
            'address_complement' => 'nullable'
        ];
    }


    /**
     * Method to customize the default message
     *
     * @return array with parameter message formated
     */
    public function messages()
    {
        return [
            'cnpj' => __('auth.cnpj_invalid'),
            'email.unique' => __('auth.register.invalid_email'),
            'cnpj.exists' => __('Mobile\auth.cnpj_duplicated'),
            'neighborhood.string' => __('auth.register.neighborhood_string'),
            'address_number.integer' => __('auth.register.address_number_integer'),
            'address_complement.string' => __('auth.register.address_complement_string'),
            'address_address.min' => __('auth.register.address_address_min'),
            'city.min' => __('auth.register.city_min'),
            'social_reason.min' => __('auth.register.social_reason_min'),
        ];
    }

    /**
     * Method throw new exception when failed validation
     * @param Illuminate\Contracts\Validation\Validator $validator validator
     * @return array
     */
    protected function failedValidation(Validator $validator)
    {
        throw new MobileWithArrayValidationException($validator);
    }
}
