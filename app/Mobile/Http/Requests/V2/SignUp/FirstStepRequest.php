<?php
namespace App\Mobile\Http\Requests\V2\SignUp;

use App\Mobile\Http\Requests\MobileRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Mobile\Exceptions\MobileWithArrayValidationException;

class FirstStepRequest extends MobileRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj)
            ]
        ));
    }

    public function rules()
    {
        return [
            'cnpj' => 'required|cnpj',
        ];
    }

    public function messages()
    {
        return [
            'cnpj.*' => __('Mobile/signUp.validation.cnpj'),
        ];
    }
}
