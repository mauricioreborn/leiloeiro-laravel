<?php
namespace App\Mobile\Http\Requests\V2\SignUp;

use App\Core\Rules\ForbiddenEmail;
use Illuminate\Validation\Rule;
use App\Mobile\Http\Requests\MobileRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Mobile\Exceptions\MobileWithArrayValidationException;

class ContactInformationRequest extends MobileRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to prepare for validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj)
            ]
        ));
    }

    /**
     * Method to validate request parameters
     *
     * @return array with validation
     */
    public function rules()
    {
        return [
            'contact_name' => 'required',
            'email' => [
                'confirmed',
                'required','email',
                new ForbiddenEmail(),
                Rule::unique('clientes', 'email')
                    ->where(function ($query) {
                    return $query->where('email_verified',  1);
                })
            ],
            'cnpj' => 'required|cnpj',
        ];
    }

    /**
     * Method to customize the default message
     *
     * @return array with parameter message formated
     */
    public function messages()
    {
        return [
            'cnpj.*' => __('Mobile/signUp.validation.cnpj'),
            'email' => __('Mobile/signUp.validation.email'),
            'email.required' => __('Mobile/signUp.validation.email'),
            'email.unique' => __('Mobile/signUp.validation.email_unique'),
            'email.confirmed' => __('Mobile/signUp.validation.email_confirmed'),
        ];
    }
}
