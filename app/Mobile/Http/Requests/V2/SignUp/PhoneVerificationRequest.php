<?php
namespace App\Mobile\Http\Requests\V2\SignUp;

use App\Mobile\Http\Requests\MobileRequest;

class PhoneVerificationRequest extends MobileRequest
{
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj),
                'phone' => sanitize_number($this->phone),
            ]
        ));
    }

    public function rules()
    {
        return [
            'cnpj' => 'required|cnpj',
            'phone' => 'required|regex:/^([1-9]{2})([9]{1})([0-9]{8})$/',
        ];
    }

    public function messages()
    {
        return [
            'cnpj.cnpj' => __('validation.cnpj'),
            'phone.*' => __('Mobile/signUp.validation.phone'),
        ];
    }
}
