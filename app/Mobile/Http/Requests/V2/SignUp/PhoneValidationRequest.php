<?php
namespace App\Mobile\Http\Requests\V2\SignUp;

use App\Mobile\Http\Requests\MobileRequest;

class PhoneValidationRequest extends MobileRequest
{
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj),
                'phone' => preg_replace('/[^\d+]/', '', $this->phone),
            ]
        ));
    }

    public function rules()
    {
        return [
            'cnpj' => 'required|cnpj',
            'validation_code' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cnpj.cnpj' => __('validation.cnpj'),
            'validation_code.*' => __('Mobile/signUp.validation.validation_code'),
        ];
    }
}
