<?php
namespace App\Mobile\Http\Requests\V2\SignUp;

use Illuminate\Validation\Rule;
use App\Mobile\Http\Requests\MobileRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Mobile\Exceptions\MobileWithArrayValidationException;

class ZipcodeVerificationRequest extends MobileRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to validate request parameters
     *
     * @return array with validation
     */
    public function rules()
    {
        return [
            'zipcode' => 'required|regex:/^\d{5}-\d{3}$/',
        ];
    }

    /**
     * Method to customize the default message
     *
     * @return array with parameter message formated
     */
    public function messages()
    {
        return [
            'zipcode.*' => __('Mobile/signUp.validation.zipcode'),
        ];
    }
}
