<?php
namespace App\Mobile\Http\Requests\V2\SignUp;

use Illuminate\Validation\Rule;
use App\Mobile\Http\Requests\MobileRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Mobile\Exceptions\MobileWithArrayValidationException;

class RegistrationRequest extends MobileRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to prepare for validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'cnpj' => sanitize_number($this->cnpj)
            ]
        ));
    }

    /**
     * Method to validate request parameters
     *
     * @return array with validation
     */
    public function rules()
    {
        return [
            'cnpj' => 'required|cnpj',
            'social_reason' => 'required|string',
            'state_registration' => 'required|string',
            'zipcode' => 'required|regex:/^\d{5}-\d{3}$/',
            'address' => 'required|string|min:3',
            'city' => 'required|string|min:2',
            'state' => 'required|string|max:2',
            'address_number' => 'required|integer',
            'neighborhood' => 'required|string',
            'address_complement' => 'nullable'
        ];
    }

    /**
     * Method to customize the default message
     *
     * @return array with parameter message formated
     */
    public function messages()
    {
        return [
            'cnpj.*' => __('Mobile/signUp.validation.cnpj'),
            'zipcode.*' => __('Mobile/signUp.validation.zipcode'),
        ];
    }
}
