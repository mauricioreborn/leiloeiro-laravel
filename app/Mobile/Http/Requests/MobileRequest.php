<?php

namespace App\Mobile\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Mobile\Exceptions\MobileValidationException;

abstract class MobileRequest extends FormRequest
{
    function __construct() {}

    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new MobileValidationException($validator);
    }

    /**
     * Format the errors from the given Validator instance.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return array
     */
    protected function formatErrors(Validator $validator)
    {
        return current($validator->getMessageBag()->messages())[0];
    }
}
