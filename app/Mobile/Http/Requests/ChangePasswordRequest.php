<?php

namespace App\Mobile\Http\Requests;

use Illuminate\Http\Request;

class ChangePasswordRequest extends MobileRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to prepare for validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'md5Password' => md5($this->password)
            ]
        ));
    }

    /**
     * Method to validate params
     * @param Request $request request
     * @return array with success or failed
     */
    public function rules(Request $request)
    {
        return [
            'client_id'  => 'exists:clientes,id',
            'md5Password' => 'required|exists:clientes,password,id,'.$request->user()->id,
            'confirm_password' => 'required|same:new_password'
        ];
    }

    /**
     * Method to change messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'md5Password.exists' => 'Senha atual inválida'
        ];
    }
}
