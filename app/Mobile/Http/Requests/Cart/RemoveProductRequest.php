<?php
namespace App\Mobile\Http\Requests\Cart;

use App\Mobile\Http\Requests\MobileRequest;

class RemoveProductRequest extends MobileRequest
{
    /**
     * Rules
     * @return array
     **/
    public function rules()
    {
        return [
            'produto_id' => 'required',
        ];
    }
}
