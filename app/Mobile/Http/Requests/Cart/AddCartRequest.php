<?php

namespace App\Mobile\Http\Requests\Cart;

use App\Mobile\Http\Requests\MobileRequest;

class AddCartRequest extends MobileRequest
{
    public function rules()
    {
        return [
            'produto_id' => 'required',
            'qtd_caixas' => 'required|min:1',
        ];
    }
}
