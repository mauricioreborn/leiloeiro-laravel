<?php

namespace App\Mobile\Http\Requests;

class BidEditRequest extends MobileRequest
{
    public function rules()
    {
        return [
            'client_id' => 'integer',
            'lance_id' => 'required|integer',
        ];
    }
}
