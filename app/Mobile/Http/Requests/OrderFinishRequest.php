<?php

namespace App\Mobile\Http\Requests;

use App\Client\Rules\ClientCardRule;
use App\Client\Rules\ClientCompanyPaymentRule;
use Illuminate\Support\Facades\Gate;

class OrderFinishRequest extends MobileRequest
{
    /**
     * Method to validate params
     *
     * @return array
     */
    public function rules()
    {
        $clientCompanyPaymentRule = new ClientCompanyPaymentRule($this->company->id, $this->user()->id);
        $clientCardRule = new ClientCardRule($this->client_company_payment_id, $this->user()->id);

        return [
            'client_company_payment_id' => $clientCompanyPaymentRule,
            'client_card_id' => $clientCardRule,
            'cc_months' => 'integer|required_with:client_card_id',
        ];
    }

    /**
     * Method to prepare for validation
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'client_card_id' => $this->client_card_id
            ]
        ));
    }
}
