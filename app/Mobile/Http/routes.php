<?php

Route::group([
    'prefix'     => 'api/v1/mobile'
 ], function () {
    Route::post('error', 'MobileController@error');
    Route::post('login', 'LoginController@login');
    Route::post('cadastro', 'LoginController@register');
    Route::post('recuperar_senha', 'LoginController@resetPassword');
});

Route::group(['prefix' => 'api/v1/mobile', 'middleware' => ['validateJwtMobile', 'sentry']], function () {

    Route::get('companies', 'CompanyController@list');
    Route::post('check', 'LoginController@check');
    Route::post('alterar_senha', 'LoginController@changePassword');
    Route::post('primeiro_acesso', 'LoginController@accessFirst');
    Route::post('lista_pedidos', 'OrderController@clientOrders');
    Route::post('lances', 'BidController@listClientBids');

    Route::group(['prefix' => 'client'], function () {
        Route::get('/credit_cards', 'ClientCardController@list');
        Route::post('/credit_cards', 'ClientCardController@post');
        Route::delete('/credit_cards/{client_card}', 'ClientCardController@delete');

        Route::post('/request_confirmation', 'ClientConfirmationController@send');
        Route::post('/confirm_sms', 'ClientConfirmationController@confirmSms');
    });

    Route::group([
        'prefix' => 'company/{company}',
        'middleware' => 'can:clientAccess,company'
    ], function () {
        Route::post('categorias', 'CategoryController@index');
        Route::post('home', 'HomeController@index');
        Route::post('add_carrinho', 'CartController@addCart');
        Route::post('busca', 'ProductController@index');
        Route::post('produto', 'ProductController@show');
        Route::post('lista_produtos', 'ProductController@index');
        Route::post('lista_custom', 'ProductController@highLightFefo');
        Route::post('remove_item_carrinho', 'CartController@delete');
        Route::post('valida_lance', 'BidController@bidValidate');
        Route::post('add_lance', 'BidController@addBid');
        Route::post('altera_lance', 'BidController@changeBid');
        Route::post('confirma_lance', 'BidController@confirmBid');
        Route::post('edita_lance', 'BidController@editBid');
        Route::post('update_lance', 'BidController@updateBid');
        Route::post('remove_lance', 'BidController@removeBid');
        Route::post('carrinho', 'CartController@list');
        Route::post('change_item_carrinho', 'CartController@update');
        Route::post('pedido', 'OrderController@show');
        Route::post('finaliza_pedido', 'OrderController@finish');
        Route::post('lance', 'BidController@show');
        Route::post('logistica_info', 'HomeController@logisticaInfo');
        Route::get('fefo/{fefo}/bids', 'FefoController@getBidsByFefo');
        Route::get('client/payments', 'PaymentController@list');
        Route::post('register_updated', 'LoginController@registerUpdated');
        Route::get('register_updated', 'RegisterUpdateController@show')->middleware('nestleAuthorization');
    });
});

// V2

Route::group([
    'prefix'     => 'api/v2/mobile'
], function () {
    Route::post('cadastro', 'V2\LoginController@register');

    Route::group(['prefix' => 'signup'], function () {
        Route::post('/first-step', 'V2\SignUpController@firstStep');
        Route::post('/phone-verification', 'V2\SignUpController@phoneVerification')->middleware('throttle:1,0.5');
        Route::post('/phone-validation', 'V2\SignUpController@phoneValidation');
        Route::post('/zipcode-verification', 'V2\SignUpController@zipcodeVerification');
        Route::post('/registration', 'V2\SignUpController@registration')->middleware('throttle:1,0.5');
        Route::put('/contact-information', 'V2\SignUpController@updateContactInformation');
    });

    Route::group(['middleware' => ['validateJwtMobile', 'sentry']], function () {
        Route::get('bids', 'V2\BidController@list');
        Route::get('bids/counter', 'V2\BidController@counterList');
    });
});
