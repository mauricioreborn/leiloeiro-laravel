<?php
namespace App\Mobile\Traits;

use App\Client\Client;
use App\Client\IndependentClient;
use App\Mobile\Exceptions\MobileException;
use Illuminate\Support\Facades\Log;

trait ValidatesClientRegistration
{
    public function validateIndependentClient(IndependentClient $client)
    {
        $currentStep = $this->verifyRegistrationStepIndependent($client);
        $message = false;

        if ($currentStep === self::STEP_COMPLETED) {
            $message = __('Mobile/signUp.first_step.independent_client_exists');
            $currentStep = false;

            Log::info(
                "CADASTRO: Cliente independente de CNPJ {$client->cnpj} aguardando ".
                "resposta da indústria, tentou se cadastrar novamente"
            );
        }

        return [
            'status' => true,
            'message' => $message,
            'next_step' => $currentStep,
            'client_type' => 'independent_client',
            'phone' => formatPhone($client->phone),
        ];
    }

    public function verifyRegistrationStepIndependent(IndependentClient $client): int
    {
        if ($client->confirm_sms !== 10) {
            return self::STEP_PHONE_VALIDATION;
        }
        if (!$this->independentRegistrationIsComplete($client)) {
            return self::STEP_INDEPENDENT_SIGN_UP;
        }
        if (!$this->independentHasInformationContact($client)) {
            return self::STEP_CONTACT_INFORMATION;
        }

        return self::STEP_COMPLETED;
    }

    public function independentRegistrationIsComplete(IndependentClient $client)
    {
        $clientData = $client->only([
            'cnpj',
            'name',
            'zipcode',
            'address',
            'address_number',
            'neighborhood',
            'city',
            'state',
        ]);

        foreach ($clientData as $field => $value) {
            if ($value === null) {
                return false;
            }
        }

        return true;
    }

    public function independentHasInformationContact(IndependentClient $client)
    {
        $clientData = $client->only(['phone', 'email', 'contact_name']);

        foreach ($clientData as $field => $value) {
            if ($value === null) {
                return false;
            }
        }

        return true;
    }

    public function validateClient(Client $client)
    {
        $nextStep = $client->confirm_sms !== 10 ?
            self::STEP_PHONE_VALIDATION : self::STEP_CONTACT_INFORMATION;

        return [
            'status' => true,
            'message' => false,
            'next_step' => $nextStep,
            'client_type' => 'client',
            'phone' => formatPhone($client->phone),
        ];
    }
}
