<?php
function generatePassword($initialString = "", $characters = ["1234567890"], $length = 6)
{
    $new_password = "";
    if ($initialString != "") {
        $new_password = substr($initialString, 0, $length / 3);
    }
    if (!empty($characters)) {
        foreach ($characters as $list) {
            $new_password .= $list;
        }
    }
    $new_password = substr(str_shuffle($new_password), 0, $length);

    /**
     * A esta altura a senha já deve ter $length digitos porém
     * para ter certeza realizamos mais um str_pad completando com zeros a esquerda
     */
    return str_pad($new_password, $length, 0, STR_PAD_LEFT);
}

/**
 * Mascara CNPJ
 * 14 dígitos: ##.###.***/##**-**
/**
 * 15 dígitos: ###.###.***/##**-**
function maskPrivateCNPJ($val, $mask)
{
    if (!$val || empty($val) || strlen($val) < 14) {
        return '**.***.***/****-**';
    } else {
        if (strlen($val) == 14) {
            return preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})([0-9]{2})/", "$1.$2.***/$4**-**",
                $val);
        } else {
            if (strlen($val) == 15) {
                return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})([0-9]{2})/",
                    "$1.$2.***/$4**-**", $val);
            }
        }
    }
}

function subtractDayOfWeek($dw, $days)
{
    $_newDw = $dw - $days;
    if ($_newDw < 0) {
        $_newDw = 7 + $_newDw;
    }

    return $_newDw;
}

function sumDayOfWeek($dw, $days)
{
    $_newDw = $dw + $days;
    if ($_newDw > 6) {
        $_newDw = $_newDw - 7;
    }

    return $_newDw;
}


function formata_moeda($valor, $identifier = true, $dec_point = ',' , $thousands_sep = '.')
{
    if ($identifier) {
        return 'R$ ' . number_format($valor, 2, $dec_point, $thousands_sep);
    }

    return number_format($valor, 2, $dec_point, $thousands_sep);
}



/*
* Método que cria ranges de caixas baseado no valor passado
*/
function rangeCaixas($qtd_caixas)
{
    $qtd_caixas = intval($qtd_caixas);
    switch (true) {
        case ($qtd_caixas <= 5):
            return "1 a 5cx";
            break;
        case ($qtd_caixas <= 20):
            return "5 a 20cx";
            break;
        case ($qtd_caixas <= 50):
            return "20 a 50cx";
            break;
        case ($qtd_caixas <= 100):
            return "50 a 100cx";
            break;
        case ($qtd_caixas <= 500):
            return "100 a 500cx";
            break;
        case ($qtd_caixas > 500):
            return "500cx ou mais";
            break;
        default:
            return "1 ou mais caixas";
            break;
    }
}

/**
 * Cálculo do valor unitário a partir do preço/kg e gramatura
 */
function unitPrice($kgPrice, $grammage)
{
    $grammage = str_replace(',', '.', $grammage);
    if ($kgPrice == 0 || $grammage <= 0) {
        return false;
    }
    $unit_price = $kgPrice * $grammage;

    return formata_moeda($unit_price);
}

function volume($value, $decPoint = '.', $thousandsSep = '', $suffix = '')
{
    return (string) number_format($value, 2, $decPoint, $thousandsSep) . $suffix;
}

/**
 * Retorna a unidade preferencial de acordo com a config e condição de valor/gramatura
 */
function getUnit($kgPrice, $grammage, $defaultUnit, $isUnit)
{
    $grammage = str_replace(',', '.', $grammage);
    if ($isUnit) {
        if ($kgPrice == 0 || $grammage <= 0) {
            return ucfirst(strtolower($defaultUnit));
        }

        return "Un";
    } else {
        return ucfirst(strtolower($defaultUnit));
    }
}

/**
 * Formata gramatura removendo vírgulas
 */
function formatGrammage($grammage)
{
    if (!$grammage || $grammage == '') {
        return 0;
    } else {
        return floatval(str_replace(',', '.', $grammage));
    }
}

function dataPTtoMySQL($data)
{
    return implode("-", array_reverse(explode("/", $data)));
}

function dataMySQLtoPT($data)
{
    return implode("/", array_reverse(explode("-", $data)));
}

/**
 * Retorna a quantidade de unidades
 */
function getUnitAmount($totalKg, $grammage)
{
    $grammage = str_replace(',', '.', $grammage);
    if ($totalKg == 0 || $grammage <= 0) {
        return false;
    }

    return floor($totalKg / $grammage);
}

function dataMySQLtoFriendly($date, $short = false)
{
    $today = new DateTime(date('Y-m-d')); // Com timestamp a hora sera incluida
    $dateFrom = new DateTime($date);
    $dateArray = array_reverse(explode("-", $date));
    if ($short) {
        $date = $dateArray[0] . '/' . $dateArray[1];
    } else {
        $date = implode("/", $dateArray);
    }
    $interval = $today->diff($dateFrom)->format('%a');

    return $interval == 0 ? "Hoje" : ($interval == 1 ? "Ontem" : $date);
}

/**
 * format amount for mobile.
 * eg: 50 (500g) | 5 (5kg)
 *
 * @param string $amount Amount formated
 * @param float  $weight weight piece
 *
 * @return string
 */
function formatAmount(string $amount, float $weight): string
{
    if ($weight < 1) {
        $weightPiece = $weight * 1000;
        $weightPieceLabel = $weightPiece . 'g';
    } else {
        $weightPieceLabel = $weight . 'Kg';
    }

    return "{$amount} ({$weightPieceLabel})";
}

function mailConfirmationCode(): string
{
    return now()->timestamp . str_random(32);
}