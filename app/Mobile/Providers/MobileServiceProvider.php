<?php

namespace App\Mobile\Providers;

use App\Fefo\Fefo;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Gate;

class MobileServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Mobile\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::middleware(['api', 'logRequest', 'mobileAuthorization'])
            ->namespace($this->namespace)
            ->group(base_path('app/Mobile/Http/routes.php'));

        Route::prefix('api/v1/public')
            ->namespace($this->namespace)
            ->group(base_path('app/Mobile/Http/routes_public.php'));
    }
}
