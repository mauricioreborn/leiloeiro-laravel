<?php

namespace App\Mobile\Exceptions;

use Exception;

use Illuminate\Validation\ValidationException;

class MobileWithArrayValidationException extends ValidationException
{
    public function __construct($validator, $response = null, $errorBag = 'default')
    {
        parent::__construct($validator, $response, $errorBag);
    }
}
