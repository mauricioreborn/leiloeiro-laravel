<?php

namespace App\Mobile\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class MobileException extends HttpException
{
    /**
     * Constructor
     * @param $statusCode = false, 200
     * @param string $message
     * @param array customFields. ex.: ['token_is_valid' => false]
     **/
    public function __construct($statusCode = 200, string $message = null, array $customFields = [])
    {
        parent::__construct((int) $statusCode, $message);

        $this->statusCode = $statusCode;
        $this->message = $message;
        $this->customFields = $customFields;
    }

    public function errors()
    {
        $defaultResponse = [
            'status' => $this->statusCode,
            'msg' => $this->message ?? false,
            'message' => $this->message ?? false
        ];

        $response = array_merge($defaultResponse, $this->customFields);

        return $response;
    }
}
