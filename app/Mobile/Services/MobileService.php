<?php


namespace App\Mobile\Services;

use App\Client\Client;
use App\Core\Classes\CrudService;
use App\Mobile\Exceptions\MobileException;

use App\Bid\Bid;
use App\Order\Order;

use Illuminate\Support\Facades\DB;

class MobileService extends CrudService
{
    /**
     * MobileService constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Get products joined with fefo and leadTime
     * @return Illuminate\Support\Facades\DB
     */
    protected function getProductsJoinedWithFefoAndLeadTime()
    {
        return  DB::table('produtos')
            ->selectRaw('fefo.*, fefo.id as id_fefo, leadtime.*, produtos.*')
            ->join('fefo', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('leadtime', 'leadtime.cod_origem', 'fefo.cod_origem');
    }

    /**
     * Client check token for each company
     * @param ClientId $clientId Client ID
     * @return App\Client\Client
     */
    public function checkToken($clientId)
    {
        $client = $this->client->with(['companies'])->findOrFail($clientId);

        $client = $this->getAuthorizedClientCompanies($client);

        if ($client->companies->isNotEmpty()) {
            return $client;
        }

        throw new MobileException(false, "CNPJ não encontrado", ['token_valido' => false]);
    }

    /**
     * Get authorized client companies based on rules
     * @param Client $client client
     * @return Client with filtered companies
     **/
    public function getAuthorizedClientCompanies(Client $client)
    {
        foreach($client->companies as $key => $company) {
            if($company->clientCompany->status === 1) {
                continue;
            }

            //@TODO: Após campanha de cadastro de clientes, vamos remover esse if que permite que usuários
            // que estão desativados na nestle possam ver a loja para atualizar seu cadastro no banner do app
            if($company->id === 4) {
                continue;
            }

            $hasHigherAccountBallance = $company->clientCompany->account_balance < $company->clientCompany->min_order;
            if($hasHigherAccountBallance) {
                continue;
            }

            $hasBid = Bid::where('client_id', $client->id)
                ->where('company_id', $company->id)->exists();

            $hasOrder = Order::where('id_cliente', $client->id)
                ->where('company_id', $company->id)->exists();

            if($hasBid || $hasOrder) {
                continue;
            }

            $client->companies->forget($key);
        }

        return $client;
    }
}
