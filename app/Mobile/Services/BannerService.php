<?php

namespace App\Mobile\Services;

use App\Client\Client;
use App\Client\Services\ClientCompanyPaymentService;
use App\Client\Services\ClientService;
use App\Client\Services\ClientTirolezService;
use App\Core\Entities\Status;
use App\Core\Helpers\FormatHelper;
use App\Mobile\Services\ClientService as MobileClientService;
use App\Company\Company;
use Illuminate\Support\Facades\Log;

class BannerService extends MobileService
{
    public $appVersion;
    public $clientsSmsTest;
    protected $clientTirolezService;
    protected $clientService;

    /**
     * BannerService constructor.
     */
    public function __construct()
    {
        $this->appVersion = 46;
        $this->minAppVersion = 52;

        $this->clientService = new ClientService();
        $this->clientTirolezService = new ClientTirolezService();
    }

    /**
     * get banner of home
     *
     * @param Company $company        Company entity
     * @param Client  $client         Client entity
     * @param Request $appVersionUser App version user
     * @return array|bool
     */
    public function getHomeBanner(Company $company, Client $client, $appVersionUser, $appPlatform = null)
    {
        $banner = false;
        $status = Status::type(Status::AUTHORIZED);

        if($appPlatform == 'android') {
            $this->minAppVersion = 49;
        }

        if($appPlatform && $appVersionUser < $this->minAppVersion) {
            return $this->getUpdateAppBanner();
        }

        if($appVersionUser >= $this->appVersion &&
           ($client->confirm_sms !== $status->id || $client->confirm_email !== $status->id)){
            return $this->getClientBannerUpdateEmailFone($client);
        }

        $bannerTirolez = $this->getTirolezBanner($company, $client);
        if ($bannerTirolez) {
            return $bannerTirolez;
        }

        if($company->id == 4 && $banner = $this->getBannerUpdateRegisterPromo($company, $client)){
            return $banner;
        }

        $creditCardEnabled = (new ClientCompanyPaymentService())->creditCardEnabledWithCompany($company, $client);

        if ($creditCardEnabled) {
            return $this->getCreditCardBanner($company, $client);
        }

        return $banner;
    }

    /**
     * get banner of home
     *
     * @param Company $company Company entity
     * @param Client  $client  Client entity
     *
     * @return array|bool
     */
    public function getPromoBanner(Company $company, Client $client)
    {
        $banner = false;

        $showTirolezModal = $this->getTirolezModal($company, $client);

        if($showTirolezModal) {
            return $showTirolezModal;
        }

        $enable = $this->getClientPromo($client);

        if ($enable && $company->name != 'Nestlé DPA') {
            $banner = [
                'title' => 'A Nestlé Iogurtes chegou, aproveite!',
                // phpcs:ignore
                'description' => 'Preços especiais de lançamento com descontos de até 60%. <br>Acesse e confira!',
                'background_url' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/icons/banner_nestle_bg.svg',
                'image_url' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/banners/banner_nestle_produtos.png',
                'actions' => [
                    'cta_label' => 'Selecionar loja',
                    'cta_action' => 'CompaniesPage',
                    'close_action' => 'dismiss',
                    'close_label' => 'Agora não'
                ]
            ];
        }

        return $banner;
    }

    public function getBannerUpdateRegisterPromo(Company $company, Client $client)
    {
        $clientService  = new ClientService();
        $registerUpdated = $clientService->getRegisterUpdate($company, $client);

        if ($registerUpdated['registerUpdated'] == true) {
            return [
                'title' => '',
                'body' => 'Confirme seus dados para aproveitar as ofertas da Nestlé Iogurtes',
                'color' => 'rgb(255, 255, 255)',
                // phpcs:ignore
                'background_color' => 'linear-gradient(-135deg, rgb(28, 167, 182) 0%,rgb(28, 167, 182) 70%,rgb(28, 167, 182) 100%)',
                'icon' => 'https://souk-company.s3.amazonaws.com/4/banner/icon-attention-white.svg',
                'page' => 'AtualizarCadastroPage',
                'resource' => null,
            ];
        }

        if ($registerUpdated['waitingApproval'] == true) {
            return [
                'title' => '',
                'body' => 'Seus dados estão em análise pela Nestlé. Aguarde, você será avisado em breve.',
                'color' => 'rgb(102, 102, 102)',
                // phpcs:ignore
                'background_color' => 'linear-gradient(-135deg, rgb(238, 238, 238) 0%,rgb(238, 238, 238) 70%,rgb(238, 238, 238) 100%)',
                'icon' => 'https://souk-company.s3.amazonaws.com/4/banner/icon-attention-gray.svg',
                'page' => false,
                'resource' => null,
            ];
        }
        return false;
    }

    protected function getCreditCardBanner(Company $company, Client $client)
    {
        $color = 'linear-gradient(-135deg, rgb(139, 47, 203) 0%,rgb(126, 40, 222) 70%,rgb(115, 33, 223) 100%)';

        return [
            'title' => null,
            'body' => __('Mobile/banner.creditCard'),
            'color' => 'rgb(255, 255, 255)',
            'background_color' => $color,
            'icon' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/icons/ilustra-met-pag@svg.svg',
            'page' => 'MeusCartoesPage',
            'resource' => null,
        ];
    }

    protected function getUpdateAppBanner()
    {
        $color = 'linear-gradient(-135deg, rgb(238, 238, 238) 0%,rgb(238, 238, 238) 70%,rgb(238, 238, 238) 100%)';

        return [
            'title' => false,
            'body' => __('Mobile/banner.updateVersion.body'),
            'color' => 'rgba(102, 102, 102, 1)',
            'background_color' => $color,
            'icon' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/icons/icon-attention.svg',
            'page' => false,
            'resource' => false,
        ];
    }

    protected function getTirolezBanner(Company $company, Client $client)
    {
        if ($company->id == 5) {
            return false;
        }

        $company = Company::find(5);

        $hasAlreadyBought = $this->clientService->hasAlreadyBought($company, $client);
        $isActiveCampaign = $this->clientTirolezService->isActiveCampaign($client);

        $banner = false;

        if (!$hasAlreadyBought && $isActiveCampaign) {

            $color = 'linear-gradient(-135deg, rgb(28, 167, 182) 0%,rgb(28, 167, 182) 70%,rgb(28, 167, 182) 100%)';

            $banner = [
                'campaign' => 'souk_ativacao-tirolez_app_d0-base1',
                'title' => __("Mobile/banner.tirolez.type.{$client->clientTirolez->type}.title"),
                'body' => __("Mobile/banner.tirolez.type.{$client->clientTirolez->type}.body"),
                'color' => 'rgb(255, 255, 255)',
                'background_color' => $color,
                'icon' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/banners/icon-banner-tirolez.svg',
                'page' => 'HomePage',
                'resource' => [
                    'company_id' => 5
                ],
            ];
        }

        return $banner;
    }

    public function getTirolezModal(Company $company, Client $client)
    {
        if ($company->id == 5) {
            return false;
        }

        $hasReachedLimitViews = $this->clientTirolezService->hasReachedLimitViews($client);
        $hasModalAlreadySeenToday = $this->clientTirolezService->hasModalAlreadySeenToday($client);
        $hasAlreadyBought = $this->clientService->hasAlreadyBought($company, $client);

        $modal = false;

        $clientTirolez = $client->clientTirolez;
        if ($clientTirolez && !$hasAlreadyBought && !$hasReachedLimitViews && !$hasModalAlreadySeenToday) {

            $clientTirolez->last_updated_at = now();
            $clientTirolez->modal_view_counter += 1;
            $clientTirolez->save();

            $modal = [
                'campaign' => "souk_ativacao-tirolez_app_d0-base1",
                'title' => __("Mobile/modal_home.tirolez.title"),
                'description' => __("Mobile/modal_home.tirolez.body"),
                'background_url' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/icons/banner_nestle_bg.svg',
                'image_url' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/banners/img-modal-tirolez.png',
                'company_id' => 5,
                'actions' => [
                    'cta_label' => 'Ir para a loja',
                    'cta_action' => 'HomePage',
                    'close_action' => 'dismiss',
                    'close_label' => 'Agora não'
                ]
            ];
        }

        return $modal;
    }

    /**
     * return if client enable banner promo
     *
     * @param  Client $client Client entity
     *
     * @return boolean
     */
    public function getClientPromo(Client $client): bool
    {
        $dates = [
            '2019-08-05',
            '2019-08-06',
            '2019-08-07',
            '2019-08-08',
            '2019-08-09',
            '2019-08-10',
            '2019-08-11',
            '2019-08-12'
        ];

        $clients = [
            199692,
            240023,
            305014,
            241932,
            239080,
            242815,
            244667,
            239621,
            239719,
            237658,
            244627,
            235843,
            243823,
            237151,
            235924,
            218917,
            242542,
            238323,
            239384,
            235900,
            236212,
            236758,
            243099,
            242956,
            242398,
            236927,
            241655,
            236030,
            237694,
            236259,
            238647,
            245894,
            219806,
            235721,
            239917,
            244877,
            244434,
            323807,
            238142,
            235982,
            242167,
            244842,
            243410,
            237633,
            305558,
            241418,
            240648,
            244462,
            330499,
            243241,
            323269,
            239235,
            237862,
            243563,
            328696,
            236756,
            322460,
            244278,
            238217,
            245018,
            244476,
            241383,
            238607,
            239638,
            238138,
            241686,
            242527,
            242986,
            237637,
            243459,
            240128,
            241753,
            322479,
            333632,
            325480,
            242077,
            244506,
            238507,
            318632,
            241855,
            244635,
            244195,
            327826,
            236295,
            242879,
            243760,
            240987,
            245671,
            303964,
            239500,
            242901,
            329456,
            222215,
            323196,
            238539,
            238298,
            236654,
            245035,
            217389,
            244151,
            240573,
            243505,
            332423,
            241644,
            239859,
            242001,
            332424,
            322865,
            243581,
            238490,
            330893,
            323131,
            325953,
            213529,
            298357,
            331815,
            325100,
            238446,
            322331,
            243091,
        ];

        if (!in_array(today()->toDateString(), $dates)) {
            return false;
        }

        return in_array($client->id, $clients);
    }

    /**
     * @param Client $client Client
     * @return array
     */
    public function getClientBannerUpdateEmailFone(Client $client)
    {
        $step = null;
        $body = null;
        $resource = [];

        // 1 - Atualizacao cadastral (email e telefone)
        if($client->isStepOne()){
            $step = 1;
            $body = __("Mobile/banner.updateEmailFone.steps.{$step}.body");
            $resource = ['step' => $step];
        }

        // 2 - Banner para confirmar SMS
        if($client->isStepTwo()){
            $step = 2;
            $confirmation = (new MobileClientService())->getClientConfirmation($client, 'sms');

            $body = __("Mobile/banner.updateEmailFone.steps.{$step}.body", [
                'phone_number' => formatPhone($confirmation->value)
            ]);

            $resource = [
                'step' => $step,
                'phone' => formatPhone($confirmation->value)
            ];
        }

        // 3 - Avisando para atualizar apenas o email
        if($client->isStepThree()){
            $step = 3;
            $body = __("Mobile/banner.updateEmailFone.steps.{$step}.body");
            $resource = ['step' => $step];
        }

        // 4 - Confirmacao de EMAIL
        if($client->isStepFour()){
            $step = 4;
            $confirmation = (new MobileClientService())->getClientConfirmation($client, 'mail');

            if(!$confirmation){
                $step = 3;
                $body = __("Mobile/banner.updateEmailFone.steps.{$step}.body");
                $resource = ['step' => $step];

                //phpcs:ignore
                if(app()->environment() == 'production'){
                    Log::channel('slack')->warning("Cliente {$client->id} chegou ao passo 4 sem dados de email confirmation");
                }
            }else{
                $body = __("Mobile/banner.updateEmailFone.steps.{$step}.body", ['email' => $confirmation->value]);
                $resource = [
                    'step' => $step,
                    'email' => $confirmation->value
                ];
            }
        }

        $color = 'linear-gradient(-135deg, rgb(28, 167, 182) 0%,rgb(28, 167, 182) 70%,rgb(28, 167, 182) 100%)';

        $banner = [
            'title' => __("Mobile/banner.updateEmailFone.title"),
            'body' => $body,
            'color' => 'rgb(255, 255, 255)',
            'background_color' => $color,
            'icon' => 'https://souk-company.s3.amazonaws.com/4/banner/icon-attention-white.svg',
            'page' => 'VerificacaoDadosPage',
            'resource' => ($resource && count($resource) > 0) ? $resource : null,
        ];

        return $banner;
    }
}
