<?php

namespace App\Mobile\Services;

use App\Client\Services\ClientCardService;
use App\Client\Services\ClientCompanyPaymentService;
use App\Company\Company;
use App\Company\Services\CompanySettingService;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Order\Order;
use App\Order\OrderProduct;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class OrderService
{
    /**
     * Lista pedidos do cliente agrupados por status
     * @param string $client_id client_id
     * @return array
     */
    public function getClientOrders(string $client_id): array
    {
        $approved = $this->getClientOrdersByStatus($client_id, Status::type(Status::APPROVED));
        $pending = $this->getClientOrdersByStatus($client_id, Status::type(Status::PENDING));
        $canceled = $this->getClientOrdersByStatus($client_id, Status::type(Status::CANCELED));

        return [
            'pendentes' => $pending,
            'aprovados' => $approved,
            'reprovados' => $canceled,
        ];
    }

    /**
     * Lista pedidos cliente por status
     * @param string  $client_id client_id
     * @param integer $status    status [0, 1, 2]
     * @return Illuminate\Support\Collection
     */
    public function getClientOrdersByStatus(string $client_id, Status $status, array $filters = []): Collection
    {
        $clientCardId = array_get($filters, 'client_card_id');

        $orders = Order::with('orderProduct')
            ->selectRaw('companies.id as companyId, companies.name as companyName,pedidos.*')
            ->join('pedidos_produtos', function ($query) {
                return $query->on('pedidos_produtos.pedido_id', '=', 'pedidos.id')
                    ->whereNull('pedidos_produtos.deleted_at');
            })
            ->join('companies', function ($query) {
                return $query->on('pedidos.company_id', '=', 'companies.id');
            })
            ->where([
                ['pedidos.id_cliente', '=', $client_id],
                ['pedidos.status_id', '=', $status->id]
            ])
            ->where(function ($query) use ($status) {
                if ($status->id = Status::type(Status::PENDING)->id) {
                    $query->orWhere('pedidos_produtos.status_id', '=', Status::type(Status::PENDING)->id);
                }

                if ($status->id = Status::type(Status::APPROVED)->id) {
                    $query->orWhere('pedidos_produtos.status_id', '=', Status::type(Status::APPROVED)->id)
                        ->orWhere('pedidos_produtos.status_id', '=', Status::type(Status::CANCELED)->id);
                }

                if($status->id = Status::type(Status::CANCELED)->id) {
                    $query->orWhere('pedidos_produtos.status_id', '=', Status::type(Status::CANCELED)->id);
                }

                return $query;
            })
            ->whereNull('pedidos.lance_id');

        if ($clientCardId) {
            $orders->where('client_card_id', $clientCardId);
        }

        $orders = $orders->groupBy('pedidos.id')
            ->orderBy('pedidos.nao_lido', 'desc')
            ->orderBy('pedidos.id', 'desc')
            ->get();

        $orderProducts = OrderProduct::selectRaw("
                COALESCE(COUNT(pedidos_produtos.id), 0) as total,
                COALESCE(SUM(pedidos_produtos.valor), 0) as valor,
                COALESCE(SUM(pedidos_produtos.status_id = ".Status::type(Status::APPROVED)->id."), 0) as billed,
                pedido_id")
            ->whereIn('pedido_id', $orders->pluck('id'))
            ->groupBy('pedido_id')
            ->get();

        $orderProducts = $orderProducts->mapWithKeys(function ($order_product) {
            return [$order_product->pedido_id => $order_product];
        });

        $client_orders = collect([]);

        $clientCompanyPaymentService = new ClientCompanyPaymentService();

        $orders->each(function ($order) use (&$client_orders, $orderProducts, $clientCompanyPaymentService) {

            $order_product = $orderProducts[$order->id];

            if($order_product->total == 0 || $order_product->valor == 0) {
                return false;
            }

            $statusClientOrder = $this->orderStatus($order);

            $toPush = [
                'id_pedido' => (string) $order->id,
                'numero' => (string) $order->id,
                'produtos' => (string) $order_product->total,
                'valor' => formata_moeda($clientCompanyPaymentService->calculateTaxTotal(
                    $order_product->price,
                    $order->tax_percent
                )),
                'dataPedido' => Carbon::parse($order->data)->format('d/m/Y'),
                'dataEntrega' => $order->data_entrega ? Carbon::parse($order->data_entrega)->format('d/m/Y') : false,
                'aprovado' => false,
                'novo' => (bool) $order->nao_lido,
                'parcial' => false,
                'companyId' => $order->companyId,
                'companyName' => $order->companyName
            ];

            $client_orders->push(array_merge($toPush, $statusClientOrder));
        });

        $unread = $orders->where('nao_lido', 1);

        if($unread->count()) {
            Order::whereIn('id', $unread->pluck('id'))->update(['nao_lido' =>  0]);
        }

        return $client_orders;
    }

    /**
     * Descricao do status do pedido
     * @param Order             $order $order
     * @param OrderProduct|null $orderProduct
     * @return array
     */
    protected function orderStatus(Order $order, OrderProduct $orderProduct = null ) : array
    {
        $statusName = ($orderProduct) ? $orderProduct->status()->first()->name : $order->status()->first()->name;
        $langKey = "status";

        if($orderProduct === null && $order->isPartiallyBilled()) {
            $statusName = "partially_billed";
        }

        if($orderProduct === null && $statusName === Status::CANCELED) {
            $langKey = 'rejectionMotive';
            $statusName = $order->rejectionMotives->name;
        }elseif($orderProduct && $statusName === Status::CANCELED){
            $langKey = 'rejectionMotive';

            $statusName = isset($orderProduct->rejectionMotives->name) ? $orderProduct->rejectionMotives->name
                : RejectionMotive::type(RejectionMotive::INVALID_FEFO);
        }

        $statusData = [
            'status' => __("{$langKey}.order.{$statusName}.label"),
            'status_message' => __("{$langKey}.order.{$statusName}.mobile", [
                'order_id' => $order->id,
                'account_balance' => price_br($order->orderProduct->sum('price') - $order->client_account_balance),
            ]),
            'status_class' => __("{$langKey}.order.{$statusName}.class"),
        ];

        if($orderProduct){
            $statusData['status_block'] = __("{$langKey}.order.{$statusName}.block");
        }

        return $statusData;
    }

    /**
     * Method to list the orders
     *
     * @param string  $order_id order identify
     * @param Company $company  company entity
     * @return mixed
     */
    public function show(string $order_id, Company $company)
    {
        $order = Order::selectRaw('pedidos.*,SUM(pedidos_produtos.valor) as total_price, client_companies.address,
            client_companies.state,client_companies.city')
            ->join('pedidos_produtos', function ($query) {
                return $query->on('pedidos_produtos.pedido_id', '=', 'pedidos.id')
                    ->whereNull('pedidos_produtos.deleted_at');
            })
            ->join('client_companies', function ($query) {
                return $query->on('client_companies.client_id', '=', 'pedidos.id_cliente');
            })
            ->where([['pedidos.id', '=', $order_id]])
            ->where([['client_companies.company_id', '=', $company->id]])
            ->with(['clientCompanyPayment.companyPayment.payment', 'clientCard'])
            ->first();

        if ($order->clientCompanyPayment) {
            $payment = $order->clientCompanyPayment->companyPayment->payment;

            if ($payment->isCompanyCredit) {
                $order->payment = __('Mobile/payment.companyCredit', ['company' => $company->name]);
                $order->paymentIcon = $payment->image;
            }

            if ($payment->isCreditCard) {
                $quota = (new ClientCardService())->calculateQuotaValue(
                    $order->clientCard,
                    $order->total_price,
                    $order->cc_months
                );

                $order->payment = $order->clientCard->description;
                $order->paymentIcon = $order->clientCard->image;
                $order->paymentQuota = $quota['message'];
            }
        }

        $totalPrice = (new ClientCompanyPaymentService())->calculateTaxTotal($order->total_price, $order->tax_percent);
        $order->approved = ($order->status_id == Status::type(Status::CANCELED)->id) ? true : false;

        $orderStatus = $this->orderStatus($order);

        if ($order->status_id == Status::type(Status::CANCELED)->id) {
            $order->canceled = true;
            $order->cancellation_reason = $orderStatus['status_message'];
        }

        $order->date = Carbon::parse($order->data)->format('d/m/Y');
        $order->total_price = formata_moeda($totalPrice);
        $order->address = $order->address.', '.$order->city.'/'.$order->state;

        $orderProducts = OrderProduct::selectRaw('
                pedidos_produtos.*,
                pedidos_produtos.id as item_id,
                produtos.nome,
                produtos.marca,
                produtos.foto,
                produtos.peso_caixa,
                produtos.exibicao_app,
                produtos.gramatura,
                produtos.medida')
            ->join('pedidos', function ($query) {
                return $query->on('pedidos_produtos.pedido_id', '=', 'pedidos.id')
                    ->whereNull('pedidos_produtos.deleted_at');
            })
            ->join('produtos', function ($query) {
                return $query->on('pedidos_produtos.produto_id', '=', 'produtos.codigo')
                ->on('pedidos.company_id', '=', 'produtos.company_id');
            })
            ->where('pedidos_produtos.pedido_id', $order_id)
            ->groupBy('produtos.codigo')
            ->get();

        $products = collect();

        $orderProducts->each(function ($order_product) use (&$products, $order, $company) {

            $validateLabel = (new CompanySettingService())->getPeriodView($order->company, $order_product->semanas);
            $orderProductStatus = $this->orderStatus($order, $order_product);

            $products->push([
                'id' => (string) $order_product->id,
                'nome' => $order_product->nome,
                'status_descricao' => $orderProductStatus['status_block'],
                'img' => env('PRODUCT_IMAGES_BASE_URL').'souk-company/'.$order->company_id.'/'.$order_product->foto,
                'empresa' => $order_product->marca,
                'pesoCaixa' => volume($order_product->peso_caixa).'Kg',
                'pesoTotal' => volume($order_product->total_kg).'Kg',
                'dimensaoCaixa' => $order_product->exibicao_app,
                'validade' => $order_product->semanas . ' ' . $validateLabel,
                'quantidadeCaixas' => (intval($order_product->qtd_caixas) > 1) ?
                                        $order_product->qtd_caixas.' caixas' :
                                        $order_product->qtd_caixas.' caixa',
                'valorKg' => formata_moeda($order_product->valor_kg),
                'valor' => formata_moeda($order_product->valor),
                'dataEmbarque' => $order_product->data_embarque ?
                                    Carbon::parse($order_product->data_embarque)->format('d/m/Y') :
                                    false,
                'dataEntrega' => $order_product->data_entrega ?
                                    Carbon::parse($order_product->data_entrega)->format('d/m/Y') :
                                    false,
                'precoUn' => unitPrice($order_product->valor_kg, $order_product->gramatura),
                'unidadeMedida' => getUnit(
                                    $order_product->valor_kg,
                                    $order_product->gramatura,
                                    $order_product->medida,
                                    true
                ),
                'padraoMedida' =>  ucfirst(strtolower($order_product->medida)),
                'gramatura' =>  formatGrammage($order_product->gramatura),
                'quantidadeUn' =>  getUnitAmount($order_product->total_kg, $order_product->gramatura)
            ]);
        });

        $statusPendingBlocksView = __("status.order.".Status::type(Status::PENDING)->name.".block");
        $statusCanceledBlocksView = __("status.order.".Status::type(Status::CANCELED)->name.".block");

        $products = $products->mapToGroups(function ($product) use ($statusPendingBlocksView) {
            $status = ($product['status_descricao'] === null) ? $statusPendingBlocksView : $product['status_descricao'];
            return [$status => $product];
        });

        $order->totalProducts = 0;
        $order->totalProductsAnalyzed = 0;
        $totalProductsCancelled = 0;

        foreach ($products as $status => $product) {
            $statusWithCount = $status." (".$product->count().")";
            $order->totalProducts += $product->count();

            if($status != $statusPendingBlocksView){
                $order->totalProductsAnalyzed += $product->count();
            }

            if($status == $statusCanceledBlocksView){
                $totalProductsCancelled += $product->count();
            }

            $products[$statusWithCount] = $product->mapWithKeys(function ($item) {
                return [$item['id'] => array_except($item, ['id', 'status_descricao'])];
            });

            unset($products[$status]);
        }

        $order->products = $products;

        $statusOrder = $this->orderStatus($order);
        $order->status = $statusOrder['status'];
        $order->status_message = $statusOrder['status_message'];
        $order->status_class = $statusOrder['status_class'];

        return $order;
    }
}
