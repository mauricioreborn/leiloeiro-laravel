<?php

namespace App\Mobile\Services;

use App\Bid\Bid;
use App\Cart\ProductCart;
use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Client\Services\ClientSearchService;
use App\Company\Company;
use App\Company\Services\CompanySettingService;
use App\Core\Entities\Status;
use App\LeadTime\LeadTime;
use App\LeadTime\Services\LeadTimeService;
use App\Mobile\Exceptions\MobileException;
use App\Order\Order;
use App\Order\OrderProduct;
use App\ProductTypology\Services\ProductTypologyService;
use App\Products\Product;
use App\Products\ProductTribute;
use App\Season\SeasonProduct;
use App\Season\SeasonType;
use App\Season\Services\SeasonService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductService
{
    /**
     * Lista produtos em destaque
     *
     * @param Client  $client      the client id
     * @param Company $company     company entity
     * @param string  $custom_view the custom view
     *
     * @return array with product data
     */
    public function getAreasHighlightFefo(Client $client, Company $company, string $custom_view = null): array
    {
        $productIds = [];
        $productsAlreadyBought = [];
        $custom_colors = false;
        $client->clientCompany = $client->setClientCompany($company);
        $exceptionCustomView = [
            'destaques',
            strtolower(__('Mobile/product.best_price_from_track')),
            strtolower(__('Mobile/product.best_seller_typology')),
            strtolower(__('Mobile/product.suggested_products')),
            strtolower(__('Mobile/product.buy_again')),
        ];

        if ($custom_view == strtolower(__('Mobile/product.best_seller_typology'))) {
            $leadtime = LeadTime::where(['cnpj' => $client->cnpj,'company_id' => $company->id,])->first();
            $productsAlreadyBought = $this->getProductCodeAlreadyBought($client, $company)->toArray();
            $productIds = Cache::get(
                "campaign:home:typology:{$company->id}:{$leadtime->code}.{$client->clientCompany->typology}"
            );
        }

        if ($custom_view == strtolower(__('Mobile/product.suggested_products'))) {
            $productsAlreadyBought = $this->getProductCodeAlreadyBought($client, $company)->toArray();
            $productIds = Cache::get("campaign:home:suggested:{$company->id}:{$client->id}");
        }

        if ($custom_view == strtolower(__('Mobile/product.buy_again'))) {
            $productIds = $this->getProductCodeAlreadyBought($client, $company)->toArray();
        }

        if ($productIds && count($productIds) > 0) {
            $products = $this->getAreasProducts($client, $company, [], false, false, $productIds,
                $productsAlreadyBought);
            $products['nome'] = strtolower($custom_view);
            $products['showOrder'] = true;
            $products['showFilter'] = false;
            $products['custom_colors'] = $custom_colors;

            return $products;
        }

        if ($custom_view != null && !in_array(strtolower($custom_view), $exceptionCustomView)) {

            $seasonType = SeasonType::where(['name' => $custom_view])->first();
            if (isset($seasonType->id)) {
                $seasonService = new SeasonService();
                $custom_colors = $seasonService->getCustomColorBySeasonName($custom_view, $company->id);
                $products = $this->getAreasProducts($client, $company, [], false, $seasonType);
            } else {
                throw new MobileException(false, __('Mobile/product.custom_view_not_found'));
            }
        } else {
            $param = strtolower($custom_view) == strtolower(__('Mobile/product.best_price_from_track')) ?
                ['track' => [003, 004]] : ['featured' => 1];
            $products = $this->getAreasProducts($client, $company, $param, false);
        }

        $products['nome'] = strtolower($custom_view);
        $products['showOrder'] = true;
        $products['showFilter'] = false;
        $products['custom_colors'] = $custom_colors;

        return $products;
    }

    /**
     * Lista produtos
     *
     * @param Client  $client         the client id
     * @param Company $company        Company entity
     * @param string  $attributes     the custom view
     * @param string  $includeFilters the filters to shown
     * @param array   $seasonType     season products ids
     *
     * @return array with products
     */
    public function getAreasProducts(
        Client $client,
        Company $company,
        array $attributes,
        $includeFilters = true,
        $seasonType = false,
        $productsIds = false,
        array $productsAlreadyBought = []
    ) : array {

        $client->clientCompany = $client->setClientCompany($company);

        $date = array_get($attributes, 'date', today()->toDateString());

        $tracks = array_get($attributes, 'track');
        $category = array_get($attributes, 'category');
        $type = array_get($attributes, 'type');
        $weeks = array_get($attributes, 'weeks');
        $min_selling_price = array_get($attributes, 'min_selling_price');
        $max_selling_price = array_get($attributes, 'max_selling_price');
        $search = array_get($attributes, 'search');
        $orderBy = array_get($attributes, 'orderBy');
        $featured = array_get($attributes, 'featured', false);

        $products = collect();
        $productList = collect();

        $enableProducts = (new ProductTypologyService())->getProducts($client, $company);

        if ($category) {
            $category = sanitizeCategory($category, true);
        }

        if (!is_bool($enableProducts)) {
            if (!$enableProducts->count()) {
                return $area = [
                    'status' => true,
                    'showOrder' => true,
                    'showFilter' => false,
                    'nome' => $category ?? false,
                    'total_produtos' => $products->count(),
                    'produtos' => $productList,
                    'msg' => __('Mobile/product.no_record')
                ];
            }
        }


        if ($seasonType != false) {
            $seasonProduct = (new SeasonService())->getProductsBySeason($seasonType->id);
            $seasonProductIds = $seasonProduct[$seasonType->name];
        }

        $products = Product::selectRaw('fefo.*,fefo.id as id_fefo, leadtime.*, produtos.*')
            ->join('fefo', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('leadtime', function ($query) {
                return $query->on('fefo.cod_origem', '=', 'leadtime.cod_origem')
                    ->whereRaw('leadtime.company_id = fefo.company_id')
                    ->whereNull('leadtime.deleted_at');
            })
            ->where([
                ['fefo.volume', '>', 0],
                ['fefo.date', '=', $date],
                ['leadtime.cnpj', '=', $client->cnpj],
                ['produtos.company_id', '=', $company->id],
            ])
            ->whereIn('fefo.faixa_fefo', $client->clientCompany->tracks)
            ->whereRaw('fefo.volume >= produtos.peso_caixa');

        if ($category) {
            $products->where([
                ['produtos.area', '=', $category]
            ]);
        }

        if ($type) {
            $products->whereIn('produtos.familia', (array) $type);
        }

        if ($tracks) {
            $products->whereIn('fefo.faixa_fefo', $tracks);
        }

        if ($weeks) {
            $products->whereIn('fefo.semanas', preg_replace("/[^0-9,.]/", "", (array) $weeks));
        }

        if ($productsIds) {
            $products->whereIn('produtos.codigo', $productsIds);
        }

        if (count($productsAlreadyBought) >0) {
            $products->whereNotIn('produtos.codigo', $productsAlreadyBought);
        }

        if ($min_selling_price) {
            $products->where([
                ['fefo.valor_atual', '>=', str_replace(',', '.', $min_selling_price)]
            ]);
        }

        if($max_selling_price) {
            $products->where([
                ['fefo.valor_atual', '<=', str_replace(',', '.', $max_selling_price)]
            ]);
        }

        if($featured) {
            $products->where([
                ['fefo.destaque', '=', 1]
            ]);
        }

        if (isset($seasonProductIds)) {
            $products->whereIn('fefo.codigo_produto', $seasonProductIds);
        }

        if ($search) {
            $searchTerms = substr($search, 0, 150);
            $terms = explode(' ',$searchTerms);

            $products->where(function ($query) use ($terms) {
                foreach ($terms as $value) {
                    $query->orWhere('produtos.codigo', 'like', "%{$value}%");
                    $query->orWhere('produtos.nome', 'like', "%{$value}%");
                }
            });
        }

        if ($orderBy == 'nome') {
            $products->orderByRaw('produtos.nome asc, fefo.valor_atual asc, fefo.semanas asc');
        }

        if ($orderBy == 'preco') {
            $products->orderByRaw('fefo.valor_atual asc, fefo.semanas asc');
        }

        if ($orderBy == 'validade' || empty($orderBy)) {
            $products->orderByRaw('fefo.semanas asc, fefo.valor_atual asc');
        }

        if (!is_bool($enableProducts)) {
            $products->where(function ($query) use ($enableProducts) {
                foreach ($enableProducts as $enableProduct) {
                    $query->orWhere(function ($query) use ($enableProduct) {
                        return $query->where('fefo.codigo_produto', $enableProduct->product_code)
                            ->whereIn('fefo.faixa_fefo', $enableProduct->tracks);
                    });
                }

                return $query;
            });
        }

        $products = $products->get();
        $products = $this->removeDuplicates($products);

        foreach ($products as $key => $product) {
            $orderPrice = Order::selectRaw(
                'COALESCE(MIN(pedidos_produtos.valor_kg), 0) as min,
                 COALESCE(MAX(pedidos_produtos.valor_kg),0) as max'
            )
                ->join('pedidos_produtos', function ($query) {
                    return $query->on('pedidos_produtos.pedido_id', '=', 'pedidos.id')
                        ->whereNull('pedidos_produtos.deleted_at');
                })
                ->where([
                    ['pedidos_produtos.produto_id', '=', $product->codigo],
                    ['pedidos_produtos.semanas', '=', $product->semanas],
                    ['pedidos_produtos.cod_origem', '=', $product->cod_origem],
                    ['pedidos.status_id', '=', Status::type(Status::APPROVED)->id],
                    ['pedidos.data', '<=', Carbon::parse($date)->subDays(7)->toDateString()],
                    ['pedidos.company_id', '=', $company->id]
                ])->first();

            $minOrderPrice = (
                $orderPrice->min < $product->valor_atual && $orderPrice->min > 0
            )
                ?
                $orderPrice->min
                :
                $product->valor_atual;

            $maxOrderPrice = (
                $orderPrice->max > $product->valor_cheio && $orderPrice->max > 0
            )
                ?
                $orderPrice->max
                :
                $product->valor_cheio;

            $boxRange = ($product->peso_caixa > 0) ? floor(
                floatval($product->volume)/floatval($product->peso_caixa))
                : 0;

            $validateLabel = (new CompanySettingService())->getPeriodView($company, $product->semanas);

            $productList->push([
                'id_fefo' => $product->id_fefo,
                'codigo_produto' => $product->codigo_produto,
                'nome' => $product->nome,
                'empresa' => $product->marca,
                'img' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$company->id. '/'.$product->foto,
                'disponivel' => rangeCaixas($boxRange),
                'pesoCaixa' => volume($product->peso_caixa),
                'caixas' => $boxRange,
                'dimensaoCaixa' => $product->exibicao_app,
                'validade' => $product->semanas.' '.$validateLabel,
                'precoMin' => $minOrderPrice,
                'precoMax' => $maxOrderPrice,
                'precoAtual' => $product->valor_atual,
                'precoMinUn' => unitPrice($minOrderPrice, $product->gramatura),
                'precoMaxUn' => unitPrice($maxOrderPrice, $product->gramatura),
                'precoAtualUn' => unitPrice($product->valor_atual, $product->gramatura),
                'unidadeMedida' => getUnit($product->valor_atual, $product->gramatura, $product->medida, true),
                'padraoMedida' => $this->padraoMedida($product),
            ]);
        }

        $area = [
            'status' => true,
            'showOrder' => true,
            'showFilter' => false,
            'nome' => $category ? sanitizeCategory($category) : false,
            'total_produtos' => $products->count(),
            'produtos' => $productList,
        ];


        if (!$products->count()) {
            $area['msg'] = __('Mobile/product.no_record');
        }

        if ($includeFilters) {
            $filters = $this->getFiltros($client, $company, $category, $search, $date, $enableProducts);

            if ($filters) {
                $area['valor_min'] = floor($filters['valor_min']);
                $area['valor_max'] = ceil($filters['valor_max']);
                $area['valor_min_exibicao'] = formata_moeda($filters['valor_min']);
                $area['valor_max_exibicao'] = formata_moeda($filters['valor_max']);
                $area['classes'] = $filters['classes'];
                $area['semanas'] = $filters['semanas'];
            }
        }

        if ($search) {
            (new ClientSearchService())->createClientSearch(
                $company,
                $client,
                $search,
                $area
            );
        }

        return $area;
    }

    /**
     * Filtros
     *
     * @param Client  $client         Filter data to be searched
     * @param Company $company        Client entity
     * @param string  $category       A string with a company id
     * @param array   $search         the user category
     * @param string  $date           DateTime to filter
     * @param boolean $enableProducts enableProducts enableProducts
     *
     * @return array filtered
     */
    protected function getFiltros(
        Client $client,
        Company $company,
        $category = false,
        $search = false,
        $date = false,
        $enableProducts = false
    ) : array {
        $client->clientCompany = $client->setClientCompany($company);

        $date = $date ?: today()->toDateString();

        if (!is_bool($enableProducts)) {
            if (!$enableProducts->count()) {
                return [];
            }
        }

        $products = Product::selectRaw('fefo.valor_atual,fefo.semanas,produtos.familia')
            ->join('fefo', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('leadtime', function ($query) {
                return $query->on('fefo.cod_origem', '=', 'leadtime.cod_origem')
                    ->whereRaw('leadtime.company_id = fefo.company_id')
                    ->whereNull('leadtime.deleted_at');
            })
            ->where([
                ['fefo.volume', '>', 0],
                ['fefo.date', '=', $date],
                ['leadtime.cnpj', '=', $client->cnpj],
                ['produtos.company_id', '=', $company->id],
            ])
            ->whereIn('fefo.faixa_fefo', $client->clientCompany->tracks);

        if ($category) {
            $products->where([
                ['produtos.area', '=', $category]
            ]);
        }

        if ($search) {
            $searchTerms = substr($search, 0, 150);
            $terms = explode(' ', $searchTerms);

            $products->where(function ($query) use ($terms) {
                foreach ($terms as $value) {
                    $query->orWhere('produtos.codigo', 'like', "%{$value}%");
                    $query->orWhere('produtos.nome', 'like', "%{$value}%");
                }
            });
        }

        if (!is_bool($enableProducts)) {
            $products->where(function ($query) use ($enableProducts) {
                foreach ($enableProducts as $enableProduct) {
                    $query->orWhere(function ($query) use ($enableProduct) {
                        return $query->where('fefo.codigo_produto', $enableProduct->product_code)
                            ->whereIn('fefo.faixa_fefo', $enableProduct->tracks);
                    });
                }

                return $query;
            });
        }

        $products = $products->get();

        if (!$products->count()) {
            return [];
        }

        $filters = [];

        $filters['valor_min'] = $products->min('valor_atual') ?? 0;
        $filters['valor_max'] = $products->max('valor_atual') ?? 0;
        $filters['classes'] = $products->sortBy('familia')->pluck('familia')->unique();

        $weeks = [];

        foreach ($products->sortBy('semanas') as $key => $product) {
            $weeks[$product->semanas] = $product->semanas;
        }

        $companySettingService = (new CompanySettingService());

        array_walk($weeks,
            function (&$value, $key) use ($companySettingService, $company) {
                $validateLabel = $companySettingService->getPeriodView($company, $value);
                $value .= " {$validateLabel}";
            }
        );

        $filters['semanas'] = collect(array_unique($weeks));

        return $filters;
    }

    /**
     * Method to show a product
     *
     * @param Client  $client     the client Id
     * @param Company $company    Company entity
     * @param string  $fefo_id    the product Id
     * @param array   $attributes the Attributes to be used
     *
     * @return array with a product to show
     */
    public function show(Client $client, Company $company, $fefo_id = null, array $attributes = []): array
    {
        $client->clientCompany = $client->setClientCompany($company);

        $date = array_get($attributes, 'date', today()->toDateString());

        $code = array_get($attributes, 'code');
        $validate = array_get($attributes, 'validate');
        $originCode = array_get($attributes, 'origin_code');

        $product = Product::selectRaw('fefo.*,fefo.id as fefo_id, leadtime.*,produtos.*')
            ->join('fefo', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('leadtime', function ($query) {
                return $query->on('fefo.cod_origem', '=', 'leadtime.cod_origem')
                    ->whereRaw('leadtime.company_id = fefo.company_id')
                    ->whereNull('leadtime.deleted_at');
            })
            ->where([
                ['fefo.date', '=', $date],
                ['fefo.volume', '>', 0],
                ['leadtime.cnpj', '=', $client->cnpj],
                ['produtos.company_id', '=', $company->id],
            ])
            ->whereIn('fefo.faixa_fefo', $client->clientCompany->tracks)
            ->whereRaw('fefo.volume >= produtos.peso_caixa');

        if (!is_null($fefo_id)) {
            $product->where([
                'fefo.id' => $fefo_id
            ]);
        }

        if (is_null($fefo_id) && $code && $validate && $originCode) {
            $product->where([
                'fefo.codigo_produto' => $code,
                'fefo.semanas' => $validate,
                'fefo.cod_origem' => $originCode,
            ]);
        }

        $product = $product->first();

        if (!$product) {
            throw new MobileException(false, __('Mobile/product.not_available'));
        }

        $hasTributeSubstitution = $this->hasTributeSubstitution($product->code, $client, $client->clientCompany);

        $leadtime = array_first((new LeadTimeService())->getLogistica([$client->id], $product->cod_origem));

        $message = false;

        if (!$leadtime['aceita_compra']) {
            $next_purchase_date = Carbon::parse($leadtime['visao_logistica'])->format('d/m');
            $message = __('Mobile/product.not_accept_purchase', ['date', $next_purchase_date]);
        }

        $boxRange = ($product->peso_caixa > 0) ? floor(floatval(
                $product->volume)/floatval($product->peso_caixa)
        ) : 0;
        $boxes = trans_choice('Mobile/order.boxes', $boxRange, ['box' => $boxRange]);

        $attributes = [
            'unidadeMedida' => getUnit($product->valor_atual, $product->gramatura, $product->medida, true),
            'padraoMedida' => $this->padraoMedida($product),
            'gramatura' => formatGrammage($product->gramatura),
        ];

        $validateLabel = (new CompanySettingService())->getPeriodView($company, $product->semanas);

        $response = [
            'fefo_id' => (string) $product->fefo_id,
            'codigo_produto' => $product->codigo_produto,
            'img' => env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$company->id. '/'.$product->foto,
            'nome' => $product->nome,
            'empresa' => $product->marca,
            'caixas' => $boxes,
            'pesoCaixa' => volume($product->peso_caixa),
            'dimensaoCaixa' => $product->exibicao_app,
            'validade' => $product->semanas. ' ' .$validateLabel,
            'disponivel' => rangeCaixas($boxRange),
            'preco' => $product->valor_atual,
            'precoAtual' => formata_moeda($product->valor_atual),
            'precoAtualUn' => unitPrice($product->valor_atual, $product->gramatura),
            'unidadeMedida' => $attributes['unidadeMedida'],
            'padraoMedida' => $attributes['padraoMedida'],
            'gramatura' => $attributes['gramatura'],
            'aceita_compra' => $leadtime['aceita_compra'],
            'msg_logistica' => $message,
            'label' => ($hasTributeSubstitution) ? __('Mobile/product.without_tribute') : false
        ];

        $discount_percentage =  ($product->valor_cheio-$product->valor_atual)/$product->valor_cheio * 100;
        $response['percentual_desconto'] = $discount_percentage >= 5 ? floor($discount_percentage) : 0;

        $response['lances'] = [];
        $response['lances']['abertos'] = $this->getProductBids(
            Status::type(Status::PENDING),
            $product->codigo_produto,
            $product->cod_origem,
            $product->semanas,
            $company,
            $attributes
        );
        $response['lances']['aceitos'] = $this->getProductBids(
            Status::type(Status::APPROVED),
            $product->codigo_produto,
            $product->cod_origem,
            $product->semanas,
            $company,
            $attributes
        );

        $response['total_produtos_carrinho'] = $this->countClientProductCart($client->id, $company);

        return $response;
    }

    /**
     * Conta produtos no carrinho do cliente
     *
     * @param Client  $client_id the client object
     * @param Company $company   Company entity
     *
     * @return integer
     */
    public function countClientProductCart($client_id, Company $company): int
    {
        return ProductCart::selectRaw('produtos.codigo')
            ->groupBy('produtos.codigo')
            ->where([
                ['carrinho.company_id', '=', $company->id],
                ['carrinho.id_cliente', '=', $client_id],
                ['carrinho.status', '=', '1'],
                ['carrinho_produtos.status', '=', '1'],

            ])
            ->join('produtos', function ($query) {
                return $query->on('carrinho_produtos.cod_produto', '=', 'produtos.codigo')
                        ->whereNull('produtos.deleted_at');
            })
            ->join('carrinho', function ($query) {
                return $query->on('carrinho.id', '=', 'carrinho_produtos.carrinho_id')
                        ->whereNull('carrinho.deleted_at');
            })
            ->get()->count();
    }

    /**
     * Pega lances do produto
     *
     * @param integer $status       integer status
     * @param string  $product_code product Code
     * @param string  $origin_code  leadtime Code
     * @param int     $weeks        weeks
     * @param Company $company      company entity
     * @param array   $attributes   attributes to be shown
     * @param integer $limit        total limit
     *
     * @return Illuminate\Support\Collection | boolean
     */
    public function getProductBids(
        Status $status,
        string $product_code,
        string $origin_code,
        int $weeks,
        Company $company,
        array $attributes = [],
        int $limit = 5
    ) {

        $bids = Bid::where(
            [
                ['lances.cod_produto', '=', $product_code],
                ['lances.cod_origem', '=', $origin_code],
                ['lances.semanas', '=', $weeks],
                ['lances.status_id', '=', $status->id],
                ['lances.company_id', '=', $company->id],
            ])
            ->join('clientes', function ($query) {
                return $query->on('lances.id_cliente', '=', 'clientes.id');
            })
            ->limit($limit);

        if ($status->name === Status::PENDING) {
            $bids->selectRaw('lances.total_kg as volume, lances.valor_kg as preco, lances.valor, clientes.cnpj')
                ->orderBy('lances.valor_kg', 'desc');
        }

        if ($status->name === Status::APPROVED) {
            $bids->selectRaw(
                'SUM(lances.total_kg) as volume,
                lances.valor_kg as preco, lances.valor,
                COUNT(lances.id) as quantidadeClientes,
                lances.data'
            )
                ->groupBy('lances.valor_kg')
                ->groupBy(DB::raw('DATE(`lances`.`created_at`)'))
                ->orderBy(DB::raw('DATE(lances.data)'), 'DESC')
                ->orderBy('lances.valor_kg', 'desc')
                ->orderBy('lances.valor_kg', 'desc');
        }

        $bids = $bids->get();

        if (!$bids->count()) {
            return false;
        }

        $bidList = collect();

        foreach ($bids as $key => $bid) {
            $unit_price = $bid->preco * $attributes['gramatura'];

            $item = [
                'preco' => formata_moeda($bid->preco),
                'valor' => formata_moeda($bid->valor),
                'volume' => volume($bid->volume) . 'Kg',
                'precoUn' => ($unit_price > 0) ? formata_moeda($unit_price) : false,
                'unidadeMedida' => $attributes['unidadeMedida'],
                'padraoMedida' => $attributes['padraoMedida'],
                'gramatura' => $attributes['gramatura'],
            ];

            if ($status->name === Status::PENDING) {
                $item['cnpj'] = maskPrivateCNPJ($bid->cnpj, '##.###.***/##**-**');
            }

            if ($status->name == Status::APPROVED) {
                $item['data'] = dataMySQLtoFriendly($bid->data, true);
                $item['quantidadeClientes'] = (string) $bid->quantidadeClientes;
            }

            $bidList->push($item);
        }

        return $bidList;
    }

    /**
     * @param array $input Input
     * @return \Illuminate\Support\Collection
     */
    public function removeDuplicates($input)
    {
        $products = [];
        foreach($input as $product) {
            $hash = "{$product->codigo_produto}-{$product->faixa_fefo}-{$product->valor_atual}";

            if(!isset($products[$hash])) {
                $products[$hash] = $product;
            }

            $currProduct = $products[$hash];

            if($product->faixa_fefo == $currProduct->faixa_fefo
               && $product->codigo_produto == $currProduct->codigo_produto
               && $product->cod_origem == $currProduct->cod_origem
               && $product->valor_atual == $currProduct->valor_atual
               && $product->volume > $currProduct->volume) {
                $products[$hash] = $product;
            }

            $product->foto = $this->checkBlackWeek($product);
        }

        return collect($products);
    }

    /**
     * Method to get product codes of products already bought
     * @param Client  $client  client object
     * @param Company $company company object
     * @return mixed
     */
    public function getProductCodeAlreadyBought(Client $client, Company $company)
    {
        $orderProduct =  OrderProduct::select('pedidos_produtos.produto_id')
            ->join('pedidos', 'pedidos.id', '=', 'pedidos_produtos.pedido_id')
            ->where([
                'pedidos.company_id' => $company->id,
                'pedidos.id_cliente' => $client->id
            ])->groupBy("pedidos_produtos.produto_id")
            ->get();
        return $orderProduct->pluck('produto_id');
    }

    /**
     * Method to change product image blackweek
     * @param $product product
     * @return string
     * @throws \Exception
     */
    public function checkBlackWeek($product)
    {
        $productsSeason = [];
        $photo = $product->foto;
        $today = Carbon::now();

        if (!$today->between(Carbon::parse("2019-11-21"), Carbon::parse("2019-12-12"))){
            return str_replace("_bw", "", $photo);
        }

        if(!cache()->has('SeasonTypeBlackWeek')){
            $session = SeasonType::whereRaw('? between start_season and finish_season', [now()->toDateTimeString()])
                ->with('seasonProduct:product_code,season_id')
                ->first();

            if($session){
                $minutes = Carbon::parse($session->finish_season)->diffInMinutes(now());
                $productsSeason = $session->seasonProduct->pluck('product_code')->toArray();
                cache()->add('SeasonTypeBlackWeek', $productsSeason, $minutes);
            }
        }else{
            $productsSeason = cache()->get('SeasonTypeBlackWeek');
        }

        $fefo = ["001", "002"];

        if(in_array($product->foto, $productsSeason) && !in_array($product->faixa_fefo, $fefo)){
            $photo = str_replace("_bw", "", $photo);
        }

        return $photo;
    }

    public function padraoMedida($data)
    {
        $data = is_array($data) ? $data : $data->toArray();
        $medida = isset($data['unit']) ? $data['unit'] : $data['medida'];

        if($data['company_id'] === 5 && strtolower($medida) == "cx"){
            $medida = "Kg";
        }

        return ucfirst(strtolower($medida));
    }

    public function hasTributeSubstitution(int $code, Client $client, ClientCompany $clientCompany)
    {
        $response = false;

        $productTribute = (new ProductTribute())
            ->where('state', $clientCompany->state)
            ->where('code', $code)
            ->where('company_id', $clientCompany->company_id)
            ->first();

        if ($productTribute) {
            $response = true;
        }

        return $response;
    }

    public function getAllTributeSubstitution(Client $client, ClientCompany $clientCompany)
    {
        return (new ProductTribute())
            ->where('state', $clientCompany->state)
            ->where('company_id', $clientCompany->company_id)
            ->get();
    }
}
