<?php


namespace App\Mobile\Services;

use App\Cart\Cart;
use App\Cart\ProductCart;
use App\Client\Client;
use App\Client\ClientCard;
use App\Client\Services\ClientCardService;
use App\Client\Services\ClientCompanyPaymentService;
use App\Client\Services\ClientService;
use App\ClientsCompanies\Services\ClientsCompaniesService;
use App\Company\Company;
use App\Company\Services\CompanySettingService;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\Fefo\Services\FefoService;
use App\LeadTime\Services\LeadTimeService;
use App\Mobile\Exceptions\MobileException;
use App\Order\Services\OrderService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CartService extends MobileService
{
    /**
     * Add cart
     * @param Client  $client     client
     * @param Company $company    company
     * @param int     $fefo_id    fefo_id
     * @param int     $qtd_caixas qtd_caixas
     * @return array
     **/
    public function addCart(Client $client, Company $company, $fefo_id, $qtd_caixas)
    {
        $clientCompany = $client->setClientCompany($company);

        if ($clientCompany->status === 0) {
            throw new MobileException(false, __('Mobile/bid.bids_and_buys_blocked'));
        }

        $cartModel = new Cart();
        $cartData = $cartModel->where([
            'id_cliente' => $client->id,
            'status' => 1,
            'company_id' => $company->id,
            'deleted_at' => null
        ])->first();

        if (!$cartData) {
            $cart_id = $cartModel->create([
                'id_cliente' => $client->id,
                'data' => date("Y-m-d"),
                'created_at' => date("Y-m-d H:i:s"),
                'company_id' => $company->id
            ])->id;

            if (!$cart_id) {
                return [
                    'status' => false,
                    'msg' => 'Não foi possível adicionar o produto.',
                    'message' => 'Não foi possível adicionar o produto.'
                ];
            }
        } else {
            $cart_id = $cartData->id;
        }

        $product = $this->getProductsJoinedWithFefoAndLeadTime()
            ->where([
                'fefo.id' => $fefo_id,
                'fefo.deleted_at' => null,
                'leadtime.cnpj' => $client->cnpj,
                'produtos.deleted_at' => null,
                'leadtime.company_id' => $company->id
            ])
            ->whereIn('fefo.faixa_fefo', $clientCompany->tracks)
            ->where(
                'fefo.volume', '>', '0'
            )->first();

        if (!$product) {
            return [
                'status' => false,
                'msg' => 'Não foi possível adicionar o produto.',
                'message' => 'Não foi possível adicionar o produto.'
            ];
        }

        $productCartModel = new ProductCart();
        $productExist = $productCartModel->where(
            [
                'carrinho_id' => $cart_id,
                'cod_produto' => $product->codigo_produto,
                'cod_origem' => $product->cod_origem,
                'semanas' => $product->semanas,
                'valor_kg' => $product->valor_atual,
                'status' => 1,
                'deleted_at' => null,
            ]
        )->first();

        if ($productExist) {
            $_volume_carrinho = $productExist->total_kg;
        } else {
            $_volume_carrinho = 0;
        }

        $total_kg = number_format($qtd_caixas * $product->peso_caixa, 2, '.', '');
        $valor = number_format(($qtd_caixas * $product->peso_caixa) * $product->valor_atual, 2, '.', '');

        $_volume_disponivel = $product->volume - $_volume_carrinho;

        /**
         * Quantidade acima do volume disponível
         */

        if ($_volume_disponivel < $total_kg) {
            $boxAmount = floor($_volume_disponivel / $product->peso_caixa);

            return [
                'status' => false,
                'msg' => trans_choice('Mobile/cart.volume_unavailable', $boxAmount, ['value' => $boxAmount]),
                'message' => trans_choice('Mobile/cart.volume_unavailable', $boxAmount, ['value' => $boxAmount])
            ];
        }

        if ($productExist) {

            $qtd_caixas = number_format($qtd_caixas + $productExist->qtd_caixas, 2, '.', '');
            $total_kg = number_format($total_kg + $productExist->total_kg, 2, '.', '');
            $valor = number_format($valor + $productExist->valor, 2, '.', '');

            $_update = [
                'qtd_caixas' => $qtd_caixas,
                'total_kg' => $total_kg,
                'valor' => $valor
            ];

            $productExist->update($_update);
        } else {
            $insert = [
                'carrinho_id' => $cart_id,
                'cod_produto' => $product->codigo_produto,
                'cod_origem' => $product->cod_origem,
                'semanas' => $product->semanas,
                'qtd_caixas' => $qtd_caixas,
                'total_kg' => $total_kg,
                'valor_kg' => $product->valor_atual,
                'valor' => $valor,
                'created_at' => date("Y-m-d H:i:s")

            ];

            $newProduct = $productCartModel->create($insert);

            if (!$newProduct) {
                return [
                    'status' => false,
                    'msg' => 'Não foi possível adicionar o produto.',
                    'message' => 'Não foi possível adicionar o produto.'
                ];
            }
        }

        $list = $this->list($client, $company);
        $total_carrinho = (float) array_get($list, 'price_total', 0);

        $jsonMock = [];

        $jsonMock['status'] = true;
        $jsonMock['valor'] = formata_moeda($total_carrinho);
        $jsonMock['valorMinimoAtingido'] = ($total_carrinho >= $clientCompany->min_order) ? true : false;
        $jsonMock['valorMinimo'] = formata_moeda($clientCompany->min_order);
        if ($jsonMock['valorMinimoAtingido']) {
            $jsonMock['valorFaltante'] = 0;
        } else {
            $jsonMock['valorFaltante'] = formata_moeda($clientCompany->min_order - $total_carrinho);
        }
        return $jsonMock;
    }

    /**
     * Remove product
     * @param int $productId product_id
     * @param int $clientId  clientId
     * @param int $company   company
     * @return array
     **/
    public function removeProduct($productId, $clientId, $company)
    {
        $cart = new Cart();
        $userCart = $cart->where(['id_cliente' => $clientId, 'company_id' => $company])->first();

        if (!isset($userCart->id)) {
            return [
                'status' => false,
                'msg' => __('Mobile/cart.cart_not_found'),
                'message' => __('Mobile/cart.cart_not_found')
            ];
        }
        $productCart = ProductCart::where('id', $productId)->get();

        $product = $productCart->first();
        if ($productCart->count() > 0) {
            $product->update([
                'status' => 0,
                'deleted_at' => date("Y-m-d H:i:s")
            ]);

            return [
                'status' => true,
                'msg' => 'Produto removido com sucesso!',
                'message' => 'Produto removido com sucesso!'
            ];
        }
        return [
            'status' => false,
            'msg' => 'Não foi possível remover o item.',
            'message' => 'Não foi possível remover o item.'
        ];
    }

    /**
     * List cart
     * @param Client  $client  client
     * @param Company $company company entity
     * @return array
     **/
    public function list(Client $client, Company $company)
    {
        $clientCompany = $client->setClientCompany($company);

        $_cartResult = DB::table('carrinho')
            ->selectRaw(
                'carrinho.*'
            )
            ->where([
                'carrinho.id_cliente' => $client->id,
                'carrinho.status' => 1,
                'carrinho.deleted_at' => null,
                'carrinho.company_id' => $company->id
            ])
            ->first();

        $jsonCart = [];
        $jsonCart['endereco'] = $clientCompany->address . ', ' . $clientCompany->city . '/' . $clientCompany->state;
        $jsonCart['total'] = 0;
        $blockCart = false;

        $_productsResult = collect([]);
        if (isset($_cartResult->id)) {
            $_productsResult = DB::table('carrinho_produtos')
                ->selectRaw(
                    'carrinho_produtos.*,
                    carrinho_produtos.id as item_id,
                    carrinho_produtos.id,
                    produtos.nome,
                    produtos.marca,
                    produtos.foto,
                    produtos.gramatura,
                    produtos.medida,
                    produtos.peso_caixa,
                    produtos.codigo,
                    produtos.exibicao_app,
                    produtos.company_id'
                )
                ->join('produtos', 'carrinho_produtos.cod_produto', 'produtos.codigo')
                ->join('carrinho', 'carrinho.id', 'carrinho_produtos.carrinho_id')
                ->where([
                    'carrinho_produtos.carrinho_id' => $_cartResult->id,
                    'carrinho_produtos.status' => 1,
                    'carrinho_produtos.deleted_at' => null,
                    'produtos.deleted_at' => null,
                    'produtos.company_id' => $company->id
                ])
                ->get();
        }

        $temGrade = true;
        $jsonCart['valorMinimo'] = formata_moeda($clientCompany->min_order);

        /**
         * Prepara os dados
         */
        $produtos = [];
        foreach ($_productsResult as $_produto) {
            $_fefoResult = DB::table('fefo')
                ->selectRaw('SUM(fefo.volume) as volume_disponivel, fefo.cod_origem, produtos.peso_caixa')
                ->join('produtos', function ($query) {
                    return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                        ->whereRaw('produtos.company_id = fefo.company_id')
                        ->whereNull('produtos.deleted_at');
                })
                ->whereRaw('fefo.volume >= ?', [$_produto->total_kg])
                ->where([
                    'fefo.date' => date("Y-m-d"),
                    'fefo.semanas' => $_produto->semanas,
                    'fefo.cod_origem' => $_produto->cod_origem,
                    'fefo.codigo_produto' => $_produto->cod_produto,
                    'fefo.valor_atual' => $_produto->valor_kg,
                    'fefo.deleted_at' => null,
                    'produtos.company_id' => $company->id
                ])
                ->whereIn('fefo.faixa_fefo', $clientCompany->tracks)
                ->groupBy(DB::Raw('fefo.codigo_produto'))
                ->first();

            $msg = '';
            if ($_fefoResult) {
                $_volumeDisponivel = $_fefoResult->volume_disponivel. 'Kg';
                $_qtdCaixas = floor(floatval($_volumeDisponivel) / floatval($_fefoResult->peso_caixa));
                $_qtdCaixas = ($_qtdCaixas >= 2) ? $_qtdCaixas . ' caixas' : $_qtdCaixas . ' caixa';
                $leadTimeService = new LeadTimeService();
                $_logistica = $leadTimeService->getLogistica(
                    [$client->id],
                    $_fefoResult->cod_origem,
                    $company->id,
                    true
                );
                $_logistica = $_logistica[$client->id];

                if (!$_logistica['aceita_compra']) {
                    $temGrade = false;
                    $proxDataCompra = date('d/m', strtotime($_logistica['visao_logistica']));
                    $msg = 'SEM LOGÍSTICA, SUA PRÓXIMA DATA DE COMPRAS É: ' . dataMySQLtoPT($proxDataCompra);
                    $blockCart = true;
                }

                $jsonCart['total'] += $_produto->valor;

                $_data_entrega[] = $_logistica['data_entrega'];
            } else {
                $_volumeDisponivel = '0Kg';
                $_qtdCaixas = 'indisponível';
                $_logistica = false;
                $msg = 'QUANTIDADE INDISPONÍVEL';
                $blockCart = true;
            }

            $validateLabel = (new CompanySettingService())->getPeriodView($company, $_produto->semanas);

            $produtos[$_produto->item_id] = [
                "nome" => $_produto->nome,
                "empresa" => $_produto->marca,
                "img" => env('PRODUCT_IMAGES_BASE_URL') . 'souk-company/' . $_produto->company_id .'/'. $_produto->foto,
                "peso" => volume($_produto->total_kg).'Kg',
                "caixa" => (intval($_produto->qtd_caixas) > 1)
                    ? $_produto->qtd_caixas . ' caixas'
                    : $_produto->qtd_caixas . ' caixa',
                "caixas" => $_qtdCaixas,
                "validade" => $_produto->semanas . ' ' . $validateLabel,
                "preco" => formata_moeda($_produto->valor_kg),
                "precoTotal" => formata_moeda($_produto->valor),
                "preco_float" => (float) $_produto->valor_kg,
                "pesoCaixa" => volume($_produto->peso_caixa),
                "precoAtualUn" => unitPrice($_produto->valor_kg, $_produto->gramatura),
                "precoUn" => unitPrice($_produto->valor_kg, $_produto->gramatura),
                "unidadeMedida" => getUnit(
                    $_produto->valor_kg,
                    $_produto->gramatura,
                    $_produto->medida,
                    Config::get('mobile_constants.unidade_un')
                ),
                "padraoMedida" => ucfirst(strtolower($_produto->medida)),
                "gramatura" => formatGrammage($_produto->gramatura),
                "disponivel" => $_volumeDisponivel,
                'appExhibition' => $_produto->exibicao_app,
                "logistica" => $_logistica
            ];

            /**
             * permite finalizar de acordo com valor mínimo e logística
             */
            if($blockCart) {
                $jsonCart['permiteFinalizar'] = false;
                $jsonCart['temGrade'] = true;
                $jsonCart['msg_logistica'] = $msg;
            } elseif (array_sum(array_column($_productsResult->toArray(), 'valor')) < $clientCompany->min_order) {
                $jsonCart['permiteFinalizar'] = false;
            } else {
                if (!$_logistica || !$temGrade) {
                    $jsonCart['temGrade'] = false;
                    $jsonCart['msg_logistica'] = $msg;
                } else {
                    $jsonCart['temGrade'] = true;
                }
                $jsonCart['permiteFinalizar'] = true;
            }
        }

        if (!empty($_data_entrega)) {
            $jsonCart['data_entrega'] = dataMySQLtoPT(min(array_map(function ($item) {
                return $item;
            }, $_data_entrega)));
        } else {
            $jsonCart['data_entrega'] = false;
        }
        $total = (float) $jsonCart['total'];

        if ($clientCompany->min_order > $total) {
            $minOrderLabel = __('Mobile/cart.min_order', [
                'value' => formata_moeda($clientCompany->min_order - $total)
            ]);
        }

        $jsonCart['price_total'] = $jsonCart['total'];
        $jsonCart['total'] = formata_moeda($jsonCart['total']);
        $jsonCart['min_order_label'] = $minOrderLabel ?? false;
        $jsonCart['status'] = true;
        $jsonCart['produtos'] = $produtos;

        return  $jsonCart;
    }

    /**
     * Update Cart
     * @param Client  $client        client
     * @param Company $company       company
     * @param int     $productCartId productCartId
     * @param int     $qtd_caixas    qtd_caixas
     * @return array
     **/
    public function updateCart(Client $client, Company $company, $productCartId, $qtd_caixas)
    {
        $clientCompany = $client->setClientCompany($company);

        $product = ProductCart::where([
            'id' => $productCartId
        ])->first();

        if (!isset($product->id)) {
            return [
                'status' => false,
                'msg' => __('Mobile/cart.product_cart_not_found'),
                'message' => __('Mobile/cart.product_cart_not_found')
            ];
        }

        $_produtoResult = $product->toArray();

        $cart = new Cart();

        $client_cart = $cart->where(
            [
                'id_cliente' => $client->id,
                'id' => $_produtoResult['carrinho_id'],
                'company_id' => $company->id
            ]
        )->first();

        if (!$client_cart) {
           return [
               'status' => false,
               'msg' => __('Mobile/cart.cart_not_found'),
               'message' => __('Mobile/cart.cart_not_found')
           ] ;
        }

        if ($client_cart->id_cliente != $client->id) {
            return [
                'status' => false,
                'msg' => __('Mobile/cart.cart_of_other_client'),
                'message' => __('Mobile/cart.cart_of_other_client')
            ];
        }

        $_fefoResult = DB::table('fefo')
            ->selectRaw('fefo.volume as volume_disponivel, produtos.peso_caixa')
            ->join('produtos', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('produtos.deleted_at');
            })
            ->join('leadtime', 'leadtime.cod_origem', 'fefo.cod_origem')
            ->where([
                'fefo.date' => date('Y-m-d'),
                'fefo.semanas' => $_produtoResult['semanas'],
                'fefo.cod_origem' => $_produtoResult['cod_origem'],
                'fefo.codigo_produto' => $_produtoResult['cod_produto'],
                'fefo.valor_atual' => $_produtoResult['valor_kg'],
                'fefo.deleted_at' => null,
                'leadtime.company_id' => $company->id
            ])
            ->whereIn('fefo.faixa_fefo', $clientCompany->tracks)
            ->groupBy(DB::Raw('fefo.codigo_produto'))
            ->first();

        /**
         * Produto não disponível
         */
        if (!isset($_fefoResult->volume_disponivel)) {
            return [
                'status' => false,
                'msg' => __('Mobile/cart.not_change_cart_product'),
                'message' => __('Mobile/cart.not_change_cart_product')
            ];
        }

        $total_kg = number_format($qtd_caixas * $_fefoResult->peso_caixa, 2, '.', '');
        $valor = number_format(($qtd_caixas * $_fefoResult->peso_caixa) * $_produtoResult['valor_kg'], 2, '.', '');

        /**
         * Quantidade acima do volume disponível
         */

        if ($_fefoResult->volume_disponivel < $total_kg) {
            $boxAvailable = floor($_fefoResult->volume_disponivel / $_fefoResult->peso_caixa);

            return [
                'status' => false,
                'msg' => trans_choice('Mobile/cart.volume_unavailable', $boxAvailable, ['value' => $boxAvailable]),
                'message' => trans_choice('Mobile/cart.volume_unavailable', $boxAvailable, ['value' => $boxAvailable])
            ];
        }

        $product->update([
            'qtd_caixas' => $qtd_caixas,
            'total_kg' => $total_kg,
            'valor' => $valor
        ]);

        return [
            'status' => true,
            'msg' => __('Mobile/cart.changed_product_cart'),
            'message' => __('Mobile/cart.changed_product_cart')
        ];
    }

    /**
     * Method to finish an order
     * @param Client  $client  client  client
     * @param array   $option  option  [aceita_compra, 'client_company_payment_id', 'client_card_id']
     * @param Company $company company Company id
     * @return array
     */
    public function finish(Client $client, array $option, Company $company)
    {
        /**
         *  Não permite lances para clientes inativos
         */
        $clientCompany = $client->setClientCompany($company);

        $clientCompanyPaymentService = new ClientCompanyPaymentService();

        if ($clientCompany->status === 0) {
            throw new MobileException(false, __('Mobile/bid.bids_and_buys_blocked'));
        }

        $_cartResult  = DB::table('carrinho')
            ->selectRaw(
                'carrinho.*'
            )
            ->join('clientes', 'clientes.id', 'carrinho.id_cliente')
            ->where([
                'carrinho.id_cliente' => $client->id,
                'carrinho.status' => 1,
                'carrinho.deleted_at' => null,
                'carrinho.company_id' => $company->id
            ])
            ->first();

        if (!isset($_cartResult->id)) {
            throw new MobileException(false, __('Mobile/order.finish.cart_not_found'));
        }

        /**
         * carrega os produtos do carrinho
         */

        $_productsResult = DB::table('carrinho_produtos')
            ->selectRaw(
                'carrinho_produtos.*,
                carrinho_produtos.id as item_id,
                produtos.id as produto_id,
                produtos.nome,
                produtos.marca,
                produtos.foto'
            )
            ->join('carrinho', 'carrinho.id', 'carrinho_produtos.carrinho_id')
            ->join('produtos', function ($query) {
                return $query->on('carrinho_produtos.cod_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = carrinho.company_id')
                    ->whereNull('produtos.deleted_at');
            })
            ->where([
                'carrinho_produtos.carrinho_id' => $_cartResult->id,
                'carrinho_produtos.status' => 1,
                'carrinho_produtos.deleted_at' => null,
                'produtos.company_id' => $company->id
            ])
            ->get();

        $clientCompanyPayment = $client->clientCompanyPayments()->find($option['client_company_payment_id']);

        $isCompanyCredit = true;

        if ($clientCompanyPayment) {
            $payment = $clientCompanyPayment->companyPayment->payment;

            $isCompanyCredit = $payment->isCompanyCredit;
        }

        $price = $_productsResult->sum('valor');

        if ($price < $clientCompany->min_order) {
            throw new MobileException(false,
                __('order.reason_cancellation.min_order', ['min_order' => $clientCompany->min_order])
            );
        }

        if ($isCompanyCredit && $price > $clientCompany->account_balance) {
            throw new MobileException(false,
                __('order.reason_cancellation.account_ballance', ['price' => price_br($clientCompany->account_balance)])
            );
        }

        $_insertProduto = array();

        /**
         * Cadastra os produtos do pedido
         */
        $produtoSemVolume = '';
        $total = 0;

        $fefoService = new FefoService();

        $reduceVolumeFefo = array_get($company->settings, 'reduceVolumeFefoCheckoutCart', false);

        foreach ($_productsResult as $_produto) {
            $dados_fefo = Fefo::where([
                "date" => date('Y-m-d'),
                "codigo_produto" => $_produto->cod_produto,
                "cod_origem" => $_produto->cod_origem,
                "semanas" => $_produto->semanas,
                "deleted_at" => null,
            ])
                ->whereIn('fefo.faixa_fefo', $clientCompany->tracks)
                ->where("volume", ">=", $_produto->total_kg)
                ->first();

            if (!isset($dados_fefo->id)) {
                throw new MobileException(
                    false,
                    __('Mobile/order.finish.product_unavailable'),
                    ['action' => __('Mobile/order.finish.back_cart')]
                );
            }

            $_insertProduto[] = [
                'produto_id' => $_produto->cod_produto,
                'cod_origem' => $_produto->cod_origem,
                'semanas' => $_produto->semanas,
                'qtd_caixas' => $_produto->qtd_caixas,
                'original_box_amount' => $_produto->qtd_caixas,
                'total_kg' => $_produto->total_kg,
                'valor_kg' => $_produto->valor_kg,
                'valor' => $_produto->valor,
                'status_id' => Status::type(Status::PENDING)->id,
                'cd_empresa' => $dados_fefo->cd_empresa,
                'cd_faixa_fefo' => $dados_fefo->cd_faixa_fefo,
                'ds_chave_seara' => $dados_fefo->ds_chave_seara,
                'created_at' => date("Y-m-d H:i:s")
            ];

            if ($reduceVolumeFefo) {
                $fefoService->decrementVolume($dados_fefo, $_produto->total_kg);
            }

            $total += $_produto->valor;
        }

        $reduceAccountBalance = array_get($company->settings, 'reduceAccountBalanceCheckoutCart', false);

        if ($isCompanyCredit && $reduceAccountBalance) {
            (new ClientService())->decrementAccountBalance($company, $client, $total);
        }

        /**
         * Falha na validação de volume
         * Dependente de atualização da tela de carrinho do App
         */

        $order = [
            'id_cliente' => $client->id,
            'data' => date("Y-m-d"),
            'created_at' => date("Y-m-d H:i:s"),
            'company_id' => $company->id,
            'status_id' => Status::type(Status::PENDING)->id,
            'order_product' => $_insertProduto,
            'client_company_payment_id' => $option['client_company_payment_id']
        ];

        if (isset($option['client_card_id'])) {
            $clientCardService = new ClientCardService();
            $order['client_card_id'] = $option['client_card_id'];
            $order['cc_months'] = $option['cc_months'];

            $clientCard = ClientCard::find($order['client_card_id']);
            $quotaValue = $clientCardService->calculateQuotaValue(
                $clientCard,
                $total,
                $order['cc_months']
            );

            $clientCardService->setDefault($clientCard);

            $order['tax_percent'] = $quotaValue['tax'];
        }


        if (empty($option['client_company_payment_id'])) {

            $companyPaymentCredit = $clientCompanyPaymentService
                ->getClientCompanyPaymentCredit($company, $client);

            if ($companyPaymentCredit) {
                $order['client_company_payment_id'] = $companyPaymentCredit->id;
            }
        }

        $clientCompanyPaymentService->validateClientCompanyPayment(
            $company,
            $client,
            $price,
            $order['client_company_payment_id']
        );

        try {
            $createOrder = (new OrderService())->createOrder($order, $company);

            (new ClientsCompaniesService())->setCheckoutAt($clientCompany, 'cart');
        } catch (\Exception $e) {
            throw new MobileException(false, __('Mobile/order.finish.create_order_with_error'));
        }

        /**
         * Exclui os produtos do carrinho
         */

        $productCart = ProductCart::where([
            'carrinho_id' => $_cartResult->id,
            "deleted_at" => null
        ])->update([
            'status' => 2, 'deleted_at' => date("Y-m-d H:i:s")
        ]);

        return [
            'status' => true,
            'title' => __('Mobile/order.finish.title'),
            'description' => __('Mobile/order.finish.description')
        ];
    }
}
