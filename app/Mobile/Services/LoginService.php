<?php

namespace App\Mobile\Services;

use App\Auth\Services\AuthService;
use App\Bid\Bid;
use App\Client\Client;
use App\Client\Services\ClientCompanyPaymentService;
use App\Company\Company;
use App\LeadTime\LeadTime;
use App\Log\LogEmail;
use App\Mobile\Exceptions\MobileException;
use App\Mobile\Mail\ChangePassword;
use App\Mobile\Mail\ResetClientPassword;
use App\Order\Order;
use DB;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cache;
use App\Core\Entities\Status;

class LoginService extends AuthService
{
    protected $client;
    protected $logEmail;

    /**
     * LoginService constructor.
     * @param Client   $client   Used to set the Client
     * @param LogEmail $logEmail Used to set the LogEmail
     */
    public function __construct(Client $client, LogEmail $logEmail)
    {
        $this->client = new Client();
        $this->logEmail = $logEmail;
    }

    /**
     * Client Login
     * @param cnpj     $cnpj     Client CNPJ
     * @param password $password Password
     * @return Array
     */
    public function login($cnpj, $password) : array
    {
        try {
            $loginClient = $this->loginClient($cnpj, $password);
            return $this->check($loginClient['client'], $loginClient['authorization_token']);
        } catch (AuthenticationException $e) {
            throw new MobileException(false, __('auth.failed'));
        }
    }

    /**
     * Client check
     * @param Client $client             Client
     * @param string $authorizationToken Token
     * @return boolean|array
     */
    public function check(Client $client, $authorizationToken)
    {
        if(!$client) {
            return false;
        }

        $clientCompany = $client->companies->first()->clientCompany;
        $status = isset($clientCompany->id) ? 1 : 0;
        $isEmailEmpty = $client->email === null;

        if ($client->email_verified <= 0 && strlen($client->email) > 1) {

            $status = new Status();
            $pendingStatus = $status::type($status::PENDING);
            $authorizedStatus = $status::type($status::AUTHORIZED);

            if ($client->confirm_email == $pendingStatus->id) {
                $client->confirm_email = $authorizedStatus->id;
            }

            $client->email_verified = 1;
            $client->save();
        }

        $newOrders = Order::where(['id_cliente' => $client->id, 'nao_lido' => 1])->count();
        $totalOrders = Order::where(['id_cliente' => $client->id, 'lance_id' => null])->count();
        $newBids = Bid::where(['id_cliente' => $client->id, 'nao_lido' => 1])->count();
        $totalBids = Bid::where(['id_cliente' => $client->id])->count();

        $canBuy = $this->logistica($client->cnpj);

        $creditCardEnabled = (new ClientCompanyPaymentService)->creditCardEnabled($client);

        return [
            'access_token' => $authorizationToken,
            'client_id' => $client->id,
            'ativo' => $status,
            'name' => $client->name,
            'nome' => $client->name,
            'cnpj' => maskPrivateCNPJ($client->cnpj, '##.###.***/##**-**'),
            'phone' => $client->phone,
            'email' => $client->email,
            'confirm_sms' => $client->confirm_sms,
            'confirm_email' => $client->confirm_email,
            'endereco' => sprintf(
                "%s - %s/%s", $clientCompany->address, $clientCompany->city, $clientCompany->state
            ),
            'vendedores' => $client->seller,
            'first_login' => $isEmailEmpty,
            'first_login_day' => false,
            'novos_pedidos' => $newOrders,
            'novos_lances' => $newBids,
            'total_pedidos' => $totalOrders,
            'total_lances' => $totalBids,
            'aceita_compra' => $canBuy,
            'cc_enabled' => $creditCardEnabled
        ];
    }


    /**
     * Obtem visao logística e datas de embarque/entrega
     * @param cnpj $cnpj Client CNPJ
     * @return Boolean
     */
    protected function logistica($cnpj): bool
    {
        $_w = date('w'); // hoje
        $_Hi = date('Hi'); //HHmm
        $aceita_compra = true;

        $_leadtimeResults = LeadTime::where([
            'cnpj' => $cnpj
        ])->get()->toArray();

        if (count($_leadtimeResults) == 0) {
            return false;
        }

        foreach ($_leadtimeResults as $_leadtimeResult) {
            $_horaCorte = intval($_leadtimeResult['limit_hour']);
            // array de visao logistica
            $_arrVs = [
                0 => $_leadtimeResult['sunday'],
                1 => $_leadtimeResult['monday'],
                2 => $_leadtimeResult['tuesday'],
                3 => $_leadtimeResult['wednesday'],
                4 => $_leadtimeResult['thursday'],
                5 => $_leadtimeResult['friday'],
                6 => $_leadtimeResult['saturday'],
            ];

            foreach ($_arrVs as $key => $value) { // somente os dias com embarque
                if ($value == 0) {
                    unset($_arrVs[$key]);
                }
            }

            $_arrVsLg = [];

            // array com todos os dias com visao logística
            foreach ($_arrVs as $key => $value) {
                // só inclui o dia do embarque se for anterior ou dentro do horario de corte
                if (($key != $_w)) {
                    $_arrVsLg[] = $key;
                }
                if ($key == $_w && $_Hi <= $_horaCorte) {
                    $_arrVsLg[] = $key;
                }
                // demais dias
                for ($_vs = 1; $_vs <= $_leadtimeResult['prediction']; $_vs++) {
                    $_d = $key - $_vs;
                    if ($_d < 0) {
                        $_d = $_d + 7;
                    }
                    $_arrVsLg[] = $_d;
                }
            }
            // verifica se hoje está dentro da visão logística
            if ($aceita_compra) {
                if (array_search($_w, $_arrVsLg) !== false) {
                    $aceita_compra = true;
                } else {
                    $aceita_compra = false;
                }
            }
        }

        return $aceita_compra;
    }

    /**
     * Password reset
     * @param cnpj $cnpj Client CNPJ
     * @return array
     */
    public function resetPassword($cnpj)
    {
        $cnpj = sanitize_number($cnpj);

        $clientData = $this->client->where('cnpj', $cnpj)->firstOrFail();

        if ($clientData->email_verified == 0 || $clientData->email == "") {
            return [
                'status' => false,
                'message' => __('auth.email_vefified_false'),
                'email_verified' => false
            ];
        }
        $newPassword = generatePassword($cnpj);

        $clientData->password = $newPassword;

        try {
            $saveClient = $clientData->save();
        } catch(Exception $e) {
            throw new MobileException(false, __('auth.change_password.reset_error'));
        }

        //Se o cliente possui email válido envia email pra ele
        if($clientData->email != "" && intval($clientData->email_verified) == 1) {
            $params = [
                'to' => $clientData->email,
                'password' => $newPassword,
                'name' => $clientData->contact_name ?? $clientData->name,
                'email' => $clientData->email,
                'cnpj' => $clientData->cnpj,
            ];

            $sendMail = New ResetClientPassword($params);

            Mail::send($sendMail);

            $this->logEmail->create([
                'page' => "Recuperação de senha do cliente",
                'subject' => "Contato Souk | Recuperar Senha!",
                'message' => "
                    Voc&ecirc; est&aacute; recebendo este e-mail
                    porque recebemos um pedido de redefinição de senha para sua conta.
                    Segue sua nova senha: <strong>'".$newPassword."'</strong>
                    <br /><br />
                    Importante: Desativamos sua senha anterior",
                "emails" =>  $clientData->email
            ]);

            return [
                'status' => 'true',
                'msg' => __('auth.change_password.email_send'),
                'message' => __('auth.change_password.email_send')
            ];
        }
    }

    /**
     * function to update client after access first
     * @param Client client $client Client instance
     * @param string email  $email  Email
     * @param string phone  $phone  Phone
     * @return array
     */
    public function accessFirst(Client $client, $email, $phone) : array
    {
        $clientModel = new Client();

        $clientData = $clientModel->find($client->id);

        if ($clientData) {
            $update = array(
                "email" => $email,
                "telefone" => $phone,
            );

            $clientData->update($update);

            $msg = "Primeiro acesso do cliente salvo com sucesso.";

            return [
                'status' => true,
                'message' => $msg
            ];
        }

        return [
            'status' => false,
            'message' => 'Erro ao salvar primeiro acesso.'
        ];
    }

    /**
     * Method to change a password
     * @param Client client      $client      Client
     * @param string password    $password    Password
     * @param string newPassword $newPassword New Password
     * @return array
     */
    public function changePassword(Client $client, $password, $newPassword)
    {
        $clientId = $client->id;

        $where_user = [
            'id' => $clientId,
            'password' => md5($password)
        ];

        /**
         * Verifica cliente normal
         */
        $clientData = $this->client->where($where_user)->first();

        if ($clientData) {

            $saveClient =  $clientData->update(["password" => $newPassword]);

            if ($saveClient && $clientData->email != "") {
                $changePassword = New ChangePassword([
                    'password' => $newPassword,
                    'email' => $clientData->email,
                    'name' => $clientData->contact_name ?? $clientData->name,
                    'to' => $clientData->email,
                    'cnpj' => $clientData->cnpj,
                    'subject' => 'Contato Souk | Sua senha foi alterada'
                ]);

                $changePassword->build();

                $var =  Mail::send($changePassword);

                 return [
                    'message' => "Senha atualizada com sucesso!",
                    'status' => true
                ];
            }

            if ($clientData->email == "") {
                $retorno = [
                    'message' => "Senha atualizada com sucesso! "
                ];

                /**
                 * Envia email para os vendedores
                 */
                $sellers = $this->client->findOrFail($clientData->id)->seller()->get();

                foreach ($sellers as $seller) {
                    $changePassword = New ChangePassword([
                        'password' => $newPassword,
                        'email' => $seller->email,
                        'name' => $seller->name,
                        'to' => $seller->email,
                        'cnpj' => $clientData->cnpj,
                        'subject' => 'Contato Souk | Sua senha foi alterada'
                    ]);

                    $changePassword->build();

                    $var =  Mail::send($changePassword);
                }

                return [
                    'status' => true,
                    'message' =>  $retorno['message'] .=
                        "A nova senha foi enviada para os e-mails dos
                        vendedores relacionados ao cliente!"
                ];
            }
        }
        return [
            'status' => false,
            'message' =>  'Senha atual inválida'
        ];

    }

    /**
     * Method to update a register updated field
     * @param Client  $client  client object
     * @param Company $company company object
     * @param array   $params  [name,phone,email,ie,version]
     * @return bool
     */
    public function registerUpdated(Client $client, Company $company,  $params)
    {
        $data = [
            'name' => $params['name'],
            'phone' => $params['phone'],
            'email' => $params['email'],
            'ie' => $params['ie'],
            'version' => $params['version'],
            'company_id' => $company->id,
            'updated_at' => now(),
        ];

        $client->register_updated = json_encode($data);
        $client->updated_at = now();
        $client->update();

        Cache::store('redis')->forget("home.{$company->id}.{$client->cnpj}");

        return ['status'=> true,
            'message' => __('Mobile/client.register_updated')];
    }
}
