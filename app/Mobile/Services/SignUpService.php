<?php
namespace App\Mobile\Services;

use App\Client\Client;
use App\Client\IndependentClient;
use App\Client\Notifications\ClientWelcomeNotification;
use App\Client\Notifications\IndependentClientWelcomeNotification;
use App\Client\Notifications\MobileConfirmationNotification;
use App\Mobile\Exceptions\MobileException;
use App\Mobile\Traits\ValidatesClientRegistration;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class SignUpService
{
    use ValidatesClientRegistration;

    const STEP_PHONE_VALIDATION = 2;
    const STEP_INDEPENDENT_SIGN_UP = 3;
    const STEP_CONTACT_INFORMATION = 4;
    const STEP_COMPLETED = 5;

    public function registration(string $cnpj, array $data): array
    {
        $step = $this->verifyPreviousRegistrations($cnpj);

        $nextStep = array_get($step, 'next_step');

        if ($nextStep == self::STEP_INDEPENDENT_SIGN_UP) {

            $independentClient = IndependentClient::where('cnpj', $cnpj)
                ->where('integrado', 0)
                ->first();

            if (!$independentClient) {
                throw new MobileException(false, false, ['next_step' => self::STEP_CONTACT_INFORMATION]);
            }

            $params = [
                'nome' => array_get($data, 'social_reason'),
                'social_reason' => array_get($data, 'social_reason'),
                'state_registration' => array_get($data, 'state_registration'),
                'zipcode' => array_get($data, 'zipcode'),
                'endereco' => array_get($data, 'address'),
                'municipio' => array_get($data, 'city'),
                'uf' => array_get($data, 'state'),
                'address_number' => array_get($data, 'address_number'),
                'neighborhood' => array_get($data, 'neighborhood'),
                'address_complement' => array_get($data, 'address_complement'),
            ];

            $independentClient->update($params);
        }

        return $this->verifyPreviousRegistrations($cnpj);
    }

    public function updateContactInformation(string $cnpj, string $email, string $contactName, string $ip)
    {
        $step = $this->verifyPreviousRegistrations($cnpj);

        $nextStep = array_get($step, 'next_step');

        if ($nextStep == self::STEP_CONTACT_INFORMATION) {

            $cnpj = format_cnpj($cnpj);
            $password = generatePassword(rand(12, 100));

            $client = $this->getClientInstance($cnpj);

            if ($client instanceof IndependentClient) {
                $client->update([
                    'email' => $email,
                    'contact_name' => $contactName,
                    'registration_ip' => $ip,
                    'terms_of_use_version' => config('souk.terms_of_use_version'),
                    'privacy_policy_version' => config('souk.privacy_policy_version'),
                ]);

                $params = [
                    'contact_name' => $client->contact_name,
                    'social_reason' => $client->social_reason,
                    'state_registration' => $client->state_registration,
                    'address' => $client->address,
                    'number' => $client->address_number,
                    'address_complement' => $client->address_complement,
                    'neighborhood' => $client->neighborhood,
                    'city' => $client->city,
                    'state' => $client->state,
                    'zipcode' => $client->zipcode,
                    'phone' => $client->phone,
                    'email' => $client->email,
                    'to' => $client->email,
                    'cnpj' => maskCnpj($client->cnpj),
                ];

                $client->notify(new IndependentClientWelcomeNotification($params));

                return [
                    'status' => true,
                    'message' => __(
                        'Mobile/signUp.contact_information.independent_client_updated',
                        [
                            'email' => $client->email,
                            'social_reason' => $client->social_reason,
                        ]
                    ),
                    'client_type' => 'independent_client',
                    'phone' => $client->phone,
                    'next_step' => self::STEP_COMPLETED,
                ];
            }

            if ($client instanceof Client) {
                $client->update([
                    'email' => $email,
                    'contact_name' => $contactName,
                    'confirm_email' => 3,
                    'password' => $password,
                    'registration_ip' => $ip,
                    'terms_of_use_version' => config('souk.terms_of_use_version'),
                    'privacy_policy_version' => config('souk.privacy_policy_version'),
                ]);

                $clientCompany = $client->companies->first()->clientCompany;

                $params = [
                    'password' => $password,
                    'email' => $client->email,
                    'to' => $client->email,
                    'cnpj' => maskCnpj($client->cnpj),
                    'name' => $contactName,
                    'contact_name' => $client->contact_name,
                    'address' => $clientCompany->address,
                    'city' => $clientCompany->city,
                    'state' => $clientCompany->state,
                    'phone' => $client->phone,
                    'email' => $client->email,
                ];

                $client->notify(new ClientWelcomeNotification($params));

                return [
                    'status' => true,
                    'message' => __(
                        'Mobile/signUp.contact_information.client_updated',
                        [ 'email' => $client->email ]
                    ),
                    'client_type' => 'client',
                    'phone' => $client->phone,
                    'next_step' => self::STEP_COMPLETED,
                ];
            }
        }

        return $step;
    }

    public function getClientInstance(string $cnpj): Model
    {
        $independentClient = IndependentClient::where('cnpj', $cnpj)->where('integrado', 0)->first();
        if ($independentClient !== null) {
            return $independentClient;
        }

        $client = Client::where('cnpj', $cnpj)->first();

        if ($client !== null) {
            if ($client->email_verified == 1) {
                Log::info("CADASTRO: Cliente de CNPJ {$client->cnpj} com cadastro ativo tentou se cadastrar novamente");
                throw new MobileException(
                    false,
                    __('Mobile/signUp.first_step.client_exists'),
                    ['next_step' => false, 'status' => true]
                );
            }

            return $client;
        }

        Log::info("CADASTRO: Novo cadastro para CNPJ {$cnpj}");

        return IndependentClient::create(['cnpj' => $cnpj]);
    }

    public function verifyPreviousRegistrations(string $cnpj)
    {
        $cnpj = format_cnpj($cnpj);
        $client = $this->getClientInstance($cnpj);

        if ($client instanceof IndependentClient) {
            return $this->validateIndependentClient($client);
        }

        if ($client instanceof Client) {
            return $this->validateClient($client);
        }

        throw new MobileException(false, __('Mobile/signUp.first_step.invalid_instance'));
    }

    public function sendPhoneConfirmationCode(string $cnpj, string $phone)
    {
        $client = $this->getClientInstance($cnpj);

        if($client->confirm_sms === 10) {
            Log::info("CADASTRO: Cliente de CNPJ {$cnpj} com telefone confirmado tentou confirmar novamente");
            throw new MobileException(false, __('Mobile/signUp.first_step.phone_already_confirmed'));
        }

        $code = random_int(1000, 9999);
        $confirmation = $client->confirmations()->create([
            'type' => 'sms',
            'value' => $phone,
            'code' => $code,
        ]);

        $client->confirm_sms = 3;
        $client->phone = $phone;
        $client->save();

        Notification::send(
            $confirmation,
            (new MobileConfirmationNotification)->delay(now()->addSeconds(1))
        );

        Log::info("CADASTRO: confirmação de telefone enfileirada para cliente de CNPJ {$cnpj} e telefone {$phone}");

        return [
            'status' => true,
            'message' => __('Mobile/signUp.first_step.phone_confirmation_sent'),
            'phone' => formatPhone($phone),
        ];
    }

    public function confirmClientPhone(string $cnpj, string $code)
    {
        $client = $this->getClientInstance($cnpj);

        $confirmation = $this->validatePhoneConfirmationCode($client, $code);

        $this->persistClientPhone($client, $confirmation->value);

        $client = $client->fresh();

        Log::info("CADASTRO: telefone confirmado para cliente de CNPJ {$cnpj}");

        if ($client instanceof IndependentClient) {
            return $this->validateIndependentClient($client);
        }

        if ($client instanceof Client) {
            return $this->validateClient($client);
        }

        throw new MobileException(false, __('Mobile/signUp.first_step.invalid_instance'));
    }

    public function validatePhoneConfirmationCode(Model $client, string $code)
    {
        $confirmation = $client->confirmations()
            ->where('type', 'sms')
            ->where('code', $code)
            ->first();

        if($confirmation === null) {
            throw new MobileException(false, __('Mobile/signUp.phone_validation.invalid_code'));
        }

        $confirmation->confirmed_at = now();
        $confirmation->save();

        return $confirmation->fresh();
    }

    public function persistClientPhone(Model $client, string $phone)
    {
        $client->confirmations()->delete();

        $client->phone = $phone;
        $client->confirm_sms = 10;
        return $client->save();
    }
}
