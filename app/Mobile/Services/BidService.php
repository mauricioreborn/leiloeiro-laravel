<?php

namespace App\Mobile\Services;

use App\Bid\Bid;
use App\Bid\Services\BidLogService;
use App\Client\Client;
use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Client\Services\ClientCardService;
use App\Client\Services\ClientCompanyPaymentService;
use App\ClientsCompanies\Services\ClientsCompaniesService;
use App\Company\Company;
use App\Company\Services\CompanySettingService;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\LeadTime\Services\LeadTimeService;
use App\Mobile\Exceptions\MobileException;
use App\ProcessedFefo\Service\ProcessedFefoService;
use App\Products\Product;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class BidService extends MobileService
{
    protected $bid;
    protected $unidade_un;

    /**
     * Construct
     * @param Bid $bid bid
     * @return void
     **/
    public function __construct(Bid $bid = null)
    {
        parent::__construct();

        $this->bid = $bid;
        $this->unidade_un = Config::get('mobile_constants.unidade_un');
    }

    /**
     * Bid validate
     * @param Client  $client  client
     * @param int     $fefo_id fefo_id
     * @param Company $company company
     * @return void
     **/
    public function bidValidate(Client $client, $fefo_id, Company $company) : array
    {
        $clientCompany = $client->setClientCompany($company);

        $today = Carbon::now();

        $leadTimeService = new LeadtimeService();

        $product_query = Product::selectRaw('fefo.*,fefo.id as fefo_id, leadtime.*, produtos.*')
        ->join('fefo', function ($query) {
            return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                ->whereRaw('produtos.company_id = fefo.company_id')
                ->whereNull('fefo.deleted_at');
        })
        ->join('leadtime', function ($query) {
            return $query->on('fefo.cod_origem', '=', 'leadtime.cod_origem');
        })
        ->where([
            'fefo.id' => $fefo_id,
            'leadtime.cnpj' => $client->cnpj,
            'produtos.company_id' => $company->id,
            'produtos.deleted_at' => null
        ])
        ->whereIn('fefo.faixa_fefo', $clientCompany->tracks)
        ->whereRaw('fefo.volume > 0')
        ->first();

        /**
        * Produto não disponível
        */
        if (!$product_query)
        {
            throw new MobileException('false', __('Mobile/product.not_available_for_bids'));
        }

        $_produtoResult = $product_query->toArray();

        $nextWeekDay = $leadTimeService->checkWeekDay($today);

        if ($nextWeekDay) {
            $nextWeekDay = $nextWeekDay->format('Y-m-d');
        }

        $valor_caixa = $_produtoResult['valor_atual'] * $_produtoResult['weight'];

        $minimo_caixas = ceil($clientCompany->min_order/$valor_caixa); // pra cima
        $minimo_caixas = ($minimo_caixas < 1) ? 1 : $minimo_caixas;

        $caixas_disponivel = floor($_produtoResult['volume']/$_produtoResult['weight']); // pra baixo

        $_exibicaoCaixas = ($caixas_disponivel > 1) ? $caixas_disponivel.' caixas' : $caixas_disponivel.' caixa';

        $_logistica = $leadTimeService->getLogistica([$client->id], $_produtoResult['cod_origem']);

        $prazoMaxPedido = new \DateTime($_logistica[$client->id]['limite_compra']);
        $prazoMaxPedido = $prazoMaxPedido->format('Y-m-d');

        $logisticVision = ($nextWeekDay === false) ? $_logistica[$client->id]['aceita_compra'] : false;

        $jsonLance = [];

        $jsonLance['precoAtual'] = formata_moeda($_produtoResult['valor_atual']);
        $jsonLance['precoAtualComparacao'] = $_produtoResult['valor_atual'];
        $jsonLance['quantidadeMinima'] = $minimo_caixas;
        $jsonLance['valorMinimo'] = price($clientCompany->min_order);
        $jsonLance['pesoCaixa'] = price($_produtoResult['weight']);
        $jsonLance['disponivelExibicao'] = rangeCaixas($caixas_disponivel);
        $jsonLance['kgDisponivel'] = $_produtoResult['volume'];
        $jsonLance['caixasDisponivel'] = $caixas_disponivel;
        $jsonLance['nextWeekday'] = $nextWeekDay;
        $jsonLance['visaoLogistica'] = $logisticVision;
        $jsonLance['dataVisaoLogistica'] = $_logistica[$client->id]['visao_logistica'];
        $jsonLance['prazoMaxPedido'] = $prazoMaxPedido;
        $jsonLance['precoAtualUn'] = unitPrice($_produtoResult['valor_atual'], $_produtoResult['weight_piece']);
        $jsonLance['unidadeMedida'] = getUnit(
                                        $_produtoResult['valor_atual'],
                                        $_produtoResult['weight_piece'],
                                        $_produtoResult['unit'],$this->unidade_un
                                    );

        $jsonLance['padraoMedida'] = (new ProductService())->padraoMedida($_produtoResult);
        $jsonLance['gramatura'] = formatGrammage($_produtoResult['weight_piece']);


        return $jsonLance;
    }

    /**
     * Add bid
     * @param client        $client        client
     * @param fefo_id       $fefo_id       fefo_id
     * @param price         $price         price
     * @param amount        $amount        amount
     * @param bidType       $bidType       bidType
     * @param acceptPartial $acceptPartial acceptPartial
     * @param choosenDate   $choosenDate   choosenDate
     * @param company       $company       company
     * @return array
     **/
    public function addBid(
        $client,
        $fefo_id,
        $price,
        $amount,
        $bidType,
        $acceptPartial,
        $choosenDate,
        Company $company
    ) {
        $clientCompany = $client->setClientCompany($company);

        /**
         *  Não permite lances para clientes inativos
         **/
        if ($clientCompany->status == 0) {
            throw new MobileException(
                'false',
                __('Mobile/bid.bids_and_buys_blocked'),
                ['errors' => [array('status' => 403)]]
            );
        }

        $fefo_id = $fefo_id;
        $tipo_lance = $bidType;
        $valor = str_replace(',','.',$price);
        $qtd_caixas = $amount;
        $aceite_parcial = $acceptPartial;
        $dataEscolhida = $choosenDate;

        $product_query = Product::selectRaw(
                            'fefo.*,fefo.id as fefo_id, leadtime.*, produtos.*, produtos.id as produto_id'
                        )
        ->join('fefo', function ($query) {
            return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                ->whereRaw('produtos.company_id = fefo.company_id')
                ->whereNull('fefo.deleted_at');
        })
        ->join('leadtime', function ($query) {
            return $query->on('fefo.cod_origem', '=', 'leadtime.cod_origem');
        })
        ->where([
            'fefo.id' => $fefo_id,
            'leadtime.cnpj' => $client->cnpj,
            'produtos.company_id' => $company->id,
            'produtos.deleted_at' => null,
        ])
        ->whereIn('fefo.faixa_fefo', $clientCompany->tracks)
        ->whereRaw('fefo.volume > 0')
        ->first();

        /**
        * Produto não disponível
        */
        if (!$product_query)
        {
            throw new MobileException('false', __('Mobile/product.not_available_for_bids'));
        }

        $_produtoResult = $product_query;

        $total_kg = number_format($qtd_caixas * $_produtoResult['weight'], 2, '.', '');

        // calculado a partir do lance dado
        $valor_total = number_format(($qtd_caixas * $_produtoResult['weight']) * $valor, 2, '.', '');

        /**
        * VALIDA VALOR MÍNIMO DO LANCE
        */
        if ($valor_total < $clientCompany->min_order) {
            throw new MobileException(
                'false',
                __('Mobile/bid.bids_min_value').formata_moeda($clientCompany->min_order)
            );
        }

        /**
        * verifica tipo de lance e data
        */
        if ($tipo_lance == '1') {
            $duracao_lance = date("Y-m-d");
        } elseif ($tipo_lance == '2') {
            $duracao_lance = dataPTtoMySQL($dataEscolhida);
        }

        /**
        * Calcula parcentual de economia
        */
        $percentualEconomia = calcDiscount($_produtoResult['valor_cheio'], $valor);

        $_qtdCaixas = floor( floatval($_produtoResult['volume'])/ floatval($_produtoResult['weight'] ));
        $_qtdCaixas = ($_qtdCaixas >= 2) ? $_qtdCaixas.' caixas' : $_qtdCaixas.' caixa';

        $jsonLance = [];

        $validateLabel = (new CompanySettingService())->getPeriodView($company, $_produtoResult->semanas);

        $jsonLance['nome'] = $_produtoResult->name;
        $jsonLance['empresa'] = $_produtoResult->brand;
        $jsonLance['img'] = $_produtoResult->picture_url;
        $jsonLance['kgCaixas'] = number_format($qtd_caixas * $_produtoResult['weight'],2,',','.').'Kg';
        $jsonLance['dimensaoCaixa'] = $_produtoResult->app_exhibition;
        $jsonLance['precoKg'] = formata_moeda($valor);
        $jsonLance['quantidadeCaixas'] = $qtd_caixas.((intval($qtd_caixas) > 1) ? ' caixas' : ' caixa');
        $jsonLance['validade'] = $_produtoResult->semanas . ' ' . $validateLabel;
        $jsonLance['validadeData'] = 'até '.dataMySQLtoPT($duracao_lance);
        $jsonLance['dataEntrega'] = __('Mobile/bid.bids.labels.delivery_date');
        $jsonLance['valorTotal'] = formata_moeda($valor_total);
        $jsonLance['textoFinal'] = __('Mobile/bid.bids.labels.economy', [
            'economy_percentage' => $percentualEconomia
        ]);
        $jsonLance['precoUn'] = unitPrice($valor,$_produtoResult->weight_piece);
        $jsonLance['unidadeMedida'] = getUnit(
                                        $valor,$_produtoResult->weight_piece,
                                        $_produtoResult->medida,
                                        $this->unidade_un
                                    );

        $jsonLance['padraoMedida'] = (new ProductService())->padraoMedida($_produtoResult);
        $jsonLance['gramatura'] = formatGrammage($_produtoResult->weight_piece);
        $jsonLance['quantidadeUn'] = getUnitAmount($total_kg, $_produtoResult->weight_piece);
        $jsonLance['amountLabel'] = formatAmount($jsonLance['quantidadeUn'], $_produtoResult->weight_piece);

        /**
         * dados para inclusão do lance
         **/

        $jsonLance['lanceCodOrigem'] = $_produtoResult->cod_origem;
        $jsonLance['lanceCodProduto'] = $_produtoResult->codigo;
        $jsonLance['lanceSemanas'] = $_produtoResult->semanas;
        $jsonLance['lanceQtdCaixas'] = $qtd_caixas;
        $jsonLance['lanceTotalKg'] = $total_kg;
        $jsonLance['lanceValorKg'] = $valor;
        $jsonLance['lanceValor'] = $valor_total;
        $jsonLance['lanceAceiteParcial'] = ( intval($aceite_parcial) == 1) ? true : false;
        $jsonLance['lanceDuracaoLance'] = $duracao_lance;

        return $jsonLance;
    }

    /**
     * Add bid
     * @param client        $client        client
     * @param bidId         $bidId         bidId
     * @param price         $price         price
     * @param amount        $amount        amount
     * @param bidType       $bidType       bidType
     * @param acceptPartial $acceptPartial acceptPartial
     * @param choosenDate   $choosenDate   choosenDate
     * @param company       $company       company
     * @return array
     **/
    public function changeBid(
        $client,
        $bidId,
        $price,
        $amount,
        $bidType,
        $acceptPartial,
        $choosenDate,
        Company $company
    ) {
        $clientCompany = $client->setClientCompany($company);

        /**
         *  Não permite lances para clientes inativos
        **/
        if ($clientCompany->status == 0) {
            throw new MobileException(
                'false',
                __('Mobile/bid.bids_and_buys_blocked'),
                ['errors' => [array('status' => 403)]]
            );
        }

        if ($clientCompany) {
            $client_id = $client->id;
            $lance_id = $bidId;
            $tipo_lance = $bidType;
            $valor = str_replace(',' , '.', $price);
            $qtd_caixas = $amount;
            $aceite_parcial = $acceptPartial;
            $cnpj = $client->cnpj;
            $dataEscolhida = $choosenDate;

            /**
            *Carrega os dados do Produto
            */
            $product_query = Product::selectRaw(
                                'fefo.*,fefo.id as fefo_id, leadtime.*,produtos.*,produtos.id as produto_id'
                            )
            ->join('fefo', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('lances', function ($query) {
                return $query->on('lances.cod_produto', '=', 'produtos.codigo');
            })
            ->join('leadtime', function ($query) {
                return $query->on('fefo.cod_origem', '=', 'leadtime.cod_origem');
            })
            ->where([
                'lances.id' => $lance_id,
                'leadtime.cnpj' => $cnpj,
                'produtos.company_id' => $company->id,
                'produtos.deleted_at' => null
            ])
            ->whereIn('fefo.faixa_fefo', $clientCompany->tracks)
            ->whereRaw('fefo.volume > 0')
            ->whereRaw('fefo.date = lances.data')
            ->first();

            /**
            *Produto não disponível
            */
            if (!$product_query)
            {
                throw new MobileException('false', __('Mobile/product.not_available_for_bids'));
            }

            $_produtoResult = $product_query->toArray();

            $total_kg = number_format($qtd_caixas * $_produtoResult['weight'], 2, '.', '');

            // calculado a partir do lance dado
            $valor_total = number_format(($qtd_caixas * $_produtoResult['weight']) * $valor, 2, '.', '');

            /**
            * VALIDA VALOR MÍNIMO DO LANCE
            */
            if ($valor_total < $clientCompany->min_order) {
                throw new MobileException(
                    'false',
                    __('Mobile/bid.bids_min_value').formata_moeda($clientCompany->min_order)
                );
            }

            /**
            * verifica tipo de lance e data
            */
            if ($tipo_lance == '1') {
                $duracao_lance = date("Y-m-d");
            } elseif ($tipo_lance == '2') {
                $duracao_lance = dataPTtoMySQL($dataEscolhida);
            }

            /**
            * Calcula parcentual de economia
            */
            $percentualEconomia = calcDiscount($_produtoResult['valor_cheio'], $valor);

            $_qtdCaixas = floor(floatval($_produtoResult['volume'])/floatval($_produtoResult['weight']));
            $_qtdCaixas = ($_qtdCaixas >= 2) ? $_qtdCaixas.' caixas' : $_qtdCaixas.' caixa';

            $jsonLance = [];
            $validateLabel = (new CompanySettingService())->getPeriodView($company, $_produtoResult['semanas']);

            $jsonLance['lance_id'] = $lance_id;
            $jsonLance['nome'] = $_produtoResult['name'];
            $jsonLance['empresa'] = $_produtoResult['brand'];
            $jsonLance['img'] = $_produtoResult['picture_url'];
            $jsonLance['kgCaixas'] = number_format($qtd_caixas * $_produtoResult['weight'],2,',','.').'Kg';
            $jsonLance['dimensaoCaixa'] = $_produtoResult['app_exhibition'];
            $jsonLance['precoKg'] = formata_moeda($valor);
            $jsonLance['quantidadeCaixas'] = $qtd_caixas.((intval($qtd_caixas) > 1) ? ' caixas' : ' caixa');
            $jsonLance['validade'] = $_produtoResult['semanas'] . ' ' . $validateLabel;
            $jsonLance['validadeData'] = dataMySQLtoPT($duracao_lance);
            $jsonLance['dataEntrega'] = __('Mobile/bid.bids.labels.delivery_date');
            $jsonLance['valorTotal'] = formata_moeda($valor_total);
            $jsonLance['textoFinal'] = __('Mobile/bid.bids.labels.economy', [
                'economy_percentage' => $percentualEconomia
            ]);
            $jsonLance['precoUn'] = unitPrice($valor,$_produtoResult['weight_piece']);
            $jsonLance['unidadeMedida'] = getUnit(
                                            $valor,
                                            $_produtoResult['weight_piece'],
                                            $_produtoResult['unit'],
                                            $this->unidade_un
                                        );
            $jsonLance['padraoMedida'] = (new ProductService())->padraoMedida($_produtoResult);
            $jsonLance['gramatura'] = formatGrammage($_produtoResult['weight_piece']);
            $jsonLance['quantidadeUn'] = getUnitAmount($total_kg,$_produtoResult['weight_piece']);
            $jsonLance['amountLabel'] = formatAmount($jsonLance['quantidadeUn'], $product_query->weight_piece);

            /**
            * dados para inclusão do lance
            */
            $jsonLance['lanceQtdCaixas'] = $qtd_caixas;
            $jsonLance['lanceTotalKg'] = $total_kg;
            $jsonLance['lanceValorKg'] = $valor;
            $jsonLance['lanceValor'] = $valor_total;
            $jsonLance['lanceAceiteParcial'] = ( intval($aceite_parcial) == 1) ? true : false;
            $jsonLance['lanceDuracaoLance'] = $duracao_lance;

            return $jsonLance;
        }
    }

    /**
     * Confirm bid
     * @param client         $client         client
     * @param cod_origem     $cod_origem     cod_origem
     * @param cod_produto    $cod_produto    cod_produto
     * @param aceite_parcial $aceite_parcial aceite_parcial
     * @param duracao_lance  $duracao_lance  duracao_lance
     * @param qtd_caixas     $qtd_caixas     qtd_caixas
     * @param semanas        $semanas        semanas
     * @param total_kg       $total_kg       total_kg
     * @param valor          $valor          valor
     * @param valor_kg       $valor_kg       valor_kg
     * @param array          $payment        Payment array
     * @param Company        $company        company
     * @return array
     **/
    public function confirmBid(
        $client,
        $cod_origem,
        $cod_produto,
        $aceite_parcial,
        $duracao_lance,
        $qtd_caixas,
        $semanas,
        $total_kg,
        $valor,
        $valor_kg,
        array $payment = [],
        Company $company
    ) {
        $clientCompanyPaymentService = new ClientCompanyPaymentService();

        $clientCompany = $client->setClientCompany($company);

        /**
        *  Não permite lances para clientes inativos
        */
        if ($clientCompany->status == 0) {
            throw new MobileException(
                'false',
                __('Mobile/bid.bids_and_buys_blocked'),
                ['errors' => [array('status' => 403)]]
            );
        }

        if ($clientCompany) {
            $client_id = $client->id;
            $aceite_parcial = ( intval($aceite_parcial) == 1) ? true : false;

            $jsonLance = [];

            $_insert = [
                'client_id' => $client_id,
                'date' => date("Y-m-d"),
                'origin_code' => $cod_origem,
                'product_code' => $cod_produto,
                'weeks' => $semanas,
                'box_amount' => $qtd_caixas,
                'weight' => $total_kg,
                'kg_price' => $valor_kg,
                'price' => $valor,
                'partial_accept' => $aceite_parcial,
                'duration' => $duracao_lance,
                'status_id' => Status::type(Status::PENDING)->id,
                'company_id' => $company->id,
                'original_box_amount' => $qtd_caixas,
                'client_company_payment_id' => array_get($payment, 'client_company_payment_id'),
            ];

            if (empty($_insert['client_company_payment_id'])) {
                $companyPaymentCredit = $clientCompanyPaymentService->getClientCompanyPaymentCredit($company, $client);

                if ($companyPaymentCredit) {
                    $_insert['client_company_payment_id'] = $companyPaymentCredit->id;
                }
            }

            $clientCompanyPaymentService->validateClientCompanyPayment(
                $company,
                $client,
                $valor,
                $_insert['client_company_payment_id']
            );

            $clientCardId = array_get($payment, 'client_card_id');

            if (!empty($clientCardId)) {
                $clientCardService = new ClientCardService();

                $_insert['client_card_id'] = $clientCardId;
                $_insert['cc_months'] = array_get($payment, 'cc_months', 1);

                $clientCard = ClientCard::find($clientCardId);
                $quotaValue = $clientCardService->calculateQuotaValue(
                    $clientCard,
                    $_insert['price'],
                    $_insert['cc_months']
                );

                $clientCardService->setDefault($clientCard);

                $_insert['tax_percent'] = $quotaValue['tax'];
            }

            $_lanceInsert = $this->bid->create($_insert);

            if ($_lanceInsert) {
                $bidLogService = new BidLogService();

                $fefo = Fefo::where([
                    'codigo_produto' => $cod_produto,
                    'cod_origem' => $cod_origem,
                    'semanas' => $semanas,
                    'date' => now()->toDateString()
                ])->first();

                if (isset($fefo->id)) {
                    $processFefoService = new ProcessedFefoService();
                    $processFefoService->processedFefo($fefo->id);
                }

                $bidLogParams = [
                    'client_id' => $client_id,
                    'box_amount' => $qtd_caixas,
                    'bid_id' => $_lanceInsert->id
                ];

                $bidLogService->saveBidLog($bidLogParams);

                (new ClientsCompaniesService())->setCheckoutAt($clientCompany);

                return [
                    'status' => true,
                    'title' => __('Mobile/bid.bids.confirm.title'),
                    'description' => __('Mobile/bid.bids.confirm.description')
                ];

            } else {
                throw new MobileException('false', __('Mobile/bid.confirm_fail'));
            }
        }
    }

    /**
     * Edit bid
     * @param client  $client  client
     * @param bidId   $bidId   bidId
     * @param company $company company
     * @return array
     **/
    public function editBid($client, $bidId, Company $company) : array
    {
        $clientCompany = $client->setClientCompany($company);

        $leadTimeService = new LeadtimeService();

        if($clientCompany) {

            $client_id = $client->id;
            $lance_id = $bidId;
            $cnpj = $client->cnpj;

            $bid = $this->bid->where('status_id', Status::type(Status::PENDING)->id)->find($lance_id);

            if (!$bid) {
                throw new MobileException('false', __('Mobile/bid.bids.update.fail'));
            }

            /**
            *Carrega os dados do Produto
            */
            $product_query = Product::selectRaw(
                'fefo.*,fefo.id as fefo_id, leadtime.*, produtos.*,
                 lances.qtd_caixas, lances.valor_kg, lances.total_kg,
                 lances.aceite_parcial, lances.duracao_lance')
            ->join('fefo', function ($query) {
                return $query->on('fefo.codigo_produto', '=', 'produtos.codigo')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('lances', function ($query) {
                return $query->on('lances.cod_produto', '=', 'produtos.codigo');
            })
            ->join('leadtime', function ($query) {
                return $query->on('fefo.cod_origem', '=', 'leadtime.cod_origem');
            })
            ->where([
                'lances.id' => $lance_id,
                'leadtime.cnpj' => $cnpj,
                'produtos.company_id' => $company->id,
                'produtos.deleted_at' => null
            ])
            ->whereIn('fefo.faixa_fefo', $clientCompany->tracks)
            ->whereRaw('fefo.volume > 0')
            ->whereRaw('fefo.date = lances.data')
            ->first();

            /**
            *Produto não disponível
            */
            if (!$product_query)
            {
                throw new MobileException('false', __('Mobile/product.not_available_for_bids'));
            }

            $_produtoResult = $product_query->toArray();

            $valor_caixa = $_produtoResult['valor_kg'] * $_produtoResult['weight'];
            $minimo_caixas = ceil($clientCompany->min_order/$valor_caixa); // pra cima
            $caixas_disponivel = floor($_produtoResult['volume']/$_produtoResult['weight']); // pra baixo

            $_exibicaoCaixas = ($caixas_disponivel > 1) ? $caixas_disponivel.' caixas' : $caixas_disponivel.' caixa';

            $_logistica = $leadTimeService->getLogistica([$client_id], $_produtoResult['cod_origem']);

            $prazoMaxPedido = new \DateTime($_logistica[$client_id]['limite_compra']);
            $prazoMaxPedido = $prazoMaxPedido->format('Y-m-d');

            $jsonLance = [];

            $jsonLance['precoAtual'] = formata_moeda($_produtoResult['valor_atual']);
            $jsonLance['precoAtualComparacao'] = $_produtoResult['valor_atual'];
            $jsonLance['quantidadeMinima'] = $minimo_caixas;
            $jsonLance['valorMinimo'] = price($clientCompany->min_order);
            $jsonLance['pesoCaixa'] = $_produtoResult['weight'];

            $jsonLance['disponivelExibicao'] = rangeCaixas($caixas_disponivel);
            $jsonLance['kgDisponivel'] = $_produtoResult['volume'];
            $jsonLance['caixasDisponivel'] = $caixas_disponivel;
            $jsonLance['lancePreco'] = str_replace('.', ',', $_produtoResult['valor_kg']);
            $jsonLance['lanceCaixas'] = $_produtoResult['qtd_caixas'];
            $jsonLance['aceitaParcial'] = ($_produtoResult['aceite_parcial'] == 1) ? true : false;
            $jsonLance['dataVisaoLogistica'] = $_logistica[$client_id]['visao_logistica'];
            $jsonLance['prazoMaxPedido'] = $prazoMaxPedido;
            $jsonLance['validadeData'] = dataMySQLtoPT($_produtoResult['duracao_lance']);
            $jsonLance['validadeHoje'] = ($_produtoResult['duracao_lance'] == date("Y-m-d")) ? true : false;
            $jsonLance['precoAtualUn'] = unitPrice($_produtoResult['valor_atual'],$_produtoResult['weight_piece']);
            $jsonLance['precoUn'] = unitPrice($_produtoResult['valor_kg'],$_produtoResult['weight_piece']);
            $jsonLance['unidadeMedida'] = getUnit(
                                            $_produtoResult['valor_kg'],
                                            $_produtoResult['weight_piece'],
                                            $_produtoResult['unit'],
                                            $this->unidade_un
                                        );
            $jsonLance['padraoMedida'] = (new ProductService())->padraoMedida($_produtoResult);
            $jsonLance['gramatura'] = formatGrammage($_produtoResult['weight_piece']);
            $jsonLance['quantidadeUn'] = getUnitAmount($_produtoResult['total_kg'], $_produtoResult['weight_piece']);
            $jsonLance['amountLabel'] = formatAmount($jsonLance['quantidadeUn'], $product_query->weight_piece);

            return $jsonLance;
        }
    }

    /**
     * Update bid
     * @param client         $client         client
     * @param aceite_parcial $aceite_parcial aceite_parcial
     * @param duracao_lance  $duracao_lance  duracao_lance
     * @param lance_id       $lance_id       lance_id
     * @param qtd_caixas     $qtd_caixas     qtd_caixas
     * @param total_kg       $total_kg       total_kg
     * @param valor          $valor          valor
     * @param valor_kg       $valor_kg       valor_kg
     * @param array          $payment        payment
     * @return array
     **/
    public function updateBid(
        $client,
        $aceite_parcial,
        $duracao_lance,
        $lance_id,
        $qtd_caixas,
        $total_kg,
        $valor,
        $valor_kg,
        array $payment = []
    ) {
        $data_user = $client;
        $clientCompanyPaymentService = new ClientCompanyPaymentService();

        if ($data_user) {
            $client_id = $client->id;
            $aceite_parcial = ($aceite_parcial == true) ? 1 : 0;
            $bid = $this->bid->where('status_id', Status::type(Status::PENDING)->id)->find($lance_id);

            if (!$bid) {
                throw new MobileException('false', __('Mobile/bid.bids.update.fail'));
            }

            $company = $bid->company;

            $jsonLance = [];

            $clientCardId = array_get($payment, 'client_card_id');
            $months = array_get($payment, 'cc_months', 1);

            /**
            * CONFIRMA ALTERAÇÃO DO LANCE
            */
            $_update = [
                'qtd_caixas' => $qtd_caixas,
                'total_kg' => $total_kg,
                'valor_kg' => $valor_kg,
                'valor' => $valor,
                'status_id' => Status::type(Status::PENDING)->id,
                'aceite_parcial' => $aceite_parcial,
                'duracao_lance' => $duracao_lance,
                'original_box_amount' => $qtd_caixas,
                'client_company_payment_id' => array_get($payment, 'client_company_payment_id'),
                'client_card_id' => $clientCardId,
            ];

            if (empty($_update['client_company_payment_id'])) {
                $companyPaymentCredit = $clientCompanyPaymentService->getClientCompanyPaymentCredit($company, $client);

                if ($companyPaymentCredit) {
                    $_update['client_company_payment_id'] = $companyPaymentCredit->id;
                }
            }

            $clientCompanyPaymentService->validateClientCompanyPayment(
                $company,
                $client,
                $valor,
                $_update['client_company_payment_id']
            );

            if (!empty($clientCardId)) {
                $clientCardService = new ClientCardService();
                $_update['client_card_id'] = $clientCardId;
                $_update['cc_months'] = $months;

                $clientCard = ClientCard::find($clientCardId);
                $quotaValue = $clientCardService->calculateQuotaValue(
                    $clientCard,
                    $_update['valor'],
                    $_update['cc_months']
                );

                $clientCardService->setDefault($clientCard);

                $_update['tax_percent'] = $quotaValue['tax'];
            } else {
                $_update['cc_months'] = 1;
                $_update['tax_percent'] = 0;
            }

            $_lanceUpdate = $bid->update($_update);

            $bid = $bid->fresh();

            if ($_lanceUpdate) {
                $bidLogService = new BidLogService();

                $fefo = Fefo::where([
                    'codigo_produto' => $bid->cod_produto,
                    'cod_origem' => $bid->cod_origem,
                    'semanas' => $bid->weeks,
                    'date' => now()->toDateString()
                ])->first();

                if (isset($fefo->id)) {
                    $processFefoService = new ProcessedFefoService();
                    $processFefoService->processedFefo($fefo->id);
                }

                $bidLogParams = [
                    'client_id' => $client_id,
                    'box_amount' => $qtd_caixas,
                    'bid_id' => $lance_id
                ];

                $bidLogService->saveBidLog($bidLogParams);

                return [
                    'status' => true,
                    'title' => __('Mobile/bid.bids.confirm.title'),
                    'description' => __('Mobile/bid.bids.confirm.description')
                ];
            } else {
                throw new MobileException('false', 'Não foi possível atualizar o lance.');
            }
        }
    }

    /**
     * List client's bids grouped by status
     * @param int $client_id client_id
     * @return array
     */
    public function listBidsByClientId(string $client_id): array
    {
        $statusApproved = Status::type(Status::APPROVED)->id;
        $statusPending = Status::type(Status::PENDING)->id;
        $statusCanceled = Status::type(Status::CANCELED)->id;

        $approved = $this->getClientBidsByStatus($client_id, $statusApproved, $statusApproved);
        $pending = $this->getClientBidsByStatus($client_id, $statusPending, $statusPending);
        $canceled = $this->getClientBidsByStatus($client_id, $statusCanceled)
            ->merge($this->getClientBidsByStatus($client_id, $statusApproved, $statusCanceled));

        return [
            'pendentes' => $pending,
            'aprovados' => $approved,
            'cancelados' => $canceled,
        ];
    }

    /**
     * List bids from user
     * @param Client $client  client
     * @param array  $filters filters
     * @return Bid
     **/
    public function list(Client $client, array $filters)
    {
        $status = array_get($filters, 'status');
        $clientCardId = array_get($filters, 'client_card_id');

        $bids = $client->bids()
            ->selectRaw('lances.*')
            ->with(['order', 'order.orderProduct', 'product', 'company'])
            ->join('produtos', function ($query) {
                return $query
                    ->on('lances.cod_produto', '=', 'produtos.codigo')
                    ->whereNull('produtos.deleted_at');
            });

        switch ($status) {
            case 'pending':
                $bids->pending();
                break;
            case 'approved':
                $bids->approved();
                break;
            case 'canceled':
                $bids->canceled();
                break;
            default:
                $bids->pending();
                break;
        }

        if ($clientCardId) {
            $bids->where('lances.client_card_id', $clientCardId);
        }

        $bids = $bids->orderBy('lances.nao_lido', 'desc')->orderBy('lances.id', 'desc')->paginate(50);

        $bids->each(function ($bid) {
            $status = $this->getBidStatus($bid);

            $bid->status_description = $status['label'];
            $bid->status_message = $status['mobile'];
            $bid->status_class = $status['class'];

            return $bid;
        });

        Bid::whereIn('id', $bids->pluck('id'))->update([
            'nao_lido' =>  0
        ]);

        return $bids;
    }

    /**
     * Method to counter bids by status
     * @param Client $client client_object
     * @return array
     */
    public function counterList(Client $client)
    {
        $approveds = $client->bids()
            ->selectRaw("count(lances.id) as total, lances.status_id as bid_status")
            ->join('produtos', function ($query) {
                return $query
                    ->on('lances.cod_produto', '=', 'produtos.codigo')
                    ->whereNull('produtos.deleted_at');
            })
            ->groupBy('lances.status_id')->approved()->get();

        $pendings = $client->bids()
            ->selectRaw("count(lances.id) as total, lances.status_id as bid_status")
            ->join('produtos', function ($query) {
                return $query
                    ->on('lances.cod_produto', '=', 'produtos.codigo')
                    ->whereNull('produtos.deleted_at');
            })
           ->pending()->get();

        $canceleds = $client->bids()
            ->selectRaw("count(lances.id) as total, lances.status_id as bid_status")
            ->join('produtos', function ($query) {
                return $query
                    ->on('lances.cod_produto', '=', 'produtos.codigo')
                    ->whereNull('produtos.deleted_at');
            })
            ->canceled()->get();

        $counterBids = [];
        foreach ($approveds as $approved) {
            $counterBids['approved'] = [
                'status' => 'approved',
                'total' => $approved->total
            ];
        }

        foreach ($pendings as $pending) {
            $counterBids['pending'] = [
                'status' => 'pending',
                'total' => $pending->total
            ];
        }

        foreach ($canceleds as $canceled) {
            $counterBids['canceled'] = [
                'status' => 'canceled',
                'total' => $canceled->total
            ];
        }

       return $counterBids;
    }

    /**
     * Lista lances do cliente por status
     * @param int     $client_id    client_id
     * @param integer $bidStatus   [0, 1, 2]
     * @param boolean $orderStatus order_status
     * @return Illuminate\Support\Collection
     */
    public function getClientBidsByStatus($client_id, $bidStatus, $orderStatus = false)
    {
        $status = [
            'approved' => Status::type(Status::APPROVED)->id,
            'pending' => Status::type(Status::PENDING)->id,
            'canceled' => Status::type(Status::CANCELED)->id,
        ];

        $bids = Bid::selectRaw(
            'companies.id as companyId, companies.name as companyName,
            lances.*,
            produtos.nome,produtos.marca,produtos.foto'
        )
        ->join('produtos', function ($query) {
            return $query->on('lances.cod_produto', '=', 'produtos.codigo')
                ->whereNull('produtos.deleted_at');
        })
        ->join('companies', function ($query) {
            return $query->on('lances.company_id', '=', 'companies.id');
        })
        ->where('lances.id_cliente', $client_id)
        ->where(function ($query) use ($bidStatus, $orderStatus, $status) {
            $query->where('lances.status_id', $bidStatus);

            if($orderStatus === $status['pending']) {
                $query
                    ->orWhere([
                        ['pedidos.status_id', '=', $status['pending'] ],
                        ['lances.status_id', '=', $status['approved'] ],
                        ['pedidos.lance_id', '!=', null]
                    ]);
            }

            return $query;
        })
        ->orderBy('lances.nao_lido', 'desc')
        ->orderBy('lances.id', 'desc');

        if($orderStatus !== false) {

            if($orderStatus === $status['canceled']) {
                $bids->selectRaw('lances.*,produtos.nome,produtos.marca,produtos.foto,pedidos.status_id')
                    ->join('pedidos', function ($query) {
                        return $query->on('pedidos.lance_id', '=', 'lances.id')
                            ->whereNull('produtos.deleted_at');
                    })
                    ->where('pedidos.status_id', $orderStatus);
            }

            if($orderStatus === $status['pending']) {
                $bids->selectRaw(
                    'lances.*,
                    produtos.nome,produtos.marca,produtos.foto,
                    pedidos.status_id as pedido_status'
                )
                ->leftJoin('pedidos', function ($query) {
                    return $query->on('pedidos.lance_id', '=', 'lances.id')
                        ->whereNull('produtos.deleted_at');
                });
            }

            if($orderStatus === $status['approved']) {
                $bids->selectRaw(
                    'lances.*,
                    produtos.nome,produtos.marca,produtos.foto,
                    pedidos.data_embarque,pedidos.data_entrega,pedidos.status_id as pedido_status,
                    pedidos_produtos.total_kg as volume_faturado'
                )
                ->leftJoin('pedidos', function ($query) {
                    return $query->on('lances.id', '=', 'pedidos.lance_id');
                })
                ->leftJoin('pedidos_produtos', function ($query) {
                    return $query->on('pedidos.id', '=', 'pedidos_produtos.pedido_id')
                        ->whereNull('pedidos_produtos.deleted_at');
                })
                ->where('pedidos.status_id', $orderStatus)
                ->whereNotNull('pedidos.lance_id')
                ->whereNotNull('pedidos.data_embarque');
            }
        }

        $client_bids = collect([]);

        $bids->each(function ($bid) use (&$client_bids, $orderStatus, $status) {
            $validate = now()->startOfDay()->diffInDays($bid->duracao_lance, false);

            $bidStatus = $this->getBidStatus($bid);

            $client_bid = [
                'id_lance' => (string) $bid->id,
                'numero' => (string) $bid->id,
                'fechado' => ($bid->status_id == $status['approved'] && $orderStatus == $status['approved']) ?? true,
                'produto' => $bid->nome,
                'marca' => $bid->marca,
                'peso' => formata_moeda($bid->total_kg, false).'Kg',
                'caixas' => trans_choice('Mobile/order.boxes', $bid->qtd_caixas, ['box' => $bid->qtd_caixas]),
                'preco' => formata_moeda($bid->valor, false),
                'validade' => Carbon::parse($bid->duracao_lance)->format('d/m/Y'),
                'validadeDias' =>
                    ($bid->status_id == $status['approved'] && $orderStatus == $status['approved'])
                    ?
                        ''
                    :
                        (($validate >= 0)
                        ?
                            trans_choice('Mobile/order.validate', $validate, ['days' => $validate])
                        :
                            __('Mobile/order.expired')
                ),
                'novo' => (bool) $bid->nao_lido,
                'parcial' =>
                    ($bid->aceite_parcial)
                    ?
                        (
                            ($bid->status_id == $status['approved'])
                            ?
                                formata_moeda($bid->volume_faturado, false).'Kg'
                            : true
                        )
                        : false,
                'status' => $bidStatus['label'],
                'companyId' => $bid->companyId,
                'companyName' => $bid->companyName
            ];

            if($orderStatus == $status['pending']) {
                $client_bid['aceito'] = $bid->pedido_status == $status['pending'] ?? true;
            }

            $client_bids->push($client_bid);
        });

        $unread = $client_bids->where('novo', true);

        if($unread->count()) {
            Bid::whereIn('id', $unread->pluck('id_lance'))->update([
                'nao_lido' =>  0
            ]);
        }

        return $client_bids;
    }

    /**
     * Show client bid
     * @param bid_id  $bid_id  bid_id
     * @param Company $company company_id
     * @return array
     **/
    public function showClientBid($bid_id, Company $company): array
    {
        $removedByUser = RejectionMotive::type(RejectionMotive::REMOVED_BY_USER);
        $canceled = Status::type(Status::CANCELED);
        $pending = Status::type(Status::PENDING);

        $bid = Bid::selectRaw('
                lances.*,
                produtos.nome,
                produtos.marca,
                produtos.foto,
                produtos.peso_caixa,
                produtos.exibicao_app,
                produtos.gramatura,
                produtos.medida,
                client_companies.address as endereco,
                client_companies.city as municipio,
                client_companies.state as uf,
                pedidos.data_embarque,
                pedidos.data_entrega,
                pedidos.status_id as pedido_status
            ')
            ->join('produtos', function ($query) {
                return $query->on('lances.cod_produto', '=', 'produtos.codigo');
            })
            ->join('client_companies', function ($query) {
                return $query->on('client_companies.client_id', '=', 'lances.id_cliente');
            })
            ->leftJoin('pedidos', function ($query) {
                return $query->on('pedidos.lance_id', '=', 'lances.id');
            })
            ->where([
                ['lances.id', '=', $bid_id],
            ])
            ->where(function ($query) use ($removedByUser) {
                $query->where('lances.rejection_motives_id', '<>', $removedByUser->id)
                      ->orWhereNull('lances.rejection_motives_id');
            })
            ->with(['clientCompanyPayment.companyPayment.payment', 'clientCard'])
            ->withTrashed()
            ->first();

        if (!$bid) {
            throw new MobileException('false', __('Mobile/bid.bids.not_found'));
        }

        $validate = now()->startOfDay()->diffInDays($bid->duracao_lance, false);
        $validateLabel = (new CompanySettingService())->getPeriodView($bid->company, $bid->semanas);
        $product_validate = $bid->semanas . ' ' . $validateLabel;
        $status = $this->getBidStatus($bid);

        $response = [];
        $response['status_lance'] = $status['label'];
        $response['status_message'] = $status['mobile'];
        $response['status_class'] = $status['class'];
        $response['can_edit'] = ($bid->status_id == $pending->id) ? true : false;
        $response['lance_id'] = (string) $bid->id;
        $response['dataPedido'] = Carbon::parse($bid->data)->format('d/m/Y');
        $response['validade'] = $product_validate;
        $response['dataEmbarque'] = $bid->data_embarque ? Carbon::parse($bid->data_embarque)->format('d/m/Y') : false;
        $response['validadeLance'] = Carbon::parse($bid->duracao_lance)->format('d/m/Y');
        $response['valorTotal'] = formata_moeda($bid->price_with_tax);
        $response['endereco'] = $bid->endereco.', '.$bid->municipio.'/'.$bid->uf;

        $response['aceitaParcial'] = $bid->aceite_parcial === 1 ?? true;
        $response['validadeDias'] = " ";

        if ($bid->data_entrega){
            $response['dataEntrega'] = Carbon::parse($bid->data_entrega)->format('d/m/Y');
        } elseif (!$bid->data_entrega && $bid->status_id !== Status::type(Status::CANCELED)->id){
            $response['dataEntrega'] = __('Mobile/bid.bids.labels.delivery_date');
        } else {
            $response['dataEntrega'] = __('Mobile/bid.bids.labels.delivery_canceled');
        }

        $bidFefo = Fefo::where([
            'cod_origem' => $bid->origin_code,
            'codigo_produto' => $bid->product_code,
            'semanas' => $bid->weeks,
            'date' => $bid->date,
        ])->first();

        $percentualEconomia = $bidFefo !== null ?
            calcDiscount($bidFefo->max_price, $bid->kg_price) : false;

        $response['textoFinal'] = $percentualEconomia ? __('Mobile/bid.bids.labels.economy', [
            'economy_percentage' => $percentualEconomia
        ]) : null;

        if ($bid->clientCompanyPayment) {
            $payment = $bid->clientCompanyPayment->companyPayment->payment;

            if ($payment->isCompanyCredit) {
                $response['payment'] = __('Mobile/payment.companyCredit', ['company' => $bid->company->name]);
                $response['paymentIcon'] = $payment->image;
            }

            if ($payment->isCreditCard) {
                $quota = (new ClientCardService())->calculateQuotaValue(
                    $bid->clientCard,
                    $bid->price,
                    $bid->cc_months
                );

                $response['payment'] = $bid->clientCard->description;
                $response['paymentIcon'] = $bid->clientCard->image;
                $response['paymentQuota'] = $quota['message'];
            }
        }

        if (!$bid->pedido_status) {
            if ($validate >= 0) {
                $response['validadeDias'] = trans_choice('Mobile/order.validate', $validate, ['days' => $validate]);
            }
        }

        $response['produtos'] = collect();

        $amount = getUnitAmount($bid->total_kg, $bid->product->gramatura);

        $bid->gramatura = formatGrammage($bid->gramatura);

        $response['produtos'][$bid->cod_produto] = [
            'nome' => $bid->nome,
            'img' =>  env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$bid->company_id. '/'. $bid->foto,
            'empresa' => $bid->marca,
            'pesoCaixa' => volume($bid->peso_caixa).'Kg',
            'dimensaoCaixa' => $bid->exibicao_app,
            'validade' => $product_validate,
            'quantidadePeso' => volume($bid->total_kg).'Kg',
            'quantidadeCaixas' => trans_choice('Mobile/order.boxes', $bid->qtd_caixas, ['box' => $bid->qtd_caixas]),
            'volumeFaturado' => false,
            'valor' => formata_moeda($bid->valor_kg),
            'parcial' => false,
            'valorUn' => unitPrice($bid->valor_kg, $bid->gramatura),
            'unidadeMedida' => getUnit($bid->valor_kg, $bid->gramatura,$bid->medida, $this->unidade_un),
            'padraoMedida' => (new ProductService())->padraoMedida($bid),
            'gramatura' => $bid->gramatura,
            'quantidadeUn' => $amount,
            'amountLabel' => formatAmount($amount, $bid->gramatura),
        ];

        return $response;
    }

    /**
     * Remove bid
     * @param client   $client   client
     * @param lance_id $lance_id lance_id
     * @param company  $company  company
     * @return array
     **/
    public function removeBid($client, $lance_id, $company)
    {
        $data_user = $client;

        if ($data_user) {

            $client_id = $client->id;

            /**
             * UPDATE DO LANCE
             **/
            $_update = [
                'updated_at' => date("Y-m-d H:i:s"),
                'deleted_at' => date("Y-m-d H:i:s"),
                'status_id' => Status::type(Status::CANCELED)->id,
                'rejection_motives_id' => RejectionMotive::type(RejectionMotive::REMOVED_BY_USER)->id
            ];

            try {
                $_lanceUpdate = $this->bid->where([
                    'id_cliente' => $client_id,
                    'company_id' => $company->id,
                    'deleted_at' => null,
                ])->findOrFail($lance_id);

                $_lanceUpdate->update($_update);
                $_lanceUpdate->delete();

                $jsonLance = [];
                $jsonLance['status'] = true;
                $jsonLance['message'] = __('Mobile/bid.bids.remove.success');
                $response = array_merge($jsonLance, $this->listBidsByClientId($data_user->id, $company->id));
                return $response;

            } catch (\Exception $e) {
                throw new MobileException('false', __('Mobile/bid.bids_confirm.fail'));
            }
        }
    }

    /**
     * Get Bid Status
     * @param Bid $Bid Bid Instance
     * @return string
     **/
    public function getBidStatus(Bid $bid)
    {
        $bidStatus = $bid->status()->first();
        $canceled = Status::type(Status::CANCELED);

        if ($bid->order) {
            $orderStatus = $bid->order->status()->first();

            $deliveryDateFormated = ($bid->order->delivery_date) ?
                Carbon::parse($bid->order->delivery_date)->format('d/m/Y') : null;

            $response = __("status.order_bid.{$orderStatus->name}", [
                'date' => $deliveryDateFormated
            ]);

            if($bid->isPartiallyBilled()) {
                $response = __('status.order_bid.partially_billed', [
                    'bid' => $bid->id,
                    'product' => $bid->product->name,
                    'date' => $deliveryDateFormated
                ]);
            }

            if($bid->isBilledAbove()) {
                $response = __('status.order_bid.billed_above', [
                    'bid' => $bid->id,
                    'product' => $bid->product->name,
                    'date' => $deliveryDateFormated
                ]);
            }

            if ($orderStatus->id == $canceled->id) {

                $response = __("rejectionMotive.bid.{$bid->order->rejectionMotives->name}", [
                    'account_balance' => price_br($bid->price - $bid->order->client_account_balance),
                    'bid' => $bid->id,
                    'product_name' => $bid->product->name,
                    'date' => Carbon::parse($bid->data)->format('d/m/Y')
                ]);
            }
        } else {
            $response = __("status.bid.{$bidStatus->name}", [
                'company_name' => $bid->company->name
            ]);

            if ($bidStatus->id == $canceled->id) {
                $response = __("rejectionMotive.bid.{$bid->rejectionMotives->name}", [
                    'company_name' => $bid->company->name,
                    'bid' => $bid->id,
                    'product_name' => $bid->product->name,
                    'date' => Carbon::parse($bid->data)->format('d/m/Y')
                ]);
            }
        }

        return $response;
    }
}
