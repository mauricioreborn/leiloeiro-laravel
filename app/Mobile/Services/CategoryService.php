<?php

namespace App\Mobile\Services;

use App\Client\Client;
use App\Company\Company;
use App\Mobile\Exceptions\MobileException;
use App\Mobile\Services\HomeService;
use App\Mobile\Services\MobileService;
use Illuminate\Support\Collection;

class CategoryService extends MobileService
{
    /**
     * Method to get categories count
     *
     * @param Client $client  client object
     * @param int    $company company id
     * @return Collection
     */
    public function categoriesProductCount(Client $client, Company $company) : Collection
    {
        $categories = (new HomeService())->getHome($client, $company);

        if (!$categories->count()) {
            throw new MobileException(false);
        }

        return $categories;
    }
}