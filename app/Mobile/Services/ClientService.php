<?php
namespace App\Mobile\Services;

use App\Client\Client;
use App\Client\ClientConfirmation;
use App\Core\Entities\Status;
use App\Client\Notifications\MobileConfirmationNotification;
use App\Mobile\Exceptions\MobileException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

class ClientService
{
    protected $status;
    protected $clientConfirmation;

    /**
     * ClientService constructor.
     * @return void
     */
    public function __construct()
    {
        $this->status = new Status();
        $this->clientConfirmation = new ClientConfirmation();
    }

    /**
     * @param Client $client Client instance
     * @param string $type   Confirmation type
     * @param string $value  Confirmation value
     *
     * @return array
     */
    public function issueConfirmation(Client $client, string $type, string $value): array
    {
        $confirmationType = 'issue' . ucfirst(strtolower($type)) . 'Confirmation';
        if (!method_exists($this, $confirmationType)) {
            throw new MobileException(200, __('Mobile/clientConfirmation.errors.invalid_type'));
        }

        $this->$confirmationType($client, $type, $value);

        return [
            'title'   => __("Mobile/clientConfirmation.{$type}.title"),
            'message' => __("Mobile/clientConfirmation.{$type}.message"),
        ];
    }

    public function issueSmsConfirmation(Model $client, string $type, string $value)
    {
        $phoneNumber = validate_phone($value);

        if($client->confirm_sms === $this->status::type($this->status::AUTHORIZED)->id) {
            throw new MobileException(422, __("Mobile/clientConfirmation.{$type}.errors.already_has_validated"));
        }

        $pendingStatus = $this->status::type($this->status::PENDING);

        $client->confirm_sms = $pendingStatus->id;
        $client->save();

        $code = random_int(1000, 9999);
        $confirmation = $client->confirmations()->create([
            'type' => $type,
            'value' => $phoneNumber,
            'code' => $code,
        ]);

        Notification::send($confirmation,
            (new MobileConfirmationNotification)->delay(now()->addSeconds(1))
        );
    }

    /**
     * @param Client $client Client instance
     * @param string $type   Confirmation type
     * @param string $value  Confirmation value
     *
     * @return void
     *
     * @throws MobileException
     */
    public function issueMailConfirmation(Client $client, string $type, string $value)
    {
        if($client->confirm_mail === $this->status::type($this->status::AUTHORIZED)->id) {
            throw new MobileException(422, __("Mobile/clientConfirmation.{$type}.errors.already_has_validated"));
        }

        $pendingStatus = $this->status::type($this->status::PENDING);

        $client->confirm_email = $pendingStatus->id;
        $client->save();

        $code = mailConfirmationCode();

        $confirmation = $client->confirmations()->create([
            'type' => $type,
            'value' => $value,
            'code' => $code,
        ]);

        Notification::send($confirmation,
            (new MobileConfirmationNotification('mail'))->delay(now()->addSeconds(1))
        );
    }

    /**
     * @param Client $client Client instance
     * @param string $type   Confirmation type
     * @param string $code   Confirmation code
     * @return array
     */
    public function confirmSms(Client $client, string $type, string $code)
    {
        $confirmation = $this->checkClientConfirmation($type, $code);

        $authorizedStatus = $this->status::type($this->status::AUTHORIZED);

        $client->phone = $confirmation->value;
        $client->confirm_sms = $authorizedStatus->id;

        $client->save();

        $client->confirmations()->where('code', $code)->update(['confirmed_at' => now()]);
        $client->confirmations()->where('type', $type)->delete();

        return [
            'title'   => __("Mobile/clientConfirmation.sms.title"),
            'message' => __("Mobile/clientConfirmation.sms.confirmation"),
        ];
    }

    /**
     * @param string $code Confirmation code
     * @return array
     */
    public function confirmMail(string $code)
    {
        $type = ClientConfirmation::TYPES['mail'];

        $confirmation = $this->checkClientConfirmation($type, $code);

        if($confirmation->client_id === null) {
            throw new MobileException(422, __("Mobile/clientConfirmation.{$type}.errors.client_not_found"));
        }

        $confirmation->client->email = $confirmation->value;
        $confirmation->client->confirm_email = $this->status::type($this->status::AUTHORIZED)->id;
        $confirmation->client->save();

        $confirmation->where('code', $code)->update(['confirmed_at' => now()]);
        $this->clientConfirmation->where('type', $type)->where('client_id', $confirmation->client_id)->delete();

        return [
            'title'   => __("Mobile/clientConfirmation.mail.title"),
            'message' => __("Mobile/clientConfirmation.mail.confirmation"),
        ];
    }

    /**
     * @param string             $type               Confirmation type
     * @param string             $code               Confirmation code
     * @return ClientConfirmation object
     *
     * @throws MobileException
     */
    protected function checkClientConfirmation(string $type, string $code)
    {
        $confirmation = $this->clientConfirmation
            ->where('type', $type)
            ->where('code', $code)
            ->first();

        if ($confirmation === null) {
            throw new MobileException(422, __("Mobile/clientConfirmation.{$type}.errors.invalid_code"));
        }

        return $confirmation;
    }

    /**
     * @param Client $client Client
     * @param string $type   Type
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getClientConfirmation(Client $client, string $type)
    {
        return $client->confirmations()->where('type', $type)->latest('id')->first();
    }
}
