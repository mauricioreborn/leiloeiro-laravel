<?php
namespace App\Mobile\Services;

use App\Bid\Bid;
use App\Company\Company;
use App\Fefo\Fefo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

class FefoService extends MobileService
{
    protected $fefo;
    /**
     * Construct
     * @param Bid $bid bid
     * @return void
     **/
    public function __construct(Fefo $fefo)
    {
        parent::__construct();

        $this->fefo = $fefo;
    }

    /**
     * Method to get bids by fefo identifier
     * @param Fefo    $fefo    fefo object
     * @param Company $company company object
     * @param int     $limit   total data
     * @return mixed
     */
    public function getBidsByFefo(Fefo $fefo, Company $company, $limit = 12)
    {
        $bids = Bid::where('weeks', $fefo->weeks)
            ->where('product_code', $fefo->product_code)
            ->where('company_id', $fefo->company_id)
            ->where('origin_code', $fefo->leadtime_code)
            ->where('date', '<=', now()->toDateString())
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get();

        foreach ($bids as $bid) {
            $bid->unitPrice = unitPrice($bid->kg_price, $bid->product->weight_piece);
            $bid->volume =  volume($bid->weight) . 'Kg';
        }
        return $bids;
    }
}
