<?php

namespace App\Mobile\Services;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Company\Company;
use App\Core\Entities\Status;
use App\LeadTime\LeadTime;
use App\LeadTime\Services\LeadTimeService;
use App\Mobile\Exceptions\MobileException;
use App\Mobile\Services\ProductService;
use App\Order\Order;
use App\Order\OrderProduct;
use App\ProductTypology\Services\ProductTypologyService;
use App\Products\Product;
use App\Products\ProductTribute;
use App\Season\Services\SeasonService;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Predis\Connection\ConnectionException;

class HomeService extends MobileService
{
    protected $entity = '';
    protected $module = 'Mobile';
    protected $min_max_home;
    protected $typologyProductCampaign = [];
    const BLACKFRIDAYCAMPAIGN = ['BLACK WEEK', 'ESQUENTA BLACK WEEK', 'RESSACA BLACK WEEK'];

    /**
     * HomeService constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->min_max_home = false;
        $this->unidade_un = Config::get('mobile_constants.unidade_un');
    }


    public function getHome(Client $client, Company $company)
    {
        try {
            return Cache::remember("home.{$company->id}.{$client->id}", now()->addMinutes(30),
                function () use ($client, $company) {
                    return $this->getHomeProducts($client, $company);
                }
            );
        } catch(ConnectionException $exception) {
            return $this->getHomeProducts($client, $company);
        }
    }

    /**
     * Produtos da Home
     *
     * @param  Client $client Client Entity
     * @param  Company $company Company Entity
     *
     * @return Illuminate\Support\Collection
     */
    protected function getHomeProducts(Client $client, Company $company)
    {
        $client->clientCompany = $client->setClientCompany($company);

        $jsonHome = [];

        $enableProducts = (new ProductTypologyService())->getProducts($client, $company);

        /** ProductTribute **/

        $getAllProductTribute = (new ProductService())
            ->getAllTributeSubstitution($client, $client->clientCompany);
        
        /** seasons */

        $seasonService = new SeasonService();
        $seasons = $seasonService->getAllSeasonProducts($company->id);

        $totalSeasons = array_keys($seasons);
        $arraySeason = [];

        foreach ($totalSeasons as $seasonName) {
            $highLightProduct = $this->getHomeDestaques(
                $client,
                $company,
                $seasons[$seasonName],
                $enableProducts,
                $getAllProductTribute,
            );

            if ($highLightProduct) {
                $highLightProduct['nome'] = strtoupper($seasonName);
                $highLightProduct['custom_view'] = strtoupper($seasonName);
                $highLightProduct['custom_colors'] = $seasonService->getCustomColorBySeasonName(
                    $seasonName,
                    $company->id
                );

                if (in_array($seasonName, self::BLACKFRIDAYCAMPAIGN)) {
                    $jsonHome[strtoupper($seasonName)] =  $highLightProduct;
                    continue;
                }

                $arraySeason[strtoupper($seasonName)] = $highLightProduct;
            }
        }

        /**
         * Destaques
         */
        $highLightProduct = $this->getHomeDestaques($client, $company, false, $enableProducts, $getAllProductTribute);

        if ($highLightProduct) {
            $jsonHome['DESTAQUES'] = $highLightProduct;
        }

        $productsAlreadyBought = $this->getProductsAlreadyBought($company, $client);

        $bestSellerOfTypology = $this->getBestSellerTypologyProducts(
            $company,
            $client,
            $productsAlreadyBought,
            $getAllProductTribute
        );

        if (count($bestSellerOfTypology) > 0) {
            $jsonHome[__('Mobile/product.best_seller_typology')] = $bestSellerOfTypology;
        }

        $buyAgain = $this->getProductsToBuyAgain(
            $company,
            $client,
            $productsAlreadyBought,
            $getAllProductTribute
        );

        if (count($buyAgain) > 0) {
            $jsonHome[__('Mobile/product.buy_again')] = $buyAgain;
        }

        $suggested = $this->getSuggestedProducts(
            $company,
            $client,
            $productsAlreadyBought,
            $getAllProductTribute
        );

        if (count($suggested) > 0) {
            $jsonHome[__('Mobile/product.suggested_products')] = $suggested;
        }

        foreach ($arraySeason as $name => $values) {
            $jsonHome[$name] = $values;
        }

        $bestProductsByPrice = $this->getProductsWithRedAndYellowTrack(
            $client,
            $company,
            $getAllProductTribute
        );

        if ($bestProductsByPrice) {
            $bestProductsByPrice['custom_colors'] = [
                "card_bg" => "",
                "card_text" => "",
                "price_bg" => "1CA7B6",
                "price_text" => "fff",
                "price_highlight" => "fff"
            ];
            $jsonHome[__('Mobile/product.best_price_from_track')] = $bestProductsByPrice;
        }

        $categorias = $this->getCategorias($company->id);

        $categories = $categorias->pluck('category');

        $products = DB::table('produtos')
            ->selectRaw('
                fefo.*,
                fefo.id as id_fefo,
                leadtime.*,
                produtos.*,
                fefo.valor_atual AS menor_preco
            ')
            ->join('fefo', function ($query) {
                return $query->on('produtos.codigo', '=', 'fefo.codigo_produto')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('leadtime', function ($query) {
                return $query->on('leadtime.cod_origem', '=', 'fefo.cod_origem')
                    ->whereRaw('leadtime.company_id = fefo.company_id')
                    ->whereNull('leadtime.deleted_at');
            })
            ->where([
                'produtos.deleted_at' => null,
                'fefo.date' => now()->toDateString(),
                'leadtime.cnpj' => $client->cnpj,
                'leadtime.company_id' => $company->id
            ])
            ->whereIn('fefo.faixa_fefo', $client->clientCompany->tracks)
            ->whereIn('produtos.area', $categories)
            ->where('fefo.volume', '>', '0')
            ->whereRaw('fefo.volume >= produtos.peso_caixa')
            ->orderByRaw('fefo.valor_atual ASC');

        if (!is_bool($enableProducts)) {
            if (!$enableProducts->count()) {
                return collect($jsonHome);
            }

            $products = $products->where(function ($query) use ($enableProducts) {
                foreach ($enableProducts as $enableProduct) {
                    $query->orWhere(function ($query) use ($enableProduct) {
                        return $query->where('fefo.codigo_produto', $enableProduct->product_code)
                            ->whereIn('fefo.faixa_fefo', $enableProduct->tracks);
                    });
                }

                return $query;
            });
        }

        $products = $this->productsWithLowestPrice($products->get());

        foreach ($categorias as $categoria) {
            $produtosCategoria = [];
            $i = 0;
            $total_produtos = 0;
            $produtos_listados = [];

            foreach ($products as $product) {

                $hasTributeSubstitution = false;

                if ($categoria->category !== $product->area) {
                    continue;
                }

                $total_produtos += $product->qtd_produtos;

                if (in_array($product->codigo, $produtos_listados)) {
                    continue;
                }

                $produtos_listados[] = $product->codigo;

                /**
                 * Preço exibido na Home
                 * Busca valores mínimos e máximos nos pedidos ou carrega valor atual
                 */
                if ($this->min_max_home) {
                    $_minMaxResult = $this->precoMinMax(
                        $product->codigo,
                        $product->semanas,
                        $product->cod_origem
                    );

                    $_vmin = (empty($_minMaxResult['valor_min_pedido'])) ?
                        false : $_minMaxResult['valor_min_pedido'];

                    if ($_vmin) {
                        $_vmin = ($_vmin < $product->menor_preco) ? $_vmin : $product->menor_preco;
                    } else {
                        $_vmin = $product->menor_preco;
                    }

                    $_msgPreco = "Menor valor desta semana";
                } else {
                    $_vmin = $product->valor_atual;
                    $_msgPreco = "Valor atual";
                }

                $percentualEconomia = number_format((1 - ($_vmin / $product->valor_cheio)) * 100, 2);

                if($getAllProductTribute) {
                    $productTributes = $getAllProductTribute->pluck('code')->toArray();

                    if (in_array($product->codigo, $productTributes)) {
                        $hasTributeSubstitution = true;
                    }
                }

                $produtosCategoria[] = [
                    "codigo_produto" => (string) $product->id_fefo, // utiliza o ID do FEFO
                    "nome" => $product->nome,
                    "empresa" => $product->marca,
                    "img" => env('PRODUCT_IMAGES_BASE_URL') .
                        'souk-company/' . $company->id . '/' . $product->foto,
                    "precoMin" => $_vmin,
                    "precoAtual" => formata_moeda($_vmin),
                    "precoAtualUn" => unitPrice($_vmin, $product->gramatura),
                    "unidadeMedida" => getUnit(
                        $_vmin,
                        $product->gramatura,
                        $product->medida,
                        $this->unidade_un
                    ),
                    "padraoMedida" => (new ProductService())->padraoMedida(collect($product)),
                    "msgPreco" => $_msgPreco,
                    "indiceDesconto" => $percentualEconomia,
                    'label' => ($hasTributeSubstitution) ? __('Mobile/product.without_tribute') : false
                ];
            }

            if ($total_produtos === 0) {
                continue;
            }

            /**
             * ordenação dos produtos pelo indiceDesconto
             */
            $price = array();
            foreach ($produtosCategoria as $key => $row) {
                $price[$key] = $row['indiceDesconto'];
            }
            array_multisort($price, SORT_DESC, $produtosCategoria);

            if ($total_produtos > 0) {
                $category = sanitizeCategory($categoria->category);

                $jsonHome[$category] = [
                    'nome' => $category,
                    'total_produtos' => $total_produtos,
                    'produtos' => $produtosCategoria,
                    'custom_view' => false
                ];
            }
        }

        return collect($jsonHome);
    }

    /**
     * Destaques da Home
     *
     * @param  Client $client Client Entity
     * @param  Company $company Company ID
     * @param  array $season season ids
     * @param  array $enableProducts season ids
     *
     * @return array
     */
    protected function getHomeDestaques(
        Client $client,
        Company $company,
        $season = false,
        $enableProducts = false,
        $getAllProductTribute = false
    ) {
        $jsonDestaques = [];
        $produtosDestaque = [];

        $client->clientCompany = $client->setClientCompany($company);

        $products = DB::table('produtos')
            ->selectRaw('
                fefo.*,
                fefo.id as id_fefo,
                leadtime.*,
                produtos.*,
                fefo.valor_atual AS menor_preco
            ')
            ->join('fefo', function ($query) {
                return $query->on('produtos.codigo', '=', 'fefo.codigo_produto')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('leadtime', 'leadtime.cod_origem', 'fefo.cod_origem')
            ->where([
                'produtos.deleted_at' => null,
                'fefo.deleted_at' => null,
                'fefo.date' => date("Y-m-d"),
                'leadtime.deleted_at' => null,
                'leadtime.cnpj' => $client->cnpj
            ])
            ->whereIn('fefo.faixa_fefo', $client->clientCompany->tracks)
            ->where('fefo.volume', '>', '0')
            ->where('leadtime.company_id', $company->id)
            ->where('produtos.company_id', $company->id)
            ->where('fefo.company_id', $company->id)
            ->whereRaw('fefo.volume >= produtos.peso_caixa');


        if (!is_bool($enableProducts)) {
            if (!$enableProducts->count()) {
                return $jsonDestaques;
            }

            $products = $products->where(function ($query) use ($enableProducts) {
                foreach ($enableProducts as $enableProduct) {
                    $query->orWhere(function ($query) use ($enableProduct) {
                        return $query->where('fefo.codigo_produto', $enableProduct->product_code)
                            ->whereIn('fefo.faixa_fefo', $enableProduct->tracks);
                    });
                }

                return $query;
            });
        }

        if ($season == false) {
            $products = $products->where('fefo.destaque', 1);
        } else {
            $products = $products->whereIn('fefo.codigo_produto', $season);
        }

        $productsResult = $this->productsWithLowestPrice($products->get());

        if ($productsResult) {
            $i = 0;
            foreach ($productsResult as $_produto) {

                $hasTributeSubstitution = false;

                /**
                 * Preço exibido na Home
                 * Busca valores mínimos e máximos nos pedidos ou carrega valor atual
                 */
                if ($this->min_max_home) {
                    $_minMaxResult = $this->precoMinMax($_produto->codigo, $_produto->semanas, $_produto->cod_origem);

                    $_vmin = (empty($_minMaxResult['valor_min_pedido'])) ? false : $_minMaxResult['valor_min_pedido'];
                    if ($_vmin) {
                        $_vmin = ($_vmin < $_produto->menor_preco) ? $_vmin : $_produto->menor_preco;
                    } else {
                        $_vmin = $_produto->menor_preco;
                    }
                    $_msgPreco = "Menor valor essa semana";
                } else {
                    $_vmin = $_produto->valor_atual;
                    $_msgPreco = "Valor atual";
                }

                if($getAllProductTribute) {
                    $productTributes = $getAllProductTribute->pluck('code')->toArray();

                    if (in_array($_produto->codigo, $productTributes)) {
                        $hasTributeSubstitution = true;
                    }
                }

                $produtosDestaque[] = [
                    "codigo_produto" => (string) $_produto->id_fefo, // utiliza o ID do FEFO
                    "nome" => $_produto->nome,
                    "empresa" => $_produto->marca,
                    "img" => env('PRODUCT_IMAGES_BASE_URL') .
                        'souk-company/' . $company->id . '/' . $_produto->foto,
                    "precoMin" => $_vmin,
                    "precoAtual" => formata_moeda($_vmin),
                    "precoAtualUn" => unitPrice($_vmin, $_produto->gramatura),
                    "unidadeMedida" => getUnit($_vmin, $_produto->gramatura, $_produto->medida, $this->unidade_un),
                    "padraoMedida" => (new ProductService())->padraoMedida(collect($_produto)),
                    "msgPreco" => $_msgPreco,
                    'label' => ($hasTributeSubstitution) ? __('Mobile/product.without_tribute') : false
                ];
            }

            if (count($produtosDestaque) > 0) {
                $jsonDestaques = [
                    'nome' => 'DESTAQUES',
                    'total_produtos' => count($productsResult),
                    'produtos' => $produtosDestaque,
                    'custom_view' => 'destaques'
                ];
            }
        }
        return $jsonDestaques;
    }

    /**
     * Lista de areas/cagegorias
     *
     * @param  integer $company Company ID
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    protected function getCategorias($company = 1)
    {
        return Product::select('area')->where('company_id', $company)->groupBy('area')->orderBy('area', 'ASC')->get();
    }

    /**
     * Preço mínimo e máximo
     *
     * @param  integer $produto_id Product code
     * @param  integer $semanas week
     * @param  integer $origem Origin code
     *
     * @return array
     */
    protected function precoMinMax($produto_id, $semanas, $origem)
    {
        return $_minMaxQuery = DB::table('pedidos')
            ->selectRaw('
                MIN(pedidos_produtos.valor_kg) as valor_min_pedido, MAX(pedidos_produtos.valor_kg) as valor_max_pedido
            ')
            ->join('pedidos_produtos', 'pedidos_produtos.pedido_id', 'pedidos.id')
            ->where([
                'pedidos_produtos.deleted_at' => null,
                'pedidos_produtos.produto_id' => $produto_id,
                'pedidos_produtos.semanas' => $semanas,
                'pedidos_produtos.cod_origem' => $origem,
                'pedidos.status_id' => Status::type(Status::APPROVED)->id
            ])->get()->toArray();
    }

    /**
     * Logistica info
     *
     * @param  Client $client Client Entity
     * @param  company $company Company ID
     *
     * @return array
     */
    public function logisticaInfo(Client $client, $company)
    {
        $logistica = (new LeadTimeService())->getLogistica([$client->id], false, $company, true);

        $logistica = $logistica[$client->id];


        if (!isset($logistica['aceita_compra'])) {
            throw new MobileException(false, __('Mobile/home.logistics_not_found'));
        }
        $prazoEntrega = (
            $logistica['prazo_entrega'] > 1
        ) ?
            $logistica['prazo_entrega'] . ' dias'
            :
            $logistica['prazo_entrega'] . ' dia';

        $info_logistica['logistica'] = [
            'embarque_domingo' => $logistica['dias_embarque'][0],
            'embarque_segunda' => $logistica['dias_embarque'][1],
            'embarque_terca' => $logistica['dias_embarque'][2],
            'embarque_quarta' => $logistica['dias_embarque'][3],
            'embarque_quinta' => $logistica['dias_embarque'][4],
            'embarque_sexta' => $logistica['dias_embarque'][5],
            'embarque_sabado' => $logistica['dias_embarque'][6],
            'prazo_entrega' => $prazoEntrega,
            'visao_domingo' => $logistica['dias_visao_logistica'][0],
            'visao_segunda' => $logistica['dias_visao_logistica'][1],
            'visao_terca' => $logistica['dias_visao_logistica'][2],
            'visao_quarta' => $logistica['dias_visao_logistica'][3],
            'visao_quinta' => $logistica['dias_visao_logistica'][4],
            'visao_sexta' => $logistica['dias_visao_logistica'][5],
            'visao_sabado' => $logistica['dias_visao_logistica'][6],
            'info_adicional' => __('Mobile/home.logistics_info')
        ];

        return array_merge(['status' => true], $info_logistica);
    }


    /**
     * Method to get products with red and yellow track
     * @param Client $client client object
     * @param Company $company company object
     * @return array|bool
     */
    public function getProductsWithRedAndYellowTrack(Client $client, Company $company, $getAllProductTribute = false)
    {
        $jsonDestaques = [];
        $produtosDestaque = [];

        $client->clientCompany = $client->setClientCompany($company);

        $clientTrack = $client->clientCompany->tracks;

        $tracks = [];
        if (in_array('003', $clientTrack)) {
            $tracks[] = '003';
        }

        if (in_array('004', $clientTrack)) {
            $tracks[] = '004';
        }

        if (count($tracks) == 0) {
            return $jsonDestaques;
        }

        $products = DB::table('produtos')
            ->selectRaw('
                fefo.*,
                fefo.id as id_fefo,
                leadtime.*,
                produtos.*,
                MIN(fefo.valor_atual) AS menor_preco,
                COUNT(fefo.id) as qtd_produtos
            ')
            ->join('fefo', function ($query) {
                return $query->on('produtos.codigo', '=', 'fefo.codigo_produto')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('leadtime', 'leadtime.cod_origem', 'fefo.cod_origem')
            ->where([
                'produtos.deleted_at' => null,
                'fefo.deleted_at' => null,
                'fefo.date' => date("Y-m-d"),
                'leadtime.deleted_at' => null,
                'leadtime.cnpj' => $client->cnpj,
            ])
            ->whereIn('fefo.faixa_fefo', $tracks)
            ->where('fefo.volume', '>', '0')
            ->where('fefo.volume', '>', '0')
            ->where('leadtime.company_id', $company->id)
            ->where('produtos.company_id', $company->id)
            ->where('fefo.company_id', $company->id)
            ->whereRaw('fefo.volume >= produtos.peso_caixa');

        $products = $products->whereIn('fefo.faixa_fefo', $tracks)->groupBy(DB::Raw('fefo.codigo_produto'));

        $productsResult = $products->get()->toArray();

        if (!$productsResult) {
            return false;
        }

        if ($productsResult) {
            $i = 0;
            foreach ($productsResult as $_produto) {

                $hasTributeSubstitution = false;

                /**
                 * Preço exibido na Home
                 * Busca valores mínimos e máximos nos pedidos ou carrega valor atual
                 */
                if ($this->min_max_home) {
                    $_minMaxResult = $this->precoMinMax($_produto->codigo, $_produto->semanas, $_produto->cod_origem);

                    $_vmin = (empty($_minMaxResult['valor_min_pedido'])) ? false : $_minMaxResult['valor_min_pedido'];
                    if ($_vmin) {
                        $_vmin = ($_vmin < $_produto->menor_preco) ? $_vmin : $_produto->menor_preco;
                    } else {
                        $_vmin = $_produto->menor_preco;
                    }
                    $_msgPreco = "Menor valor essa semana";
                } else {
                    $_vmin = $_produto->valor_atual;
                    $_msgPreco = "Valor atual";
                }

                if($getAllProductTribute) {
                    $productTributes = $getAllProductTribute->pluck('code')->toArray();

                    if (in_array($_produto->codigo, $productTributes)) {
                        $hasTributeSubstitution = true;
                    }
                }

                $produtosDestaque[] = [
                    "codigo_produto" => (string) $_produto->id_fefo, // utiliza o ID do FEFO
                    "nome" => $_produto->nome,
                    "empresa" => $_produto->marca,
                    "img" => env('PRODUCT_IMAGES_BASE_URL') .
                        'souk-company/' . $company->id . '/' . $_produto->foto,
                    "precoMin" => $_vmin,
                    "precoAtual" => formata_moeda($_vmin),
                    "precoAtualUn" => unitPrice($_vmin, $_produto->gramatura),
                    "unidadeMedida" => getUnit($_vmin, $_produto->gramatura, $_produto->medida, $this->unidade_un),
                    "padraoMedida" => (new ProductService())->padraoMedida(collect($_produto)),
                    "msgPreco" => $_msgPreco,
                    'label' => ($hasTributeSubstitution) ? __('Mobile/product.without_tribute') : false
                ];
            }

            if (count($produtosDestaque) > 0) {
                $jsonDestaques = [
                    'nome' => __('Mobile/product.best_price_from_track'),
                    'total_produtos' => count($productsResult),
                    'produtos' => $produtosDestaque,
                    'custom_view' => __('Mobile/product.best_price_from_track')
                ];
            }
        }

        return $jsonDestaques;
    }

    /**
     * forgot home by origin code
     *
     * @param Company $company Company entity
     * @param string $originCode origin code
     *
     * @return void
     */
    public function forgetClientsHomeOriginCode(Company $company, string $originCode)
    {
        $clients = (new LeadTimeService())->getClientsOriginCode($company, $originCode);

        $this->forgetClientsHome($company, $clients);
    }

    /**
     * forgot home by client ids
     *
     * @param Company $company Company entity
     * @param array $clientIds Client ids
     *
     * @return void
     */
    public function forgetClientsHome(Company $company, array $clientIds)
    {
        foreach ($clientIds as $clientId) {
            $key = "home.{$company->id}.{$clientId}";

            if (Cache::store('redis')->has($key)) {
                Cache::store('redis')->forget($key);
            }
        }
    }

    /**
     * @param array $input Input
     * @return array
     */
    protected function productsWithLowestPrice($input): array
    {
        $products = [];
        $input = $input->map(function ($produto) {
            $produto->qtd_produtos = 1;
            return $produto;
        });


        foreach ($input as $prod) {
            if (!isset($products[$prod->codigo_produto])) {
                $products[$prod->codigo_produto] = $prod;
                continue;
            }

            $currProduct = $products[$prod->codigo_produto];

            if ($prod->faixa_fefo != $currProduct->faixa_fefo
                && $prod->codigo_produto == $currProduct->codigo_produto
                && $prod->cod_origem == $currProduct->cod_origem
                && $prod->valor_atual != $currProduct->valor_atual) {
                $products[$prod->codigo_produto]->qtd_produtos += 1;
            }

            if ($prod->faixa_fefo == $currProduct->faixa_fefo
                && $prod->codigo_produto == $currProduct->codigo_produto
                && $prod->cod_origem == $currProduct->cod_origem
                && $prod->valor_atual == $currProduct->valor_atual
                && $prod->volume > $currProduct->volume
            ) {
                $products[$prod->codigo_produto] = $prod;
                $products[$prod->codigo_produto]->qtd_produtos += $currProduct->qtd_produtos;
            }

        }

        return $products;
    }

    /**
     * Destaques da Home
     *
     * @param  Client $client Client Entity
     * @param  Company $company Company ID
     * @param  array $season season ids
     * @param  array $enableProducts season ids
     *
     * @return array
     */
    public function getProductsByCode(
        Client $client,
        Company $company,
        array $productsCode = [],
        $limit = 20,
        array $exceptProducts = [],
        $productsAlreadyBought = [],
        $getAllProductTribute = false
    ) {
        $jsonProducts = [];

        $client->clientCompany = $client->setClientCompany($company);

        $products = Product::selectRaw('
                fefo.*,
                fefo.id as id_fefo,
                leadtime.*,
                produtos.*,
                fefo.valor_atual AS menor_preco
            ')
            ->join('fefo', function ($query) {
                return $query->on('produtos.codigo', '=', 'fefo.codigo_produto')
                    ->whereRaw('produtos.company_id = fefo.company_id')
                    ->whereNull('fefo.deleted_at');
            })
            ->join('leadtime', 'leadtime.cod_origem', 'fefo.cod_origem')
            ->where([
                'produtos.deleted_at' => null,
                'fefo.deleted_at' => null,
                'fefo.date' => date("Y-m-d"),
                'leadtime.deleted_at' => null,
                'leadtime.cnpj' => $client->cnpj
            ])
            ->whereIn('fefo.faixa_fefo', $client->clientCompany->tracks)
            ->where('fefo.volume', '>', '0')
            ->where('leadtime.company_id', $company->id)
            ->where('produtos.company_id', $company->id)
            ->where('fefo.company_id', $company->id)
            ->whereRaw('fefo.volume >= produtos.peso_caixa')
            ->whereIn('produtos.codigo', $productsCode);

        if (count($exceptProducts) > 0) {
            $products = $products->whereNotIn('produtos.codigo', $exceptProducts);
        }

        if (count($productsAlreadyBought) > 0) {
            $products = $products->whereNotIn('produtos.codigo', $productsAlreadyBought);
        }

        $products = $products->get();
        $productsResult = $this->productsWithLowestPrice($products);
        $productCodes = [];
        if ($productsResult) {
            $i = 0;

            foreach ($productsResult as $product) {

                $hasTributeSubstitution = false;

                if ($i >= $limit) {
                    break;
                }
                $productCodes[] = $product->codigo;
                /**
                 * Preço exibido na Home
                 * Busca valores mínimos e máximos nos pedidos ou carrega valor atual
                 */
                if ($this->min_max_home) {
                    $_minMaxResult = $this->precoMinMax($product->codigo, $product->semanas, $product->cod_origem);

                    $_vmin = (empty($_minMaxResult['valor_min_pedido'])) ? false : $_minMaxResult['valor_min_pedido'];
                    if ($_vmin) {
                        $_vmin = ($_vmin < $product->menor_preco) ? $_vmin : $product->menor_preco;
                    } else {
                        $_vmin = $product->menor_preco;
                    }
                    $_msgPreco = "Menor valor essa semana";
                } else {
                    $_vmin = $product->valor_atual;
                    $_msgPreco = "Valor atual";
                }

                if($getAllProductTribute) {
                    $productTributes = $getAllProductTribute->pluck('code')->toArray();

                    if (in_array($product->codigo, $productTributes)) {
                        $hasTributeSubstitution = true;
                    }
                }

                $jsonProducts[] = [
                    "codigo_produto" => (string) $product->id_fefo, // utiliza o ID do FEFO
                    "nome" => $product->nome,
                    "empresa" => $product->marca,
                    "img" => env('PRODUCT_IMAGES_BASE_URL') .
                        'souk-company/' . $company->id . '/' . $product->foto,
                    "precoMin" => $_vmin,
                    "precoAtual" => formata_moeda($_vmin),
                    "precoAtualUn" => unitPrice($_vmin, $product->gramatura),
                    "unidadeMedida" => getUnit($_vmin, $product->gramatura, $product->medida, $this->unidade_un),
                    "padraoMedida" => (new ProductService())->padraoMedida($product),
                    "msgPreco" => $_msgPreco,
                    'label' => ($hasTributeSubstitution) ? __('Mobile/product.without_tribute') : false
                ];
                $i++;

            }
        }
        $jsonProducts['products'] = $jsonProducts;
        $jsonProducts['selectProductCodes'] = $productCodes;
        $jsonProducts['totalProductCodes'] = $products->count();
        return $jsonProducts;
    }

    /**
     * Method to get suggested products
     * @param Company    $company              company object
     * @param Client     $client               client object
     * @param Collection $productAlreadyBought product already bought
     * @return array
     */
    protected function getSuggestedProducts(
        Company $company,
        Client $client,
        Collection $productAlreadyBought,
        $getAllProductTribute = false
    ) {
        $productIds = Cache::get("campaign:home:suggested:{$company->id}:{$client->id}");

        if($productIds === null) {
            return [];
        }

        $products = $this->getProductsByCode(
            $client,
            $company,
            $productIds,
            20,
            $this->typologyProductCampaign,
            $productAlreadyBought->toArray(),
            $getAllProductTribute
        );

        if(count($products['products']) === 0) {
            return [];
        }

        return [
            'nome' => __('Mobile/product.suggested_products'),
            'total_produtos' => $products['totalProductCodes'],
            'produtos' => $products['products'],
            'custom_view' => strtolower(__('Mobile/product.suggested_products'))
        ];
    }

    /**
     * Method to get suggested products
     * @param Company $company company object
     * @param Client $client client object
     * @return array[
     */
    protected function getBestSellerTypologyProducts(
        Company $company,
        Client $client,
        $productAlreadyBought,
        $getAllProductTribute = false
    ) {
        $bestSellerTypology = [];

        $leadtime = LeadTime::where([
            'cnpj' => $client->cnpj,
            'company_id' => $company->id
        ])->first();

        if (isset($leadtime->id)) {
            $productIds = Cache::get(
                "campaign:home:typology:{$company->id}:{$leadtime->code}.{$client->clientCompany->typology}");

            if (!is_null($productIds)) {
                $products = $this->getProductsByCode(
                    $client,
                    $company,
                    $productIds, 20,
                    [],
                    $productAlreadyBought->toArray(),
                    $getAllProductTribute
                );

                $this->typologyProductCampaign = $products['selectProductCodes'];

                if (count($products['products']) > 0) {
                    $bestSellerTypology = [
                        'nome' => __('Mobile/product.best_seller_typology'),
                        'total_produtos' => $products['totalProductCodes'],
                        'produtos' => $products['products'],
                        'custom_view' => strtolower(__('Mobile/product.best_seller_typology'))
                    ];
                }
            }
        }
        return $bestSellerTypology;
    }

    /**
     * Method to get products already bought
     * @param Company    $company     company object
     * @param Client     $client      client object
     * @param Collection $productCode product already bought
     */
    public function getProductsToBuyAgain(
        Company $company,
        Client $client,
        Collection $productCode,
        $getAllProductTribute = false
    ) {
        $productsAlreadyBought = [];

        if (count($productCode) == 0) {
            return $productsAlreadyBought;
        }

        $products = $this->getProductsByCode(
            $client,
            $company,
            $productCode->toArray(),
            20,
            [],
            [],
            $getAllProductTribute
        );

        if (count($products['products']) > 0) {
            $productsAlreadyBought = [
                'nome' => __('Mobile/product.buy_again'),
                'total_produtos' => $products['totalProductCodes'],
                'produtos' => $products['products'],
                'custom_view' => strtolower(__('Mobile/product.buy_again'))
            ];
        }
        return $productsAlreadyBought;
    }

    /**
     * Method to get products already bought
     * @param Company $company company object
     * @param Client  $client  client object
     * @return mixed
     */
    public function getProductsAlreadyBought(Company $company, Client $client)
    {
        $productService = new ProductService();
        return $productService->getProductCodeAlreadyBought($client, $company);
    }
}
