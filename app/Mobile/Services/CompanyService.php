<?php

namespace App\Mobile\Services;

use App\Client\Client;
use App\Client\Services\ClientCompanyPaymentService;
use App\Company\Company;
use App\Mobile\Exceptions\MobileException;

class CompanyService extends MobileService
{
    protected $company;
    protected $client;

    /**
     * constructor
     * @param Company $company company
     * @return void
     **/
    public function __construct(Company $company)
    {
        parent::__construct();

        $this->company = $company;
        $this->client = new Client();
    }

    /**
     * @param Client $client client
     * @return array
     */
    public function listByClientId(Client $client)
    {
        $client = $this->getAuthorizedClientCompanies($client);

        $avaliableCompaniesId = $client->companies->pluck('id');

        $companies = $client->companies()->with('companySettings')->
            whereIn('companies.id', $avaliableCompaniesId)->paginate();

        return $companies->map(function ($company) use ($client) {
            $payments = (new ClientCompanyPaymentService())->getClientPayments($company, $client);

            $types = $payments->pluck('companyPayment.payment.type');

            $paymentLabel = '';

            foreach ($types as $type) {
                $label = __("Mobile/payment.cart.{$type}");

                $paymentLabel .= "<p>- {$label}</p>";
            }

            $company->cart_label = $this->getCartLabel($company, $paymentLabel);

            if(is_null($company->product_info)){
                $company->product_info = false;
            }

            return $company;
        });
    }

    public function getCartLabel(Company $company, string $payment = '')
    {
        $minOrder = $company->clientCompany->min_order;
        $billingDay = $company->companySettings->where('setting', 'billingDueDay')->first();
        $acceptsDevolution = $company->companySettings->where('setting', 'acceptsDevolution')->first();

        $langKey = $acceptsDevolution !== null &&
                   $acceptsDevolution->value === '1' ? 'cart_with_devolution' : 'cart';

        return __(
            "Mobile/company.{$langKey}",
            [
                'payment' => $payment,
                'billing_day' => $billingDay->value,
                'min_order' => $minOrder,
                'company_name' => $company->name
            ]
        );
    }
}
