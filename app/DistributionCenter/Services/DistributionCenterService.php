<?php
namespace App\DistributionCenter\Services;

use App\Company\Company;
use App\DistributionCenter\DistributionCenter;

class DistributionCenterService
{
    /**
     * get Distribution Center from company and state
     *
     * @param  Company $company Company
     * @param  string  $code    Code
     *
     * @return null|App\DistributionCenter\DistributionCenter
     */
    public function getUfCode(Company $company, string $code): ?DistributionCenter
    {
        return $company->distributionCenters
            ->where('code', $code)
            ->first();
    }
}
