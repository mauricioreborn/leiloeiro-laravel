<?php
namespace App\DistributionCenter\Http\Resources;

use App\Company\Http\Resources\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DistributionCenterResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'distribution_center',
            'id' => $this->id,
            'attributes' => [
                'code' => $this->code,
                'name' => $this->name,
                'state' => $this->state,
            ],
            'relationships' => [
                'company' => new CompanyResource($this->whenLoaded('company')),
            ],
        ];
    }
}
