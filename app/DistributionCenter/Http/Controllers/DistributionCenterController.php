<?php

namespace App\DistributionCenter\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\DistributionCenter\Services\DistributionCenterService;
use Illuminate\Http\Request;

class DistributionCenterController extends Controller
{
    protected $distributionCenterService;

    /**
     * DistributionCenterController constructor.
     *
     * @param DistributionCenterService $distributionCenterService Service class
     * @return null
     */
    public function __construct(DistributionCenterService $distributionCenterService)
    {
        $this->distributionCenterService = $distributionCenterService;
    }
}
