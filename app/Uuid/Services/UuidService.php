<?php

namespace App\Uuid\Services;

use App\Uuid\Uuid;

class UuidService
{
    /**
     * Method to update a Fcm Token
     * @param Client $clientId client id
     * @param string $uuid     devide id
     * @return bool
     */
    public function updateOrCreateUuid(string $clientId, string $uuid)
    {

        $uuIdModel = new Uuid();

        $uuidQuery = $uuIdModel->where([
            'client_id' => $clientId,
            'uuid' => $uuid
        ])->first();

        if (!isset($uuidQuery->id)) {
            $uuIdModel->create([
                'client_id' => $clientId,
                'uuid' => $uuid
            ]);
            return true;
        }

        $uuidQuery->update();
        return true;
    }
}