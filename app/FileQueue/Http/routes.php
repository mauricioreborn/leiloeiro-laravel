<?php

Route::group(['prefix' => 'v1'], function () {
        Route::group(['prefix' => 'file_queue','middleware' => ['validateJwt', 'sentry']], function () {
            Route::get('/', 'FileQueueController@list');
            Route::post('/', 'FileQueueController@store');
            Route::get('/type', 'FileQueueController@show');
        });

    Route::group(['prefix' => 'service'], function () {
        Route::group(['prefix' => 'file_queue', 'middleware' => ['ValidateServiceAccountJwt', 'sentry']], function () {
            Route::post('/', 'FileQueueController@storeByService');
        });
    });
});
