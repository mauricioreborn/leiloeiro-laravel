<?php

namespace App\FileQueue\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\FileQueue\Http\Resources\FileQueueResource;
use App\FileQueue\Http\Resources\FileQueueTypeCollection;
use App\FileQueue\Requests\FileQeueuesUploadByService;
use App\FileQueue\Requests\FileQeueuesUploadByServiceRequest;
use App\FileQueue\Requests\FileQueuesUploadRequest;
use App\FileQueue\Requests\GetFileQueuesByTypeRequest;
use App\FileQueue\Services\FileQueueService;
use Illuminate\Http\Request;

class FileQueueController extends Controller
{
    protected $fileQueueService;

    /**
     * FileQueueController constructor.
     * @param FileQueueService $fileQueueService Service class
     *
     * @return null
     */
    public function __construct(FileQueueService $fileQueueService)
    {
        $this->fileQueueService = $fileQueueService;
    }

    /**
     * Method to upload a file
     * @param Request $request request params
     * @return array
     */
    public function list(Request $request)
    {
        return new FileQueueTypeCollection($this->fileQueueService->getFileQueues($request->user()->company));
    }

    /**
     * Method to upload a file
     * @param FileQueuesUploadRequest $request request params
     * @return array
     */
    public function store(FileQueuesUploadRequest $request)
    {
        $company = $request->user()->company;
        $type = $request->get('type');

        $fileName = $this->fileQueueService->createFileName(
            $company,
            $type,
            $request->file('file')->getClientOriginalName()
        );

        $path = $this->fileQueueService->uploadFile(
            $request->file('file'),
            $fileName
        );

        //@TODO: Massa X condition, remove this when patternize commands by company
        if($company->id === 6) {
            $type = 'massax_'.$type;
        }

        $fileQueue = $this->fileQueueService->createFileQueue(
            $company,
            $request->user(),
            $path,
            $request->file('file')->getClientOriginalName(),
            $type,
            $request->file('file')->getSize()
        );

        return [
            'message' => __('filequeue.success'),
            'data' => new FileQueueResource($fileQueue)
        ];
    }

    /**
     * Method to upload a file
     * @param FileQueuesUploadRequest $request request params
     * @return array
     */
    public function storeByService(FileQeueuesUploadByServiceRequest $request)
    {
        $company = $this->fileQueueService->getCompanyByPath($request->key);
        $fileName = $this->fileQueueService->getFileName($request->key);
        $type = $this->fileQueueService->getFileTypeByFileName($fileName);

        $extension = array_last(explode('.', $request->key));

        $fileQueue = $this->fileQueueService->createFileQueue(
            $company,
            $request->user(),
            $request->key,
            "{$fileName}.{$extension}",
            $type,
            $request->size,
            's3_tirolez'
        );

        return [
            'message' => __('filequeue.success'),
            'data' => new FileQueueResource($fileQueue)
        ];
    }

    /**
     * Method to show file queues by type
     * @param GetFileQueuesByTypeRequest $request request params
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show(GetFileQueuesByTypeRequest $request)
    {
        $company = $request->user()->company;
        $type = $request->type;

        //@TODO: Massa X condition, remove this when patternize commands by company
        if($company->id === 6) {
            $type = 'massax_'.$type;
        }

        return FileQueueResource::collection($this->fileQueueService->getFileQueuesByType(
            $type,
            $company,
            $request->file_name
        ));
    }
}
