<?php
namespace App\FileQueue\Requests;

use App\FileQueue\FileQueue;
use Illuminate\Foundation\Http\FormRequest;

class GetFileQueuesByTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:'.implode(FileQueue::TYPES, ','),
        ];
    }
}