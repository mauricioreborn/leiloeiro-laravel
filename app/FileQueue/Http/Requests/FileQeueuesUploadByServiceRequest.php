<?php
namespace App\FileQueue\Requests;

use App\Company\Company;
use App\FileQueue\FileQueue;
use Illuminate\Foundation\Http\FormRequest;

class FileQeueuesUploadByServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => 'required',
            'size' => 'required'
        ];
    }
}
