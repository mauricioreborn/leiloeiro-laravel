<?php
namespace App\FileQueue\Http\Resources;

use App\Auth\Http\Resources\UserResource;
use App\Company\Http\Resources\CompanyResource;
use App\Core\Http\Resources\StatusResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class FileQueueResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        $date = Carbon::parse($this->updated_at)->format('d/m/Y');
        $time = Carbon::parse($this->updated_at)->format('h:m:s');
        /**
         * @todo remove bucket of resource
         */
        $bucket = $this->company_id  != 5 ? 's3' : 's3_tirolez';

        $type = $this->type;

        //@TODO: Massa X condition, remove this when patternize commands by company
        if($this->company_id === 6) {
            $type = str_replace('massax_', '', $this->type);
        }

        return [
            'type' => 'file_queue',
            'id' => $this->id,
            'attributes' => [
                'path' => $this->getPath($this->path, $bucket),
                'file_size' => $this->size,
                'type' => $type,
                'original_name' => $this->original_name,
                'updated_at' => __('Uploads/generic.last_update_decription', [
                    'user_name' => $this->user->name,
                    'date' => $date,
                    'time' => $time
                ]),
            ],
            'relationships' => [
                'status' => new StatusResource($this->whenLoaded('status')),
                'fileQueueLog' => new FileQueueLogResource($this->whenLoaded('fileQueueLog')),
                'user' => new UserResource($this->whenLoaded('user')),
                'company' => new CompanyResource($this->whenLoaded('company')),
            ],
        ];
    }
}
