<?php
namespace App\FileQueue\Http\Resources;

use App\Auth\Http\Resources\UserResource;
use App\Company\Http\Resources\CompanyResource;
use App\Core\Http\Resources\StatusResource;
use App\FileQueue\Http\Resources\FileQueueTypeResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class FileQueueTypeCollection extends ResourceCollection
{
    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'data' => $this->collection->map(function ($data, $key) use ($request) {
                return (new FileQueueTypeResource($data))->toArray($request);
            })
        ];
    }
}
