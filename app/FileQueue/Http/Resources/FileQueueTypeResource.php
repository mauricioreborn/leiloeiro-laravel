<?php
namespace App\FileQueue\Http\Resources;

use App\Auth\Http\Resources\UserResource;
use App\Company\Http\Resources\CompanyResource;
use App\Core\Http\Resources\StatusResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FileQueueTypeResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        $bucket = $this->company_id  != 5 ? 's3' : 's3_tirolez';

        $type = $this->type;

        //@TODO: Massa X condition, remove this when patternize commands by company
        if($this->company_id === 6) {
            $type = str_replace('massax_', '', $this->type);
        }

        $response = [
            'type' => 'file_queue_type',
            'id' => ($this->id) ?? null,
            'attributes' => [
                'type' => ($type) ?? null,
                'path' => $this->getPath($this->path, $bucket),
                'original_name' => ($this->original_name) ?? null,
                'custom_messages' => ($this->custom_messages) ?? []
            ]
        ];

        $response['relationships'] = [
            'status' => $this->whenLoaded('status') ?? new StatusResource($this->whenLoaded('status')),
            'user' => $this->whenLoaded('user') ?? new UserResource($this->whenLoaded('user')),
            'company' => $this->whenLoaded('company') ?? new CompanyResource($this->whenLoaded('company')),
        ];

        return $response;
    }
}
