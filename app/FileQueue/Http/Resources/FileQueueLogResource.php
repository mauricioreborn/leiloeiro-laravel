<?php
namespace App\FileQueue\Http\Resources;

use App\Auth\Http\Resources\UserResource;
use App\Company\Http\Resources\CompanyResource;
use App\Core\Http\Resources\StatusResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FileQueueLogResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'file_queue_log',
            'id' => $this->id,
            'attributes' => [
                'messages' => $this->messages,
            ],
            'relationships' => [],
        ];
    }
}
