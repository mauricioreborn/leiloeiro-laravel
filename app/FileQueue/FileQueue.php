<?php

namespace App\FileQueue;

use App\Auth\User;
use App\Company\Company;
use App\Core\Entities\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class FileQueue extends Model
{
    use SoftDeletes;

    const CLIENT = 'client';
    const LEADTIME = 'leadtime';
    const PRODUCT = 'product';
    const PRODUCT_PRICE = 'product_price';
    const STOCK = 'stock';
    const TIROLEZ_CLIENT = 'tirolez_client';
    const TIROLEZ_LEADTIME = 'tirolez_leadtime';
    const TIROLEZ_PRODUCT = 'tirolez_product';
    const TIROLEZ_PRODUCT_PRICE = 'tirolez_product_price';
    const TIROLEZ_STOCK = 'tirolez_stock';
    const TIROLEZ_BACK_ORDERS = 'tirolez_back_orders';

    const TYPES = [
        self::CLIENT,
        self::LEADTIME,
        self::PRODUCT,
        self::PRODUCT_PRICE,
        self::STOCK,
    ];
    
    const SERVICETYPES = [
        'produtos_tirolez' => self::TIROLEZ_PRODUCT,
        'tirolez_clientes_souk' => self::TIROLEZ_CLIENT,
        'estoque_tirolez' => self::TIROLEZ_STOCK,
        'leadtime_tirolez' => self::TIROLEZ_LEADTIME,
        'pedidos_tirolez_envio' => self::TIROLEZ_BACK_ORDERS,
    ];

    protected $fillable = [
        'path',
        'type',
        'size',
        'user_id',
        'company_id',
        'status_id',
        'original_name',
        'bucket',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Method to set storage on path
     *
     * @param string $path Relative path
     *
     * @return mixed
     */
    public function getPath($path, $bucket = 's3')
    {
        if ($path) {
            return Storage::disk($bucket)->temporaryUrl(
                $path, now()->addHour()
            );
        }
    }

    /**
     * Company relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * status relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * status relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * status relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fileQueueLog()
    {
        return $this->hasOne(FileQueueLog::class);
    }

    /**
     * verify if type is valid
     *
     * @param string $type Type
     *
     * @return bool
     */
    public function validType($type): bool
    {
        return in_array($type, self::TYPES);
    }
}
