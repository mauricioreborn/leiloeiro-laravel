<?php

namespace App\FileQueue\Services;

use App\Auth\User;
use App\Company\Company;
use App\Core\Entities\Status;
use App\FileQueue\FileQueue;

use App\Jobs\Integration\MassaX\StockJob as MassaXStockJob;
use App\Jobs\Integration\MassaX\ClientJob as MassaXClientJob;
use App\Jobs\Integration\MassaX\ProductImportJob as MassaXProductJob;
use App\Jobs\Integration\MassaX\LeadtimeJob as MassaXLeadtimeJob;
use App\Jobs\Integration\Tirolez\BackOrdersJob;
use App\Jobs\Integration\Tirolez\ClientJob;
use App\Jobs\Integration\Tirolez\LeadtimeJob;
use App\Jobs\Integration\Tirolez\ProductImportJob;
use App\Jobs\Integration\Tirolez\StockJob;
use App\Jobs\ProcessAccountBalanceJob;
use App\Jobs\ProcessLeadTimeJob;
use App\Jobs\ProductFileQueueJob;
use App\Jobs\ProductPriceFileQueueJob;
use App\Jobs\StockFileQueueJob;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use InvalidArgumentException;

class FileQueueService
{
    /**
     * Method to create a file name
     * @param Company $company company object
     * @param string  $type    file type
     * @return string
     */
    public function createFileName(Company $company, $type, $originaName)
    {
        $name = Str::random(32).'_'.$originaName;
        return "file_queues/company/$company->id/type/$type/$name";
    }

    /**
     * Method to upload a file
     * @param \File  $file     File object
     * @param string $fileName File name
     * @return mixed
     */
    public function uploadFile($file, $fileName)
    {
        return Storage::cloud()->putFileAs('', $file, $fileName);
    }

    /**
     * Method to create a file queues
     * @param Company $company      company entity
     * @param User    $user         User entity
     * @param string  $path         file path
     * @param string  $originalName original file name
     * @param string  $type         file type
     * @return mixed
     */
    public function createFileQueue(
        Company $company,
        User $user,
        string $path,
        string $originalName,
        string $type,
        string $size,
        string $bucket = null
    ) {
        return FileQueue::create([
            'company_id' => $company->id,
            'user_id' => $user->id,
            'path' => $path,
            'type' => $type,
            'size' => $size,
            'original_name' => $originalName,
            'bucket' => $bucket,
            'status_id' =>  Status::type(Status::PENDING)->id
        ]);
    }

    /**
     * Method to create a file queues
     *
     * @param FileQueue $fileQueue FileQueue entity
     * @param array     $messages  Messages
     *
     * @return Model
     */
    public function createLog(FileQueue $fileQueue, array $messages)
    {
        $fileQueueLog = $fileQueue->fileQueueLog()->first();

        if ($fileQueueLog) {
            $messages = array_values(array_unique(array_merge($fileQueueLog->messages, $messages)));
        }

        return $fileQueue->fileQueueLog()->updateOrCreate([], [
            'messages' => $messages
        ]);
    }

    /**
     * Transforms import failures into messages for queueLog
     *
     * @param FileQueue  $fileQueue FileQueue entity
     * @param Collection $failures  Import failures collection
     *
     * @return Model
     */
    public function createImportLog(FileQueue $fileQueue, Collection $failures)
    {
        $fileQueue->update([
            'status_id' => Status::type(Status::WITH_ERRORS)->id
        ]);

        $errors = [];

        foreach ($failures as $failure) {
            $messages = implode(',', $failure->errors());
            $errors[] = "Linha {$failure->row()}: {$messages}";
        }

        return $this->createLog($fileQueue, $errors);
    }

    /**
     * Method to get file queues by specifically type
     * @param string  $type    file type
     * @param Company $company company object
     * @return mixed
     */
    public function getFileQueuesByType($type, Company $company, $name = null)
    {
        $fileQueue = FileQueue::with('fileQueueLog')->where([
            'type' => $type,
            'company_id' => $company->id
        ])->orderBy('updated_at', 'desc')->with('status','user','company');

        $fileQueue = is_null($name) ? $fileQueue->get() : $fileQueue->where([
            'original_name' => $name
        ])->get();

        return $fileQueue;
    }

    /**
     * Method to download file
     * @param string $path         file path
     * @param string $originalName file name
     * @return string
     */
    public function downloadFile($path, $originalName)
    {
        $content_file = file_get_contents($path);
        Storage::disk('local')->put($originalName, $content_file);
        return storage_path('app/' . $originalName);
    }

    /**
     * Method to delete a local file
     * @param string $originalName original name
     * @return null
     */
    public function deleteFile($originalName)
    {
        Storage::delete($originalName);
    }

    /**
     * Method to dispatch stock files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchStock(FileQueue $fileQueue)
    {
        StockFileQueueJob::dispatch($fileQueue->id);
    }

    /**
     * Method to dispatch leadtime files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchLeadtime(FileQueue $fileQueue)
    {
        ProcessLeadTimeJob::dispatch($fileQueue->id);
    }

    /**
     * Method to dispatch client files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchClient(FileQueue $fileQueue)
    {
        ProcessAccountBalanceJob::dispatch($fileQueue->id);
    }

    /**
     * Method to dispatch product files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchProduct(FileQueue $fileQueue)
    {
        ProductFileQueueJob::dispatch($fileQueue->id);
    }

    /**
     * Method to dispatch product files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchProductPrice(FileQueue $fileQueue)
    {
        ProductPriceFileQueueJob::dispatch($fileQueue->id);
    }

    /**
     * Method to get file queues by type and show last
     *
     * @return collect
     */
    public function getFileQueues(Company $company)
    {
        $types = FileQueue::TYPES;

        foreach($types as $type){

            $fileQueue = FileQueue::where('type', 'like', "%{$type}")
            ->groupBy(['type', 'id'])
            ->orderBy('id', 'desc')
            ->with('user', 'company', 'status')
            ->where('company_id', $company->id)->first();

            if($fileQueue) {

                $fileQueue->custom_messages = [
                    'title' => __("Uploads/{$type}.title"),
                    'description' => __("Uploads/{$type}.description"),
                    'last_update_decription' => __("Uploads/generic.last_update_decription", [
                        'date' => Carbon::parse($fileQueue->updated_at)->format('d/m/Y'),
                        'time' => Carbon::parse($fileQueue->updated_at)->format('h:i'),
                        'user_name' => $fileQueue->user->name
                    ])
                ];
            } else {
                $fileQueue = new FileQueue();
                $fileQueue->type = $type;
                $fileQueue->custom_messages = [
                    'title' => __("Uploads/{$type}.title"),
                    'description' => __("Uploads/{$type}.description"),
                    'last_update_decription' => null
                ];
            }

            $response[$type] = $fileQueue;
        }

        return collect($response);
    }

    /**
     * Method to dispatch stock files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchTirolezStock(FileQueue $fileQueue)
    {
        StockJob::dispatch($fileQueue->id);
    }

    /**
     * Method to dispatch leadtime files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchTirolezLeadtime(FileQueue $fileQueue)
    {
        LeadtimeJob::dispatch($fileQueue->id);
    }

    /**
     * Method to dispatch client files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchTirolezClient(FileQueue $fileQueue)
    {
        ClientJob::dispatch($fileQueue->id);
    }

    /**
     * Method to dispatch product files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchTirolezProduct(FileQueue $fileQueue)
    {
        dispatch(new ProductImportJob($fileQueue->id));
    }

    /**
     * Method to dispatch product files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchTirolezProductPrice(FileQueue $fileQueue)
    {
        return true;
    }

    /**
     * Method to dispatch back order files
     * @param FileQueue $fileQueue file queue object
     * @return null
     */
    public function dispatchTirolezBackOrders(FileQueue $fileQueue)
    {
        BackOrdersJob::dispatch($fileQueue->id);
    }

    public function dispatchMassaXClient(FileQueue $fileQueue)
    {
        MassaXClientJob::dispatch($fileQueue->id);
    }

    public function dispatchMassaXStock(FileQueue $fileQueue)
    {
        MassaXStockJob::dispatch($fileQueue->id);
    }

    public function dispatchMassaXProduct(FileQueue $fileQueue)
    {
        MassaXProductJob::dispatch($fileQueue->id);
    }

    public function dispatchMassaXLeadtime(FileQueue $fileQueue)
    {
        MassaXLeadtimeJob::dispatch($fileQueue->id);
    }

    public function getCompanyByPath($path)
    {
        $filePath = explode('/', $path);
        $companyName = $filePath[1];

        $company =  Company::where(['name' => ucfirst($companyName)])->first();

        if (!isset($company->id)) {
            throw new InvalidArgumentException(__('filequeue.company_not_found', ['name' =>$companyName ]));
        }

        return $company;
    }

    public function getFileName($path)
    {
        $filePath = explode('/', $path);
        $filePath = array_last($filePath);
        return explode('.', $filePath)[0];
    }

    public function getFileTypeByFileName($fileName)
    {
        foreach (FileQueue::SERVICETYPES as $prefix => $type) {
            if (is_integer(strpos(strtolower($fileName), $prefix))) {
                return $type;
            }
        }

        throw new InvalidArgumentException(__('filequeue.type_not_found'));
    }
}
