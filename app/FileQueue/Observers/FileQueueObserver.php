<?php

namespace App\FileQueue\Observers;

use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;

class FileQueueObserver
{
    /**
     * Handle the FileQueue "created" event.
     *
     * @param FileQueue $fileQueue FileQueue entity
     *
     * @return void
     */
    public function created(FileQueue $fileQueue)
    {
        $type = str_replace('_', '', ucwords($fileQueue->type, '_'));

        $dispatch = "dispatch{$type}";

        (new FileQueueService())->$dispatch($fileQueue);
    }
}
