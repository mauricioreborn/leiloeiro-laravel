<?php

namespace App\FileQueue;

use App\Auth\User;
use App\Company\Company;
use App\Core\Entities\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class FileQueueLog extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'file_queue_id',
        'messages'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'messages' => 'array',
    ];

    /**
     * fileQueue relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fileQueue()
    {
        return $this->belongsTo(FileQueue::class);
    }
}
