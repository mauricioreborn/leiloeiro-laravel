<?php

namespace App\Jobs;

use App\Jobs\NotificationJob;
use App\Notification\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotificationBusinessHoursJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notifications = Notification::whereNull('send_at')
            ->whereIn('type', [
                Notification::rejectedBid
            ])
            ->get();

        foreach ($notifications as $notification) {
            NotificationJob::dispatch($notification->id);
        }
    }
}
