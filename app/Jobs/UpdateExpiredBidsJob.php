<?php

namespace App\Jobs;

use App\Bid\Services\BidService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class UpdateExpiredBidsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        return DB::transaction(function ($transaction) {
            try {
                (new BidService())->expire();
            } catch (Exception $exception) {
                if (app('env') != "testing") {
                    $transaction->rollback();
                }

                throw $exception;
            }
        });
    }
}
