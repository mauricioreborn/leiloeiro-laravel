<?php

namespace App\Jobs;

use App\Client\Services\TransactionalEmailsService;
use App\Company\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ProcessTransactionalEmailsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    protected $process = 'transactionalEmails';
    protected $transactionalEmailsService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->transactionalEmailsService = new TransactionalEmailsService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("{$this->process}: PROCESSO INICIADO!");

        $companies = Company::all();
        Log::info("{$this->process}: {$companies->count()} companies encontradas");

        foreach ($companies as $company) {
            Log::info("{$this->process}: Iniciando processo para {$company->name}");

            $orders = $this->transactionalEmailsService->getPendingOrders($company);

            if($orders->count() === 0) {
                Log::info("{$this->process}: Nenhum email pendente para {$company->name}");
                continue;
            }

            foreach($orders as $order) {
                if($order->bid) {
                    $this->transactionalEmailsService->sendBidEmail($order->bid);
                    continue;
                }

                $this->transactionalEmailsService->sendOrderEmail($order);
            }

            Log::info("{$this->process}: Processo finalizado para {$company->name}");
        }

        Log::info("{$this->process}: PROCESSO FINALIZADO!");
    }
}
