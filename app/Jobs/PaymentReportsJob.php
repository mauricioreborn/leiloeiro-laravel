<?php

namespace App\Jobs;

use App\Company\Company;
use App\Exports\PaymentReportsExport;
use App\Invoice\Services\InvoiceService;
use App\Order\Services\OrderService;
use App\Payment\MonthlyReport;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Order\Order;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Payment\MonthlyReports;
use Illuminate\Support\Str;
use App\Core\Helpers\FormatHelper;

class PaymentReportsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    protected $company_id;

    /**
     * Construct method to start job
     * PaymentReportsJob constructor.
     * @param integer $company_id company identifier
     */
    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->generatePaymentReports();
    }

    /**
     * Method to generate PaymentReports
     * @return bool
     */
    public function generatePaymentReports()
    {
        $start_at = Carbon::now()->subMonth(1)->startOfMonth();
        $finish_at = Carbon::now()->subMonth(1)->endOfMonth();
        $company = Company::find($this->company_id);

        $invoiceService = new InvoiceService();
        $invoices = $invoiceService->getInvoiceByRangeDateAndCompany($start_at, $finish_at, $company);

        if ($invoices->count() == 0) {
            Log::info(__('Payment/errors.invoice_not_found'));
            return false;
        }

        $paymentReportsArrays[] = [
            'id da fatura',
            'id do pedido',
            'codigo do pedido',
            'total',
            'id do lance',
        ];

        $date = Carbon::now();
        $companyName = Str::studly($company->name);
        $storeName = $companyName. '_payment_reports_' . $date->month . '_' . $date->year . '.xlsx';

        $orderService = new OrderService();
        $totalOrderProducts = $orderService->sumOrderProductsByOrderIds($invoices->pluck('order_id')->toArray());

        $monthlyReports = new MonthlyReport();

        $monthlyReports->where([
            'url' => $storeName,
            'company_id' => $company->id
        ])->updateOrCreate([
            'url' => $storeName,
            'month' => $date->month,
            'year'  => $date->year,
            'status_id' => 1,
            'total' => $totalOrderProducts,
            'company_id' => $company->id
        ]);

        foreach ($invoices as $invoice) {
            $order = Order::find($invoice->order_id);
            $paymentReportsArray['invoice_id'] = $invoice->id;
            $paymentReportsArray['order_id'] = $invoice->order_id;
            $paymentReportsArray['order_code'] = $order->codigo_pedido;
            $paymentReportsArray['total'] = FormatHelper::currencyFormat($invoiceService->getTotalByOrderId($order));

            $paymentReportsArray['bid_id'] = isset($order->bid_id) ? $order->bid_id : null;
            $paymentReportsArrays[] = $paymentReportsArray;
        }

        $paymentReportsArray = new PaymentReportsExport($paymentReportsArrays);

        Excel::store($paymentReportsArray, $storeName, 's3');
    }
}

