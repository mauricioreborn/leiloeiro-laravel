<?php

namespace App\Jobs;

use App\Notification\Notification;
use App\Notification\Services\NotificationService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    protected $notificationId;

    /**
     * Create a new job instance.
     *
     * @param integer $id Notification ID
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->notificationId = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return DB::transaction(function ($transaction) {
            try {
                $start = now()->hour(1)->minute(0)->second(0);
                $end = now()->hour(18)->minute(0)->second(0);

                if (!now()->between($start, $end, true)) {
                    Log::info('Skipping Notification id='. $this->notificationId .', reason: out of business hours');

                    return;
                }

                $capture = Notification::where('id', $this->notificationId)
                    ->whereNull('send_at')
                    ->update([
                        'send_at' => now()
                    ]);

                if ($capture) {
                    $notification = Notification::find($this->notificationId);

                    $message = (new NotificationService())->sendFCM(
                        $notification->client_id,
                        $notification->title,
                        $notification->message,
                        $notification->attributes
                    );

                    $notification->success = $message['success'];
                    $notification->failure = $message['failure'];
                    $notification->deleted_at = now();

                    $notification->save();

                    DB::commit();

                    return $notification;
                }

                Log::info('Skipping Notification id='. $this->notificationId .', reason: already sent');
            } catch (Exception $exception) {
                if (app('env') != "testing") {
                    $transaction->rollback();
                }

                throw $exception;
            }
        });
    }
}
