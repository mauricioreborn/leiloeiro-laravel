<?php

namespace App\Jobs;

use App\Bid\Services\BehaviourApprovalService;
use App\Company\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class BehaviourApprovalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $process = 'behaviourAproval';
    protected $queryDate;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startedAt = now();
        $behaviourService = new BehaviourApprovalService;

        // phpcs:ignore
        Log::info("{$this->process}: [INÍCIO] {$startedAt->format('d/m/Y H:i:s')}: Iniciando processo de aprovação por comportamento.");

        $this->queryDate = now()->toDateString();

        foreach(Company::where('name', '!=', 'Souk')->get() as $company) {
            $soukBot = $company->users()->where('name', 'soukBot')->first();

            if($soukBot === null) {
                // phpcs:ignore
                Log::info("{$this->process}: SoukBot user não encontrado para Company {$company->name}");
                continue;
            }

            // phpcs:ignore
            Log::info("{$this->process}: Iniciando processo de aprovação por comportamento para Company {$company->name}");

            $bidsFefo = $behaviourService->getBids($soukBot, $this->queryDate);

            foreach($bidsFefo as $fefoKey => $thresholds) {
                dispatch(new ProcessBidsByBehaviourJob($soukBot, $fefoKey, $thresholds, $this->queryDate))
                    ->delay(now()->addMinutes(random_int(0, 9)));
            }
        }

        $finishedAt = now();
        // phpcs:ignore
        Log::info("{$this->process}: {$finishedAt->format('d/m/Y H:i:s')}: Finalizando processo de aprovação por comportamento.");
        // phpcs:ignore
        Log::info("{$this->process}: [FIM] Tempo decorrido: {$finishedAt->diffInSeconds($startedAt)}");
    }
}
