<?php

namespace App\Jobs;

use App\Stock\Services\StockService;
use App\Stock\Stock;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessStockJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $stockId;

    /**
     * Create a new job instance.
     *
     * @param  integer $stockId Stock id
     *
     * @return void
     */
    public function __construct($stockId)
    {
        $this->stockId = $stockId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \App::setLocale('pt-BR');

        $stock = Stock::findOrFail($this->stockId);

        if ($stock) {
            (new StockService)->processFefo($stock, today());
        }
    }
}
