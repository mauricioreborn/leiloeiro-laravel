<?php

namespace App\Jobs;

use App\ClientsCompanies\Services\ClientsCompaniesService;
use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\Client\Imports\ClientImport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Excel;

class ProcessAccountBalanceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    protected $filQueueId;

    public $timeout = 900;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addSeconds($this->timeout);
    }

    /**
     * Create a new job instance.
     *
     * @param integer $id file queue id
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->filQueueId = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sleep(1);

        $fileQueue = FileQueue::findOrFail($this->filQueueId);
        $fileToDownload = (new FileQueueService())->downloadFile($fileQueue->getPath($fileQueue->path), $fileQueue->original_name);

        $import = new ClientImport($fileQueue->id);
        $import->import($fileToDownload, null, Excel::XLSX);
    }
}
