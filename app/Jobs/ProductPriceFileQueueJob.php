<?php

namespace App\Jobs;

use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\Products\Imports\ProductPriceImport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Excel;

class ProductPriceFileQueueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    protected $fileQueueId;

    /**
     * Create a new job instance.
     *
     * @param integer $id file queue id
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->fileQueueId = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \App::setLocale('pt-BR');

        sleep(1);

        $fileQueue = FileQueue::findOrFail($this->fileQueueId);

        $fileToDownload = (new FileQueueService())->downloadFile($fileQueue->getPath($fileQueue->path), $fileQueue->original_name);

        $import = new ProductPriceImport($fileQueue->id);
        $import->import($fileToDownload, null, Excel::CSV);

        if($import->failures()->count() > 0) {
            (new FileQueueService())->createImportLog($fileQueue, $import->failures());
        }
    }
}

