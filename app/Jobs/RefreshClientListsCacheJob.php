<?php

namespace App\Jobs;

use App\Boost\Services\ClientListCacheService;
use App\Boost\Services\ClientListService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RefreshClientListsCacheJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Iniciando Job '.$this->job->resolveName());

        $clientLists = (new ClientListService)->all();

        $updateThreshold = now()->subMinutes(30);
        $updatedClients = DB::table('client_companies')
            ->select('client_id', 'updated_at', 'company_id')
            ->where('updated_at', '>', $updateThreshold)
            ->get()
            ->groupBy('company_id')
            ->toArray();

        if(count($updatedClients) === 0) {
            return;
        }

        foreach($clientLists as $clientList) {
            if(!isset($updatedClients[$clientList->company_id])) {
                continue;
            }

            $cacheClientList = Cache::get("clientLists:{$clientList->id}:clients");

            if($cacheClientList !== null) {
                $cacheClientList = collect($cacheClientList)->pluck('id')->toArray();
                $clients = $updatedClients[$clientList->company_id];
                $clientsIds = collect($clients)->pluck('client_id')->toArray();

                if(count(array_intersect($cacheClientList, $clientsIds)) === 0) {
                    continue;
                }
            }

            (new ClientListCacheService($clientList))->cache();
        }

        Log::info('Finalizando Job '.$this->job->resolveName());
    }
}