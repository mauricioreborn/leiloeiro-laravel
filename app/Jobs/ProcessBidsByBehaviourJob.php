<?php

namespace App\Jobs;

use App\Auth\User;
use App\Bid\Services\BehaviourApprovalService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessBidsByBehaviourJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $soukBot;
    protected $fefoKey;
    protected $thresholds;
    protected $date;

    public function __construct(User $soukBot, $fefoKey, $thresholds, $date)
    {
        $this->soukBot = $soukBot;
        $this->fefoKey = $fefoKey;
        $this->thresholds = $thresholds;
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new BehaviourApprovalService())->processFefoBids(
            $this->soukBot,
            $this->fefoKey,
            $this->thresholds,
            $this->date
        );
    }
}
