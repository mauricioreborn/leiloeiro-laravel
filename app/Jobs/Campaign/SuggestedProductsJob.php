<?php

namespace App\Jobs\Campaign;

use App\Campaign\Services\CampaignHomeService;
use App\Company\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SuggestedProductsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $companies = Company::with('products')->get();

        foreach($companies as $company) {
            Log::info("[CAMPANHA][SUGESTOES_DO_DIA][{$company->id}] Iniciado execução do cache");
            (new CampaignHomeService())->createDailySuggestionCampaign($company);
        }


    }
}