<?php
namespace App\Jobs\Campaign;

use App\Campaign\Services\CampaignHomeService;
use App\Client\Client;
use App\Client\Services\ClientService;
use App\Company\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ProductsByTypologyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $timeout = 900;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addSeconds($this->timeout);
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $companies = Company::with('products')->get();

        foreach($companies as $company) {
            Log::info("[CAMPANHA][TIPOLOGIA][{$company->id}] Iniciado execução do cache");
            (new CampaignHomeService())->createDailyTypologyCampaign($company);
        }
    }
}
