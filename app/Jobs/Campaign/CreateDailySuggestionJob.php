<?php

namespace App\Jobs\Campaign;

use App\Campaign\Services\CampaignHomeService;
use App\Client\Client;
use App\Company\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class CreateDailySuggestionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    protected $companyId;

    protected $clientId;

    /**
     * CreateDailySuggestionJob constructor.
     * @param $companyId
     * @param $clientId
     */
    public function __construct($companyId, $clientId)
    {
        $this->companyId = $companyId;
        $this->clientId = $clientId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $company = Company::with('products')->find($this->companyId);
        $client = Client::find($this->clientId);

        if($company === null || $client === null) {
            return;
        }

        //phpcs:ignore
        Log::info("[CAMPANHA][SUGESTOES_DO_DIA][{$company->id}][{$client->id}] Iniciando processo para cliente");

        (new CampaignHomeService())->createDailySuggestionForClient($company, $client);
    }
}