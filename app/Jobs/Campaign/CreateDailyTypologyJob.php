<?php

namespace App\Jobs\Campaign;

use App\Campaign\Services\CampaignHomeService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateDailyTypologyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public    $tries = 3;
    protected $companyId;
    protected $typologyCode;
    protected $typology;
    protected $originCodes;

    /**
     * CreateDailyTypologyJob constructor.
     * @param $companyId
     * @param $typologyCode
     * @param $typology
     * @param $originCodes
     */
    public function __construct($companyId, $typologyCode, $typology, $originCodes)
    {
        $this->companyId = $companyId;
        $this->typologyCode = $typologyCode;
        $this->typology = $typology;
        $this->originCodes = $originCodes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new CampaignHomeService())->getTopProductsByOrigin(
            $this->companyId,
            $this->typologyCode,
            $this->typology,
            $this->originCodes
        );
    }
}