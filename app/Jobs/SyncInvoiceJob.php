<?php

namespace App\Jobs;

use App\Core\Entities\Status;
use App\Invoice\Invoice;
use App\Invoice\Services\InvoiceService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SyncInvoiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    protected $invoiceId;

    /**
     * Create a new job instance.
     *
     * @param integer $id Notification ID
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->invoiceId = $id;
    }

    /**
     * get invoice id
     *
     * @return $this->invoiceId
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info($this->getInvoiceId());

        (new InvoiceService())->syncInvoice($this->getInvoiceId());
    }
}