<?php

namespace App\Jobs;

use App\Core\Entities\Status;
use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\Stock\Imports\StockImport;
use App\Stock\Services\StockService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Validators\ValidationException;

class StockFileQueueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    protected $fileQueueId;

    public $timeout = 600;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addSeconds($this->timeout);
    }

    /**
     * Create a new job instance.
     *
     * @param integer $id file queue id
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->fileQueueId = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \App::setLocale('pt-BR');

        sleep(1);

        $fileQueue = FileQueue::findOrFail($this->fileQueueId);
        $fileToDownload = (new FileQueueService())->downloadFile($fileQueue->getPath($fileQueue->path), $fileQueue->original_name);

        $import = new StockImport($fileQueue->id);
        $import->import($fileToDownload, null, Excel::XLSX);
    }
}
