<?php

namespace App\Jobs\Integration\Tirolez;

use App\Order\Order;
use App\Order\Services\OrderService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $timeout = 900;

    protected $orderId;
    protected $orderService;

    /**
     * Create a new job instance.
     *
     * @param integer $id file queue id
     *
     * @return void
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;

        $this->orderService = (new OrderService());
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addSeconds($this->timeout);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sleep(1);

        Log::info("JOB sendo disparado para o pedido ".$this->orderId);

        $order = Order::with(['orderProduct', 'client', 'company'])->find($this->orderId);

        Log::info("pedido encontrado  ".$order->id);

        $isValid = $this->orderService->isValidMinOrder($order) &&
            $this->orderService->isValidAccountBallance($order) &&
            !$order->isCanceled();

        if ($isValid) {
            $this->orderService->exportToIntegration($order);
        } else {
            Log::info("pedido cancelado e não integrado:  ".$order->id);
        }
    }
}