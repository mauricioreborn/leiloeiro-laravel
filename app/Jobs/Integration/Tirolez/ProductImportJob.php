<?php

namespace App\Jobs\Integration\Tirolez;

use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\Products\Imports\Tirolez\ProductImport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Excel;

class ProductImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    protected $fileQueueId;

    public $timeout = 900;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addSeconds($this->timeout);
    }

    /**
     * Create a new job instance.
     *
     * @param integer $id file queue id
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->fileQueueId = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fileQueue = FileQueue::findOrFail($this->fileQueueId);
        $fileToDownload = (new FileQueueService)
            ->downloadFile($fileQueue->getPath($fileQueue->path, 's3_tirolez'), $fileQueue->original_name);

        (new ProductImport($fileQueue->id))->import($fileToDownload, null, Excel::CSV);
    }
}