<?php

namespace App\Jobs;

use App\LeadTime\Services\LeadTimeService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\FileQueue\FileQueue;

class ProcessLeadTimeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    protected $fileQueueId;

    /**
     * construct method
     * ProcessLeadTimeJob constructor.
     * @param integer $fileQueueId file queue object
     * @return void
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueueId = $fileQueueId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sleep(1);

        $fileQueue = FileQueue::findOrFail($this->fileQueueId);

        $leadtimeService = new LeadTimeService();
        $leadtimeService->processLeadtime($fileQueue);
    }
}
