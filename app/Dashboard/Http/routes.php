<?php

Route::group(['prefix' => 'v1/dashboard'], function () {
    Route::get('sales-growth', 'SalesGrowthController');
    Route::get('channel-group', 'ChannelGroupController');
    Route::get('purchases-by-status', 'PurchasesByStatusController');
    Route::get('top-clients', 'TopClientsController');
});
