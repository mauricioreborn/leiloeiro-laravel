<?php

namespace App\Dashboard\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Dashboard\Http\Requests\DashboardRequest;
use App\Dashboard\Services\ChannelGroupService;

class ChannelGroupController extends Controller
{
    public function __invoke(DashboardRequest $request)
    {
        $filters = $request->all();

        return (new ChannelGroupService($filters))
            ->applyFilters($request->user()->company);
    }
}