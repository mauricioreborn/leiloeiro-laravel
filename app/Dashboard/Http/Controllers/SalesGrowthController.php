<?php

namespace App\Dashboard\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Dashboard\Http\Requests\SalesGrowthRequest;
use App\Dashboard\Services\SalesGrowthStatsService;

class SalesGrowthController extends Controller
{
    public function __invoke(SalesGrowthRequest $request)
    {
        $filters = $request->all();

        return (new SalesGrowthStatsService($filters))
            ->applyFilters($request->user()->company);
    }
}
