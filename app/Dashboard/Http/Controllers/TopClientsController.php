<?php

namespace App\Dashboard\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Dashboard\Http\Requests\TopClientsRequest;
use App\Dashboard\Services\TopClientsService;

class TopClientsController extends Controller
{
    public function __invoke(TopClientsRequest $request)
    {
        $filters = $request->all();

        return (new TopClientsService($filters))
            ->applyFilters($request->user()->company);
    }
}
