<?php

namespace App\Dashboard\Http\Requests;

use App\Dashboard\Services\DashboardService;
use Illuminate\Foundation\Http\FormRequest;

class DashboardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'month' => ['required', 'string', function ($attribute, $value, $fail) {
                $months = explode(',', $value);

                foreach($months as $month) {
                    if(!array_key_exists($month, DashboardService::ACCEPTED_MONTHS)) {
                        $fail(__('validation.dashboard.months', ['param' => $month]));
                    }
                }
            }],
            'year' => 'required|string',
            'category' => 'required|string',
            'group_by' => 'required|string||in:volume,revenue,absolute,absolute_client',
        ];
    }
}
