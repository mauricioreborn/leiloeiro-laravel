<?php

namespace App\Dashboard\Http\Requests;

use App\Dashboard\Services\TopClientsService;
use Illuminate\Validation\Rule;

class TopClientsRequest extends DashboardRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'order_by' => ['required', 'string', Rule::in(TopClientsService::ACCEPTED_ORDER_FIELDS)]
        ]);
    }
}