<?php
namespace App\Dashboard\Services;

use App\Company\Company;
use App\Core\Entities\Status;
use App\Dashboard\Contracts\DashboardInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class SalesGrowthStatsService extends DashboardService implements DashboardInterface
{
    public const ACCEPTED_TYPES = ['bids', 'orders', 'all'];
    protected $response = [];

    public function applyFilters(Company $company): array
    {
        $query = $this->baseQuery($company)
            ->filterByDateRange()
            ->filterByCategory()
            ->filterByType()
            ->getQuery()
            ->groupBy('pedido_id');

        if(isset($this->filters['group_by']) && $this->filters['group_by'] === 'absolute_client') {
            $query->groupBy('pedidos.id_cliente');
        }
        $this->response = [
            'recurrent' => [],
            'first_purchase' => [],
            'price' => [],
        ];

        $this->fillResponse($query->get());
        $this->fillResponsePrice();

        return array_sort($this->response);
    }

    protected function baseQuery(Company $company)
    {
        $this->query = DB::table('pedidos_produtos')
            ->selectRaw('
                COALESCE(COUNT(pedidos.id)) as absolute,
                COALESCE(COUNT(DISTINCT(pedidos.id_cliente))) as absolute_client,
                COALESCE(COUNT(pedidos_produtos.id), 0) as total_products,
                COALESCE(SUM(pedidos_produtos.valor), 0) as revenue,
                COALESCE(SUM(pedidos_produtos.total_kg), 0) as volume,
                pedido_id,
                pedidos.lance_id,
                pedidos.status_id,
                pedidos.data,
                DAY(pedidos.data) as day,
                MONTH(pedidos.data) as month,
                YEAR(pedidos.data) as year,
                WEEKDAY(pedidos.data) as week,
                pedidos.first_purchase')
            ->join('pedidos', 'pedidos_produtos.pedido_id', '=', 'pedidos.id')
            ->where('pedidos.company_id', $company->id)
            ->where('pedidos.status_id', Status::type(Status::APPROVED)->id)
            ->where('pedidos_produtos.status_id', Status::type(Status::APPROVED)->id);

        return $this;
    }

    protected function fillResponse(Collection $orders): void
    {
        foreach ($orders as $order) {
            $order = collect($order)->toArray();
            $statsKey = $order['first_purchase'] ? 'first_purchase' : 'recurrent';

            if(!isset($this->response[$statsKey][$order[$this->filters['view_mode']]])) {
                $this->response[$statsKey][$order[$this->filters['view_mode']]] = 0;
            }

            $this->response[$statsKey][$order[$this->filters['view_mode']]] += (float) $order[$this->filters['group_by']];

            if(!isset($stats['price'][$order[$this->filters['view_mode']]])) {
                $this->response['price'][$order[$this->filters['view_mode']]]['volume'] = 0;
                $this->response['price'][$order[$this->filters['view_mode']]]['revenue'] = 0;
            }
            $this->response['price'][$order[$this->filters['view_mode']]]['volume'] += (float) $order['volume'];
            $this->response['price'][$order[$this->filters['view_mode']]]['revenue'] += (float) $order['revenue'];
        }
    }

    protected function fillResponsePrice(): void
    {
        foreach($this->response['price'] as $key => $averagePrice) {
            if($this->response['price'][$key]['revenue'] === 0 || $this->response['price'][$key]['volume'] === 0) {
                $this->response['price'][$key]['average_price'] = 0;
                continue;
            }

            $this->response['price'][$key]['average_price'] = $this->response['price'][$key]['revenue'] /
                                                            $this->response['price'][$key]['volume'];
        }
    }

    protected function filterByType(): self
    {
        if(!isset($this->filters['type']) || $this->filters['type'] === 'all') {
            return $this;
        }

        if(!in_array($this->filters['type'], self::ACCEPTED_TYPES, true)) {
            throw new \Exception('Tipo não aceito');
        }

        if($this->filters['type'] === 'bids') {
            $this->query->whereNotNull('pedidos.lance_id');
        }

        if($this->filters['type'] === 'orders') {
            $this->query->whereNull('pedidos.lance_id');
        }

        return $this;
    }
}