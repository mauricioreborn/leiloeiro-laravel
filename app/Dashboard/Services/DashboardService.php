<?php
namespace App\Dashboard\Services;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

abstract class DashboardService
{
    public const ACCEPTED_MONTHS = [
        'jan' => 1,
        'fev' => 2,
        'mar' => 3,
        'abr' => 4,
        'mai' => 5,
        'jun' => 6,
        'jul' => 7,
        'ago' => 8,
        'set' => 9,
        'out' => 10,
        'nov' => 11,
        'dez' => 12,
    ];

    protected $filters;
    protected $query;

    /**
     * SalesGrowthService constructor.
     * @param array $filters
     */
    public function __construct(array $filters) {
        $this->filters = $filters;
    }

    public function getQuery(): Builder {
        return $this->query;
    }

    public function filterByDateRange(): self
    {
        if(!isset($this->filters['year'], $this->filters['month'])) {
            return $this;
        }

        $years = explode(',', $this->filters['year']);
        $months = explode(',', $this->filters['month']);

        $years = array_sort($years);

        $months = array_map(function($month) {
            return self::ACCEPTED_MONTHS[strtolower($month)];
        }, $months);

        sort($months);

        if(count($years) === 1) {
            $this->query->whereYear('data', '=', $years[0]);
        } else {
            $this->query->whereIn(DB::raw('YEAR(data)'), $years)
                ->whereYear('data', '<=', end($years));
        }

        if(count($months) === 1) {
            $this->query->whereMonth('data', '=', $months[0]);
        } else {
            $this->query->whereIn(DB::raw('MONTH(data)'), $months)
                ->whereYear('data', '<=', end($years));
        }

        return $this;
    }

    public function filterByCategory(): self
    {
        if(!isset($this->filters['category']) || $this->filters['category'] === 'all') {
            return $this;
        }

        $categories = explode(',', $this->filters['category']);

        $this->query
            ->addSelect(['produtos.area'])
            ->join('produtos', 'pedidos_produtos.produto_id', '=', 'produtos.codigo')
            ->whereIn('area', $categories);

        return $this;
    }
}