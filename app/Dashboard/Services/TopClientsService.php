<?php

namespace App\Dashboard\Services;

use App\Client\Client;
use App\Company\Company;
use App\Core\Entities\Status;
use App\Dashboard\Contracts\DashboardInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TopClientsService extends DashboardService implements DashboardInterface
{
    public const ACCEPTED_ORDER_FIELDS = ['volume', 'revenue', 'average_price', 'average_ticket', 'total'];

    protected $response = [];

    public function applyFilters(Company $company): array
    {
        $rows = $this->baseQuery($company)
            ->filterByDateRange()
            ->filterByCategory()
            ->getQuery()
            ->groupBy('pedido_id')
            ->get();

        $this->fillResponse($rows->groupBy('id_cliente'));

        return $this->response;
    }

    protected function baseQuery(Company $company)
    {
        $this->query = DB::table('pedidos_produtos')
            ->selectRaw('
                COALESCE(COUNT(pedidos_produtos.id), 0) as total_products,
                COALESCE(SUM(pedidos_produtos.valor), 0) as revenue,
                COALESCE(SUM(pedidos_produtos.total_kg), 0) as volume,
                pedido_id,
                pedidos.lance_id,
                pedidos.status_id,
                pedidos.data,
                pedidos.id_cliente,
                DAY(pedidos.data) as day,
                MONTH(pedidos.data) as month,
                YEAR(pedidos.data) as year,
                WEEK(pedidos.data) as week')
            ->join('pedidos', 'pedidos_produtos.pedido_id', '=', 'pedidos.id')
            ->where('pedidos.company_id', $company->id)
            ->where('pedidos.status_id', Status::type(Status::APPROVED)->id)
            ->where('pedidos_produtos.status_id', Status::type(Status::APPROVED)->id);

        return $this;
    }

    protected function fillResponse(Collection $collection): void
    {
        if(!isset($this->filters['order_by'])
           || !in_array($this->filters['order_by'], self::ACCEPTED_ORDER_FIELDS,true)) {
            throw new \Exception('Filtro de ordenação inválido.');
        }

        $parsed = [];

        foreach ($collection as $clientId => $pedidos) {
            $clientOrders = [
                'total' => 0,
                'revenue' => 0,
                'volume' => 0,
            ];

            foreach($pedidos as $pedido) {
                $pedido = collect($pedido)->toArray();
                ++$clientOrders['total'];
                $clientOrders['revenue'] += $pedido['revenue'];
                $clientOrders['volume'] += $pedido['volume'];
            }

            $clientOrders['average_price'] = $clientOrders['revenue'] / $clientOrders['volume'];
            $clientOrders['average_ticket'] = $clientOrders['revenue'] / $clientOrders['total'];

            $parsed[$clientId] = $clientOrders;
        }

        $topClients = collect($parsed)->sortByDesc($this->filters['order_by'])->take(5);

        foreach($topClients as $clientId => $stats) {
            $client = Client::find($clientId);

            $stats['client'] = $client->name;
            $stats['cnpj'] = $client->cnpj;
            $this->response[] = $stats;
        }
    }
}