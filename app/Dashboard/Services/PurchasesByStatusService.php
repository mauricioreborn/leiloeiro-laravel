<?php

namespace App\Dashboard\Services;

use App\Company\Company;
use App\Core\Entities\Status;
use App\Dashboard\Contracts\DashboardInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PurchasesByStatusService extends DashboardService implements DashboardInterface
{
    public const ACCEPTED_TYPES = ['bids', 'orders'];

    protected $response = [];

    public function applyFilters(Company $company): array
    {
        $rows = $this->baseQuery($company)
            ->filterByDateRange()
            ->filterByCategory()
            ->getQuery()
            ->groupBy('status_id')
            ->get();

        $this->fillResponse($rows);

        return $this->response;
    }

    protected function baseQuery(Company $company)
    {
        if(!in_array($this->filters['type'], self::ACCEPTED_TYPES, true)) {
            throw new \Exception('Tipo não aceito');
        }

        if($this->filters['type'] === 'bids') {
            $this->query = $this->getQueryForBids($company);
        }

        if($this->filters['type'] === 'orders') {
            $this->query = $this->getQueryForOrders($company);
        }

        return $this;
    }

    public function getQueryForBids(Company $company)
    {
        return DB::table('lances')
            ->selectRaw('
                lances.id, 
                COALESCE(COUNT(lances.id), 0) as absolute,
                COALESCE(COUNT(DISTINCT(id_cliente))) as absolute_client,
                COALESCE(SUM(valor), 0) as revenue,
                COALESCE(SUM(total_kg), 0) as volume,
                lances.status_id, 
                lances.rejection_motives_id, 
                lances.data, 
                DAY(data) as day, 
                MONTH(data) as month, 
                YEAR(data) as year, 
                WEEK(data) as week
            ')
            ->where('lances.company_id', $company->id)
            ->where('status_id', '!=', Status::type(Status::PENDING)->id);
    }

    public function getQueryForOrders(Company $company)
    {
        return DB::table('pedidos_produtos')
            ->selectRaw('
                COALESCE(COUNT(pedidos_produtos.id), 0) as absolute,
                COALESCE(COUNT(DISTINCT(pedidos.id_cliente))) as absolute_client,
                COALESCE(SUM(pedidos_produtos.valor), 0) as revenue,
                COALESCE(SUM(pedidos_produtos.total_kg), 0) as volume,
                pedido_id,
                pedidos.lance_id,
                pedidos.status_id,
                pedidos.rejection_motives_id, 
                pedidos.data,
                DAY(pedidos.data) as day,
                MONTH(pedidos.data) as month,
                YEAR(pedidos.data) as year,
                WEEK(pedidos.data) as week')
            ->join('pedidos', 'pedidos_produtos.pedido_id', '=', 'pedidos.id')
            ->where('pedidos.company_id', $company->id)
            ->where('pedidos.status_id', '!=', Status::type(Status::PENDING)->id)
            ->whereNotNull('pedidos.id');
    }

    protected function fillResponse(Collection $rows): void
    {
        $this->response['total'] = [
            'status' => 'Total',
            'total' => 0,
        ];

        foreach ($rows as $row) {
            $row = collect($row)->toArray();
            $status = Status::find($row['status_id'])->name;

            $this->response[$status] = [
                'status' => __("status.{$status}"),
                'total' => (int) $row[$this->filters['group_by']],
            ];

            $this->response['total']['total'] += (int) $row[$this->filters['group_by']];
        }
    }

    public function filterByCategory(): parent
    {
        if(!in_array($this->filters['type'], self::ACCEPTED_TYPES, true)) {
            return $this;
        }

        if(!isset($this->filters['category']) || $this->filters['category'] === 'all') {
            return $this;
        }

        $categories = explode(',', $this->filters['category']);

        if($this->filters['type'] === 'orders') {
            $this->query
                ->addSelect(['produtos.area'])
                ->join('produtos', 'pedidos_produtos.produto_id', '=', 'produtos.codigo')
                ->whereIn('area', $categories);
        }

        if($this->filters['type'] === 'bids') {
            $this->query
                ->addSelect(['produtos.area'])
                ->join('produtos', 'lances.cod_produto', '=', 'produtos.codigo')
                ->whereIn('area', $categories);
        }

        return $this;
    }
}