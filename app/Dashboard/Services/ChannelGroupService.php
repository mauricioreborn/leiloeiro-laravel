<?php

namespace App\Dashboard\Services;

use App\Company\Company;
use App\Core\Entities\Status;
use App\Dashboard\Contracts\DashboardInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ChannelGroupService extends DashboardService implements DashboardInterface
{
    protected $response = [];

    public function applyFilters(Company $company): array
    {
        $orders = $this->baseQuery($company)
            ->filterByDateRange()
            ->filterByCategory()
            ->getQuery()
            ->groupBy('pedido_id')
            ->get();

        $this->fillResponse($orders);
        $response = $this->removeIndices();

        return array_sort($response);
    }

    protected function baseQuery(Company $company)
    {
        $this->query = DB::table('pedidos_produtos')
            ->selectRaw('
                COALESCE(COUNT(pedidos_produtos.id), 0) as total_products,
                COALESCE(COUNT(DISTINCT(pedidos.id_cliente))) as absolute_client,
                COALESCE(SUM(pedidos_produtos.valor), 0) as revenue,
                COALESCE(SUM(pedidos_produtos.total_kg), 0) as volume,
                pedido_id,
                pedidos.lance_id,
                pedidos.status_id,
                pedidos.data,
                client_companies.typology,
                DAY(pedidos.data) as day,
                MONTH(pedidos.data) as month,
                YEAR(pedidos.data) as year,
                WEEK(pedidos.data) as week')
            ->join('pedidos', 'pedidos_produtos.pedido_id', '=', 'pedidos.id')
            ->join('client_companies', 'pedidos.id_cliente', '=', 'client_companies.client_id')
            ->where('pedidos.company_id', $company->id)
            ->where('client_companies.company_id', $company->id)
            ->where('pedidos.status_id', Status::type(Status::APPROVED)->id)
            ->where('pedidos_produtos.status_id', Status::type(Status::APPROVED)->id);

        return $this;
    }

    protected function fillResponse(Collection $orders): void
    {
        foreach ($orders as $order) {
            $order = collect($order)->toArray();

            $typology = $order['typology'];
            $order['absolute'] = 1;


            if (isset( $this->response[$typology])) {
                $this->response[$typology]['total'] += (float) $order[$this->filters['group_by']];
                continue;
            }
            $this->response[$typology]['typology'] = $typology;
            $this->response[$typology]['total'] =  (float) $order[$this->filters['group_by']];
        }
    }

    protected  function removeIndices()
    {
        $channelGroupArray =[];

        if ($this->response !== null) {
            foreach ($this->response as $typology => $data) {
                $channelGroupArray['data'][] = $data;
            }
        }
        return $channelGroupArray;
    }
}