<?php
namespace App\Dashboard\Contracts;

use App\Company\Company;

interface DashboardInterface
{
    public function applyFilters(Company $company): array;

    public function filterByDateRange();

    public function filterByCategory();
}