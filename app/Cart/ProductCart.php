<?php


namespace App\Cart;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class ProductCart extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    protected $table = 'carrinho_produtos';

    public $timestamps = false;

    protected $fillable = [
        'carrinho_id',
        'cod_produto',
        'cod_origem',
        'semanas',
        'qtd_caixas',
        'total_kg',
        'valor_kg',
        'valor',
        'created_at',
        'status',
        'deleted_at'
    ];

    protected $hidden = [

    ];

    protected $appends = [

    ];

    public $maps = [

    ];

    protected $casts = [
        'total_kg' => 'float',
        'valor_kg' => 'float',
        'valor' => 'float'
    ];
}