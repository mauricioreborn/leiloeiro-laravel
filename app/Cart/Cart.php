<?php

namespace App\Cart;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class Cart extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    public $timestamps = false;

    protected $table = 'carrinho';

    protected $fillable = [
        'id_cliente',
        'data',
        'created_at',
        'status',
        'company_id'
    ];

    protected $hidden = [

    ];

    protected $appends = [

    ];

    public $maps = [

    ];

}