<?php

namespace App\NotificationOrigin\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\LeadTime\Http\Resources\LeadTimeResource;

class NotificationOriginResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'notification_origin',
            'id' => $this->id,
            'attributes' => [
                'origin_code' => $this->origin_code,
                'subject' => $this->subject,
                'message' => $this->message,
                'total_sent' => $this->total_sent,
                'day_period' => $this->day_period,
                'processed_at' => $this->processed_at,
                'scheduled_at' => $this->scheduled_at,
                'created_at' => $this->created_at->toDateTimeString(),
                'updated_at' => $this->updated_at->toDateTimeString(),
            ],
            'relationships' => [
                'leadtime' => new LeadTimeResource($this->whenLoaded('leadtime')),
            ],
        ];
    }
}
