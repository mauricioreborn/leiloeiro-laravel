<?php

namespace App\NotificationOrigin\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

use App\Core\Http\Controllers\Controller;

use App\NotificationOrigin\Services\NotificationOriginService;
use App\NotificationOrigin\Http\Requests\NotificationOriginRequest;
use App\NotificationOrigin\Http\Resources\NotificationOriginResource;

use App\NotificationOrigin\NotificationOrigin;
use App\Company\Company;

/**
 * Class NotificationOrigin
 * @package App\NotificationOrigin\Http\Controllers
 */
class NotificationOriginController extends Controller
{
    /**
    * @var NotificationOriginService
    **/
    protected $notificationOriginService;

    /**
     * constructor.
     * @param NotificationOriginService $notificationOriginService NotificationOrigin Service
     */
    public function __construct(NotificationOriginService $notificationOriginService)
    {
        $this->notificationOriginService = $notificationOriginService;
    }

    /**
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     */
    public function list(Request $request) : AnonymousResourceCollection
    {
        $this->authorize('canAccess', NotificationOrigin::class);

        $collection = $this->notificationOriginService->search($request->all(), auth()->user()->company);

        return NotificationOriginResource::collection($collection);
    }

    /**
     * Store
     * @param NotificationOriginRequest $request request
     * @return NotificationOriginResource
     **/
    public function store(NotificationOriginRequest $request) : AnonymousResourceCollection
    {
        $this->authorize('canAccess', NotificationOrigin::class);

        $input = $request->except(['processed_at', 'total_sent']);

        return NotificationOriginResource::collection(
            $this->notificationOriginService->createOriginCodes($input, auth()->user()->company)
        );
    }

    /**
     * @param NotificationOriginRequest $request            Request object
     * @param NotificationOrigin        $notificationOrigin NotificationOrigin
     * @return NotificationOriginResource
     */
    public function update(NotificationOriginRequest $request, NotificationOrigin $notificationOrigin
    ) : NotificationOriginResource {

        $this->authorize('userAccess', $notificationOrigin);

        $input = $request->only(['subject', 'message']);

        $this->notificationOriginService->update($notificationOrigin, $request->all());

        return new NotificationOriginResource($this->notificationOriginService->find($notificationOrigin->id));
    }

    /**
     * @param integer $notificationOrigin NotificationOriginService id
     * @return JsonResponse
     */
    public function destroy(NotificationOrigin $notificationOrigin): JsonResponse
    {
        $this->authorize('userAccess', $notificationOrigin);

        $this->notificationOriginService->delete($notificationOrigin);

        return new JsonResponse([], 204);
    }
}
