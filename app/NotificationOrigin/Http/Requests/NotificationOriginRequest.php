<?php

namespace App\NotificationOrigin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationOriginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Method to cast confirm password
     *
     * @return Request
     */
    protected function prepareForValidation()
    {
        return $this->replace(array_merge(
            $this->all(),
            [
                'company_id' => auth()->user()->company_id
            ]
        ));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'origin_code' => 'required|array|min:1',
                    'origin_code.*' => 'integer|exists:leadtime,cod_origem',
                    'subject' => 'nullable|string|max:172',
                    'message' => 'required|string|max:172',
                    'day_period' => 'required|string|in:morning,afternoon,night',
                    'scheduled_at' => 'required|date_format:Y-m-d|after_or_equal:today',
                ];
            case 'PUT':
                return [
                    'subject' => 'nullable|string|max:172',
                    'message' => 'required|string|max:172',
                ];
        }
    }
}
