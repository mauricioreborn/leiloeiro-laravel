<?php

namespace App\NotificationOrigin\Http\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Auth\User;
use App\NotificationOrigin\NotificationOrigin;

class NotificationOriginActionPolicy
{
    use HandlesAuthorization;

    /**
     * Authorize user to save or show
     * @param User $user User object
     * @return boolean
     **/
    public function canAccess(User $user)
    {
        return $user->root;
    }

    /**
     * Authorize user to update or delete
     * @param User               $user               User object
     * @param NotificationOrigin $notificationOrigin NotificationOrigin object
     * @return boolean
     **/
    public function userAccess(User $user, NotificationOrigin $notificationOrigin)
    {
        return (bool) $user->root && ($notificationOrigin->company_id === $user->company_id);
    }
}
