<?php

namespace App\NotificationOrigin\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Id implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $value Search param
     * @return Builder
     */
    public function run($query, $value)
    {
        return $query->where('id', $value);
    }
}
