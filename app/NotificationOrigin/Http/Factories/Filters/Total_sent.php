<?php

namespace App\NotificationOrigin\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Total_sent implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $value Search param
     * @return Builder
     */
    public function run($query, $value)
    {
        return $query->where('total_sent', "{$value}");
    }
}
