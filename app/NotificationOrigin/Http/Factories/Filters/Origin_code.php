<?php

namespace App\NotificationOrigin\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use App\LeadTime\LeadTime;

class Origin_code implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $value Search param
     * @return Builder
     */
    public function run($query, $value)
    {
        $ids = LeadTime::where('name', 'LIKE', "%{$value}%")
            ->pluck('code')
            ->toArray();

        return $query->whereIn('origin_code', $ids);
    }
}
