<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'notification_origins'], function () {
        Route::get('/', 'NotificationOriginController@list');
        Route::post('/', 'NotificationOriginController@store');
        Route::put('/{notification_origin}', 'NotificationOriginController@update');
        Route::delete('/{notification_origin}', 'NotificationOriginController@destroy');
    });
});
