<?php

namespace App\NotificationOrigin\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

use App\NotificationOrigin\NotificationOrigin;
use App\NotificationOrigin\Http\Policies\NotificationOriginActionPolicy;

class NotificationOriginServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\NotificationOrigin\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Gate::policy(NotificationOrigin::class, NotificationOriginActionPolicy::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware(['api', 'validateJwt', 'sentry'])
            ->namespace($this->namespace)
            ->group(base_path('app/NotificationOrigin/Http/routes.php'));
    }
}
