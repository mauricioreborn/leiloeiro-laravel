<?php

namespace App\NotificationOrigin\Services;

use App\Company\Company;
use App\Core\Classes\CrudService;
use App\Core\Exceptions\ActionNotAllowedException;
use App\NotificationOrigin\NotificationOrigin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;

class NotificationOriginService extends CrudService
{
    protected $entity = NotificationOrigin::class;

    protected $module = 'NotificationOrigin';

    /**
     * Create Notification Origin
     *
     * @param string  $subject     Subject
     * @param string  $message     Message
     * @param string  $originCode  Origin code
     * @param string  $scheduledAt Date of execution
     * @param string  $dayPeriod   Period: morning,afternoon,night
     * @param Company $company     Company entity
     *
     * @return Model
     */
    public function createModel($subject, $message, $originCode, $scheduledAt, $dayPeriod, Company $company): Model
    {
        // $hasDuplicate = NotificationOrigin::where('origin_code', $originCode)
        //     ->where('scheduled_at', $scheduledAt)
        //     ->where('day_period', $dayPeriod)
        //     ->where('company_id', $company->id)
        //     ->exists();
        //
        // if ($hasDuplicate) {
        //     throw new InvalidArgumentException(
        //         __('NotificationOrigin/errors.duplicated_schedule', ['period' => $dayPeriod, 'origin' => $originCode])
        //     );
        // }

        $model = $this->factory([
            'subject' => $subject,
            'message' => $message,
            'origin_code' => $originCode,
            'scheduled_at' => $scheduledAt,
            'day_period' => $dayPeriod,
            'company_id' => $company->id,
        ]);

        $this->save($model);

        return $model;
    }

    /**
     * Create Notification Origin
     *
     * @param array   $data    Data
     * @param Company $company Company entity
     *
     * @return Collection
     */
    public function createOriginCodes(array $data = [], Company $company): Collection
    {
        $dayPeriod = array_get($data, 'day_period');
        $scheduledAt = Carbon::parse(array_get($data, 'scheduled_at'));

        if ($dayPeriod === NotificationOrigin::MORNING) {
            $scheduledAt->hour(12)->minute(0)->second(0);
        }

        if ($dayPeriod === NotificationOrigin::AFTERNOON) {
            $scheduledAt->hour(18)->minute(0)->second(0);
        }

        if (now()->gt($scheduledAt)) {
            throw new InvalidArgumentException(
                __('validation.after', [
                    'attribute' => __('validation.attributes.scheduled_at'),
                    'date' => $scheduledAt
                ])
            );
        }

        $notificationOrigins = collect();

        $originCodes = array_get($data, 'origin_code', []);

        foreach ($originCodes as $originCode) {
            $model = $this->createModel(
                $data['subject'],
                $data['message'],
                $originCode,
                $data['scheduled_at'],
                $data['day_period'],
                $company
            );

            $notificationOrigins->push($model);
        }

        return $notificationOrigins;
    }

    /**
     * Updates model data using $data.
     * The sequence performs the Model update.
     *
     * @param mixed $model model
     * @param array $data  data
     *
     * @return boolean
     */
    public function update($model, array $data = []): bool
    {
        if ($model->processed_at !== null) {
            throw new InvalidArgumentException(__('notAllowed.update'));
        }

        $this->setModelData($model, $data);

        return $this->save($model);
    }

    /**
     * Runs the delete command model.
     * The goal is to allow the implementation of your business logic
     * before the command.
     *
     * @param mixed $model model
     *
     * @return bool
     * @throws \Exception
     */
    public function delete($model): bool
    {
        if ($model->processed_at !== null) {
            throw new InvalidArgumentException(__('notAllowed.delete'));
        }

        return $model->delete();
    }
}
