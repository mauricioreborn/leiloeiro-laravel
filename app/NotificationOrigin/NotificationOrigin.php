<?php

namespace App\NotificationOrigin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

use App\LeadTime\LeadTime;

class NotificationOrigin extends Model
{
    use Eloquence, Mappable, SoftDeletes;

    const MORNING = 'morning';

    const AFTERNOON = 'afternoon';

    const NIGHT = 'night';

    protected $fillable = [
        'origin_code',
        'subject',
        'message',
        'processed_at',
        'total_sent',
        'day_period',
        'scheduled_at',
        'company_id'
    ];

    protected $guarded = ['processed_at', 'total_sent'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leadtime()
    {
        return $this->belongsTo(LeadTime::class, 'origin_code', 'cod_origem')->groupBy('cod_origem');
    }

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return mixed
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }
}
