<?php
namespace App\Stock\Services;

use App\Company\Company;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\Fefo\Services\FefoBidsService;
use App\Fefo\Services\FefoPriceService;
use App\Fefo\Services\FefoService;
use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\Jobs\ProcessStockJob;
use App\Mobile\Services\HomeService;
use App\ProcessedFefo\Service\ProcessedFefoService;
use App\Products\Product;
use App\Products\Services\ProductPriceService;
use App\Products\Services\ProductService;
use App\RangePrice\Services\RangePriceService;
use App\Stock\Stock;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\SocketHandler;

class StockService
{
    const ACCOUNTBALANCE = 500; // @TODO: REMOVER `ACCOUNTBALANCE` E MOVER PARA `HOME MOBILE`

    /**
     * process product price
     *
     * @param FileQueue $fileQueue  fileQueue
     * @param string    $status     status
     * @param float     $shelfLife  shelfLife
     * @param Carbon    $expiredAt  expiredAt
     * @param string    $originCode originCode
     * @param float     $volume     volume
     * @param string    $batch      batch
     * @param string    $sku        sku
     *
     * @return App\Stock\Stock
     */
    public function processStock(
        FileQueue $fileQueue,
        string $status,
        float $shelfLife,
        Carbon $expiredAt,
        string $originCode,
        float $volume,
        string $batch,
        string $sku,
        Carbon $fabricatedAt = null,
        array $prices = []
    ) {
        $company  = $fileQueue->company;

        $product = (new ProductService())->getProductBySku($sku, $company);

        $attributes = [
            'company_id' => $company->id,
            'product_id' => $product->id,
            'origin_code' => $originCode,
            'batch' => $batch,
        ];

        if (((100 - $shelfLife) / 100) > 0) {
            $lifetime = now()->diffInDays($expiredAt, false) / ((100 - $shelfLife) / 100);
        } else {
            $lifetime = 0;
        }

        $fabricatedAt = $fabricatedAt ?? $expiredAt->copy()->subDays($lifetime);

        $status = (stristr($status, 'sim') ?
            Status::type(Status::ACTIVE) :
            Status::type(Status::BLOCKED));

        $data = [
            'volume' => $volume,
            'fabricated_at' => $fabricatedAt->startOfDay(),
            'expired_at' => $expiredAt->endOfDay(),
            'status_id' => $status->id,
            'file_queue_id' => $fileQueue->id,
        ];

        if (count($prices)) {
            $data['price'] = array_get($prices, 'price', 0);
            $data['original_price'] = array_get($prices, 'originalPrice', 0);
            $data['min_price'] = array_get($prices, 'minPrice', 0);
        }

        if($company->id == 5){

            $batch = explode("|", $batch);
            $batch = array_shift($batch);

            if(strtolower($batch) == "normal"){
                $data['price'] = $this->priceIncreaseTirolez($product, $data['price']);
            }
        }

        if($expiredAt->isPast()) {
            $message = "{$sku} com shelfLife expirado (100%)";

            Log::error($message);

            (new FileQueueService)->createLog($fileQueue, ['shelfLifeExpired' => $message]);

            return false;
        }

        return Stock::updateOrCreate($attributes, $data);

    }

    /**
     * process fefo by date
     *
     * @param  Stock  $stock Stock
     * @param  Carbon $date  Date
     *
     * @return App\Stock\Stock
     */
    public function processFefo(Stock $stock, Carbon $date)
    {
        $isValid = $stock->isValid($date->addDays(7));

        $erros = [];

        if (!$isValid) {
            $this->expireStock($stock);
            Log::error("Stock: {$stock->id} EXPIRADO");

            $erros[] = __('Uploads/stock.expired', [
                'product' => $stock->product->name,
                'origin_code' => $stock->origin_code
            ]);
        }

        if ($stock->status->id != Status::type(Status::ACTIVE)->id) {
            $isValid = false;
            Log::error("Stock: {$stock->id} BLOQUEADO");

            $erros[] = __('Uploads/stock.not_active', [
                'product' => $stock->product->name,
                'origin_code' => $stock->origin_code
            ]);
        }

        $fefo = $this->getFefo($stock);

        $bidVolume = 0;

        if ($fefo) {
            $bids = (new FefoBidsService())->getFefoBids($fefo);

            $bidVolume = $bids->sum('weight');
        }

        $sellingPrice = $stock->price;

        $originalPrice = $stock->original_price;
        $minPrice = $stock->min_price;

        $tracks = $this->getStockTrack($stock);
        $track = array_get($tracks, 'track');

        if (!$sellingPrice) {

            $productPrice = (new ProductPriceService())->getProductPrice($stock->product, $stock->origin_code);

            if (isset($productPrice->id)) {
                $originalPrice = $productPrice->price;

                $sellingPrices = $this->getSellingPrice($track, $productPrice->price);

                $sellingPrice = array_get($sellingPrices, 'sellingPrice');
                $minPrice = array_get($sellingPrices, 'minPrice');

                $piecesInKg = 1 / $stock->product->weight_piece;

                $originalPrice *= $piecesInKg;
                $sellingPrice *= $piecesInKg;
                $minPrice *= $piecesInKg;
            }
        }

        if($stock->company_id === 5 && strtolower($stock->product->unit) == 'cx'){

            if($stock->product->weight_piece == 0) {
                $isValid = false;
                Log::error("Stock: {$stock->product->name} PRODUTO COM GRAMATURA ZERADA");
                $erros[] = __('Uploads/stock.without_min_order', ['product' => $stock->product->name]);
            }else{
                $piecesInKg = 1 / $stock->product->weight_piece;
                $originalPrice = ($originalPrice / $stock->product->pieces) * $piecesInKg;
                $sellingPrice = ($sellingPrice / $stock->product->pieces) * $piecesInKg;
                $minPrice = ($minPrice / $stock->product->pieces) * $piecesInKg;
            }
        }

        if ($bidVolume > $stock->volume || (!$sellingPrice)) {
            Log::error("Stock: {$stock->id} SEM PRODUCT PRICE");
            $isValid = false;

            $erros[] = __('Uploads/stock.without_product_price', [
                'product' => $stock->product->name,
                'origin_code' => $stock->origin_code
            ]);
        }

        if ($sellingPrice) {
            $volume = ($stock->volume - $bidVolume);

            if ($stock->company_id === 4 && ($volume * $sellingPrice) < self::ACCOUNTBALANCE) {
                $isValid = false;
                Log::error("Stock: {$stock->id} MENOR QUE PEDIDO MINIMO");

                $erros[] = __('Uploads/stock.without_min_order', [
                    'product' => $stock->product->name,
                    'volume' => $stock->volume,
                    'origin_code' => $stock->origin_code
                ]);
            }
        }

        if ($isValid) {
            if ($fefo) {
                $data = [
                    'volume' => $volume,
                    'ds_chave_seara' => $stock->batch,
                ];

                if($stock->company_id === 5){
                    $data['selling_price'] = $sellingPrice;
                    $data['max_price'] = $originalPrice;
                    $data['min_price'] = $minPrice;
                }

                $fefo = (new FefoService())->updateFefo($fefo, $data);
            } else {

                $oldFefo =  (new FefoService())->searchFefoOrderWeeks($stock);

                $data = [
                    'company_id' => $stock->company->id,
                    'date' => now()->toDateString(),
                    'volume' => $volume,
                    'faixa_fefo' => $track,
                    'cd_faixa_fefo' => $track,
                    'weeks' => $stock->period,
                    'leadtime_code' => $stock->origin_code,
                    'product_code' => $stock->product->code,
                    'highlight_app' => ($oldFefo) ? $oldFefo->highlight_app : 0,
                    'selling_price' => $sellingPrice,
                    'max_price' => $originalPrice,
                    'min_price' => $minPrice,
                    'ds_chave_seara' => $stock->batch,
                    'cd_empresa' => $stock->company->code,
                ];

                $fefoPrice = (new FefoPriceService())->search(
                    $stock->company,
                    $stock->period,
                    $stock->origin_code,
                    $stock->product->code
                )->first();

                if ($fefoPrice) {
                    $data['selling_price'] = $fefoPrice->selling_price;
                }

                $fefo = (new FefoService())->createFefo($data);
            }

            if($stock->company_id !== 5){
                (new RangePriceService())->createRangePrice($stock->company, $stock->product, $stock->origin_code, $track);
            }else if($stock->company_id === 5 && $this->getTirolezStockBatchType($stock->batch) == "normal") {
                (new RangePriceService())->createRangePrice($stock->company, $stock->product, $stock->origin_code, $track);
            }

            (new ProcessedFefoService())->processedFefo($fefo->id, now());
        } else {
            $this->invalidStock($stock);
        }

        if (count($erros)) {
            (new FileQueueService())->createLog($stock->fileQueue, $erros);
        }

        (new HomeService())->forgetClientsHomeOriginCode($stock->company, $stock->origin_code);

        return $stock;
    }

    /**
     * calculate track of stock
     *
     * @param Stock $stock Stock entity
     *
     * @return array
     */
    public function getStockTrack(Stock $stock): array
    {
        $codeTrack = '600001';
        $track = '001';

        if($stock->company_id !== 5){
            if ($stock->price) {
                if ($stock->shelf_life > 33) {
                    $codeTrack = '600002';
                    $track = '002';
                }

                if ($stock->shelf_life > 50) {
                    $codeTrack = '600003';
                    $track = '003';
                }

                if ($stock->shelf_life > 66) {
                    $codeTrack = '600003';
                    $track = '003';
                }

                return [
                    'codeTrack' => $codeTrack,
                    'track' => $track,
                ];
            }  else {
                if ($stock->shelf_life > 50) {
                    $codeTrack = '600002';
                    $track = '002';
                }

                if ($stock->shelf_life > 60) {
                    $codeTrack = '600003';
                    $track = '003';
                }
            }
        }else{
            if($this->getTirolezStockBatchType($stock->batch) != "normal"){
                $codeTrack = '600003';
                $track = '003';
            }
        }

        return [
            'codeTrack' => $codeTrack,
            'track' => $track,
        ];
    }

    /**
     * calculate track of stock
     *
     * @param Stock $stock Stock entity
     *
     * @return array
     */
    public function getSellingPrice(string $track, float $price): array
    {
        $sellingPrice = $price;

        if ($track == '002') {
            $sellingPrice = $price * .85;
        }

        if ($track == '003') {
            $sellingPrice = $price * .55;
        }

        return [
            'sellingPrice' => $sellingPrice,
            'minPrice' => 0,
        ];
    }

    /**
     * block stock
     *
     * @param Company $company Company entity
     * @param array   $ids     Ids of Stock
     *
     * @return void
     */
    public function blockStock(Company $company, array $ids)
    {
        $toBlock = Stock::where('company_id', $company->id)
            ->whereNotIn('id', $ids)
            ->pluck('id');

        Stock::whereIn('id', $toBlock)->update(['status_id' => Status::type(Status::BLOCKED)->id]);

        foreach($toBlock as $id) {
            dispatch(new ProcessStockJob($id));
        }
    }

    /**
     * expire stock
     *
     * @param  Stock $stock Stock
     *
     * @return Stock
     */
    public function expireStock(Stock $stock): Stock
    {
        $stock->status_id = Status::type(Status::EXPIRED)->id;

        $stock->save();

        return $stock->fresh();
    }

    /**
     * invalid all type stock
     *
     * @param Stock $stock Stock entity
     *
     * @return void
     */
    public function invalidStock(Stock $stock)
    {
        $fefo = $this->getFefo($stock);

        if ($fefo) {
            (new FefoService())->delete($fefo);
        }
    }

    /**
     * get fefo list from stock
     *
     * @param Stock $stock Stock entity
     *
     * @return collection
     */
    public function getFefo(Stock $stock)
    {
        return Fefo::where('weeks', $stock->period)
            ->where('company_id', $stock->company_id)
            ->where('leadtime_code', $stock->origin_code)
            ->where('product_code', $stock->product->code)
            ->where('ds_chave_seara', $stock->batch)
            ->where('date', now()->toDateString())
            ->first();
    }

    /**
     * Method to replicate Stock and Fefo
     * @return void
     */
    public function replicate()
    {
        $companies = Company::with('companySettings')
            ->whereHas('companySettings', function ($query) {
                return $query->where(['setting' => 'hasStockReplicate', 'value' => 1]);
            })
            ->get();

        if($companies) {

            $statusActive = Status::type(Status::ACTIVE);

            foreach ($companies as $company) {
                $stocks = $company->stocks()->where('status_id', $statusActive->id)->get();

                foreach($stocks as $stock){

                    $tracks = $this->getStockTrack($stock);

                    if($tracks['track'] != "001"){
                        $msg = "FEFO do stock {$stock->id} para a company {$company->id} não duplicado, fora da faixa";
                        Log::info($msg);
                        continue;
                    }

                    Log::info("Duplicando FEFO do stock {$stock->id} para a company {$company->id}");
                    ProcessStockJob::dispatch($stock->id);
                }
            }
        }
    }

    public function priceIncreaseTirolez($product, $sellingPrice)
    {
        $percent = $product->price_increase != 0.00 ? $product->price_increase - 1 : 0;
        return $sellingPrice * (1 + ($percent/100));
    }

    /**
     * Method to get type Stock Tirolez
     *
     * @param $batch
     * @return string
     */
    public function getTirolezStockBatchType($batch): string
    {
        $batch = explode("|", $batch);
        return strtolower(array_shift($batch));
    }
}
