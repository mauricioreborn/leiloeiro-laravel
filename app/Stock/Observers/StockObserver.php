<?php

namespace App\Stock\Observers;

use App\Jobs\ProcessStockJob;
use App\Stock\Stock;

class StockObserver
{
    /**
     * Handle the Stock "created" event.
     *
     * @param Stock $stock Stock entity
     *
     * @return void
     */
    public function created(Stock $stock)
    {
        ProcessStockJob::dispatch($stock->id);
    }

    /**
     * Handle the Stock "updated" event.
     *
     * @param Stock $stock Stock entity
     *
     * @return void
     */
    public function updated(Stock $stock)
    {
        ProcessStockJob::dispatch($stock->id);
    }
}
