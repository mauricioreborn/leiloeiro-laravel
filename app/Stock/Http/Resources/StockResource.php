<?php
namespace App\Stock\Http\Resources;

use App\Company\Http\Resources\CompanyResource;
use App\Core\Http\Resources\StatusResource;
use App\FileQueue\Http\Resources\FileQueueResource;
use App\Products\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class StockResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request Request object
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'stock',
            'id' => $this->id,
            'attributes' => [
                'origin_code' => $this->origin_code,
                'volume' => $this->volume,
                'batch' => $this->batch,
                'fabricated_at' => $this->fabricated_at,
                'expired_at' => $this->expired_at,
            ],
            'relationships' => [
                'status' => new StatusResource($this->whenLoaded('status')),
                'product' => new ProductResource($this->whenLoaded('product')),
                'company' => new CompanyResource($this->whenLoaded('company')),
                'fileQueue' => new FileQueueResource($this->whenLoaded('fileQueue')),
            ],
        ];
    }
}
