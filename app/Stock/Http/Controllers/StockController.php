<?php

namespace App\Stock\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Stock\Services\StockService;
use Illuminate\Http\Request;

class StockController extends Controller
{
    protected $stockService;

    /**
     * StockController constructor.
     *
     * @param StockService $stockService Service class
     * @return null
     */
    public function __construct(StockService $stockService)
    {
        $this->stockService = $stockService;
    }
}
