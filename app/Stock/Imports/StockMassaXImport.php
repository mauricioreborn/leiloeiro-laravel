<?php

namespace App\Stock\Imports;

use App\Core\Entities\Status;
use App\FileQueue\FileQueue;
use App\Stock\Services\StockService;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;

class StockMassaXImport implements
    ToModel,
    WithHeadingRow,
    WithCustomCsvSettings,
    WithValidation,
    SkipsOnFailure,
    WithEvents
{
    protected $productService;
    protected $fileQueue;
    protected $stockIds = [];
    protected $errors = [];

    use Importable, SkipsFailures, RegistersEventListeners;

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);

        $this->stockService = new StockService();
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;

            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function afterImport(AfterImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $stockIds = $event->getConcernable()->stockIds;
            $fileQueue = $event->getConcernable()->fileQueue;

            (new StockService())->blockStock($fileQueue->company, $stockIds);
        }
    }

    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     */
    public function model(array $row)
    {
        $status = "sim";
        $shelfLife = 0;

        $fabricatedAt = Carbon::parse(array_get($row, 'fabricacao'));
        $expiredAt = Carbon::parse(array_get($row, 'vencimento'));
        $volume = array_get($row, 'volume_total_kg', 0);
        $sku = array_get($row, 'codigo');

        $price = array_get($row, 'valor_atual');
        $originalPrice = array_get($row, 'valor_cheio');
        $minPrice = array_get($row, 'valor_minimo');

        $prices = [
            'price' => $price,
            'originalPrice' => $originalPrice,
            'minPrice' => $minPrice,
        ];

        $distribuionCenter = $this->fileQueue->company->distributionCenters->first();
        $originCode = $distribuionCenter->code;

        $batch = "{$row['fabricacao']}|{$row['vencimento']}";

        $stock = $this->stockService->processStock(
            $this->fileQueue,
            $status,
            $shelfLife,
            $expiredAt,
            $originCode,
            $volume,
            $batch,
            $sku,
            $fabricatedAt,
            $prices
        );

        if($stock) {
            $this->stockIds[] = $stock->id;
        }
    }

    /**
     * get csv settings
     *
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        $companyId = $this->fileQueue->company_id;

        return [
            'codigo' => "required|exists:produtos,codigo,deleted_at,NULL,company_id,{$companyId}",
            'volume_total_kg' => 'required',
            'valor_atual' => 'required',
            'valor_cheio' => 'required',
            'valor_minimo' => 'required',
            'fabricacao' => 'required',
            'vencimento' => 'required',
        ];
    }
}