<?php

namespace App\Stock\Imports;

use App\Core\Entities\Status;
use App\Core\Helpers\FormatHelper;
use App\FileQueue\FileQueue;
use App\FileQueue\Services\FileQueueService;
use App\Stock\Services\StockService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;

class StockImport implements ToCollection, WithHeadingRow, WithEvents
{
    use Importable, RegistersEventListeners;

    protected $stockService;
    protected $fileQueue;
    protected $stockIds = [];
    protected $errors = [];
    protected $headingRow = 4;

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);

        $this->stockService = new StockService();
    }

    /**
     * trigged before import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $fileQueue = $event->getConcernable()->fileQueue;

            $fileQueue->update([
                'status_id' => Status::type(Status::INTEGRATED)->id
            ]);
        }
    }

    /**
     * trigged after import file
     *
     * @param AfterImport $event event
     *
     * @return void
     */
    public static function afterImport(AfterImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $stockIds = $event->getConcernable()->stockIds;
            $fileQueue = $event->getConcernable()->fileQueue;

            (new StockService())->blockStock($fileQueue->company, $stockIds);
        }
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return $this->headingRow;
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        $companyId = $this->fileQueue->company_id;

        return [
            'material' => "required|exists:produtos,codigo,deleted_at,NULL,company_id,{$companyId}",
            'kg' => 'required',
            'lote' => 'required',
            'shelf_life' => 'required',
            'data_de_vencimento' => function ($attribute, $value, $onFailure) {

                $errorFormat = __('validation.date_format', ['format' => 'dd/mm/aaaa']);
                $errorNull   = __('validation.required');

                if (is_null($value)) {
                    $onFailure($errorNull);
                }

                if (is_int($value) || is_float($value)) {

                    $value = FormatHelper::transformDate((int) $value);

                    if (!Carbon::checkDate($value->format('Y-m-d'))) {
                        $onFailure($errorFormat);
                    }
                }

                if (is_string($value)) {
                    $parsed = date_parse_from_format('d/m/Y', $value);

                    if ($parsed['error_count'] > 0 || $parsed['warning_count'] > 0) {
                        $onFailure($errorFormat);
                    }
                }
            },
        ];
    }

    /**
     * @param Collection $rows imported rows
     * @return void
     */
    public function collection(Collection $rows)
    {
        $errors = [];
        $validRows = $rows->filter(function ($row) {
            $emptyColumnsCount = 0;

            foreach($row as $column => $value) {
                if(array_key_exists($column, $this->rules()) && !$value) {
                    $emptyColumnsCount++;
                }
            }

            return $emptyColumnsCount !== 5;
        });

        foreach($validRows as $rowNumber => $row) {
            $validation = Validator::make($row->toArray(), $this->rules());
            if($validation->fails()) {
                $errors[$rowNumber + $this->headingRow() + 1] = $validation->errors()->getMessageBag()->all();
                continue;
            }

            $status = "sim";
            $shelfLife = FormatHelper::percent(array_get($row, 'shelf_life'));
            $expiredAt = FormatHelper::transformDate(array_get($row, 'data_de_vencimento'));
            $originCode = (string) array_get($row, 'cen');
            $volume = array_get($row, 'kg', 0);
            $batch = (string) array_get($row, 'lote');
            $sku = array_get($row, 'material');

            $stock = $this->stockService->processStock(
                $this->fileQueue,
                $status,
                $shelfLife,
                $expiredAt,
                $originCode,
                $volume,
                $batch,
                $sku
            );

            if($stock) {
                $this->stockIds[] = $stock->id;
            }
        }

        if(count($errors) > 0) {
            $this->logErrors($errors);
        }
    }

    /**
     * @param array $errors errors array
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function logErrors($errors)
    {
        $this->fileQueue->update([
            'status_id' => Status::type(Status::WITH_ERRORS)->id
        ]);

        $errorMessages = [];

        foreach($errors as $row => $messages) {
            $message = implode(', ', $messages);
            $errorMessages[] = "Linha {$row}: {$message}";
        }

        return (new FileQueueService)->createLog($this->fileQueue, $errorMessages);
    }
}
