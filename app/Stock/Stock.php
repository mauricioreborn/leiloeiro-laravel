<?php

namespace App\Stock;

use App\Company\Company;
use App\Core\Entities\Status;
use App\FileQueue\FileQueue;
use App\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Stock extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'origin_code',
        'volume',
        'batch',
        'fabricated_at',
        'expired_at',
        'company_id',
        'status_id',
        'product_id',
        'file_queue_id',
        'price',
        'min_price',
        'original_price',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $dates = ['fabricated_at', 'expired_at'];

    /**
     * Company relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fileQueue()
    {
        return $this->belongsTo(FileQueue::class);
    }

    /**
     * Company relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }

    /**
     * status relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * product relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get if stock is valid
     *
     * @param  null|Carbon $date Date
     *
     * @return bool
     */
    public function isValid(?Carbon $date = null): bool
    {
        $period = $date ?? now();

        return $period->lt($this->expired_at);
    }

    /**
     * Get period of stock
     *
     * @return integer
     */
    public function getPeriodAttribute(): int
    {
        return now()->diffInDays($this->expired_at, false);
    }

    /**
     * Get shelf life
     *
     * @return float
     */
    public function getShelfLifeAttribute(): float
    {
        $validate = $this->fabricated_at->diffInDays($this->expired_at);

        $shelfLife = 100 - (($this->period / $validate) * 100);

        return (integer) ceil(number_format($shelfLife, 2));
    }
}
