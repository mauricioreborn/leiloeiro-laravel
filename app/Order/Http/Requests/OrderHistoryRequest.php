<?php

namespace App\Order\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'days' => 'integer|min:1|max:5',
            'product_code' => 'integer|required_with_all:origin_code|required_with_all:weeks',
            'origin_code' => 'integer|required_with_all:product_code|required_with_all:weeks',
            'weeks' => 'integer|required_with_all:product_code|required_with_all:origin_code',
            'type' => 'in:bid,cart',
        ];
    }
}
