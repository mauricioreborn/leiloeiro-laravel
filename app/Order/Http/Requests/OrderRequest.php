<?php

namespace App\Order\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'POST':
                return true;
            break;
            case 'PUT':
                return Gate::allows('companyManagementRequest', $this->order->company) && Gate::allows('userAccess', $this->order->company);
            break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                return [
                    'order_code' => 'nullable|integer',
                    'client_id' => 'required|exists:clientes,id',
                    'date' => 'required|date_format:Y-m-d',
                    'bid_id' => 'nullable|integer|exists:lances,id',
                    'status_id' => 'required|integer',
                    'rejection_motives_id' => 'nullable|integer',
                    'partial_accept' => 'required|boolean',
                    'shipment_date' => 'date_format:Y-m-d',
                    'delivery_date' => 'date_format:Y-m-d',
                    'unread' => 'nullable|boolean',
                    'order_product' => 'required',
                    'order_product.*.order_product_code' => 'integer',
                    'order_product.*.product_id' => 'required|integer|exists:produtos,codigo',
                    'order_product.*.origin_code' => 'required|integer',
                    'order_product.*.weeks' => 'required|integer|min:1',
                    'order_product.*.box_amount' => 'required|integer|min:1',
                    'order_product.*.kg_total' => 'required|min: 0.1',
                    'order_product.*.kg_price' => 'required|min: 0.1',
                    'order_product.*.shipment_date' => 'nullable|date_format:Y-m-d',
                    'order_product.*.delivery_date' => 'nullable|date_format:Y-m-d',
                    'order_product.*.status_id' => 'nullable|integer',
                    'order_product.*.rejection_motives_id' => 'nullable|integer',
                    'order_product.*.email_sent' => 'nullable|boolean',
                    'order_product.*.cd_empresa' => 'required|integer',
                    'order_product.*.cd_faixa_fefo' => 'nullable|integer',
                ];
            }

            case 'PUT': {
                return [
                    'order_code' => 'nullable|integer',
                    'status_id' => 'integer',
                    'rejection_motives_id' => 'nullable|integer',
                    'partial_accept' => 'boolean',
                    'shipment_date' => 'nullable|date_format:Y-m-d',
                    'delivery_date' => 'nullable|date_format:Y-m-d',
                    'unread' => 'nullable|boolean',
                    'order_product' => 'nullable',
                    'order_product.*.order_product_code' => 'nullable|integer',
                    'order_product.*.id' => 'required|integer|min:1',
                    'order_product.*.box_amount' => 'integer|min:1',
                    'order_product.*.kg_total' => 'min: 0.1',
                    'order_product.*.kg_price' => 'min: 0.1',
                    'order_product.*.shipment_date' => 'nullable|date_format:Y-m-d',
                    'order_product.*.delivery_date' => 'nullable|date_format:Y-m-d',
                    'order_product.*.status_id' => 'nullable|integer',
                    'order_product.*.rejection_motives_id' => 'nullable|integer',
                    'order_product.*.email_sent' => 'nullable|boolean',
                    'order_product.*.cd_empresa' => 'integer',
                    'order_product.*.cd_faixa_fefo' => 'integer',
                ];
            }
        }
    }
}
