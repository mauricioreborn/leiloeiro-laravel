<?php

namespace App\Order\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Order\Http\Requests\OrderHistoryRequest;
use App\Order\Http\Requests\OrderRequest;
use App\Order\Http\Resources\OrderHistoryResource;
use App\Order\Http\Resources\OrderResource;
use App\Order\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Order\Http\Resources\OrderExportResource;

use App\Order\Order;

class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * Set Order Service
     * OrderController constructor.
     * @param OrderService $orderService Order service class
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param Request $request Request object
     * @return AnonymousResourceCollection
     */
    public function list(Request $request)
    {
        $collection = $this->orderService->search(auth()->user()->company_id, $request->all());

        return OrderResource::collection($collection);
    }

    /**
     * @param OrderRequest $request Request object
     * @param integer      $id      Order ID
     * @return OrderResource
     * @throws \Exception
     */
    public function update(OrderRequest $request, Order $order)
    {
        $this->orderService->update($order, $request->except(['client_id', 'date', 'bid_id', 'status_id']));

        return new OrderResource($order->fresh('orderProduct', 'client'));
    }

    /**
     * @param OrderHistoryRequest $request Request object
     * @return AnonymousResourceCollection
     */
    public function history(OrderHistoryRequest $request)
    {
        return OrderHistoryResource::collection($this->orderService->history($request->user()->company));
    }

    /**
     * @param Request $request Request object
     * @return OrderExportResource
     */
    public function export(Request $request)
    {
        return new OrderExportResource($this->orderService->export($request->user()->company));
    }
}
