<?php

namespace App\Order\Http\Resources;

use App\Products\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Method to cast object to array
     * @param \Illuminate\Http\Request $request params to cast
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'order_product',
            'id' => $this->id,
            'attributes' => [
                'order_product_code' => $this->order_product_code,
                'order_id' => $this->order_id,
                'product_id' => $this->product_id,
                'origin_code' => $this->origin_code,
                'weeks' => $this->weeks,
                'original_box_amount' => $this->original_box_amount,
                'box_amount' => $this->box_amount,
                'kg_total' => $this->kg_total,
                'kg_price' => $this->kg_price,
                'price' => $this->price,
                'shipment_date' => $this->shipment_date,
                'delivery_date' => $this->delivery_date,
                'status_id' => $this->status_id,
                'rejection_motives_id' => $this->rejection_motives_id,
                'cd_empresa' => $this->cd_empresa,
                'cd_faixa_fefo' => $this->cd_faixa_fefo,
                'email_sent' => $this->email_sent,
                'created_at' => $this->created_at,
            ],
            'relationships' => [
                'product' => new ProductResource($this->whenLoaded('product')),
            ],
        ];
    }
}
