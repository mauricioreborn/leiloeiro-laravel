<?php

namespace App\Order\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Order\Http\Resources\OrderResource;

class OrderCollection extends ResourceCollection
{
     public function toArray($request)
     {
         return [
             'data' =>
                 $this->collection->map(function ($order) use ($request) {
                     return (new OrderResource($order))->toArray($request);
                 })
         ];
     }
}
