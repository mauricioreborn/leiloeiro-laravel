<?php

namespace App\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Order\Http\Resources\OrderProductResource;
use App\Client\Http\Resources\ClientResource;

class OrderExportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'orders_export',
            'attributes' => [
                'path' => array_get($this, 'path')
            ],
        ];


    }
}
