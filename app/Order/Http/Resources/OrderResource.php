<?php

namespace App\Order\Http\Resources;

use App\Bid\Http\Resources\BidResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Order\Http\Resources\OrderProductResource;
use App\Client\Http\Resources\ClientResource;
use App\Company\Http\Resources\CompanyResource;

class OrderResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request $request Request object
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'order',
            'id' => $this->id,
            'attributes' => [
                'order_code' => $this->order_code,
                'client_id' => $this->client_id,
                'date' => $this->date,
                'bid_id' => $this->bid_id,
                'status_description' => $this->status_description,
                'status_id' => $this->status_id,
                'reason_cancellation' => $this->reason_cancellation,
                'rejection_motives_id' => $this->rejection_motives_id,
                'partial_accept' => $this->partial_accept,
                'shipment_date' => $this->shipment_date,
                'delivery_date' => $this->delivery_date,
                'unread' => $this->unread,
                'integrated' => $this->integrated,
                'volume' => $this->orderProduct->sum('total_kg'),
                'price' => $this->orderProduct()->notCanceled()->sum('valor'),
                'box_amount' => $this->orderProduct->sum('qtd_caixas'),
                'kg_price' => $this->orderProduct->sum('valor_kg'),
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
            'relationships' => [
                'order_product' => OrderProductResource::collection($this->whenLoaded('orderProduct')),
                'client' => ClientResource::make($this->whenLoaded('client')),
                'bid' => BidResource::make($this->whenLoaded('bid')),
                'company' => CompanyResource::make($this->whenLoaded('company'))
            ],
        ];
    }
}
