<?php

namespace App\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Order\Http\Resources\OrderProductResource;
use App\Client\Http\Resources\ClientResource;

class OrderHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'order_history',
            'attributes' => [
                'date' => $this['date'],
                'total_volume' => $this['total_volume'],
                'orders' => $this['orders'],
                'clients' => $this['clients'],
                'traded_volume' => $this['traded_volume'],
                'volume' => $this['volume'],
            ],
        ];
    }
}
