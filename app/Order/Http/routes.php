<?php
Route::group(['prefix' => 'v1'], function (){

    Route::group(['prefix' => 'orders'], function (){
        Route::get('/', 'OrderController@list');
        Route::get('/history', 'OrderController@history');
        Route::get('/export', 'OrderController@export');
        Route::put('/{order}', 'OrderController@update');
    });
});
