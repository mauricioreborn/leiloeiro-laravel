<?php

namespace App\Order\Http\Factories\Filters;

use App\Auth\User;
use App\Bid\Bid;
use App\Core\Contracts\FilterInterface;
use Illuminate\Database\Query\Builder;

class Accepted_by implements FilterInterface
{

    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        $users = User::where('name', 'LIKE', "%{$param}%")->pluck('id')->toArray();
        $bids = Bid::whereIn('id_user', $users)->pluck('id')->toArray();

        return $query->whereNotNull('lance_id')->whereIn('lance_id', $bids);
    }
}
