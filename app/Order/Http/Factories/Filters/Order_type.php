<?php

namespace App\Order\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Order_type implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        if($param === 'cart') {
            return $query->whereNull('lance_id');
        }

        return $query->whereNotNull('lance_id');
    }
}
