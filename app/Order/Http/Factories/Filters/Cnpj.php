<?php

namespace App\Order\Http\Factories\Filters;

use App\Client\Client;
use App\Core\Contracts\FilterInterface;

class Cnpj implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        $clients = Client::where('cnpj', 'LIKE', "%{$param}%")->pluck('id')->toArray();

        return $query->whereIn('id_cliente', $clients);
    }
}
