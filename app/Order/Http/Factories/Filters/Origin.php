<?php

namespace App\Order\Http\Factories\Filters;

use App\Client\Client;
use App\Core\Contracts\FilterInterface;
use App\LeadTime\LeadTime;

class Origin implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        $cnpj = LeadTime::distinct()->where('name', 'LIKE', "%{$param}%")->pluck('cnpj')->toArray();
        $clients = Client::whereIn('cnpj', $cnpj)->pluck('id')->toArray();

        return $query->whereIn('id_cliente', $clients);
    }
}
