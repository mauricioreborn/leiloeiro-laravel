<?php

namespace App\Order\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;
use App\Order\OrderProduct;

class Product_code implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        $orderProductIds = OrderProduct::distinct()->where(
            'product_id',
            'LIKE',
            "%{$param}%"
        )->pluck('pedido_id')->toArray();
        return $query->whereIn('id', $orderProductIds);
    }
}
