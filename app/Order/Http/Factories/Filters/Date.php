<?php

namespace App\Order\Http\Factories\Filters;

use App\Core\Contracts\FilterInterface;

class Date implements FilterInterface
{
    /**
     * @param Builder $query Query builder
     * @param mixed   $param Search param
     * @return Builder
     */
    public function run($query, $param)
    {
        return $query->whereDate('date', '=', $param);
    }
}
