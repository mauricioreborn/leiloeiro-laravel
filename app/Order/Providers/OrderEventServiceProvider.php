<?php

namespace App\Order\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;

use App\Order\Events\OrderApprovedEvent;
use App\Order\Events\OrderCanceledEvent;

use App\Order\Listeners\OrderApprovedListener;
use App\Order\Listeners\OrderCanceledListener;

class OrderEventServiceProvider extends EventServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OrderApprovedEvent::class => [
            OrderApprovedListener::class
        ],
        OrderCanceledEvent::class => [
            OrderCanceledListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
