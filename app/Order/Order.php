<?php

namespace App\Order;

use App\Bid\Bid;
use App\Client\Client;
use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Core\Entities\RejectionMotive;
use App\Products\Product;
use App\Core\Entities\Status;
use App\Invoice\Invoice;
use App\Order\OrderProduct;
use App\Company\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class Order extends Model
{
    use Eloquence, Mappable;

    protected $table = 'pedidos';

    protected $fillable = [
        'codigo_pedido',
        'id_cliente',
        'data',
        'lance_id',
        'payment_status_id',
        'status_id',
        'aceite_parcial',
        'data_embarque',
        'data_entrega',
        'nao_lido',
        'company_id',

        //en
        'order_code',
        'client_id',
        'date',
        'bid_id',
        'partial_accept',
        'shipment_date',
        'delivery_date',
        'unread',
        'client_company_payment_id',
        'client_card_id',
        'cc_months',
        'tax_percent',
        'rejection_motives_id',
        'client_account_balance',
    ];

    public $maps = [
        'order_code' => 'codigo_pedido',
        'client_id' => 'id_cliente',
        'date' => 'data',
        'bid_id' => 'lance_id',
        'partial_accept' => 'aceite_parcial',
        'shipment_date' => 'data_embarque',
        'delivery_date' => 'data_entrega',
        'unread' => 'nao_lido',
        'integrated' => 'integrado'
    ];

    protected $hidden = [
        'codigo_pedido',
        'id_cliente',
        'data',
        'lance_id',
        'aceite_parcial',
        'data_embarque',
        'data_entrega',
        'nao_lido',
        'integrado',
    ];

    protected $appends = [
        'order_code',
        'client_id',
        'date',
        'bid_id',
        'partial_accept',
        'shipment_date',
        'delivery_date',
        'unread',
        'integrated',
        'status_description',
    ];

    protected $casts = [
        'partial_accept' => 'boolean',
        'unread' => 'boolean',
        'integrated' => 'boolean',
        'first_purchase' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderProduct()
    {
        return $this->hasMany(OrderProduct::class, 'pedido_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'pedidos_produtos', 'pedido_id', 'produto_id', null, 'codigo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bid()
    {
        return $this->belongsTo(Bid::class, 'lance_id');
    }

    /**
     * Company relation
     *
     * @return Company
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @param Builder $builder Query builder
     * @param integer $id      Company id
     * @return Builder
     */
    public function scopeFromCompany($builder, $id)
    {
        return $builder->where('company_id', $id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientCard()
    {
        return $this->belongsTo(ClientCard::class, 'client_card_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientCompanyPayment()
    {
        return $this->belongsTo(ClientCompanyPayment::class, 'client_company_payment_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function isPartiallyBilled()
    {
        $approved = Status::type(Status::APPROVED);
        $canceled = Status::type(Status::CANCELED);

        if($this->status_id !== $approved->id) {
            return false;
        }

        $orderHasProductPartiallyBilled = $this->orderProduct->filter(function ($value) {
            return $value->original_box_amount > $value->box_amount || $value->original_box_amount < $value->box_amount;
        });

        return $orderHasProductPartiallyBilled->isNotEmpty()
               || $this->orderProduct->where('status_id', $canceled->id)->isNotEmpty();
    }

    public function isCanceled()
    {
        return $this->status_id === Status::type(Status::CANCELED)->id;
    }

    /**
     * Acessors
     **/

    /**
     * get status description Accessor
     * transform status from int to string
     * @return string
    **/
    public function getStatusDescriptionAttribute()
    {
        return __('status.' . Status::find($this->status_id)->name);
    }

    /**
     * Return Rejection Motive
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rejectionMotives()
    {
        return $this->belongsTo(RejectionMotive::class);
    }
}
