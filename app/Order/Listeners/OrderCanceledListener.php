<?php
namespace App\Order\Listeners;

use App\Invoice\Services\InvoiceService;
use App\Order\Events\OrderCanceledEvent;
use App\Notification\Services\NotificationService;
use App\Order\Services\OrderService;

class OrderCanceledListener
{
    protected $notificationService;

    /**
     * Constructor method
     * @return void
     **/
    public function __construct()
    {
        $this->notificationService = new NotificationService();
    }

    /**
     * Handle the event.
     * @param OrderUpdatedEvent $event event
     * @return mixed
     */
    public function handle(OrderCanceledEvent $event)
    {
        $order = $event->order;

        (new OrderService())->saveAccountBalance($order);
        
        if ($order->bid_id) {
            foreach ($order->orderProduct as $orderProduct) {
                $this->notificationService->canceledBid(
                    $order->client_id,
                    $order->bid->id,
                    $orderProduct->product->name,
                    $order->rejectionMotives
                );
            }
        } else {
            $this->notificationService->allCanceledOrder($order->client_id, $order->id);
        }

        (new InvoiceService())->cancelOrder($order);
    }
}
