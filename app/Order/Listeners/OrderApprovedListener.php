<?php
namespace App\Order\Listeners;

use App\Invoice\Services\InvoiceService;
use App\Order\Events\OrderApprovedEvent;
use App\Notification\Services\NotificationService;

use App\Order\OrderProduct;
use App\Core\Entities\Status;

class OrderApprovedListener
{
    protected $notificationService;

    /**
     * Constructor method
     * @return void
     **/
    public function __construct()
    {
        $this->notificationService = new NotificationService();
    }

    /**
     * Handle the event.
     * @param OrderUpdatedEvent $event event
     * @return mixed
     */
    public function handle(OrderApprovedEvent $event)
    {
        $order = $event->order;

        if ($order->bid_id) {
            foreach ($order->orderProduct as $orderProduct) {

                if($order->bid->isPartiallyBilled()) {
                    $this->notificationService->partiallyBilledBid(
                        $order->client_id,
                        $order->bid->id,
                        $orderProduct->product->name,
                        $order->delivery_date->format('d/m/Y')
                    );
                } else if($order->bid->isBilledAbove()) {
                    $this->notificationService->billedAboveBid(
                        $order->client_id,
                        $order->bid->id,
                        $orderProduct->product->name,
                        $order->delivery_date->format('d/m/Y')
                    );
                } else {
                    $this->notificationService->finishedBid(
                        $order->client_id,
                        $order->bid->id,
                        $orderProduct->product->name,
                        $order->delivery_date->format('d/m/Y')
                    );
                }
            }
        } else {
            if ($order->isPartiallyBilled()) {
                $this->notificationService->partiallyBilledOrder($order->client_id, $order->id);
            } else {
                $this->notificationService->allApprovedOrder($order->client_id, $order->id);
            }
        }

        (new InvoiceService())->captureOrder($order);
    }
}
