<?php

namespace App\Order\Exports;

use App\Client\Client;
use App\Core\Entities\Status;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Products\Services\ProductPriceService;
use App\Products\Services\ProductService;

use App\Order\Order;
use App\Company\Company;
use App\Stock\Stock;

class OrderExport implements FromCollection, WithHeadings
{
    use Exportable;

    protected $company;

    /**
     * @param Company $company company
     * @return void
     **/
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $clientExceptions = Client::whereIn('cnpj', config('souk.sandbox_clients'))->pluck('id')->toArray();

        $orders = Order::with(['client'])
            ->where('status_id', Status::type(Status::PENDING)->id)
            ->where('company_id', $this->company->id)
            ->whereNotIn('client_id', $clientExceptions)
            ->get();

        $response = [];

        foreach ($orders as $order) {

            foreach ($order->orderProduct as $orderProduct) {

                $originCode = $orderProduct->origin_code;
                $clientCompany = $order->client->setClientCompany($this->company);
                $product = (new ProductService())->getProductBySku($orderProduct->product_id, $order->company);

                $productPrice = (new ProductPriceService())->getProductPrice($product, $originCode);

                $piecesInKg = 1 / $product->weight_piece;
                $priceInKg = $productPrice->price * $piecesInKg;

                $grammature = 1 / $product->weight_piece;
                $priceInUnit = (float) number_format($orderProduct->kg_price / $grammature, 2);

                $discount = ($priceInUnit - $productPrice->price) / $productPrice->price;

                $percent = $discount*100;

                $stock = (new Stock())
                    ->where('product_id', $product->id)
                    ->where('batch', $orderProduct->ds_chave_seara)
                    ->where('company_id', $this->company->id)
                    ->first();

                if($productPrice) {
                    $response[] = [
                        'origin' => $originCode,
                        'tomorrow' => now()->tomorrow()->format('d/m/y'),
                        'order_id' => $order->id,
                        'client_code' => $clientCompany->client_code,
                        'client_cnpj' => $order->client->cnpj,
                        'product_code' => $orderProduct->product_id,
                        'batch' => $orderProduct->ds_chave_seara,
                        'box_amount' => $orderProduct->box_amount,
                        'product_discount' => round($percent,2),
                        'shelf' => $stock->getShelfLifeAttribute(),
                        'product_price' => $productPrice->price,
                        'price_in_unit' => $priceInUnit
                    ];
                }
            }
        }
        return collect($response);
    }

    public function headings(): array
   {
        return [
            'PLANTA',
            'DATA REMESSA',
            'CÓDIGO PEDIDO',
            'SOLD',
            'CLIENTE',
            'PRODUTO',
            'LOTE',
            'CAIXAS SOLICITADAS',
            'DESCONTO SOLICITADO (%)',
            'SHELF',
            'PREÇO UNIT BASE',
            'PREÇO UNIT FECHADO'
        ];
   }
}
