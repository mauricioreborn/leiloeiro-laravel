<?php

namespace App\Order\Exports;

use App\ClientCompanies\ClientCompany;
use App\Order\Services\OrderService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;

use App\Order\Order;

class OrderS3IntegrationExport implements FromCollection, WithHeadings, WithColumnFormatting, WithCustomCsvSettings
{
    use Exportable;

    protected $order;

    /**
     * @param Order $order order
     * @return void
     **/
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $response = [];
        $client = $this->order->client;

        $client->clientCompany = ClientCompany::where([
            'client_id' => $client->id, 'company_id' => $this->order->company_id
        ])->first();

        foreach ($this->order->orderProduct as $orderProduct) {

            $dadosProduto = explode('|', $orderProduct->ds_chave_seara);
            $pedidoCliente = in_array('OFERTA', $dadosProduto) ? "Prioridade FF" : "";
            $precoNegociado = (new OrderService())->getBoxValueProduct($orderProduct);

            $response[] = [
                'cod_cliente' => $client->clientCompany->client_code,
                'tipo_pedido' => $dadosProduto[0],
                'pedido_cliente' => $pedidoCliente,
                'pedido_souk' => $this->order->id,
                'cod_item' => $orderProduct->product->code_formatted,
                'quantidade_cx' => $orderProduct->box_amount,
                'preco_negociado' => $precoNegociado,
                'fabricacao' => $dadosProduto[1],
                'vencimento' => $dadosProduto[2],
            ];
        }

        return collect($response);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'COD_CLIENTE',
            'TIPO_PEDIDO',
            'PEDIDO_CLIENTE',
            'PEDIDO_SOUK',
            'COD_ITEM',
            'QUANTIDADE_CX',
            'PRECO_NEGOCIADO',
            'FABRICACAO',
            'VENCIMENTO',
        ];
    }

    /**
    * @return array
    */
    public function columnFormats(): array
    {
        return [
            'G2:G' => '0.00',
        ];
    }

    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ";",
        ];
    }
}
