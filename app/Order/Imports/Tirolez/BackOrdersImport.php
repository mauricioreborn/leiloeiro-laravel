<?php

namespace App\Order\Imports\Tirolez;

use App\Core\Entities\Status;
use App\Core\Exceptions\EmptyRowException;
use App\FileQueue\Services\FileQueueService;
use App\Order\Services\OrderService;
use Illuminate\Support\Collection;
use App\FileQueue\FileQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\BeforeImport;
use Illuminate\Support\Facades\Validator;

class BackOrdersImport implements ToCollection, WithHeadingRow, WithCustomCsvSettings, WithEvents
{
    use Importable, RegistersEventListeners;

    protected $orderService;
    protected $fileQueue;
    protected $errors = [];
    protected $status = [];

    /**
     * construct
     *
     * @param integer $fileQueueId file queue id
     */
    public function __construct($fileQueueId)
    {
        $this->fileQueue = FileQueue::find($fileQueueId);

        $this->orderService = new OrderService();
        $this->orderService->getClientWithCodes($this->fileQueue->company);
        $this->orderService->getProductCodes($this->fileQueue->company);

        $this->status = $this->getStatus();
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * trigged before import file
     *
     * @param beforeImport $event event
     *
     * @return void
     */
    public static function beforeImport(BeforeImport $event)
    {
        if (get_class($event->getConcernable()) == static::class) {
            $worksheet = $event->reader->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();

            if($highestRow <= 1){
                $errors[] = "Arquivo sem dados ou apenas com o header";
                $event->getConcernable()->logErrors($errors, false);
                throw new EmptyRowException("Arquivo sem dados ou apenas com o header");
            }

            $fileQueue = $event->getConcernable()->fileQueue;
            $fileQueue->update(['status_id' => Status::type(Status::INTEGRATED)->id]);
        }
    }

    /**
     * get csv settings
     *
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    /**
     * process line
     *
     * @param array $row row
     *
     * @return void
     * @throws \Exception
     */
    public function collection(Collection $rows)
    {
        $errors = [];

        $validRows = $rows->filter(function ($row) {
            $emptyColumnsCount = 0;

            foreach ($row as $column => $value) {
                if (array_key_exists($column, $this->rules()) && !$value) {
                    $emptyColumnsCount++;
                }
            }

            return $emptyColumnsCount !== count(array_keys($this->rules()));
        });

        foreach ($validRows as $rowNumber => $row) {

            $validation = Validator::make($row->toArray(), $this->rules());

            $rowNumberValidation = $rowNumber + $this->headingRow() + 1;

            if ($validation->fails()) {
                $errors[$rowNumberValidation] = $validation->errors()->getMessageBag()->all();
                continue;
            }

            $clientCode = (int) array_get($row, 'codigo_cliente');
            $orderType = trim(array_get($row, 'tipo_pedido'));
            $orderNumber = (int) array_get($row, 'num_pedido_souk');
            $productCode = (int) array_get($row, 'codigo_item');
            $boxAmount = (float) array_get($row, 'quatidade_cx');
            $kgTotal = (float) array_get($row, 'quatidade_kg');
            $price = (float) array_get($row, 'preco_negociado');
            $expiredAt = trim(array_get($row, 'vencimento'));
            $status = (int) array_get($row, 'codigo_status');
            $reasonCancelation = trim(array_get($row, 'motivo_cancelamento'));

            $orderOrError = $this->orderService->importBackOrder(
                $clientCode,
                $orderType,
                $orderNumber,
                $productCode,
                $boxAmount,
                $kgTotal,
                $price,
                $expiredAt,
                $this->status[$status],
                $reasonCancelation
            );

            if (!isset($orderOrError->id)) {
                $errors[$rowNumberValidation][0] = $orderOrError;
            }
        }

        if (count($errors) > 0) {
            $this->logErrors($errors);
        }
    }

    /**
     * rules validation
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'codigo_cliente' => 'required',
            'tipo_pedido' => 'required',
            'num_pedido_souk' => 'required',
            'codigo_item' => 'required',
            'quatidade_cx' => 'required',
            'quatidade_kg' => 'required',
            'preco_negociado' => 'required',
            'vencimento' => 'required',
            'codigo_status' => 'required',
            'descricao_status' => 'required',
        ];
    }

    /**
     * @TODO move to abstract class
     * @param array $errors errors array
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function logErrors($errors, $withImplode = true)
    {
        $this->fileQueue->update(['status_id' => Status::type(Status::WITH_ERRORS)->id]);

        $errorMessages = [];

        foreach ($errors as $row => $messages) {
            $message =  $withImplode == true ? implode(', ', $messages) : $messages;
            $message = "Linha {$row}: {$message}";
            $errorMessages[] = $message;
            Log::info($message);
        }

        (new FileQueueService)->createLog($this->fileQueue, $errorMessages);
    }

    public function getStatus()
    {
        $inAnalysis = Status::type(Status::IN_ANALYSIS)->id;
        $canceled = Status::type(Status::CANCELED)->id;
        $approved = Status::type(Status::APPROVED)->id;

        return [
            1 => $inAnalysis,
            2 => $inAnalysis,
            3 => $inAnalysis,
            4 => $inAnalysis,
            5 => $approved,
            6 => $inAnalysis,
            7 => $approved,
            8 => $canceled,
            9 => $canceled,
        ];
    }
}