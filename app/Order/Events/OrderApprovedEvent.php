<?php
namespace App\Order\Events;

use Illuminate\Queue\SerializesModels;

use App\Order\Order;

class OrderApprovedEvent
{
    use SerializesModels;

    public $order;

    /**
     * Constructor method
     * @param Order $order order
     * @return void
     **/
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}
