<?php

namespace App\Order\Services;

use App\Bid\Services\BidService;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Core\Classes\CrudService;
use App\Core\Classes\Search;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Core\Helpers\FormatHelper;
use App\Fefo\Fefo;
use App\Invoice\Services\InvoiceService;
use App\Order\Events\OrderApprovedEvent;
use App\Order\Events\OrderCanceledEvent;
use App\Order\Exports\OrderExport;
use App\Order\Exports\OrderS3IntegrationExport;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Payment\Payment;
use App\Products\Services\ProductService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class OrderService extends CrudService
{
    /**
     * @var string
     */
    protected $entity = Order::class;

    protected $clientCodes = [];

    protected $productCodes = [];


    /**
     * @var string
     */
    protected $module = 'Order';

    protected $s3Path = 'data/tirolez/Recebe/';

    /**
     * Method to create an order
     * @param array   $data    order params
     * @param Company $company company entity
     * @return Model
     * @throws \Exception
     */
    public function createOrder(array $data = [], Company $company): Model
    {
        $clientCardId = array_get($data, 'client_card_id');

        $pending = Status::type(Status::PENDING);
        $paid = Status::type(Status::PAID);

        $data['payment_status_id'] = isset($clientCardId) ? $pending->id : $paid->id;

        $order = parent::create($data);

        if (isset($data['order_product'])) {
            $order->orderProduct()->createMany($data['order_product']);
        }

        $order = $order->fresh('orderProduct', 'client', 'clientCompanyPayment.companyPayment.payment');
        $order->client->clientCompany = $order->client->setClientCompany($company);

        if ($order->clientCompanyPayment) {
            if ($order->clientCompanyPayment->companyPayment->payment->type === Payment::CREDIT_CARD) {
                (new InvoiceService())->calculateReserve($order);
            }
        }

        return $order;
    }

    /**
     * Method to update a new order
     * @param Model $model Order model
     * @param array $data  Data to be updated
     * @return bool
     * @throws \Exception
     */
    public function update($model, array $data = []): bool
    {
        $model->update($data);

        $bid = $model->bid;

        if(isset($data['order_product'])) {
            foreach ($data['order_product'] as $toUpdate) {
                $status = $this->convertOrderStatusByBool($toUpdate['status']);

                $orderProduct = $model->orderProduct->find($toUpdate['id']);

                if ($orderProduct) {
                    $beforeUpdateStatus = $orderProduct->status_id;

                    $kg_total = $toUpdate['box_amount'] * $orderProduct->product->weight;
                    $price = $kg_total * $orderProduct->kg_price;

                    $updateData = [
                        'qtd_caixas' => $toUpdate['box_amount'],
                        'kg_total' => $kg_total,
                        'price' => $price,
                        'status_id' => $status['status_id'],
                        'rejection_motives_id' => $status['rejection_motives_id'],
                        'email_sent' => 0
                    ];

                    if(isset($toUpdate['ds_motivo_ccl_pedido']) && !empty($toUpdate['ds_motivo_ccl_pedido'])){
                        $updateData['DS_MOTIVO_CCL_PEDIDO'] = $toUpdate['ds_motivo_ccl_pedido'];
                        $updateData['rejection_motives_id'] = $toUpdate['rejection_motives_id'];
                    }

                    $orderProduct->update($updateData);

                    if ($bid) {
                        (new BidService())->update($bid, [
                            'weight' => $kg_total,
                            'price' => $price,
                            'box_amount' => $toUpdate['box_amount'],
                        ]);
                    }
                }
            }
        }

        $this->closeOrder($model);

        return true;
    }

    /**
     * Close an Order based on follow rules:
     * If there is any open order product, don't close (return false)
     * if all order products is canceled, then close order
     * else approve order
     * @param Order $order order
     * @return boolean
     **/
    protected function closeOrder(Order $order)
    {
        if ($order->orderProduct()->isPending()->get()->isNotEmpty()) {
            return false;
        }

        $isAllCanceled = $order->orderProduct->every(function ($value) {
            return $value->status_id == Status::type(Status::CANCELED)->id;
        });

        if ($isAllCanceled) {
            $order->status_id = Status::type(Status::CANCELED)->id;
            $order->rejection_motives_id = RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id;
        } else {
            $order->status_id = Status::type(Status::APPROVED)->id;
            $order->shipment_date = today()->addDay();
            $order->delivery_date = $order->shipment_date->addDay();
        }

        $order->integrated = 1;

        $order->save();

        if ($isAllCanceled) {
            event(new OrderCanceledEvent($order));
        } else {
            event(new OrderApprovedEvent($order));
        }

        return $order;
    }

    /**
     * Method to get consolidate order history (with status > 0) per day
     * @param Company $company
     * @return Collection
     */
    public function history(Company $company)
    {
        $fefo = Fefo::select(['date', DB::raw('SUM(volume) as total'), 'company_id'])
            ->fromCompany($company->id)
            ->orderBy('date', 'desc')
            ->take(5)
            ->groupBy('date')
            ->pluck('total', 'date');

        $allOrders = Order::with('orderProduct')
            ->fromCompany($company->id)
            ->where('status_id', '!=', Status::type(Status::CANCELED)->id)
            ->whereIn('date', $fefo->keys())
            ->get();

        $days = [];

        foreach($fefo as $date => $volume) {
            $orders = $allOrders->where('date', $date);

            $tradedVolume = $orders->where('status_id', '!=', Status::type(Status::CANCELED)->id)->sum(function ($order) {
                return $order->orderProduct->where('status_id', '!=', Status::type(Status::CANCELED)->id)->sum('total_kg');
            });

            $days[$date] = [
                'date' => $date,
                'total_volume' => FormatHelper::volume($volume),
                'orders' => $orders->count(),
                'clients' => $orders->pluck('client_id')->unique()->count(),
                'traded_volume' => FormatHelper::volume($tradedVolume),
                'volume' => $this->orderProductVolume($orders->pluck('id')->toArray()),
            ];
        }

        return collect(array_values($days));
    }

    /**
     * Method to get consolidate order product volume per day
     * @param array $orderIds
     * @return array
     */
    public function orderProductVolume($orderIds = []): array
    {
        return OrderProduct::selectRaw('status_id, sum(total_kg) as volume, sum(valor) as valor')
            ->whereIn('order_id', $orderIds)
            ->groupBy('status_id')
            ->orderBy('status_id')
            ->get()
            ->map(function ($orderProduct) {
                $title = __('status.'.Status::type(Status::INTEGRATED)->name);
                if($orderProduct->status_id){
                    $title = __('status.'.$orderProduct->status->name);
                }

                return [
                    'title' => $title,
                    'volume' => FormatHelper::volume($orderProduct->volume),
                    'price' => FormatHelper::price($orderProduct->price/$orderProduct->volume),
                ];
            })
            ->toArray();
    }

    /**
     * @param integer $companyId Company ID
     * @param array   $filters   Filters
     * @return mixed
     */
    public function search($companyId, $filters = [])
    {
        $searchInstance = new Search($this->module, $this->newQuery());

        $query = $searchInstance->run($filters);

        $query->fromCompany($companyId);

        return $query->paginate($filters['limit'] ?? 15);
    }

    /**
     * Calculate price of order
     *
     * @param Order $order Order entity
     * @return float $response response;
     * @TODO Improve this method for don't make unecessary queries
     **/
    public function calculatePriceOrder(Order $order)
    {
        $orderProducts = $order->orderProduct()->notCanceled()->get();

        $response = 0;
        foreach($orderProducts as $orderProduct) {
            $orderProduct['valor'] = (float) $orderProduct['valor'];
            $response += $orderProduct['valor'];
        }

        return $response;
    }

    /**
     * Cancel order
     *
     * @param Order           $order           Order entity
     * @param RejectionMotive $rejectionMotive Reason of cancel
     * @param string          $statusId        status Id
     *
     * @return App/Order/Order
     */
    public function cancelOrder(Order $order, RejectionMotive $rejectionMotive, $statusId = null): Order
    {
        if ($order->status_id !== Status::type(Status::CANCELED)->id) {
            $order->integrated = 1;
            $order->rejection_motives_id = $rejectionMotive->id;
            $order->status_id = Status::type(Status::CANCELED)->id;

            if ($statusId) {
                $order->payment_status_id = $statusId;
            }

            $order->save();

            $order->orderProduct()->update([
                'status_id' => Status::type(Status::CANCELED)->id,
                'rejection_motives_id' => $rejectionMotive->id,
                'email_enviado' => 0,
            ]);
        }

        return $order->fresh();
    }

    /**
     * Cancel order
     * @param Company $company Company
     * @return array
     */
    public function export(Company $company)
    {
        $now = now();

        $storagePath = "orders/exports/orders{$now}.xlsx";

        Excel::store(new OrderExport($company), $storagePath, 's3');

        return [
            'path' => $response = Storage::temporaryUrl($storagePath, now()->addHour())
        ];
    }

    /**
     * Method to export and save csv orders in s3 storage
     *
     * @param Order $order
     * @return array
     */
    public function exportToIntegration(Order $order)
    {
        $storagePath = $this->s3Path."PEDIDOS_SOUK_RECEBIMENTO_{$order->id}.csv";

        Log::info("Iniciando export para {$storagePath}");

        Excel::store(new OrderS3IntegrationExport($order), $storagePath, 's3_tirolez');

        Log::info("Arquivo exportado para {$storagePath}");

        return [
            'path' => $response = Storage::temporaryUrl($storagePath, now()->addHour())
        ];
    }

    /**
     * Method to sum all order products by order id
     * @param array $orderIds order ids array
     * @return mixed
     */
    public function sumOrderProductsByOrderIds(array $orderIds)
    {
        return OrderProduct::
            whereIn('pedido_id', $orderIds)
            ->sum('valor');
    }

    /**
     *
     * Method to validate if Client min_order is higher than order price
     * If order $price is higher than min_order, order is `valid` (return true), else is `false`
     * @param Order $order order
     * @return boolean
     **/
    public function isValidMinOrder(Order $order)
    {
        $price = $this->calculatePriceOrder($order);

        $clientCompany = $order->client->setClientCompany($order->company);

        if ($price > $clientCompany->min_order) {
            return true;
        }

        return false;
    }

    /**
     * Method to validate if Client account_balance is higher than order price
     * If order $price is smaller than account_balance, order is `valid` (return true), else is `false`
     * @param Order $order order
     * @return boolean
     **/
    public function isValidAccountBallance(Order $order)
    {
        if ($order->clientCompanyPayment) {
            $payment = $order->clientCompanyPayment->companyPayment->payment;

            if ($payment->isCreditCard) {
                return true;
            }
        }

        $price = $this->calculatePriceOrder($order);

        $clientCompany = $order->client->setClientCompany($order->company);

        if ($price < $clientCompany->account_balance) {
            return true;
        }

        return false;
    }

    /**
     * Method translate an order status
     * @param boolean|null $status status
     * @return array
     **/
    protected function convertOrderStatusByBool($status)
    {
        $response = [];

        if ($status === true) {
            $response['status_id'] = Status::type(Status::APPROVED)->id;
            $response['rejection_motives_id'] = null;
        } elseif ($status === false) {
            $response['status_id'] = Status::type(Status::CANCELED)->id;
            $response['rejection_motives_id'] = RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id;
        } else {
            $response['status_id'] = Status::type(Status::PENDING)->id;
            $response['rejection_motives_id'] = null;
        }

        return $response;
    }

    /**
     * Method to get client codes of client companies by company
     * @param Company $company company object
     * @return void
     */
    public function getClientWithCodes(Company $company)
    {
        $clientCompanies = ClientCompany::where([
            'company_id' => $company->id,
        ])->whereHas('client')->whereNotNull('client_code')->get();

        foreach ($clientCompanies as $clientCompany) {
            $this->clientCodes[$clientCompany->client_code]['id'] = $clientCompany->client->id;
        }
    }

    /**
     * Method to get product codes by company
     *
     * @param Company $company company object
     * @return array
     */
    public function getProductCodes(Company $company): void
    {
        $this->productCodes = (new ProductService())->getProductCodes($company);
    }

    /**
     * Method to import the back orders file
     *
     * @param Company $company Company object
     * @param string $client_code client code
     * @param string $orderType type order
     * @param string $orderNumber incremental order number
     * @param string $productCode product code on product table
     * @param string $boxAmount box amount on order product table
     * @param string $kgTotal kg total on order product table
     * @param string $price price on order product table
     * @param string $expiredAt date of expired_at of product
     * @param string $status status
     * @param string $reasonCancelation motive of rejection
     * @return array|null|string
     * @throws \Exception
     */
    public function importBackOrder(
        $client_code,
        $orderType,
        $orderNumber,
        $productCode,
        $boxAmount,
        $kgTotal,
        $price,
        $expiredAt,
        $status,
        $reasonCancelation
    ) {

        if (!array_key_exists($client_code, $this->clientCodes)) {

            $errorMessage =  __('Integration/backOrder.client_code_not_found',['client_code' => $client_code]);

            Log::error($errorMessage);
            return $errorMessage;
        }

        if (!in_array($productCode, $this->productCodes)) {

            $errorMessage =  __('Integration/backOrder.product_code_not_found',['product_code' => $productCode]);

            Log::error($errorMessage);
            return $errorMessage;
        }

        $order = Order::find($orderNumber);

        if (is_null($order)) {

            $errorMessage =  __('Integration/order.order_not_found', ['order' => $orderNumber]);

            Log::error($errorMessage);
            return $errorMessage;
        }

        $orderProducts = OrderProduct::where(['pedido_id' => $order->id, 'produto_id' => $productCode])->get();
        $orderProductHash = [];

        foreach ($orderProducts as $orderProduct) {

            $dsKey = $orderProduct->ds_chave_seara;
            $dsKey = explode('|', $dsKey);

            if (isset($dsKey[0]) && isset($dsKey[2])) {
                $hash = $dsKey[0] . "_" . $dsKey[2];
                $orderProductHash[$hash] = $orderProduct->toArray();
            } else {
                $errorMessage =  __('Integration/order.order_expired_at_not_found');
                Log::error($errorMessage);
                return $errorMessage;
            }
        }

        $hashOfParams = $orderType."_".$expiredAt;

        if (! array_key_exists($hashOfParams, $orderProductHash)) {

            $errorMessage =  __('Integration/order.order_product_not_found', [
                'product_code' => $productCode,
                'expired_at' => $expiredAt
            ]);

            Log::error($errorMessage);
            return $errorMessage;
        }

        $orderProduct = $orderProductHash[$hash];
        $rejection_motives_id = null;

        if(!empty($reasonCancelation) && strlen($reasonCancelation) > 0){
            $rejection_motives_id = RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id;
        }

        if ($status == Status::type(Status::APPROVED)->id) {
            $status = true;
        } elseif ($status === false || $status == Status::type(Status::CANCELED)->id) {
            $status = false;
        } else {
            $status = null;
        }

        $dateToUpdate = [
            'id' => $orderProduct['id'],
            'box_amount' => $boxAmount,
            'kg_total' => $kgTotal,
            'price' => $price,
            'status' => $status,
            'rejection_motives_id' => $rejection_motives_id,
            'ds_motivo_ccl_pedido' => $reasonCancelation,
        ];

        $orderToUpdate['order_product'][] = $dateToUpdate;

        $this->update($order, $orderToUpdate);

        return $order;
    }

    /**
     * Method to get unit value product in box
     * @param $orderProduct
     * @return float
     */
    public function getBoxValueProduct($orderProduct)
    {
        if(strtolower($orderProduct->product->medida) == "cx"){
            if($orderProduct->valor_kg !== 0 && $orderProduct->product->unidades_caixa !== 0){
                return round($orderProduct->valor_kg * (float)$orderProduct->product->peso_caixa, 2);
            }
        }

        return $orderProduct->kg_price;
    }

    public function saveAccountBalance($order)
    {
        $clientCompany = $order->client->setClientCompany($order->company);
        $order->client_account_balance = $clientCompany->account_balance;

        return $order->save();
    }
}
