<?php

namespace App\Order\Observers;

use App\Client\Client;
use App\Jobs\Integration\Tirolez\SendOrder;
use App\Order\Order;
use Illuminate\Support\Facades\Log;

class OrderObserver
{
    public function created(Order $order): void
    {
        if($order->client->order()->fromCompany($order->company_id)->count() === 1) {
            $order->first_purchase = true;
            $order->save();
        }

        $hasOrderS3Integration = $order->company->companySettings
            ->where('setting', 'hasOrderS3Integration')
            ->where('value', "1")
            ->first();

        if($hasOrderS3Integration){

            if($order->company_id == 5){
                $clientExceptions = Client::whereIn('cnpj', config('souk.sandbox_clients'))->pluck('id')->toArray();

                if(in_array($order->client_id, $clientExceptions, true)) {
                    return;
                }
            }

            Log::info('disparando o job para o pedido '.$order->id);
            dispatch(new SendOrder($order->id));
        }
    }
}
