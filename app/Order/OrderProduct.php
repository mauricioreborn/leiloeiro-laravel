<?php

namespace App\Order;

use App\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Mutable;

use App\Order\Order;
use App\LeadTime\LeadTime;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;

class OrderProduct extends Model
{
    use Eloquence, Mappable, Mutable, SoftDeletes;

    const UPDATED_AT = null;

    protected $table = 'pedidos_produtos';

    protected $fillable = [
        'codigo_pedido_produto',
        'pedido_id',
        'produto_id',
        'cod_origem',
        'semanas',
        'qtd_caixas',
        'original_box_amount',
        'total_kg',
        'valor',
        'valor_kg',
        'data_embarque',
        'data_entrega',
        'cd_empresa',
        'cd_faixa_fefo',
        'ds_chave_seara',
        'email_enviado',
        'CD_MOTIVO_CCL_PEDIDO',
        'DS_MOTIVO_CCL_PEDIDO',

        //en
        'order_product_code',
        'order_id',
        'product_id',
        'origin_code',
        'weeks',
        'box_amount',
        'kg_total',
        'price',
        'kg_price',
        'shipment_date',
        'delivery_date',
        'email_sent',
        'status_id',
        'rejection_motives_id',
    ];

    protected $maps = [
        'order_product_code' => 'codigo_pedido_produto',
        'order_id' => 'pedido_id',
        'product_id' => 'produto_id',
        'origin_code' => 'cod_origem',
        'weeks' => 'semanas',
        'box_amount' => 'qtd_caixas',
        'kg_total' => 'total_kg',
        'kg_price' => 'valor_kg',
        'price' => 'valor',
        'shipment_date' => 'data_embarque',
        'delivery_date' => 'data_entrega',
        'email_sent' => 'email_enviado',
    ];

    protected $hidden = [
        'codigo_pedido_produto',
        'pedido_id',
        'produto_id',
        'cod_origem',
        'semanas',
        'qtd_caixas',
        'total_kg',
        'valor_kg',
        'valor',
        'data_embarque',
        'data_entrega',
        'cd_empresa',
        'cd_faixa_fefo',
        'ds_chave_seara',
        'email_enviado',
        'CD_MOTIVO_CCL_PEDIDO',
        'DS_MOTIVO_CCL_PEDIDO',
    ];

    protected $appends = [
        'order_product_code',
        'order_id',
        'product_id',
        'origin_code',
        'weeks',
        'box_amount',
        'kg_total',
        'kg_price',
        'price',
        'shipment_date',
        'delivery_date',
        'email_sent',
    ];

    /**
     * @param mixed $value Value
     * @return void
     */
    public function setValorAttribute($value)
    {
        $this->attributes['valor'] = round($this->attributes['total_kg'] * $this->attributes['valor_kg'], 2);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'pedido_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function leadtime()
    {
        return $this->hasOne(LeadTime::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'codigo')->withTrashed();
    }

    /**
     * Scopes*
     **/

     /**
      * @param Builder $query Query builder
      * @return Builder
      */
    public function scopeIsPending($query)
    {
        return $query->where('status_id', Status::type(Status::PENDING)->id);
    }

    public function scopeNotCanceled($query)
    {
        return $query->where('status_id', '!=', Status::type(Status::CANCELED)->id);
    }

    /**
     * Return status
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * Return Rejection Motive
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rejectionMotives()
    {
        return $this->belongsTo(RejectionMotive::class);
    }

    public function getStatusDescriptionAttribute()
    {
        return __('status.' . Status::find($this->status_id)->name);
    }
}
