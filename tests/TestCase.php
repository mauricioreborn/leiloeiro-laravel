<?php

namespace Tests;

use App\Auth\User;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Company\Company;
use App\Core\Helpers\JWT;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, MigrateFreshSeedOnce;

    protected $user;

    protected $client;

    protected $jwt;

    protected $company;

    protected function setUpMockedUser($company = false): self
    {
        $this->company = $company;

        if ($company === false) {
            $this->company = Company::first();
        }

        $this->user = $this->company->users()->where('name', 'DummyUser')->first();

        return $this;
    }

    protected function setUpMockedJwt(): self
    {
        $this->setUpMockedUser();
        $this->jwt = JWT::encodeUser($this->user->id);
        return $this;
    }

    /**
     * Mocks a client for the authentication
     *
     * @param bool $company
     * @return $this
     */
    protected function setUpMockedClient($company = false)
    {
        $this->company = $company;

        if ($company === false) {
            $this->company = Company::first();
        }

        $this->client = Client::first();

        return $this;
    }

    /**
     * Mocks a client JWT token to be sent with every request
     *
     * @return $this
     */
    protected function setUpMockedJwtClient()
    {
        $this->setUpMockedClient();
        $this->jwt = JWT::encodeClient($this->client->id);
        return $this;
    }

    /**
     * Mocks a client JWT token to be sent with every request
     *
     * @return $this
     */
    protected function setUpMockedJwtHelipa()
    {
        $user = User::where(['email' => 'soukbot@souk.com.br'])->first();

        $this->jwt = JWT::encodeSA($user->id);
        return $this;
    }

    /**
     * Mocks a mobile client JWT token to be sent with every request
     *
     * @param ClientId $id Get Client ID
     * @return $this
     */
    protected function setUpMockJwtMobileClient($id)
    {
        $this->jwt = JWT::encodeClient($id);
        return $this;
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object $object     Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }


    /**
     * Method to set a user With a specifically Company
     *
     * @param Company $company company id
     * @return $this
     */
    public function setUpJwtUserWithSpecificallyCompanyId($company)
    {
        $this->setUpMockedUser($company);
        $this->jwt = JWT::encodeUser($this->user->id);
        return $this;
    }

    /**
     * Method to set a user With a specifically Company
     *
     * @param Company $company company id
     * @return $this
     */
    public function setUpJwtClientWithSpecificallyCompanyId(Company $company)
    {
        $this->setUpMockedClient($company);
        $this->jwt = JWT::encodeClient($this->client->id);
        return $this;
    }
}
