<?php

namespace Tests\Feature;

use App\Auth\User;
use App\Company\Company;
use App\Core\Helpers\JWT;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserControllerTest extends TestCase
{

    use DatabaseTransactions;

    protected $userReturnStructure = [
        'data' => [
            'id',
            'attributes' => [
                'name',
                'email',
                'root',
                'price',
                'featured',
                'company_id',
            ],
        ],
    ];

    protected $rootUser;
    protected $rootJwt;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpMockedUser();

        $this->rootUser = factory(User::class)->create([
            'root' => true,
            'company_id' => $this->company->id,
        ]);
        $this->rootJwt = JWT::encodeUser($this->rootUser->id);
    }

    /**
     * @return void
     */
    public function testListRequestWithoutTokenShouldReturnUnhautorized()
    {
        $response = $this->get('/api/v1/users');
        $response->assertStatus(401);
    }

    /**
     * @return void
     */
    public function testListRequestShouldReturnTestUsersCount()
    {
        $this->user->update(['root' => true]);

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")->get('/api/v1/users');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(4, 'data');
    }

    /**
     * @return void
     */
    public function testPostRequestShouldReturnUserObject()
    {
        $newUserData = [
            'name'                  => 'NewUser',
            'email'                 => 'newUser@souk.com.br',
            'password'              => '123qwe',
            'password_confirmation' => '123qwe',
            'root'                  => 1,
            'price'                 => 1,
            'featured'              => 1,
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")
            ->post('/api/v1/users', $newUserData);

        $response->assertStatus(201);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                'type'          => 'user',
                'id'            => $content['data']['id'],
                'attributes'    => [
                    'name'       => 'NewUser',
                    'email'      => 'newUser@souk.com.br',
                    'root'       => 1,
                    'price'      => 1,
                    'featured'   => 1,
                    'company_id' => $this->company->id,
                ],
                'relationships' => [],
            ],
        ], $content);
    }

    /**
     * @return void
     */
    public function testGetRequestFromSameCompanyShouldReturnUserObject()
    {
        $newUserData = [
            'name'       => 'NewUser',
            'email'      => 'newUser@souk.com.br',
            'password'   => '123qwe',
            'root'       => 1,
            'price'      => 1,
            'featured'   => 1,
            'company_id' => $this->company->id,
        ];
        $user        = factory(User::class)->create($newUserData);

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")->get("/api/v1/users/{$user->id}");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                'type'          => 'user',
                'id'            => $content['data']['id'],
                'attributes'    => [
                    'name'       => 'NewUser',
                    'email'      => 'newUser@souk.com.br',
                    'root'       => 1,
                    'price'      => 1,
                    'featured'   => 1,
                    'company_id' => $this->company->id,
                ],
                'relationships' => [],
            ],
        ], $content);
    }

    /**
     * @return void
     */
    public function testGetRequestFromDifferentCompaniesShouldReturnUnauthorized()
    {
        $newUserData = [
            'name'       => 'NewUser',
            'email'      => 'newUser@souk.com.br',
            'password'   => '123qwe',
            'root'       => 1,
            'price'      => 1,
            'featured'   => 1,
            'company_id' => 2,
        ];
        $user        = factory(User::class)->create($newUserData);

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")->get("/api/v1/users/{$user->id}");

        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @return void
     */
    public function testUpdateRequestFromSameCompanyShouldReturnUserObject()
    {
        $newUserData = [
            'name'     => 'NewUserUpdated',
            'email'    => 'newUser@souk.com.br',
            'password' => '123qwe',
            'root'     => 1,
            'price'    => 1,
            'featured' => 1,
        ];
        $user        = factory(User::class)->create(['name' => 'NewUser', 'company_id' => $this->company->id]);

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")
            ->put("/api/v1/users/{$user->id}", array_merge($newUserData, ['password_confirmation' => '123qwe']));

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                'type'          => 'user',
                'id'            => $content['data']['id'],
                'attributes'    => [
                    'name'       => 'NewUserUpdated',
                    'email'      => 'newUser@souk.com.br',
                    'root'       => 1,
                    'price'      => 1,
                    'featured'   => 1,
                    'company_id' => $this->company->id,
                ],
                'relationships' => [],
            ],
        ], $content);
    }

    /**
     * @return void
     */
    public function testUpdateRequestFromDifferentCompaniesShouldReturnUnauthorized()
    {
        $newUserData = [
            'name'     => 'NewUserUpdated',
            'email'    => 'newUser@souk.com.br',
            'password' => '123qwe',
            'root'     => 1,
            'price'    => 1,
            'featured' => 1,
        ];
        $user = factory(User::class)->create([
            'name' => 'NewUser',
            'company_id' => factory(Company::class)->create()
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")
            ->put("/api/v1/users/{$user->id}", array_merge($newUserData, ['password_confirmation' => '123qwe']));

        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @return void
     */
    public function testDeleteRequestFromSameCompanyShouldReturnSuccess()
    {
        $user = factory(User::class)->create(['company_id' => $this->company->id]);

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")
            ->delete("/api/v1/users/{$user->id}");

        $response->assertStatus(204);
    }

    /**
     * @return void
     */
    public function testDeleteRequestFromDifferentCompaniesShouldReturnUnauthorized()
    {
        $user = factory(User::class)->create(['company_id' => 2]);

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")
            ->delete("/api/v1/users/{$user->id}");

        $response->assertStatus(403);
    }

    public function testUserListRequestShouldReturnListOfUsersKeyedById()
    {
        $company2 = factory(Company::class)->create();

        factory(User::class, 3)->create([
            'company_id' => $this->user->company_id,
        ]);

        factory(User::class, 2)->create([
            'company_id' => $company2->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->rootJwt}")->get('/api/v1/users/list');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson(
            Company::find($this->user->company_id)
                ->users()
                ->pluck('name', 'id')
                ->toArray()
        );
    }
}
