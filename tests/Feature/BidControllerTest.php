<?php


namespace Tests\Feature;

use App\Auth\User;
use App\Bid\Bid;
use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Products\Product;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class BidControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpMockedJwt();
    }

    /**
     * Assures that a request will return 401 for this endpoint if
     * there isn't an authorization token attached to it
     *
     * @return null
     */
    public function testListRequestWithoutToken()
    {
        $response = $this->get('/api/v1/bids');
        $response->assertStatus(401);
    }

    /**
     * test empty list
     *
     * @return null
     */
    public function testEmptyList()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/bids');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['data' => []]);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * Method to get bid stats with a valid token
     *
     * @return null
     */
    public function testBidStatsRequestWithToken()
    {
        $client1 = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();
        $client3 = factory(Client::class)->create();

        $leadtime1 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client1->cnpj,
            'company_id' => $this->company->id
        ]);

        $leadtime2 = factory(LeadTime::class)->state('cannotPurchase')->create([
            'cnpj' => $client2->cnpj,
            'limit_hour' => 1700,
            'company_id' => $this->company->id
        ]);

        $leadtime3 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client3->cnpj,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime1->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5160.00,
            'best_before' => null,
        ]);

        $this->assertEquals(LeadTime::count(), 3);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        // Good bid
        factory(Bid::class)->create([
            'client_id' => $client1->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 4000,
            'kg_price' => 8.50,
            'price' => 8.50 * 4000,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client1->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Price below minimum acceptable
        factory(Bid::class)->create([
            'client_id' => $client2->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 100,
            'kg_price' => 3.50,
            'price' => 3.50 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client2->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Volume exceeding available volume
        factory(Bid::class)->create([
            'client_id' => $client3->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 10000,
            'kg_price' => 9.00,
            'price' => 9.00 * 10000,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Client not eligible because of logistics
        factory(Bid::class)->create([
            'client_id' => $client2->id,
            'origin_code' => $leadtime2->code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 100,
            'kg_price' => 9.00,
            'price' => 9.00 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client2->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Another good bid with same client
        factory(Bid::class)->create([
            'client_id' => $client3->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 200,
            'kg_price' => 9.30,
            'price' => 9.30 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Cancelled bids
        factory(Bid::class, 5)->create([
            'client_id' => $client3->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 200,
            'kg_price' => 9.30,
            'price' => 8.30 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::CANCELED)->id,
        ]);

        // Sold bids
        factory(Bid::class, 5)->create([
            'client_id' => $client3->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 200,
            'kg_price' => 9.30,
            'price' => 8.30 * 100,
            'duration' => Carbon::now()->addDays(2),
            'accept_date' => Carbon::now(),
            'user_id' => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::APPROVED)->id,
            'rejection_motives_id' => null,

        ]);

        $yesterdayBid = factory(Bid::class)->create([
            'product_code' => $fefo->product_code,
            'duration' => today()->subDay()->toDateString(),
            'weeks' => $fefo->weeks,
            'origin_code' => $fefo->leadtime_code,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $invalidFefoBid = factory(Bid::class)->create([
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => 999999,
            'origin_code' => $fefo->leadtime_code,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $this->assertEquals(Bid::count(), 17);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}"
        )->get("/api/v1/bids/stats");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'bids' => [
                        'open',
                    ],
                    'weight' => [
                        'open',
                        'accepted',
                    ],
                ],
            ],
        ]);

        $response->assertJsonFragment([
            'bids' => [
                'open' => 1,
            ],
        ]);

        $response->assertJsonFragment([
            'weight' => [
                'open' => '200',
                'accepted' => '1.000',
            ],
        ]);
    }

    /**
     * Method to get ineligible bids with sucess
     * @return void
     */
    public function testIneligibleRequestWithSuccess()
    {
        $client1 = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();

        $leadtime1 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client1->cnpj,
            'company_id' => $this->company->id
        ]);

        $leadtime2 = factory(LeadTime::class)->state('cannotPurchase')->create([
            'cnpj' => $client2->cnpj,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime1->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5160.00,
            'best_before' => null,
        ]);

        $this->assertEquals(LeadTime::count(), 2);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $product = Product::first();
        $product->users()->save(factory(User::class)->create());

        // Good bid
        factory(Bid::class)->create([
            'client_id' => $client1->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 4000,
            'kg_price' => 8.50,
            'price' => 8.50 * 4000,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client1->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Price below minimum acceptable
        factory(Bid::class)->create([
            'client_id' => $client2->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 100,
            'kg_price' => 3.50,
            'price' => 3.50 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client2->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $this->assertEquals(Bid::count(), 2);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/bids/ineligible');

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = $response->decodeResponseJson('data');

        $this->assertLessThan(
            $content['0']['attributes']['minimum_acceptable_price'],
            $content['0']['attributes']['kg_price']
        );

        $response->assertJsonCount(1, 'data');
    }

    /**
     * List bid filtered a company
     *
     * @return void
     */
    public function testGetRequestShouldReturnOnlyOneCompanyData()
    {
        factory(Bid::class, 10)->create([
            'company_id' => $this->company->id,
        ]);

        $company2 = factory(Company::class)->create();
        factory(Bid::class, 10)->create([
            'company_id' => $company2->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/bids");
        $responseContent = json_decode($response->getContent(), true);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(10, 'data');
    }

    /**
     * List bid filtered by array of status
     *
     * @return void
     */
    public function testGetBidsWithArrayStatus()
    {
        $statusCancelled = Status::type(Status::CANCELED)->id;
        $statusPending = Status::type(Status::PENDING)->id;
        $statusApproved = Status::type(Status::APPROVED)->id;

        $bid1 = factory(Bid::class)->create([
            'company_id' => $this->company->id,
            'status_id' => $statusCancelled,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id
        ]);

        $bid2 = factory(Bid::class)->create([
            'company_id' => $this->company->id,
            'status_id' => $statusPending,
        ]);

        $bid3 = factory(Bid::class)->create([
            'company_id' => $this->company->id,
            'status_id' => $statusApproved,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/bids?status_id[]={$statusCancelled}&status_id[]={$statusPending}");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(2, 'data');

        $response->assertJsonFragment(['status_id' => $statusCancelled]);
        $response->assertJsonFragment(['status_id' => $statusPending]);

        $response->assertJsonMissing(['status_id' => $statusApproved]);
    }

    /**
     * Test bid stats with user of another company
     *
     * @return void
     */
    public function testBidStatsWithUserOfAnotherCompany()
    {
        $client1 = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();
        $client3 = factory(Client::class)->create();

        $leadtime1 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client1->cnpj,
            'company_id' => $this->company->id,
        ]);

        $leadtime2 = factory(LeadTime::class)->state('cannotPurchase')->create([
            'cnpj' => $client2->cnpj,
            'company_id' => $this->company->id,
        ]);

        $leadtime3 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client3->cnpj,
            'company_id' => $this->company->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime1->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5160.00,
            'best_before' => null,
        ]);

        $this->assertEquals(LeadTime::count(), 3);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $product = Product::first();

        $product->users()->save(factory(User::class)->create());

        // Good bid
        factory(Bid::class)->create([
            'client_id' => $client1->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 4000,
            'kg_price' => 8.50,
            'price' => 8.50 * 4000,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client1->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Price below minimum acceptable
        factory(Bid::class)->create([
            'client_id' => $client2->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 100,
            'kg_price' => 3.50,
            'price' => 3.50 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client2->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Volume exceeding available volume
        factory(Bid::class)->create([
            'client_id' => $client3->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 10000,
            'kg_price' => 9.00,
            'price' => 9.00 * 10000,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Client not eligible because of logistics
        factory(Bid::class)->create([
            'client_id' => $client2->id,
            'origin_code' => $leadtime2->code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 100,
            'kg_price' => 9.00,
            'price' => 9.00 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client2->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Another good bid with same client
        factory(Bid::class)->create([
            'client_id' => $client3->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 200,
            'kg_price' => 9.30,
            'price' => 9.30 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Cancelled bids
        factory(Bid::class, 5)->create([
            'client_id' => $client3->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 200,
            'kg_price' => 9.30,
            'price' => 8.30 * 100,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id
        ]);

        // Sold bids
        factory(Bid::class, 5)->create([
            'client_id' => $client3->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 200,
            'kg_price' => 9.30,
            'price' => 8.30 * 100,
            'duration' => Carbon::now()->addDays(2),
            'accept_date' => Carbon::now(),
            'user_id' => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::APPROVED)->id,

        ]);

        $yesterdayBid = factory(Bid::class)->create([
            'product_code' => $fefo->product_code,
            'duration' => today()->subDay()->toDateString(),
            'weeks' => $fefo->weeks,
            'origin_code' => $fefo->leadtime_code,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $invalidFefoBid = factory(Bid::class)->create([
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => 999999,
            'origin_code' => $fefo->leadtime_code,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $this->assertEquals(Bid::count(), 17);

        $this->setUpJwtUserWithSpecificallyCompanyId(Company::skip(2)->first());

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}"
        )->get("/api/v1/bids/stats");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'bids' => [
                        'open',
                    ],
                    'weight' => [
                        'open',
                        'accepted',
                    ],
                ],
            ],
        ]);

        $response->assertJsonFragment([
            'bids' => [
                'open' => 0,
            ],
        ]);

        $response->assertJsonFragment([
            'weight' => [
                'open' => '0',
                'accepted' => '0',
            ],
        ]);
    }

    /**
     * Check price value not affected by tax percent in bid list
     *
     * @return void
     */
    public function testPriceValueWithTaxBidList()
    {
        factory(Bid::class)->create([
            'price' => 830,
            'tax_percent' => 5,
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/bids");
        $responseContent = json_decode($response->getContent(), true);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $this->assertEquals($responseContent['data'][0]['attributes']['price'], 830);
    }

    /**
     * Check price value not affected by tax percent in bid show
     *
     * @return void
     */
    public function testPriceValueWithTaxBidShow()
    {
        $bid = factory(Bid::class)->create([
            'price' => 830,
            'tax_percent' => 5,
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/bids/{$bid->id}");
        $responseContent = json_decode($response->getContent(), true);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $this->assertEquals($responseContent['data']['attributes']['price'], 830);
    }

    public function testDisapproveShouldReturnSuccess()
    {
        Notification::fake();
        $client = factory(Client::class)->create();

        $fefo = factory(Fefo::class)->create([
            'selling_price' => 20.00,
            'max_price' => 23.46,
            'min_price' => 21.35
        ]);

        $leadtime = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client->cnpj
        ]);

        $bid = factory(Bid::class)->create([
            'date' => $fefo->date,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'status_id' => Status::type(Status::PENDING)->id,
            'total_kg' => 12.00,
            'kg_price' => 14.00,
            'price' => 168.00,
            'client_id' => $client->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/bids/disapprove", ['bid_ids' => [$bid->id]]);

        $response->assertJsonFragment([
            'status' => 200,
            'title' => __('Mobile/bid.bids.disapprove.success')
        ]);

        $this->assertDatabaseHas('lances', [
            'id' => $bid->id,
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id,
        ]);
    }

    public function testBidClientWithMoreThanOneCompany()
    {
        $anotherCompany = factory(Company::class)->create();
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client->id,
            'company_id' => $this->company->id,
        ]);

        factory(ClientCompany::class)->create([
            'client_id' => $client->id,
            'company_id' => $anotherCompany->id
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $client->id,
            'price' => 830,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/bids/{$bid->id}?include=client");

        $responseContent = $response->decodeResponseJson();

        $this->assertEquals($responseContent['data']['relationships']['client']['relationships']['companies'][0]['id'],
            $this->company->id);
    }
}
