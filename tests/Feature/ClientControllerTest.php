<?php

namespace Tests\Feature;

use App\Client\ClientCompanyPayment;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Payment\Payment;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;

use Tests\TestCase;

use App\Client\Client;
use App\Client\Seller;
use App\ClientCompanies\ClientCompany;

class ClientControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $clientReturnStructure = [
        'data' => [
        'id',
        'attributes' => [
            'cnpj',
            'name',
            'status',
            'account_balance',
            'min_order',
            'address',
            'state',
            'city',
            'email',
            'phone',
            'typology',
            'created_at'
        ]]
    ];

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * testListRequestWithoutToken
     *
     * @return void
     */
    public function testListRequestWithoutToken()
    {
        $response = $this->get('/api/v1/clients');
        $response->assertStatus(401);
    }

    /**
     * testEmptyList
     *
     * @return void
     */
    public function testFreshRequestShouldReturnDummyClientOnly()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/clients');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(1, 'data');
    }

    public function testListWithUserCompany()
    {
        $client1 = Client::first();
        $client2 = factory(Client::class)->create();

        $company2 = Company::skip(2)->first();

        $payment1 = factory(Payment::class)->create();
        factory(ClientCompany::class)->create(['client_id' => $client2->id, 'company_id' => $company2->id]);

        $companyPayment1 = factory(CompanyPayment::class)->create([
            'payment_id' => $payment1->id,
            'company_id' => $this->company->id
        ]);

        $companyPayment2 = factory(CompanyPayment::class)->create(['company_id' => $company2->id]);

        factory(ClientCompanyPayment::class)->create([
            'client_id' => $client1->id,
            'company_payment_id' => $companyPayment1->id
        ]);

        factory(ClientCompanyPayment::class)->create([
            'client_id' => $client2->id,
            'company_payment_id' => $companyPayment2->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/clients?include=companyPayments.payment');

        $content = $response->decodeResponseJson('data.0.relationships.companyPayments.0');

        $this->assertEquals([
                "type" => "company_payment",
                "id" => $companyPayment1->id,
                "attributes" => [
                    "company_id" => auth()->user()->company_id,
                    "payment_id" => $payment1->id,
                ],
                "relationships" => [
                    "payments" => [
                        "type" => "payment",
                        "id" =>  $payment1->id,
                        "attributes" => [
                            "name" =>  $payment1->name,
                            "type" =>  $payment1->type,
                            "image" =>  $payment1->image,
                        ],
                    ],
                ],
            ], $content);

        $response->assertJsonCount(1, 'data.0.relationships.companyPayments');
    }

    /**
     * testAddClientWithSuccess
     *
     * @return void
     */
    public function testAddClientWithSuccess()
    {
        $client = factory(Client::class)->make([
            'password' => 12345,
        ]);

        $clientCompany = factory(ClientCompany::class)->make([
            'client_id' => $client->id
        ]);

        $seller = factory(Seller::class, 2)->make();

        $clientPayload = array_merge($client->makeVisible('password')->toArray(), $clientCompany->toArray());

        $payload = array_merge($clientPayload, ['seller' => $seller->toArray()]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/clients', $payload);


        $response->assertStatus(201);

        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                'type' => 'client',
                'id' => $content['data']['id'],
                'attributes' => [
                    'cnpj' => $payload['cnpj'],
                    'name' => $payload['name'],
                    'email' => $payload['email'],
                    'phone' => $payload['phone'],
                    'verified_mail' => $payload['verified_mail'],
                ],
                'relationships' => [
                    'sellers' => [
                        [
                            'type' => 'seller',
                            'attributes' => [
                                'name' => $payload['seller'][0]['name'],
                                'email' => $payload['seller'][0]['email'],
                                'phone' => $payload['seller'][0]['phone']
                            ]
                        ]
                    ],
                    'companies' => [
                        [
                            'type' => 'company',
                            'relationships' => [
                                'clientCompanies' => [
                                    'type' => 'client_companies',
                                    'attributes' => [
                                        'account_balance' => $payload['account_balance'],
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ], $content);
    }

    /**
     * Assures that a request will return 401 for this endpoint if
     * there isn't an authorization token attached to it
     *
     * @return void
     */
    public function testUpdateRequestWithoutToken()
    {
        $client = factory(Client::class)->create();
        $clientObj = $client->first();

        $response = $this->withHeader('Authorization', "")
            ->put("/api/v1/clients/$clientObj->id", $this->buildAClient());

        $response->assertStatus(401);

        if ($response->status() == 422) {
            Log::emergency($response->getContent());
        }
    }

    /**
     * testUpdateRequestWithToken
     *
     * @return void
     */
    public function testUpdateRequestWithToken()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
        ]);

        $seller = factory(Seller::class)->create();

        $client->seller()->save($seller);

        $client_id = $client->id;

        $payload = $client->toArray();
        unset($payload['updated_at']);
        $payload['verified_mail'] = $payload['email_verified'];

        unset($payload['email_verified']);

        $payload['name'] = 'Testing new client name';

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/clients/$client_id", $payload);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                'type' => 'client',
                'id' => $content['data']['id'],
                'attributes' => [
                    'cnpj' => $payload['cnpj'],
                    'name' => $payload['name'],
                    'email' => $payload['email'],
                    'phone' => $payload['phone'],
                    'verified_mail' => $payload['verified_mail'],
                ],
                'relationships' => [
                    'companies' => [
                        [
                            'type' => 'company',
                            'relationships' => [
                                'clientCompanies' => [
                                    'type' => 'client_companies',
                                    'attributes' => [
                                        'account_balance' => $clientCompany->account_balance,
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ], $content);
    }

    /**
     * Assures that a request will return 401 for this endpoint if
     * there isn't an authorization token attached to it
     *
     * @return void
     */
    public function testDeleteClientRequestWithoutToken()
    {
        $client = factory(Client::class)->create();

        $response = $this->delete("/api/v1/clients/$client->id");

        $response->assertStatus(401);
    }

    /**
     * testDeleteClientWithSuccess
     *
     * @return void
     */
    public function testDeleteClientWithSuccess()
    {
        $client = factory(Client::class)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->delete("/api/v1/clients/$client->id");

        $response->assertStatus(204);
    }

    /**
     * @dataProvider buildSearchParams
     *
     * @param string $param params
     * @param string $value value
     *
     * @return void
     */
    public function testGetClientWithParams($param, $value)
    {
        $this->buildAClient();
        $response = $this->withHeader('Authorization', "Bearer $this->jwt")
                ->get("/api/v1/clients?$param=$value");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * buildAClient
     *
     * @return void
     */
    protected function buildAClient()
    {
        $faker = \Faker\Factory::create('pt_BR');
        return [
            'cnpj' => $faker->cnpj(),
            'name' => $faker->firstName,
            'password' => md5($faker->randomElement(['12345', '123456', '1234567'])),
            'email_verified' => (int) 1,
            'email' => $faker->email(),
            'phone' => $faker->phone(),
            'seller' => [
                [
                    'name' => $faker->name(),
                    'email' => $faker->email(),
                    'phone' => $faker->phone(),
                ]
            ]
        ];
    }

    /**
     * Params to test if all queries are working
     *
     * @return array
     */
    public function buildSearchParams()
    {
        $faker = \Faker\Factory::create('pt_BR');
        return [
            [
                'cnpj',
                $faker->cnpj()
            ],
            [
                'name',
                $faker->firstName
            ],
            [
                'account_balance',
                $faker->randomFloat(2, 1, 10)

            ],
            [
                'min_order',
                $faker->randomFloat(2, 1, 10)
            ],
            [
                'address',
                $faker->address()
            ],
            [
                'state',
                $faker->state()
            ],
            [
                'city',
                $faker->city()
            ],
            [
                'email',
                $faker->email()
            ],
            [
                'phone',
                $faker->phoneNumber()
            ],
            [
                'typology',
                $faker->randomElement(['AS 5 - 9 CK', 'AS 5 - 10 CK', 'ARMAZENS E MERCEARIAS'])
            ]
        ];
    }
}
