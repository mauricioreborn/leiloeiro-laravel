<?php

namespace Tests\Feature;

use App\Client\Notifications\PasswordResetNotification;
use App\Auth\User;
use App\Company\Company;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Tests\TestCase;

class PasswordResetControllerTest extends TestCase
{
    use DatabaseTransactions;

    // CENÁRIOS TESTADOS
    // Testar solicitação de redefinição para usuário existente (deve enviar a notificação)
    // Testar solicitação de redefinição para usuário não existente (deve apenas gerar um log)
    // Testar verificação de token existente
    // Testar verificação de token para token inexistente
    // Testar atualização de senha cenário feliz
    // Testar atualização de senha cenários com informações erradas
    // Testar que o usuário consegue logar após atualizar a senha

    public function testPasswordResetRequestShouldResetPasswordAndSendUserNotification()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $user = factory(User::class)->create();

        $response = $this->post('/api/v1/password/reset', [
            'email' => $user->email,
        ]);

        $response->assertStatus(204);

        Notification::assertSentTo($user, PasswordResetNotification::class);
        
        $this->assertNotNull($user->fresh()->reset_token);
    }

    public function testPasswordResetRequestWithInvalidEmailShouldNotSendNotificationButReturnOk()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $response = $this->post('/api/v1/password/reset', [
            'email' => 'testeemail@souk.com.br',
        ]);

        $response->assertStatus(204);
        Notification::assertNothingSent();
    }

    public function testTokenVerificationRequestShouldReturnUserEmail()
    {
        $user = factory(User::class)->create(['reset_token' => Str::random(60)]);

        $response = $this->post('/api/v1/password/verify-token', [
            'token' => $user->reset_token,
        ]);

        $response->assertOk();
        $response->assertJson([
            'data' => [
                'attributes' => [
                    'email' => $user->email,
                ],
            ],
        ]);
    }

    public function testTokenVerificationRequestWithNonExistentTokenShouldThrowNotFoundException()
    {
        $response = $this->post('/api/v1/password/verify-token', [
            'token' => '123',
        ]);

        $response->assertNotFound();
    }

    public function testPasswordUpdateRequestShouldUpdateUsersPasswordAndInvalidateResetToken()
    {
        $user = factory(User::class)->create(['reset_token' => Str::random(60)]);

        $response = $this->put('/api/v1/password', [
            'token' => $user->reset_token,
            'email' => $user->email,
            'password' => '12345',
            'password_confirmation' => '12345',
        ]);

        $user = $user->fresh();

        $response->assertStatus(204);
        $this->assertNull($user->reset_token);
    }

    public function testPasswordUpdateRequestWithInvalidTokenEmailCombinationShouldThrowNotFoundException()
    {
        $user = factory(User::class)->create(['reset_token' => Str::random(60)]);

        $response = $this->put('/api/v1/password', [
            'token' => $user->reset_token,
            'email' => 'emailteste@souk.com.br',
            'password' => '12345',
            'password_confirmation' => '12345',
        ]);

        $user = $user->fresh();

        $response->assertNotFound();
        $this->assertNotNull($user->reset_token);
    }

    public function testUserCanLogInAfterResettingTheirPassword()
    {
        $company = factory(Company::class)->create();
        $user = factory(User::class)->create([
            'reset_token' => Str::random(60),
            'company_id' => $company->id,
        ]);

        $updateResponse = $this->put('/api/v1/password', [
            'token' => $user->reset_token,
            'email' => $user->email,
            'password' => '12345',
            'password_confirmation' => '12345',
        ]);

        $user = $user->fresh();

        $updateResponse->assertStatus(204);
        $this->assertNull($user->reset_token);

        $loginResponse = $this->post('/api/v1/login', [
            'username' => $user->email,
            'password' => '12345',
        ]);

        $loginResponse->assertOk();
    }
}
