<?php

namespace Tests\Feature\Invoice;

use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Company\CompanyPayment;
use App\Core\Entities\Status;
use App\Invoice\Invoice;
use App\Jobs\CancelInvoiceJob;
use App\Jobs\CaptureInvoiceJob;
use App\Jobs\IntegrateInvoiceJob;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Payment\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class InvoiceControllerTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedClient();

        $this->setUpMockedJwtHelipa();
    }

    /**
     * Capture invoice with same order amount
     *
     * @return void
     */
    public function testCaptureInvoiceWithOrderSomePrice()
    {
        Queue::fake();

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'tax_percent' => 2.55,
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
            'kg_total' => 10,
            'kg_price' => 10,
        ]);

        $invoice = factory(Invoice::class)->states('in_analysis')->create([
            'order_id' => $order->id,
            'client_card_id' => $clientCard->id,
            'total' => 102.62,
            'tax' => $order->tax_percent,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/invoices/order/{$order->id}/capture", []);

        $response->assertJson(['ok']);
        $response->assertStatus(200);

        //Queue::assertPushedTimes(CaptureInvoiceJob::class, 1);

        Queue::assertPushed(CaptureInvoiceJob::class, function ($job) use ($invoice) {
            return $job->getInvoiceId() == $invoice->id;
        });
    }

    /**
     * Capture invoice with hightest order amount
     *
     * @return void
     */
    public function testCaptureInvoiceWithOrderHighPrice()
    {
        Queue::fake();

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'tax_percent' => 2.55,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
            'kg_total' => 10,
            'kg_price' => 10,
        ]);

        $invoice = factory(Invoice::class)->states('in_analysis')->create([
            'order_id' => $order->id,
            'client_card_id' => $clientCard->id,
            'total' => 52.62,
            'tax' => $order->tax_percent,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/invoices/order/{$order->id}/capture", []);

        $response->assertJson(['ok']);
        $response->assertStatus(200);

        $lastInvoice = $order->invoices()->get()->last();

        //Queue::assertPushedTimes(CaptureInvoiceJob::class, 2);
        //Queue::assertPushedTimes(IntegrateInvoiceJob::class, 1);

        Queue::assertPushed(CaptureInvoiceJob::class, function ($job) use ($invoice) {
            return $job->getInvoiceId() == $invoice->id;
        });

        Queue::assertPushed(IntegrateInvoiceJob::class, function ($job) use ($lastInvoice) {
            return $job->getInvoiceId() == $lastInvoice->id;
        });

        Queue::assertPushed(CaptureInvoiceJob::class, function ($job) use ($lastInvoice) {
            return $job->getInvoiceId() == $lastInvoice->id;
        });

        $this->assertEquals(2, Invoice::count());

        $this->assertEquals($lastInvoice->total, 50.0);
        $this->assertEquals($lastInvoice->capture, 1);
    }

    /**
     * Capture invoice with lower order amount
     *
     * @return void
     */
    public function testCaptureInvoiceWithOrderLowerPrice()
    {
        Queue::fake();

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'tax_percent' => 2.55,
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
            'kg_total' => 8,
            'kg_price' => 10,
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::CANCELED)->id,
            'kg_total' => 50,
            'kg_price' => 10,
        ]);

        $invoice = factory(Invoice::class)->states('in_analysis')->create([
            'order_id' => $order->id,
            'client_card_id' => $clientCard->id,
            'total' => 100,
            'tax' => $order->tax_percent,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/invoices/order/{$order->id}/capture", []);

        $response->assertJson(['ok']);
        $response->assertStatus(200);

        $lastInvoice = $order->invoices()->get()->last();

        //Queue::assertPushedTimes(CancelInvoiceJob::class, 1);
        //Queue::assertPushedTimes(IntegrateInvoiceJob::class, 1);

        Queue::assertPushed(CancelInvoiceJob::class, function ($job) use ($invoice) {
            return $job->getInvoiceId() == $invoice->id;
        });

        Queue::assertPushed(IntegrateInvoiceJob::class, function ($job) use ($lastInvoice) {
            return $job->getInvoiceId() == $lastInvoice->id;
        });

        $this->assertEquals(2, Invoice::count());

        $this->assertEquals($lastInvoice->total, 82.09);
        $this->assertEquals($lastInvoice->capture, 1);
    }

    /**
     * Capture invoice with same order amount
     *
     * @return void
     */
    public function testCaptureInvoiceWithoutCreditCard()
    {
        Queue::fake();

        $payment = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => null,
            'tax_percent' => 0,
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
            'kg_total' => 8,
            'kg_price' => 10,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/invoices/order/{$order->id}/capture", []);

        $response->assertJson(['ok']);
        $response->assertStatus(200);

        Queue::assertNotPushed(CancelInvoiceJob::class);
        Queue::assertNotPushed(IntegrateInvoiceJob::class);
        Queue::assertNotPushed(CaptureInvoiceJob::class);
    }
}
