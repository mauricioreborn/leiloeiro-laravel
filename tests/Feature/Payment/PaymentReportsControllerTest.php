<?php

namespace Tests\Feature\Payment;

use App\Payment\MonthlyReport;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Mockery;
use Tests\TestCase;

class PaymentReportsControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Method to test the monthly reports
     * @return null
     */
    public function testListMonthlyReports()
    {
        Storage::shouldReceive('temporaryUrl')
            ->once()
            ->with('report.pdf', Mockery::any())
            ->andReturn('https://s3.aws.com/bucket/report.pdf');

        $monthlyReport = factory(MonthlyReport::class)->create([
            'url' => 'report.pdf'
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/payments/reports');

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'total' => $monthlyReport->total,
            'month' => intval($monthlyReport->month),
            'year' => intval($monthlyReport->year),
            'url' => 'https://s3.aws.com/bucket/report.pdf',
        ]);
    }
}
