<?php

namespace Tests\Feature\Payment;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Payment\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PaymentControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testGetCompanyPaymentsWithSuccess()
    {
        factory(Payment::class)->create([
            'type' => 'credit_card',
            'name' => 'Cartão de crédito'
        ]);

        factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => function () {
                return factory(CompanyPayment::class)->create([
                    'payment_id' => Payment::first()
                ]);
            }
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/payments');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            "type" => "company_payment",
            "name" => "Cartão de crédito",
            "type" => "credit_card"
        ]);
    }

    /**
     * Method to test companyPayment without success
     *
     * @return void
     */
    public function testGetCompanyPaymentsWithoutPayment()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/payments');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson([
            'data' => []
        ]);
    }

    /**
     * Method to test companyPayment with two companies
     *
     * @return void
     */
    public function testGetCompanyPaymentsWithTwoCompanies()
    {
        factory(Payment::class)->create([
            'type' => 'credit_card',
            'name' => 'Cartão de crédito'
        ]);

        $company = factory(Company::class)->create();

        factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => function () use ($company) {
                return factory(CompanyPayment::class)->create([
                    'payment_id' => Payment::first(),
                    'company_id' => $company->id
                ]);
            }
        ]);

        factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => function () {
                return factory(CompanyPayment::class)->create([
                    'payment_id' => Payment::first(),
                ]);
            }
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/payments');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(1, 'data');
    }


    /**
     * Method to attach a new companyPayment
     * @return void
     */
    public function testAttachANewClientCompanyPayment()
    {
        factory(Payment::class)->create([
            'type' => 'credit_card',
            'name' => 'Cartão de crédito'
        ]);

        $clientPayment =  factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => function () {
                return factory(CompanyPayment::class)->create([
                    'payment_id' => Payment::first(),
                ]);
            }
        ]);


        factory(ClientCompany::class)->create([
            'client_id' => $clientPayment->client_id,
        ]);

        $payment = factory(Payment::class)->create([
            'type' => 'slavery',
            'name' => 'Trabalho escravo'
        ]);

        $companyPayment =  factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id,
        ]);



        $params = [
            'company_payment_ids' => [
                $companyPayment->id,
                $clientPayment->company_payment_id
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->put(
            "/api/v1/payments/client/$clientPayment->client_id",
            $params
        );
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            "attached" =>[$companyPayment->id],
            "detached" => [],
            "updated" => []
        ]);
    }

    /**
     * Method to attach a new companyPayment
     * @return void
     */
    public function testUpdatePaymentValidatorClientCompanyPayment()
    {
        factory(Payment::class)->create([
            'type' => 'credit_card',
            'name' => 'Cartão de crédito'
        ]);

        $clientPayment =  factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => function () {
                return factory(CompanyPayment::class)->create([
                    'payment_id' => Payment::first(),
                ]);
            }
        ]);


        factory(ClientCompany::class)->create([
            'client_id' => $clientPayment->client_id,
        ]);

        $payment = factory(Payment::class)->create([
            'type' => 'slavery',
            'name' => 'Trabalho escravo'
        ]);

        factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id,
        ]);



        $params = [
            'company_payment_ids' => [
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->put(
            "/api/v1/payments/client/$clientPayment->client_id",
            $params
        );
        $response->assertStatus(422);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            'description' => ["O campo company payment ids é obrigatório."]
        ]);
    }

    /**
     * Method to attach a new companyPayment
     * @return void
     */
    public function testAttachedACompanyPaymentOfAnotherCompany()
    {
        factory(Payment::class)->create([
            'type' => 'credit_card',
            'name' => 'Cartão de crédito'
        ]);

        $clientPayment =  factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => function () {
                return factory(CompanyPayment::class)->create([
                    'payment_id' => Payment::first(),
                ]);
            }
        ]);


        factory(ClientCompany::class)->create([
            'client_id' => $clientPayment->client_id,
        ]);

        $payment = factory(Payment::class)->create([
            'type' => 'slavery',
            'name' => 'Trabalho escravo'
        ]);

        factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id,
        ]);



        $params = [
            'company_payment_ids' => [
                99
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->put(
            "/api/v1/payments/client/$clientPayment->client_id",
            $params
        );
        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            'description' => "company payment id 99 não encontrada"
        ]);
    }
}