<?php

namespace Tests\Feature\Iugu;

use App\Client\ClientCard;
use App\Core\Entities\Status;
use App\Invoice\Invoice;
use App\Jobs\SyncInvoiceJob;
use App\Order\Order;
use App\Order\OrderProduct;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class WebHookControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedClient();
    }

    /**
     * Method to test web hooks dispatch Sync Invoice Job
     *
     * @param string $event  Iugu event fired
     * @param string $status Status of event
     *
     * @dataProvider invoiceEvents
     *
     * @return void
     */
    public function testWebHookFiredSyncInvoiceJob($event, $status)
    {
        Queue::fake();

        $clientCard = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
            'client_card_id' => $clientCard->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $invoice = factory(Invoice::class)->states('pending')->create([
            'client_card_id' => $clientCard->id,
            'order_id' => $order->id
        ]);

        $response = $this->call('post', '/api/v1/iugu/webhooks', [
            'event' => $event,
            'data' => [
                'id' => $invoice->id,
                'status' => $status,
                'account_id' => '90035844E4C24E628CE25E5E00000000',
            ]
        ]);

        Queue::assertPushed(SyncInvoiceJob::class, function ($job) use ($invoice) {
            return $job->getInvoiceId() == $invoice->id;
        });
    }

    /**
     * Method to test web hooks event not fired
     *
     * @return void
     */
    public function testWebHookWithoutFiredJob()
    {
        Queue::fake();

        $response = $this->call('post', '/api/v1/iugu/webhooks', [
            'event' => 'withdraw_request.status_changed',
            'data' => [
                'id' => '123',
                'status' => Status::CANCELED,
                'account_id' => '90035844E4C24E628CE25E5E00000000',
            ]
        ]);

        Queue::assertNotPushed(SyncInvoiceJob::class);
    }

    /**
     * Method to return invoice events
     *
     * @return array
     */
    public function invoiceEvents()
    {
        return [
            [
                'invoice.created',
                Status::PENDING,
            ],
            [
                'invoice.status_changed',
                Status::CANCELED,
            ],
            [
                'invoice.refund',
                Status::REFUNDED,
            ],
            [
                'invoice.payment_failed',
                Status::REJECTED,
            ],
            [
                'invoice.dunning_action',
                Status::PAYMENT_IN_PROGRESS,
            ],
            [
                'invoice.due',
                Status::CANCELED,
            ],
            [
                'invoice.installment_released',
                Status::AUTHORIZED,
            ],
            [
                'invoice.released',
                Status::AUTHORIZED,
            ],
        ];
    }
}
