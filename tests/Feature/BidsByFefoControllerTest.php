<?php


namespace Tests\Feature;

use App\Auth\User;
use App\Bid\Bid;
use App\Client\Client;
use App\Company\Company;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\LeadTime\Services\LeadTimeService;
use App\Products\Product;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class BidsByFefoControllerTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Asserts the given client will only see results from the companies
     * they are related to.
     *
     * @return null
     */
    public function testBidFefoRequestShouldReturnResultsFromTheirCompanyOnly()
    {
        $client1 = factory(Client::class)->create();
        $company2 = factory(Company::class)->create();

        $client1->companies()->save($company2);

        $leadtime1 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client1->cnpj,
            'company_id' => $this->company->id,
        ]);

        $leadtime2 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client1->cnpj,
            'company_id' => $company2->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
            'product_code' => function () {
                return factory(Product::class)->create([
                    'company_id' => $this->company->id,
                    'name' => 'good_meat'
                ])->code;
            }
        ]);

        $fefo2 = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime2->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5160.00,
            'best_before' => null,
            'company_id' => $company2->id,
            'product_code' => function () use ($company2) {
                return factory(Product::class)->create([
                    'company_id' => $company2->id,
                    'name' => 'evil_meat'
                ])->code;
            }
        ]);

        factory(Bid::class)->create([
            'client_id'    => $client1->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 4000,
            'kg_price'     => 8.50,
            'price'        => 8.50 * 4000,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client1->id,
            'company_id'   => $this->company->id,
            'status_id'    => Status::type(Status::PENDING)->id,
        ]);

        factory(Bid::class)->create([
            'client_id'    => $client1->id,
            'origin_code'  => $fefo2->leadtime_code,
            'product_code' => $fefo2->product_code,
            'date'         => $fefo2->date,
            'weeks'        => $fefo2->weeks,
            'box_amount'   => 1,
            'total_kg'     => 4000,
            'kg_price'     => 8.50,
            'price'        => 8.50 * 4000,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client1->id,
            'company_id'   => $company2->id,
            'status_id'    => Status::type(Status::PENDING)->id,
        ]);

        $product = Product::first();
        $product->users()->save(factory(User::class)->create());

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/bids/fefo?eligibleOnly=true');

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonCount(1, 'data');

        $content = $response->decodeResponseJson();
        $this->assertEquals(count($content['data']), 1);
        $this->assertNotEquals($content['data'][0]['attributes']['product_name'], 'evil_meat');
        $response->assertJsonMissing([
            "product_name" => "evil_meat"
        ]);

    }


    /**
     * Method to get bid fefo with a valid token
     *
     * @return null
     */
    public function testBidFefoRequestWithToken()
    {
        $client1 = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();
        $client3 = factory(Client::class)->create();

        $leadtime1 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client1->cnpj,
            'company_id' => $this->company->id,
        ]);

        $leadtime2 = factory(LeadTime::class)->state('cannotPurchase')->create([
            'cnpj' => $client2->cnpj,
            'company_id' => $this->company->id,
        ]);

        $leadtime3 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client3->cnpj,
            'company_id' => $this->company->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
        ]);

        $this->assertEquals(LeadTime::count(), 3);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $product = Product::first();
        $product->users()->save(factory(User::class)->create());

        // Good bid
        factory(Bid::class)->create([
            'client_id'    => $client1->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 4000,
            'kg_price'     => 8.50,
            'price'        => 8.50 * 4000,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client1->id,
            'company_id'   => $this->company->id,
            'status_id'    => Status::type(Status::PENDING)->id,
        ]);

        // Price below minimum acceptable
        factory(Bid::class)->create([
            'client_id'    => $client2->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 100,
            'kg_price'     => 3.50,
            'price'        => 3.50 * 100,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client2->id,
            'company_id'   => $this->company->id,
            'status_id'    => Status::type(Status::PENDING)->id,
        ]);

        // Volume exceeding available volume
        factory(Bid::class)->create([
            'client_id'    => $client3->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 10000,
            'kg_price'     => 9.00,
            'price'        => 9.00 * 10000,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client3->id,
            'company_id'   => $this->company->id,
            'status_id'    => Status::type(Status::PENDING)->id,
        ]);

        // Client not eligible because of logistics
        factory(Bid::class)->create([
            'client_id'    => $client2->id,
            'origin_code'  => $leadtime2->code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 100,
            'kg_price'     => 9.00,
            'price'        => 9.00 * 100,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client2->id,
            'company_id'   => $this->company->id,
            'status_id'    => Status::type(Status::PENDING)->id,
        ]);

        // Another good bid
        factory(Bid::class)->create([
            'client_id'    => $client3->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 100,
            'kg_price'     => 9.00,
            'price'        => 9.00 * 100,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client3->id,
            'company_id'   => $this->company->id,
            'status_id'    => Status::type(Status::PENDING)->id,
        ]);

        // Another good bid with same client
        factory(Bid::class)->create([
            'client_id'    => $client3->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 200,
            'kg_price'     => 9.30,
            'price'        => 9.30 * 200,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client3->id,
            'company_id'   => $this->company->id,
            'status_id'    => Status::type(Status::PENDING)->id,
        ]);

        $this->assertEquals(Bid::count(), 6);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/bids/fefo?eligibleOnly=true');

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonCount(1, 'data');

        $response->assertJsonFragment([
            'weeks'              => 5,
            'product_code'       => (int) $product->code,
            'product_name'       => $product->name,
            'product_picture'    => 'souk-company/' . $product->company_id .'/'. $product->picture,
            'product_type'       => $product->type,
            'product_seller'     => $product->users->first()->name,
            'origin_code'        => $leadtime1->code,
            'cd'                 => 'SAO PAULO - SP',
            'faixa_fefo'         => $fefo->faixa_fefo,
            'fefo_volume'        => 5160,
            'fefo_selling_price' => 9.32,
            'fefo_featured'      => true,
            'bids_volume'        => 14300,
            'bids_kg_price'      => 8.86, // (1860 + 900 + 90000 + 34000) / 14300 = 8,864335664
            'bids_price'         => 126760,
            'bids'               => 4,
            'clients'            => 2,
        ]);

        $response->assertJsonStructure([
            'data' => [
                [
                    'type',
                    'attributes' => [
                        'weeks',
                        'product_code',
                        'product_name',
                        'product_picture',
                        'product_type',
                        'product_seller',
                        'origin_code',
                        'cd',
                        'faixa_fefo',
                        'fefo_volume',
                        'fefo_selling_price',
                        'fefo_featured',
                        'bids_volume',
                        'bids_kg_price',
                        'bids_price',
                        'bids',
                        'clients',
                        'client_ids',
                    ],
                ],
            ],
        ]);
    }


    /**
     * Method to get bid fefo with a valid token
     *
     * @return null
     */
    public function testBidFefoRequestWithClientWithoutLeadtimeShouldIgnoreClient()
    {
        $client1 = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();
        $client3 = factory(Client::class)->create();

        $leadtime1 = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client1->cnpj,
            'company_id' => $this->company->id
        ]);

        $leadtime2 = factory(LeadTime::class)->state('cannotPurchase')->create([
            'cnpj' => $client2->cnpj,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
        ]);

        $product = Product::first();
        $product->users()->save(factory(User::class)->create());

        // Good bid
        factory(Bid::class)->create([
            'client_id'    => $client1->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 100,
            'kg_price'     => 8.50,
            'price'        => 8.50 * 100,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client1->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Price below minimum acceptable
        factory(Bid::class)->create([
            'client_id'    => $client2->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 100,
            'kg_price'     => 3.50,
            'price'        => 3.50 * 100,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client2->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Volume exceeding available volume
        factory(Bid::class)->create([
            'client_id'    => $client3->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 10000,
            'kg_price'     => 9.00,
            'price'        => 9.00 * 10000,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Client not eligible because of logistics
        factory(Bid::class)->create([
            'client_id'    => $client2->id,
            'origin_code'  => $leadtime2->code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 100,
            'kg_price'     => 9.00,
            'price'        => 9.00 * 100,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client2->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Another good bid
        factory(Bid::class)->create([
            'client_id'    => $client3->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 100,
            'kg_price'     => 9.00,
            'price'        => 9.00 * 100,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client3->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Another good bid with same client
        factory(Bid::class)->create([
            'client_id'    => $client1->id,
            'origin_code'  => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date'         => $fefo->date,
            'weeks'        => $fefo->weeks,
            'box_amount'   => 1,
            'total_kg'     => 200,
            'kg_price'     => 9.30,
            'price'        => 9.30 * 100,
            'duration'     => Carbon::now()->addDays(2),
            'user_id'      => $client1->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/bids/fefo?eligibleOnly=true');

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonCount(1, 'data');

        $response->assertJsonFragment([
            'weeks'              => 5,
            'product_code'       => (int) $product->code,
            'product_name'       => $product->name,
            'product_picture'    => 'souk-company/' . $product->company_id .'/'. $product->picture,
            'product_type'       => $product->type,
            'product_seller'     => $product->users->first()->name,
            'origin_code'        => $leadtime1->code,
            'cd'                 => 'SAO PAULO - SP',
            'faixa_fefo'         => $fefo->faixa_fefo,
            'fefo_volume'        => 5160,
            'fefo_selling_price' => 9.32,
            'fefo_featured'      => true,
            'bids_volume'        => 300,
            'bids_kg_price'      => 5.93,
            'bids_price'         => 1780,
            'bids'               => 2,
            'clients'            => 1,
        ]);
    }
}
