<?php


namespace Tests\Feature;

use App\Bid\Bid;
use App\Bid\Services\BidService;
use App\Client\ClientCompanyPayment;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Company\Services\CompanySettingService;
use App\Company\Company;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class FefoBidsControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Testes realizado com lances que aceita volume parcial.
     * - Volume acima do limite e nao aceita parcial
     * - Volume valido e nao aceita parcial
     * - Lance sem logistica e aceita parcial, can_box_change = false
     * - Lance valido e aceita parcial, sendo o primeiro valido da lista
     *
     * @return null
     */
    public function testBidsByFefoListWithInvalidBids()
    {
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'min_order' => 5,
            'client_id' => $client->id
        ]);

        $clientRJ = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'min_order' => 5,
            'client_id' => $clientRJ->id
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);
        $this->createLeadTime($clientRJ->cnpj, 'CD RJ', 0);

        $product = factory(Product::class)->create([
            'weight' => 5
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5000.00,
            'best_before' => null,
        ]);

        // Good bid and accept partial
        $bid = $this->createBid($fefo, 10, 8.5, 50, 1, $client->id);

        // Volume exceeding available volume but not accept partial
        $bidVolumeNotAvailable = $this->createBid($fefo, 2000, 9.00, 10000, 0, $client->id);

        // Another good bid
        $bidWithoutAcceptPartial = $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        // Client not eligible because of logistics
        $bidWithoutLogistic = $this->createBid($fefo, 20, 9.00, 100, 1, $clientRJ->id);

        // Bid without acceptable price: 7.19 * 0,8 = 5.75
        $this->createBid($fefo, 10, 5.74, 50, 1, $client->id);

        $bidMinAcceptablePercent = $this->company
            ->companySettings
            ->firstWhere('setting', '=', 'bidMinAcceptablePercent');

        $bidMinAcceptablePercent->value = 20;
        $bidMinAcceptablePercent->save();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo/{$fefo->id}/bids" );

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'type',
                    'relationships',
                    'attributes' => [
                        'client_id',
                        'date',
                        'origin_code',
                        'product_code',
                        'weeks',
                        'box_amount',
                        'weight',
                        'kg_price',
                        'price',
                        'partial_accept',
                        'duration',
                        'user_id',
                        'accept_date',
                        'can_purchase',
                        'original_box_amount',
                        'original_weight',
                        'partial_purchase',
                        'discount_percentage',
                        'original_price',
                        'status_id',
                        'rejection_motives_id',
                    ],
                ],
            ],
        ]);

        $this->bidTestResponse($response, $bid, [
            "box_amount" => 10,
            "weight" => 50,
            "original_weight" => 50,
            "kg_price" => '8.50',
            "price" => '425.00',
            "partial_accept" => 1,
            "can_purchase" => true,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 8.5),
        ]);

        $this->bidTestResponse($response, $bidVolumeNotAvailable, [
            "box_amount" => 2000,
            "weight" => 10000,
            "original_weight" => 10000,
            "kg_price" => '9.00',
            "price" => '90000.00',
            "partial_accept" => 0,
            "can_purchase" => false,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 9),
        ]);

        $this->bidTestResponse($response, $bidWithoutAcceptPartial, [
            "box_amount" => 20,
            "weight" => 100,
            "original_weight" => 100,
            "kg_price" => '9.00',
            "price" => '900.00',
            "partial_accept" => 0,
            "can_purchase" => true,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 9),
        ]);

        $this->bidTestResponse($response, $bidWithoutLogistic, [
            "box_amount" => 20,
            "weight" => 100,
            "original_weight" => 100,
            "kg_price" => '9.00',
            "price" => '900.00',
            "partial_accept" => 1,
            "can_purchase" => false,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 9),
        ]);

        $this->bidTestResponse($response, $bid, [
            "box_amount" => 10,
            "weight" => 50,
            "original_weight" => 50,
            "kg_price" => '5.74',
            "price" => '287.00',
            "partial_accept" => 1,
            "can_purchase" => false,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 5.74),
        ]);
    }

    /**
     * Testes realizado com lances que aceita volume parcial.
     * - Volume acima do estoque fefo, aceita parcial sendo o primeiro da lista valido
     * - Volume valido com menor valor, aceita parcial e can_box_change = false
     * - Volume valido com maior valor, nao aceita parcial
     *
     * @return null
     */
    public function testBidsByFefoListWithInvalidVolume()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'min_order' => 5,
            'client_id' => $client->id
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);

        $product = factory(Product::class)->create([
            'weight' => 5
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);

        // Good bid with low value and can_purchase = true
        $bid = $this->createBid($fefo, 10, 7.0, 50, 1, $client->id);

        // Volume exceeding available volume but not accept partial
        $bidVolumeNotAvailable = $this->createBid($fefo, 2000, 7.50, 10000, 1, $client->id);

        // Another good bid
        $bidWithoutAcceptPartial = $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo/{$fefo->id}/bids" );

        $this->bidTestResponse($response, $bidWithoutAcceptPartial, [
            "box_amount" => 20,
            "weight" => 100,
            "original_weight" => 100,
            "kg_price" => '9.00',
            "price" => '900.00',
            "partial_accept" => 0,
            "can_purchase" => true,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 9),
        ]);

        $this->bidTestResponse($response, $bidVolumeNotAvailable, [
            "box_amount" => 40,
            "weight" => 200,
            "original_weight" => 10000,
            "kg_price" => '7.50',
            "price" => '1500.00',
            "partial_accept" => 1,
            "can_purchase" => true,
            "partial_purchase" => true,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 7.5),
        ]);

        $this->bidTestResponse($response, $bid, [
            "box_amount" => 10,
            "weight" => 50,
            "original_weight" => 50,
            "kg_price" => '7.00',
            "price" => '350.00',
            "partial_accept" => 1,
            "can_purchase" => false,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 7),
        ]);
    }

    /**
     * Testes realizado com lances que aceita volume parcial com valor invalido
     *
     * @return null
     */
    public function testBidsByFefoListWithInvalidPrice()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'min_order' => 5,
            'client_id' => $client->id
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);

        $product = factory(Product::class)->create([
            'weight' => 5
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5160.00,
            'best_before' => null,
        ]);

        // Good bid with  can_purchase = true
        $bid = $this->createBid($fefo, 10, 7.0, 50, 0, $client->id);

        // KG Price invalid
        $bidKgPriceInvalid = $this->createBid($fefo, 20, 5.00, 100, 1, $client->id);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo/{$fefo->id}/bids" );

        $this->bidTestResponse($response, $bid, [
            "box_amount" => 10,
            "weight" => 50,
            "original_weight" => 50,
            "kg_price" => '7.00',
            "price" => '350.00',
            "partial_accept" => 0,
            "can_purchase" => true,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 7),
        ]);

        $this->bidTestResponse($response, $bidKgPriceInvalid, [
            "box_amount" => 20,
            "weight" => 100,
            "original_weight" => 100,
            "kg_price" => '5.00',
            "price" => '500.00',
            "partial_accept" => 1,
            "can_purchase" => false,
            "partial_purchase" => false,
            "original_price" => (string) $fefo->max_price,
            "discount_percentage" => (new BidService)->calcDiscountPercentage($fefo->max_price, 5),
        ]);
    }

    /**
     * Aceitar lances com volume parcial
     *
     * @return null
     */
    public function testAcceptBidPartialPurchase()
    {
        Notification::fake();
        $this->user->root = 1;
        $this->user->save();

        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'min_order' => 5,
            'client_id' => $client->id
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);

        $product = factory(Product::class)->create([
            'weight' => 5
        ]);

        $product->users()->save($this->user);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);

        // Another good bid
        $bidWithoutAcceptPartial = $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        // Volume exceeding available volume but accept partial
        $bidVolumeNotAvailable = $this->createBid($fefo, 2000, 7.50, 10000, 1, $client->id);

        $invalidBid = $this->createBid($fefo, 10, 7.0, 50, 1, $client->id);

        $approve = [
            'bid_ids' => [
                $invalidBid->id,
                $bidVolumeNotAvailable->id,
                $bidWithoutAcceptPartial->id
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/fefo/{$fefo->id}/bids/approve", $approve);

        $response->assertJsonFragment([
            'status' => 200,
            'bids' => [
                $bidWithoutAcceptPartial->id,
                $bidVolumeNotAvailable->id,
            ]
        ]);

        $fefo = $fefo->fresh();

        $invalidBid = $invalidBid->fresh();
        $bidVolumeNotAvailable = $bidVolumeNotAvailable->fresh();
        $bidWithoutAcceptPartial = $bidWithoutAcceptPartial->fresh();

        $this->assertEquals($fefo->volume, 0);

        $this->assertEquals($bidVolumeNotAvailable->weight, 200);
        $this->assertEquals($bidVolumeNotAvailable->box_amount, 40);
        $this->assertEquals($bidVolumeNotAvailable->price, '1500.00');
        $this->assertEquals($bidVolumeNotAvailable->accept_date, now()->toDateString());
        $this->assertEquals($bidVolumeNotAvailable->nao_lido, 1);
        $this->assertEquals($bidVolumeNotAvailable->user_id, $this->user->id);
        $this->assertEquals($bidVolumeNotAvailable->status_id, Status::type(Status::APPROVED)->id);
        $this->assertEquals($bidVolumeNotAvailable->bidLogs->count(), 1);

        $this->assertEquals($bidWithoutAcceptPartial->weight, 100);
        $this->assertEquals($bidWithoutAcceptPartial->box_amount, 20);
        $this->assertEquals($bidWithoutAcceptPartial->accept_date, now()->toDateString());
        $this->assertEquals($bidWithoutAcceptPartial->nao_lido, 1);
        $this->assertEquals($bidWithoutAcceptPartial->user_id, $this->user->id);
        $this->assertEquals($bidWithoutAcceptPartial->status_id, Status::type(Status::APPROVED)->id);
        $this->assertEquals($bidWithoutAcceptPartial->bidLogs->count(), 0);

        $this->assertEquals($invalidBid->status_id, Status::type(Status::PENDING)->id);
        $this->assertEquals($invalidBid->bidLogs->count(), 0);
    }

    /**
     * Rejeitar lances
     *
     * @return null
     */
    public function testDisapproveBid()
    {
        Notification::fake();
        $this->user->root = 1;
        $this->user->save();

        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'min_order' => 5,
            'client_id' => $client->id
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);

        $product = factory(Product::class)->create([
            'weight' => 5
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);

        $bid1 = $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        $bid2 = $this->createBid($fefo, 2000, 7.50, 10000, 1, $client->id);

        $bid3 = $this->createBid($fefo, 2000, 7.49, 10000, 1, $client->id);

        $bid4 = $this->createBid($fefo, 2000, 7.48, 10000, 1, $client->id);

        $bid5 = $this->createBid($fefo, 2000, 7.47, 10000, 1, $client->id);

        $disapprove = [
            'bid_ids' => [
                $bid1->id,
                $bid4->id,
                $bid5->id
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/fefo/{$fefo->id}/bids/disapprove", $disapprove);

        $response->assertJsonFragment([
            'status' => 200,
            'bids' => [
                $bid4->id,
                $bid5->id,
            ]
        ]);

        $bid1 = $bid1->fresh();
        $bid2 = $bid2->fresh();
        $bid3 = $bid3->fresh();
        $bid4 = $bid4->fresh();
        $bid5 = $bid5->fresh();

        $this->assertEquals($bid4->accept_date, now()->toDateString());
        $this->assertEquals($bid4->nao_lido, 1);
        $this->assertEquals($bid4->user_id, $this->user->id);
        $this->assertEquals($bid4->status_id, Status::type(Status::CANCELED)->id);
        $this->assertEquals($bid4->rejection_motives_id, RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id);

        $this->assertEquals($bid5->accept_date, now()->toDateString());
        $this->assertEquals($bid5->nao_lido, 1);
        $this->assertEquals($bid5->user_id, $this->user->id);
        $this->assertEquals($bid5->status_id, Status::type(Status::CANCELED)->id);
        $this->assertEquals($bid5->rejection_motives_id, RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id);

        $this->assertEquals($bid1->status_id, Status::type(Status::PENDING)->id);
        $this->assertEquals($bid2->status_id, Status::type(Status::PENDING)->id);
        $this->assertEquals($bid3->status_id, Status::type(Status::PENDING)->id);
    }

    /**
     * Create a bid
     *
     * @param Fefo  $fefo          Fefo Entity
     * @param int   $boxAmount     box Amount
     * @param float $price         Kg price
     * @param float $weight        Total Weight
     * @param int   $partialAccept Partial volume
     * @param int   $clientId      Client ID
     * @param int   $leadTimeCode  LeadTime Code
     * @param int   $companyId     Company ID
     *
     * @return App\Bid\Bid
     **/
    protected function createBid(
        Fefo $fefo,
        int $boxAmount,
        float $price,
        float $weight,
        int $partialAccept,
        int $clientId,
        int $leadTimeCode = null,
        int $companyId = null
    ) {
        return factory(Bid::class)->create([
            'client_id' => $clientId,
            'origin_code' => $leadTimeCode ?? $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => $boxAmount,
            'total_kg' => $weight,
            'kg_price' => $price,
            'price' => $price * $weight,
            'duration' => now()->addDays(2),
            'user_id' => null,
            'company_id' => $companyId ?? $this->company->id,
            'partial_accept' => $partialAccept,
            'status_id' => Status::type(Status::PENDING)->id,
            'rejection_motives_id' => null,
            'client_company_payment_id' => factory(ClientCompanyPayment::class)
                ->states('companyCredit')->create(['client_id' => $clientId])->id,
        ]);
    }

    /**
     * create LeadTime
     *
     * @param  string $cnpj      CNPJ
     * @param  string $name      Name
     * @param  int    $operate   Operate
     * @param  string $code      code
     * @param  string $companyId Company Id
     *
     * @return App\LeadTime\LeadTime
     */
    protected function createLeadTime(string $cnpj, string $name, int $operate, $code = null, $companyId = null)
    {
        $params = [
            'name' => $name,
            'cnpj' => $cnpj,
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => $operate,
            'tuesday' => $operate,
            'wednesday' => $operate,
            'thursday' => $operate,
            'friday' => $operate,
            'saturday' => $operate,
            'sunday' => $operate,
            'operates_saturday' => $operate,
            'operates_sunday' => $operate,
            'company_id' => $companyId ?? $this->company->id
        ];

        if ($code) {
            $params['code'] = $code;
        }

        return factory(LeadTime::class)->create($params);
    }

    /**
     * test bid response in json response
     * @param  [type] $response [description]
     * @param  Bid    $bid      [description]
     * @param  array  $values   [description]
     * @return [type]           [description]
     */
    protected function bidTestResponse($response, Bid $bid, array $values)
    {
        $company    = $bid->company;
        $periodView = $company !== null ? $company->settings['periodView'] : false;
        $periodLabel = $periodView ?
            $bid->weeks . ' ' . trans_choice("companySetting.{$periodView}", $bid->weeks) : false;

        $response->assertJsonFragment([
            'attributes' => [
                'validade' => $periodLabel?: null,
                "client_id" => $bid->client_id,
                "date" => $bid->date,
                "origin_code" => $bid->origin_code,
                "product_code" => $bid->product_code,
                "weeks" => $bid->weeks,
                "box_amount" => array_get($values, 'box_amount', $bid->box_amount),
                "weight" => array_get($values, 'weight', $bid->weight),
                "kg_price" => array_get($values, 'kg_price', $bid->kg_price),
                "price" => array_get($values, 'price', $bid->price),
                "partial_accept" => array_get($values, 'partial_accept', $bid->partial_accept),
                "duration" => $bid->duration->toDateString(),
                "user_id" => $bid->user_id,
                "status_id" => $bid->status_id,
                "rejection_motives_id" => $bid->rejection_motives_id,
                "accept_date" => $bid->accept_date,
                'original_box_amount' => $bid->original_box_amount,
                "can_purchase" => array_get($values, 'can_purchase', $bid->can_purchase),
                "original_weight" => array_get($values, 'original_weight', $bid->original_weight),
                "partial_purchase" => array_get($values, 'partial_purchase', $bid->partial_purchase),
                'original_price' => $values['original_price'],
                'discount_percentage' => "{$values['discount_percentage']}%",
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testAcceptBidShouldGenerateAnOrder()
    {
        Queue::fake();
        $this->user->root = 1;
        $this->user->save();

        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'min_order' => 5,
            'account_balance' => 999.00,
            'client_id' => $client->id
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);

        $product = factory(Product::class)->create([
            'weight' => 5
        ]);

        $product->users()->save($this->user);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'company_id' => $clientCompany->company_id,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);

        // Another good bid
        $bid = $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        $approve = [
            'bid_ids' => [
                $bid->id
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/fefo/{$fefo->id}/bids/approve", $approve);

        $response->assertJsonFragment([
            'status' => 200,
            'bids' => [
                $bid->id,
            ]
        ]);

        $fefo = $fefo->fresh();
        $bid = $bid->fresh();

        $orders = Order::where('bid_id', $bid->id)->get();
        foreach ($orders as $order) {
            $this->assertEquals($order->status_id, Status::type(Status::PENDING)->id);
        }
        $this->assertCount(1, $orders);

        $this->user->root = 0;
        $this->user->save();
    }

    /**
     * @return void
     */
    public function testFirstBidFromClientShouldShowFirstPurchaseLabel()
    {
        $client = factory(Client::class)->create();
        $clientCompany = factory(ClientCompany::class)->create([
            'client_id' => $client->id
        ]);
        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);
        $product = factory(Product::class)->create();

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);
        $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo/{$fefo->id}/bids");

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'labels' => [
                [
                    'type' => 'first_purchase',
                    'content' => __('fefoBids.labels.first_purchase'),
                ],
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testFirstBidFromClientShouldConsiderCompany()
    {
        $client = factory(Client::class)->create();
        $company2 = factory(Company::class)->create();
        factory(ClientCompany::class)->create([
            'client_id' => $client->id
        ]);
        factory(ClientCompany::class)->create([
            'client_id' => $client->id,
            'company_id' => $company2->id,
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);
        $product = factory(Product::class)->create();
        factory(Order::class)->create([
            'client_id' => $client->id,
            'company_id' => $company2->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);
        $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo/{$fefo->id}/bids");

        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data.0.relationships.labels');
    }

    /**
     * Test accept bid with reduce account balance
     *
     * @return void
     **/
    public function testAcceptBidShouldReduceBalanceAccount()
    {
        Notification::fake();
        (new CompanySettingService())->createReduceAccountBalanceApproveBid($this->company);

        $this->createApprovedFefoBid();

        $this->assertDatabaseHas('client_companies', [
            'company_id' => $this->company->id,
            'client_id' => $this->client->id,
            'account_balance' => 4100
        ]);
    }

    /**
     * Test accept bid without reduce account balance
     *
     * @return void
     **/
    public function testAcceptBidShouldWithoutReduceBalanceAccount()
    {
        Notification::fake();
        (new CompanySettingService())->createReduceAccountBalanceApproveBid($this->company, false);

        $this->createApprovedFefoBid();

        $this->assertDatabaseHas('client_companies', [
            'company_id' => $this->company->id,
            'client_id' => $this->client->id,
            'account_balance' => 5000
        ]);
    }

    /**
     * Test accept bid with reduce volume fefo
     *
     * @return void
     **/
    public function testAcceptBidShouldReduceVolumeFefo()
    {
        Notification::fake();
        (new CompanySettingService())->createReduceVolumeFefoApproveBid($this->company);

        $fefo = $this->createApprovedFefoBid();

        $this->assertDatabaseHas('fefo', [
            'id' => $fefo->id,
            'volume' => 200
        ]);
    }

    /**
     * Test accept bid without reduce volume fefo
     *
     * @return void
     **/
    public function testAcceptBidShouldWithoutReduceVolumeFefo()
    {
        Notification::fake();
        (new CompanySettingService())->createReduceVolumeFefoApproveBid($this->company, false);

        $fefo = $this->createApprovedFefoBid();

        $this->assertDatabaseHas('fefo', [
            'id' => $fefo->id,
            'volume' => 300
        ]);
    }

    /**
     * create reduce balance account test env
     *
     * @return void
     */
    public function createApprovedFefoBid()
    {
        Notification::fake();
        $this->client = factory(Client::class)->create();

        $this->user->root = 1;
        $this->user->save();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $this->client->id,
            'min_order' => 200,
            'account_balance' => 5000
        ]);

        $leadtime = $this->createLeadTime($this->client->cnpj, 'CD SP', $this->company->id);

        $product = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'weight' => 5
        ]);

        $product->users()->save($this->user);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
            'company_id' => $this->company->id,
        ]);

        $bid = $this->createBid($fefo, 20, 9.00, 100, 0, $this->client->id, $leadtime->code, $this->company->id);

        $approve = [
            'bid_ids' => [
                $bid->id
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/fefo/{$fefo->id}/bids/approve", $approve);

        return $fefo;
    }

    /**
     * @return void
     */
    public function testSecondBidFromClientShouldNotReturnFirstPurchaseLabel()
    {
        $client = factory(Client::class)->create();
        factory(ClientCompany::class)->create([
            'client_id' => $client->id
        ]);
        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);
        $product = factory(Product::class)->create();
        factory(Order::class)->create([
            'client_id' => $client->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);
        $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo/{$fefo->id}/bids");

        $response->assertStatus(200);
        $response->assertJsonCount(0, 'data.0.relationships.labels');
    }
}
