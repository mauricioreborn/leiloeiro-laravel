<?php

namespace Tests\Feature;

use App\Auth\User;
use App\Bid\Bid;
use App\Client\Client;
use App\Company\Company;
use App\Core\Entities\Status;
use App\Core\Helpers\JWT;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class FefoControllerTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @var array
     */
    protected $fefoApiStructure = [
        'data' => [
            'id',
            'attributes' => [
                'date',
                'weeks',
                'leadtime_code',
                'product_code',
                'selling_price',
                'max_price',
                'min_price',
                'highlight_app',
            ],
        ],
    ];

    /**
     * Setup
     * @return void
     **/
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Fefo related routes
     * @return array
     */
    public function resourceRoutes()
    {
        return [
            ['get', '/api/v1/fefo'],
            ['put', '/api/v1/fefo/foo'],
            ['get', '/api/v1/fefo/stats/volume'],
        ];
    }

    /**
     * @param string $method Method name
     * @param string $url    Url
     * @dataProvider resourceRoutes
     * @return void
     */
    public function testResourceEndpointsWithoutTokenShouldReturnError($method, $url)
    {
        $response = $this->$method($url);
        $response->assertStatus(401);
    }

    /**
     * @return void
     */
    public function testGetRequestShouldReturnEmptyList()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/fefo');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['data' => []]);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * @return void
     */
    public function testUpdateRequestWithInvalidDataShouldReturnError()
    {
        $user     = $this->mockUser(1);
        $jwt      = JWT::encodeUser($user->id);
        $client   = factory(Client::class)->create();
        $leadtime = factory(LeadTime::class)->create(
            [
                'name'              => 'SAO PAULO - SP',
                'cnpj'              => $client->cnpj,
                'days'              => 1,
                'prediction'        => 1,
                'limit_hour'        => 2300,
                'monday'            => 1,
                'tuesday'           => 1,
                'wednesday'         => 1,
                'thursday'          => 1,
                'friday'            => 1,
                'saturday'          => 1,
                'sunday'            => 1,
                'operates_saturday' => 1,
                'operates_sunday'   => 1,
                'company_id'        => $this->company->id,
            ]
        );
        $fefo     = factory(Fefo::class)->create(
            [
                'date'                => date('Y-m-d'),
                'weeks'               => 5,
                'leadtime_code'       => $leadtime->code,
                'selling_price'       => 9.32,
                'max_price'           => 9.85,
                'min_price'           => 7.19,
                'changed_admin_price' => 0,
                'highlight_app'       => 1,
                'volume'              => 5160.00,
                'best_before'         => null,
            ]
        );

        $minPrice     = $fefo->min_price - ($fefo->min_price * 30 / 100);
        $invalidPrice = $minPrice - 1;

        $payload = ['price' => $invalidPrice];

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")->put("/api/v1/fefo/{$fefo->id}", $payload);
        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson(
            [
                'errors' => [
                    [
                        'status'      => 400,
                        'description' => __('Fefo/errors.price_too_low', [
                            'minPrice' => number_format($minPrice, 2, ',', '.')
                        ]),
                    ],
                ],
            ]
        );
    }

    /**
     * @return void
     */
    public function testUpdateRequestWithInvalidUserShouldReturnError()
    {
        $user = $this->mockUser(0);
        $jwt = JWT::encodeUser($user->id);
        $client = factory(Client::class)->create();
        $leadtime = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5160.00,
            'best_before' => null,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")
            ->put("/api/v1/fefo/{$fefo->id}", ['price' => 9.0]);
        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'errors' => [
                [
                    'code' => __('auth.unauthorized.code'),
                    'title' => __('auth.unauthorized.title'),
                    'status' => 403,
                ],
            ],
        ]);
    }

    /**
     * @return void
     */
    public function testUpdateRequestShouldReturnEligibleBidsList()
    {
        $user     = $this->mockUser(1);
        $jwt = JWT::encodeUser($user->id);
        $client = factory(Client::class)->create();
        $leadtime = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client->cnpj,
            'company_id' => $this->company->id,
        ]);
        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5160.00,
            'best_before' => null,
        ]);
        factory(Bid::class, 5)->create([
            'client_id' => $client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 1,
            'total_kg' => 200,
            'kg_price' => 9.30,
            'price' => 9.30 * 200,
            'duration' => Carbon::now()->addDays(2),
            'user_id' => $client->id,
            'company_id' => $this->company->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")
            ->put("/api/v1/fefo/{$fefo->id}", ['price' => 9.0]);
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment(['type' => 'bids_for_auto_approval']);
        $response->assertJsonCount(5, 'data.bids');
    }

    /**
     * @return void
     */
    public function testUpdateRequestShouldReturnUpdatedFefo()
    {
        $user     = $this->mockUser(1);
        $jwt      = JWT::encodeUser($user->id);
        $client   = factory(Client::class)->create();
        $leadtime = factory(LeadTime::class)->create(
            [
                'name'              => 'SAO PAULO - SP',
                'cnpj'              => $client->cnpj,
                'days'              => 1,
                'prediction'        => 1,
                'limit_hour'        => 2300,
                'monday'            => 1,
                'tuesday'           => 1,
                'wednesday'         => 1,
                'thursday'          => 1,
                'friday'            => 1,
                'saturday'          => 1,
                'sunday'            => 1,
                'operates_saturday' => 1,
                'operates_sunday'   => 1,
                'company_id'        => $this->company->id,
            ]
        );

        $fefo = factory(Fefo::class)->create(
            [
                'date'                => date('Y-m-d'),
                'weeks'               => 5,
                'leadtime_code'       => $leadtime->code,
                'selling_price'       => 9.32,
                'max_price'           => 9.85,
                'min_price'           => 7.19,
                'changed_admin_price' => 0,
                'highlight_app'       => 1,
                'volume'              => 5160.00,
                'best_before'         => null,
            ]
        );

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")
            ->put("/api/v1/fefo/{$fefo->id}", ['price' => 9.0]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $fefo = $fefo->fresh();

        $response->assertJson(
            [
                "data" => [
                    "type" => "fefo",
                    "id" => $fefo->id,
                    "attributes" => [
                        "date" => $fefo->date,
                        "weeks" => $fefo->weeks,
                        "leadtime_code" => $fefo->leadtime_code,
                        "product_code" => $fefo->product_code,
                        "volume" => $fefo->volume,
                        "selling_price" => 9,
                        "max_price" => $fefo->max_price,
                        "min_price" => $fefo->min_price,
                        "highlight_app" => $fefo->highlight_app,
                        'product' => $fefo->product->toArray(),
                    ],
                ],
            ]
        );

        $this->assertDatabaseHas('processed_fefos', [
            'fefo_id' => $fefo->id
        ]);
    }

    /**
     * @param int $root     root permission
     * @param int $price    update price permission
     * @param int $featured update featured permission
     * @return mixed
     */
    public function mockUser($root = 0, $price = 0, $featured = 0)
    {
        $company = factory(Company::class)->create(['name' => 'seara']);

        $user = factory(User::class)->create(
            [
                'password' => md5('testpassword'),
                'root'     => $root,
                'price'    => $price,
                'featured' => $featured,
            ]
        );
        $company->users()->save($user);

        return $user;
    }

    /**
     * @return void
     */
    public function testUpdateFeaturedShouldReturnUpdatedFefo()
    {
        $user     = $this->mockUser(1);
        $jwt      = JWT::encodeUser($user->id);
        $client   = factory(Client::class)->create();
        $leadtime = factory(LeadTime::class)->create(
            [
                'name'              => 'SAO PAULO - SP',
                'cnpj'              => $client->cnpj,
                'days'              => 1,
                'prediction'        => 1,
                'limit_hour'        => 2300,
                'monday'            => 1,
                'tuesday'           => 1,
                'wednesday'         => 1,
                'thursday'          => 1,
                'friday'            => 1,
                'saturday'          => 1,
                'sunday'            => 1,
                'operates_saturday' => 1,
                'operates_sunday'   => 1,
                'company_id'        => $this->company->id,
            ]
        );
        $fefo     = factory(Fefo::class)->create(
            [
                'date'                => date('Y-m-d'),
                'weeks'               => 5,
                'leadtime_code'       => $leadtime->code,
                'selling_price'       => 9.32,
                'max_price'           => 9.85,
                'min_price'           => 7.19,
                'changed_admin_price' => 0,
                'highlight_app'       => 1,
                'volume'              => 5160.00,
                'best_before'         => null,
            ]
        );

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")
            ->put("/api/v1/fefo/{$fefo->id}/featured", ['highlight_app' => 0]);
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment(['highlight_app' => 0]);
    }

    /**
     * @return void
     */
    public function testUpdateFeaturedWithInvalidUserShouldReturnUnauthorized()
    {
        $user     = $this->mockUser(0);
        $jwt      = JWT::encodeUser($user->id);
        $client   = factory(Client::class)->create();
        $leadtime = factory(LeadTime::class)->create(
            [
                'name'              => 'SAO PAULO - SP',
                'cnpj'              => $client->cnpj,
                'days'              => 1,
                'prediction'        => 1,
                'limit_hour'        => 2300,
                'monday'            => 1,
                'tuesday'           => 1,
                'wednesday'         => 1,
                'thursday'          => 1,
                'friday'            => 1,
                'saturday'          => 1,
                'sunday'            => 1,
                'operates_saturday' => 1,
                'operates_sunday'   => 1,
                'company_id'        => $this->company->id,
            ]
        );
        $fefo     = factory(Fefo::class)->create(
            [
                'date'                => date('Y-m-d'),
                'weeks'               => 5,
                'leadtime_code'       => $leadtime->code,
                'selling_price'       => 9.32,
                'max_price'           => 9.85,
                'min_price'           => 7.19,
                'changed_admin_price' => 0,
                'highlight_app'       => 1,
                'volume'              => 5160.00,
                'best_before'         => null,
            ]
        );

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")
            ->put("/api/v1/fefo/{$fefo->id}/featured", ['highlight_app' => 0]);
        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson(
            [
                'errors' => [
                    [
                        'code'   => __('auth.unauthorized.code'),
                        'title'  => __('auth.unauthorized.title'),
                        'status' => 403,
                    ],
                ],
            ]
        );
    }

    /**
     * Assures that fefo has access to fefo from own company and return right fields on endpoint
     * @return void;
     */
    public function testShowFefoWithSuccess()
    {
        $fefo = factory(Fefo::class)->create([
            'product_code' => function () {
                return factory(Product::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            },
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/fefo/$fefo->id");
        $responseContent = json_decode($response->getContent(), true);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonStructure($this->fefoApiStructure);
    }

    /**
     * Assures that user hasn't access to fefo from other company
     * @return void;
     */
    public function testShowFefoWithWrongCompanyAndReturnUnauthorized()
    {
        $company2 = factory(Company::class)->create();

        $product = factory(Product::class)->create([
            'company_id' => $company2,
        ]);

        $fefo = factory(Fefo::class)->create([
            'product_code' => $product->code,
            'company_id' => $company2->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/fefo/$fefo->id");

        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['errors' => [
            [
                'code' => __('auth.unauthorized.code'),
                'status' => '403',
                'title' => __('auth.unauthorized.title'),
            ]
        ]]);
    }

    /**
     * @return void
     */
    public function testGetRequestShouldReturnOnlyOneCompanyData()
    {
        factory(Fefo::class, 10)->create([
            'product_code' => function () {
                return factory(Product::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            },
        ]);

        $company2 = factory(Company::class)->create();
        factory(Fefo::class, 10)->create([
            'company_id' => $company2->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/fefo");
        $responseContent = json_decode($response->getContent(), true);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(10, 'data');
    }

    /**
     * Test fefo statistics by bid status
     *
     * @return void
     */
    public function testGetFefoStatisticsByBidStatus()
    {
        $client   = factory(Client::class)->create();

        $leadtime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 5160.00,
            'best_before' => null,
        ]);

        $bid1 = factory(Bid::class)->create([
            'company_id' => $this->company->id,
            'weeks' => $fefo->weeks,
            'product_code' => $fefo->product_code,
            'origin_code' => $fefo->leadtime_code,
            'total_kg' => 50,
            'kg_price' => 8.5,
            'price' => 425,
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        $bid2 = factory(Bid::class)->create([
            'company_id' => $this->company->id,
            'weeks' => $fefo->weeks,
            'product_code' => $fefo->product_code,
            'origin_code' => $fefo->leadtime_code,
            'total_kg' => 100,
            'kg_price' => 8.5,
            'price' => 850,
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        $bid3 = factory(Bid::class)->create([
            'company_id' => $this->company->id,
            'weeks' => $fefo->weeks,
            'product_code' => $fefo->product_code,
            'origin_code' => $fefo->leadtime_code,
            'total_kg' => 10,
            'kg_price' => 7.5,
            'price' => 850,
            'status_id' => Status::type(Status::CANCELED)->id,
        ]);

        $bid4 = factory(Bid::class)->create([
            'company_id' => $this->company->id,
            'weeks' => $fefo->weeks,
            'product_code' => $fefo->product_code,
            'origin_code' => $fefo->leadtime_code,
            'total_kg' => 12,
            'kg_price' => 10.5,
            'price' => 126,
            'status_id' => Status::type(Status::CANCELED)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo/$fefo->id/statistics?limit=100&status_id[]=6&status_id[]=22");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            "data" => [
                "type" => "fefo_statistics",
                "attributes" => [
                    "price" => [
                        "min" => 7.5,
                        "max" => 10.5,
                        "average" => 8.75,
                        "mode" => 8.5
                    ],
                    "volume" => [
                        "min" => 10,
                        "max" => 100,
                        "average" => 43
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testFefoListOrdenation()
    {
        $client = factory(Client::class)->create();
        $leadtime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $highVolume = 5160.00;
        $lowVolume = 5160.00;

        factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'leadtime_code' => $leadtime->code,
            'volume' => $highVolume,
        ]);

        factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'leadtime_code' => $leadtime->code,
            'volume' => $lowVolume,
        ]);

        $responseAsc = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo?page=1&sort=volume&include=product,leadtime");

        $responseDesc = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/fefo?page=1&sort=-volume&include=product,leadtime");

        $responseAsc->assertStatus(200);
        $responseDesc->assertStatus(200);

        $responseAsc->assertJson([
            'data' => [[
                'attributes' => [
                    'volume' => $lowVolume
                ]
            ]],
        ]);

        $responseDesc->assertJson([
            'data' => [[
               'attributes' => [
                   'volume' => $highVolume
               ]
           ]],
        ]);
    }
}
