<?php

namespace Tests\Feature;

use App\Company\Company;
use App\Core\Helpers\JWT;
use App\Auth\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Tests that a user was successfully logged in
     * @return void
     */
    public function testUserLoginWithSuccess()
    {
        $company = factory(Company::class)->create();
        $user = factory(User::class)->create(
            [
                'password' => 'testpassword',
                'company_id' => $company->id
            ]);

        $response = $this->post('/api/v1/login', [
            'username' => $user->email,
            'password' => 'testpassword',
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonStructure([
            'data' => [
                'authorization_token',
                'expires_at',
                'company'
            ],
        ]);
        $content = $response->decodeResponseJson();
        $tokenEncoded = $content['data']['authorization_token'];
        $token = (array) JWT::decode($tokenEncoded);
        $token['exp'] =  Carbon::createFromTimestamp($token['exp'])->toDateString();
        $token['iat'] =  Carbon::createFromTimestamp($token['iat'])->toDateString();

        $this->assertEquals($token, [
            'iss'  => config('app.url'),
            'exp'  => Carbon::now()->addDay()->toDateString(),
            'iat'  => Carbon::now()->toDateString(),
            'type' => JWT::USER_TYPE,
            'uid'  => $user->id,
        ]);
    }

    /**
     * Tests that the user's credentials were not found
     * @return void
     */
    public function testUserNotFound()
    {
        $response = $this->post('/api/v1/login', [
            'username' => 'testing@souk.com',
            'password' => 'testpassword',
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(401);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['errors' => [
            [
                'code' => __('auth.unauthorized.code'),
                'status' => '401',
                'title' => __('auth.unauthorized.title'),
            ]
        ]]);
    }

    /**
     * Tests a request attempt without a token
     * @return void
     */
    public function testRequestWithoutToken()
    {
        $response = $this->get('/api/v1/me');
        $response->assertStatus(401);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['errors' => [
            [
                'code' => __('auth.unauthorized.code'),
                'status' => '401',
                'title' => __('auth.unauthorized.title'),
            ]
        ]]);
    }

    /**
     * Tests a request attempt with a valid token
     * @return void
     */
    public function testHeadRequestWithValidTokenShouldReturnNoContent()
    {
        $user = factory(User::class)->create();
        $token = JWT::encodeUser($user->id);

        $server = $this->transformHeadersToServerVars(['Authorization' => "Bearer {$token}"]);

        $response = $this->call('HEAD', '/api/v1/me', [], [], [], $server);

        $response->assertStatus(204);
    }

    /**
     * Tests a request attempt with a valid token
     * @return void
     */
    public function testGetRequestWithValidTokenShouldReturnAuthenticatedUser()
    {
        $user = factory(User::class)->create();
        $token = JWT::encodeUser($user->id);

        $response = $this->withHeaders(['Authorization' => "Bearer {$token}"])->get('/api/v1/me');

        //dd($response->decodeResponseJson(), $user->toArray());

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'attributes' => [
                'name' => $user->name,
                'email' => $user->email,
                'root' => $user->root,
                'price' => $user->price,
                'featured' => $user->featured,
                'company_id' => $user->company_id,
                'deleted_at' => $user->deleted_at,
            ],
        ]);
    }

    /**
     * Tests a request attempt with an expired token
     * @return void
     */
    public function testRequestWithExpiredToken()
    {
        $yesterday = Carbon::now()->addDay(-1);
        $user = factory(User::class)->create();
        $token = JWT::encode($user->id, $expiration = $yesterday, JWT::USER_TYPE);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->get('/api/v1/me');

        $response->assertStatus(401);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['errors' => [
            [
                'code' => __('auth.unauthorized.code'),
                'status' => '401',
                'title' => __('auth.unauthorized.title'),
            ]
        ]]);
    }

    /**
     * Tests a request attempt with an invalid token signature
     * @return void
     */
    public function testRequestWithInvalidTokenSignature()
    {
        $user = factory(User::class)->create();
        $token = JWT::encodeUser($user->id);

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}xxx", // the xxx at the end invalidates the signature
        ])->get('/api/v1/me');

        $response->assertStatus(401);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['errors' => [
            [
                'code' => __('auth.unauthorized.code'),
                'status' => '401',
                'title' => __('auth.unauthorized.title'),
            ]
        ]]);
    }

    /**
     * Tests a request attempt with an invalid authorization header
     * @return void
     */
    public function testRequestWithInvalidAuthorizationHeader()
    {
        $response = $this->withHeaders([
            'Authorization' => 'nope',
        ])->get('/api/v1/me');

        $response->assertStatus(401);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['errors' => [
            [
                'code' => __('auth.unauthorized.code'),
                'status' => '401',
                'title' => __('auth.unauthorized.title'),
            ]
        ]]);
    }

    /**
     * Tests a request with a valid service token
     * @return void
     */
    public function testRequestWithValidServiceToken()
    {
        $token = JWT::encodeSA('fake-service-account');

        $server = $this->transformHeadersToServerVars(['Authorization' => "Bearer {$token}"]);

        $response = $this->call('HEAD', '/api/v1/me', [], [], [], $server);

        $response->assertStatus(204);
    }

    /**
     * Tests a request with an invalid token type
     * @return void
     */
    public function testRequestWithInvalidTokenType()
    {
        $token = JWT::encode('invalid', $expiration = Carbon::now()->addDay(), $type = 'invalid');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->get('/api/v1/me');

        $response->assertStatus(401);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['errors' => [
            [
                'code' => __('auth.unauthorized.code'),
                'status' => '401',
                'title' => __('auth.unauthorized.title'),
            ]
        ]]);
    }

    /**
     * Tests update user with success
     * @return void
     **/
    public function testUpdateUserWithSuccessUsingConfirmPassword()
    {
        $faker = \Faker\Factory::create();

        $company = factory(Company::class)->create();
        $user = factory(User::class)->create([
            'password' => 'testpassword',
            'company_id' => $company->id
        ]);
        $token = JWT::encodeUser($user->id);
        $password = $faker->password();

        $fieldsToUpdate = [
            'name' => $faker->name(),
            'email' => $faker->email(),
            'password' => $password,
            'confirm_password' => $password,
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->put('/api/v1/user/update', $fieldsToUpdate);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'type' => "user",
                'id' => $user->id,
                'attributes' => [
                    'name' => $fieldsToUpdate['name'],
                    'email' => $fieldsToUpdate['email']
                ]
            ]
        ]);
    }

    /**
     * Tests update user with success
     * @return void
     **/
    public function testUpdateUserWithSuccessUsingPasswordConfirmation()
    {
        $faker = \Faker\Factory::create();

        $company = factory(Company::class)->create();
        $user = factory(User::class)->create([
            'password' => 'testpassword',
            'company_id' => $company->id
        ]);
        $token = JWT::encodeUser($user->id);
        $password = $faker->password();

        $fieldsToUpdate = [
            'name' => $faker->name(),
            'email' => $faker->email(),
            'password' => $password,
            'password_confirmation' => $password
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->put('/api/v1/user/update', $fieldsToUpdate);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'type' => "user",
                'id' => $user->id,
                'attributes' => [
                    'name' => $fieldsToUpdate['name'],
                    'email' => $fieldsToUpdate['email']
                ]
            ]
        ]);
    }
}
