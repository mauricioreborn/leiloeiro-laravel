<?php
namespace Tests\Feature\EnableCreditCard;

use App\Bid\Bid;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCard;
use App\Core\Entities\Status;
use App\Jobs\IntegrateInvoiceJob;
use App\Order\Order;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Queue;

class ClientCanManageCardsTest extends EnableCreditCard
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testClientCredicardListShouldReturnEmpty()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->get('/api/v1/mobile/client/credit_cards');
        $response->assertOk();
        $response->assertJsonCount(0, 'data');
    }

    /**
     * @return void
     */
    public function testAddCreditCardRequestWithMockedDataShouldReturnSuccess()
    {
        Queue::fake();

        $mockedIuguResponse = [
            'token' => str_random(32),
            'name' => 'Cartão VISA final 1111',
        ];
        $createPaymentResponse = [
            'id' => str_random(32),
            'description' => 'cartao de credito',
            'item_type' => 'credit_card',
            'customer_id' => str_random(32),
            'data' => [
                'brand' => 'VISA',
                'holder_name' => 'teste iugu',
                'display_number' => 'XXXX-XXXX-XXXX-1111',
                'bin' => '411111',
                'month' => 2,
                'year' => 2021,
            ],
        ];

        $this->iuguService->shouldReceive('createPaymentMethods')->andReturn($createPaymentResponse);

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->post('/api/v1/mobile/client/credit_cards', $mockedIuguResponse);

        $clientCard = ClientCard::first();

        $response->assertOk();
        $response->assertJsonFragment([
            'attributes' => [
                'payment_method_id' => $createPaymentResponse['id'],
                'description' => 'cartao de credito',
                'holder_name' => 'teste iugu',
                'display_number' => '1111',
                'brand' => 'VISA',
                'bin' => '411111',
                'month' => 2,
                'year' => 2021,
                'cc_accepted_in' => null,
                'default' => true,
                'image' => $clientCard->image,
                'enable' => true,
                'label' => false,
            ],
            'relationships' => [
                'status' => [
                    'type' => 'status',
                    'id' => 3,
                    'attributes' => [
                        'name' => Status::PENDING,
                        'displayName' => __('status.' . Status::PENDING),
                    ],
                ],
            ],
        ]);

        Queue::assertPushed(IntegrateInvoiceJob::class);
    }

    /**
     * Method to return invoice events
     *
     * @return array
     */
    public function brands()
    {
        return [
            [
                'VISA',
                [
                    'quotaTax' => 2.51,
                    'quotaPrice' => 1025.73,
                    '2quotaTax' => 3.21,
                    '2quotaPrice' => 1033.13,
                    '7quotaTax' => 3.55,
                    '7quotaPrice' => 1036.76,
                ]
            ],
            [
                'MASTERCARD',
                [
                    'quotaTax' => 2.51,
                    'quotaPrice' => 1025.73,
                    '2quotaTax' => 3.21,
                    '2quotaPrice' => 1033.13,
                    '7quotaTax' => 3.55,
                    '7quotaPrice' => 1036.76,
                ]
            ],
            [
                'American Express',
                [
                    'quotaTax' => 4.60,
                    'quotaPrice' => 1048.12,
                    '2quotaTax' => 4.60,
                    '2quotaPrice' => 1048.12,
                    '7quotaTax' => 4.60,
                    '7quotaPrice' => 1048.12,
                ]
            ],
        ];
    }

    /**
     * @param string $brand Card brand
     * @param array  $taxes Tax values
     *
     * @dataProvider brands
     *
     * @return void
     */
    public function testAddCreditCardListNotEmpty(string $brand, array $taxes)
    {
        Queue::fake();

        $clientCard = factory(ClientCard::class)->state('pending')->create([
            'client_id' => $this->mockedClient->id,
            'brand' => $brand
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->get('/api/v1/mobile/client/credit_cards?quotaPrice=1000');

        $response->assertOk();
        $response->assertJsonCount(1, 'data');

        $data = array_get($response->decodeResponseJson(), 'data', []);
        $quotas = array_get(array_first($data), 'relationships.quotas');

        $this->assertContains([
            "quota" => 1,
            "total" => number_format(array_get($taxes, 'quotaPrice'), 2),
            "tax" => array_get($taxes, 'quotaTax'),
            "message" => __('Payment/clientCard.quota', [
                'quota' => 1,
                'price' => formata_moeda(array_get($taxes, 'quotaPrice'), false),
                'tax' => array_get($taxes, 'quotaTax'),
            ])
        ], $quotas);

        $this->assertContains([
            "quota" => 2,
            "total" => number_format(array_get($taxes, '2quotaPrice'), 2),
            "tax" => array_get($taxes, '2quotaTax'),
            "message" => __('Payment/clientCard.quota', [
                'quota' => 2,
                'price' => formata_moeda(array_get($taxes, '2quotaPrice') / 2, false),
                'tax' => array_get($taxes, '2quotaTax'),
            ])
        ], $quotas);

        $this->assertContains([
            "quota" => 7,
            "total" => number_format(array_get($taxes, '7quotaPrice'), 2),
            "tax" => array_get($taxes, '7quotaTax'),
            "message" => __('Payment/clientCard.quota', [
                'quota' => 7,
                'price' => formata_moeda(array_get($taxes, '7quotaPrice')  / 7, false),
                'tax' => array_get($taxes, '7quotaTax'),
            ])
        ], $quotas);
    }

    /**
     * @return void
     */
    public function testClientCanRemoveTheirCards()
    {
        Queue::fake();
        $card = factory(ClientCard::class)->state('pending')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->delete("/api/v1/mobile/client/credit_cards/{$card->id}");

        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testClientCanNotRemoveOtherClientCards()
    {
        Queue::fake();

        $differentClient = factory(Client::class)->create();
        factory(ClientCompany::class)->create([
            'company_id' => $this->companyWithCreditCard->id,
            'client_id' => $differentClient->id,
            'min_order' => 100.00,
        ]);

        $myCard = factory(ClientCard::class)->state('pending')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $otherCard = factory(ClientCard::class)->state('pending')->create([
            'client_id' => $differentClient,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->delete("/api/v1/mobile/client/credit_cards/{$otherCard->id}");

        $response->assertOk();
        $response->assertJsonFragment(['status' => false]);
    }

    /**
     * @return void
     */
    public function testClientCantRemoveWithOrderPending()
    {
        Queue::fake();

        $card = factory(ClientCard::class)->state('pending')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $order = factory(Order::class)->state('open')->create([
            'client_id' => $this->mockedClient->id,
            'client_card_id' => $card->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->delete("/api/v1/mobile/client/credit_cards/{$card->id}");

        $response->assertOk();
        $response->assertJsonFragment([
            'status' => false,
            'message' => __('Payment/clientCard.invoice_pending')
        ]);
    }

    /**
     * @return void
     */
    public function testClientCantRemoveWithBidPending()
    {
        Queue::fake();

        $card = factory(ClientCard::class)->state('pending')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $this->mockedClient->id,
            'client_card_id' => $card->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->delete("/api/v1/mobile/client/credit_cards/{$card->id}");

        $response->assertOk();
        $response->assertJsonFragment([
            'status' => false,
            'message' => __('Payment/clientCard.invoice_pending')
        ]);
    }

    /**
     * @return void
     */
    public function testClientCanRemoveWithOrderCanceled()
    {
        Queue::fake();

        $card = factory(ClientCard::class)->state('pending')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $order = factory(Order::class)->state('canceled')->create([
            'client_id' => $this->mockedClient->id,
            'client_card_id' => $card->id,
        ]);

        $bid = factory(Bid::class)->state('canceled')->create([
            'client_id' => $this->mockedClient->id,
            'client_card_id' => $card->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->delete("/api/v1/mobile/client/credit_cards/{$card->id}");

        $response->assertOk();
        $response->assertJsonFragment([
            'id' => $card->id,
        ]);
    }
}