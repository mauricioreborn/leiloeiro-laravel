<?php

namespace Tests\Feature\EnableCreditCard;

use App\Bid\Bid;
use App\Cart\Cart;
use App\Cart\ProductCart;
use App\Client\ClientCard;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Payment\Payment;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Core\Entities\Status;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;

class LastCardUsedTest extends ClientCanManageCardsTest
{
    use DatabaseTransactions;

    /**
     * Testar quando cadastrar um novo cartão
     * @return void
     */
    public function testAddNewCreditCardAndBecomeDefault()
    {
        Queue::fake();

        factory(ClientCard::class)->state('pending')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $mockedIuguResponse = [
            'token' => str_random(32),
            'name' => 'Cartão VISA final 1111',
        ];

        $createPaymentResponse = [
            'id' => str_random(32),
            'description' => 'cartao de credito',
            'item_type' => 'credit_card',
            'customer_id' => str_random(32),
            'data' => [
                'brand' => 'VISA',
                'holder_name' => 'teste iugu',
                'display_number' => 'XXXX-XXXX-XXXX-1111',
                'bin' => '411111',
                'month' => 2,
                'year' => 2021,
            ],
        ];

        $this->iuguService->shouldReceive('createPaymentMethods')->andReturn($createPaymentResponse);

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->post('/api/v1/mobile/client/credit_cards', $mockedIuguResponse);

        $response->assertOk();

        $response = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->get('/api/v1/mobile/client/credit_cards');

        $response->assertOk();
        $response->assertJsonCount(2, 'data');
        $response = $response->decodeResponseJson();

        $this->assertArraySubset(['default' => false], $response['data'][0]['attributes']);
        $this->assertArraySubset(['default' => true], $response['data'][1]['attributes']);
    }

    /**
     * @return void
     */
    public function testFinalizeABidAndCheckDefaultCard()
    {
        $clientCard1 = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->mockedClient->id]
        );

        $clientCardToBid = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $clientCard3 = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'company_id' => $this->companyWithCreditCard->id,
            'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'cnpj' => $this->mockedClient->cnpj,
                    'company_id' => $this->companyWithCreditCard->id
                ])->code;
            }
        ]);

        $paymentCreditCard = Payment::isCreditCard()->first();

        $paymentCompanyCreditCard = $this->companyWithCreditCard->companyPayments()
            ->where('payment_id', $paymentCreditCard->id)->first();

        $clientCompanyPayment = $this->mockedClient->clientCompanyPayments()
            ->where('company_payment_id', $paymentCompanyCreditCard->id)
            ->first();

        $bid = factory(Bid::class)->create([
            'client_id' => $this->mockedClient->id,
            'status_id' => Status::type(Status::PENDING)->id,
            'origin_code' => $fefo->leadtime_code,
            'box_amount' => 2,
            'company_id' => $fefo->company_id,
            'client_card_id' => $clientCardToBid->id,
            'client_company_payment_id' => $clientCompanyPayment->id
        ]);

        $bidRequest = [
            'client_id' => $bid->client_id,
            'cod_origem' => $bid->origin_code,
            'cod_produto' => $bid->product_code,
            'aceite_parcial' => $bid->partial_accept,
            'duracao_lance' => date('Y-m-d'),
            'qtd_caixas' => $bid->box_amount,
            'semanas' => $bid->weeks,
            'total_kg' => $bid->weight,
            'valor' => $bid->price,
            'valor_kg' => $bid->kg_price,
            'client_card_id' => $clientCardToBid->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'cc_months' => 1,
            'tax_percent' => 5
        ];

        $responseBid = $this->withHeader('Authorization', "Bearer {$this->mockedClientJwt}")
            ->post("/api/v1/mobile/company/{$this->companyWithCreditCard->id}/confirma_lance", $bidRequest);

        $responseBid->assertOk();

        $clientCardToBid = $clientCardToBid->fresh();
        $clientCard1 = $clientCard1->fresh();
        $clientCard3 = $clientCard3->fresh();

        $this->assertEquals(1, $clientCardToBid->default);
        $this->assertEquals(false, $clientCard1->default);
        $this->assertEquals(false, $clientCard3->default);
    }

    /**
     * @return void
     */
    public function testFinalizeAOrderAndCheckDefaultCard()
    {
        Queue::fake();

        $clientCardOrder = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->mockedClient->id
        ]);

        $clientCard2 = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $clientCard3 = factory(ClientCard::class)->state('authorized')->create([
            'client_id' => $this->mockedClient->id,
        ]);

        $createPaymentResponse = [
            'id' => str_random(32),
            'description' => 'cartao de credito',
            'item_type' => 'credit_card',
            'customer_id' => str_random(32),
            'data' => [
                'brand' => 'VISA',
                'holder_name' => 'teste iugu',
                'display_number' => 'XXXX-XXXX-XXXX-1111',
                'bin' => '411111',
                'month' => 2,
                'year' => 2021,
            ],
        ];

        $this->iuguService->shouldReceive('createPaymentMethods')->andReturn($createPaymentResponse);

        $cart = factory(Cart::class)->create([
            'id_cliente' => $this->mockedClient->id,
            'company_id' => $this->companyWithCreditCard->id,
        ]);

        $productCart = factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'valor' => 300,
            'valor_kg' => 100
        ]);

        factory(LeadTime::class)->create([
            'code' =>  $productCart->cod_origem,
        ]);

        factory(Product::class)->create([
            'code' =>  $productCart->cod_produto,
            'company_id' => $this->companyWithCreditCard->id,
        ]);

        factory(Fefo::class)->create([
            'selling_price' => 400,
            'date'  =>  date('Y-m-d'),
            'weeks'  => $productCart->semanas,
            'leadtime_code' => $productCart->cod_origem,
            'product_code' =>  $productCart->cod_produto,
            'selling_price' => $productCart->valor_kg,
        ]);

        $paymentCreditCard = Payment::isCreditCard()->first();

        $paymentCompanyCreditCard = $this->companyWithCreditCard->companyPayments()
            ->where('payment_id', $paymentCreditCard->id)->first();

        $clientCompanyPayment = $this->mockedClient->clientCompanyPayments()
            ->where('company_payment_id', $paymentCompanyCreditCard->id)
            ->first();

        $orderRequest = [
            'aceita_parcial' => 1,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCardOrder->id,
            'cc_months' => 1,
        ];

        $response = $this->withHeader('Authorization',  "Bearer {$this->mockedClientJwt}")
            ->post("/api/v1/mobile/company/{$this->companyWithCreditCard->id}/finaliza_pedido", $orderRequest);

        $response = $response->decodeResponseJson();
        
        $this->assertArraySubset(['status' => true], $response);

        $clientCardOrder = $clientCardOrder->fresh();
        $clientCard2 = $clientCard2->fresh();
        $clientCard3 = $clientCard3->fresh();

        $this->assertEquals(1, $clientCardOrder->default);
        $this->assertEquals(false, $clientCard2->default);
        $this->assertEquals(false, $clientCard3->default);
    }
}
