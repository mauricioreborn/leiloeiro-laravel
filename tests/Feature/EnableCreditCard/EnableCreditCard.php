<?php
namespace Tests\Feature\EnableCreditCard;

use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Core\Helpers\JWT;
use App\Iugu\Services\IuguApiService;
use App\Iugu\Services\IuguService;
use App\Payment\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

class EnableCreditCard extends TestCase
{
    use DatabaseTransactions;

    protected $companyWithCreditCard;
    protected $companyWithoutCreditCard;
    protected $mockedClient;
    protected $mockedClientJwt;
    protected $iuguService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpMockedJwt();

        $this->createTestMocks();

        $this->iuguService = resolve(IuguService::class);
    }

    /**
     * @return void
     */
    public function createTestMocks()
    {
        $this->companyWithCreditCard = factory(Company::class)->create();
        $this->companyWithoutCreditCard = factory(Company::class)->create();

        foreach(Payment::all() as $payment) {
            $data = ['payment_id' => $payment->id];

            if($payment->type !== 'credit_card') {
                $this->companyWithoutCreditCard->companyPayments()->create($data);
            }

            $this->companyWithCreditCard
                ->companyPayments()
                ->create($data);
        }

        $this->mockedClient = factory(Client::class)->create(['password' => '12345']);

        factory(ClientCompany::class)->create([
            'company_id' => $this->companyWithCreditCard->id,
            'client_id' => $this->mockedClient->id,
            'min_order' => 100.00,
            'account_balance' => 1000.00
        ]);
        factory(ClientCompany::class)->create([
            'company_id' => $this->companyWithoutCreditCard->id,
            'client_id' => $this->mockedClient->id,
            'min_order' => 100.00,
        ]);

        foreach ($this->companyWithCreditCard->companyPayments as $companyPayment){
            factory(ClientCompanyPayment::class)->create([
                'client_id' => $this->mockedClient->id,
                'company_payment_id' => $companyPayment->id
            ]);
        }

        $this->mockedClientJwt = JWT::encodeClient($this->mockedClient->id);
    }
}