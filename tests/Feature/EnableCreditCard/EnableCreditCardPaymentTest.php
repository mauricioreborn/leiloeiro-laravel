<?php
namespace Tests\Feature\EnableCreditCard;

use App\Client\Services\ClientCompanyPaymentService;
use App\Payment\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EnableCreditCardPaymentTest extends EnableCreditCard
{
    use DatabaseTransactions;

    /**
     * Testar que um cliente pode ter meio de pagamento atualizado.
     * @return void
     */
    public function testClientCanHaveCreditCardEnabled()
    {
        $creditCardPayment = Payment::where('type', 'credit_card')->first();
        $companyPayment = $this->companyWithCreditCard
            ->companyPayments()
            ->where('payment_id', $creditCardPayment->id)
            ->first();

        (new ClientCompanyPaymentService)
            ->syncCompanyPayments(
                $this->mockedClient, $this->companyWithCreditCard, [$companyPayment->id]
            );

        $this->assertNotNull(
            $this->mockedClient->clientCompanyPayments->where('company_payment_id', $companyPayment->id)
        );
    }

    /**
     * Testar que um cliente de uma company sem cartão de crédito não pode ter
     * meio de pagamento atualizado.
     * @return void
     */
    public function testPaymentFromDifferentCompaniesShouldThrowError()
    {
        $this->expectException(\InvalidArgumentException::class);

        $creditCardPayment = Payment::where('type', 'credit_card')->first();
        $companyPayment = $this->companyWithCreditCard
            ->companyPayments()
            ->where('payment_id', $creditCardPayment->id)
            ->first();

        (new ClientCompanyPaymentService)
            ->syncCompanyPayments(
                $this->mockedClient, $this->companyWithoutCreditCard, [$companyPayment->id]
            );

        $this->assertNull(
            $this->mockedClient->clientCompanyPayments->where('company_payment_id', $companyPayment->id)
        );
    }


    /**
     * Testar endpoint de atualização de clientes para habilitar cartão de crédito.
     * @return void
     */
    public function testClientPaymentUpdateRequestShouldEnableCreditCardForClient()
    {
        $this->user->update(['company_id' => $this->companyWithCreditCard->id]);

        $creditCardPayment = Payment::where('type', 'credit_card')->first();
        $companyPayment = $this->companyWithCreditCard
            ->companyPayments()
            ->where('payment_id', $creditCardPayment->id)
            ->first();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/payments/client/{$this->mockedClient->id}", [
                'company_payment_ids' => [$companyPayment->id]
            ]);

        $response->assertOk();

        $this->assertNotNull(
            $this->mockedClient->clientCompanyPayments->where('company_payment_id', $companyPayment->id)
        );
    }
}