<?php
namespace Tests\Feature\EnableCreditCard;

use App\Client\Services\ClientCompanyPaymentService;
use App\Payment\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MobileFeatureSwitchTest extends EnableCreditCard
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testClientLoginReturnsCreditCardEnabled()
    {
        $creditCardPayment = Payment::where('type', 'credit_card')->first();
        $companyPayment = $this->companyWithCreditCard
            ->companyPayments()
            ->where('payment_id', $creditCardPayment->id)
            ->first();

        (new ClientCompanyPaymentService)
            ->syncCompanyPayments(
                $this->mockedClient, $this->companyWithCreditCard, [$companyPayment->id]
            );

        $response = $this->post('/api/v1/mobile/login', [
            'cnpj'     => $this->mockedClient->cnpj,
            'password' => '12345',
        ]);

        $response->assertOk();
        $response->assertJsonFragment(['cc_enabled' => true]);
    }
}