<?php

namespace Tests\Feature;

use App\Company\Company;
use App\LeadTime\LeadTime;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class LeadTimeControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to start the configuration
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * List leadtime group by origin code and name
     *
     * @return void
     */
    public function testLeadTimeOrigins()
    {
        $company2 = factory(Company::class)->create();

        factory(LeadTime::class)->create([
            'code' => 713,
            'name' => 'CAMBORIU - CD',
            'company_id' => $company2->id
        ]);

        factory(LeadTime::class, 2)->create([
            'code' => 697,
            'name' => 'SAO PAULO - CD',
            'company_id' => $this->company->id
        ]);

        factory(LeadTime::class, 2)->create([
            'code' => 905,
            'name' => 'DUQUE DE CAXIAS - CD',
            'company_id' => $this->company->id
        ]);

        factory(LeadTime::class)->create([
            'code' => 892,
            'name' => 'SP ANHANGUERA - CD',
            'deleted_at' => now()->toDateTimeString(),
            'company_id' => $this->company->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/leadtime/origins');

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'data' => [
                [
                    'type' => 'leadtime_origin',
                    'attributes'    => [
                        'code' => 905,
                        'name' => 'DUQUE DE CAXIAS - CD',
                    ]
                ],
                [
                    'type' => 'leadtime_origin',
                    'attributes'    => [
                        'code' => 697,
                        'name' => 'SAO PAULO - CD',
                    ]
                ],
            ]
        ]);
    }
}
