<?php

namespace Tests\Feature;

use App\Company\Company;
use App\Log\LogEmail;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LogControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    public function testLogEmailList(){
        factory(LogEmail::class)->create([
            'company_id' => $this->company->id,
            'message' => 'message',
            'page' => 'screen',
            'subject' => 'subject',
            'emails' => 'email@email.com',
        ]);

        $company2 = factory(Company::class)->create();

        factory(LogEmail::class)->create([
            'company_id' => $company2->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/logs/emails');

        $response->assertStatus(200);

        $response->assertJsonCount(1, 'data');

        $response->assertJsonStructure([
            'data' => [
                    [
                    'id',
                    'type',
                    'attributes' => [
                        'message',
                        'screen',
                        'subject',
                        'emails',
                        'created_at',
                    ]
                ]
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ]
        ]);

        $response->assertJsonFragment([
            'type' => 'log_email',
            'message' => 'message',
            'screen' => 'screen',
            'subject' => 'subject',
            'emails' => 'email@email.com',
        ]);
    }
}
