<?php

namespace Tests\Feature;

use App\Bid\Bid;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\Company\Company;
use App\Company\CompanySetting;
use App\Core\Entities\Status;
use App\Core\Entities\RejectionMotive;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Notification\Notification;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Products\Product;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class OrderControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();

        $this->client = factory(Client::class)->create();
        $clientCompany = factory(ClientCompany::class)->create(['client_id' => $this->client->id]);

        CompanySetting::where([
            'company_id' => $this->user->company->id,
            'setting' => 'hasManagementRequests'
        ])->update(['value' => '1']);
    }

    /**
     * Assures that a request will return 401 for this endpoint if
     * there isn't an authorization token attached to it
     * @return void
     */
    public function testListRequestWithoutToken()
    {
        $response = $this->get('/api/v1/orders');
        $response->assertStatus(401);
    }

    /**
     * test empty list
     * @return void
     */
    public function testEmptyList()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/orders');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['data' => []]);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * Assures that a request will return 401 for this endpoint if
     * there isn't an authorization token attached to it
     * @return void
     */
    public function testUpdateRequestWithoutToken()
    {
        $order = factory(Order::class)->create();

        $response = $this->withHeader('Authorization', "")
            ->put("/api/v1/orders/{$order['id']}", $order->first()->toArray());

        $response->assertStatus(401);
        if ($response->status() == 422) {
            Log::emergency($response->getContent());
        }
    }

    /**
     * Test approve order partialy without closing it, with success
     * @return void
     */
    public function testApproveOrderPartialyWithSuccess()
    {
        $faker = Factory::create();

        $order = factory(Order::class)->state('open')->create([
            'client_id' => $this->client->id,
        ]);

        $fieldsToUpdate = [
            'order_code' => $faker->randomNumber(),
            'order_product' => [
                [
                    'id' => $order->orderProduct->first()->id,
                    'box_amount' => $order->orderProduct->first()->box_amount,
                    'status' => true
                ]
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/orders/{$order['id']}", $fieldsToUpdate);

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $kg_total = $fieldsToUpdate['order_product'][0]['box_amount'] * $order->orderProduct->first()->product->weight;
        $price = $kg_total * $order->orderProduct->first()->kg_price;

        $this->assertArraySubset(
            [
                'data' => [
                    'type' => 'order',
                    'id' => $order->id,
                    'attributes' => [
                        'order_code' => $fieldsToUpdate['order_code'],
                        'client_id' => $order->client_id,
                        'status_id' => $order->status_id
                    ],
                    'relationships' => [
                        'order_product' => [
                            [
                                'type' => 'order_product',
                                'id' => $fieldsToUpdate['order_product'][0]['id'],
                                'attributes' => [
                                    'box_amount' => $fieldsToUpdate['order_product'][0]['box_amount'],
                                    'kg_price' => round($order->orderProduct->first()->kg_price, 2),
                                    'kg_total' => round($kg_total, 2),
                                    'price' => round($price, 2),
                                    'status_id' => Status::type(Status::APPROVED)->id
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            $content
        );

        $this->assertDatabaseMissing('notifications', [
            'title' => __('status.order.pending.push.title', ['order_id' => $order->id]),
        ]);
    }

    /**
     * Test cancel order partialy without closing it, with success
     * @return void
     */
    public function testCancelOrderPartialyWithSuccess()
    {
        $faker = Factory::create();

        $order = factory(Order::class)->state('open')->create([
            'client_id' => $this->client->id
        ]);

        $fieldsToUpdate = [
            'order_code' => $faker->randomNumber(),
            'order_product' => [
                [
                    'id' => $order->orderProduct->first()->id,
                    'box_amount' => $faker->randomDigitNotNull(),
                    'status' => false,
                ],
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/orders/{$order['id']}", $fieldsToUpdate);

        $response->assertStatus(200);

        $this->assertEquals($order->orderProduct()->first()->status_id, Status::type(Status::CANCELED)->id);

    }

    /**
     * Test approve order closing it, with success
     * @return void
     */
    public function testApproveOrderClosingItWithSuccess()
    {
        $faker = Factory::create();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->states('companyCredit')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->state('open')->create([
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id
        ]);

        $fieldsToUpdate = [
            'order_code' => $faker->randomNumber(),
            'order_product' => []
        ];

        foreach ($order->orderProduct as $orderProduct) {
            $fieldsToUpdate['order_product'][] = [
                'id' => $orderProduct->id,
                'box_amount' => $faker->randomDigitNotNull(),
                'status' => true
            ];
        }

        //cancel an orderProduct
        $totalFieldsToUpdate = count($fieldsToUpdate);
        $fieldsToUpdate['order_product'][$totalFieldsToUpdate - 1]['status'] = false;

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/orders/{$order['id']}", $fieldsToUpdate);

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset (
            [
                'data' => [
                    'type' => 'order',
                    'id' => $order->id,
                    'attributes' => [
                        'shipment_date' => today()->addDay(2)->toDateString(),
                        'order_code' => $fieldsToUpdate['order_code'],
                        'client_id' => $order->client_id,
                        'status_id' => Status::type(Status::APPROVED)->id
                    ]
                ]
            ],
            $content
        );

        $this->assertDatabaseHas('notifications', [
            'title' => __('status.order.partially_billed.push.title', ['order_id' => $order->id]),
            'message' => __('status.order.partially_billed.push.message')
        ]);
    }

    /**
     * Test cancel order closing it, with success
     * @return void
     */
    public function testCancelOrderClosingItWithSuccess()
    {
        $faker = Factory::create();

        $order = factory(Order::class)->state('open')->create([
            'client_id' => $this->client->id
        ]);

        $fieldsToUpdate = [
            'order_code' => $faker->randomNumber(),
            'order_product' => []
        ];

        foreach ($order->orderProduct as $orderProduct) {
            $fieldsToUpdate['order_product'][] = [
                'id' => $orderProduct->id,
                'box_amount' => $faker->randomDigitNotNull(),
                'status' => false
            ];
        }

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/orders/{$order['id']}", $fieldsToUpdate);

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset (
            [
                'data' => [
                    'type' => 'order',
                    'id' => $order->id,
                    'attributes' => [
                        'order_code' => $fieldsToUpdate['order_code'],
                        'client_id' => $order->client_id,
                        'status_id' => Status::type(Status::CANCELED)->id
                    ]
                ]
            ],
            $content
        );

        $this->assertDatabaseHas('notifications', [
            'client_id' => $this->client->id,
            'title' => __('rejectionMotive.order.canceled.push.title', ['order_id' => $order->id]),
            'message' => __('rejectionMotive.order.canceled.push.message')
        ]);
    }

    /**
     * test order history
     * @return void
     */
    public function testOrderHistory()
    {
        $this->markTestSkipped('this test will be performed in another PR');

        $leadTime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id
        ]);

        $fefo_count = 3;
        for ($i=1; $i <= $fefo_count; $i++) {
            $product = factory(Product::class)->create([
                'weight' => 10,
                'weight_piece' => 10,
                'pieces' => 1,
                'company_id' => $this->company->id
            ]);

            $fefo = factory(Fefo::class)->create([
                'date' => now()->subDays($i - 1)->toDateString(),
                'leadtime_code' => $leadTime->code,
                'product_code' =>  $product->code,
                'volume' => 10000 * $i
            ]);

            $orders = factory(Order::class, $i)->create([
                'date' => now()->subDays($i - 1)->toDateString(),
                'client_id' => $this->client->id,
                'rejection_motives_id' => null,
                'status_id' => Status::type(Status::PENDING)->id,
            ]);

            foreach ($orders as $order)
            {
                factory(OrderProduct::class)->create([
                    'order_id' => $order->id,
                    'product_id' => $fefo->product_code,
                    'weeks' => $fefo->weeks,
                    'kg_total' => 10 * $i,
                    'kg_price' => 11 * (1 - ($i/10)),
                    'final_status_description' => '',
                    'origin_code' => $leadTime->code,
                    'rejection_motives_id' => null,
                ]);

                if($i % 2 == 0) {
                    factory(OrderProduct::class)->create([
                        'order_id' => $order->id,
                        'product_id' => $fefo->product_code,
                        'weeks' => $fefo->weeks,
                        'kg_total' => 15 * $i,
                        'kg_price' => 7 * (1 - ($i/10)),
                        'final_status_description' => 'CANCELADO',
                        'origin_code' => $leadTime->code,
                        'rejection_motives_id' => null,
                    ]);
                }

                factory(OrderProduct::class)->create([
                    'order_id' => $order->id,
                    'product_id' => $fefo->product_code,
                    'weeks' => $fefo->weeks,
                    'kg_total' => 20 * $i,
                    'kg_price' => 8 * (1 - ($i/10)),
                    'final_status_description' => 'EM ANALISE',
                    'origin_code' => $leadTime->code,
                    'rejection_motives_id' => null,
                ]);

                factory(OrderProduct::class)->create([
                    'order_id' => $order->id,
                    'product_id' => $fefo->product_code,
                    'weeks' => $fefo->weeks,
                    'kg_total' => 25 * $i,
                    'kg_price' => 9 * (1 - ($i/10)),
                    'final_status_description' => 'EM ATENDIMENTO',
                    'origin_code' => $leadTime->code,
                    'rejection_motives_id' => null,
                ]);

                factory(OrderProduct::class)->create([
                    'order_id' => $order->id,
                    'product_id' => $fefo->product_code,
                    'weeks' => $fefo->weeks,
                    'kg_total' => 30 * $i,
                    'kg_price' => 10 * (1 - ($i/10)),
                    'final_status_description' => 'FATURADO',
                    'origin_code' => $leadTime->code,
                    'rejection_motives_id' => null,
                ]);

            }

            $cancelledOrder = factory(Order::class)->create([
                'date' => now()->subDays($i)->toDateString(),
                'client_id' => $this->client->id,
                'status_id' => Status::type(Status::CANCELED)->id,
                'rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id
            ]);

            factory(OrderProduct::class, 2)->create([
                'order_id' => $cancelledOrder->id,
                'product_id' => $fefo->product_code,
                'weeks' => $fefo->weeks,
                'kg_total' => 100,
                'kg_price' => 8 * (1 - ($i/10)),
                'origin_code' => $leadTime->code,
                'status_id' => Status::type(Status::CANCELED)->id,
                'rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id
            ]);
        }

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/orders/history");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'date' => today()->toDateString(),
            'total_volume' => "10.000",
            'orders' => 1,
            'clients' => 1,
            'traded_volume' => "85",
            'volume' => [
                [
                    "title" => "EM ANALISE",
                    "volume" => "20",
                ],
                [
                    "title" => "EM ATENDIMENTO",
                    "volume" => "25",
                ],
                [
                    "title" => "FATURADO",
                    "volume" => "30",
                ],
                [
                    "title" => "INTEGRADO",
                    "volume" => "10",
                ]
            ]
        ]);

        $response->assertJsonFragment([
            'date' => today()->subDays(1)->toDateString(),
            'total_volume' => "20.000",
            'orders' => 3,
            'clients' => 1,
            'traded_volume' => "340",
            'volume' => [
                [
                    "title" => "CANCELADO",
                    "volume" => "60",
                ],
                [
                    "title" => "EM ANALISE",
                    "volume" => "280",
                ],
                [
                    "title" => "EM ATENDIMENTO",
                    "volume" => "100",
                ],
                [
                    "title" => "FATURADO",
                    "volume" => "120",
                ],
                [
                    "title" => "INTEGRADO",
                    "volume" => "40",
                ]
            ]
        ]);

        $response->assertJsonCount(3, 'data');

        $this->assertEquals($fefo_count, Fefo::count());
    }

    /**
     * test order history without history
     * @return void
     */
    public function testOrderHistoryWithoutOrders()
    {
        $fefo_count = 2;
        for ($i=1; $i <= $fefo_count; $i++) {
            $fefo = factory(Fefo::class)->create([
                'date' => now()->subDays($i)->toDateString(),
                'volume' => 10000 * $i
            ]);
        }

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/orders/history");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'date' => today()->subDay()->toDateString(),
            'total_volume' => "10.000",
            'orders' => 0,
            'clients' => 0,
            'traded_volume' => "0",
            'volume' => []
        ]);

        $response->assertJsonCount(2, 'data');

        $this->assertEquals($fefo_count, Fefo::count());
    }

    /**
     * test order history without history
     * @return void
     */
    public function testOrderHistoryWithOrdersDateDifferentOfFefoDate()
    {
        $fefo_count = 1;

        $fefo = factory(Fefo::class)->create([
            'date' => now()->subDays(1)->toDateString(),
            'volume' => 10000
        ]);

        $order = factory(Order::class)->create([
            'date' => now()->addDays(5)->toDateString(),
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'produto_id' => $fefo->product_code,
            'origin_code' => $fefo->leadtime_code,
            'weeks' => $fefo->weeks,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/orders/history");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'date' => today()->subDay()->toDateString(),
            'total_volume' => "10.000",
            'orders' => 0,
            'clients' => 0,
            'traded_volume' => "0",
            'volume' => []
        ]);

        $response->assertJsonCount(1, 'data');

        $this->assertEquals($fefo_count, Fefo::count());
    }

    /**
     * @dataProvider searchParams
     * @param string $param param
     * @param string $value value
     * @return void
     */
    public function testGetOrderWithParams($param, $value)
    {
        $leadTime = factory(Order::class)->create();
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
                ->get("/api/v1/orders?$param=$value");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Params to test if all queries are working
     * @return array
     */
    public function searchParams()
    {
        return [
            [
                'order_code',
                '591000001'
            ],
            [
                'date',
                '2018-06-13'
            ],
            [
                'status',
                '1'
            ],
            [
                'unread',
                '0'
            ],
            [
                'product_code',
                1
            ],
        ];
    }

    /**
     * @return void
     */
    public function testGetRequestShouldReturnOnlyOneCompanyData()
    {
        factory(Order::class, 10)->create([
            'company_id' => $this->company->id,
        ]);

        $company2 = factory(Company::class)->create();
        factory(Order::class, 10)->create([
            'company_id' => $company2->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get("/api/v1/orders");
        $responseContent = json_decode($response->getContent(), true);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(10, 'data');
    }

    /**
     * @return void
     */
    public function testUpdateOrderUnauthorized()
    {
        $faker = Factory::create();
        $client = factory(Client::class)->create();
        $order = factory(Order::class)->state('open')->create([
            'client_id' => $client->id
        ]);

        CompanySetting::where([
            'company_id' => $this->user->company->id,
            'setting' => 'hasManagementRequests'
        ])->update(['value' => '0']);

        $fieldsToUpdate = [
            'order_code' => $faker->randomNumber(),
            'order_product' => [
                [
                    'id' => $order->orderProduct->first()->id,
                    'box_amount' => $faker->randomDigitNotNull(),
                    'status' => true
                ]
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/orders/{$order['id']}", $fieldsToUpdate);

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testOrderSearchWithStatusParam()
    {
        $statusPending = Status::type(Status::PENDING);

        $orderPending = factory(Order::class)->state('open')->create();
        $orderCanceled = factory(Order::class)->state('canceled')->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/orders?status={$statusPending->id}");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                [
                    'type' => 'order',
                    'id' => $orderPending->id,
                    'attributes' => [
                        'status_description' => $orderPending->status_description,
                    ]
                ]
            ]
        ], $content);
    }

    /**
     * @return void
     */
    public function testOrderNoShowValueTax()
    {
        $order = factory(Order::class)->create(['tax_percent' => 5]);
        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'price' => 100
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/orders?include=orderProduct.product");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $responseContent = json_decode($response->getContent(), true);

        $totalPriceOrder = $responseContent['data'][0]['attributes']['price'];
        $order_product = $responseContent['data'][0]['relationships']['order_product'];
        $totalPriceProductOrder = 0;

        foreach($order_product as $order_product){
            $totalPriceProductOrder += $order_product['attributes']['price'];
        }

        $this->assertEquals($totalPriceOrder, $totalPriceProductOrder);
    }

    public function testUpdateOrderAcceptSomeProducts()
    {
        $faker = Factory::create();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->states('companyCredit')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->state('open')->create([
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id
        ]);

        $fieldsToUpdate = [
            'order_code' => $faker->randomNumber(),
            'order_product' => []
        ];

        foreach ($order->orderProduct as $index => $orderProduct) {
            $fieldsToUpdate['order_product'][] = [
                'id' => $orderProduct->id,
                'box_amount' => $faker->randomDigitNotNull(),
                'status' => $index > 0 ? null : true
            ];
        }

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/orders/{$order['id']}", $fieldsToUpdate);

        $response->assertStatus(200);
        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset (
            [
                'data' => [
                    'type' => 'order',
                    'id' => $order->id,
                    'attributes' => [
                        'order_code' => $fieldsToUpdate['order_code'],
                        'client_id' => $order->client_id,
                        'status_id' => Status::type(Status::PENDING)->id
                    ]
                ]
            ],
            $content
        );
    }

    public function testUpdateBidOrderBoxAmount()
    {
        $faker = Factory::create();

        $leadTime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id
        ]);

        $product = factory(Product::class)->create([
            'name' => 'LING T PAIO SEARA 370G',
            'weight' => 10,
            'weight_piece' => 10,
            'pieces' => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'  =>  now()->toDateString(),
            'leadtime_code' => $leadTime->code,
            'product_code' =>  $product->code,
            'selling_price' => 10.0,
            'volume' => 1000.0,
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => 10,
            'total_kg' => 100,
            'kg_price' => 8.50,
            'price' => 8.50 * 100,
            'duration' => now()->addDays(2),
            'user_id' => null,
            'company_id' => $this->company->id
        ]);

        $order = factory(Order::class)->state('open')->create([
            'client_id' => $this->client->id,
            'bid_id' => $bid->id,
        ]);

        $fieldsToUpdate = [
            'order_code' => $faker->randomNumber(),
            'order_product' => [
                [
                    'id' => $order->orderProduct->first()->id,
                    'box_amount' => 9,
                    'status' => false,
                ]
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/orders/{$order['id']}", $fieldsToUpdate);

        $response->assertStatus(200);

        $this->assertDatabaseHas('lances', [
            'id' => $bid->id,
            'qtd_caixas' => 9
        ]);
    }
}
