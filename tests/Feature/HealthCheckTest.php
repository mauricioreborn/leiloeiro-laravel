<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HealthCheckTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHealthCheck()
    {
        $response = $this->get('/api/v1/healthcheck');
        $response->assertStatus(200);
        $response->assertJson(['health' => true]);
    }
}
