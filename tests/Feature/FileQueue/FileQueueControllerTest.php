<?php

namespace Tests\Feature\FileQueue;

use App\Company\Company;
use App\FileQueue\FileQueue;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class FileQueueControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Method to test get file_queues by type
     * @return null
     */
    public function testGetFilesUploadByTypeShouldReturnSuccess()
    {
        FileQueue::unsetEventDispatcher();

        $fileQueue = factory(FileQueue::class)->create();

        Storage::shouldReceive('disk')
            ->andReturn(new class() {
                public function temporaryUrl()
                {
                    return 'https://s3.aws.com/bucket/{$fileQueue->path}';
                }
            });

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->get("/api/v1/file_queue/type?type=$fileQueue->type");

        $response->assertJsonFragment([
            'type' => 'file_queue',
            'id' => $fileQueue->id,
            'path' => $fileQueue->getPath($fileQueue->path, 's3'),
            'type' => $fileQueue->type
        ]);
    }

    /**
     * Method to test get file_queues
     * @return null
     */
    public function testGetFilesShouldReturnSuccess()
    {
        FileQueue::unsetEventDispatcher();

        $fileQueueFirst = factory(FileQueue::class)->create([
            'type' => FileQueue::CLIENT,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()->subWeek()
        ]);

        $fileQueueLast = factory(FileQueue::class)->create([
            'type' => FileQueue::CLIENT,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        Storage::shouldReceive('disk')
            ->andReturn(new class() {
                public function temporaryUrl()
                {
                    return 'https://s3.aws.com/bucket/{$fileQueueLast->path}';
                }
            });

        $response = $this->withHeader('Authorization',"Bearer {$this->jwt}")->get("/api/v1/file_queue");

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset(
            [
                'data' => [
                    'client' => [
                        'type' => 'file_queue_type',
                        'id' => $fileQueueLast->id,
                        'attributes' => [
                            'type' => $fileQueueLast->type,
                            'path' => $fileQueueLast->getPath($fileQueueLast->path, 's3'),
                            'original_name' => $fileQueueLast->original_name,
                            'custom_messages' => [
                                'title' => __("Uploads/{$fileQueueLast->type}.title"),
                                'description' => __("Uploads/{$fileQueueLast->type}.description"),
                                'last_update_decription' => __("Uploads/generic.last_update_decription", [
                                    'date' => Carbon::parse($fileQueueLast->updated_at)->format('d/m/Y'),
                                    'time' => Carbon::parse($fileQueueLast->updated_at)->format('h:i'),
                                    'user_name' => $fileQueueLast->user->name
                                ])
                            ]
                        ]
                    ],
                    'stock' => [
                        'type' => 'file_queue_type',
                        'id' => null,
                        'attributes' => [
                            'type' => 'stock',
                            'path' => null,
                            'original_name' => null,
                            'custom_messages' => [
                                'title' => __("Uploads/stock.title"),
                                'description' => __("Uploads/stock.description"),
                                'last_update_decription' => null
                            ]
                        ]
                    ]
                ]
            ],
            $content
        );
    }

    /**
     * Method to test get file_queues
     * @return null
     */
    public function testGetFilesFromAnotherCompanyShouldReturnEmpty()
    {
        FileQueue::unsetEventDispatcher();
        $fileQueueFromAnotherCompany= factory(FileQueue::class)->create([
            'type' => FileQueue::CLIENT,
            'company_id' => function () {
                return factory(Company::class)->create();
            },
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()->subWeek()
        ]);

        $response = $this->withHeader('Authorization',"Bearer {$this->jwt}")->get("/api/v1/file_queue");

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset(
            [
                'data' => [
                    'client' => [
                        'type' => 'file_queue_type',
                        'id' => null,
                        'attributes' => [
                            'type' => 'client',
                            'path' => null,
                            'original_name' => null,
                            'custom_messages' => [
                                'title' => __("Uploads/client.title"),
                                'description' => __("Uploads/client.description"),
                                'last_update_decription' => null
                            ]
                        ]
                    ]
                ]
            ],
            $content
        );
    }

    public function fileQueueServiceTypes()
    {
        $types = [];

        foreach (FileQueue::SERVICETYPES as $prefix => $type) {
            $types[] = [$prefix, $type];
        }

        return $types;
    }

    /**
     * Method to test get file_queues by type
     * @dataProvider fileQueueServiceTypes
     * @return null
     */
    public function testGetFilesUploadWithWebHooksService($prefix, $type)
    {
        $company = Company::first();

        $fileName = "data/{$company->name}/{$prefix}_201001010001.csv";

        $this->setUpMockedJwtHelipa();

        FileQueue::unsetEventDispatcher();

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/service/file_queue", [
            'key' => $fileName,
            'size' => 300
        ]);

        $response->assertJsonFragment([
            'type' => 'file_queue',
            'message' => __('filequeue.success'),
            'original_name' => "{$prefix}_201001010001.csv"
        ]);

        $this->assertDatabaseHas('file_queues', [
            'path' => $fileName,
            'type' => $type
        ]);
    }
}
