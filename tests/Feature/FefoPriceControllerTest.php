<?php

namespace Tests\Feature;

use App\Auth\User;
use App\Bid\Bid;
use App\Client\Client;
use App\Company\Company;
use App\Core\Helpers\JWT;
use App\Fefo\Fefo;
use App\Fefo\FefoPrice;
use App\Fefo\Http\Services\FefoService;
use App\Fefo\Services\FefoPriceService;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FefoPriceControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup
     * @return void
     **/
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Show fefo price
     *
     * @return null
     */
    public function testFefoPriceShow()
    {
        $company2 = factory(Company::class)->create();

        factory(FefoPrice::class)->create([
            'company_id' => $company2->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.77,
            'expired_at' => now()->addDays(2)->toDateString(),
        ]);

        $fefoPrice = factory(FefoPrice::class)->create([
            'company_id' => $this->company->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.51,
            'expired_at' => now()->addDays(1)->toDateString(),
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->json('GET', '/api/v1/fefo_price', ['weeks' => 2, 'leadtime_code' => 697, 'product_code' => 112233]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson(
            [
                "data" => [
                    "type" => "fefo_price",
                    "id" => $fefoPrice->id,
                    "attributes"    => [
                        "weeks" => $fefoPrice->weeks,
                        "leadtime_code" => $fefoPrice->leadtime_code,
                        "product_code" => $fefoPrice->product_code,
                        "selling_price" => $fefoPrice->selling_price,
                        "expired_at" => $fefoPrice->expired_at->toDateString()
                    ],
                    "relationships" => [],
                ],
            ]
        );
    }

    /**
     * Show fefo price
     *
     * @return null
     */
    public function testFefoPriceShowInOtherCompany()
    {
        $company2 = factory(Company::class)->create();

        factory(FefoPrice::class)->create([
            'company_id' => $company2->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.77,
            'expired_at' => now()->addDays(2)->toDateString(),
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->json('GET', '/api/v1/fefo_price', ['weeks' => 2, 'leadtime_code' => 697, 'product_code' => 112233]);

        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Show fefo price
     *
     * @return null
     */
    public function testFefoPriceShowWithExpiredAndDeletedPrice()
    {
        factory(FefoPrice::class)->create([
            'company_id' => $this->company->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.77,
            'expired_at' => now()->subDays(1)->toDateString(),
        ]);

        factory(FefoPrice::class)->create([
            'company_id' => $this->company->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.77,
            'expired_at' => now()->addDays(1)->toDateString(),
            'deleted_at' => now(),
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->json('GET', '/api/v1/fefo_price', ['weeks' => 2, 'leadtime_code' => 697, 'product_code' => 112233]);

        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Show fefo price
     *
     * @return null
     */
    public function testFefoPriceShowNotFound()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->json('GET', '/api/v1/fefo_price', ['weeks' => 2, 'leadtime_code' => 697, 'product_code' => 112233]);

        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'application/json');
    }
}
