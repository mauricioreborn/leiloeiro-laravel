<?php

namespace Tests\Feature;

use App\Auth\User;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to start the setup
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Method to test listing of product without token
     *
     * @return void
     */
    public function testListRequestWithoutToken()
    {
        $response = $this->get('/api/v1/products');
        $response->assertStatus(401);
    }

    /**
     * Method to test empty list of product
     *
     * @return void
     */
    public function testEmptyList()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/products');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['data' => []]);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * Test get two range_price objects with success
     * @return void
     **/
    public function testGetRequestShouldReturnTwoResultProducts()
    {
        $product = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'weight' => 50
        ]);

        $otherProduct = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'weight' => 60
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/products');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                [
                    'type' => 'product',
                    'id' => $product->id,
                    "attributes" => [
                        'name' => $product->name,
                        'code' => $product->code,
                        'picture_url' => $product->picture_url,
                        'category' => $product->category,
                        'type' => $product->type,
                        'brand' => $product->brand,
                        'subbrand' => $product->subbrand,
                        'unit' => $product->unit,
                        'weight' => $product->weight,
                        'weight_piece' => $product->weight_piece,
                        'pieces' => $product->pieces,
                        'min_billing' => $product->min_billing,
                        'avg_price' => $product->avg_price
                    ]
                ],
                [
                    'type' => 'product',
                    'id' => $otherProduct->id,
                    "attributes" => [
                        'name' => $otherProduct->name,
                        'code' => $otherProduct->code,
                        'picture_url' => $otherProduct->picture_url,
                        'category' => $otherProduct->category,
                        'type' => $otherProduct->type,
                        'brand' => $otherProduct->brand,
                        'subbrand' => $otherProduct->subbrand,
                        'unit' => $otherProduct->unit,
                        'weight' => $otherProduct->weight,
                        'weight_piece' => $otherProduct->weight_piece,
                        'pieces' => $otherProduct->pieces,
                        'min_billing' => $otherProduct->min_billing,
                        'avg_price' => $otherProduct->avg_price
                    ]
                ]
            ]
        ], $content);
    }

    /**
     * Method to test updated request without token
     *
     * @return void
     */
    public function testUpdateRequestWithoutToken()
    {
        $product = factory(Product::class)->create();

        $productPayload = factory(Product::class)->make();

        $response = $this->withHeader('Authorization', "")
            ->put("/api/v1/products/{$product->id}", $productPayload->toArray());

        $response->assertStatus(401);
    }

    /**
     * Method to test updated product with success
     *
     * @return void
     */
    public function testUpdateProductWithSuccess()
    {
        $this->user->root = 1;
        $this->user->save();

        $product = factory(Product::class)->create();

        $productPayload = factory(Product::class)->make()->only(['picture', 'picture_url', 'behaviour_approval']);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/products/{$product->id}", $productPayload);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                'type' => 'product',
                'id' => $product->id,
                "attributes" => [
                    'name' => $product->name,
                    'code' => $product->code, //code is not updated on request
                    'picture_url' => $productPayload['picture_url'],
                    'category' => $product->category,
                    'type' => $product->type,
                    'brand' => $product->brand,
                    'subbrand' => $product->subbrand,
                    'unit' => $product->unit,
                    'weight' => $product->weight,
                    'weight_piece' => $product->weight_piece,
                    'pieces' => $product->pieces,
                    'min_billing' => $product->min_billing,
                    'avg_price' => $product->avg_price,
                    'behaviour_approval' => $productPayload['behaviour_approval'],
                ]
            ]
        ], $content);
    }

    /**
     * Method to test updated product with success
     *
     * @return void
     */
    public function testUpdateProductWithNonRootUserShouldReturnUnauthorized()
    {
        $product = factory(Product::class)->create();

        $productPayload = factory(Product::class)->make();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/products/{$product->id}", $productPayload->toArray());

        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'code' => __('auth.unauthorized.code'),
            'title' => __('auth.unauthorized.title'),
            'status' => 403
        ]);
    }

    /**
     * Method to test picture url with correct company id
     *
     * @return void
     */
    public function testReturnACorrectPictureUrl()
    {
        $product = factory(Product::class)->create();
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/products');
        $response->assertStatus(200);
        $picture = env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' . $product->company_id .'/'. $product->picture;
        $response->assertJsonFragment(['picture_url' => $picture]);
    }

    public function testUpdateWithManagerKeyAndRootUserShouldUpdateProductUsers()
    {
        $this->user->root = 1;
        $this->user->save();

        $users = factory(User::class, 3)->create([
            'company_id' => $this->user->company_id
        ]);

        $product = factory(Product::class)->create();

        $productPayload = factory(Product::class)->make()->only(['picture', 'picture_url', 'behaviour_approval']);
        $productPayload['managers'] = $users->pluck('id')->toArray();


        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/products/{$product->id}", $productPayload);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonCount(3, 'data.relationships.user');
        $this->assertCount(3, $product->fresh()->users->toArray());
    }

    public function testUpdateWithManagerKeyAndNotRootUserShouldReturnUnauthorizedError()
    {
        $users = factory(User::class)->create([
            'company_id' => $this->user->company_id
        ]);

        $product = factory(Product::class)->create();

        $productPayload = factory(Product::class)->make()->only(['picture', 'picture_url', 'behaviour_approval']);
        $productPayload['managers'] = [$users->id];


        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/products/{$product->id}", $productPayload);

        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'errors' => [
                [
                    'code' => __('auth.unauthorized.code'),
                    'title' => __('auth.unauthorized.title'),
                    'status' => 403,
                ],
            ],
        ]);
    }

    public function testCheckIfPriceIncreaseHasBeenReturned()
    {
        $product = factory(Product::class, 2)->create([
            'price_increase' => 6.0
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/products');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                [
                    'type' => 'product',
                    'id' => $product[0]->id,
                    "attributes" => [
                        'name' => $product[0]->name,
                        'code' => $product[0]->code,
                        'picture_url' => $product[0]->picture_url,
                        'category' => $product[0]->category,
                        'type' => $product[0]->type,
                        'brand' => $product[0]->brand,
                        'subbrand' => $product[0]->subbrand,
                        'unit' => $product[0]->unit,
                        'weight' => $product[0]->weight,
                        'weight_piece' => $product[0]->weight_piece,
                        'pieces' => $product[0]->pieces,
                        'price_increase' => $product[0]->price_increase,
                        'min_billing' => $product[0]->min_billing,
                        'avg_price' => $product[0]->avg_price
                    ]
                ],
                [
                    'type' => 'product',
                    'id' => $product[1]->id,
                    "attributes" => [
                        'name' => $product[1]->name,
                        'code' => $product[1]->code,
                        'picture_url' => $product[1]->picture_url,
                        'category' => $product[1]->category,
                        'type' => $product[1]->type,
                        'brand' => $product[1]->brand,
                        'subbrand' => $product[1]->subbrand,
                        'unit' => $product[1]->unit,
                        'weight' => $product[1]->weight,
                        'weight_piece' => $product[1]->weight_piece,
                        'price_increase' => $product[0]->price_increase,
                        'pieces' => $product[1]->pieces,
                        'min_billing' => $product[1]->min_billing,
                        'avg_price' => $product[1]->avg_price
                    ]
                ]
            ]
        ], $content);
    }
}
