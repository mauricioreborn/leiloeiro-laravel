<?php

namespace Tests\Feature;

use App\QueueLoad\QueueLoad;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QueueLoadControllerTest extends TestCase
{

    use DatabaseTransactions;

    protected $leadTimeReturnStructure = [
        'data' => [
            'id',
            'attributes' => [
                'file_name',
                'file_url',
                'total_success',
                'total_error',
                'total_duplicates',
                'status',
                'file_size',
                'file_type',
                'started_at',
                'finished_at'
            ]
        ]
    ];

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Assures that a request will return 401 for this endpoint if
     * there isn't an authorization token attached to it
     */
    public function testListRequestWithoutToken()
    {
        $response = $this->get('/api/v1/queueload/');
        $response->assertStatus(401);
    }

    /**
     * test empty list
     */
    public function testEmptyList()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/queueload');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['data' => []]);
        $response->assertJsonCount(0, 'data');
    }

    public function testAddQueueLoadWithSuccess()
    {
        $leadTime = $this->buildALeadTime();
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->post('/api/v1/queueload/', $leadTime);
        $response->assertStatus(201);
        if ($response->status() == 422) {
            Log::emergency($response->getContent());
        }
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonStructure($this->leadTimeReturnStructure);
    }

    /**
     * Assures that a request will return 401 for this endpoint if
     * there isn't an authorization token attached to it
     */
    public function testUpdateRequestWithoutToken()
    {
        $leadTime    = factory(QueueLoad::class)->create();
        $leadTimeObj = $leadTime->first();
        $response    = $this->withHeader('Authorization', "")->put("/api/v1/queueload/$leadTimeObj->id", $this->buildALeadTime());
        $response->assertStatus(401);
        if ($response->status() == 422) {
            Log::emergency($response->getContent());
        }
    }

    /**
     * Assures that a request will return 401 for this endpoint if
     * there isn't an authorization token attached to it
     */
    public function testUpdateRequestWithToken()
    {

        $leadTime    = factory(QueueLoad::class)->create();
        $leadTimeObj = $leadTime->first();
        $response    = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/queueload/$leadTimeObj->id", $this->buildALeadTime());
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonStructure($this->leadTimeReturnStructure);
    }

    protected function buildALeadTime()
    {
        $faker = \Faker\Factory::create();

        return [
            'file_name'        => $faker->firstName,
            'file_url'         =>$faker->url,
            'total_success'    =>$faker->numberBetween(1,9),
            'total_error'      =>$faker->numberBetween(1,9),
            'total_duplicates' =>$faker->numberBetween(1,9),
            'status'           =>$faker->randomElement($array = array ('1','2','done')),
            'file_size'        =>$faker->numberBetween(1,9),
            'file_type'        =>$faker->randomElement($array = array (1,2,4))
        ];
    }
}
