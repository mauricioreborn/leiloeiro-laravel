<?php
namespace Tests\Feature\Campaign;

use App\Auth\User;
use App\Core\Helpers\JWT;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Campaign\Campaign;
use App\Campaign\CampaignType;
use App\Company\Company;

class CampaignControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
        $this->setUpMockedJwtHelipa();
    }

    /**
     * @return void
     **/
    public function testEmptyList()
    {
        $jwt = JWT::encodeUser($this->user->id);
        $response = $this->withHeader('Authorization', "Bearer {$jwt}")->get('/api/v1/campaign');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['data' => []]);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * @return void
     **/
    public function testGetRequestShouldReturnTwoResults()
    {
        $jwt = JWT::encodeUser($this->user->id);
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);
        factory(Campaign::class, 2)->create(['campaign_type_id' => $campaignType->id]);

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")->get('/api/v1/campaign');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(2, 'data');
    }

    /**
     * @return void
     **/
    public function testDeleteRequestForProcessedCampaignShouldReturnError()
    {
        $this->user->root = 1;
        $this->user->save();

        $jwt = JWT::encodeUser($this->user->id);
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);
        $campaign = factory(Campaign::class)->create(['campaign_type_id' => $campaignType->id]);

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")->delete("/api/v1/campaign/{$campaign->id}");
        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(["error" => __('auth.access_denied')]);

        $this->user->root = 0;
        $this->user->save();
    }

    /**
     * @return void
     **/
    public function testDeleteRequestForValidCampaignShouldReturnSuccessWithNoContent()
    {
        $this->user->root = 1;
        $this->user->save();

        $jwt = JWT::encodeUser($this->user->id);
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);
        $campaign = factory(Campaign::class)->create(['campaign_type_id' => $campaignType->id, 'processed_at' => null]);

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")->delete("/api/v1/campaign/{$campaign->id}");
        $response->assertStatus(204);

        $this->user->root = 0;
        $this->user->save();
    }

    /**
     * testAddClientWithSuccess
     *
     * @return void
     */
    public function testCreateCampaignShouldReturnNewCampaignData()
    {
        $this->artisan('db:seed', ['--class' => 'CampaignTypeSeeder']);

        $campaignDate = now()->addDay()->toDateTimeString();
        $campaign = [
            'type' => CampaignType::first()->uid,
            'scheduled_at' => $campaignDate,
        ];

        $jwt = JWT::encodeUser($this->user->id);

        $response = $this->withHeader('Authorization', "Bearer {$jwt}")->post('/api/v1/campaign', $campaign);
        $response->assertStatus(201);

        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            "attributes" => [
                "estimated_clients" => null,
                "scheduled_at" => $campaignDate,
                "processed_at" => null
            ],
        ]);
    }
    /**
     * Method to get campaign types
     *
     * @return void
     */
    public function testGetCampaignTypes()
    {
        $this->artisan('db:seed', ['--class' => 'CampaignTypeSeeder']);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/types");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $repurchase = CampaignType::where('uid', 'repurchase')->firstOrFail();
        $firstPurchase = CampaignType::where('uid', 'firstPurchase')->firstOrFail();
        $highlightFefo = CampaignType::where('uid', 'highlightFefo')->firstOrFail();

        $response->assertJsonFragment([
            'id' => $repurchase->id,
            'name' => $repurchase->name,
            'uid' => $repurchase->uid,
            'description' => $repurchase->description,
        ]);
        $response->assertJsonFragment([
            'id' => $firstPurchase->id,
            'name' => $firstPurchase->name,
            'uid' => $firstPurchase->uid,
            'description' => $firstPurchase->description,
        ]);
        $response->assertJsonFragment([
            'id' => $highlightFefo->id,
            'name' => $highlightFefo->name,
            'uid' => $highlightFefo->uid,
            'description' => $highlightFefo->description,
        ]);
    }
}
