<?php

namespace Tests\Feature\Campaign;

use App\Campaign\Campaign;
use App\Campaign\CampaignClient;
use App\Campaign\CampaignType;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Company\Company;
use App\Core\Helpers\JWT;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Products\Product;
use CampaignTypeSeeder;
use App\Core\Entities\Status;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RepurchaseTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
        $this->setUpMockedJwtHelipa();
    }

    /**
     *  Test get clients who didn't buy in 30 days
     *
     * @return null
     */
    public function testGetClientsWhoDidntBuyIn30DaysWithSuccess()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id
        ]);

        $client1 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $orderProduct1 = $this->buildOrderProductToAClient($client1, '31');
        $orderProduct2 = $this->buildOrderProductToAClient($client2, '31');

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/{$campaign->id}/clients");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $content = json_decode($response->getContent(), true);

        $response->assertJsonFragment([
            'total' => 2,
            'clientIds' => [
                $client1->id,
                $client2->id
            ]
        ]);
    }

    /**
     *  Test get clients who one is elegible and another don't
     *
     * @return null
     */
    public function testGetClientsWhoOneIsElegibleAndAnotherDont()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id
        ]);

        $client1 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $orderProduct1 = $this->buildOrderProductToAClient($client1, '30');
        $orderProduct2 = $this->buildOrderProductToAClient($client2, '31');

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/{$campaign->id}/clients");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $content = json_decode($response->getContent(), true);

        $response->assertJsonFragment([
            'total' => 1,
            'clientIds' => [
                $client2->id
            ]
        ]);
    }

    /**
     *  Test get clients who no one is elegible
     *
     * @return null
     */
    public function testGetClientsWhoNoOneIsElegible()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id
        ]);

        $client1 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $orderProduct1 = $this->buildOrderProductToAClient($client1, '30');
        $orderProduct2 = $this->buildOrderProductToAClient($client2, '30');

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/{$campaign->id}/clients");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $content = json_decode($response->getContent(), true);

        $response->assertJsonFragment([
            'total' => 0,
            'clientIds' => []
        ]);
    }

    /**
     *  Test get clients from another company campaign
     *
     * @return null
     */
    public function testGetClientsWithAnotherCompanyCampaignShouldReturnNoOne()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);

        $company = factory(Company::class)->create();

        $campaign = factory(Campaign::class)->create([
            'company_id' => $company->id,
            'campaign_type_id' => $campaignType->id
        ]);

        $client1 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $orderProduct1 = $this->buildOrderProductToAClient($client1, '30');
        $orderProduct2 = $this->buildOrderProductToAClient($client2, '30');

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/{$campaign->id}/clients");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $content = json_decode($response->getContent(), true);

        $response->assertJsonFragment([
            'total' => 0,
            'clientIds' => []
        ]);
    }

    /**
     *  Test client message with a product, should return most bought product
     *
     * @return null
     */
    public function testGetClientMessageShouldReturnProduct()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id
        ]);

        $client1 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();
        factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $product = factory(Product::class)->create();
        $product2 = factory(Product::class)->create();

        $orderProduct1 = $this->buildOrderProductToAClient($client1, '30', $product->code, 10.2);
        $orderProduct2 = $this->buildOrderProductToAClient($client1, '30', $product2->code, 10.1);

        $fefo = factory(Fefo::class)->create([
            'product_code' => $orderProduct1->product_id,
            'leadtime_code' => $orderProduct1->origin_code,
            'selling_price' => '10.20'
        ]);

        $outDatedFefo = factory(Fefo::class)->create([
            'product_code' => $orderProduct2->product_id,
            'leadtime_code' => $orderProduct2->origin_code,
            'selling_price' => '10.10',
            'date' => Carbon::now()->subDays(8)->format('Y-m-d'), //more than one week
        ]);

        $campaignClients1 = factory(CampaignClient::class)->create([
            'campaign_id' => $campaign->id,
            'client_id' => $client1->id,
            'send_at' => now(),
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/client/{$campaignClients1->id}");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'title' => __('notification.campaign.repurchase.title', ['product_name' => $product->name ]),
            'message' => __('notification.campaign.repurchase.message',
                [
                    'price' => formata_moeda($fefo->selling_price).'/'.ucfirst(strtolower($product->unit))
                ]),
        ]);
    }

    /**
     *  Test client message with a product, should return model not found exception
     *
     * @return null
     */
    public function testGetClientMessageWithoutProductShouldReturnModelNotFoundException()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'repurchase',
            'name' => 'Recompra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id
        ]);

        $client1 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $product = factory(Product::class)->create();

        $orderProduct1 = $this->buildOrderProductToAClient($client1, '31');
        $orderProduct2 = $this->buildOrderProductToAClient($client1, '31');

        $campaignClients1 = factory(CampaignClient::class)->create([
            'campaign_id' => $campaign->id,
            'client_id' => $client1->id,
            'send_at' => now(),
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/client/{$campaignClients1->id}");

        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'errors' => [
                [
                    'code' => __('auth.resource_not_found.code'),
                    'title' => __('auth.resource_not_found.title'),
                    'status' => 404,
                ]
            ]
        ]);
    }

    protected function buildOrderProductToAClient(
        $client,
        $daysFromNow,
        $productCode = null,
        $kgPrice = null
    ): OrderProduct {
        if(!$productCode) {
            $productCode = factory(Product::class)->create()->code;
        }

        $attributes = [
            'origin_code' => function () use ($client) {
                return factory(LeadTime::class)->states('canPurchase')->create([
                    'cnpj' => $client->cnpj,
                ])->code;
            },
            'product_id' => $productCode,
            'status_id' => Status::type(Status::APPROVED)->id,
            'order_id' => function () use ($client, $daysFromNow) {
                return factory(Order::class)->create([
                    'client_id' => $client->id,
                    'date' => Carbon::now()->subDays($daysFromNow)->format('Y-m-d'),
                    'status_id' => Status::type(Status::APPROVED)->id,
                ]);
            }
        ];

        if (isset($kgPrice)) {
            $attributes['kg_price'] = $kgPrice;
        }

        return $ordersFromClient = factory(OrderProduct::class)->create($attributes);
    }
}
