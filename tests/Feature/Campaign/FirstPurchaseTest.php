<?php

namespace Tests\Feature\Campaign;

use App\Campaign\Campaign;
use App\Campaign\CampaignClient;
use App\Campaign\CampaignType;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Products\Product;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class FirstPurchaseTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpMockedUser();
        $this->setUpMockedJwtHelipa();
    }

    /**
     *  Test get clients who didn't buy in 30 days
     *
     * @return null
     */
    public function testFirstPurchaseWithOneClientThatNeverBought()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'firstPurchase',
            'name' => 'Primeira compra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id,
        ]);

        $client1 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $leadtime =  factory(LeadTime::class)->create([
            'cnpj' => $client1->cnpj,
            'name' => 'SAO PAULO - SP',
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'operates_saturday' => 1,
            'operates_sunday' => 1,
        ]);

        $secondClientPurchased = $this->buildOrderProductToAClient($client2, 1);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/{$campaign->id}/clients");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'total' => 1,
            'clientIds' => [
                $client1->id,
            ]
        ]);
    }

    /**
     *  Test get clients who didn't buy in 30 days
     *
     * @return null
     */
    public function testFirstPurchaseGetMessage()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'firstPurchase',
            'name' => 'Primeira compra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id,
        ]);

        $typology = array_rand(['sasuke', 'naruto', 'souk']);

        $client1 = factory(Client::class)->create();
        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'typology' => $typology,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();
        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'typology' => $typology,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $campaignClients1 = factory(CampaignClient::class)->create([
            'campaign_id' => $campaign->id,
            'client_id' => $client1->id,
            'send_at' => now(),
        ]);

        $leadtime =  factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $client1->cnpj,
        ]);

        $product = factory(Product::class)->create();

        $secondClientPurchased = $this->buildOrderProductToAClient($client2, 0, $leadtime->code);

        $fefo = factory(Fefo::class)->create([
            'product_code' => $secondClientPurchased->product_id,
            'leadtime_code' => $secondClientPurchased->origin_code
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/client/{$campaignClients1->id}");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $content = json_decode($response->getContent(), true);

        $product = Product::where(['codigo' => $fefo->product_code])->first();

        $response->assertJsonFragment([
            'title' =>  __("notification.campaign.first_purchase.title", [
                'company_name' => strtoupper($this->company->name),
            ]),
            'message' => __("notification.campaign.first_purchase.message", [
                'price' => formata_moeda($fefo->selling_price).'/'.ucfirst(strtolower($product->unit)),
                'product_name' => $product->name
            ])
        ]);
    }

    /**
     *  Test get clients who didn't buy in 30 days
     *
     * @return null
     */
    public function testFirstPurchaseWithoutProduct()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'firstPurchase',
            'name' => 'Primeira compra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id,
        ]);

        $typology = array_rand(['sasuke', 'naruto', 'souk']);

        $client1 = factory(Client::class)->create();

        $clientCompany1 = factory(ClientCompany::class)->create([
            'typology' => $typology,
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();

        $clientCompany2 = factory(ClientCompany::class)->create([
            'typology' => $typology,
            'client_id' => $client2->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,

        ]);

        $campaignClients1 = factory(CampaignClient::class)->create([
            'campaign_id' => $campaign->id,
            'client_id' => $client1->id,
            'send_at' => now(),
        ]);

        $leadtime =  factory(LeadTime::class)->create([
            'cnpj' => $client1->cnpj,
            'name' => 'SAO PAULO - SP',
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'operates_saturday' => 1,
            'operates_sunday' => 1,
        ]);

        $secondClientPurchased = $this->buildOrderProductToAClient($client2, 0, $leadtime->code);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/client/{$campaignClients1->id}");

        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'errors' => [
                [
                    'code' => __('auth.resource_not_found.code'),
                    'title' => __('auth.resource_not_found.title'),
                    'status' => 404,
                ]
            ]
        ]);
    }

    /**
     *  Test get clients who didn't buy in 30 days
     *
     * @return null
     */
    public function testFirstPurchaseWithoutClients()
    {
        $campaignType = factory(CampaignType::class)->create([
            'uid' => 'firstPurchase',
            'name' => 'Primeira compra'
        ]);

        $campaign = factory(Campaign::class)->create([
            'campaign_type_id' => $campaignType->id,
        ]);

        $client1 = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'client_id' => $client1->id,
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
            'account_balance' => 1000,
            'min_order' => 100,
        ]);

        $client2 = factory(Client::class)->create();

        $firstClientPurchased = $this->buildOrderProductToAClient($client1, 1);
        $secondClientPurchased = $this->buildOrderProductToAClient($client2, 1);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/campaign/{$campaign->id}/clients");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $content = json_decode($response->getContent(), true);

        $response->assertJsonFragment([
            'total' => 0,
        ]);
    }

    /**
     * Method to build a product to a client
     * @param object $client      object client
     * @param int    $daysFromNow days from now
     * @param int    $code        leadtime code
     * @return OrderProduct
     */
    protected function buildOrderProductToAClient($client, $daysFromNow = 0, $code = 2) : OrderProduct
    {
        return $ordersFromClient = factory(OrderProduct::class)->create([
            'origin_code' => function () use ($client, $daysFromNow, $code) {
                return factory(LeadTime::class)->states('canPurchase')->create([
                    'cnpj' => $client->cnpj,
                    'code' => $code
                ])->code;
            },
            'order_id' => function () use ($client, $daysFromNow) {
                return factory(Order::class)->create([
                    'client_id' => $client->id,
                    'date' => Carbon::now()->subDays($daysFromNow)->format('Y-m-d')
                ]);
            }
        ]);
    }
}
