<?php
namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\NotificationOrigin\NotificationOrigin;
use App\LeadTime\LeadTime;
use App\Company\Company;

class NotificationOriginControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();

        $this->user->root = 1;
        $this->user->save();
    }

    /**
     * @return void
     **/
    public function testEmptyList()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/notification_origins');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['data' => []]);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * Test get two range_price objects with success
     * @return void
     **/
    public function testGetRequestShouldReturnTwoResults()
    {
        $notificationOrigin = factory(NotificationOrigin::class, 2)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/notification_origins');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                [
                    'type' => 'notification_origin',
                    'id' => $notificationOrigin[0]->id,
                    'attributes' => [
                        'origin_code' => $notificationOrigin[0]->origin_code,
                        'subject' => $notificationOrigin[0]->subject,
                        'message' => $notificationOrigin[0]->message,
                        'total_sent' => $notificationOrigin[0]->total_sent,
                        'day_period' => $notificationOrigin[0]->day_period,
                        'processed_at' => $notificationOrigin[0]->processed_at,
                        'scheduled_at' => $notificationOrigin[0]->scheduled_at
                    ]
                ],
                [
                    'type' => 'notification_origin',
                    'id' => $notificationOrigin[1]->id,
                    'attributes' => [
                        'origin_code' => $notificationOrigin[1]->origin_code,
                        'subject' => $notificationOrigin[1]->subject,
                        'message' => $notificationOrigin[1]->message,
                        'total_sent' => $notificationOrigin[1]->total_sent,
                        'day_period' => $notificationOrigin[1]->day_period,
                        'scheduled_at' => $notificationOrigin[1]->scheduled_at
                    ]
                ]
            ]
        ], $content);
    }

    /**
     * Test get request for diferent company, user should see just one
     * @return void
     **/
    public function testGetRequestForDiferentCompaniesUseShouldSeeJustOne()
    {
        $notificationOrigin = factory(NotificationOrigin::class)->create();
        $notificationOriginForOtherCompany = factory(NotificationOrigin::class)->create(
            [
                'company_id' => factory(Company::class)->create()
            ]
        );

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/notification_origins');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                [
                    'type' => 'notification_origin',
                    'id' => $notificationOrigin->id,
                    'attributes' => [
                        'origin_code' => $notificationOrigin->origin_code,
                        'subject' => $notificationOrigin->subject,
                        'message' => $notificationOrigin->message,
                        'total_sent' => $notificationOrigin->total_sent,
                        'day_period' => $notificationOrigin->day_period,
                        'processed_at' => $notificationOrigin->processed_at,
                        'scheduled_at' => $notificationOrigin->scheduled_at
                    ]
                ]
            ]
        ], $content);
    }

    /**
     * Test get request for user non root should return unauthorized
     * @return void
     **/
    public function testGetRequestForUserNonRootShouldReturnUnauthorized()
    {
        $this->user->root = 0;
        $this->user->save();

        $notificationOrigin = factory(NotificationOrigin::class)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/notification_origins');
        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'errors' => [
                [
                    'code' => __('auth.unauthorized.code'),
                    'title' => __('auth.unauthorized.title'),
                    'status' => 403
                ]
            ]
        ]);
    }

    /**
     * Test create a notification origin with success
     * @return void
     **/
    public function testCreateNotificationOriginWithSuccess()
    {
        $notificationOrigin = factory(NotificationOrigin::class)->make([
            'scheduled_at' => now()->addDay()->toDateString()
        ]);
        $notificationOrigin2 = factory(NotificationOrigin::class)->make();

        $data = [
          "origin_code" => [$notificationOrigin->origin_code, $notificationOrigin2->origin_code],
          "subject" => $notificationOrigin->subject,
          "message" => $notificationOrigin->message,
          "day_period" => $notificationOrigin->day_period,
          "scheduled_at" => $notificationOrigin->scheduled_at,
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/notification_origins', $data);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $response->assertJson([
            'data' => [
                [
                    'type' => 'notification_origin',
                    'id' => $content['data'][0]['id'],
                    'attributes' => [
                        'origin_code' => $notificationOrigin->origin_code,
                        'subject' => $notificationOrigin->subject,
                        'message' => $notificationOrigin->message,
                        'day_period' => $notificationOrigin->day_period,
                        'scheduled_at' => $notificationOrigin->scheduled_at,
                    ]
                ],
                [
                    'type' => 'notification_origin',
                    'id' => $content['data'][1]['id'],
                    'attributes' => [
                        'origin_code' => $notificationOrigin2->origin_code,
                        'subject' => $notificationOrigin->subject,
                        'message' => $notificationOrigin->message,
                        'day_period' => $notificationOrigin->day_period,
                        'scheduled_at' => $notificationOrigin->scheduled_at,
                    ]
                ]
            ]
        ]);

        $this->assertEquals(2, NotificationOrigin::count());
    }

    /**
     * Test create request for user non root should return unauthorized
     * @return void
     **/
    public function testCreateRequestForUserNonRootShouldReturnUnauthorized()
    {
        $this->user->root = 0;
        $this->user->save();

        $notificationOrigin = factory(NotificationOrigin::class)->make();

        $data = [
          "origin_code" => [$notificationOrigin->origin_code],
          "subject" => $notificationOrigin->subject,
          "message" => $notificationOrigin->message,
          "day_period" => $notificationOrigin->day_period,
          "scheduled_at" => $notificationOrigin->scheduled_at,
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/notification_origins', $data);
        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'errors' => [
                [
                    'code' => __('auth.unauthorized.code'),
                    'title' => __('auth.unauthorized.title'),
                    'status' => 403
                ]
            ]
        ]);
    }

    /**
     * Test create notification origin with same origin and day_period should return error
     * @return void
     **/
    public function testCreateNotificationOriginWithSameOriginAndDayPeriodShouldReturnError()
    {
        $this->markTestSkipped(
            'Temporarily disable this rule'
        );

        $origin_code = factory(LeadTime::class)->create()->code;

        $notificationOrigin = factory(NotificationOrigin::class)->create([
            'origin_code' => $origin_code,
            'scheduled_at' => now()->addDay()->toDateString(),
            'day_period' => 'morning',
        ]);

        $notificationOrigin2 = factory(NotificationOrigin::class)->make([
            'origin_code' => $origin_code,
            'scheduled_at' => now()->addDay()->toDateString(),
            'day_period' => 'morning',
        ]);

        $data = [
          "origin_code" => [$notificationOrigin2->origin_code],
          "subject" => $notificationOrigin2->subject,
          "message" => $notificationOrigin2->message,
          "day_period" => $notificationOrigin2->day_period,
          "scheduled_at" => $notificationOrigin2->scheduled_at,
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/notification_origins', $data);

        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'description' => __('NotificationOrigin/errors.duplicated_schedule',
                ['period' => $notificationOrigin2->day_period, 'origin' => $notificationOrigin2->origin_code])
        ]);
    }

    /**
     * Test update request for user from diferent company should return unauthorized
     * @return void
     **/
    public function testUpdateRequestForUserFromDiferentCompanyShouldReturnUnauthorized()
    {
        $notificationOriginForOtherCompany = factory(NotificationOrigin::class)->create(
            [
                'company_id' => factory(Company::class)->create()
            ]
        );

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put(
                "/api/v1/notification_origins/{$notificationOriginForOtherCompany->id}",
                $notificationOriginForOtherCompany->toArray()
            )
        ;

        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'errors' => [
                [
                    'code' => __('auth.unauthorized.code'),
                    'title' => __('auth.unauthorized.title'),
                    'status' => 403
                ]
            ]
        ]);
    }

    /**
     * test update notification origin with success
     * @return void
     **/
    public function testUpdateNotificationOriginWithSuccess()
    {
        $notificationOrigin = factory(NotificationOrigin::class)->create([
            'processed_at' => null
        ]);

        $notificationOriginPayload = factory(NotificationOrigin::class)->make();

        $payload = [
            'id' => $notificationOrigin->id,
            'subject' => $notificationOriginPayload->subject,
            'message' => $notificationOriginPayload->message
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/notification_origins/{$notificationOrigin->id}", $payload);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                'type' => 'notification_origin',
                'id' => $notificationOrigin->id,
                "attributes" => [
                    "origin_code" => $notificationOrigin->origin_code,
                    "subject" => $notificationOriginPayload->subject,
                    "message" => $notificationOriginPayload->message,
                    "day_period" => $notificationOrigin->day_period,
                    "scheduled_at" => $notificationOrigin->scheduled_at
                ]
            ]
        ], $content);
    }

    /**
     * Test delete notification origin with success
     * @return void
     **/
    public function testDeleteNotificationOriginWithSuccess()
    {
        $notificationOrigin = factory(NotificationOrigin::class)->create([
            'processed_at' => null
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->delete("/api/v1/notification_origins/{$notificationOrigin->id}");

        $response->assertStatus(204);

        $this->assertSoftDeleted('notification_origins', ['id' => $notificationOrigin->id]);
    }

    /**
     * Test user can't delete notification origin from other company should return unauthorized
     * @return void
     **/
    public function testCantDeleteNotificationOriginFromOtherCompanyShouldReturnUnauthorized()
    {
        $notificationOriginForOtherCompany = factory(NotificationOrigin::class)->create(
            [
                'company_id' => factory(Company::class)->create()
            ]
        );

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->delete(
                "/api/v1/notification_origins/{$notificationOriginForOtherCompany->id}",
                $notificationOriginForOtherCompany->toArray()
            )
        ;

        $response->assertStatus(403);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'errors' => [
                [
                    'code' => __('auth.unauthorized.code'),
                    'title' => __('auth.unauthorized.title'),
                    'status' => 403
                ]
            ]
        ]);
    }

    /**
     * Test user can't delete or update notification origin, should return error
     * @return void
     **/
    public function testCantDeleteOrUpdateNotificationOriginShouldReturnError()
    {
        $notificationOrigin = factory(NotificationOrigin::class)->create([
            'processed_at' => now()
        ]);

        $payload = [
            'message' => 'Test'
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/notification_origins/{$notificationOrigin->id}", $payload);

        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            'description' => __('notAllowed.update')
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->delete("/api/v1/notification_origins/{$notificationOrigin->id}");

        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            'description' => __('notAllowed.delete')
        ]);

    }

    /**
     * @return void
     */
    public function testMessageTooLongShouldReturnError()
    {
        $payload = [
            'origin_code' => [factory(LeadTime::class)->create()->code],
            'subject' => $this->faker->sentence(),
            'message' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Praesent ultrices nisl vel dolor ullamcorper, quis finibus massa ultrices.
                Aliquam vel consectetur mauris, quis tincidunt turpis.',
            'day_period' => 'night',
            'scheduled_at' => now()->addDay()->format('Y-m-d'),
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/notification_origins', $payload);

        $response->assertStatus(422);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([__('validation.max.string', ['attribute' => 'mensagem', 'max' => 172])]);
    }
}
