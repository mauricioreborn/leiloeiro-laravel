<?php
namespace Tests\Feature\Mobile;

use App\Bid\Bid;
use App\Fefo\Fefo;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class FefoControllerTest  extends TestCase
{
    use DatabaseTransactions;

    /**
     * setUp
     * @return void
     **/
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    public function testGetFefoBidsOnMobile()
    {
        $fefo = factory(Fefo::class)->create([
            'company_id' => $this->company->id
        ]);

        $bid = factory(Bid::class)->create([
            'origin_code' => $fefo->leadtime_code,
            'weeks' => $fefo->weeks,
            'kg_price'=> $fefo->selling_price,
            'price'=> $fefo->selling_price,
            'product_code' => $fefo->product_code,
            'company_id' => $fefo->company_id,
            'date' => Carbon::now()->subDay(5)
        ]);


        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->get("/api/v1/mobile/company/{$this->company->id}/fefo/$fefo->id/bids");

        
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'type' => 'bid',
            'attributes' => [
                "date" =>  Carbon::parse($bid->date)->format('d/m'),
                'kg_price' => formata_moeda($bid->kg_price),
                'price' => formata_moeda($bid->price),
                'unitPrice' => unitPrice($bid->kg_price, $bid->product->weight_piece),
                'volume' => volume($bid->weight) . 'Kg',
            ]
        ]);
    }
}