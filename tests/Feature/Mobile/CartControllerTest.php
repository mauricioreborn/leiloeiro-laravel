<?php


namespace Tests\Feature\Mobile;

use App\Cart\Cart;
use App\Cart\ProductCart;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\Seller;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CartControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Tests add cart with success
     *
     * @return void
     */
    public function testAddCartWithSuccess()
    {
        $client = factory(Client::class)->create([
            'name' => 'sasuke'
        ]);

        $clientCompany = factory(ClientCompany::class)->create([
            'status' => 1,
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 150,
            'account_balance' => 9000
        ]);

        $this->setUpMockJwtMobileClient($client->id);

        $fefo = factory(Fefo::class)->create([
            'selling_price' => 250,
            'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            },
            'product_code' => function () {
                return factory(Product::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            },
        ]);

        $leadTime = LeadTime::where('code', $fefo->leadtime_code)->first();

        $leadTime->update(['cnpj' => $client->cnpj]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/add_carrinho", [
                'produto_id' => $fefo->id,
                'qtd_caixas' => '2'
            ]
        );

        $cart = new Cart();

        $clientCart = $cart->where('id_cliente', $client->id)->first();

        $productCart = new ProductCart();

        $productCart  = $productCart->where('carrinho_id', $clientCart->id)->first();

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $valorFaltante = formata_moeda($clientCompany->min_order - $productCart->valor) > 0 ?
            formata_moeda($clientCompany->min_order - $productCart->valor) :
            0;
        $response->assertJsonFragment(
            [
                'status' => true,
                'valorMinimo' =>  formata_moeda($clientCompany->min_order),
                 'valor' => formata_moeda($productCart->valor),
                'valorFaltante' => $valorFaltante
            ]
        );
    }

    /**
     * Tests add cart with success
     *
     * @return void
     */
    public function testAddCartWithInvalidClientStatusShouldReturnError()
    {
        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            },
            'product_code' => function () {
                return factory(Product::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            },
        ]);

        $client = factory(Client::class)->create([
            'name' => 'sasuke',
        ]);


        $clientCompany = factory(ClientCompany::class)->create([
            'client_id' => $client->id,
            'company_id' => $this->company->id,
            'min_order' => 9000,
            'account_balance' => 0,
            'status' => 0
        ]);

        $this->setUpMockJwtMobileClient($client->id);


        $leadTime = LeadTime::where('code', $fefo->leadtime_code)->first();

        $leadTime->update(['cnpj' => $client->cnpj]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/add_carrinho", [
                'produto_id' => $fefo->id,
                'qtd_caixas' => '1'
            ]
        );

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            'status' => false,
            'msg' => __('Mobile/bid.bids_and_buys_blocked')
        ]);
    }

    /**
     * Test add cart validators
     * @param field $field name
     * @param value $value value
     * @param msg   $msg   message
     * @dataProvider validatorsParams
     * @return void
     */
    public function testAddCartValidators($field, $value,  $msg)
    {
        $this->setUpMockJwtMobileClient($this->client->id);

        $params = [
            'access_token' => $this->jwt,
            'produto_id' => 1,
            'qtd_caixas' => '1'
        ];

        $params[$field] = $value;

        $response = $this->post("/api/v1/mobile/company/{$this->company->id}/add_carrinho", $params);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment(
            [
                'status' => false,
                'msg' => $msg,
                'message' => $msg
            ]
        );
    }

    /**
     * Tests list cart with Sucess
     *
     * @return void
     */
    public function testListACartWithSuccess()
    {
        $client = factory(Client::class)->create([
        ]);

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 150,
            'address' => 'konoha',
            'city' => 'Japan'
        ]);

        $this->setUpMockJwtMobileClient($client->id);

        $product = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'app_exhibition' => '6kg(12x500g)',
        ]);

        $fefo = factory(Fefo::class)->create([
                'product_code'  => $product->code,
                'selling_price'  => 300,
                'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            }]
        );

        $leadTime = LeadTime::where('code', $fefo->leadtime_code)->first();

        $leadTime->update(['cnpj' => $client->cnpj]);

        $cartModel = new Cart();
        $cart = $cartModel->create(
            [
                'id_cliente' => $client->id,
                'data' => date('Y-m-d'),
                'status' => '1',
                'company_id' => $this->company->id,
            ]
        );
        $cartProduct = new ProductCart();
        $cartProduct = $cartProduct->create(
            [
                'carrinho_id' => $cart->id,
                'cod_produto' => $fefo->product_code,
                'cod_origem' => $fefo->leadtime_code,
                'semanas' => $fefo->weeks,
                'qtd_caixas' => 2,
                'valor_kg' => $fefo->selling_price,
                'total_kg' => ($fefo->selling_price * 2),
                'total' => ($fefo->selling_price * 3),
                'valor' => $fefo->selling_price * ($fefo->selling_price * 2),
                'status' => 1
            ]
        );

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/carrinho");

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            "appExhibition" => "6kg(12x500g)"
        ], $content['produtos'][$cartProduct->id]);

        $response->assertJsonStructure([
            'status',
            'endereco',
            'total',
            'valorMinimo',
            'temGrade',
            'permiteFinalizar',
            'data_entrega',
            'produtos'  => [
                $cartProduct->id => [
                    'nome',
                    'empresa',
                    'img',
                    'peso',
                    'caixa',
                    'validade',
                    'preco',
                    'preco_float',
                    'precoTotal',
                    'pesoCaixa',
                    'precoAtualUn',
                    'precoUn',
                    'unidadeMedida',
                    'padraoMedida',
                    'gramatura',
                    'disponivel',
                    'logistica'
                ]
            ]
        ]);
    }

    /**
     * Tests list cart with min order label
     *
     * @return void
     */
    public function testListCartWithMinOrderLabel()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'client_id' => $client->id,
            'min_order' => 150
        ]);

        $this->setUpMockJwtMobileClient($client->id);

        $leadtime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $product = factory(Product::class)->create([
            'weight' => 5
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => now()->toDateString(),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 10,
            'max_price' => 11,
            'min_price' => 8,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);

        $cart = Cart::create([
            'id_cliente' => $client->id,
            'data' => now()->toDateString(),
            'status' => '1',
            'company_id' => $this->company->id,
        ]);

        $cartProduct = ProductCart::create([
            'carrinho_id' => $cart->id,
            'cod_produto' => $fefo->product_code,
            'cod_origem' => $fefo->leadtime_code,
            'semanas' => $fefo->weeks,
            'qtd_caixas' => 2,
            'valor_kg' => $fefo->selling_price,
            'total_kg' => ($product->weight * 2),
            'valor' => $fefo->selling_price * ($product->weight * 2),
            'status' => 1
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/carrinho");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            "min_order_label" => __('Mobile/cart.min_order', ['value' => 'R$ 50,00'])
        ], $content);

        $cartProduct->qtd_caixas = 20;
        $cartProduct->total_kg = ($product->weight * 20);
        $cartProduct->valor = $fefo->selling_price * ($product->weight * 20);
        $cartProduct->save();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/carrinho");

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            "min_order_label" => false
        ], $content);
    }

    /**
     * @return array
     **/
    public function validatorsParams()
    {
        return [
            [
                'field' => 'produto_id',
                'value' => '',
                'message' => 'O campo identificador do produto é obrigatório.'
            ],
             [
                 'field' => 'qtd_caixas',
                 'value' => '',
                 'message' => 'O campo qtd caixas é obrigatório.'
             ],
        ];
    }

    /**
     * Tests change product from cart with success
     *
     * @return void
     */
    public function testChangeProductCartWithSuccess()
    {
        $client = $this->client;

        $boxAmount = 10;
        $boxWeight = 10;

        $cart = factory(Cart::class)->create([
            'id_cliente' => $client->id
        ]);

        $product = factory(Product::class)->create([
            'weight' => $boxWeight
        ]);

        $productCart =  factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'cod_produto' => $product->code,
            'qtd_caixas' => $boxAmount
        ]);

        $faker = \Faker\Factory::create();

        $fefo = factory(Fefo::class)->create([
            'date'  =>  date('Y-m-d'),
            'weeks'  => $productCart->semanas,
            'leadtime_code' => $productCart->cod_origem,
            'product_code' =>  $productCart->cod_produto,
            'selling_price' => $productCart->valor_kg,
            'volume' => 900
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/change_item_carrinho", [
                'produto_id' => $productCart->id,
                'qtd_caixas' => $boxAmount
            ]
        );

        $response->assertJsonFragment([
            'status' => true,
            'message' => 'product has been updated'
        ]);
    }

    /**
     * Tests change product from cart with success
     *
     * @return void
     */
    public function testUpdateCartWithoutVolumeShouldReturnException()
    {
        $client = $this->client;

        $boxAmount = 10;
        $boxWeight = 10;

        $cart = factory(Cart::class)->create([
            'id_cliente' => $client->id
        ]);

        $product = factory(Product::class)->create([
            'weight' => $boxWeight
        ]);

        $productCart =  factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'cod_produto' => $product->code,
            'qtd_caixas' => $boxAmount
        ]);

        $faker = \Faker\Factory::create();

        $fefo = factory(Fefo::class)->create([
            'date'  =>  date('Y-m-d'),
            'weeks'  => $productCart->semanas,
            'leadtime_code' => $productCart->cod_origem,
            'product_code' =>  $productCart->cod_produto,
            'selling_price' => $productCart->valor_kg,
            'volume' => 90
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/change_item_carrinho", [
                'produto_id' => $productCart->id,
                'qtd_caixas' => $boxAmount
            ]
        );

        $boxAvailable = floor($fefo->volume / $product->weight);

        $response->assertJsonFragment([
            'status' => false,
            'msg' => trans_choice('Mobile/cart.volume_unavailable', $boxAvailable, ['value' => $boxAvailable]),
            'message' => trans_choice('Mobile/cart.volume_unavailable', $boxAvailable, ['value' => $boxAvailable])
        ]);
    }

    /**
     * Tests remove a product from cart validator
     *
     * @return void
     */
    public function testRemoveAProductFromCartValidator()
    {
        $cart = factory(Cart::class)->create(['company_id' => $this->company->id]);
        $productCart =  factory(ProductCart::class)->create(
            [
                'carrinho_id' => $cart->id,
            ]
        );

        $response = $this->post("/api/v1/mobile/company/{$this->company->id}/remove_item_carrinho", [
            'access_token' => $this->jwt,
        ]);

        $response->assertJsonFragment([
            'status' => false,
            'message' => 'O campo identificador do produto é obrigatório.'
        ]);
    }

    /**
     * Tests remove a product from cart with success
     *
     * @return void
     */
    public function testRemoveAProductFromCartWithSuccess()
    {
        $cart = factory(Cart::class)->create([
            'id_cliente' => $this->client->id,
            'company_id' => $this->company->id]);
        $productCart =  factory(ProductCart::class)->create(
            [
                'carrinho_id' => $cart->id
            ]
        );

        $response = $this->post("/api/v1/mobile/company/{$this->company->id}/remove_item_carrinho", [
            'access_token' => $this->jwt,
            'produto_id' => $productCart->id,
        ]);

        $response->assertJsonFragment([
            'status' => true,
            'msg' => 'Produto removido com sucesso!',
            'message' => 'Produto removido com sucesso!'
        ]);

    }

    /**
     * Tests remove a product from cart with error
     *
     * @return void
     */
    public function testRemoveAProductFromCartWithError()
    {
        $faker = \Faker\Factory::create();

        $response = $this->post("/api/v1/mobile/company/{$this->company->id}/remove_item_carrinho", [
            'access_token' => $this->jwt,
            'produto_id' => $faker->numberBetween(100,999),
        ]);

        $response->assertJsonFragment([
            'status' => false,
            'message' => 'carrinho não encontrado'
        ]);
    }
}
