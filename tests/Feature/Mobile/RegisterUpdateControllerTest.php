<?php
namespace Tests\Feature\Mobile;

use App\Bid\Bid;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Company\Company;
use App\Core\Entities\Status;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class RegisterUpdateControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to start a setup
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpJwtClientWithSpecificallyCompanyId(Company::skip(3)->first());
    }

    /**
     * Method to get and register update with register update false and waiting_approval true
     * @return void
     */
    public function testRegisterUpdateReturnWithRegisterUpdateFalse()
    {

        $this->client->register_updated = true;
        $this->client->save();


        $clientCompany = ClientCompany::where([
            'client_id' => $this->client->id,
            'company_id' => $this->company->id
        ])->first();

        $clientCompany->status = 0;
        $clientCompany->min_order = 5000;
        $clientCompany->account_balance = 10;
        $clientCompany->save();

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->get(
            "/api/v1/mobile/company/{$this->company->id}/register_updated");

        $response->assertJsonFragment([
            'register_updated' => false,
            'waiting_approval' => true,
        ]);
    }

    /**
     * Method to get and register update with register update false and waiting_approval true
     * @return void
     */
    public function testRegisterUpdateReturnWithRegisterUpdateTrue()
    {
        $this->client->register_updated = false;
        $this->client->save();


        $clientCompany = ClientCompany::where([
            'client_id' => $this->client->id,
            'company_id' => $this->company->id
        ])->first();

        $clientCompany->status = 1;
        $clientCompany->min_order = 5000;
        $clientCompany->account_balance = 10;
        $clientCompany->save();

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->get(
            "/api/v1/mobile/company/{$this->company->id}/register_updated");

        $response->assertJsonFragment([
            'register_updated' => true,
            'waiting_approval' => false,
        ]);
    }

    /**
     * Method to get and register update with register update false and waiting_approval true
     * @return void
     */
    public function testRegisterUpdateReturnWithRegisterUpdateFalseAndWaitingFalse()
    {

        $this->client->register_updated = true;
        $this->client->save();


        $clientCompany = ClientCompany::where([
            'client_id' => $this->client->id,
            'company_id' => $this->company->id
        ])->first();

        $clientCompany->status = 1;
        $clientCompany->min_order = 5000;
        $clientCompany->account_balance = 10;
        $clientCompany->save();

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->get(
            "/api/v1/mobile/company/{$this->company->id}/register_updated");

        $response->assertJsonFragment([
            'register_updated' => false,
            'waiting_approval' => false,
        ]);
    }

    public function testGetBannerOfRegisterUpdatedWithUpdatedTrue()
    {
        $statusAuthorized = Status::type(Status::AUTHORIZED);

        $this->client->confirm_sms = $statusAuthorized->id;
        $this->client->confirm_email = $statusAuthorized->id;

        $this->client->register_updated = false;
        $this->client->save();

        $clientCompany = ClientCompany::where([
            'client_id' => $this->client->id,
            'company_id' => $this->company->id
        ])->first();

        $clientCompany->status = 1;
        $clientCompany->min_order = 5000;
        $clientCompany->account_balance = 10;
        $clientCompany->save();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/home", ['version' => "0.5.0"]);

        $response->assertJsonFragment([
            'body' => __('Mobile/company.banner.register_updated.body'),
            'color' => 'rgb(255, 255, 255)',
            'icon' => 'https://souk-company.s3.amazonaws.com/4/banner/icon-attention-white.svg',
            'page' => 'AtualizarCadastroPage',
            'resource' => null
        ]);
    }

    public function testGetBannerOfRegisterUpdatedWithUpdatedFalse()
    {
        $statusAuthorized = Status::type(Status::AUTHORIZED);

        $this->client->confirm_sms = $statusAuthorized->id;
        $this->client->confirm_email = $statusAuthorized->id;

        $this->client->register_updated = true;
        $this->client->save();

        $clientCompany = ClientCompany::where([
            'client_id' => $this->client->id,
            'company_id' => $this->company->id
        ])->first();

        $clientCompany->status = 0;
        $clientCompany->min_order = 5000;
        $clientCompany->account_balance = 10;
        $clientCompany->save();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/home", ['version' => "0.5.0"]);

        $response->assertJsonFragment([
            'body' => __('Mobile/company.banner.waiting_approval.body'),
            'color' => 'rgb(102, 102, 102)',
            'background_color' => 'linear-gradient(-135deg, rgb(238, 238, 238) 0%,rgb(238, 238, 238) 70%,rgb(238, 238, 238) 100%)',
            'icon' => 'https://souk-company.s3.amazonaws.com/4/banner/icon-attention-gray.svg',
            'page' => false,
            'resource' => null
        ]);
    }


}