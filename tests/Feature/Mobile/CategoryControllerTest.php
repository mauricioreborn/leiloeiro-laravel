<?php

namespace Tests\Feature\Mobile;

use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    public function testCategoryList()
    {
        $clientCompany = $this->client->setClientCompany($this->company);

        $clientCompany->track_1 = 1;
        $clientCompany->track_2 = 1;
        $clientCompany->track_3 = 0;
        $clientCompany->track_4 = 0;

        $clientCompany->save();
        $this->client->fresh();

        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj
        ]);

        $product = factory(Product::class)->create([
            'category' => 'category 1'
        ]);

        $product2 = factory(Product::class)->create([
            'category' => 'category 2'
        ]);

        $product3 = factory(Product::class)->create([
            'category' => 'category 3'
        ]);

        $fefoHhighlightApp = factory(Fefo::class)->create([
            'highlight_app' => 1,
            'product_code' => $product->code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString(),
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001'
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product2->code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString(),
            'cd_faixa_fefo' => '500002',
            'faixa_fefo' => '002'
        ]);

        $fefo3 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product3->code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString(),
            'cd_faixa_fefo' => '500004',
            'faixa_fefo' => '004'
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/categorias");
        $response->assertStatus(200);

        $response->assertJsonFragment([
            [
                "area" => "CATEGORY 1",
                "custom_view" => false,
                "total_produtos" => "1"
            ]
        ]);

        $response->assertJsonFragment([
            [
                "area" => "CATEGORY 2",
                "custom_view" => false,
                "total_produtos" => "1"
            ]
        ]);

        $response->assertJsonFragment([
            [
                "area" => "DESTAQUES",
                "custom_view" => 'destaques',
                "total_produtos" => "1"
            ]
        ]);

        $response->assertJsonMissing([
            "area" => "CATEGORY 3",
        ]);
    }

    public function testCategoryListWithoutHighlightFefo()
    {
        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj
        ]);

        $product = factory(Product::class)->create([
            'category' => 'category 1'
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product->code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString()
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/categorias");
        $response->assertStatus(200);

        $response->assertJsonMissing([
            "area" => "DESTAQUES"
        ]);
    }

    /**
     * @return void
     */
    public function testListaCustomWithoudTypologyCache()
    {
        $clientCompany = $this->client->setClientCompany($this->company);

        $clientCompany->track_1 = 1;
        $clientCompany->track_2 = 1;
        $clientCompany->track_3 = 0;
        $clientCompany->track_4 = 0;
        $clientCompany->typology = "Test";

        $clientCompany->save();
        $this->client->fresh();

        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj
        ]);

        $product = factory(Product::class)->create([
            'category' => 'category 1'
        ]);

        factory(Fefo::class)->create([
            'highlight_app' => 1,
            'product_code' => $product->code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString(),
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001'
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/lista_custom", ['custom_view' => 'para seu negocio']);

        $response->assertStatus(200);
        $this->assertArraySubset(["nome" => "Para seu negocio"], $response->decodeResponseJson());
    }
}
