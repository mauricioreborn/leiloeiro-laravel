<?php

namespace Tests\Feature\Mobile;

use App\Cart\Cart;
use App\Cart\ProductCart;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Company\Services\CompanySettingService;
use App\Core\Entities\Status;
use App\Core\Entities\RejectionMotive;
use App\Fefo\Fefo;
use App\Invoice\Invoice;
use App\Jobs\IntegrateInvoiceJob;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Payment\Payment;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class OrderControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Set Up
     * @return void
     **/
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * test client order list with all companies
     * @return void
     **/
    public function testClientOrderList()
    {
        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => function () {
                return factory(Order::class)->create([
                    'status_id' => Status::type(Status::PENDING)->id,
                    'client_id' => $this->client->id,
                ]);
            },
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $orderProduct2 = factory(OrderProduct::class)->create([
            'order_id' => function () {
                return factory(Order::class)->create([
                    'status_id' =>  Status::type(Status::APPROVED)->id,
                    'client_id' => $this->client->id,
                ]);
            },
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        $newCompany = Company::skip(2)->first();

        $orderProduct3 = factory(OrderProduct::class)->create([
            'order_id' => function () use ($newCompany) {
                return factory(Order::class)->create([
                    'status_id' =>  Status::type(Status::CANCELED)->id,
                    'client_id' => $this->client->id,
                    'company_id' => $newCompany->id
                ]);
            },
            'status_id' => Status::type(Status::CANCELED)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/lista_pedidos");
        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset(
            [
                'status' => true,
                'pendentes' => [
                    [
                        'id_pedido' => (string) $orderProduct->order_id,
                        'companyId' => $orderProduct->order->company_id
                    ]
                ],
                'aprovados' => [
                    [
                        'id_pedido' => (string) $orderProduct2->order_id,
                        'companyId' => $orderProduct2->order->company_id
                    ]
                ],
                'reprovados' => [
                    [
                        'id_pedido' => (string) $orderProduct3->order_id,
                        'companyId' => $newCompany->id
                    ]
                ]
            ],
            $content
        );
    }

    /**
     * test client order list with only pending
     * @return void
     **/
    public function testClientOrderListWithOnlyPendingAndOtherCompany()
    {
        $newCompany = factory(Company::class)->create();

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => function () use ($newCompany) {
                return factory(Order::class)->create([
                    'status_id' => Status::type(Status::PENDING)->id,
                    'client_id' => $this->client->id,
                    'company_id' => $newCompany
                ]);
            },
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/lista_pedidos");
        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset(
            [
                'status' => true,
                'pendentes' => [
                    [
                        'id_pedido' => (string) $orderProduct->order_id,
                        'companyId' => $orderProduct->order->company_id
                    ]
                ],
            ],
            $content
        );

        $response->assertJsonCount(2);
    }

    /**
     * test client order empty list
     * @return void
     **/
    public function testClientOrderEmptyList()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/lista_pedidos");
        $response->assertStatus(200);

        $response->assertJson([
            'status' => true
        ]);

        $response->assertJsonCount(1);
    }

    /**
     * test order show with success
     * @return void
     **/
    public function testOrderShow()
    {
        $statusPending = Status::type(Status::PENDING);
        $statusApproved = Status::type(Status::APPROVED);
        $statusCanceled = Status::type(Status::CANCELED);
        $invalidFefoRejectionMotive = RejectionMotive::type(RejectionMotive::INVALID_FEFO);

        $company = $this->company;

        $order = factory(Order::class)->create([
            'status_id' => $statusPending->id,
            'client_id' => $this->client->id,
            'rejection_motives_id' => $invalidFefoRejectionMotive
        ]);

        $productDeleted = factory(Product::class)->create([
            'weight' => 10,
            'weight_piece' => 10,
            'pieces' => 1,
            'company_id' => $this->company->id,
            'deleted_at' => now()
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => $statusPending->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => $statusPending->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => $statusApproved->id,
        ]);

        $orderWithProductDeleted = factory(OrderProduct::class)->create([
            'status_id' => $statusPending->id,
            'order_id' => $order->id,
            'product_id' => $productDeleted->code
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => $statusCanceled->id,
            'rejection_motives_id' => $invalidFefoRejectionMotive->id
        ]);

        $orderProduct = OrderProduct::all();

        $totalOrderProductPending = $orderProduct->where('status_id', $statusPending->id)->count();
        $totalOrderProductApproved = $orderProduct->where('status_id', $statusApproved->id)->count();
        $totalOrderProductCanceled = $orderProduct->where('status_id', $statusCanceled->id)->count();

        $statusLabelPending = __("status.order.{$statusPending->name}.block")." (".$totalOrderProductPending.")";
        $statusLabelApproved = __("status.order.{$statusApproved->name}.block")." (".$totalOrderProductApproved.")";
        $statusLabelCanceled = __("rejectionMotive.order.{$invalidFefoRejectionMotive->name}.block")." (".$totalOrderProductCanceled.")";

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/pedido", ['pedido_id' => $order->id]);

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status' => true,
            'totalProductsAnalyzed' => ($totalOrderProductApproved + $totalOrderProductCanceled),
            'totalProducts' => ($totalOrderProductApproved + $totalOrderProductCanceled + $totalOrderProductPending),
        ]);

        $response->assertJsonCount(3, 'produtos');
        $response->assertJsonCount(3, "produtos.".$statusLabelPending);
        $response->assertJsonCount(1, "produtos.".$statusLabelApproved);
        $response->assertJsonCount(1, "produtos.".$statusLabelCanceled);

        $response->assertJsonStructure([
            'status',
            'status_pedido',
            'aprovado',
            'pedido_id',
            'dataPedido',
            'valorTotal',
            'endereco',
            'produtos',
            'totalProducts',
            'totalProductsAnalyzed',
        ]);
    }

    /**
     * test canceled order show with success
     * @return void
     **/
    public function testCancelledOrderShow()
    {
        $statusCanceled = Status::type(Status::CANCELED);
        $invalidFefoRejectionMotive = RejectionMotive::type(RejectionMotive::INVALID_FEFO);

        $order = factory(Order::class)->create([
            'status_id' => $statusCanceled->id,
            'client_id' => $this->client->id,
            'rejection_motives_id' => $invalidFefoRejectionMotive
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => $statusCanceled->id,
            'rejection_motives_id' => $invalidFefoRejectionMotive->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/pedido", [
            'pedido_id' => $order->id
        ]);

        $response->assertStatus(200);

        $statusLabelCanceled = __("rejectionMotive.order.{$invalidFefoRejectionMotive->name}.block")." (".OrderProduct::count().")";

        $response->assertJsonStructure([
            'status',
            'status_pedido',
            'aprovado',
            'pedido_id',
            'dataPedido',
            'valorTotal',
            'endereco',
            'cancelado',
            'produtos' => [
                $statusLabelCanceled
            ]
        ]);
    }

    /**
     * test invalid show with error
     * @return void
     **/
    public function testInvalidOrderShow()
    {
        $other_client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
        'client_id' => $other_client->id]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'client_id' => $other_client->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/pedido", [
            'pedido_id' => $order->id
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => false,
            'message' => __('validation.exists', [
                'attribute' => __('validation.attributes.pedido_id')
            ])
        ]);

        $not_found = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/pedido", [
            'pedido_id' => 789456123
        ]);

        $not_found->assertStatus(200);
        $not_found->assertJsonFragment([
            'status' => false,
            'message' => __('validation.exists', [
                'attribute' => __('validation.attributes.pedido_id')
            ])
        ]);

        $without_id = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/pedido");

        $without_id->assertStatus(200);
        $without_id->assertJsonFragment([
            'status' => false,
            'message' => __('validation.required', [
                'attribute' => __('validation.attributes.pedido_id')
            ])
        ]);
    }

    /**
     * test finish order with success
     * @return void
     **/
    public function testFinishOrderWithSuccess()
    {
        $client = factory(Client::class)->create([
            'name' => 'sasuke'
        ]);

        $company = $this->company;

        $clientCompany = $this->client->setClientCompany($company);

        $clientCompany->min_order = 200;
        $clientCompany->account_balance = 1000;
        $clientCompany->save();

        $this->assertNull($clientCompany->first_cart_at);
        $this->assertNull($clientCompany->first_bid_at);


        $cart = factory(Cart::class)->create([
            'id_cliente' => $this->client->id
        ]);

        $productCart = factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'valor' => 300
        ]);

        $leadTime = factory(LeadTime::class)->create([
            'code' =>  $productCart->cod_origem,
        ]);

        $product = factory(Product::class)->create([
            'code' =>  $productCart->cod_produto,
        ]);

        $faker = \Faker\Factory::create();

        $fefo = factory(Fefo::class)->create([
            'selling_price' => 400,
            'date'  =>  date('Y-m-d'),
            'weeks'  => $productCart->semanas,
            'leadtime_code' => $productCart->cod_origem,
            'product_code' =>  $productCart->cod_produto,
            'selling_price' => $productCart->valor_kg,
        ]);

        $payment = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'client_id' => $this->client->id,
            'company_payment_id' => function () use ($company, $payment) {
                return factory(CompanyPayment::class)->create([
                    'company_id' => $company->id,
                    'payment_id' => $payment->id
                ]);
            }
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$company->id}/finaliza_pedido");

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $orderProduct = OrderProduct::where(['produto_id' => $productCart->cod_produto])
            ->orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('pedidos', [
            'id' => $orderProduct->order_id,
            'status_id' => Status::type(Status::PENDING)->id
        ]);

        $clientCompany = $clientCompany->fresh();

        $this->assertNotNull($clientCompany->first_cart_at);
        $this->assertNull($clientCompany->first_bid_at);
    }

    /**
     * Test finish order With Reduce Account balance
     * @return void
     **/
    public function testFinishOrderReduceAccountBalance()
    {
        (new CompanySettingService())->createReduceAccountBalanceCheckoutCart($this->company);

        $fefo = $this->createFefoCart($this->company);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido");

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $this->assertDatabaseHas('client_companies', [
            'company_id' => $this->company->id,
            'client_id' => $this->client->id,
            'account_balance' => 500
        ]);
    }

    /**
     * Test finish order With Reduce Account balance
     * @return void
     **/
    public function testFinishOrderReduceAccountBalanceWithCreditCard()
    {
        Queue::fake();

        (new CompanySettingService())->createReduceAccountBalanceCheckoutCart($this->company);

        $fefo = $this->createFefoCart($this->company, 1000);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido", [
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
            'aceita_parcial' => true,
        ]);

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $this->assertDatabaseHas('client_companies', [
            'company_id' => $this->company->id,
            'client_id' => $this->client->id,
            'account_balance' => 1000
        ]);
    }

    /**
     * Test finish order Without Reduce Account balance
     * @return void
     **/
    public function testFinishOrderWithoutReduceAccountBalance()
    {
        (new CompanySettingService())->createReduceAccountBalanceCheckoutCart($this->company, false);

        $fefo = $this->createFefoCart($this->company);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido");

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $this->assertDatabaseHas('client_companies', [
            'company_id' => $this->company->id,
            'client_id' => $this->client->id,
            'account_balance' => 1500
        ]);
    }

    /**
     * Test finish order With Reduce Volume Fefo
     * @return void
     **/
    public function testFinishOrderReduceVolumeFefo()
    {
        $fefo = $this->createFefoCart($this->company);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido");

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $this->assertDatabaseHas('fefo', [
            'id' => $fefo->id,
            'volume' => 900
        ]);
    }

    /**
     * Test finish order Without Reduce Volume Fefo
     * @return void
     **/
    public function testFinishOrderWithoutReduceVolumeFefo()
    {
        (new CompanySettingService())->createReduceVolumeFefoCheckoutCart($this->company, false);

        $fefo = $this->createFefoCart($this->company);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido");

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $this->assertDatabaseHas('fefo', [
            'id' => $fefo->id,
            'volume' => 1000
        ]);
    }

    /**
     * create client cart
     *
     * @param Company $company Company
     *
     * @return void
     */
    protected function createFefoCart(Company $company, $accountBalance = 1500, $boxAmount = 10)
    {
        $this->client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $company->id,
            'client_id' => $this->client->id,
            'min_order' => 200,
            'account_balance' => $accountBalance
        ]);


        $this->setUpMockJwtMobileClient($this->client->id);

        $cart = factory(Cart::class)->create([
            'id_cliente' => $this->client->id,
            'company_id' => $company->id,
        ]);

        $leadTime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $company->id
        ]);

        $product = factory(Product::class)->create([
            'weight' => 10,
            'weight_piece' => 10,
            'pieces' => 1,
            'company_id' => $company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'  =>  now()->toDateString(),
            'leadtime_code' => $leadTime->code,
            'product_code' =>  $product->code,
            'selling_price' => 10.0,
            'volume' => 1000.0,
        ]);

        $payment = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'client_id' => $this->client->id,
            'company_payment_id' => function () use ($company, $payment) {
                return factory(CompanyPayment::class)->create([
                    'company_id' => $company->id,
                    'payment_id' => $payment->id
                ]);
            }
        ]);

        $totalVolume = $boxAmount * 10;

        $productCart =  factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'cod_origem' => $leadTime->code,
            'cod_produto' => $product->code,
            'semanas' => $fefo->weeks,
            'qtd_caixas' => $boxAmount,
            'valor_kg' => 10,
            'total_kg' => $totalVolume,
            'valor' => $totalVolume * 10,
        ]);

        return $fefo;
    }

    /**
     * Test finish order with another company and check if its saved on database correctly
     * @return void
     **/
    public function testFinishOrderWithAnotherCompanyWithSuccess()
    {
        $client = factory(Client::class)->create([
            'name' => 'sasuke'
        ]);

        $company = factory(Company::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $company->id,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 1000
        ]);

        $this->setUpMockJwtMobileClient($client->id);

        $cart = factory(Cart::class)->create([
            'id_cliente' => $client->id,
            'company_id' => $company->id,
        ]);

        $productCart =  factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'valor' => 300
        ]);

        $leadTime = factory(LeadTime::class)->create([
            'code' =>  $productCart->cod_origem,
            'company_id' => $company->id
        ]);

        $product = factory(Product::class)->create([
            'code' =>  $productCart->cod_produto,
            'company_id' => $company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'  =>  date('Y-m-d'),
            'weeks'  => $productCart->semanas,
            'leadtime_code' => $productCart->cod_origem,
            'product_code' =>  $productCart->cod_produto,
            'selling_price' => $productCart->valor_kg,
        ]);

        $payment = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'client_id' => $client->id,
            'company_payment_id' => function () use ($company, $payment) {
                return factory(CompanyPayment::class)->create([
                    'company_id' => $company->id,
                    'payment_id' => $payment->id
                ]);
            }
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$company->id}/finaliza_pedido", [
            'client_company_payment_id' => $clientCompanyPayment->id
        ]);

        $this->assertDatabaseHas('pedidos', [
            'id_cliente' => $client->id,
            'company_id' => $company->id
        ]);

        $response->assertJsonFragment([
            'status' => true,
        ]);
    }

    /**
     * test finish order with success
     * @return void
     **/
    public function testFinishOrderWithDuplicatedProductShouldntDuplicate()
    {
        $client = factory(Client::class)->create();

        $company = $this->company;

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 200,
            'account_balance' => 10000
        ]);

        $this->setUpMockJwtMobileClient($client->id);

        $product = factory(Product::class)->create([
            'code' =>  12345,
            'company_id' => $company->id,
        ]);

        $productDeleted = factory(Product::class)->create([
            'code' => 12345,
            'company_id' => $company->id,
            'deleted_at' => now()
        ]);

        $cart = factory(Cart::class)->create([
            'id_cliente' => $client->id
        ]);

        $productCart = factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'cod_produto' => $product->code,
            'valor' => 300
        ]);

        $leadTime = factory(LeadTime::class)->create([
            'code' =>  $productCart->cod_origem,
        ]);

        $fefo = factory(Fefo::class)->create([
            'selling_price' => 400,
            'date'  =>  date('Y-m-d'),
            'weeks'  => $productCart->semanas,
            'leadtime_code' => $productCart->cod_origem,
            'product_code' =>  $productCart->cod_produto,
            'selling_price' => $productCart->valor_kg,
        ]);

        $payment = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'client_id' => $client->id,
            'company_payment_id' => function () use ($company, $payment) {
                return factory(CompanyPayment::class)->create([
                    'company_id' => $company->id,
                    'payment_id' => $payment->id
                ]);
            }
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$company->id}/finaliza_pedido");


        $orderProduct = OrderProduct::where(['produto_id' => $productCart->cod_produto])
            ->orderBy('id', 'desc')->count();

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $this->assertEquals($orderProduct, 1);
    }

    /**
     * test finish order with error
     * @return void
     **/
    public function testFinishOrderWithError()
    {
        $client = factory(Client::class)->create([
            'name' => 'sasuke'
        ]);

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 1000
        ]);

        $this->setUpMockJwtMobileClient($client->id);

        $cart = factory(Cart::class)->create([
            'id_cliente' => $client->id
        ]);

        $productCart =  factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'valor' => 300
        ]);

        $leadTime = factory(LeadTime::class)->create([
            'code' =>  $productCart->cod_origem,
        ]);

        $product = factory(Product::class)->create([
            'code' =>  $productCart->cod_produto,
        ]);

        $productName = Product::where('code', $productCart->cod_produto)->first();

        $faker = \Faker\Factory::create();

        $fefo = factory(Fefo::class)->create([
            'date'  =>  date('Y-m-d'),
            'weeks'  => ($productCart->semanas/2),
            'leadtime_code' => $productCart->cod_origem,
            'product_code' =>  $productCart->cod_produto,
            'selling_price' => ($productCart->valor_kg),
        ]);

        $response = $this->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido", [
            'access_token' => $this->jwt,
            'client_id' =>  $cart->id,
        ]);

        $response->assertJsonFragment([
            'status' => false,
            'message' => __('Mobile/order.finish.product_unavailable'),
            'action' => __('Mobile/order.finish.back_cart')
        ]);
    }

    public function testShowOrderWithCorrectPeriodView()
    {
        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::CANCELED)->id,
            'client_id' => $this->client->id,
            'company_id' => $this->company->id
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::CANCELED)->id,
        ]);

        $validateLabel = (new CompanySettingService())->getPeriodView($order->company, $orderProduct->semanas);
        $product_validate = $orderProduct->semanas . ' ' . $validateLabel;

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/pedido", [
                'pedido_id' => $order->id
            ]);

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status' => true,
            'validade' => $product_validate
        ]);
    }

    /**
     * test tax in price
     * @return void
     */
    public function testListOrderWithTax()
    {
        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
            'tax_percent' => 5
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/lista_pedidos");
        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);
        $totalOrder = $content['pendentes'][0]['valor'];
        $totalPriceProductOrder = 0;

        $order->load('orderProduct');

        foreach($order->orderProduct as $orderProduct){
            $totalPriceProductOrder += $orderProduct->valor;
        }

        $this->assertNotEquals($totalOrder, $totalPriceProductOrder);
        $this->assertEquals($totalOrder, $this->calculeTax($order->tax_percent, $totalPriceProductOrder));
    }

    /**
     * Method to calc tax credit card
     * @param float $tax_percent tax percent
     * @param float $price       price
     * @return string
     */
    protected function calculeTax($tax_percent, $price): string
    {
        $percent = ($tax_percent / 100);
        $taxPrice = ($price + ($price * $percent)) * $percent;
        return formata_moeda(round($price + $taxPrice, 2));
    }

    /**
     * Test finish order With Reduce Account balance
     *
     * @group Payment
     *
     * @return void
     **/
    public function testIntegrateInvoiceFinishOrderWithCreditCard()
    {
        Queue::fake();

        $fefo = $this->createFefoCart($this->company);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido", [
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
            'aceita_parcial' => true,
        ]);

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $order = $this->client->order->first();
        $invoice = $order->invoices->first();

        $this->assertEquals(1, $order->invoices->count());

        Queue::assertPushed(IntegrateInvoiceJob::class, function ($job) use ($invoice) {
            return $job->getInvoiceId() == $invoice->id;
        });

        $this->assertEquals($order->client_company_payment_id, $clientCompanyPayment->id);
        $this->assertEquals($order->client_card_id, $clientCard->id);
        $this->assertEquals($order->cc_months, 1);

        $this->assertEquals($invoice->months, 1);
        $this->assertEquals($invoice->order_id, $order->id);
        $this->assertEquals($invoice->client_card_id, $clientCard->id);
    }

    /**
     * Test finish order with error in credit card validation
     *
     * @group Payment
     *
     * @return void
     **/
    public function testFinishOrderWithErrorCreditCard()
    {
        $fefo = $this->createFefoCart($this->company);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $otherCompany = factory(Company::class)->create();
        $otherClient = factory(Client::class)->create();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id,
            'company_id' => $otherCompany->id,
        ]);

        $otherCompanyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id,
            'company_id' => $otherCompany->id,
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $otherClientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $otherCompanyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $otherClientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $otherClient->id,
        ]);

        $withClientCompanyPaymentInvalid = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido", [
            'client_company_payment_id' => $otherClientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
            'aceita_parcial' => true,
        ]);

        $withOtherClientCard = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido", [
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $otherClientCard->id,
            'cc_months' => 1,
            'aceita_parcial' => true,
        ]);

        $clientCompanyPayment->delete();

        $withoutCreditCardEnable = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido", [
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
            'aceita_parcial' => true,
        ]);

        $message = __('Payment/errors.client_company_payment_invalid');

        $withClientCompanyPaymentInvalid->assertJsonFragment([
            'status' => false,
            'message' => $message,
            'msg' => $message,
        ]);

        $withOtherClientCard->assertJsonFragment([
            'status' => false,
            'message' => $message,
            'msg' => $message,
        ]);

        $withoutCreditCardEnable->assertJsonFragment([
            'status' => false,
            'message' => $message,
            'msg' => $message,
        ]);
    }

    public function testFinishOrderWithProductWithoutVolumeShouldReturnError()
    {
        $client = factory(Client::class)->create([
            'name' => 'sasuke'
        ]);

        $company = $this->company;

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 200,
            'account_balance' => 1000
        ]);

        $this->setUpMockJwtMobileClient($client->id);

        $cart = factory(Cart::class)->create([
            'id_cliente' => $client->id
        ]);

        $leadTime = factory(LeadTime::class)->state('canPurchase')->create();

        $product = factory(Product::class)->create([
            'nome' => 'Produto 1',
        ]);

        $product2 = factory(Product::class)->create([
            'nome' => 'Produto 2',
        ]);

        $productCart = factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'cod_produto' => $product->code,
            'valor' => 300,
            'total_kg' => 100,
            'cod_origem' => $leadTime->code,
        ]);

        $productCart2 = factory(ProductCart::class)->create([
            'carrinho_id' => $cart->id,
            'cod_produto' => $product2->code,
            'valor' => 150,
            'total_kg' => 100,
            'cod_origem' => $leadTime->code,
        ]);

        factory(Fefo::class)->create([
            'volume' => 10000.00,
            'date'  =>  date('Y-m-d'),
            'weeks'  => $productCart->semanas,
            'leadtime_code' => $productCart->cod_origem,
            'product_code' =>  $productCart->cod_produto,
            'selling_price' => $productCart->valor_kg,
        ]);


        factory(Fefo::class)->create([
            'volume' => 0,
            'date'  =>  date('Y-m-d'),
            'weeks'  => $productCart2->semanas,
            'leadtime_code' => $productCart2->cod_origem,
            'product_code' =>  $productCart2->cod_produto,
            'selling_price' => $productCart2->valor_kg,
        ]);

        $payment = Payment::where('type', Payment::BILLET)->first();

        factory(ClientCompanyPayment::class)->create([
            'client_id' => $client->id,
            'company_payment_id' => function () use ($company, $payment) {
                return factory(CompanyPayment::class)->create([
                    'company_id' => $company->id,
                    'payment_id' => $payment->id
                ]);
            }
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$company->id}/finaliza_pedido");

        $response->assertJsonFragment([
            'status' => false,
            'msg' => __('Mobile/order.finish.product_unavailable'),
            'message' => __('Mobile/order.finish.product_unavailable'),
            'action' => __('Mobile/order.finish.back_cart')
        ]);
    }

    /**
     * test order show with success
     * @return void
     **/
    public function testOrderShowPaymentIconWithCreditCard()
    {
        Queue::fake();

        $fefo = $this->createFefoCart($this->company);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
            'brand' => 'VISA',
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/pedido", [
            'pedido_id' => $order->id
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'paymentIcon' => $clientCard->image,
            'payment' => $clientCard->description,
        ]);
    }

    /**
     * test order show with success
     * @return void
     **/
    public function testCheckoutCreditCardLimitError()
    {
        $fefo = $this->createFefoCart($this->company, 10000, 50);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/finaliza_pedido", [
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
            'aceita_parcial' => true,
        ]);

        $response->assertJsonFragment([
            'status' => false,
            'message' => __('Mobile/payment.creditCardDisable', ['limit' => '2.000,00']),
            'msg' => __('Mobile/payment.creditCardDisable', ['limit' => '2.000,00']),
        ]);
    }

    /**
     * test order show with 1 product canceled and others pending
     * @return void
     */
    public function testOrderWithOneOrderProductCanceledAndOthersPending()
    {
        $order = factory(Order::class)->state('open')->create(['client_id' => $this->client->id]);
        factory(OrderProduct::class, 3)->create(['order_id' => $order->id]);

        $order->fresh();

        $orderProduct = $order->orderProduct->first();
        $orderProduct->status_id = Status::type(Status::CANCELED)->id;
        $orderProduct->rejection_motives_id = RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id;
        $orderProduct->save();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/pedido", ['pedido_id' => $order->id]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }
}
