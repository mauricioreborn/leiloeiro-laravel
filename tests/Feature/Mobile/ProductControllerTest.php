<?php

namespace Tests\Feature\Mobile;

use App\Auth\User;
use App\Bid\Bid;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Company\Company;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Products\Product;
use App\Season\SeasonProduct;
use App\Season\SeasonType;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Tests list client products
     *
     * @return void
     */
    public function testProductList()
    {
        $leadtime1 = factory(LeadTime::class)->create([
            'name'              => 'SAO PAULO - SP',
            'cnpj'              => $this->client->cnpj,
            'days'              => 1,
            'prediction'        => 1,
            'limit_hour'        => 2300,
            'monday'            => 1,
            'tuesday'           => 1,
            'wednesday'         => 1,
            'thursday'          => 1,
            'friday'            => 1,
            'saturday'          => 1,
            'sunday'            => 1,
            'operates_saturday' => 1,
            'operates_sunday'   => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
        ]);

        $this->assertEquals(Client::count(), 1);
        $this->assertEquals(LeadTime::count(), 1);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $product = Product::first();

        $product->users()->save(factory(User::class)->create());

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lista_produtos"
        );

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'nome',
            'showOrder',
            'showFilter',
            'valor_min',
            'valor_max',
            'valor_min_exibicao',
            'valor_max_exibicao',
            'classes',
            'semanas',
            'total_produtos',
            'produtos'
        ]);

        $boxRange = ($product->weight > 0)
            ?
            floor(floatval($fefo->volume)/floatval($product->weight))
            :
            0;

        $response->assertJsonFragment([
            'status' => true,
            'showOrder' => true,
            'showFilter' => false,
            'total_produtos' => 1,
            'id_fefo' => $fefo->id,
            'codigo_produto' => $product->code,
            'empresa' => $product->brand,
            'nome' => $product->name,
            'img' =>  env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$this->company->id. '/'. $product->foto,
            'pesoCaixa' => volume($product->weight),
            'caixas' => $boxRange,
            'dimensaoCaixa' => $product->app_exhibition,
            'validade' =>  $fefo->weeks . ' semanas',
            ]);
    }


    /**
     * Tests list client products by fefo track
     *
     * @return void
     */
    public function testProductListByFefoTrack()
    {
        $clientCompany = $this->client->setClientCompany($this->company);

        $clientCompany->track_1 = 0;
        $clientCompany->track_2 = 1;
        $clientCompany->track_3 = 1;
        $clientCompany->track_4 = 0;

        $clientCompany->save();
        $this->client->fresh();

        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj
        ]);

        $product = factory(Product::class)->create([
            'name' => 'product 1',
            'category' => 'category 1',
            'type' => 'category 1',
        ]);

        $product2 = factory(Product::class)->create([
            'name' => 'product 2',
            'category' => 'category 2',
            'type' => 'category 2',
        ]);

        $product3 = factory(Product::class)->create([
            'name' => 'product 3',
            'category' => 'category 3',
            'type' => 'category 3',
        ]);

        $product4 = factory(Product::class)->create([
            'name' => 'product 4',
            'category' => 'category 4',
            'type' => 'category 4',
        ]);

        $fefo1 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product->code,
            'leadtime_code' => $leadTime->code,
            'weeks' => 3,
            'date' => now()->toDateString(),
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001'
        ]);

        $fefo2 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product2->code,
            'leadtime_code' => $leadTime->code,
            'weeks' => 3,
            'date' => now()->toDateString(),
            'cd_faixa_fefo' => '500002',
            'faixa_fefo' => '002'
        ]);

        $fefo3 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product3->code,
            'leadtime_code' => $leadTime->code,
            'weeks' => 3,
            'date' => now()->toDateString(),
            'cd_faixa_fefo' => '500003',
            'faixa_fefo' => '003'
        ]);

        $fefo4 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product4->code,
            'leadtime_code' => $leadTime->code,
            'weeks' => 3,
            'date' => now()->toDateString(),
            'cd_faixa_fefo' => '500004',
            'faixa_fefo' => '004'
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lista_produtos"
        );

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'total_produtos' => 2,
            'nome' => "product 2",
            'nome' => "product 3",
            'classes' => [
                "category 2",
                "category 3"
            ]
        ]);

        $response->assertJsonMissing([
            "product 1",
            "product 4",
            "category 1",
            "category 4"
        ]);
    }


    /**
     * Tests list client products order by
     *
     * @return void
     */
    public function testProductListOrderBy()
    {
        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj
        ]);

        $product = factory(Product::class)->create([
            'name' => 'last product',
            'category' => 'category 1',
            'type' => 'category 1',
        ]);

        $product2 = factory(Product::class)->create([
            'name' => 'first product',
            'category' => 'category 2',
            'type' => 'category 2',
        ]);

        $fefo1 = factory(Fefo::class)->create([
            'highlight_app' => 1,
            'product_code' => $product->code,
            'leadtime_code' => $leadTime->code,
            'weeks' => 3,
            'date' => now()->toDateString(),
            'selling_price' => 2
        ]);

        $fefo2 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product2->code,
            'leadtime_code' => $leadTime->code,
            'weeks' => 1,
            'date' => now()->toDateString(),
            'selling_price' => 5
        ]);

        /*
         * Order by name
        **/

        $orderName = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lista_produtos",
            ['ordenacao' => 'nome']
        );

        $orderName->assertStatus(200);

        $product = $orderName->getOriginalContent()['produtos']->first();

        $this->assertEquals( $product['nome'], 'first product' );

        /*
         * Order by price
        **/

        $orderPrice = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lista_produtos",
            ['ordenacao' => 'preco']
        );

        $orderPrice->assertStatus(200);

        $product = $orderPrice->getOriginalContent()['produtos']->first();

        $this->assertEquals( $product['nome'], 'last product' );

        /*
         * Order by validate
        **/
        $orderPrice = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lista_produtos",
            ['ordenacao' => 'validade']
        );

        $orderPrice->assertStatus(200);

        $product = $orderPrice->getOriginalContent()['produtos']->first();

        $this->assertEquals( $product['nome'], 'first product' );
    }

    /**
     * Tests list client featured products
     *
     * @return void
     */
    public function testFeaturedProductList()
    {
        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $product = factory(Product::class)->create([
            'name' => 'featured product',
            'company_id' => $this->company->id,
        ]);

        $product2 = factory(Product::class)->create([
            'name' => 'product 1',
            'company_id' => $this->company->id,
        ]);

        $fefo1 = factory(Fefo::class)->create([
            'highlight_app' => 1,
            'product_code' => $product->code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString(),
            'company_id' => $this->company->id,
        ]);

        $fefo2 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'product_code' => $product2->code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString(),
            'company_id' => $this->company->id,
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lista_custom",
            ['custom_view' => 'Destaques']
        );

        $response->assertStatus(200);

        $product = $response->getOriginalContent()['produtos']->first();

        $this->assertEquals( $product['nome'], 'featured product');

        $response->assertJsonFragment([
            'total_produtos' => 1,
            'nome' => 'Destaques',
            'showOrder' => true
        ]);
    }



    /**
     * Tests list client featured products
     *
     * @return void
     */
    public function testFeaturedProductListWithASeason()
    {
        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj
        ]);

        $customColors = [
            'card_bg' => 'souk',
            'card_text' => '3224400',
            'price_bg' => '40028922',
            'price_text' => 'hadouken',
            'price_highlight' => 'naruto'
        ];

        $seasonProduct =  factory(SeasonProduct::class)->create();

        $createdProduct = Product::where('code', $seasonProduct->product_code)->first();
        $createdProduct->update(['company_id' => $this->company->id]);

        $seasonTypeResult = SeasonType::where(['id' => $seasonProduct->season_id]);
        $seasonType = $seasonTypeResult->first();
        $seasonTypeResult->update([
            'custom_colors' => json_encode($customColors),
            'start_season' => Carbon::now()->subDay(1)
        ]);

        $seasonProduct2 =  factory(SeasonProduct::class)->create([
            'season_id' => $seasonProduct->season_id
        ]);

        $createdProduct2 = Product::where('code', $seasonProduct2->product_code)->first();
        $createdProduct2->update(['company_id' => $this->company->id]);


        $fefo1 = factory(Fefo::class)->create([
            'highlight_app' => 1,
            'product_code' => $seasonProduct->product_code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString(),
            'weeks' => 12,
            'company_id' => $this->company->id,
        ]);

        $fefo2 = factory(Fefo::class)->create([
            'weeks' => 20,
            'highlight_app' => 0,
            'product_code' => $seasonProduct2->product_code,
            'leadtime_code' => $leadTime->code,
            'date' => now()->toDateString(),
            'company_id' => $this->company->id,
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lista_custom",
            ['custom_view' => $seasonType->name]
        );

        $response->assertStatus(200);

        $product = $response->getOriginalContent()['produtos']->first();

        $custom_colors_Result = $response->getOriginalContent()['custom_colors'];


        $this->assertEquals($product['nome'], $createdProduct->name);

        $this->assertEquals($custom_colors_Result['card_bg'], $customColors['card_bg']);
        $this->assertEquals($custom_colors_Result['card_text'], $customColors['card_text']);
        $this->assertEquals($custom_colors_Result['price_bg'], $customColors['price_bg']);
        $this->assertEquals($custom_colors_Result['price_text'], $customColors['price_text']);
        $this->assertEquals($custom_colors_Result['price_highlight'], $customColors['price_highlight']);


        $response->assertJsonFragment([
            'total_produtos' => 2,
            'nome' => ucfirst($seasonType->name),
        ]);
    }

    /**
     * Method to test product list with duplicated code in other company
     *
     * @return null
     */
    public function testDuplicateCodeProductList()
    {
        $otherCompany = factory(Company::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $otherCompany->id,
            'client_id' => $this->client->id
        ]);

        $leadtime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'code' => 697,
            'company_id' => $this->company->id,
        ]);

        $otherLeadtime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'code' => 697,
            'company_id' => $otherCompany->id,
        ]);

        $otherProduct = factory(Product::class)->create([
            'company_id' => $otherCompany->id,
            'code' => '123',
            'name' => 'product 2',
            'category' => 'category 2',
            'type' => 'category 2',
        ]);

        $product = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'code' => '123',
            'name' => 'product 1',
            'category' => 'category 1',
            'type' => 'category 1',
        ]);

        $otherFefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 10,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001',
            'company_id' => $otherCompany
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 11,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001'
        ]);

        $otherFefoOrdened = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 12,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'cd_faixa_fefo' => '500002',
            'faixa_fefo' => '002',
            'company_id' => $otherCompany
        ]);

        $response = $this->withHeader('Authorization',"Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/lista_produtos");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'total_produtos' => 1,
            'nome' => "product 1",
            'classes' => [
                "category 1"
            ],
            'precoAtual' => '11.00'
        ]);

        $response->assertJsonMissing([
            "product 2",
            "category 2"
        ]);

        $response->assertJsonCount(1, 'classes');
    }

    /**
     * Tests product show
     *
     * @return void
     */
    public function testProductShow()
    {
        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id
        ]);

        $product = factory(Product::class)->create([
            'name' => 'product 1',
            'category' => 'category 1',
            'type' => 'category 1',
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 1,
            'product_code' => $product->code,
            'leadtime_code' => $leadTime->code,
            'weeks' => 3,
            'date' => now()->toDateString(),
            'company_id' => $this->company->id,
        ]);


        factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::PENDING)->id,
            'weeks' => $fefo->weeks,
            'company_id' => $this->company->id
        ]);

        factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::APPROVED)->id,
            'weeks' => $fefo->weeks,
            'company_id' => $this->company->id
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/produto",
            ['produto_id' => $fefo->id]
        );

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status' => true,
            'total_produtos_carrinho' => 0,
            'fefo_id' => (string) $fefo->id,
            'nome' => $product->name,
            'codigo_produto' => (string) $fefo->codigo_produto,
            'dimensaoCaixa' => $product->exibicao_app,
            'preco' => (float) number_format($fefo->valor_atual, 2, '.', ''),
            'precoAtual' => formata_moeda($fefo->valor_atual),
            'validade' => trans_choice('Mobile/order.weeks', 3, ['weeks' => 3])
        ]);

        $response->assertJsonStructure([
            'status',
            'total_produtos_carrinho',
            'lances' => [
                'abertos' => [
                    [
                        'cnpj',
                        'gramatura',
                        'padraoMedida',
                        'preco',
                        'precoUn',
                        'unidadeMedida',
                        'valor',
                        'volume'
                    ]
                ],
                'aceitos' => [
                    [
                        'data',
                        'gramatura',
                        'padraoMedida',
                        'preco',
                        'precoUn',
                        'quantidadeClientes',
                        'unidadeMedida',
                        'valor',
                        'volume',
                    ]
                ],
            ],
            'produto' => [
                'fefo_id',
                'codigo_produto',
                'img',
                'nome',
                'empresa',
                'caixas',
                'pesoCaixa',
                'dimensaoCaixa',
                'validade',
                'disponivel',
                'preco',
                'precoAtual',
                'precoAtualUn',
                'unidadeMedida',
                'padraoMedida',
                'gramatura',
                'aceita_compra',
                'msg_logistica',
            ]
        ]);

        $withoutFefoId = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/produto",
            [
                'codigo_produto' => $fefo->codigo_produto,
                'validade' => $fefo->semanas,
                'codigo_origem' => $fefo->cod_origem,
            ]
        );

        $withoutFefoId->assertJsonFragment([
            'fefo_id' => (string) $fefo->id
        ]);
    }

    /**
     * Tests product show
     *
     * @return void
     */
    public function testProductShowWithDiscountPercentage()
    {
        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id
        ]);

        $product = factory(Product::class)->create([
            'name' => 'product 1',
            'category' => 'category 1',
            'type' => 'category 1',
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 1,
            'product_code' => $product->code,
            'leadtime_code' => $leadTime->code,
            'weeks' => 3,
            'date' => now()->toDateString(),
            'company_id' => $this->company->id
        ]);


        factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::PENDING)->id,
            'weeks' => $fefo->weeks,
            'company_id' => $this->company->id
        ]);

        factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::APPROVED)->id,
            'weeks' => $fefo->weeks,
            'company_id' => $this->company->id
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/produto",
            ['produto_id' => $fefo->id]
        );

        $response->assertStatus(200);

        $discount_percentage =  ($fefo->valor_cheio-$fefo->valor_atual)/$fefo->valor_cheio * 100;
        $discount_percentage = $discount_percentage >= 5 ? floor($discount_percentage) : 0;

        $response->assertJsonFragment([
            'status' => true,
            'total_produtos_carrinho' => 0,
            'fefo_id' => (string) $fefo->id,
            'nome' => $product->name,
            'codigo_produto' => (string) $fefo->codigo_produto,
            'dimensaoCaixa' => $product->exibicao_app,
            'precoAtual' => formata_moeda($fefo->valor_atual),
            'validade' => trans_choice('Mobile/order.weeks', 3, ['weeks' => 3]),
            'percentual_desconto' => $discount_percentage
        ]);

        $response->assertJsonStructure([
            'status',
            'total_produtos_carrinho',
            'lances' => [
                'abertos' => [
                    [
                        'cnpj',
                        'gramatura',
                        'padraoMedida',
                        'preco',
                        'precoUn',
                        'unidadeMedida',
                        'valor',
                        'volume'
                    ]
                ],
                'aceitos' => [
                    [
                        'data',
                        'gramatura',
                        'padraoMedida',
                        'preco',
                        'precoUn',
                        'quantidadeClientes',
                        'unidadeMedida',
                        'valor',
                        'volume',
                    ]
                ],
            ],
            'produto' => [
                'fefo_id',
                'codigo_produto',
                'img',
                'nome',
                'empresa',
                'caixas',
                'pesoCaixa',
                'dimensaoCaixa',
                'validade',
                'disponivel',
                'precoAtual',
                'precoAtualUn',
                'unidadeMedida',
                'padraoMedida',
                'gramatura',
                'aceita_compra',
                'msg_logistica',
            ]
        ]);
    }

    public function testProductSearch()
    {
        $leadTime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id
        ]);

        $product = factory(Product::class)->create([
            'name' => 'LING T PAIO SEARA 370G',
            'weight' => 10,
            'weight_piece' => 10,
            'pieces' => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'  =>  now()->toDateString(),
            'leadtime_code' => $leadTime->code,
            'product_code' =>  $product->code,
            'selling_price' => 10.0,
            'volume' => 1000.0,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/busca", [
                'search_terms' => 'PAIO'
            ]);

        $this->assertDatabaseHas('client_searches', [
            'client_id' => $this->client->id,
            'company_id' => $this->company->id,
            'search' => 'PAIO'
        ]);
    }
}
