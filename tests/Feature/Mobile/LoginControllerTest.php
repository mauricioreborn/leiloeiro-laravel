<?php

namespace Tests\Feature\Mobile;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\Client\Seller;
use App\Company\CompanyPayment;
use App\Core\Helpers\JWT;
use App\Fcm\Fcm;
use App\LeadTime\LeadTime;
use App\Payment\Payment;
use App\Uuid\Uuid;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Tests that a client was successfully logged in
     *
     * @return void
     */
    public function testLoginWithSuccess()
    {
        $client = factory(Client::class)->create();

        $clientCompanies = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
        ]);

        $seller = factory(Seller::class)->create([
            'company_id' => $this->company->id
        ]);

        $client->seller()->sync([$seller->id]);

        $other_seller = factory(Seller::class)->create([
            'company_id' => $this->company->id
        ]);

        $response = $this->post('/api/v1/mobile/login', [
            'cnpj'     => $client->cnpj,
            'password' => '12345',
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'client_id' => (string) $client->id,
            'ativo' => (bool) "{$clientCompanies->status}",
            'status' => (bool) "{$clientCompanies->status}",
            'endereco' => sprintf(
                "%s - %s/%s",
                $clientCompanies->address,
                $clientCompanies->city,
                $clientCompanies->state
            ),
            'nome' => (string) $client->name,
            'name' => (string) $client->name,
            "phone" => formatPhone($client->phone),
            "email" => $client->email,
            "confirm_sms" => $client->confirmSms,
            "confirm_email" => $client->confirmEmail,
        ]);

        $response->assertJsonCount(1, 'vendedores');

        $response->assertJsonFragment([
            [
                'name' => $seller->name,
                'email' => $seller->email,
                'phone' => $seller->phone,
                'company' => [
                    'name' => $this->company->name,
                    'image' => $this->company->image,
                ]
            ]
        ]);

        $this->assertEquals(Uuid::count(), 1);
    }

    /**
     * Tests that the user's credentials were not found
     *
     * @return void
     */
    public function testLoginNotFound()
    {
        $faker = \Faker\Factory::create('pt_BR');

        $response = $this->post('/api/v1/mobile/login', [
            'cnpj' => $faker->cnpj(),
            'password' => 'testpassword',
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            "status" => false,
            "msg" => __('auth.failed'),
            "message" => __('auth.failed')
        ]);
    }

    /**
     * Tests that the user's credentials were not found
     *
     * @return void
     */
    public function testLoginCnpjInvalid()
    {
        $faker = \Faker\Factory::create('pt_BR');

        $response = $this->post('/api/v1/mobile/login', [
            'cnpj' => '1122334455',
            'password' => 'testpassword',
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            "status" => false,
            "msg" =>  "O campo cnpj não é um CNPJ válido"
        ]);
    }

    /**
     * Test check token with success
     *
     * @return void
     */
    public function testCheckTokenWithSuccess()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'status' => 1
        ]);

        $makeLogin = $this->post('/api/v1/mobile/login', [
            'cnpj'     => $client->cnpj,
            'password' => '12345',
            'token_fcm' => '124345'
        ]);

        $makeLoginContent = $makeLogin->getOriginalContent();

        $response = $this->post('/api/v1/mobile/check', [
            'access_token' => $makeLoginContent['access_token'],
            'client_id' => $client->id,
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'client_id' => (string) $client->id,
            'ativo' => (bool) "{$clientCompany->status}",
            'status' => (bool) "{$clientCompany->status}",
            'endereco' => sprintf("%s - %s/%s", $clientCompany->address, $clientCompany->city, $clientCompany->state),
            'nome' => (string) $client->name,
            'name' => (string) $client->name,
            "phone" => formatPhone($client->phone),
            "email" => $client->email,
            "confirm_sms" => $client->confirmSms,
            "confirm_email" => $client->confirmEmail,
        ]);
    }

    /**
     * Test check token without Required Params
     *
     * @return void
     */
    public function testCheckTokenWithoutRequiredParams()
    {
        $response = $this->post('/api/v1/mobile/check', [
            'access_token' => '',
            'client_id' => '3224400',
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'status' => false,
            'message' => __('auth.failed'),
            'message' => __('auth.failed'),
        ]);
    }

    /**
     * Validate if fcm token is not duplicate in clients from login in same device
     *
     * @return void
     */
    public function testFcmTokenClientInDuplicateClientsInSameDevice()
    {
        $token_fcm = $this->faker->uuid;

        $response = $this->post('/api/v1/mobile/login', [
            'cnpj'     => $this->client->cnpj,
            'password' => 'testpassword',
            'token_fcm' => $token_fcm,
            'uuid' => $this->faker->uuid
        ]);

        $response->assertStatus(200);

        $fcm = Fcm::first();

        $this->assertEquals(Fcm::count(), 1);
        $this->assertEquals($fcm->token, $token_fcm);
        $this->assertEquals($fcm->client_id, $this->client->id);

        $other_client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $other_client->id
        ]);

        $this->post('/api/v1/mobile/login', [
            'cnpj'     => $other_client->cnpj,
            'password' => '12345',
            'token_fcm' => $token_fcm,
            'uuid' => $this->faker->uuid
        ]);

        $fcm = Fcm::first();

        $this->assertEquals(Fcm::count(), 1);
        $this->assertEquals($fcm->token, $token_fcm);
        $this->assertEquals($fcm->client_id, $other_client->id);
    }

    /**
     * Test check cc_enabled
     *
     * @return void
     */
    public function testCheckCcEnabled()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'status' => 1
        ]);


        $payment = factory(Payment::class)->create([
            'type' => Payment::CREDIT_CARD,
            'name' => 'Cartão de crédito'
        ]);

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id,
            'company_id' => $this->company->id
        ]);

        factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $client->id,
        ]);

        $makeLogin = $this->post('/api/v1/mobile/login', [
            'cnpj'     => $client->cnpj,
            'password' => '12345',
            'token_fcm' => '124345'
        ]);

        $makeLoginContent = $makeLogin->getOriginalContent();

        $response = $this->post('/api/v1/mobile/check', [
            'access_token' => $makeLoginContent['access_token'],
            'client_id' => $client->id,
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'cc_enabled' => true
        ]);
    }

    /**
     * Test check without cc_enabled
     *
     * @return void
     */
    public function testCheckWithoutCcEnabled()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'status' => 1
        ]);

        $makeLogin = $this->post('/api/v1/mobile/login', [
            'cnpj'     => $client->cnpj,
            'password' => '12345',
            'token_fcm' => '124345'
        ]);

        $makeLoginContent = $makeLogin->getOriginalContent();

        $response = $this->post('/api/v1/mobile/check', [
            'access_token' => $makeLoginContent['access_token'],
            'client_id' => $client->id,
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'cc_enabled' => false
        ]);
    }

    /**
     * Test Change Password With Success
     *
     * @return void
     */
    public function testChangePasswordWithSuccess()
    {
        $faker = \Faker\Factory::create();

        $newPassword = $faker->numberBetween(1,999);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->post('/api/v1/mobile/alterar_senha', [

            'password' => 'testpassword',
            'new_password' => $newPassword,
            'confirm_password' => $newPassword,
        ]);

        $response->assertJsonFragment([
            'status' => true,
            'message' => 'Senha atualizada com sucesso!',
            'msg' => 'Senha atualizada com sucesso!'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Test Reset Client PassWord
     *
     * @return void
     */
    public function testResetPassWordToClientWithSuccess()
    {
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id
        ]);

        $response = $this->post('/api/v1/mobile/recuperar_senha', [
            'cnpj' => $client->cnpj
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'message' => 'Senha enviada para o e-mail cadastrado!'
        ]);
    }

    /**
     * Test Reset Client PassWord
     *
     * @return void
     */
    public function testResetPassWordToClientWithInvalidCnpj()
    {
        $faker = \Faker\Factory::create('pt_BR');
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id
        ]);

        $response = $this->post('/api/v1/mobile/recuperar_senha', [
            'cnpj' => $faker->cnpj
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'status' => false,
            'msg' => 'O valor selecionado para o campo cnpj é inválido.',
            'message' => 'O valor selecionado para o campo cnpj é inválido.'
        ]);
    }

    /**
     * Test Reset Seller PassWord
     *
     * @return void
     */
    public function testResetPassWordToSellerWithSuccess()
    {
        $client = factory(Client::class)->create([
            'verified_mail' => '0',
        ]);

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id
        ]);

        $seller = factory(Seller::class)->create();

        $client->seller()->save($seller);

        $response = $this->post('/api/v1/mobile/recuperar_senha', [
            'cnpj' => $client->cnpj
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'email_verified' => false,
            'message' => __('auth.email_vefified_false')
        ]);
    }

    /**
     * @dataProvider buildParamsToValidatorFail
     *
     * @param fieldName  $fieldName  name
     * @param fieldValue $fieldValue value
     * @param msg        $msg        message
     * @return void
     */
    public function testChangePasswordValidators($fieldName, $fieldValue, $msg)
    {
        $faker = \Faker\Factory::create();

        $newPassword = $faker->password();

        $params = [
            'password' => 'testpassword',
            'new_password' => $newPassword,
            'confirm_password' => $newPassword
        ];

        $params[$fieldName] = $fieldValue;

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post('/api/v1/mobile/alterar_senha', $params);

        $response->assertJsonFragment([
            'status' => false,
            'message' => $msg,
            'msg' => $msg
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }


    /**
     * Build Params To Validator Fail
     *
     * @return array
     */
    public function buildParamsToValidatorFail()
    {
        $faker = \Faker\Factory::create();
        return [
            [
                'password',
                $faker->numberBetween(1,885),
                'Senha atual inválida'
            ],
            [
                'client_id',
                $faker->numberBetween(2,91),
                "O valor selecionado para o campo id do cliente é inválido.",
            ],
            [
                'confirm_password',
                $faker->password(),
                "Os campos confirmação da senha e nova senha devem conter valores iguais."
            ],

        ];
    }

    /**
     * Test Client Login Can Buy
     *
     * @return void
     */
    public function testClientLoginCanBuy()
    {
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id
        ]);

        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $client->cnpj,
            'days' => 1,
            'monday'=> now()->addDay()->format('w') == 1  ? 1 : 0,
            'tuesday'=> now()->addDay()->format('w') == 2 ? 1 : 0,
            'wednesday'=> now()->addDay()->format('w') == 3 ? 1 : 0,
            'thursday'=> now()->addDay()->format('w') == 4 ? 1 : 0,
            'friday'=> now()->addDay()->format('w') == 5 ? 1 : 0,
            'saturday'=> now()->addDay()->format('w') == 6 ? 1 : 0,
            'sunday'=> now()->addDay()->format('w') == 0 ? 1 : 0
        ]);

        $this->assertEquals(Client::count(), 2);

        $this->assertEquals(LeadTime::count(), 1);

        $response = $this->post('/api/v1/mobile/login', [
            'cnpj'     => $client->cnpj,
            'password' => '12345',
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'aceita_compra' => true,
        ]);
    }

    /**
     * Test Client Login Cant Buy
     *
     * @return void
     */
    public function testClientLoginCantBuy()
    {
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id
        ]);

        $leadTime = factory(LeadTime::class)->create([
            'cnpj' => $client->cnpj,
            'days' => 1,
            'cnpj' => $client->cnpj,
            'monday'=> 0,
            'tuesday'=> 0,
            'wednesday'=> 0,
            'thursday'=> 0,
            'friday'=> 0,
            'saturday'=> 0,
            'sunday'=> 0
        ]);

        $this->assertEquals(Client::count(), 2);

        $this->assertEquals(LeadTime::count(), 1);

        $response = $this->post('/api/v1/mobile/login', [
            'cnpj'     => $client->cnpj,
            'password' => '12345',
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'aceita_compra' => false,
        ]);
    }

    /**
     * Test client first access
     *
     * @return void
     */
    public function testFirstAccessWithSuccess()
    {
        $faker = \Faker\Factory::create();

        $email =  $faker->email;

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->post('/api/v1/mobile/primeiro_acesso', [
            'telefone'     => '981882148',
            'email' => $email,
        ]);

        $clientModel = new Client();
        $client = $clientModel->find($this->client->id);

        $response->assertJsonFragment(
            [
                'message' => 'Primeiro acesso do cliente salvo com sucesso.',
                'msg' => 'Primeiro acesso do cliente salvo com sucesso.',
                'status' => true
            ]
        );

        $this->assertEquals($client->email, $email);
        $this->assertEquals($client->phone, '981882148');
    }

    /**
     * Method to register an independent client
     *
     * @return void
     */
    public function testRegisterAnIndependentClient()
    {
        $faker = \Faker\Factory::create('pt_BR');

        $params = [
            'nome' => $faker->name(),
            'cnpj' => $faker->cnpj(),
            'email' => $faker->email,
        ];

        $response = $this->post('api/v1/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Test Register client
     *
     * @return void
     */
    public function testRegisterAClient()
    {
        $clientFactory = factory(Client::class)->create([
            'email_verified' => null
        ]);

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $clientFactory->id
        ]);

        $params = [
            'nome' =>  $clientFactory->name,
            'cnpj' =>  $clientFactory->cnpj,
            'email' =>  $clientFactory->email,
        ];

        $response = $this->post('api/v1/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.client.register_with_success'),
            'message' => __('auth.client.register_with_success'),
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @dataProvider builRegisterRules
     *
     * @param field $field name
     * @param value $value value
     * @param msg   $msg   message
     * @param type  $type  type
     * @return void
     */
    public function testCheckValidatorsRulesOnRegisterClient($field, $value, $msg, $type)
    {
        $faker = \Faker\Factory::create('pt_BR');

        $params = [
            'nome' => $faker->name(),
            'cnpj' => $faker->cnpj(),
            'email' => $faker->email,
        ];

        $params[$field] = $value;
        if ($type == 'in_use') {
            $params[$field] = $this->client->$field;
        }

        $response = $this->post('api/v1/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => false,
            'message' => [$msg],
            'msg' => $msg
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * validate Cnpj And Email In Use
     *
     * @return void
     */
    public function validateCnpjAndEmailInUse()
    {
        $faker = \Faker\Factory::create('pt_BR');

        $params = [
            'nome' => $faker->name(),
            'cnpj' => $this->client->cnpj,
            'email' => $faker->email,
        ];

        $response = $this->post('api/v1/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => false,
            'message' => 'O valor informado para o campo cnpj já está em uso.',
            'msg' => 'O valor informado para o campo cnpj já está em uso.'
        ]);

        $params = [
            'nome' => $faker->name(),
            'cnpj' => $faker->cnpj(),
            'email' => $this->client->email,
        ];


        $response = $this->post('api/v1/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => false,
            'message' => 'Email inválido',
            'msg' => 'Email inválido'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }


    /**
     * build Register Rules
     *
     * @return void
     */
    public function builRegisterRules()
    {
        return [
            [
                'field' => 'nome',
                'value' => '',
                'message' => 'O campo nome é obrigatório.',
                'type' => 'required_name'
            ],
            [
                'field' => 'cnpj',
                'value' => '',
                'message' => 'O campo cnpj é obrigatório.',
                'type' => 'required_cnpj'
            ],
            [
                'field' => 'email',
                'value' => '',
                'message' => 'O campo email é obrigatório.',
                'type' => 'required_email'
            ]
        ];
    }
}
