<?php
namespace Tests\Feature\Mobile;

use App\Client\Notifications\MobileConfirmationNotification;
use App\Client\ClientConfirmation;
use App\Core\Entities\Status;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ClientConfirmationControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpMockedJwtClient();
    }

    /**
     * Testing a client can request a phone confirmation with a cell phone number
     * - Asserts a notification is sent to the informed number;
     *
     * @return void
     */
    public function testClientRequestsPhoneConfirmationShouldReturnSuccessMessage()
    {
        Notification::fake();

        Notification::assertNothingSent();

        $clientConfirmation = factory(ClientConfirmation::class)->make([
            'client_id' => $this->client->id,
            'type' => ClientConfirmation::TYPES['sms']
        ]);

        $payload = [
            'type' => $clientConfirmation->type,
            'value' => $clientConfirmation->value,
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/mobile/client/request_confirmation', $payload);

        $response->assertStatus(200);
        $response->assertExactJson([
            'data' => [
                'status' => true,
                'title'   => __("Mobile/clientConfirmation.{$payload['type']}.title"),
                'message' => __("Mobile/clientConfirmation.{$payload['type']}.message"),
            ],
        ]);

        $this->assertEquals($this->client->fresh()->confirm_sms, Status::type(Status::PENDING)->id);
        $clientConfirmation = $this->client->confirmations()->latest()->first();

        Notification::assertSentTo($clientConfirmation, MobileConfirmationNotification::class);
    }

    /**
     * Testing a client must provide a valid cell phone number
     * - Asserts a notification is NOT sent to the informed number;
     * - Asserts a confirmation request is NOT created in the database;
     * - Asserts the response includes an error;
     *
     * @return void
     */
    public function testClientRequestsPhoneConfirmationShouldRequireValidNumber()
    {
        Notification::fake();

        Notification::assertNothingSent();

        $payload = [
            'type' => 'sms',
            'value' => 'olar',
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/mobile/client/request_confirmation', $payload);

        $response->assertStatus(200);
        $response->assertExactJson([
            'status' => 400,
            'msg' => __("Mobile/clientConfirmation.{$payload['type']}.errors.invalid_number"),
            'message' => __("Mobile/clientConfirmation.{$payload['type']}.errors.invalid_number"),
        ]);

        $clientConfirmations = $this->client->confirmations()->latest()->get();
        $this->assertEquals(0, $clientConfirmations->count());

        Notification::assertNotSentTo([$this->client], MobileConfirmationNotification::class);
    }

    /**
     * Test client phone is updated
     * - Asserts phone number is updated on client;
     * - Asserts soft deleted all codes from client;
     * @return void
     */
    public function testClientConfirmationShouldUpdateClientPhone()
    {
        $clientConfirmation = factory(ClientConfirmation::class)->create([
            'client_id' => $this->client->id
        ]);

        $authorizedStatus = Status::type(Status::AUTHORIZED);

        $payload = [
            'client_id' => $clientConfirmation->client_id,
            'type' => $clientConfirmation->type,
            'code' => $clientConfirmation->code
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/mobile/client/confirm_sms', $payload);

        $this->client = $this->client->fresh();

        $this->assertEquals($clientConfirmation->value, $this->client->phone);
        $this->assertEquals($authorizedStatus->id, $this->client->confirm_sms);
        $this->assertEquals($clientConfirmation->fresh()->confirmed_at->format('dmy'), now()->format('dmy'));
        $this->assertSoftDeleted('client_confirmations', [
            'client_id' => $this->client->id
        ]);
    }

    /**
     * Test client confirmation with invalid code should return exception
     * @return void
     */
    public function testClientConfirmationWithInvalidCodeShouldReturnException()
    {
        $clientConfirmation = factory(ClientConfirmation::class)->create([
            'client_id' => $this->client->id
        ]);

        $clientConfirmationFromAnotherClient = factory(ClientConfirmation::class)->make();

        $payload = [
            'client_id' => $clientConfirmationFromAnotherClient->client_id,
            'type' => $clientConfirmationFromAnotherClient->type,
            'code' => $clientConfirmationFromAnotherClient->code
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/mobile/client/confirm_sms', $payload);

        $response->assertStatus(200);
        $response->assertExactJson([
            'status' => 422,
            'msg' => __("Mobile/clientConfirmation.{$payload['type']}.errors.invalid_code"),
            'message' => __("Mobile/clientConfirmation.{$payload['type']}.errors.invalid_code"),
        ]);
    }

    /**
     * Testing a client can request a phone confirmation with a cell phone number
     * - Asserts a notification is sent to the informed number;
     *
     * @return void
     */
    public function testClientRequestsMailConfirmationShouldReturnSuccessMessage()
    {
        Notification::fake();

        Notification::assertNothingSent();

        $payload = [
            'type' => 'mail',
            'value' => 'email@souk.com.br',
            'value_confirmation' => 'email@souk.com.br',
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/mobile/client/request_confirmation', $payload);

        $response->assertStatus(200);
        $response->assertExactJson([
            'data' => [
                'status' => true,
                'title'   => __("Mobile/clientConfirmation.{$payload['type']}.title"),
                'message' => __("Mobile/clientConfirmation.{$payload['type']}.message"),
            ],
        ]);

        $this->assertEquals($this->client->fresh()->confirm_email, Status::type(Status::PENDING)->id);
        $clientConfirmation = $this->client->confirmations()->latest()->first();

        Notification::assertSentTo($clientConfirmation, MobileConfirmationNotification::class);
    }

    public function testConfirmEmailRequestShouldUpdateClientEmail()
    {
        $confirmation = factory(ClientConfirmation::class)->state('mail')->create();

        $response = $this->get("/api/v1/public/clients/confirm_mail?code={$confirmation->code}");

        $response->assertStatus(302);

        $client = $confirmation->client->fresh();

        $this->assertEquals($confirmation->value, $client->email);
        $this->assertNotNull($confirmation->fresh()->confirmed_at);
        $this->assertSoftDeleted($confirmation);
    }

    public function testClientRequestsEmailConfirmationWithForbiddenEmailShouldReturnError()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $payload = [
            'type' => ClientConfirmation::TYPES['mail'],
            'value' => 'email@seara.com.br',
            'value_confirmation' => 'email@seara.com.br',
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/mobile/client/request_confirmation', $payload);

        $response->assertStatus(200);
        $response->assertExactJson([
            'status' => false,
            'msg' => __('validation.forbiddenemail'),
            'message' => __('validation.forbiddenemail'),
        ]);

        Notification::assertNothingSent();
    }

    public function testConfirmEmailRequestShouldSoftDeleteOnlyItsOwnClientsConfirmations()
    {
        $confirmation = factory(ClientConfirmation::class)->state('mail')->create();

        $confirmation2 = factory(ClientConfirmation::class)->state('mail')->create([
            'client_id' => $confirmation->client_id,
        ]);

        $confirmation3 = factory(ClientConfirmation::class)->state('mail')->create();

        $response = $this->get("/api/v1/public/clients/confirm_mail?code={$confirmation->code}");

        $response->assertStatus(302);

        $client = $confirmation->client->fresh();

        $this->assertEquals($confirmation->value, $client->email);
        $this->assertNotNull($confirmation->fresh()->confirmed_at);
        $this->assertSoftDeleted($confirmation);
        $this->assertSoftDeleted($confirmation2);
        $this->assertNull($confirmation3->fresh()->deleted_at);
    }
}

