<?php
namespace Tests\Feature\Mobile;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\Company\CompanyPayment;
use App\Payment\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PaymentControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Method to get client company payments
     * @return null
     */
    public function testGetClientCompanyPayments()
    {
        factory(Payment::class)->create([
            'type' => 'credit_card',
            'name' => 'Cartão de crédito'
        ]);

        factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => function () {
                return factory(CompanyPayment::class)->create([
                    'payment_id' => Payment::first()
                ]);
            },
            'client_id' => $this->client->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
             ->get("/api/v1/mobile/company/{$this->company->id}/client/payments");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            "type" => "credit_card",
            "name" => "Cartão de crédito",
        ]);
    }

    public function testGetClientCompanyEnablePayments()
    {
        $this->client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $this->client->id,
            'min_order' => 200,
            'account_balance' => 500
        ]);

        $this->setUpMockJwtMobileClient($this->client->id);

        $companyCredit = factory(CompanyPayment::class)->states('companyCredit')->create([
            'company_id' => $this->company->id
        ]);

        $creditCard = factory(CompanyPayment::class)->states('creditCard')->create([
            'company_id' => $this->company->id
        ]);

        factory(ClientCompanyPayment::class)->create([
            'client_id' => $this->client->id,
            'company_payment_id' => $companyCredit->id
        ]);

        factory(ClientCompanyPayment::class)->create([
            'client_id' => $this->client->id,
            'company_payment_id' => $creditCard->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
             ->get("/api/v1/mobile/company/{$this->company->id}/client/payments?price=550");

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                [
                    'attributes' => [
                        "enable" => false,
                        "message" => __('Mobile/payment.companyCreditDisable', ['limit' => '50,00']),
                    ]
                ]
            ]
        ], $content);
    }
}