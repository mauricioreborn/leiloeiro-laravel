<?php

namespace Tests\Feature\Mobile;

use App\Company\Company;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * setUp
     * @return void
     **/
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Tests company in routes
     * @dataProvider routesProvider
     * @param url $url url
     * @return void
     */
    public function testAuthorizedRequest($url)
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/{$url}");

        $response->assertStatus(200);

        $response->assertJsonMissing([
            'message' => __('auth.access_denied'),
            'msg' => __('auth.access_denied'),
        ]);
    }

    /**
     * Tests company in routes
     * @dataProvider routesProvider
     * @param url $url url
     * @return void
     **/
    public function testUnauthorizedRequest($url)
    {
        $new_company = factory(Company::class)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$new_company->id}/{$url}");

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status' => false,
            'msg' => __('auth.access_denied'),
            'message' => __('auth.access_denied')
        ]);
    }

    /**
     * routesProvider
     * @return array
     */
    public function routesProvider()
    {
        return [
            ['categorias'],
            ['home'],
            ['add_carrinho'],
            ['busca'],
            ['lista_produtos'],
            ['lista_custom'],
            ['remove_item_carrinho'],
            ['valida_lance'],
            ['add_lance'],
            ['altera_lance'],
            ['confirma_lance'],
            ['edita_lance'],
            ['update_lance'],
            ['carrinho'],
            ['change_item_carrinho'],
            ['pedido'],
            ['finaliza_pedido'],
        ];
    }
}
