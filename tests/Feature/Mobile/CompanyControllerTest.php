<?php

namespace Tests\Feature\Mobile;

use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Company\CompanySetting;
use App\Mobile\Services\CompanyService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CompanyControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Tests client companies list
     *
     * @return void
     */
    public function testClientCompaniesList()
    {
        $other_company = factory(Company::class)->create();
        $companySettings = ['value' => 48];

        $this->company->companySettings()->where('setting', 'billingDueDay')->update($companySettings);


        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/mobile/companies');

        $response->assertStatus(200);
        $response->assertJsonCount(5, 'data');

        $newest = __("Mobile/company.{$this->company->id}.newest") !== 'false';


        $response->assertJsonFragment([
            [
                'type' => 'company',
                'id' => $this->company->id,
                'attributes' => [
                    'name' => $this->company->name,
                    'image' => $this->company->image,
                    'quantity' => $this->company->quantity,
                    'cart' => (new CompanyService($this->company))->getCartLabel($this->client->companies->first()),
                    'bid' => $this->company->bid,
                    'rules' => $this->company->rules,
                    'aboult' => $this->company->aboult,
                    'policy' => $this->company->policy,
                    'information' => $this->company->information,
                    'product_info' => false,
                    'payment' => __('Mobile/company.payment'),
                    'payment_reserve_info' => __('Payment/infos.payment_reserve'),
                    'image_color' => __("Mobile/company.{$this->company->id}.image_color"),
                    'image_background' => __("Mobile/company.{$this->company->id}.image_background"),
                    'image_products' => __("Mobile/company.{$this->company->id}.image_products"),
                    'discount' => __("Mobile/company.{$this->company->id}.discount"),
                    'payment_methods' => __('Mobile/company.payment_methods', [
                        'company' => $this->company->name,
                        'days' => $companySettings['value']
                    ]),
                    'conditions_rates' => __('Mobile/company.conditions_rates'),
                    'payment_cards_info' => __('Mobile/company.payment_cards_info'),
                    'newest' => $newest,
                ]
            ]
        ]);
    }
}
