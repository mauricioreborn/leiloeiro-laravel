<?php

namespace Tests\Feature\Mobile;

use App\Bid\Bid;
use App\Bid\BidLog;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Company\Services\CompanySettingService;
use App\Core\Entities\Status;
use App\Core\Entities\RejectionMotive;
use App\Core\Helpers\JWT;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Payment\Payment;
use App\ProcessedFefo\ProcessedFefo;
use App\Products\Product;
use App\Mobile\Services\BidService;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class BidControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * setUp
     * @return void
     **/
    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpMockedJwtClient();
    }

    /**
     * Test Bid Validate with success
     * @return void
     **/
    public function testBidValidateWithSuccess()
    {
        $fefo = $this->buildFefo();

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/valida_lance", [
            'client_id' => $this->client->id,
            'fefo_id' => $fefo->id
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonStructure([
            'status',
            'precoAtual',
            'precoAtualComparacao',
            'quantidadeMinima',
            'valorMinimo',
            'pesoCaixa',
            'disponivelExibicao',
            'kgDisponivel',
            'caixasDisponivel',
            'visaoLogistica',
            'nextWeekday',
            'dataVisaoLogistica',
            'prazoMaxPedido',
            'precoAtualUn',
            'unidadeMedida',
            'padraoMedida',
            'gramatura',
        ]);
    }

    /**
     * Method to add a bid with sucess
     *
     * @return void
     */
    public function testBidAddWithSuccess()
    {
        $fefo = $this->buildFefo();

        $price = $fefo->selling_price - 1;

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/add_lance", [
            'client_id' => $this->client->id,
            'fefo_id' => $fefo->id,
            'preco' => $price,
            'quantidade' => 1000,
            'tipoLance' => 1
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonStructure([
            'status',
            'nome',
            'empresa',
            'img',
            'kgCaixas',
            'dimensaoCaixa',
            'precoKg',
            'quantidadeCaixas',
            'validade',
            'validadeData',
            'dataEntrega',
            'valorTotal',
            'textoFinal',
            'precoUn',
            'unidadeMedida',
            'padraoMedida',
            'gramatura',
            'quantidadeUn',
            'lanceCodOrigem',
            'lanceCodProduto',
            'lanceSemanas',
            'lanceQtdCaixas',
            'lanceTotalKg',
            'lanceValorKg',
            'lanceValor',
            'lanceAceiteParcial',
            'lanceDuracaoLance'
        ]);
    }

    /**
     * Method to add a bid with price less than min order
     *
     * @return void
     */
    public function testBidAddWithPriceLessThanMinOrderWithError()
    {
        $fefo = $this->buildFefo();

        $clientCompany = $this->client->setClientCompany($this->company);

        $price = $fefo->selling_price - 1;
        $min_order = formata_moeda($clientCompany->min_order);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post(
            "/api/v1/mobile/company/{$this->company->id}/add_lance",
            [
                'client_id' => $this->client->id,
                'fefo_id' => $fefo->id,
                'preco' => 1,
                'quantidade' => 0,
                'tipoLance' => 1
            ]
        );

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'status' => 'false',
            'msg' => "Valor do lance não atingiu o valor mínimo de {$min_order}"
        ]);
    }

    /**

     * Method to confirm a bid with success
     *
     * @return void
     */
    public function testBidConfirmWithSuccess()
    {
        $fefo = $this->buildFefo();

        $bid = factory(Bid::class)->make([
            'weeks' => $fefo->weeks
        ]);

        $company = $this->company;
        $client = $this->client;

        $clientCompany = $this->client->setClientCompany($this->company);
        $this->assertNull($clientCompany->first_bid_at);
        $this->assertNull($clientCompany->first_cart_at);

        $payment = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'client_id' => $client->id,
            'company_payment_id' => function () use ($company, $payment) {
                return factory(CompanyPayment::class)->create([
                    'company_id' => $company->id,
                    'payment_id' => $payment->id
                ]);
            }
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/confirma_lance", [
            'client_id' => $this->client->id,
            'cod_origem' => $fefo->leadtime_code,
            'cod_produto' => $fefo->product_code,
            'aceite_parcial' => $bid->partial_accept,
            'duracao_lance' => date('Y-m-d'),
            'qtd_caixas' => $bid->box_amount,
            'semanas' => $bid->weeks,
            'total_kg' => $bid->weight,
            'valor' => $bid->price,
            'valor_kg' => $bid->kg_price
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment(['status' => true]);
        $this->assertEquals(BidLog::count(), 1);

        $createdBid = Bid::first();

        $this->assertEquals($createdBid->original_box_amount, $bid->box_amount);
        $this->assertEquals($createdBid->status_id, Status::type(Status::PENDING)->id);

        $this->assertEquals(ProcessedFefo::count(), 1);
        $this->assertDatabaseHas('processed_fefos', [
            'fefo_id' => $fefo->id
        ]);

        $clientCompany = $clientCompany->fresh();

        $this->assertNotNull($clientCompany->first_bid_at);
        $this->assertNull($clientCompany->first_cart_at);
    }

    /**
     * Test Bid Change with success
     *
     * @return void
     **/
    public function testBidChangeWithSuccess()
    {
        $leadTime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id
        ]);

        $product = factory(Product::class)->create([
            'name' => 'LING T PAIO SEARA 370G',
            'weight' => 10,
            'weight_piece' => 10,
            'pieces' => 1,
            'company_id' => $this->company->id
        ]);

        $oldFefo = factory(Fefo::class)->create([
            'date'  =>  now()->subDays(10)->toDateString(),
            'leadtime_code' => $leadTime->code,
            'product_code' =>  $product->code,
            'selling_price' => 10.50,
            'volume' => 500.0,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'  =>  now()->toDateString(),
            'leadtime_code' => $leadTime->code,
            'product_code' =>  $product->code,
            'selling_price' => 10.0,
            'max_price' => 11.0,
            'volume' => 1000.0,
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'company_id' => $this->company->id
        ]);

        $price = $fefo->selling_price - 1;

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/altera_lance", [
            'client_id' => $this->client->id,
            'lance_id' => $bid->id,
            'preco' => $price,
            'quantidade' => 1000,
            'tipoLance' => 1
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'textoFinal' => __('Mobile/bid.bids.labels.economy', [
                'economy_percentage' => 19
            ])
        ]);

        $response->assertJsonStructure([
            'status',
            'dataEntrega',
            'dimensaoCaixa',
            'empresa',
            'gramatura',
            'img',
            'kgCaixas',
            'lanceAceiteParcial',
            'lanceDuracaoLance',
            'lanceQtdCaixas',
            'lanceTotalKg',
            'lanceValor',
            'lanceValorKg',
            'lance_id',
            'nome',
            'padraoMedida',
            'precoKg',
            'precoUn',
            'quantidadeCaixas',
            'quantidadeUn',
            'textoFinal',
            'unidadeMedida',
            'validade',
            'validadeData',
            'valorTotal'
        ]);
    }

    /**
     * Test Bid Edit with success
     * @return void
     **/
    public function testBidEditWithSuccess()
    {
        $leadTime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id
        ]);

        $product = factory(Product::class)->create([
            'name' => 'LING T PAIO SEARA 370G',
            'weight' => 10,
            'weight_piece' => 10,
            'pieces' => 1,
            'company_id' => $this->company->id
        ]);

        $oldFefo = factory(Fefo::class)->create([
            'date'  =>  now()->subDays(10)->toDateString(),
            'leadtime_code' => $leadTime->code,
            'product_code' =>  $product->code,
            'selling_price' => 10.50,
            'volume' => 500.0,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => now()->toDateString(),
            'leadtime_code' => $leadTime->code,
            'product_code' =>  $product->code,
            'selling_price' => 10.0,
            'volume' => 1000.0,
        ]);


        $bid = factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/edita_lance", [
                'client_id' => $this->client->id,
                'lance_id' => $bid->id
            ]
        );

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'status' => true,
            'precoAtualComparacao' => "10.00",
            'precoAtual' => formata_moeda($fefo->selling_price),
            'kgDisponivel' => "1000.00",
        ]);

        $response->assertJsonStructure([
            'status',
            'precoAtual',
            'precoAtualComparacao',
            'quantidadeMinima',
            'valorMinimo',
            'pesoCaixa',
            'disponivelExibicao',
            'kgDisponivel',
            'caixasDisponivel',
            'lancePreco',
            'lanceCaixas',
            'aceitaParcial',
            'dataVisaoLogistica',
            'prazoMaxPedido',
            'validadeData',
            'validadeHoje',
            'precoAtualUn',
            'precoUn',
            'unidadeMedida',
            'padraoMedida',
            'gramatura',
            'quantidadeUn'
        ]);
    }

    /**
     * Test Bid Update with success
     * @return void
     **/
    public function testBidUpdateWithSuccess()
    {
        $company = $this->company;
        $client = $this->client;

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'cnpj' => $this->client->cnpj,
                ])->code;
            },
        ]);

        $fefo = $fefo->first();

        $bid = factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::PENDING)->id,
            'weeks' => $fefo->weeks
        ]);

        $payment = Payment::where('type', Payment::COMPANY_CREDIT)->first();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'client_id' => $client->id,
            'company_payment_id' => function () use ($company, $payment) {
                return factory(CompanyPayment::class)->create([
                    'company_id' => $company->id,
                    'payment_id' => $payment->id
                ]);
            }
        ]);

        $params = [
            'client_id' => $this->client->id,
            'lance_id' => $bid->id,
            'duracao_lance' => date('Y-m-d'),
            'qtd_caixas' => 10,
            'total_kg' => 5,
            'valor' => 3.33,
            'valor_kg' => 5,
        ];

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/update_lance",
            $params
        );

        $checkIfBidWasChanged = Bid::find($bid->id)->toArray();

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $this->assertDatabaseHas('lances', [
            'qtd_caixas' => $params['qtd_caixas'],
            'duracao_lance' => $params['duracao_lance'],
            'total_kg' => $params['total_kg'],
            'valor' => $params['valor'],
            'status_id' => Status::type(Status::PENDING)->id,
            'valor_kg' => $params['valor_kg'],
        ]);

        $this->assertEquals(BidLog::count(), 1);

        $createdBid = Bid::find($bid->id);
        $this->assertEquals($createdBid->original_box_amount, 10);

        $this->assertEquals(ProcessedFefo::count(), 1);

        $this->assertDatabaseHas('processed_fefos', [
            'fefo_id' => $fefo->id
        ]);
    }

    /**
     * Method to test delete a bid
     *
     * @return void
     */
    public function testBidRemoveWithSuccess()
    {
        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'cnpj' => $this->client->cnpj,
                ])->code;
            },
        ]);

        $fefo = $fefo->first();

        $bid = factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $params = [
            'lance_id' => $bid->id,
        ];

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/remove_lance",
            $params
        );

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $this->assertDatabaseHas('lances', [
            'id' => $params['lance_id'],
            'status_id' => Status::type(Status::CANCELED)->id
        ]);
    }

    /**
     * method to test bid edit with fail
     *
     * @return void
     */
    public function testBidEditWithFail()
    {
        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'cnpj' => $this->client->cnpj,
                ])->code;
            },
        ]);

        $fefo = $fefo->first();

        $bid = factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/edita_lance", [
            'client_id' => $this->client->id,
            'lance_id' => $bid->id
        ]);
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'status' => 'false',
            'msg' => __('Mobile/product.not_available_for_bids')
        ]);
    }

    /**
     * Test Bid Validate with invalid fefo
     * @return void
     **/
    public function testBidValidateWithInvalidFefo()
    {
        $invalidFefoId = 50;

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")
                ->post("/api/v1/mobile/company/{$this->company->id}/valida_lance", [
            'client_id' => $this->client->id,
            'fefo_id' => $invalidFefoId
        ]);

        $response->assertStatus(200);

        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'status' => 'false',
            'msg' => __('Mobile/product.not_available_for_bids')
        ]);
    }

    /**
     * Test Bid Confirm with invalid client status with error
     * @return void
     **/
    public function testBidConfirmWithInvalidClientStatusShouldReturnError()
    {
        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'cnpj' => $this->client->cnpj,
                ])->code;
            },
        ])->first();

        $bid = factory(Bid::class)->make();
        $clientCompany = $this->client->setClientCompany($this->company);

        //Update client status to invalid
        $clientCompany->update(['status' => 0, 'min_order' => '100', 'account_balance' => '99']);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/confirma_lance", [
            'client_id' => $this->client->id,
            'cod_origem' => $fefo->leadtime_code,
            'cod_produto' => $fefo->product_code,
            'aceite_parcial' => $bid->partial_accept,
            'duracao_lance' => date('Y-m-d'),
            'qtd_caixas' => $bid->box_amount,
            'semanas' => $bid->weeks,
            'total_kg' => $bid->weight,
            'valor' => $bid->price,
            'valor_kg' => $bid->kg_price
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonFragment([
            'status' => 'false',
            'msg' => __('Mobile/bid.bids_and_buys_blocked')
        ]);

    }

    /**
     * Method to test bid validators
     *
     * @dataProvider buildParamsToValidatorFail
     *
     * @param string $fieldName  fieldname
     * @param string $fieldValue fieldValue
     * @param string $msg        msg
     * @return void
     */
    public function testRequestBidValidateWithFail($fieldName, $fieldValue, $msg)
    {
        $faker = \Faker\Factory::create();

        $params[$fieldName] = $fieldValue;

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/company/{$this->company->id}/valida_lance", [
            $params
        ]);

        $response->assertJsonFragment([
            'status' => false,
            'message' => $msg,
            'msg' => $msg
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Method to return validators fail
     *
     * @return array
     */
    public function buildParamsToValidatorFail()
    {
        $faker = \Faker\Factory::create();
        return [
            [
                'fefo_id',
                '',
                "O campo id do estoque(fefo) é obrigatório.",
            ]
        ];
    }

    /**
     * Test client bid list with approved Bids
     * @return void
     **/
    public function testClientBidList()
    {
        $newCompany = factory(Company::class)->create();

        $bid = factory(Bid::class)->state('approved')->create([
            'client_id' => $this->client->id
        ]);

        $bid2 = factory(Bid::class)->state('approved')->create([
            'client_id' => $this->client->id,
            'company_id' => $newCompany->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/lances");
        $response->assertStatus(200);

        $status = (new BidService($bid))->getBidStatus($bid);
        $status2 = (new BidService($bid2))->getBidStatus($bid2);

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset(
            [
                'status'  => true,
                'aprovados' => [
                    [
                        'id_lance' => (string) $bid2->id,
                        'companyId' => $bid2->company_id,
                        'fechado' => true,
                        'novo' => true,
                        'status' => $status2['label']
                    ],
                    [
                        'id_lance' => (string) $bid->id,
                        'companyId' => $bid->company_id,
                        'fechado' => true,
                        'novo' => true,
                        'status' => $status['label']
                    ],
                ],
                'cancelados'  => false,
                'pendentes'  => false
            ],
            $content
        );
    }

    /**
     * Test client bid list with rejected Bids
     * @return void
     **/
    public function testClientBidListWithRejected()
    {
        $bid = factory(Bid::class)->state('canceled')->create([
            'client_id' => $this->client->id
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"
        )->post("/api/v1/mobile/lances");
        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $status = (new BidService($bid))->getBidStatus($bid);

        $this->assertArraySubset(
            [
                'status'  => true,
                'cancelados' => [
                    [
                        'id_lance' => (string) $bid->id,
                        'companyId' => $bid->company->id,
                        'fechado' => false,
                        'novo' => true,
                        'status' => $status['label']
                    ]
                ],
                'aprovados'  => false,
                'pendentes'  => false
            ],
            $content
        );
    }

    /**
     * Test client bid list with pending Bids
     * @return void
     **/
    public function testClientBidListWithPending()
    {
        $statusPending = Status::type(Status::PENDING);

        $bid = factory(Bid::class)->create([
            'status_id' => $statusPending->id,
            'client_id' => $this->client->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/lances");
        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $status = (new BidService($bid))->getBidStatus($bid);

        $this->assertArraySubset(
            [
                'status'  => true,
                'pendentes' => [
                    [
                        'id_lance' => (string) $bid->id,
                        'companyId' => $bid->company_id,
                        'fechado' => false,
                        'novo' => true,
                        'status' => $status['label']
                    ]
                ],
                'aprovados'  => false,
                'cancelados'  => false
            ],
            $content
        );
    }

    /**
     * Method to test client bid show
     *
     * @return void
     */
    public function testClientBidShow()
    {
        $client = $this->client;

        $bid = factory(Bid::class)->state('approved')->create([
            'client_id' => $client->id,
            'client_company_payment_id' => function () use ($client) {
                return factory(ClientCompanyPayment::class)->state('companyCredit')->create([
                    'client_id' => $client->id
                ]);
            }
        ]);

        $productFromBid = (new Product)
            ->where('code', $bid->product_code)
            ->where('company_id', $bid->company_id)
            ->first();

        $productFromBid->weight_piece = ",947";
        $productFromBid->save();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/lance", [
                'lance_id' => $bid->id
            ]
        );

        $response->assertStatus(200);

        $status = (new BidService($bid))->getBidStatus($bid);

        $payment = $bid->clientCompanyPayment->companyPayment->payment;

        $response->assertJsonFragment([
            'status'  => true,
            'payment' => __('Mobile/payment.companyCredit', ['company' => $bid->company->name]),
            'status_lance'  => $status['label'],
            'can_edit' => false
        ]);

    }

    /**
     * Method to test client bid show
     *
     * @return void
     */
    public function testClientBidShowWithPending()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
        ]);

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => $leadtime->code
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::PENDING)
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lance", [
            'lance_id' => $bid->id
        ]);

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status'  => true,
            'status_lance'  => __("status.bid.{$bid->status()->first()->name}.label"),
            'status_message' => __("status.bid.{$bid->status()->first()->name}.mobile", [
                'company_name' => $bid->company->name
            ]),
            'can_edit' => true
        ]);
    }

    /**
     * Method to test client bid show
     *
     * @return void
     */
    public function testClientBidShowWithCanceledByPrice()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
        ]);

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => $leadtime->code
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::CANCELED),
            'rejection_motives_id' => function () {
                return RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id;
            }
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lance", [
            'lance_id' => $bid->id
        ]);

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status' => true,
            'status_lance' => __("rejectionMotive.bid.{$bid->rejectionMotives->name}.label"),
            'status_message' => __("rejectionMotive.bid.{$bid->rejectionMotives->name}.mobile"),
        ]);
    }

    /**
     * Method to test client bid show
     *
     * @return void
     */
    public function testClientBidShowWithCanceledByMinOrder()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
        ]);

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => $leadtime->code
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::CANCELED),
            'rejection_motives_id' => function () {
                return RejectionMotive::type(RejectionMotive::MIN_ORDER)->id;
            }
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lance", [
            'lance_id' => $bid->id
        ]);

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status' => true,
            'status_lance' => __("rejectionMotive.bid.{$bid->rejectionMotives->name}.label"),
            'status_message' => __("rejectionMotive.bid.{$bid->rejectionMotives->name}.mobile"),
        ]);
    }

    /**
     * @return void
     */
    public function testExpiredBidExpectsExpiredLabel()
    {
        $date = Carbon::now()->subDay(1)->format('Y-m-d');

        $bid = factory(Bid::class)->create([
            'duration' => $date,
            'client_id' => $this->client->id,
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::Type(RejectionMotive::EXPIRED)->id,
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lance", [
            'lance_id' => $bid->id
        ]);

        $response->assertStatus(200);

        $content = $response->getOriginalContent();

        $this->assertArraySubset([
            'validadeDias' => " ",
            'dataEntrega' => __('Mobile/bid.bids.labels.delivery_canceled'),
            'status_lance' => __('rejectionMotive.bid.expired.label'),
        ], $content);
    }

    /**
     * @return void
     */
    public function testValidBidExpectsValidLabel()
    {
        $date = Carbon::now();
        $bid = $this->buildOrderProductFromBid(
            Status::type(Status::PENDING),
            $this->company->id,
            $date->copy()->format('Y-m-d')
        );

        $bid2 = $this->buildOrderProductFromBid(
            Status::type(Status::PENDING),
            $this->company->id,
            $date->copy()->addDay(10)->format('Y-m-d')
        );

        $response = $this->withHeader( 'Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/lance", ['lance_id' => $bid->id]);

        $response->assertStatus(200);

        $content = $response->getOriginalContent();

        $this->assertArraySubset(['validadeDias' => "Hoje"], $content);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/lance", ['lance_id' => $bid2->id]);

        $response->assertStatus(200);

        $content = $response->getOriginalContent();

        $this->assertArraySubset(['validadeDias' => "{$date->copy()->addDay(10)->diffInDays($date)} dias"],
            $content);
    }

    /**
     * Method to test client bid authorization
     *
     * @return void
     */
    public function testClientBidAuthorization()
    {
        $other_client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $other_client->id
        ]);

        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $other_client->cnpj,
        ]);

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => $leadtime->code
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $other_client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'client_id' => $other_client->id,
            'bid_id' => $bid->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}"

        )->post(
            "/api/v1/mobile/company/{$this->company->id}/lance",
            [
                'lance_id' => $bid->id
            ]
        );

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status' => false,
            'message' => __('validation.exists', [
                'attribute' => __('validation.attributes.lance_id')])
        ]);
    }

    /**
     * Build fefo with leadtime
     *
     * @return void
     */
    protected function buildFefo()
    {
        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => function () {
                return factory(LeadTime::class)->create([
                    'cnpj' => $this->client->cnpj,
                    'company_id' => $this->company->id
                ])->code;
            }
        ]);

        return $fefo = $fefo->first();
    }

    /**
     * Build a order product from a Bid
     * @param int $status    (0,1,2)
     * @param int $companyId companyId
     * @return Order
     **/
    protected function buildOrderProductFromBid(Status $status = null, int $companyId = null, $durationDays = null)
    {
        $statusPending = Status::type(Status::PENDING);
        $statusApproved = Status::type(Status::APPROVED);
        $statusCanceled = Status::type(Status::CANCELED);

        if (!$status) {
            $status = Status::type(Status::APPROVED);
        }

        if (!$companyId) {
            $companyId = $this->company->id;
        }

        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
        ]);

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => $leadtime->code
        ]);

        $client = $this->client;

        $bidFactory = [
            'client_id' => $client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => $status->id,
            'company_id' => $companyId,
            'client_company_payment_id' => function () use ($client) {
                return factory(ClientCompanyPayment::class)->state('companyCredit')->create([
                    'client_id' => $client->id
                ]);
            }
        ];

        if($durationDays){
            $bidFactory['duration'] = $durationDays;
        }

        $bid = factory(Bid::class)->create($bidFactory);

        $response = $bid;

        //If bid is not pending, create order
        if ($status->id != $statusPending->id) {
            $order = factory(Order::class)->create([
                'status_id' => $status->id,
                'client_id' => $this->client->id,
                'status_id' => Status::type(Status::APPROVED),
                'bid_id' => $bid->id,
                'company_id' => $companyId
            ]);

            $orderProduct = factory(OrderProduct::class)->create([
                'order_id' => $order->id,
                'product_id' => function () use ($companyId) {
                    return factory(Product::class)->create([
                        'company_id' => $companyId
                    ]);
                },
                'status_id' => Status::type(Status::APPROVED),
            ]);

            $response = $order;
        }

        return $response;
    }

    /**
     * Test Show Bid field period view, with different company in url
     *
     * @return void
     */
    public function testGetPeriodViewWithDifferentCompany()
    {
        $order = $this->buildOrderProductFromBid();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/lance", [
                    'lance_id' => $order->bid->id
                ]
            );

        $validateLabel = (new CompanySettingService())->getPeriodView($order->bid->company, $order->bid->semanas);
        $product_validate = $order->bid->semanas . ' ' . $validateLabel;

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'status_lance',
            'lance_id',
            'dataPedido',
            'validade',
            'dataEmbarque',
            'dataEntrega',
            'validadeLance',
            'valorTotal',
            'endereco',
            'aceitaParcial',
            'validadeDias',
            'produtos'
        ]);

        $response->assertJsonFragment([
            'validade' => $product_validate
        ]);
    }

    /**
     * test show bid with credit card payment
     * @return [type] [description]
     */
    public function testClientBidShowWithCreditCard()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
        ]);

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => $leadtime->code
        ]);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
            'payment_method_id' => 2,
            'brand' => 'VISA',
        ]);

        $bid = factory(Bid::class)
            ->states('approved')
            ->create([
                'client_id' => $this->client->id,
                'origin_code' => $fefo->leadtime_code,
                'product_code' => $fefo->product_code,
                'company_id' => $this->company->id,
                'client_company_payment_id' => $clientCompanyPayment->id,
                'client_card_id' => $clientCard->id,
                'cc_months' => 1,
                'tax_percent' => 2.51,
                'price' => 10
            ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/lance", [
                'lance_id' => $bid->id
            ]);

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertEquals($content['valorTotal'], formata_moeda(10.26));
        $this->assertEquals($content['payment'], $clientCard->description);

        $this->assertEquals(
            $content['paymentQuota'],
            __('Payment/clientCard.quota', [
                'quota' => 1,
                'price' => formata_moeda(10.26, false),
                'tax' => 2.51,
            ])
        );
    }

    /**
     * Test finish order With Reduce Account balance
     *
     * @group Payment
     *
     * @return void
     **/
    public function testConfirmBidWithCreditCard()
    {
        $fefo = $this->buildFefo();

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/confirma_lance", [
            'cod_origem' => $fefo->leadtime_code,
            'cod_produto' => $fefo->product_code,
            'aceite_parcial' => true,
            'duracao_lance' => now()->toDateString(),
            'qtd_caixas' => 1,
            'semanas' => $fefo->weeks,
            'total_kg' => 10,
            'valor' => 10,
            'valor_kg' => 10,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
        ]);

        $response->assertJsonFragment([
            'status' => true,
        ]);

        $bid = $this->client->bids->first();

        $this->assertEquals($bid->client_company_payment_id, $clientCompanyPayment->id);
        $this->assertEquals($bid->client_card_id, $clientCard->id);
        $this->assertEquals($bid->status_id, Status::type(Status::PENDING)->id);
        $this->assertEquals($bid->cc_months, 1);
    }

    /**
     * Test finish order with error in credit card validation
     *
     * @group Payment
     *
     * @return void
     **/
    public function testConfirmBidWithErrorCreditCard()
    {
        $fefo = $this->buildFefo();

        $company = $this->company;
        $client = $this->client;

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $otherCompany = factory(Company::class)->create();
        $otherClient = factory(Client::class)->create();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id,
            'company_id' => $otherCompany->id,
        ]);

        $otherCompanyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id,
            'company_id' => $otherCompany->id,
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $otherClientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $otherCompanyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $rejectClientCard = factory(ClientCard::class)->states('rejected')->create([
            'client_id' => $this->client->id,
        ]);

        $otherClientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $otherClient->id,
        ]);

        $withClientCompanyPaymentInvalid = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/confirma_lance", [
            'cod_origem' => $fefo->leadtime_code,
            'cod_produto' => $fefo->product_code,
            'aceite_parcial' => true,
            'duracao_lance' => now()->toDateString(),
            'qtd_caixas' => 1,
            'semanas' => $fefo->weeks,
            'total_kg' => 10,
            'valor' => 10,
            'valor_kg' => 10,
            'client_company_payment_id' => $otherClientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
        ]);

        $withClientCardRejected = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/confirma_lance", [
            'cod_origem' => $fefo->leadtime_code,
            'cod_produto' => $fefo->product_code,
            'aceite_parcial' => true,
            'duracao_lance' => now()->toDateString(),
            'qtd_caixas' => 1,
            'semanas' => $fefo->weeks,
            'total_kg' => 10,
            'valor' => 10,
            'valor_kg' => 10,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $rejectClientCard->id,
            'cc_months' => 1,
        ]);

        $withOtherClientCard = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/confirma_lance", [
            'cod_origem' => $fefo->leadtime_code,
            'cod_produto' => $fefo->product_code,
            'aceite_parcial' => true,
            'duracao_lance' => now()->toDateString(),
            'qtd_caixas' => 1,
            'semanas' => $fefo->weeks,
            'total_kg' => 10,
            'valor' => 10,
            'valor_kg' => 10,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $otherClientCard->id,
            'cc_months' => 1,
        ]);

        $clientCompanyPayment->delete();

        $withoutCreditCardEnable = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/confirma_lance", [
            'cod_origem' => $fefo->leadtime_code,
            'cod_produto' => $fefo->product_code,
            'aceite_parcial' => true,
            'duracao_lance' => now()->toDateString(),
            'qtd_caixas' => 1,
            'semanas' => $fefo->weeks,
            'total_kg' => 10,
            'valor' => 10,
            'valor_kg' => 10,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'cc_months' => 1,
        ]);

        $message = __('Payment/errors.client_company_payment_invalid');

        $withClientCompanyPaymentInvalid->assertJsonFragment([
            'status' => false,
            'message' => $message,
            'msg' => $message,
        ]);

        $withClientCardRejected->assertJsonFragment([
            'status' => false,
            'message' => $message,
            'msg' => $message,
        ]);

        $withOtherClientCard->assertJsonFragment([
            'status' => false,
            'message' => $message,
            'msg' => $message,
        ]);

        $withoutCreditCardEnable->assertJsonFragment([
            'status' => false,
            'message' => $message,
            'msg' => $message,
        ]);
    }

    public function testConfirmBidWithoutAccountBalanceAndCreditCardEnable()
    {
        $this->client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'status' => 1,
            'company_id' => $this->company->id,
            'client_id' => $this->client->id,
            'min_order' => 200,
            'account_balance' => 500
        ]);

        $this->setUpMockJwtMobileClient($this->client->id);

        $leadTime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id
        ]);

        $product = factory(Product::class)->create([
            'name' => 'LING T PAIO SEARA 370G',
            'weight' => 10,
            'weight_piece' => 10,
            'pieces' => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'  =>  now()->toDateString(),
            'leadtime_code' => $leadTime->code,
            'product_code' =>  $product->code,
            'selling_price' => 10.0,
            'volume' => 1000.0,
        ]);

        $companyCredit = factory(CompanyPayment::class)->states('companyCredit')->create([
            'company_id' => $this->company->id
        ]);

        $creditCard = factory(CompanyPayment::class)->states('creditCard')->create([
            'company_id' => $this->company->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'client_id' => $this->client->id,
            'company_payment_id' => $companyCredit->id
        ]);

        factory(ClientCompanyPayment::class)->create([
            'client_id' => $this->client->id,
            'company_payment_id' => $creditCard->id
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/confirma_lance", [
            'cod_origem' => $fefo->leadtime_code,
            'cod_produto' => $fefo->product_code,
            'aceite_parcial' => true,
            'duracao_lance' => now()->toDateString(),
            'qtd_caixas' => 10,
            'semanas' => $fefo->weeks,
            'total_kg' => 100,
            'valor' => 1000,
            'valor_kg' => 10,
            'client_company_payment_id' => $clientCompanyPayment->id,
        ]);

        $response->assertJsonFragment([
            'status' => false,
            'message' => __('Mobile/payment.companyCreditDisable', ['limit' => '500,00']),
        ]);
    }

    public function testClientBidShowWithCanceledAccountBalance()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
        ]);

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => $leadtime->code
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => Status::type(Status::CANCELED),
            'rejection_motives_id' => function () {
                return RejectionMotive::type(RejectionMotive::CUSTOMER_WITHOUT_BALANCE)->id;
            }
        ]);

        $order = factory(Order::class)->create([
            'rejection_motives_id' => function () {
                return RejectionMotive::type(RejectionMotive::CUSTOMER_WITHOUT_BALANCE)->id;
            },
            'bid_id' => $bid->id,
            'client_id' => $this->client->id,
            'status_id' => Status::type(Status::CANCELED),
            'client_account_balance' => 1
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'product_id' => $fefo->product_code,
            'origin_code' => $leadtime->code
        ]);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->post("/api/v1/mobile/company/{$this->company->id}/lance", [
            'lance_id' => $bid->id
        ]);

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'status' => true,
            'status_lance' => __("rejectionMotive.bid.{$bid->rejectionMotives->name}.label"),
            'status_message' => __("rejectionMotive.bid.{$bid->rejectionMotives->name}.mobile",[
                'account_balance' => price_br($bid->price - $order->client_account_balance)
            ]),
        ]);
    }
}
