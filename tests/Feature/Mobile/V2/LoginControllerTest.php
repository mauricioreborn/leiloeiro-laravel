<?php

namespace Tests\Feature\Mobile\V2;

use App\Client\IndependentClient;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use Tests\TestCase;
use Faker\Generator as Faker;
use App\Client\Client;

class LoginControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * @return void
     */
    public function testRegisterAnIndependentClientWithoutData()
    {
        $response = $this->post('/api/v2/mobile/cadastro')->decodeResponseJson();
        $this->assertArraySubset(['status' => false], $response);
    }

     /**
     * Method to testRegisterAnIndependentClient
     *
     * @return void
     */
    public function testRegisterAnIndependentClient()
    {
        $faker = \Faker\Factory::create('pt_BR');
        $email = $faker->email;
        $params = [
            'nome' => $faker->name(),
            'cnpj' => $faker->cnpj(),
            'email' => $email,
            'confirm_email' => $email,
        ];

        $response = $this->post('api/v2/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @return void
     */
    public function testRegisterAnIndependentClientWithAppVersionString()
    {
        $post = $this->buildFormRegister(['version' => "0.4.9"]);
        $response = $this->post('api/v2/mobile/cadastro', $post);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @return void
     */
    public function testRegisterAnIndependentClientWithAppVersionArray()
    {
        $post = $this->buildFormRegister(['version' => '']);
        $response = $this->post('api/v2/mobile/cadastro', $post);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $post = $this->buildFormRegister(["version" => "0.4.0"]);
        $response = $this->post('api/v2/mobile/cadastro', $post);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);
    }

    /**
     * @return void
     */
    public function testRegisterAnIndependentClientWithAppVersionFloat()
    {
        $post = $this->buildFormRegister(['version' => (float) 4.8 ]);

        $response = $this->post('api/v2/mobile/cadastro', $post);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @return void
     */
    public function testRegisterAnIndependentClientWithAppVersionInteger()
    {
        $post = $this->buildFormRegister(['version' => (int) 45 ]);

        $response = $this->post('api/v2/mobile/cadastro', $post);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @return void
     */
    public function testRegisterAnIndependentClientWithAppVersionBoolean()
    {
        $post = $this->buildFormRegister(['version' => true]);

        $response = $this->post('api/v2/mobile/cadastro', $post);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $post = $this->buildFormRegister(['version' => false]);

        $response = $this->post('api/v2/mobile/cadastro', $post);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Test Register client
     *
     * @return void
     */
    public function testRegisterAClient()
    {
        $clientFactory = factory(Client::class)->create([
            'email_verified' => null
        ]);

        $params = [
            'nome' =>  $clientFactory->name,
            'cnpj' =>  $clientFactory->cnpj,
            'email' =>  $clientFactory->email,
            'confirm_email' =>  $clientFactory->email,
        ];

        $response = $this->post('api/v2/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.client.register_with_success'),
            'message' => __('auth.client.register_with_success'),
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @dataProvider builRegisterRules
     *
     * @param field $field name
     * @param value $value value
     * @param msg   $msg   message
     * @param type  $type  type
     * @return void
     */
    public function testCheckValidatorsRulesOnRegisterClient($field, $value, $msg, $type)
    {
        $faker = \Faker\Factory::create('pt_BR');
        $email = $faker->email;
        $params = [
            'nome' => $faker->name(),
            'cnpj' => $faker->cnpj(),
            'email' => $email,
            'confirm_email' => $email
        ];

        $params[$field] = $value;
        if ($type == 'in_use') {
            $params[$field] = $this->client->$field;
        }

        $response = $this->post('api/v2/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => false,
            'message' => [$msg],
            'msg' => $msg
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * validate Cnpj And Email In Use
     *
     * @return void
     */
    public function validateCnpjAndEmailInUse()
    {
        $faker = \Faker\Factory::create('pt_BR');
        $email = $faker->email;
        $params = [
            'nome' => $faker->name(),
            'cnpj' => $this->client->cnpj,
            'email' => $email,
            'confirm_email' => $email
        ];

        $response = $this->post('api/v2/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => false,
            'message' => __('auth.register.cnpj_in_use'),
            'msg' => __('auth.register.cnpj_in_use')
        ]);

        $params = [
            'nome' => $faker->name(),
            'cnpj' => $faker->cnpj(),
            'email' => $this->client->email,
        ];


        $response = $this->post('api/v2/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => false,
            'message' => __('auth.register.invalid_email'),
            'msg' => __('auth.register.invalid_email')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * build Register Rules
     *
     * @return array
     */
    public function builRegisterRules()
    {
        $faker = \Faker\Factory::create('pt_BR');
        $email = $faker->email;
        return [
            [
                'field' => 'nome',
                'value' => '',
                'message' => 'O campo nome é obrigatório.',
                'type' => 'required_name'
            ],
            [
                'field' => 'cnpj',
                'value' => '',
                'message' => 'O campo cnpj é obrigatório.',
                'type' => 'required_cnpj'
            ],
            [
                'field' => 'email',
                'value' => '',
                'message' => 'O campo email é obrigatório.',
                'type' => 'required_email'
            ],
            [
                'field' => 'confirm_email',
                'value' => '',
                'message' => 'O campo confirm email é obrigatório.',
                'type' => 'required_email'
            ],
            [
                'field' => 'confirm_email',
                'value' => $email,
                'message' => 'Os campos confirm email e email devem conter valores iguais.',
                'type' => 'required_email'
            ]

        ];
    }

    /**
     * Method to testRegisterAnIndependentClient
     *
     * @return void
     */
    public function testRegisterAnIndependentClientWithAddressParams()
    {
        $independentClient =   factory(IndependentClient::class)->make([
            'address_complement' => 'ao lado da casado sasuke'
        ]);

        $params = [
            'cnpj' => $independentClient->cnpj,
            'password' => $independentClient->password,
            'nome'=> $independentClient->nome,
            'address'=> $independentClient->endereco,
            'state'=> $independentClient->uf,
            'city'=> $independentClient->municipio,
            'email'=> $independentClient->email,
            'confirm_email' => $independentClient->email,
            'telefone'=> $independentClient->cnpj,
            'integrado'=> $independentClient->telefone,
            'zipcode'=> $independentClient->zipcode,
            'address_number'=> $independentClient->address_number,
            'neighborhood'=> $independentClient->neighborhood,
            'address_complement'=> $independentClient->address_complement,
            'razao'=> $independentClient->social_reason,
        ];

        $response = $this->post('api/v2/mobile/cadastro', $params);

        $response->assertJsonFragment([
            'status' => true,
            'message' => __('auth.independent_client.register_with_success'),
            'msg' => __('auth.independent_client.register_with_success')
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $independentClient = IndependentClient::first();

        $this->assertEquals($params['cnpj'], $independentClient->cnpj);
        $this->assertEquals($params['nome'], $independentClient->nome);
        $this->assertEquals($params['address'], $independentClient->endereco);
        $this->assertEquals($params['state'], $independentClient->uf);
        $this->assertEquals($params['email'], $independentClient->email);
        $this->assertEquals($params['telefone'], $independentClient->telefone);
        $this->assertEquals($params['zipcode'], $independentClient->zipcode);
        $this->assertEquals($params['address_number'], $independentClient->address_number);
        $this->assertEquals($params['address_number'], $independentClient->address_number);
        $this->assertEquals($params['neighborhood'], $independentClient->neighborhood);
        $this->assertEquals($params['razao'], $independentClient->social_reason);
    }

    /**
     * @return void
     */
    public function testIndependentClientWithClientEmailNotVerified()
    {
        $independentClient =  factory(IndependentClient::class)->create([
            'integrado' => 1
        ]);

        $clientFactory = factory(Client::class)->create([
            'email_verified' => 0,
            'cnpj' => $independentClient->cnpj
        ]);

        $params = [
            'cnpj' => $clientFactory->cnpj,
            'password' => $independentClient->password,
            'nome'=> $independentClient->nome,
            'address'=> $independentClient->endereco,
            'state'=> $independentClient->uf,
            'city'=> $independentClient->municipio,
            'email'=> $independentClient->email,
            'confirm_email' => $independentClient->email,
            'telefone'=> $independentClient->cnpj,
            'zipcode'=> $independentClient->zipcode,
            'address_number'=> $independentClient->address_number,
            'neighborhood'=> $independentClient->neighborhood,
            'address_complement'=> $independentClient->address_complement,
            'razao'=> $independentClient->social_reason,
            'version' => '',
        ];

        $response = $this->post('api/v2/mobile/cadastro', $params);
        $response->assertJsonFragment(['message' => __('auth.client.register_with_success')]);
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * @param bool|array $data data
     * @return void
     */
    public function buildFormRegister($data = false)
    {

        $faker = \Faker\Factory::create('pt_BR');
        $email = $faker->email;

        $params = [
            'nome' => $faker->name(),
            'cnpj' => $faker->cnpj(),
            'email' => $email,
            'confirm_email' => $email,
            'address' => $faker->streetAddress,
            'address_complement' => "Complement",
            'address_number' => $faker->randomDigitNotNull,
            'city' => $faker->city,

            'neighborhood' => "Bairro",
            'nome' => $faker->name,
            'razao' => $faker->company,
            'state' => $faker->citySuffix,
            'telefone' => $faker->phoneNumber,
            'zipcode' => $faker->postcode,
            'version' => '',
        ];

        if($data){
            $params = array_merge($params, $data);
        }

        return $params;
    }
}
