<?php

namespace Tests\Feature\Mobile\V2;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientConfirmation;
use App\Client\IndependentClient;
use App\Client\Notifications\ClientWelcomeNotification;
use App\Client\Notifications\IndependentClientWelcomeNotification;
use App\Client\Notifications\MobileConfirmationNotification;
use App\Core\Helpers\Zipcode;
use App\Mobile\Services\SignUpService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class SignUpControllerTest extends TestCase
{
    use DatabaseTransactions;
    const SOUK_CNPJ = 11132690000159;
    const DUMMY_PHONE = 11998877665;
    const DUMMY_PHONE_INVALID = 1198877665;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Valida se
     *
     * @return void
     */
    public function testZipcodeValidationSuccess()
    {
        $zipcode = resolve(Zipcode::class);

        $zipcodeReturn = [
            'cep' => '01001-000',
            'logradouro' => 'Praça da Sé',
            'complemento' => 'lado ímpar',
            'bairro' => 'Sé',
            'localidade' => 'São Paulo',
            'uf' => 'SP',
            'unidade' => '',
            'ibge' => '3550308',
            'gia' => '1004'
        ];

        $zipcode->shouldAllowMockingProtectedMethods()
            ->shouldReceive('fetch')
            ->with('01001000')
            ->andReturn($zipcodeReturn);

        $response = $this->post('api/v2/mobile/signup/zipcode-verification', [
            'zipcode' => '01001-000'
        ]);

        $response->assertJsonFragment([
            'type' => 'zipcode',
            'attributes' => [
                'address' => 'Praça da Sé',
                'zipcode' => '01001-000',
                'address_complement' => 'lado ímpar',
                'neighborhood' => 'Sé',
                'city' => 'São Paulo',
                'state' => 'SP'
            ]
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $this->assertTrue(Cache::has("zipcode:01001000"));
    }

    /**
     * Method to testRegisterAnIndependentClient
     *
     * @return void
     */
    public function testZipcodeNotFound()
    {
        $zipcode = resolve(Zipcode::class);

        $zipcode->shouldAllowMockingProtectedMethods()
            ->shouldReceive('fetch')
            ->with('01001111')
            ->andReturn([]);

        $response = $this->post('api/v2/mobile/signup/zipcode-verification', [
            'zipcode' => '01001-111'
        ]);

        $response->assertJsonFragment([
            'type' => 'zipcode',
            'attributes' => [
                'address' => null,
                'zipcode' => null,
                'address_complement' => null,
                'neighborhood' => null,
                'city' => null,
                'state' => null,
            ]
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Method to testRegisterAnIndependentClient
     *
     * @return void
     */
    public function testZipcodeCached()
    {
        $zipcode = resolve(Zipcode::class);

        $zipcode->shouldAllowMockingProtectedMethods()
            ->shouldReceive('fetch')
            ->with('01001000')
            ->andReturn([]);

        Cache::shouldReceive('get')
            ->once()
            ->with('zipcode:01001000')
            ->andReturn([
                'address' => 'Praça da Sé',
                'zipcode' => '01001-000',
                'address_complement' => 'lado ímpar',
                'neighborhood' => 'Sé',
                'city' => 'São Paulo',
                'state' => 'SP'
            ]);

        $response = $this->post('api/v2/mobile/signup/zipcode-verification', [
            'zipcode' => '01001-000'
        ]);

        $response->assertJsonFragment([
            'type' => 'zipcode',
            'attributes' => [
                'address' => 'Praça da Sé',
                'zipcode' => '01001-000',
                'address_complement' => 'lado ímpar',
                'neighborhood' => 'Sé',
                'city' => 'São Paulo',
                'state' => 'SP'
            ]
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    public function testFirstStepRequestWithValidCnpjShouldBeginANewRegistrationWorkflow()
    {
        $stub = [
            'data' => [
                'status' => true,
                'message' => false,
                'next_step' => SignUpService::STEP_PHONE_VALIDATION,
            ],
        ];

        $response = $this->post('api/v2/mobile/signup/first-step', [
            'cnpj' => self::SOUK_CNPJ
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson($stub);
        $this->assertNotNull(IndependentClient::where('cnpj', self::SOUK_CNPJ)->first());
    }

    public function testFirstStepRequestWithInvalidPhoneShouldReturnValidationError()
    {
        $response = $this->post('api/v2/mobile/signup/phone-verification', [
            'cnpj' => self::SOUK_CNPJ,
            'phone' => self::DUMMY_PHONE_INVALID
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'status' => false,
            'msg' => __('Mobile/signUp.validation.phone'),
            'message' => __('Mobile/signUp.validation.phone'),
        ]);
    }

    public function testFirstStepRequesWithThrottleMiddleware()
    {
        $response = $this->post('api/v2/mobile/signup/phone-verification', [
            'cnpj' => self::SOUK_CNPJ,
            'phone' => self::DUMMY_PHONE_INVALID
        ]);

        $response = $this->post('api/v2/mobile/signup/phone-verification', [
            'cnpj' => self::SOUK_CNPJ,
            'phone' => self::DUMMY_PHONE_INVALID
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $timeout = $response->headers->get('Retry-After');
        $response->assertJson([
            'status' => false,
            'msg' => __(
                "auth.to_many_requests.title",
                ['time' => $timeout]
            ),
            'message' => __(
                "auth.to_many_requests.title",
                ['time' => $timeout]
            ),
        ]);
    }

    public function testFirstStepRequestWithValidPhoneShouldSendConfirmationNotification()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $client = factory(IndependentClient::class)->state('newRegistration')->create([
            'cnpj' => self::SOUK_CNPJ,
        ]);

        $stub = [
            'data' => [
                'status' => true,
                'message' => __('Mobile/signUp.first_step.phone_confirmation_sent'),
                'phone' => formatPhone(self::DUMMY_PHONE),
            ],
        ];

        $response = $this->post('api/v2/mobile/signup/phone-verification', [
            'cnpj' => self::SOUK_CNPJ,
            'phone' => self::DUMMY_PHONE,
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson($stub);

        $clientConfirmation = $client->confirmations()->latest()->first();
        Notification::assertSentTo($clientConfirmation, MobileConfirmationNotification::class);
        $this->assertNotNull($clientConfirmation->independentClient);
        $this->assertNull($clientConfirmation->client);
    }

    public function testFirstStepRequestWithCorrectValidationCodeShouldMatchStub()
    {
        $stub = [
            'data' => [
                'status' => true,
                'message' => false,
                'next_step' => SignUpService::STEP_INDEPENDENT_SIGN_UP,
            ],
        ];

        $client = factory(IndependentClient::class)->state('newRegistration')->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 3,
        ]);

        factory(ClientConfirmation::class)->create([
            'independent_client_id' => $client->id,
            'type' => 'sms',
            'value' => self::DUMMY_PHONE,
            'confirmed_at' => null,
            'code' => 1111,
        ]);

        $response = $this->post('api/v2/mobile/signup/phone-validation', [
            'cnpj' => self::SOUK_CNPJ,
            'validation_code' => 1111,
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson($stub);
    }

    public function testSignUpRegistrationSuccess()
    {
        $client = factory(IndependentClient::class)->state('phoneValidated')->create();

        $response = $this->post('api/v2/mobile/signup/registration', [
            'cnpj' => $client->cnpj,
            'social_reason' => 'COMPANY LTDA',
            'state_registration' => '12223334',
            'zipcode' => '01001-001',
            'address' => 'Praça da Sé',
            'city' => 'São Paulo',
            'state' => 'SP',
            'address_number' => '100',
            'neighborhood' => 'Sé',
            'address_complement' => 'lado par'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'data' => [
                'status' => true,
                'message' => false,
                'next_step' => SignUpService::STEP_CONTACT_INFORMATION,
            ],
        ]);

        $client = $client->fresh();

        $this->assertEquals($client->social_reason, 'COMPANY LTDA');
    }

    public function testSignUpRegistrationWithoutPhoValidation()
    {
        $client = factory(IndependentClient::class)->state('newRegistration')->create();

        $response = $this->post('api/v2/mobile/signup/registration', [
            'cnpj' => $client->cnpj,
            'social_reason' => 'COMPANY LTDA',
            'state_registration' => '12223334',
            'zipcode' => '01001-001',
            'address' => 'Praça da Sé',
            'city' => 'São Paulo',
            'state' => 'SP',
            'address_number' => '100',
            'neighborhood' => 'Sé',
            'address_complement' => 'lado par'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'data' => [
                'status' => true,
                'message' => false,
                'next_step' => SignUpService::STEP_PHONE_VALIDATION,
                'phone' => false,
                'client_type' => 'independent_client',
            ],
        ]);
    }

    public function testSignUpRegistrationClientIntegrated()
    {
        $client = factory(IndependentClient::class)->state('newRegistration')->create();

        $response = $this->post('api/v2/mobile/signup/registration', [
            'cnpj' => $client->cnpj,
            'social_reason' => 'COMPANY LTDA',
            'state_registration' => '12223334',
            'zipcode' => '01001-001',
            'address' => 'Praça da Sé',
            'city' => 'São Paulo',
            'state' => 'SP',
            'address_number' => '100',
            'neighborhood' => 'Sé',
            'address_complement' => 'lado par'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'data' => [
                'status' => true,
                'message' => false,
                'next_step' => SignUpService::STEP_PHONE_VALIDATION,
                'phone' => false,
                'client_type' => 'independent_client',
            ],
        ]);
    }

    public function testSignUpContactInfoWelcomeNotification()
    {
        Notification::fake();

        $client = factory(IndependentClient::class)->state('withoutContactInformation')->create();

        $response = $this->put('api/v2/mobile/signup/contact-information', [
            'cnpj' => $client->cnpj,
            'email' => 'test@souk.com',
            'email_confirmation' => 'test@souk.com',
            'contact_name' => 'Contact name'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'data' => [
                'status' => true,
                'message' => __(
                    'Mobile/signUp.contact_information.independent_client_updated',
                    [
                        'email' => 'test@souk.com',
                        'social_reason' => $client->social_reason,
                    ]
                ),
                'next_step' => SignUpService::STEP_COMPLETED,
                'phone' => false,
                'client_type' => 'independent_client',
            ],
        ]);

        Notification::assertSentTo(
            [$client], IndependentClientWelcomeNotification::class
        );
    }

    public function testSignUpContactInfoWelcomeNotificationReallySended()
    {
        Notification::fake();

        $client = factory(IndependentClient::class)->create([
            'email' => 'test@souk.com',
            'phone' => '11988887777',
            'confirm_sms' => 10,
        ]);

        $response = $this->put('api/v2/mobile/signup/contact-information', [
            'cnpj' => $client->cnpj,
            'email' => 'test@souk.com',
            'email_confirmation' => 'test@souk.com',
            'contact_name' => 'Contact name'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'data' => [
                'status' => true,
                'message' => __('Mobile/signUp.first_step.independent_client_exists'),
                'next_step' => false,
                'phone' => '(11) 98888-7777',
                'client_type' => 'independent_client',
            ],
        ]);

        Notification::assertNotSentTo(
            [$client], IndependentClientWelcomeNotification::class
        );
    }

    public function testSignUpClientContactInfoWelcomeNotification()
    {
        Notification::fake();

        $client = factory(Client::class)->state('isStepThree')->create([
            'email_verified' => 0,
            'phone' => '11988887777',
        ]);

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id
        ]);

        $response = $this->put('api/v2/mobile/signup/contact-information', [
            'cnpj' => $client->cnpj,
            'email' => 'test@souk.com',
            'email_confirmation' => 'test@souk.com',
            'contact_name' => 'Contact name'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJson([
            'data' => [
                'status' => true,
                'message' => __(
                    'Mobile/signUp.contact_information.client_updated',
                    [
                        'email' => 'test@souk.com',
                    ]
                ),
                'next_step' => SignUpService::STEP_COMPLETED,
                'phone' => '11988887777',
                'client_type' => 'client',
            ],
        ]);

        Notification::assertSentTo(
            [$client], ClientWelcomeNotification::class
        );
    }
}
