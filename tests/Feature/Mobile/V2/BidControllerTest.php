<?php

namespace Tests\Feature\Mobile\V2;

use App\Bid\Bid;
use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Company\CompanyPayment;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Payment\Payment;
use App\Products\Product;
use App\Order\OrderProduct;
use App\Company\Company;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BidControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * setUp
     * @return void
     **/
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Test client bid list with approved Bids
     * @return void
     **/

    public function testCounterBidsByStatus()
    {
        $newCompany = factory(Company::class)->create();

        $this->buildOrderProductFromBid(Status::type(Status::PENDING)->id);
        $this->buildOrderProductFromBid(Status::type(Status::APPROVED)->id, $newCompany);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->get("/api/v2/mobile/bids/counter"
        );

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'approved' => [
                'status' => 'approved',
                'total' => 1
            ],
            'pending' => [
                'status' => 'pending',
                'total' => 1
            ]
        ], $content);
    }

    public function testClientBidListV2()
    {
        $newCompany = factory(Company::class)->create();

        $bid = $this->buildOrderProductFromBid(Status::type(Status::PENDING)->id);
        $this->buildOrderProductFromBid(Status::type(Status::APPROVED)->id, $newCompany);
        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->get("/api/v2/mobile/bids?status=pending"
        );

        $response->assertStatus(200);

        $response->assertJsonFragment([
                'client_id' => $bid->client_id,
                'date' => $bid->date,
                'origin_code' => $bid->origin_code,
                'product_code' => $bid->product_code,
                'weeks' => $bid->weeks,
                'box_amount' => $bid->box_amount,
                'weight' => $bid->weight,
                'kg_price' => $bid->kg_price,
                'price' => $bid->price,
                'partial_accept' => $bid->partial_accept,
                'duration' => $bid->duration,
                'user_id' => $bid->user_id,
                'status_id' => $bid->status_id,
                'rejection_motives_id' => $bid->rejection_motives_id,
                'accept_date' => $bid->accept_date,
                'custom_fields' => $bid->custom_fields,
                'unread' => $bid->nao_lido
        ]);
    }

    /**
     * @return void
     */
    public function testClientBidListV2WithCreditCard()
    {
        $this->buildCreditCardData();
        $company = Company::first() ?: factory(Company::class)->create();

        $bid = $this->buildOrderProductFromBid(Status::type(Status::PENDING)->id, $company, true);

        $response = $this->withHeader(
            'Authorization', "Bearer {$this->jwt}")->get("/api/v2/mobile/bids"
        );

        $response->assertStatus(200);

        $test = $this->calculeTax($bid->tax_percent, $bid->valor);


        $this->assertEquals($response->decodeResponseJson('data.0.attributes.custom_fields.price'), $test);
    }

    protected function buildOrderProductFromBid(int $statusId, $company = null, $withCreditCard = false)
    {
        if($company === null) {
            $company = factory(Company::class)->create();
        }

        $companyId = $company->id;

        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
        ]);

        $fefo = factory(Fefo::class)->create([
            'leadtime_code' => $leadtime->code
        ]);

        $createBid = [
            'client_id' => $this->client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'status_id' => $statusId,
            'company_id' => $companyId
        ];

        if($withCreditCard){
            $createBid['client_company_payment_id'] = $this->client->clientCompanyPayments->first()->id;
            $createBid['client_card_id'] = $this->client->clientCards()->first()->id;
            $createBid['tax_percent'] = 5;
        }

        $bid = factory(Bid::class)->create($createBid);

        $response = $bid;

        //If bid is not pending, create order
        if ($statusId !== Status::type(Status::PENDING)->id) {
            $order = factory(Order::class)->create([
                'status_id' => $statusId,
                'client_id' => $this->client->id,
                'bid_id' => $bid->id,
                'company_id' => $companyId
            ]);

            $orderProduct = factory(OrderProduct::class)->create([
                'order_id' => $order->id,
                'product_id' => function () use ($companyId) {
                    return factory(Product::class)->create([
                        'company_id' => $companyId
                    ]);
                },
                'status_id' => $statusId,
            ]);

            $response = $order;
        }

        return $response;
    }

    /**
     * @return void
     */
    protected function buildCreditCardData(): void
    {
        factory(CompanyPayment::class)->create(['payment_id' => 1]);

        foreach(CompanyPayment::all() as $companyPayment){
            factory(ClientCompanyPayment::class)->create([
                'client_id' => $this->client->id,
                'company_payment_id' => $companyPayment->id
            ]);
        }

        factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
            'payment_method_id' => 2,
        ]);
    }

    /**
     * Method to calc tax credit card
     * @param float $tax_percent tax percent
     * @param float $price       price
     * @return float
     */
    protected function calculeTax($tax_percent, $price): string
    {
        $percent = ($tax_percent / 100);
        $taxPrice = ($price + ($price * $percent)) * $percent;
        return formata_moeda(round($price + $taxPrice, 2), false);
    }
}
