<?php

namespace Tests\Feature\Mobile;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\Client\ClientTirolez;
use App\Client\ClientConfirmation;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\Fefo\FefoPrice;
use App\Fefo\FefoPriceBoost;
use App\LeadTime\LeadTime;
use App\Mobile\Services\BannerService;
use App\Mobile\Services\HomeService;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Payment\Payment;
use App\Products\Product;
use App\Season\SeasonProduct;
use App\Season\SeasonType;
use App\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $appCurrentVersion = "0.5.0";

    /**
     * Method to start a setup
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Tests that a client was successfully logged in
     *
     * @return null
     */
    public function testHomeIndexWithHighlightsWithSuccess()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 1,
            'leadtime_code' => $leadtime->code,
            'product_code' => function () {
                return factory(Product::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            }]
        );

        $fefo = $fefo->first();
        $leadtime = LeadTime::where('code', $fefo->leadtime_code)->first();
        $client = Client::where('cnpj', $leadtime->cnpj)->first();
        $product = Product::where('code', $fefo->product_code)->first();

        $seasonProduct = factory(SeasonProduct::class)->create([
            'product_code' => $fefo->product_code
        ]);

        // $seasonTypeResult = SeasonType::find($seasonProducts->season_id);

        // $seasonType = $seasonTypeResult->first();

        // $seasonTypeResult->update(['custom_colors' => json_encode($customColors)]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'nome' => 'DESTAQUES',
            'total_produtos' => 1,
        ]);

        $response->assertJsonFragment([
            'nome' => strtoupper($seasonProduct->seasonType->name),
            'total_produtos' => 1,
        ]);

        $response->assertJsonFragment([
            $product->name
        ]);

        $this->assertEquals(Uuid::count(), 1);

        $content = $response->getOriginalContent();

        $seasonColors = $content['categorias'][strtoupper($seasonProduct->seasonType->name)]['custom_colors'];

        $this->assertArraySubset($seasonProduct->seasonType['custom_colors'], $seasonColors);
    }

    /**
     * Tests that a client was successfully logged in
     *
     * @return null
     */
    public function testHomeIndexWithCategoryWithSuccess()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
                'leadtime_code' => $leadtime->code,
                'product_code' => function () {
                    return factory(Product::class)->create([
                        'company_id' => $this->company->id,
                    ])->code;
                }]
        );

        $fefo = $fefo->first();
        $leadtime = LeadTime::where('code', $fefo->leadtime_code)->first();
        $client = Client::where('cnpj', $leadtime->cnpj)->first();
        $product = Product::where('code', $fefo->product_code)->first();

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $client->id,
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            $product->name
        ]);

        $response->assertJsonFragment([
            'nome' => $product->name,
            'total_produtos' => 1,
        ]);
    }

    /**
     * Method to test home wuith fefo track
     *
     * @return null
     */
    /*public function testHomeIndexWithFefoTrackWithSuccess()
    {
        $clientCompany = $this->client->setClientCompany($this->company);

        $clientCompany->track_1 = 1;
        $clientCompany->track_2 = 0;
        $clientCompany->track_3 = 0;
        $clientCompany->track_4 = 1;

        $clientCompany->save();
        $this->client->fresh();

        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $product = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'name' => 'product 1',
            'category' => 'category 1',
            'type' => 'category 1',
        ]);

        $product2 = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'name' => 'product 2',
            'category' => 'category 2',
            'type' => 'category 2',
        ]);

        $product3 = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'name' => 'product 3',
            'category' => 'category 3',
            'type' => 'category 3',
        ]);

        $product4 = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'name' => 'product 4',
            'category' => 'category 4',
            'type' => 'category 4',
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001'
        ]);

        $fefo2 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product2->code,
            'cd_faixa_fefo' => '500002',
            'faixa_fefo' => '002'
        ]);

        $fefo3 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product3->code,
            'cd_faixa_fefo' => '500003',
            'faixa_fefo' => '003'
        ]);

        $fefo4 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product4->code,
            'cd_faixa_fefo' => '500004',
            'faixa_fefo' => '004'
        ]);

        $fefo = $fefo->first();

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'nome' => 'product 1',
            'nome' => 'product 4',
        ]);

        $response->assertJsonFragment([
            'nome' => 'category 1',
            'nome' => 'category 4',
            'total_produtos' => 1,
        ]);

        $response->assertJsonMissing([
            'nome' => 'product 2',
            'nome' => 'product 3',
            'nome' => 'category 2',
            'nome' => 'category 3',
        ]);

        $response->assertJsonCount(3, 'categorias');
    }
*/
    /**
     * Method to test home with duplicated product code in other company
     *
     * @return null
     */
    public function testHomeWithDuplicateProductCode()
    {
        $otherCompany = factory(Company::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $otherCompany->id,
            'client_id' => $this->client->id
        ]);

        $leadtime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'code' => 697,
            'company_id' => $this->company->id,
        ]);

        $otherLeadtime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'code' => 697,
            'company_id' => $otherCompany->id,
        ]);

        $otherProduct = factory(Product::class)->create([
            'company_id' => $otherCompany->id,
            'code' => '123',
            'name' => 'product 2',
            'category' => 'category 2',
            'type' => 'category 2',
        ]);

        $product = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'code' => '123',
            'name' => 'product 1',
            'category' => 'category 1',
            'type' => 'category 1',
        ]);

        $otherFefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 10,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001',
            'company_id' => $otherCompany
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 11,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001'
        ]);

        $otherFefoOrdened = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 12,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001',
            'company_id' => $otherCompany
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'nome' => 'product 1',
            'precoAtual' => 'R$ 11,00',
        ]);

        $response->assertJsonFragment([
            'nome' => 'category 1',
            'total_produtos' => 1,
        ]);

        $response->assertJsonMissing([
            'nome' => 'product 2',
            'nome' => 'category 2',
        ]);

        $response->assertJsonCount(1, 'categorias');
    }

    /**
     * Method to test home count products with different week
     *
     * @return null
     */
    public function testHomeCountProductsWithDifferentWeek()
    {
        $leadtime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'code' => 697,
            'company_id' => $this->company->id,
        ]);

        $product = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'code' => '123',
            'name' => 'product 1',
            'category' => 'category 1',
            'type' => 'category 1',
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 11,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'weeks' => 10,
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001'
        ]);

        $otherFefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 10,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'weeks' => 4,
            'cd_faixa_fefo' => '500001',
            'faixa_fefo' => '001',
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345'
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'nome' => 'category 1',
            'total_produtos' => 1,
        ]);

        $response->assertJsonCount(1, 'categorias');
    }

    /**
     * @dataProvider buildParamsToValidatorFail
     *
     * @param string $fieldName  name of parameter
     * @param string $fieldValue value of parameter
     * @param string $msg        value of form request return
     * @return array
     */
    public function testRequestHomeWithFail($fieldName, $fieldValue, $msg)
    {
        $faker = \Faker\Factory::create();

        $params['client_id'] = 12334;
        $params['token_fcm'] = 1234;
        $params[$fieldName] =  $fieldValue;

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home",
            $params
        );

        $response->assertJsonFragment([
            'status' => false,
            'message' => $msg,
            'msg' => $msg
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Method to return a parameter to be validated
     * @return array
     */
    public function buildParamsToValidatorFail()
    {
        $faker = \Faker\Factory::create();
        return [
            [
                'client_id',
                'teste',
                "O campo id do cliente deve conter um número inteiro.",
            ]
        ];
    }

    /**
     * Method to test the get logistics return
     *
     * @return null
     */
    public function testLogisticsInfo()
    {
        $client1 = factory(Client::class)->create();
        $company2 = factory(Company::class)->create();

        $client1->companies()->save($company2);

        $leadtime1 = factory(LeadTime::class)->states('canPurchase')->create([
            'name' => 'SAO PAULO - SP',
            'cnpj' => $this->client->cnpj,
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'operates_saturday' => 0,
            'operates_sunday' => 0,
            'company_id' => $this->company->id
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/logistica_info"
        );

        $response->assertJsonStructure($this->logisticsInfoReturn());

        $response->assertJsonFragment([
            'status' => true,
            "embarque_domingo"=> "1",
            "embarque_segunda"=> "1",
            "embarque_terca"=> "1",
            "embarque_quarta"=> "1",
            "embarque_quinta"=> "1",
            "embarque_sexta"=> "1",
            "embarque_sabado"=> "1",
            "prazo_entrega"=> "1 dia",
            "visao_domingo"=> "1",
            "visao_segunda"=> "1",
            "visao_terca"=> "1",
            "visao_quarta"=> "1",
            "visao_quinta"=> "1",
            "visao_sexta"=> "1",
            "visao_sabado"=> "1",
        ]);


        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Logistics structure
     * @return array
     */
    public function logisticsInfoReturn()
    {
        return [
            'status',
            "logistica" => [
                "embarque_domingo",
                "embarque_segunda",
                "embarque_terca",
                "embarque_quarta",
                "embarque_quinta",
                "embarque_sexta",
                "embarque_sabado",
                "prazo_entrega",
                "visao_domingo",
                "visao_segunda",
                "visao_terca",
                "visao_quarta",
                "visao_quinta",
                "visao_sexta",
                "visao_sabado",
                "info_adicional"
            ]
        ];
    }

    /**
     * Asserts that a request to home created the correct cache key
     *
     * @return void
     */
    public function testHomeRequestExpectsCorrectCacheKey()
    {
        $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
        ]);

        $this->assertTrue(Cache::has("home.{$this->company->id}.{$this->client->id}"));
    }

    /**
     * method to test request with banner with credit card message
     *
     * @return null
     */
    public function testHomeRequestWithBannerTirolez()
    {
        $this->setClientConfirmationStatus(5);

        $clientTirolez = factory(ClientTirolez::class)->create([
            'client_id' => $this->client->id,
            'banner_start_at' => now()->subDays(10),
            'banner_finish_at' => now()->addDays(10),
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $response->assertJsonFragment(
            [
                'title' => __("Mobile/banner.tirolez.type.{$clientTirolez->type}.title"),
                'body' => __("Mobile/banner.tirolez.type.{$clientTirolez->type}.body"),
            ]
        );
    }

    /**
     * method to test request with banner with credit card message
     *
     * @return null
     */
    public function testHomeRequestWithBannerCreditCard()
    {
        $this->setClientConfirmationStatus(5);

        $payment = factory(Payment::class)->create([
            'type' => 'credit_card'
        ]);

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $response->assertJsonFragment(
            [
                'body' => __('Mobile/banner.creditCard'),
            ]
        );
    }


    /**
     * Method to to get products with best price from track
     *
     * @return null
     */
   /* public function testGetHomeWithBestPriceFromTrack()
    {
        $clientCompany = $this->client->setClientCompany($this->company);

        $clientCompany->track_1 = 1;
        $clientCompany->track_2 = 1;
        $clientCompany->track_3 = 1;
        $clientCompany->track_4 = 0;

        $clientCompany->save();
        $this->client->fresh();

        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $product = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'name' => 'product 1',
            'category' => 'steaks',
            'type' => 'steaks',
        ]);

        $product2 = factory(Product::class)->create([
            'company_id' => $this->company->id,
            'name' => 'product 2',
            'category' => 'category 2',
            'type' => 'category 2',
        ]);

        $fefo = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 11,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'cd_faixa_fefo' => '500003',
            'faixa_fefo' => '003'
        ]);
        $fefo2 = factory(Fefo::class)->create([
            'highlight_app' => 0,
            'selling_price' => 11,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product2->code,
            'cd_faixa_fefo' => '500004',
            'faixa_fefo' => '004'
            ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345'
        ]);


        $responseData = $response->getOriginalContent();

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'custom_view' => __('Mobile/product.best_price_from_track'),
        ]);

        $produto = $responseData['categorias'][__('Mobile/product.best_price_from_track')]['produtos'][0];

        $this->assertEquals(
            $responseData['categorias'][__('Mobile/product.best_price_from_track')]['total_produtos'],
            1
        );
        $this->assertEquals($produto['nome'], $product->name);
        $this->assertEquals($produto['codigo_produto'], $fefo->id);
    }*/

   public function testGetBoostWhenAClientHasFefoPriceBoost()
   {
       $leadtime = factory(LeadTime::class)->create([
           'cnpj' => $this->client->cnpj,
           'company_id' => $this->company->id,
       ]);

       $fefo = factory(Fefo::class)->create([
               'highlight_app' => 1,
               'leadtime_code' => $leadtime->code,
               'product_code' => function () {
                   return factory(Product::class)->create([
                       'company_id' => $this->company->id,
                   ])->code;
               }]
       );

       $fefoPrice = factory(FefoPrice::class)->create([
           'weeks' => $fefo->weeks,
           'company_id' => $fefo->company_id,
           'leadtime_code' => $leadtime->code,
           'product_code' =>  $fefo->product_code,
           'selling_price' => $fefo->selling_price,
           'expired_at' => now()->addDays(20)
       ]);


       $fefoPriceBoostFactory = factory(FefoPriceBoost::class)->create([
           'client_id' => $this->client->id,
           'start_date' => now()->subDays(20),
           'finish_date' => now()->addDays(20),
           'fefo_price_id' => $fefoPrice->id
       ]);


       $fefo = $fefo->first();
       $leadtime = LeadTime::where('code', $fefo->leadtime_code)->first();
       $client = Client::where('cnpj', $leadtime->cnpj)->first();
       $product = Product::where('code', $fefo->product_code)->first();


       $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
           ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
               'client_id' => $this->client->id,
           ]);

       $discount_percentage =  floor(
           ($fefo->max_price - $fefo->selling_price)/$fefo->max_price * 100
       );

       $unred = false;

       if ($fefoPriceBoostFactory->last_view == null) {
           $unred = true;
       }

       $response->assertJsonFragment(
          [
              'title' => __('Mobile/boost.title'),
              'codigo_produto' => $fefo->id,
              'empresa' => $this->company->name,
              'img' => $fefo->product->picture_url,
              'msgPreco' => __('Mobile/boost.priceMessage'),
              'nome' => $fefo->product->name,
              'padraoMedida' => $fefo->product->unit,
              'precoAtual' => formata_moeda($fefo->selling_price),
              'precoAtualUn' =>  unitPrice($fefo->selling_price, $fefo->product->weight_piece),
              'precoMin' => formata_moeda($fefo->min_price),
              'unidadeMedida' => getUnit(
                  $fefo->selling_price, $fefo->product->weight_piece, $fefo->product->unit, true
              ),
              'msgDesconto' => "$discount_percentage%",
              'msgPromo' => __('Mobile/boost.descontMessage'),
              'unread' => $unred,
          ]
       );
   }

   public function testGetTwoProductBoost()
   {
       $leadtime1 = factory(LeadTime::class)->create(
           [
               'cnpj' => $this->client->cnpj,
               'company_id' => $this->company->id,
           ]
       );

       $leadtime2 = factory(LeadTime::class)->create(
           [
               'cnpj' => $this->client->cnpj,
               'company_id' => $this->company->id,
           ]
       );

       $fefo1 = factory(Fefo::class)->create(
           [
               'highlight_app' => 1,
               'leadtime_code' => $leadtime1->code,
               'product_code' => function () {
                   return factory(Product::class)->create(
                       [
                           'company_id' => $this->company->id,
                       ]
                   )->code;
               }
           ]
       );

       $fefo2 = factory(Fefo::class)->create(
           [
               'highlight_app' => 1,
               'leadtime_code' => $leadtime2->code,
               'product_code' => function () {
                   return factory(Product::class)->create(
                       [
                           'company_id' => $this->company->id,
                       ]
                   )->code;
               }
           ]
       );

       $fefoPrice = factory(FefoPrice::class)->create(
           [
               'weeks' => $fefo1->weeks,
               'company_id' => $fefo1->company_id,
               'leadtime_code' => $leadtime1->code,
               'product_code' => $fefo1->product_code,
               'selling_price' => $fefo1->selling_price,
               'expired_at' => now()->addDays(20)
           ]
       );

       $fefoPrice2 = factory(FefoPrice::class)->create(
           [
               'weeks' => $fefo2->weeks,
               'company_id' => $fefo2->company_id,
               'leadtime_code' => $leadtime2->code,
               'product_code' => $fefo2->product_code,
               'selling_price' => $fefo2->selling_price,
               'expired_at' => now()->addDays(20)
           ]
       );


       $fefoPriceBoostFactory = factory(FefoPriceBoost::class)->create(
           [
               'client_id' => $this->client->id,
               'start_date' => now()->subDays(20),
               'finish_date' => now()->addDays(20),
               'fefo_price_id' => $fefoPrice->id
           ]
       );

       $fefoPriceBoostFactory2 = factory(FefoPriceBoost::class)->create(
           [
               'client_id' => $this->client->id,
               'start_date' => now()->subDays(20),
               'finish_date' => now()->addDays(20),
               'fefo_price_id' => $fefoPrice2->id
           ]
       );


       $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->post(
               "/api/v1/mobile/company/{$this->company->id}/home", [
               'client_id' => $this->client->id,
           ]
           );

       $response->assertJsonCount(2, 'product_boost');
   }

   public function testCheckPhoneEmailData()
   {
       $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
           ->post( "/api/v1/mobile/check", [
               'client_id' => $this->client->id,
           ]);

       $response->assertJsonFragment(
           [
               'phone' => formatPhone($this->client->phone),
               'email' => $this->client->email,
               'confirm_sms' => $this->client->confirmSms,
               'confirm_email' => $this->client->confirmEmail,
           ]
       );
   }

    /**
     * @return void
     */
    public function testShowHomeBannerUpdateEmailFoneStep1()
    {
        $step = 1;
        $this->setClientConfirmationStatus($step);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $responseContent = $response->decodeResponseJson();

        $this->assertArraySubset(
            [
                'banner' => [
                    'title' => __("Mobile/banner.updateEmailFone.title"),
                    'body' => __("Mobile/banner.updateEmailFone.steps.{$step}.body"),
                    'resource' => [
                        'step' => $step
                    ]
                ]
            ], $responseContent
        );
    }

    /**
     * @return void
     */
    public function testShowHomeBannerUpdateEmailFoneStep2()
    {
        $step = 2;
        $this->setClientConfirmationStatus($step);

        $confirmationSms = factory(ClientConfirmation::class)->create([
            'client_id' => $this->client->id,
            'type' => "sms"
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $responseContent = $response->decodeResponseJson();

        $this->assertArraySubset(
            [
                'banner' => [
                    'title' => __("Mobile/banner.updateEmailFone.title"),
                    'body' => __("Mobile/banner.updateEmailFone.steps.{$step}.body",
                        ['phone_number' => formatPhone($confirmationSms->value)]),
                    'resource' => [
                        'step' => $step,
                        'phone' => formatPhone($confirmationSms->value)
                    ]
                ]
            ], $responseContent
        );
    }

    /**
     * @return void
     */
    public function testShowHomeBannerUpdateEmailFoneStep3()
    {
        $step = 3;
        $this->setClientConfirmationStatus($step);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $responseContent = $response->decodeResponseJson();

        $this->assertArraySubset(
            [
                'banner' => [
                    'title' => __("Mobile/banner.updateEmailFone.title"),
                    'body' => __("Mobile/banner.updateEmailFone.steps.{$step}.body"),
                    'resource' => [
                        'step' => $step
                    ]
                ]
            ], $responseContent
        );
    }

    /**
     * @return void
     */
    public function testShowHomeBannerUpdateEmailFoneStep4()
    {
        $step = 4;
        $this->setClientConfirmationStatus($step);

        $confirmationEmail = factory(ClientConfirmation::class)->create([
            'client_id' => $this->client->id,
            'type' => "mail"
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $responseContent = $response->decodeResponseJson();

        $this->assertArraySubset(
            [
                'banner' => [
                    'title' => __("Mobile/banner.updateEmailFone.title"),
                    'body' => __("Mobile/banner.updateEmailFone.steps.{$step}.body", [
                        'email' => $confirmationEmail->value
                    ]),
                    'resource' => [
                        'step' => $step,
                        'email' => $confirmationEmail->value
                    ]
                ]
            ], $responseContent
        );
    }

    /**
     * @return void
     */
    public function testShowHomeBannerUpdateEmailFoneStep4WithoutClientConfirmation()
    {
        $step = 4;
        $this->setClientConfirmationStatus($step);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $responseContent = $response->decodeResponseJson();

        $this->assertArraySubset(
            [
                'banner' => [
                    'title' => __("Mobile/banner.updateEmailFone.title"),
                    'body' => __("Mobile/banner.updateEmailFone.steps.3.body"),
                    'resource' => [
                        'step' => 3
                    ]
                ]
            ], $responseContent
        );
    }

    /**
     * @return void
     */
    public function testNoShowHomeBannerUpdateEmailFone()
    {
        $this->setClientConfirmationStatus(5);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $responseContent = $response->decodeResponseJson();

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $this->assertArraySubset(['banner' => false], $responseContent);
    }

    /**
     * @return void
     */
    public function testNoShowHomeBannerUpdateEmailFoneWithLastVersion()
    {
        $statusAuthorized = Status::type(Status::AUTHORIZED);

        $this->client->confirm_sms = $statusAuthorized->id;
        $this->client->confirm_email = $statusAuthorized->id;

        $this->client->register_updated = true;
        $this->client->save();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $responseContent = $response->decodeResponseJson();

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $this->assertArraySubset(['banner' => false], $responseContent);
    }

    /**
     * @return void
     */
    public function testShowBannerWithVersionBugFix()
    {
        $step = 1;
        $this->setClientConfirmationStatus($step);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => $this->appCurrentVersion,
            ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $responseContent = $response->decodeResponseJson();

        $this->assertArraySubset(
            [
                'banner' => [
                    'title' => __("Mobile/banner.updateEmailFone.title"),
                    'body' => __("Mobile/banner.updateEmailFone.steps.{$step}.body"),
                    'resource' => [
                        'step' => $step
                    ]
                ]
            ], $responseContent
        );
    }

    public function testShowBannerWithMinVersion()
    {
        $this->setClientConfirmationStatus(1);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post( "/api/v1/mobile/company/{$this->company->id}/home", [
                'client_id' => $this->client->id,
                'version' => "0.4.9",
                'platform' => 'ios'
            ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $responseContent = $response->decodeResponseJson();

        $this->assertArraySubset(
            [
                'banner' => [
                    'title' => null,
                    'body' => __('Mobile/banner.updateVersion.body'),
                    'resource' => null
                ]
            ], $responseContent
        );
    }

    /**
     * set client status to show banner
     * @param string $step step banner
     * @return void
     */
    public function setClientConfirmationStatus($step)
    {
        factory(Company::class)->create();
        $state = false;

        switch ($step){
            case 2:
                $state = 'isStepTwo';
                break;
            case 3:
                $state = 'isStepThree';
                break;
            case 4:
                $state = 'isStepFour';
                break;
            case 5:
                $state = 'isConfirmed';
                break;
        }

        $this->client = factory(Client::class);

        if($state){
            $this->client = $this->client->states($state);
        }

        $this->client = $this->client->create();

        factory(ClientCompany::class)->create([
            'client_id' => $this->client->id
        ]);

        $this->setUpMockJwtMobileClient($this->client->id);
    }

    public function testGetBestSellerOfTypology()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
            'code' => 697
        ]);

        $typology = $this->client->setClientCompany($this->company)->typology;

        $fefo = factory(Fefo::class)->create([
                'highlight_app' => 1,
                'leadtime_code' => $leadtime->code,
                'product_code' => function () {
                    return factory(Product::class)->create([
                        'company_id' => $this->company->id,
                    ])->code;
                }]
        );

        $expiresAt = now()->addDay();
        Cache::add("campaign:home:typology:{$this->company->id}:{$leadtime->code}.{$typology}",
            [$fefo->product_code],
            $expiresAt
        );


        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'nome' => __('Mobile/product.best_seller_typology'),
            'total_produtos' => 1,
        ]);

        $content = $response->getOriginalContent();

        $bestSeller = $content['categorias'][__('Mobile/product.best_seller_typology')];
        $this->assertEquals($bestSeller['nome'], __('Mobile/product.best_seller_typology'));
        $product = $bestSeller['produtos'][0];
        $this->assertEquals($product['codigo_produto'], $fefo->id);
        $this->assertEquals($product['nome'], $fefo->product->name);
        $this->assertEquals($product['empresa'], $fefo->product->brand);

        Cache::forget("campaign.home.{$this->company->id}.{$leadtime->code}.{$typology}");
    }

    public function testGetBestSellerOfTypologyWithoutCache()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
            'code' => 697
        ]);

        $typology = $this->client->setClientCompany($this->company)->typology;

        $fefo = factory(Fefo::class)->create([
                'highlight_app' => 1,
                'leadtime_code' => $leadtime->code,
                'product_code' => function () {
                    return factory(Product::class)->create([
                        'company_id' => $this->company->id,
                    ])->code;
                }]
        );

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonMissing(
            [
                'nome' => __('Mobile/product.best_seller_typology')
            ]
        );
    }

    public function testGetSuggestedCampaign()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
            'code' => 697
        ]);


        $fefo = factory(Fefo::class)->create([
                'highlight_app' => 1,
                'leadtime_code' => $leadtime->code,
                'product_code' => function () {
                    return factory(Product::class)->create([
                        'company_id' => $this->company->id,
                    ])->code;
                }]
        );

        $expiresAt = now()->addDay();
        Cache::add("campaign:home:suggested:{$this->company->id}:{$this->client->id}",
            [$fefo->product_code],
            $expiresAt
        );


        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment([
            'nome' => __('Mobile/product.suggested_products'),
            'total_produtos' => 1,
        ]);

        $content = $response->getOriginalContent();

        $bestSeller = $content['categorias'][__('Mobile/product.suggested_products')];
        $this->assertEquals($bestSeller['nome'], __('Mobile/product.suggested_products'));
        $product = $bestSeller['produtos'][0];
        $this->assertEquals($product['codigo_produto'], $fefo->id);
        $this->assertEquals($product['nome'], $fefo->product->name);
        $this->assertEquals($product['empresa'], $fefo->product->brand);

        Cache::forget("campaign:home:suggested:{$this->company->id}:{$this->client->id}");
    }

    public function testGetSuggestedCampaignWithCache()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
            'code' => 697
        ]);


        $fefo = factory(Fefo::class)->create([
                'highlight_app' => 1,
                'leadtime_code' => $leadtime->code,
                'product_code' => function () {
                    return factory(Product::class)->create([
                        'company_id' => $this->company->id,
                    ])->code;
                }]
        );

        $expiresAt = now()->addDay();
        Cache::add("campaign:home:suggested:{$this->company->id}:{$this->client->id}",
            [$fefo->product_code],
            $expiresAt
        );


        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment(
            [
                'nome' => __('Mobile/product.suggested_products')
            ]
        );
    }

    public function testGetSuggestedCampaignWithoutCache()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
            'code' => 697
        ]);


        $fefo = factory(Fefo::class)->create([
                'highlight_app' => 1,
                'leadtime_code' => $leadtime->code,
                'product_code' => function () {
                    return factory(Product::class)->create([
                        'company_id' => $this->company->id,
                    ])->code;
                }]
        );

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonMissing(
            [
                'nome' => __('Mobile/product.suggested_products')
            ]
        );
    }

    /**
     * Method to test get products already bought
     */
    public function testGetBuyAgainCampaign()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
            'code' => 697
        ]);


        $order = factory(Order::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $this->client->id
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id
        ]);


        factory(Fefo::class)->create([
                'highlight_app' => 1,
                'leadtime_code' => $leadtime->code,
                'product_code' => $orderProduct->product_id
        ]);


        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment(
            [
                'nome' => __('Mobile/product.buy_again'),
                'total_produtos' => 1,
            ]
        );

    }

    /**
     * Method to test get products already bought without order
     */
    public function testGetBuyAgainCampaignWithoutOrder()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
            'code' => 697
        ]);


        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonMissing(
            [
                'nome' => __('Mobile/product.buy_again'),
                'total_produtos' => 1,
            ]
        );

    }


    /**
     * Method to test get products already bought without order
     */
    public function testSuggestedProductShouldNotAppearWhenTheProductAlreadyExistOnTypologyCampaign()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
            'code' => 697
        ]);


        $fefo = factory(Fefo::class)->create([
                'highlight_app' => 1,
                'leadtime_code' => $leadtime->code,
                'product_code' => function () {
                    return factory(Product::class)->create([
                        'company_id' => $this->company->id,
                    ])->code;
                }]
        );

        $expiresAt = now()->addDay();
        $typology = $this->client->setClientCompany($this->company)->typology;

        Cache::add("campaign:home:suggested:{$this->company->id}:{$this->client->id}",
            [$fefo->product_code],
            $expiresAt
        );

        Cache::add("campaign:home:typology:{$this->company->id}:{$leadtime->code}.{$typology}",
            [$fefo->product_code],
            $expiresAt
        );


        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $this->client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $response->assertJsonFragment(
            [
                'nome' => __('Mobile/product.best_seller_typology'),
                'total_produtos' => 1,
            ]
        );

        $response->assertJsonMissing(
            [
                'nome' => __('Mobile/product.suggested_products'),
            ]
        );

    }

    /**
     * Tests that a client was successfully logged in
     *
     * @return null
     */
    public function testHomeReturnWithBlackWeekCampaign()
    {
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $fefo = factory(Fefo::class)->create([
                'highlight_app' => 1,
                'leadtime_code' => $leadtime->code,
                'product_code' => function () {
                    return factory(Product::class)->create([
                        'company_id' => $this->company->id,
                    ])->code;
                }]
        );

        $fefo = $fefo->first();
        $leadtime = LeadTime::where('code', $fefo->leadtime_code)->first();
        $client = Client::where('cnpj', $leadtime->cnpj)->first();
        $product = Product::where('code', $fefo->product_code)->first();

        $seasonType = factory(SeasonType::class)->create([
            'name' => HomeService::BLACKFRIDAYCAMPAIGN[0]
        ]);

        $seasonProduct = factory(SeasonProduct::class)->create([
            'product_code' => $fefo->product_code,
            'season_id' => $seasonType->id
        ]);

        $response = $this->withHeader(
            'Authorization',
            "Bearer {$this->jwt}")->post(
            "/api/v1/mobile/company/{$this->company->id}/home", [
            'client_id'     => $client->id,
            'token_fcm' => '124345',
            'uuid' => ' 66f3aa02-f596-4899-acb5-3b5006d5b06d '
        ]);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response = $response->getOriginalContent();
        $firstIndice = array_first($response['categorias']);
        $this->assertEquals($firstIndice['nome'], strtoupper($seasonType->name));
    }
}
