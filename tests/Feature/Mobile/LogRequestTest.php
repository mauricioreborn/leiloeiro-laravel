<?php

namespace Tests\Feature\Mobile;

use App\Client\Client;
use App\Log\LogAccess;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LogRequestTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Tests log login request
     */
    public function testLogLoginRequest()
    {
        LogAccess::truncate();
        $client = Client::first();

        $attributes = [
            'cnpj'     => $client->cnpj,
            'password' => 'testpassword',
            'version' => '0.5.5',
        ];

        $response = $this
            ->post('/api/v1/mobile/login', $attributes);

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $log = LogAccess::first();

        $this->assertEquals(
            (array) json_decode($log->post),
            ['cnpj' => $client->cnpj,'password' => 'FILTERED' , 'version' => '0.5.5']
        );

        $this->assertEquals($log->versao_app, "0.5.5");
    }

    /**
     * Tests log logged request
     */
    public function testLogLoggedRequest()
    {
        LogAccess::truncate();

        $attributes = [
            'client_id' => $this->client->id,
            'app_versao' => '0.5.5',
        ];

        $this
            ->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/home", $attributes);

        $log = LogAccess::first();

        $this->assertEquals($log->client_id, $this->client->id);
        $this->assertEquals($log->versao_app, "0.5.5");
    }

    public function testLogWithoutVersionRequest()
    {
        LogAccess::truncate();

        $attributes = [
            'client_id' => $this->client->id,
            'version' => '',
        ];

        $this
            ->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post("/api/v1/mobile/company/{$this->company->id}/home", $attributes);

        $log = LogAccess::first();

        $this->assertEquals($log->versao_app, "0");
    }
}
