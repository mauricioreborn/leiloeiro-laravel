<?php

namespace Tests\Feature;

use App\Boost\Boost;
use App\Boost\ClientList;
use App\Core\Entities\Status;
use App\Boost\ClientListCnpj;
use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BoostControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    public function testListRequestWithoutToken()
    {
        $response = $this->get('/api/v1/boosts');
        $response->assertStatus(401);
    }

    public function testListRequestWithNonRootUser()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/boosts');
        $response->assertStatus(403);
    }

    public function testListRequest()
    {
        $this->user->update(['root' => 1]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/boosts');

        $response->assertStatus(200);
    }

    public function testListRequestWithData()
    {
        $this->user->update(['root' => 1]);

        factory(Boost::class)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/boosts');

        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
        $response->assertJsonStructure([
            'data' => [
                [
                   'id',
                   'type',
                   'attributes' => [
                       'name',
                       'description',
                       'product_name',
                       'weeks',
                       'coverage',
                       'started_at',
                       'finished_at',
                       'revenue',
                       'status_id',
                       'status' => [
                           'label',
                           'bg_class',
                           'text_class',
                       ],
                    ],
                ]
            ]
        ]);
    }

    public function testPostRequestWithoutToken()
    {
        $response = $this->post('/api/v1/boosts', []);
        $response->assertStatus(401);
    }

    public function testPostRequestWithNonRootUser()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/boosts', []);
        $response->assertStatus(403);
    }

    public function testPostRequestShouldStartNewBoostDraft()
    {
        $this->user->update(['root' => 1]);

        $clientList = factory(ClientList::class)->create();

        $payload = [
            'name' => 'Campanha Natal',
            'title' => 'Lorem ipsum dolor sit amet',
            'description' => 'Donec condimentum auctor velit id laoreet. Nulla hendrerit quis ante id scelerisque.',
            'email' => true,
            'push' => true,
            'sms' => true,
            'whatsapp' => false,
            'started_at' => now(),
            'finished_at' => now()->addDays(7),
            'client_lists' => [$clientList->id],
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/boosts', $payload);

        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'type' => 'boost',
                'attributes' => [
                    'name' => 'Campanha Natal',
                    'title' => 'Lorem ipsum dolor sit amet',
                    'description' => 'Donec condimentum auctor velit id laoreet. Nulla hendrerit quis ante id scelerisque.',
                    'email' => true,
                    'push' => true,
                    'sms' => true,
                    'whatsapp' => false,
                    'started_at' => now()->toDateString(),
                    'finished_at' => now()->addDays(7)->toDateString(),
                    'status_id' => Status::type(Status::DRAFT)->id,
                    'status' => [
                        'label' => __('boosts.status.draft.label'),
                        'bg_class' => __('boosts.status.draft.bg_class'),
                        'text_class' => __('boosts.status.draft.text_class'),
                    ],
                    'client_lists' => [
                        $clientList->toArray(),
                    ],
                ],
            ]
        ]);
    }

    public function testPutRequestShouldUpdateBoostDraft()
    {
        $this->user->update(['root' => 1]);

        $clientList = factory(ClientList::class)->create();

        $payload = [
            'name' => 'Campanha Natal',
            'title' => 'Lorem ipsum dolor sit amet',
            'description' => 'Donec condimentum auctor velit id laoreet. Nulla hendrerit quis ante id scelerisque.',
            'email' => true,
            'push' => true,
            'sms' => true,
            'whatsapp' => false,
            'started_at' => now(),
            'finished_at' => now()->addDays(7),
            'client_lists' => [$clientList->id],
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/boosts', $payload);

        $response->assertStatus(201);
        $responseData = $response->decodeResponseJson();

        $payload2 = [
            'name' => 'Campanha Teste',
            'title' => 'Lorem ipsum dolor sit amet',
            'description' => 'Donec condimentum auctor velit id laoreet. Nulla hendrerit quis ante id scelerisque.',
            'email' => false,
            'push' => false,
            'sms' => false,
            'whatsapp' => true,
            'started_at' => now(),
            'finished_at' => now()->addDays(17),
            'client_lists' => [$clientList->id],
        ];
        
        $response2 = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/boosts/{$responseData['data']['id']}", $payload2);

        $response2->assertStatus(200);

        $this->assertArraySubset([
            'data' => [
                'type' => 'boost',
                'id' => $responseData['data']['id'],
                'attributes' => [
                    'name' => $payload2['name'],
                    'title' => $payload2['title'],
                    'description' => $payload2['description'],
                    'email' => (int)$payload2['email'],
                    'push' => (int)$payload2['push'],
                    'sms' => (int)$payload2['sms'],
                    'whatsapp' => (int)$payload2['whatsapp'],
                    'started_at' => $payload2['started_at']->toDateString(),
                    'finished_at' => $payload2['finished_at']->toDateString(),
                    'status_id' => Status::type(Status::DRAFT)->id,
                    'status' => [
                        'label' => __('boosts.status.draft.label'),
                        'bg_class' => __('boosts.status.draft.bg_class'),
                        'text_class' => __('boosts.status.draft.text_class'),
                    ],
                    'client_lists' => [$clientList->toArray()],
                ],
            ]
        ], $response2->decodeResponseJson());
    }

    public function testShowRequestWithoutToken()
    {
        $response = $this->post('/api/v1/boosts/1');
        $response->assertStatus(401);
    }

    public function testShowRequestWithNonRootUser()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/boosts/1');
        $response->assertStatus(403);
    }

    public function testShowRequestShouldReturnSuccess()
    {
        $this->user->update(['root' => 1]);

        $boost = factory(Boost::class)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get("/api/v1/boosts/{$boost->id}");

        $response->assertStatus(200);
    }
}
