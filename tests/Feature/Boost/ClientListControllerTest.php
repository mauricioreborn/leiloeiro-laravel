<?php

namespace Tests\Feature;

use App\Boost\ClientList;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ClientListControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    public function testListRequestWithoutToken()
    {
        $response = $this->get('/api/v1/client_lists');
        $response->assertStatus(401);
    }

    public function testListRequestWithNonRootUser()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/client_lists');
        $response->assertStatus(403);
    }

    public function testListRequest()
    {
        $this->user->update(['root' => 1]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/client_lists');

        $response->assertStatus(200);
    }

    public function testListRequestWithData()
    {
        $this->user->update(['root' => 1]);

        factory(ClientList::class)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/client_lists');

        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'type',
                    'attributes' => [
                        'name',
                        'rows',
                        'imported_rows',
                        'invalid_rows',
                        'duplicate_rows',
                        'created_at',
                        'user_id',
                        'user_name',
                        'company_id',
                    ],
                ]
            ]
        ]);
    }

    public function testDeleteRequestWithNonRootUserExpectsForbiddenError()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->delete('/api/v1/client_lists/1');

        $response->assertStatus(403);
        $response->assertJson([
            'errors' => [
                [
                    'status' => 403,
                    'code' => __('auth.unauthorized.code'),
                    'title' => __('auth.unauthorized.title'),
                ]
            ],
        ]);
    }

    public function testDeleteRequestExpectsSuccessWithNoContentResponse()
    {
        $this->user->update(['root' => 1]);

        $clientList = factory(ClientList::class)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->delete("/api/v1/client_lists/{$clientList->id}");

        $response->assertStatus(204);
    }

    public function testPostRequestWithoutToken()
    {
        $response = $this->post('/api/v1/client_lists');
        $response->assertStatus(401);
    }

    public function testPostRequestWithNoUserRoot()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->post('/api/v1/client_lists');
        $response->assertStatus(403);
    }
}
