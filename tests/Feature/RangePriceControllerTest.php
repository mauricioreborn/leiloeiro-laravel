<?php
namespace Tests\Feature;

use App\Bid\Bid;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Products\Product;
use App\RangePrice\RangePrice;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class RangePriceControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * @return void
     **/
    public function testEmptyList()
    {
        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/range_price');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson(['data' => []]);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * Test get two range_price objects with success
     * @return void
     **/
    public function testGetRequestShouldReturnResultsOnlyOneCompany()
    {
        $rangePrice = factory(RangePrice::class, 2)->create();
        $client2 = factory(Client::class)->create();
        $company = factory(Company::class)->create();
        $clientCompany2 = factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'company_id' => $company->id,
            'min_order' => 5
        ]);

        factory(RangePrice::class, 10)->create([
            'track' => 4,
            'company_id' => $company->id,
        ]);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")->get('/api/v1/range_price');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertArraySubset([
            'data' => [
                [
                    'type' => 'range_price',
                    'id' => $rangePrice[0]->id,
                    "attributes" => [
                        "product_code" => $rangePrice[0]->product_code,
                        "origin_code" => $rangePrice[0]->origin_code,
                        "track" => $rangePrice[0]->track,
                        "approval_percentage" => $rangePrice[0]->approval_percentage,
                        "reprove_percentage" => $rangePrice[0]->reprove_percentage
                    ]
                ],
                [
                    'type' => 'range_price',
                    'id' => $rangePrice[1]->id,
                    "attributes" => [
                        "product_code" => $rangePrice[1]->product_code,
                        "origin_code" => $rangePrice[1]->origin_code,
                        "track" => $rangePrice[1]->track,
                        "approval_percentage" => $rangePrice[1]->approval_percentage,
                        "reprove_percentage" => $rangePrice[1]->reprove_percentage
                    ]
                ]
            ]
        ], $content);
    }

    /**
     * Test update a range price with success
     * @return void
     **/
    public function testUpdateRangePriceWithSuccess()
    {
        $rangePrice = factory(RangePrice::class)->create();

        $payload = [
            [
                'id' => $rangePrice->id,
                'approval_percentage' => $rangePrice->approval_percentage + 20,
                'reprove_percentage' => $rangePrice->reprove_percentage + 20
            ]
        ];

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put('/api/v1/range_price', $payload);

        $response->assertStatus(204);

            $this->assertDatabaseHas('range_prices', [
                'product_code' => $rangePrice->product_code,
                'origin_code' => $rangePrice->origin_code,
                'track' => $rangePrice->track,
                'approval_percentage' => $payload[0]['approval_percentage'],
                'reprove_percentage' => $payload[0]['reprove_percentage'],
            ]);
    }

    /**
     * Test delete a range price with success
     * @return void
     **/
    public function testDeleteARangePriceWithSuccess()
    {
        $this->markTestSkipped('ENDPOINT NAO USA');
        $rangePrice = factory(RangePrice::class)->create();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->delete("/api/v1/range_price/{$rangePrice->id}");

        $response->assertStatus(204);

        $this->assertSoftDeleted('range_prices', [
            'product_code' => $rangePrice->product_code,
            'origin_code' => $rangePrice->origin_code,
            'track' => $rangePrice->track,
            'approval_percentage' => $rangePrice->approval_percentage,
            'reprove_percentage' => $rangePrice->reprove_percentage,
        ]);
    }

    /**
     *  Approve bids fefo from range price
     *
     * @return null
     */
    public function testApproveBidsFefoRangePrice()
    {
        Notification::fake();
        Queue::fake();
        $this->setUpMockedJwtHelipa();

        $client = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();
        $clientCompany = factory(ClientCompany::class)->create([
            'client_id' => $client->id,
            'min_order' => 5
        ]);

        $company = factory(Company::class)->create();
        $clientCompany2 = factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'company_id' => $company->id,
            'min_order' => 5
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);
        $leadtime2 = $this->createLeadTime($client2->cnpj, 'CD SP', 1, null, $company->id);

        $product = factory(Product::class)->create([
            'weight' => 5,
            'code' => 123,
        ]);

        $product2 = factory(Product::class)->create([
            'weight' => 5,
            'company_id' => $company->id,
            'code' => 123,
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);

        $fefo2 = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime2->code,
            'product_code' => $product2->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
            'company_id' => $product2->company_id,
        ]);

        factory(RangePrice::class)->create([
            'product_code' => $product->code,
            'origin_code' => $leadtime->code,
            'track' => $fefo->faixa_fefo,
            'approval_percentage' => '5',
            'reprove_percentage' => '19',
        ]);

        factory(RangePrice::class)->create([
            'product_code' => $product2->code,
            'origin_code' => $leadtime2->code,
            'track' => $fefo2->faixa_fefo,
            'approval_percentage' => '5',
            'reprove_percentage' => '100',
            'company_id' => $product2->company_id,
        ]);

        // Another good bid
        $bidWithoutAcceptPartial = $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        // unlegible
        $unlegible = $this->createBid($fefo, 20, 8.50, 100, 0, $client->id);

        // Volume exceeding available volume but accept partial
        $bidVolumeNotAvailable = $this->createBid($fefo, 2000, 7.50, 10000, 1, $client->id);

        $invalidBid = $this->createBid($fefo, 10, 7.0, 50, 1, $client->id);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/range_price/approve/fefo/{$fefo->id}");

        $response->assertJsonFragment([
            'accept' => [
                $bidWithoutAcceptPartial->id,
            ],
            'reject' => [
                $bidVolumeNotAvailable->id,
                $invalidBid->id,
            ],
            'unlegible' => [
                $unlegible->id,
            ]
        ]);

        $bidWithoutAcceptPartial = $bidWithoutAcceptPartial->fresh();
        $unlegible = $unlegible->fresh();
        $bidVolumeNotAvailable = $bidVolumeNotAvailable->fresh();
        $invalidBid = $invalidBid->fresh();

        $this->assertEquals($bidWithoutAcceptPartial->status_id, Status::type(Status::APPROVED)->id);
        $this->assertEquals($unlegible->status_id, Status::type(Status::PENDING)->id);
        $this->assertEquals($bidVolumeNotAvailable->status_id, Status::type(Status::CANCELED)->id);
        $this->assertEquals($invalidBid->status_id, Status::type(Status::CANCELED)->id);
        $this->assertEquals(1, RangePrice::where('company_id', $fefo->company_id)->count());
        $this->assertEquals(1, RangePrice::where('company_id', $fefo2->company_id)->count());

        $orderProd1 = Order::with('orderProduct')->where('lance_id', $bidWithoutAcceptPartial->id)
            ->first()->toArray();

        $orderProd1['order_product'] = array_shift($orderProd1['order_product']);

        $this->assertArraySubset(['product_id' => $product->code], $orderProd1['order_product']);

        $this->assertEquals(1, Product::where([
            'company_id' => $fefo->company_id,
            'codigo' => $product->code,
        ])->count());
    }

    /**
     *  Approve bids fefo from range price
     *
     * @return null
     */
    public function testWithoutRangePrice()
    {
        $this->setUpMockedJwtHelipa();

        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'client_id' => $client->id,
            'min_order' => 5
        ]);

        $leadtime = $this->createLeadTime($client->cnpj, 'CD SP', 1);

        $product = factory(Product::class)->create([
            'weight' => 5
        ]);

        $fefo = factory(Fefo::class)->create([
            'date' => date('Y-m-d'),
            'weeks' => 5,
            'leadtime_code' => $leadtime->code,
            'product_code' => $product->code,
            'selling_price' => 9.32,
            'max_price' => 9.85,
            'min_price' => 7.19,
            'changed_admin_price' => 0,
            'highlight_app' => 1,
            'volume' => 300.00,
            'best_before' => null,
        ]);

        $bid = $this->createBid($fefo, 20, 9.00, 100, 0, $client->id);

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->put("/api/v1/range_price/approve/fefo/{$fefo->id}");

        $response->assertJson([]);

        $bid = $bid->fresh();

        $this->assertEquals($bid->status_id, Status::type(Status::PENDING)->id);
    }

    /**
     * Create a bid
     *
     * @param Fefo  $fefo          Fefo Entity
     * @param int   $boxAmount     box Amount
     * @param float $price         Kg price
     * @param float $weight        Total Weight
     * @param int   $partialAccept Partial volume
     * @param int   $clientId      Client ID
     * @param int   $leadTimeCode  LeadTime Code
     * @param int   $companyId     Company ID
     *
     * @return App\Bid\Bid
     **/
    protected function createBid(
        Fefo $fefo,
        int $boxAmount,
        float $price,
        float $weight,
        int $partialAccept,
        int $clientId,
        int $leadTimeCode = null,
        int $companyId = null
    ) {
        return factory(Bid::class)->create([
            'client_id' => $clientId,
            'origin_code' => $leadTimeCode ?? $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'date' => $fefo->date,
            'weeks' => $fefo->weeks,
            'box_amount' => $boxAmount,
            'total_kg' => $weight,
            'kg_price' => $price,
            'price' => $price * $weight,
            'duration' => now()->addDays(2),
            'user_id' => null,
            'company_id' => $companyId ?? $this->company->id,
            'partial_accept' => $partialAccept,
            'status_id' => Status::type(Status::PENDING)->id,
            'client_company_payment_id' => factory(ClientCompanyPayment::class)->create(['client_id' => $clientId])->id,
        ]);
    }

    /**
     * create LeadTime
     *
     * @param  string $cnpj      CNPJ
     * @param  string $name      Name
     * @param  int    $operate   Operate
     * @param  string $code      code
     * @param  string $companyId Company Id
     *
     * @return App\LeadTime\LeadTime
     */
    protected function createLeadTime(string $cnpj, string $name, int $operate, $code = null, $companyId = null)
    {
        $params = [
            'name' => $name,
            'cnpj' => $cnpj,
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => $operate,
            'tuesday' => $operate,
            'wednesday' => $operate,
            'thursday' => $operate,
            'friday' => $operate,
            'saturday' => $operate,
            'sunday' => $operate,
            'operates_saturday' => $operate,
            'operates_sunday' => $operate,
            'company_id' => $companyId ?? $this->company->id
        ];

        if ($code) {
            $params['code'] = $code;
        }

        return factory(LeadTime::class)->create($params);
    }

    /**
     * Method to get a range prices when does not exist a leadtime
     */
    public function testGetRangePriceIfLeadtimeDeleted()
    {
        $rangePrice = factory(RangePrice::class)->create();
        $client2 = factory(Client::class)->create();
        $company = factory(Company::class)->create();
        $clientCompany2 = factory(ClientCompany::class)->create([
            'client_id' => $client2->id,
            'company_id' => $company->id,
            'min_order' => 5
        ]);


        LeadTime::where([
            'cod_origem' => $rangePrice->origin_code
        ])->delete();

        $response = $this->withHeader('Authorization', "Bearer {$this->jwt}")
            ->get('/api/v1/range_price');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');

        $content = json_decode($response->getContent(), true);

        $this->assertEquals($content['data'], []);
    }

}
