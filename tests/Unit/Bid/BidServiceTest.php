<?php

namespace Tests\Unit\Bid;

use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Bid\Bid;
use App\Fefo\Fefo;
use App\Client\Client;
use App\LeadTime\LeadTime;
use App\Core\Entities\Status;
use App\Bid\Services\BidService;

class BidServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
    }

    public function testApproveBidsForFefoShouldApprovedElegibleBid()
    {
        $client = factory(Client::class)->create();

        $fefo = factory(Fefo::class)->create([
            'selling_price' => 18.00,
            'max_price'     => 23.46,
            'min_price'     => 21.35
        ]);

        $leadtime = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client->cnpj
        ]);

        $bid = factory(Bid::class)->create([
            'date' => $fefo->date,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'status_id' => Status::type(Status::PENDING)->id,
            'total_kg' => 12.00,
            'kg_price' => 19.00,
            'price' => 228.00,
            'client_id' => $client->id
        ]);

        $approveBids = (new BidService())->approveBidsForFefo($fefo);

        $this->assertDatabaseHas('lances', [
            'id' => $bid->id,
            'status_id' => Status::type(Status::APPROVED)->id
        ]);
    }

    public function testApproveBidsForFefoInelegibleShouldDoNothing()
    {
        $client = factory(Client::class)->create();

        $fefo = factory(Fefo::class)->create([
            'selling_price' => 20.00,
            'max_price'     => 23.46,
            'min_price'     => 21.35
        ]);

        $leadtime = factory(LeadTime::class)->state('canPurchase')->create([
            'cnpj' => $client->cnpj
        ]);

        $bid = factory(Bid::class)->create([
            'date' => $fefo->date,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'status_id' => Status::type(Status::PENDING)->id,
            'total_kg' => 12.00,
            'kg_price' => 19.00,
            'price' => 228.00,
            'client_id' => $client->id
        ]);

        $approveBids = (new BidService())->approveBidsForFefo($fefo);

        $this->assertDatabaseHas('lances', [
            'id' => $bid->id,
            'status_id' => Status::type(Status::PENDING)->id
        ]);
    }
}
