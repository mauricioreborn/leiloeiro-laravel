<?php

namespace Tests\Unit\Company;

use App\Company\Company;
use App\Company\CompanySetting;
use App\Company\Services\CompanySettingService;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CompanySettingServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Method to test default settings in new company
     *
     * @return null
     */
    public function testCreateCompanyWithSettingsDefault()
    {
        $company = factory(Company::class)->create();

        $this->assertEquals(30, $company->settings->get('bidMinAcceptablePercent'));
    }

    /**
     * Method to test createBidMinAcceptablePercent
     *
     * @return null
     */
    public function testCreateBidMinAcceptablePercent()
    {
        $company = factory(Company::class)->create();

        CompanySetting::where('company_id', $company->id)
            ->where('setting', 'bidMinAcceptablePercent')
            ->delete();

        (new CompanySettingService())->createBidMinAcceptablePercent($company, 50);

        $company = $company->fresh();

        $this->assertEquals(50, $company->settings->get('bidMinAcceptablePercent'));
    }

    /**
     * Method to test create or update
     *
     * @return null
     */
    public function testCreateOrUpdateSetting()
    {
        $company = factory(Company::class)->create();

        $companySetting = factory(CompanySetting::class)->create([
            'company_id' => $company->id,
            'setting' => 'setting1'
        ]);

        (new CompanySettingService())->create($company, $companySetting->setting, 50);

        $companySetting = $companySetting->fresh();

        $this->assertEquals(50, $companySetting->value);
    }
}
