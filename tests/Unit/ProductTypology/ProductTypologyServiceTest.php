<?php

namespace Tests\Unit\ProductTypology;

use App\ProductTypology\ProductTypology;
use App\ProductTypology\Services\ProductTypologyService;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTypologyServiceTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwtClient();
    }

    /**
     * Tests get products of client typology
     *
     * @return void
     */
    public function testGetProductsProductTypology()
    {
        $clientCompany = $this->client->setClientCompany($this->company);
        $clientCompany->update(['typology' => 'idk', 'typology_code' => 1]);

        $product = factory(Product::class)->create([
            'code' => 4321,
            'company_id' => $this->company->id,
        ]);

        factory(ProductTypology::class)->create([
            'product_code' => $product->code,
            'typology_code' => $clientCompany->typology_code,
            'state' => 'Brasil',
            'company_id' => $this->company->id,
        ]);

        $products = $this->getProducts();

        $this->assertFalse($products);

        factory(ProductTypology::class)->create([
            'product_code' => $product->code,
            'typology_code' => $clientCompany->typology_code,
            'state' => $clientCompany->state,
            'company_id' => $this->company->id,
            'track_1' => 2,
            'track_2' => 2,
            'track_3' => 2,
            'track_4' => 2,
        ]);

        $products = $this->getProducts();

        $this->assertEquals($products->count(), 0);

        $productTypology = factory(ProductTypology::class)->create([
            'product_code' => $product->code,
            'typology_code' => $clientCompany->typology_code,
            'state' => $clientCompany->state,
            'company_id' => $this->company->id,
            'track_2' => 2,
            'track_4' => 2,
        ]);

        $products = $this->getProducts();

        $this->assertEquals($products->count(), 1);

        $this->assertEquals($products->first()->tracks, [
            '001',
            '003'
        ]);

        $this->client = $this->client->fresh();

        $clientCompany->track_1 = 2;
        $clientCompany->save();

        $products = $this->getProducts();

        $this->assertEquals($products->count(), 1);

        $this->assertEquals($products->first()->tracks, [
            '003'
        ]);

        $this->assertEquals(ProductTypology::count(), 3);
    }

    /**
     * Invoke getProducts in ProductTypologyService
     *
     * @return Illuminate\Database\Eloquent\Collection | boolean
     */
    protected function getProducts()
    {
        return (new ProductTypologyService())->getProducts($this->client, $this->company);
    }
}
