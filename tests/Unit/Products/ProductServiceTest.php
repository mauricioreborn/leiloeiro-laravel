<?php

namespace Tests\Unit\Products;

use App\Auth\User;
use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Company\Company;
use App\Company\Services\CompanySettingService;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use App\Mobile\Services\ProductService;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Products\Product;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to SetUp the tests
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
    }

    /**
     * Method to test show product service
     *
     * @return null
     */
    public function testShowProductService()
    {
        $client1 = Client::first();
        $company = Company::first();

        $clientCompany = ClientCompany::where('company_id', $company->id)
            ->where('client_id', $client1->id)
            ->first();

        $clientCompany->update([
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
        ]);

        $leadtime1 = factory(LeadTime::class)->create([
            'name'              => 'SAO PAULO - SP',
            'cnpj'              => $client1->cnpj,
            'days'              => 1,
            'prediction'        => 1,
            'limit_hour'        => 2300,
            'monday'            => 1,
            'tuesday'           => 1,
            'wednesday'         => 1,
            'thursday'          => 1,
            'friday'            => 1,
            'saturday'          => 1,
            'sunday'            => 1,
            'operates_saturday' => 1,
            'operates_sunday'   => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
        ]);


        $this->assertEquals(Client::count(), 1);
        $this->assertEquals(LeadTime::count(), 1);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $order = factory(Order::class)->create([
            'client_id' => $client1->id
        ]);

        $product = Product::first();

        $ordeProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'product_id' => $product->id
        ]);

        $product->users()->save(factory(User::class)->create());
        ;
        $productService = new ProductService();

        $returnService = $productService->show(
            $client1,
            $this->company,
            $fefo->id
        );

        $boxRange = ($product->weight > 0) ? floor(floatval($fefo->volume)/floatval($product->weight)) : 0;
        $boxes = trans_choice('Mobile/order.boxes', $boxRange, ['box' => $boxRange]);

        $attributes = [
            'unidadeMedida' => getUnit($fefo->selling_price, $product->weight_piece, $product->unit, true),
            'padraoMedida' => ucfirst(strtolower($product->unit)),
            'gramatura' => formatGrammage($product->weight_piece),
        ];

        $this->assertEquals($returnService['fefo_id'], $fefo->id);
        $this->assertEquals($returnService['codigo_produto'], $product->code);
        $this->assertEquals(
            $returnService['img'],
            env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$this->company->id. '/' .$product->foto
        );
        $this->assertEquals($returnService['nome'], $product->nome);
        $this->assertEquals($returnService['empresa'], $product->brand);
        $this->assertEquals($returnService['caixas'], $boxes);
        $this->assertEquals($returnService['pesoCaixa'], volume($product->peso_caixa));
        $this->assertEquals($returnService['dimensaoCaixa'], $product->exibicao_app);
        $this->assertEquals(
            $returnService['validade'],
            trans_choice('Mobile/order.weeks',
                $fefo->semanas,
                ['weeks' => $fefo->semanas]
            ));
        $this->assertEquals($returnService['disponivel'], rangeCaixas($boxRange));
        $this->assertEquals($returnService['precoAtual'], formata_moeda($fefo->selling_price));
        $this->assertEquals(
            $returnService['precoAtualUn'],
            unitPrice($fefo->selling_price, $product->weight_piece)
        );
        $this->assertEquals($returnService['unidadeMedida'], $attributes['unidadeMedida']);
        $this->assertEquals($returnService['padraoMedida'], $attributes['padraoMedida']);
        $this->assertEquals($returnService['gramatura'], $attributes['gramatura']);
    }

    /**
     * Method to test the List Product without attributes
     *
     * @return void
     **/
    public function testListProductServiceWithoutAttributes()
    {
        $client1 = Client::first();
        $company = Company::first();

        $clientCompany = ClientCompany::where('company_id', $company->id)
            ->where('client_id', $client1->id)
            ->first();

        $clientCompany->update([
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
        ]);

        $leadtime1 = factory(LeadTime::class)->create([
            'name'              => 'SAO PAULO - SP',
            'cnpj'              => $client1->cnpj,
            'days'              => 1,
            'prediction'        => 1,
            'limit_hour'        => 2300,
            'monday'            => 1,
            'tuesday'           => 1,
            'wednesday'         => 1,
            'thursday'          => 1,
            'friday'            => 1,
            'saturday'          => 1,
            'sunday'            => 1,
            'operates_saturday' => 1,
            'operates_sunday'   => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
        ]);


        $this->assertEquals(Client::count(), 1);
        $this->assertEquals(LeadTime::count(), 1);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $order = factory(Order::class)->create([
            'client_id' => $client1->id
        ]);

        $product = Product::first();

        $ordeProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'product_id' => $product->id
        ]);

        $product->users()->save(factory(User::class)->create());
        ;
        $productService = new ProductService();

        $returnService = $productService->getAreasProducts(
            $client1,
            $this->company,
            []
        );

        $produtos = $returnService['produtos'][0];

        $boxRange = ($product->weight > 0)
            ?
            floor(floatval($fefo->volume)/floatval($product->weight))
            :
            0;

        $validateLabel = (new CompanySettingService())->getPeriodView($this->company, $fefo->weeks);

        $this->assertEquals($returnService['status'], true);
        $this->assertEquals($returnService['showOrder'], true);
        $this->assertEquals($returnService['showFilter'], false);
        $this->assertEquals($returnService['total_produtos'], 1);
        $this->assertEquals($produtos['id_fefo'], $fefo->id);
        $this->assertEquals($produtos['codigo_produto'], $product->code);
        $this->assertEquals($produtos['empresa'], $product->brand);
        $this->assertEquals($produtos['nome'], $product->name);
        $this->assertEquals(
            $produtos['img'],
            env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$this->company->id. '/'. $product->foto
        );
        $this->assertEquals($produtos['pesoCaixa'], volume($product->weight));
        $this->assertEquals($produtos['caixas'], $boxRange);
        $this->assertEquals($produtos['dimensaoCaixa'], $product->app_exhibition);
        $this->assertEquals($produtos['validade'], $fefo->weeks.' '. $validateLabel);
        $this->assertEquals($produtos['precoMin'], $fefo->selling_price);
        $this->assertEquals($produtos['precoMax'], $fefo->max_price);
        $this->assertEquals($produtos['precoAtual'], $fefo->selling_price);
        $this->assertEquals(
            $produtos['precoMinUn'],
            unitPrice($fefo->selling_price, $product->weight_piece)
        );
        $this->assertEquals(
            $produtos['precoMaxUn'],
            unitPrice(
                $fefo->max_price,
                $product->weight_piece
            )
        );
        $this->assertEquals(
            $produtos['precoAtualUn'],
            unitPrice(
                $fefo->selling_price,
                $product->weight_piece
            )
        );
        $this->assertEquals(
            $produtos['unidadeMedida'],
            getUnit(
                $fefo->selling_price,
                $product->weight_piece,
                $product->unit,
                true
            )
        );
        $this->assertEquals(
            $produtos['padraoMedida'], ucfirst(strtolower($product->unit)));
    }

    /**
     * Method to test the List Product with attributes
     *
     * @return void
     */
    public function testListProductServiceWithAttributes()
    {
        $client1 = Client::first();
        $company = Company::first();

        $clientCompany = ClientCompany::where('company_id', $company->id)
            ->where('client_id', $client1->id)
            ->first();

        $clientCompany->update([
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
        ]);

        $leadtime1 = factory(LeadTime::class)->create([
            'name'              => 'SAO PAULO - SP',
            'cnpj'              => $client1->cnpj,
            'days'              => 1,
            'prediction'        => 1,
            'limit_hour'        => 2300,
            'monday'            => 1,
            'tuesday'           => 1,
            'wednesday'         => 1,
            'thursday'          => 1,
            'friday'            => 1,
            'saturday'          => 1,
            'sunday'            => 1,
            'operates_saturday' => 1,
            'operates_sunday'   => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
        ]);

        $this->assertEquals(Client::count(), 1);
        $this->assertEquals(LeadTime::count(), 1);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $product = Product::first();

        $product->users()->save(factory(User::class)->create());
        ;
        $productService = new ProductService();

        $filter['category'] = $product->category;
        $filter['type'] = $product->type;
        $filter['weeks'] = $fefo->weeks;
        $filter['min_selling_price'] = $fefo->selling_price;
        $filter['max_selling_price'] = $fefo->max_price;

        $returnService = $productService->getAreasProducts(
            $client1,
            $this->company,
            $filter
        );

        $produtos = $returnService['produtos'][0];


        $boxRange = ($product->weight > 0)
            ?
            floor(floatval($fefo->volume)/floatval($product->weight))
            :
            0;

        $validateLabel = (new CompanySettingService())->getPeriodView($this->company, $fefo->weeks);

        $this->assertEquals($returnService['status'], true);
        $this->assertEquals($returnService['showOrder'], true);
        $this->assertEquals($returnService['showFilter'], false);
        $this->assertEquals($returnService['total_produtos'], 1);
        $this->assertEquals($produtos['id_fefo'], $fefo->id);
        $this->assertEquals($produtos['codigo_produto'], $product->code);
        $this->assertEquals($produtos['empresa'], $product->brand);
        $this->assertEquals($produtos['nome'], $product->name);
        $this->assertEquals(
            $produtos['img'],
            env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$this->company->id. '/'. $product->foto
        );
        $this->assertEquals($produtos['pesoCaixa'], volume($product->weight));
        $this->assertEquals($produtos['caixas'], $boxRange);
        $this->assertEquals($produtos['dimensaoCaixa'], $product->app_exhibition);
        $this->assertEquals($produtos['validade'], $fefo->weeks.' '. $validateLabel);
        $this->assertEquals($produtos['precoMin'], $fefo->selling_price);
        $this->assertEquals($produtos['precoMax'], $fefo->max_price);
        $this->assertEquals($produtos['precoAtual'], $fefo->selling_price);
        $this->assertEquals(
            $produtos['precoMinUn'],
            unitPrice($fefo->selling_price, $product->weight_piece)
        );
        $this->assertEquals(
            $produtos['precoMaxUn'],
            unitPrice(
                $fefo->max_price,
                $product->weight_piece
            )
        );
        $this->assertEquals(
            $produtos['precoAtualUn'],
            unitPrice(
                $fefo->selling_price,
                $product->weight_piece
            )
        );
        $this->assertEquals(
            $produtos['unidadeMedida'],
            getUnit(
                $fefo->selling_price,
                $product->weight_piece,
                $product->unit,
                true
            )
        );

        $date = array_get([], 'date', today()->toDateString());

        $filters = $this->invokeMethod(
            $productService,
            'getFiltros',
            [
                $client1,
                $this->company,
                $product->category,
                false,
                $date
            ]
        );


        $this->assertEquals(
            $returnService['valor_min'],
            floor($filters['valor_min'])
        );

        $this->assertEquals(
            $returnService['valor_max'],
            ceil($filters['valor_max'])
        );

        $this->assertEquals(
            $returnService['valor_min_exibicao'],
            formata_moeda($filters['valor_min'])
        );

        $this->assertEquals(
            $returnService['valor_max_exibicao'],
            formata_moeda($filters['valor_max'])
        );

        $this->assertEquals(
            $returnService['classes'],
            $filters['classes']
        );

        $this->assertEquals(
            $returnService['semanas'],
            $filters['semanas']
        );

        $this->assertEquals(
            $produtos['padraoMedida'], ucfirst(strtolower($product->unit)));
    }

    /**
     * Method to test the List Product with attributes and filters
     *
     * @return void
     */
    public function testListProductServiceWithAttributesAndFiltersEqualsFalse()
    {
        $client1 = Client::first();
        $company = Company::first();

        $clientCompany = ClientCompany::where('company_id', $company->id)
            ->where('client_id', $client1->id)
            ->first();

        $clientCompany->update([
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
        ]);

        $leadtime1 = factory(LeadTime::class)->create([
            'name'              => 'SAO PAULO - SP',
            'cnpj'              => $client1->cnpj,
            'days'              => 1,
            'prediction'        => 1,
            'limit_hour'        => 2300,
            'monday'            => 1,
            'tuesday'           => 1,
            'wednesday'         => 1,
            'thursday'          => 1,
            'friday'            => 1,
            'saturday'          => 1,
            'sunday'            => 1,
            'operates_saturday' => 1,
            'operates_sunday'   => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
        ]);


        $this->assertEquals(Client::count(), 1);
        $this->assertEquals(LeadTime::count(), 1);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $product = Product::first();

        $product->users()->save(factory(User::class)->create());
        ;
        $productService = new ProductService();

        $filter['categoy'] = $product->category;
        $filter['type'] = $product->type;
        $filter['weeks'] = $fefo->weeks;
        $filter['min_selling_price'] = $fefo->selling_price;
        $filter['max_selling_price'] = $fefo->max_price;

        $returnService = $productService->getAreasProducts(
            $client1,
            $this->company,
            $filter,
            $includeFilters = false
        );

        $produtos = $returnService['produtos'][0];


        $boxRange = ($product->weight > 0)
            ?
            floor(floatval($fefo->volume)/floatval($product->weight))
            :
            0;

        $boxes = trans_choice('Mobile/order.boxes', $boxRange, ['box' => $boxRange]);
        $validateLabel = (new CompanySettingService())->getPeriodView($this->company, $fefo->weeks);

        $this->assertEquals($returnService['status'], true);
        $this->assertEquals($returnService['showOrder'], true);
        $this->assertEquals($returnService['showFilter'], false);
        $this->assertEquals($returnService['total_produtos'], 1);
        $this->assertEquals($produtos['id_fefo'], $fefo->id);
        $this->assertEquals($produtos['codigo_produto'], $product->code);
        $this->assertEquals($produtos['empresa'], $product->brand);
        $this->assertEquals($produtos['nome'], $product->name);
        $this->assertEquals(
            $produtos['img'],
            env('PRODUCT_IMAGES_BASE_URL'). 'souk-company/' .$this->company->id. '/'. $product->foto
        );
        $this->assertEquals($produtos['pesoCaixa'], volume($product->weight));
        $this->assertEquals($produtos['caixas'], $boxRange);
        $this->assertEquals($produtos['dimensaoCaixa'], $product->app_exhibition);
        $this->assertEquals($produtos['validade'], $fefo->weeks.' '. $validateLabel);
        $this->assertEquals($produtos['precoMin'], $fefo->selling_price);
        $this->assertEquals($produtos['precoMax'], $fefo->max_price);
        $this->assertEquals($produtos['precoAtual'], $fefo->selling_price);
        $this->assertEquals(
            $produtos['precoMinUn'],
            unitPrice($fefo->selling_price, $product->weight_piece)
        );
        $this->assertEquals(
            $produtos['precoMaxUn'],
            unitPrice(
                $fefo->max_price,
                $product->weight_piece
            )
        );
        $this->assertEquals(
            $produtos['precoAtualUn'],
            unitPrice(
                $fefo->selling_price,
                $product->weight_piece
            )
        );
        $this->assertEquals(
            $produtos['unidadeMedida'],
            getUnit(
                $fefo->selling_price,
                $product->weight_piece,
                $product->unit,
                true
            )
        );

        $this->assertEquals(
            $produtos['padraoMedida'], ucfirst(strtolower($product->unit)));
    }

    /**
     * Method to test getFilters
     *
     * @return void
     */
    public function testGetFilters()
    {
        $client1 = Client::first();
        $company = Company::first();

        $clientCompany = ClientCompany::where('company_id', $company->id)
            ->where('client_id', $client1->id)
            ->first();

        $clientCompany->update([
            'track_1' => 1,
            'track_2' => 1,
            'track_3' => 1,
            'track_4' => 1,
        ]);

        $leadtime1 = factory(LeadTime::class)->create([
            'name'              => 'SAO PAULO - SP',
            'cnpj'              => $client1->cnpj,
            'days'              => 1,
            'prediction'        => 1,
            'limit_hour'        => 2300,
            'monday'            => 1,
            'tuesday'           => 1,
            'wednesday'         => 1,
            'thursday'          => 1,
            'friday'            => 1,
            'saturday'          => 1,
            'sunday'            => 1,
            'operates_saturday' => 1,
            'operates_sunday'   => 1,
            'company_id' => $this->company->id
        ]);

        $fefo = factory(Fefo::class)->create([
            'date'                => date('Y-m-d'),
            'weeks'               => 5,
            'leadtime_code'       => $leadtime1->code,
            'selling_price'       => 9.32,
            'max_price'           => 9.85,
            'min_price'           => 7.19,
            'changed_admin_price' => 0,
            'highlight_app'       => 1,
            'volume'              => 5160.00,
            'best_before'         => null,
        ]);

        $this->assertEquals(Client::count(), 1);
        $this->assertEquals(LeadTime::count(), 1);
        $this->assertEquals(Fefo::count(), 1);
        $this->assertEquals(Product::count(), 1);

        $product = Product::first();

        $product->users()->save(factory(User::class)->create());

        $productService = new ProductService();

        $method = $this->invokeMethod(
            $productService,
            'getFiltros',
            [
                $client1,
                $this->company,
                $product->category
            ]
        );

        $validateLabel = (new CompanySettingService())->getPeriodView($this->company, $fefo->semanas);

        $this->assertEquals($method['valor_min'], $fefo->selling_price);
        $this->assertEquals($method['valor_max'], $fefo->selling_price);
        $this->assertEquals($method['classes'][0], $product->type);
        $this->assertEquals($method['semanas'][$fefo->weeks], "$fefo->weeks $validateLabel");
    }
}
