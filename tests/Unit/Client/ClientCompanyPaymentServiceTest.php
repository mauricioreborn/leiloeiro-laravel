<?php

namespace Tests\Unit\Company;

use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\ClientCompanies\ClientCompany;
use App\Client\Services\ClientCompanyPaymentService;
use App\Company\Company;
use App\Company\CompanyPayment;

class ClientCompanyPaymentServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected $clientCompanyPaymentService;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->clientCompanyPaymentService = new ClientCompanyPaymentService();
    }

    public function testCreateWithSuccess()
    {
        $client = factory(Client::class)->create();
        $company = factory(Company::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $company->id,
            'client_id' => $client->id
        ]);

        $companyPayment = factory(CompanyPayment::class)->state('companyCredit')->create();

        $response = $this->clientCompanyPaymentService->create(
            $client,
            $companyPayment
        );

        $this->assertEquals($response->client_id, $client->id);
        $this->assertEquals($response->company_payment_id, $companyPayment->id);
    }

    public function testCreateMethodShouldRestoreWithSuccess()
    {
        $client = factory(Client::class)->create();
        $company = factory(Company::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $company->id,
            'client_id' => $client->id
        ]);

        $companyPayment = factory(CompanyPayment::class)->state('companyCredit')->create();

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->state('companyCredit')->create([
            'client_id' => $client->id,
            'company_payment_id' => $companyPayment->id
        ]);

        $clientCompanyPayment->delete();

        $response = $this->clientCompanyPaymentService->create(
            $client,
            $companyPayment
        );
        
        $this->assertEquals($response->client_id, $client->id);
        $this->assertEquals($response->company_payment_id, $companyPayment->id);
    }
}
