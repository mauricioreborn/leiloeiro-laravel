<?php

namespace Tests\Unit\BidWorkers;

use App\Bid\Bid;
use App\Bid\Services\BidService;
use App\Company\Company;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Notification\Notification;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExpireBidsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     * @throws \Exception
     */
    public function testEmptyListShouldNotCreateNotifications()
    {
        (new BidService())->expire();
        $this->assertEquals(0, Notification::count());
        $this->assertEquals(0, Bid::count());
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testListWithExpiredBidsExpectsNotificationForAll()
    {
        factory(Company::class)->create();

        factory(Bid::class, 10)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'duracao_lance' => now()->subDays(2),
        ]);

        (new BidService())->expire();
        $this->assertEquals(10, Notification::count());
        $this->assertEquals(10, Bid::count());
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testListWithMixedBidsExpectsNotificationForExpiredOnly()
    {
        factory(Company::class)->create();

        factory(Bid::class, 10)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'duracao_lance' => now()->subDays(2),
        ]);

        factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'date' => now()->addDays(2),
        ]);

        factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'date' => now(),
        ]);

        (new BidService())->expire();

        $this->assertEquals(10, Notification::count());
        $this->assertEquals(12, Bid::count());
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testListWithExpiredBidsShouldUpdateBidsStatus()
    {
        factory(Company::class)->create();

        factory(Bid::class, 10)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'duracao_lance' => now()->subDays(2),
        ]);

        (new BidService())->expire();

        $updatedBids = Bid::where([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::EXPIRED)->id,
        ])->count();

        $this->assertEquals(10, $updatedBids);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testListWithMixedBidsShouldUpdateExpiredOnly()
    {
        factory(Company::class)->create();

        factory(Bid::class, 10)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'duracao_lance' => now()->subDays(2),
        ]);

        factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'date' => now()->addDays(2),
        ]);

        factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'date' => now(),
        ]);

        (new BidService())->expire();

        $updatedBids = Bid::where([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::EXPIRED)->id,
        ])->count();

        $this->assertEquals(10, $updatedBids);
    }

    /**
     * @return void
     */
    public function testBidsFromDifferentCompaniesShouldExpireOnlyOwnBids()
    {
        $company1 = factory(Company::class)->create();
        $company2 = factory(Company::class)->create();

        $produto1 = factory(Product::class)->create([
            'company_id' => $company1->id,
            'code' => '123'
        ]);

        $produto2 = factory(Product::class)->create([
            'company_id' => $company2->id,
            'code' => '123'
        ]);

        factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'duracao_lance' => now()->subDays(2),
            'company_id' => $company1->id,
            'product_code' => '123',
        ]);

        factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'duracao_lance' => now()->addDays(2),
            'company_id' => $company2->id,
            'product_code' => '123',
        ]);

        (new BidService())->expire();

        $updatedBids = Bid::where([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::EXPIRED)->id,
        ])->count();

        $this->assertEquals(1, $updatedBids);
    }

    public function testBidsFromProductInactive()
    {
        $company = factory(Company::class)->create();

        $produto1 = factory(Product::class)->create([
            'company_id' => $company->id,
            'code' => '123'
        ]);

        $produto2 = factory(Product::class)->create([
            'company_id' => $company->id,
            'code' => '1234'
        ]);

        factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'duracao_lance' => now()->subDays(2),
            'company_id' =>  $company->id,
            'product_code' => '123',
        ]);

        factory(Bid::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'duracao_lance' => now()->subDays(2),
            'company_id' =>  $company->id,
            'product_code' => '1234',
        ]);

        $produto2->delete();

        (new BidService())->expire();

        $updatedBids = Bid::where([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::EXPIRED)->id,
        ])->count();

        $this->assertEquals(2, $updatedBids);
    }
}
