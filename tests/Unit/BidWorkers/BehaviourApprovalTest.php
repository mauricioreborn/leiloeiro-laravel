<?php

namespace Tests\Unit\BidWorkers;

use App\Auth\User;
use App\Bid\Bid;
use App\Bid\Services\BehaviourApprovalService;
use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Core\Console\Commands\BehaviourApproval;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\LeadTime\LeadTime;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BehaviourApprovalTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * CENÁRIOS
     *
     * Lances com rejeição no dia deve rejeitar todos os outros lances abaixo
     * Lances com aprovação no dia deve aprovar lances acima
     * Lances com rejeição e aprovação no mesmo valor, deve priorizar rejeição
     * Lances sem aprovação ou rejeição no dia não devem ser aprovados
     */

    public function testBidsRejectedTodayShouldRejectAllBidsWithLowerPrice()
    {
        $company = Company::first();
        $soukBot = $company->users()->where('name', 'soukBot')->first();

        // Cadastra um FEFO
        $fefo = factory(Fefo::class)->create([
            'company_id' => $company->id,
            'selling_price' => 10.00,
            'min_price' => 5.00,
            'volume' => 50000.00,
            'date' => now()->toDateString(),
        ]);

        // Cadastra um lance rejeitado (histórico de rejeição do dia)
        factory(Bid::class)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 9.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::CANCELED)->id,
        ]);

        // Cadastra um lance com valor abaixo
        $bidBelow = factory(Bid::class)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 8.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Cadastra um lance com valor acima
        $bidAbove = factory(Bid::class)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 10.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $key = "{$fefo->leadtime_code}.{$fefo->product_code}.{$fefo->weeks}";
        $thresholds = [
            'rejected' => 9,
        ];
        (new BehaviourApprovalService())->processFefoBids($soukBot, $key, $thresholds, now()->toDateString());

        // Valida que lance com valor abaixo foi rejeitado
        $bidBelow = $bidBelow->fresh();

        $this->assertNotNull($bidBelow->sanctioned_at);
        $this->assertEquals($bidBelow->status_id, Status::type(Status::CANCELED)->id);
        $this->assertEquals(
            $bidBelow->rejection_motives_id,
            RejectionMotive::type(RejectionMotive::BEHAVIOUR_REJECTION)->id
        );

        // Valida que lance com valor acima continua pendente
        $bidAbove = $bidAbove->fresh();

        $this->assertNull($bidAbove->sanctioned_at);
        $this->assertEquals($bidAbove->status_id, Status::type(Status::PENDING)->id);
    }

    public function testBidsApprovedTodayShouldApproveBidsWithHigherPrice()
    {
        $company = Company::first();
        $soukBot = $company->users()->where('name', 'soukBot')->first();

        // Cadastra um FEFO
        $fefo = factory(Fefo::class)->create([
            'company_id' => $company->id,
            'selling_price' => 10.00,
            'min_price' => 5.00,
            'volume' => 50000.00,
            'date' => now()->toDateString(),
        ]);

        // Cadastra um lance aprovado (histórico de aprovação do dia)
        factory(Bid::class)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 9.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        // Cadastra um lance com valor abaixo
        $bidBelow = factory(Bid::class)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 8.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        // Cadastra um lance com valor acima

        // Mocks para condições de aprovação
        $client = Client::first();
        factory(ClientCompany::class)->create([
            'min_order' => 5,
            'client_id' => $client->id
        ]);
        factory(LeadTime::class)->state('canPurchase')->create([
            'code' => $fefo->leadtime_code,
            'cnpj' => $client->cnpj,
            'company_id' => $company->id,
        ]);

        $bidAbove = factory(Bid::class)->create([
            'company_id' => $company->id,
            'client_id' => $client->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 10.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $key = "{$fefo->leadtime_code}.{$fefo->product_code}.{$fefo->weeks}";
        $thresholds = [
            'approved' => 9,
        ];
        (new BehaviourApprovalService())->processFefoBids($soukBot, $key, $thresholds, now()->toDateString());

        // Valida que lance com valor abaixo continua pendente
        $bidAbove = $bidAbove->fresh();

        $this->assertNotNull($bidAbove->sanctioned_at);
        $this->assertEquals($bidAbove->status_id, Status::type(Status::APPROVED)->id);

        // Valida que lance com valor acima foi aprovado
        $bidBelow = $bidBelow->fresh();

        $this->assertNull($bidBelow->sanctioned_at);
        $this->assertEquals($bidBelow->status_id, Status::type(Status::PENDING)->id);
    }

    public function testBidsApprovedAndRejectedAtTheSamePriceShouldRejectBidsWithTheSamePrice()
    {
        $company = factory(Company::class)->create();
        $soukBot = factory(User::class)->create([
            'name' => 'soukBot',
            'company_id' => $company->id,
        ]);

        // Cadastra um FEFO
        $fefo = factory(Fefo::class)->create([
            'company_id' => $company->id,
            'selling_price' => 10.00,
            'min_price' => 5.00,
            'volume' => 50000.00,
            'date' => now()->toDateString(),
        ]);

        // Cadastra um lance aprovado (histórico de aprovação do dia)
        factory(Bid::class)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 9.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        // Cadastra um lance rejeitado de mesmo valor (histórico de aprovação do dia)
        factory(Bid::class)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 9.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::CANCELED)->id,
        ]);

        // Cadastra um lance pendente de mesmo valor
        $bid = factory(Bid::class)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'kg_price' => 9.00,
            'total_kg' => 1000,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $key = "{$fefo->leadtime_code}.{$fefo->product_code}.{$fefo->weeks}";
        $thresholds = [
            'approved' => 9,
            'rejected' => 9,
        ];
        (new BehaviourApprovalService())->processFefoBids($soukBot, $key, $thresholds, now()->toDateString());

        // Valida que lance pendente foi rejeitado
        $bid = $bid->fresh();

        $this->assertNotNull($bid->sanctioned_at);
        $this->assertEquals($bid->status_id, Status::type(Status::CANCELED)->id);
    }

    public function testBidsNotApprovedOrRejectedTodayShouldNotApproveOrRejectOtherBids()
    {
        $company = factory(Company::class)->create();
        $soukBot = factory(User::class)->create([
            'name' => 'soukBot',
            'company_id' => $company->id,
        ]);

        // Cadastra um FEFO
        $fefo = factory(Fefo::class)->create([
            'company_id' => $company->id,
            'selling_price' => 10.00,
            'min_price' => 5.00,
            'volume' => 50000.00,
            'date' => now()->toDateString(),
        ]);

        // Cadastra 3 lances
        factory(Bid::class, 3)->create([
            'company_id' => $company->id,
            'origin_code' => $fefo->leadtime_code,
            'product_code' => $fefo->product_code,
            'weeks' => $fefo->weeks,
            'date' => now()->toDateString(),
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $key = "{$fefo->leadtime_code}.{$fefo->product_code}.{$fefo->weeks}";
        (new BehaviourApprovalService())->processFefoBids($soukBot, $key, [], now()->toDateString());

        // Valida que lances continuam pendentes
        $this->assertEquals(0, Bid::where('status_id', Status::type(Status::CANCELED)->id)->count());
        $this->assertEquals(0, Bid::where('status_id', Status::type(Status::APPROVED)->id)->count());
        $this->assertEquals(3, Bid::where('status_id', Status::type(Status::PENDING)->id)->count());
    }
}
