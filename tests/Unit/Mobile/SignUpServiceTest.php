<?php
namespace Tests\Unit\Mobile;

use App\Client\Client;
use App\Client\ClientConfirmation;
use App\Client\IndependentClient;
use App\Client\Notifications\MobileConfirmationNotification;
use App\Mobile\Exceptions\MobileException;
use App\Mobile\Services\SignUpService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class SignUpServiceTest extends TestCase
{
    use DatabaseTransactions;
    const SOUK_CNPJ = 11132690000159;
    const DUMMY_PHONE = 11998877665;

    protected $signUpService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->signUpService = new SignUpService();
    }

    public function testIndependentClientWithContactInformationShouldReturnTrue()
    {
        $client = factory(IndependentClient::class)->create([
            'cnpj' => self::SOUK_CNPJ,
        ]);
        $this->assertTrue($this->signUpService->independentHasInformationContact($client));
    }

    public function testIndependentClientWithoutContactInformationShouldReturnFalse()
    {
        $client = factory(IndependentClient::class)->state('withoutContactInformation')->create([
            'cnpj' => self::SOUK_CNPJ,
        ]);
        $this->assertFalse($this->signUpService->independentHasInformationContact($client));
    }

    public function testRegistrationCompleteShouldReturnTrue()
    {
        $client = factory(IndependentClient::class)->create([
            'cnpj' => self::SOUK_CNPJ,
        ]);
        $this->assertTrue($this->signUpService->independentRegistrationIsComplete($client));
    }

    public function testRegistrationIncompleteShouldReturnFalse()
    {
        $client = factory(IndependentClient::class)->state('newRegistration')->create([
            'cnpj' => self::SOUK_CNPJ,
        ]);
        $this->assertFalse($this->signUpService->independentRegistrationIsComplete($client));
    }

    public function testIndependentClientWithUnconfirmedPhoneShouldReturnPhoneValidationStep()
    {
        $client = factory(IndependentClient::class)->state('newRegistration')->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => null,
        ]);
        $this->assertEquals($this->signUpService::STEP_PHONE_VALIDATION,
            $this->signUpService->verifyRegistrationStepIndependent($client));
    }

    public function testIndependentClientWithIncompleteSignUpShouldReturnIndependentSignUpStep()
    {
        $client = factory(IndependentClient::class)->state('newRegistration')->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 10,
        ]);
        $this->assertEquals($this->signUpService::STEP_INDEPENDENT_SIGN_UP,
            $this->signUpService->verifyRegistrationStepIndependent($client));
    }

    public function testIndependentClientWithoutContactInformationShouldReturnContactInformationStep()
    {
        $client = factory(IndependentClient::class)->state('withoutContactInformation')->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 10,
        ]);
        $this->assertEquals($this->signUpService::STEP_CONTACT_INFORMATION,
            $this->signUpService->verifyRegistrationStepIndependent($client));
    }

    public function testIndependentClientWithCompletedSignUpShouldReturnCompletedStep()
    {
        $client = factory(IndependentClient::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 10,
        ]);
        $this->assertEquals($this->signUpService::STEP_COMPLETED,
            $this->signUpService->verifyRegistrationStepIndependent($client));
    }

    public function testIndependentClientValidationWithCompletedSignUpShouldReturnClientExistsMessage()
    {
        $client = factory(IndependentClient::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 10,
        ]);

        $stub = [
            'status' => true,
            'message' => __('Mobile/signUp.first_step.independent_client_exists', ['email' => $client->email]),
            'next_step' => false,
            'phone' => formatPhone($client->phone),
            'client_type' => 'independent_client',
        ];

        $response = $this->signUpService->validateIndependentClient($client);

        $this->assertEquals($stub, $response);
    }

    public function testClientAlreadyRegisteredShouldThrowException()
    {
        $client = factory(Client::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'email_verified' => 1,
        ]);
        $this->expectException(MobileException::class);
        $this->expectExceptionMessage(__('Mobile/signUp.first_step.client_exists'));
        $this->signUpService->getClientInstance(self::SOUK_CNPJ);
    }

    public function testClientValidationShouldMatchStub()
    {
        $client = factory(Client::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'email_verified' => 0,
            'confirm_sms' => null,
        ]);

        $stub = [
            'status' => true,
            'message' => false,
            'next_step' => $this->signUpService::STEP_PHONE_VALIDATION,
            'phone' => formatPhone($client->phone),
            'client_type' => 'client',
        ];

        $this->assertEquals($stub, $this->signUpService->validateClient($client));
    }

    public function testSendPhoneConfirmationShouldSendNotificationToIndependentClient()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $client = factory(IndependentClient::class)->state('newRegistration')->create([
            'cnpj' => self::SOUK_CNPJ,
        ]);

        $this->signUpService->sendPhoneConfirmationCode(self::SOUK_CNPJ, self::DUMMY_PHONE);

        $clientConfirmation = $client->confirmations()->latest()->first();
        Notification::assertSentTo($clientConfirmation, MobileConfirmationNotification::class);
        $this->assertNotNull($clientConfirmation->independentClient);
        $this->assertNull($clientConfirmation->client);
    }

    public function testSendPhoneConfirmationShouldSendNotificationToClient()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $client = factory(Client::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'email_verified' => 0,
            'confirm_sms' => null,
        ]);

        $this->signUpService->sendPhoneConfirmationCode(self::SOUK_CNPJ, self::DUMMY_PHONE);

        $clientConfirmation = $client->confirmations()->latest()->first();
        Notification::assertSentTo($clientConfirmation, MobileConfirmationNotification::class);
        $this->assertNotNull($clientConfirmation->client);
        $this->assertNull($clientConfirmation->independentClient);
    }

    public function testPhoneAlreadyConfirmedShouldThrowException()
    {
        Notification::fake();
        Notification::assertNothingSent();

        factory(Client::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'email_verified' => 0,
            'confirm_sms' => 10,
        ]);

        $this->expectException(MobileException::class);

        $this->signUpService->sendPhoneConfirmationCode(self::SOUK_CNPJ, self::DUMMY_PHONE);
        Notification::assertNothingSent();
    }

    public function testPersistClientPhoneShouldReturnTrue()
    {
        $client = factory(Client::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'email_verified' => 0,
            'confirm_sms' => 3,
        ]);

        $confirmations = factory(ClientConfirmation::class, 3)->create([
            'client_id' => $client->id,
            'type' => 'sms',
            'value' => self::DUMMY_PHONE,
        ]);

        $this->assertTrue(
            $this->signUpService
                ->persistClientPhone($client, $confirmations->first()->code)
        );
    }

    public function testPersistClientPhoneShouldDeleteConfirmations()
    {
        $client = factory(Client::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'email_verified' => 0,
            'confirm_sms' => 3,
        ]);

        $confirmations = factory(ClientConfirmation::class, 3)->create([
            'client_id' => $client->id,
            'type' => 'sms',
            'value' => self::DUMMY_PHONE,
        ]);

        $this->assertCount(3, $client->confirmations);

        $this->signUpService->persistClientPhone($client, $confirmations->first()->code);

        $this->assertCount(0, $client->fresh()->confirmations);
    }

    public function testPersistClientPhoneShouldUpdateClientInfo()
    {
        $client = factory(Client::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'email_verified' => 0,
            'confirm_sms' => 3,
        ]);

        $confirmation = factory(ClientConfirmation::class)->create([
            'client_id' => $client->id,
            'type' => 'sms',
            'value' => self::DUMMY_PHONE,
        ]);

        $this->signUpService->persistClientPhone($client, $confirmation->value);

        $this->assertEquals(self::DUMMY_PHONE, $client->fresh()->phone);
        $this->assertEquals(10, $client->fresh()->confirm_sms);
    }

    public function testPersistClientPhoneShouldUpdateIndependentInfo()
    {
        $client = factory(IndependentClient::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 3,
        ]);

        $confirmation = factory(ClientConfirmation::class)->create([
            'independent_client_id' => $client->id,
            'type' => 'sms',
            'value' => self::DUMMY_PHONE,
        ]);

        $this->signUpService->persistClientPhone($client, $confirmation->value);

        $this->assertEquals(self::DUMMY_PHONE, $client->fresh()->phone);
        $this->assertEquals(10, $client->fresh()->confirm_sms);
    }

    public function testValidatePhoneConfirmationCodeShouldUpdateConfirmationEntry()
    {
        $client = factory(IndependentClient::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 3,
        ]);

        $confirmation = factory(ClientConfirmation::class)->create([
            'independent_client_id' => $client->id,
            'type' => 'sms',
            'value' => self::DUMMY_PHONE,
            'confirmed_at' => null,
        ]);

        $this->signUpService->validatePhoneConfirmationCode($client, $confirmation->code);

        $this->assertNotNull($confirmation->fresh()->confirmed_at);
    }

    public function testValidatePhoneConfirmationCodeShouldThrowException()
    {
        $client = factory(IndependentClient::class)->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 3,
        ]);

        $this->expectException(MobileException::class);
        $this->expectExceptionMessage(__('Mobile/signUp.phone_validation.invalid_code'));

        $this->signUpService->validatePhoneConfirmationCode($client, 1111);
    }

    public function testConfirmClientPhoneShouldMatchStub()
    {
        $client = factory(IndependentClient::class)->state('newRegistration')->create([
            'cnpj' => self::SOUK_CNPJ,
            'confirm_sms' => 3,
        ]);

        factory(ClientConfirmation::class)->create([
            'independent_client_id' => $client->id,
            'type' => 'sms',
            'value' => self::DUMMY_PHONE,
            'confirmed_at' => null,
            'code' => 1111,
        ]);

        $response = $this->signUpService->confirmClientPhone(self::SOUK_CNPJ, '1111');

        $stub = [
            'status' => true,
            'message' => false,
            'next_step' => $this->signUpService::STEP_INDEPENDENT_SIGN_UP,
            'phone' => formatPhone($client->fresh()->phone),
            'client_type' => 'independent_client',
        ];

        $this->assertEquals($stub, $response);
    }
}
