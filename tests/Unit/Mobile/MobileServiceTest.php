<?php

namespace Tests\Unit\Mobile;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;

use App\Client\Client;
use App\Company\Company;
use App\ClientCompanies\ClientCompany;
use App\Bid\Bid;
use App\Order\Order;

use App\Mobile\Services\MobileService;
use App\Mobile\Exceptions\MobileException;

class MobileServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected $mobileService;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();

        $this->mobileService = new MobileService();
    }

    /**
     * test check token with inactive user and account_balance > min_order should return MobileException
     * and hasnt any buy (bid and order)
     * @return void
     **/
    public function testCheckTokenWithInactiveUserShouldReturnMobileException()
    {
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 101,
            'status' => 0
        ]);

        $this->expectException(MobileException::class);

        $checkToken = $this->mobileService->checkToken($client->id);
    }

    /**
     * test check token with one active company and other dont. Should return client object
     * and hasnt any buy (bid and order)
     * @return void
     **/
    public function testCheckTokenOneActiveCompanyAndOtherDontShouldReturnClientObject()
    {
        $client = factory(Client::class)->create();

        $clientCompany1 = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 200,
            'account_balance' => 100,
            'status' => 0
        ]);

        $clientCompany2 = factory(ClientCompany::class)->create([
            'client_id' => $client->id,
            'company_id' => factory(Company::class)->create(),
            'min_order' => 100,
            'account_balance' => 200,
        ]);

        $checkToken = $this->mobileService->checkToken($client->id);

        $this->assertArraySubset($client->toArray(),$checkToken->toArray());
    }

    /**
     * test check token with inactive user that has at least one bid should return MobileException
     * even with account_balance > min_order
     * @return void
     **/
    public function testCheckTokenWithUserThatHasOneBidShouldReturnClientObject()
    {
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 110,
            'status' => 0
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $client
        ]);

        $checkToken = $this->mobileService->checkToken($client->id);

        $this->assertArraySubset($client->toArray(),$checkToken->toArray());
    }

    /**
     * test check token with inactive user that has at least one bid and order should return MobileException
     * @return void
     **/
    public function testCheckTokenWithUserThatHasOneBidAndOrderShouldReturnClientObject()
    {
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 100,
            'status' => 0
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $client
        ]);

        $order = factory(Order::class)->create([
            'id_cliente' => $client
        ]);

        $checkToken = $this->mobileService->checkToken($client->id);

        $this->assertArraySubset($client->toArray(),$checkToken->toArray());
    }


    /**
     * test get authorized companies with user which can access just one company should return one company
     * @return void
     **/
    public function testGetAuthorizedCompaniesWithUserWhichCanAccessJustOneCompanyShouldReturnOneCompany()
    {
        $client = factory(Client::class)->create();

        $company2 = factory(Company::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 1000,
            'status' => 0
        ]);

        $bid = factory(Bid::class)->create([
            'client_id' => $client
        ]);

        $order = factory(Order::class)->create([
            'id_cliente' => $client
        ]);

        $invalidClientCompany = factory(ClientCompany::class)->create([
            'company_id' => $company2->id,
            'client_id' => $client->id,
            'min_order' => 100,
            'account_balance' => 1000,
            'status' => 0
        ]);

        $getAuthorizedClientCompanies = $this->mobileService->getAuthorizedClientCompanies($client);

        $this->assertArraySubset($client->companies->toArray(),$getAuthorizedClientCompanies->companies->toArray());
    }

    /**
     * test check token with inactive user that has at least one bid and order should return MobileException
     * @return void
     **/
    public function testCheckTokenWithActiveUserShouldReturnClientObject()
    {
        $client = factory(Client::class)->create();

        factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'status' => 1
        ]);

        $checkToken = $this->mobileService->checkToken($client->id);

        $this->assertArraySubset($client->toArray(),$checkToken->toArray());
    }
}
