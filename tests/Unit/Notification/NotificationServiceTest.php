<?php

namespace Tests\Unit\Notification;

use App\Bid\Bid;
use App\Core\Entities\RejectionMotive;
use App\Core\Entities\Status;
use App\Fefo\Fefo;
use App\Notification\Notification;
use App\Notification\Services\NotificationService;
use App\Order\Order;
use App\Order\OrderProduct;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class NotificationServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Tests reject bid notification in notification service
     *
     * @return void
     */
    public function testRejectBidNotification()
    {
        $reject = (new NotificationService())->rejectedBid($this->user->id, 1, 'product name', 10.50);

        $notification = DB::table('notifications')->first();

        $this->assertEquals(__('rejectionMotive.bid.price_rejected.push.title', ['bid' => 1]), $notification->title);
        $this->assertEquals(
            __('rejectionMotive.bid.price_rejected.push.message',
                [
                    'product' => 'product name',
                    'price' => price_br(10.50)
                ]
            ),
            $notification->message
        );

        $this->assertEquals($notification->type, Notification::rejectedBid);
    }

    public function testCreateNotificationForRejectedBid(): void
    {
        $bid = factory(Bid::class)->create([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id
        ]);

        $notification = (new NotificationService)
            ->rejectedBid($bid->client_id, $bid->id, $bid->product->name, $bid->kg_price);

        // Assert title
        $this->assertEquals(
            __('rejectionMotive.bid.price_rejected.push.title', ['bid' => $bid->id]),
            $notification->title
        );

        // Assert message
        $this->assertEquals(
            __(
                'rejectionMotive.bid.price_rejected.push.message',
                ['product' => $bid->product->name, 'price' => price_br($bid->kg_price)]
            ),
            $notification->message
        );

        // Assert attributes
        $this->assertEquals(
            json_encode(['route' => 'VisualizarLancePage', 'resource' => $bid->id]),
            $notification->attributes
        );
    }

    public function testCreateNotificationForExpiredBid()
    {
        $bid = factory(Bid::class)->create([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::EXPIRED)->id
        ]);
        $expiredAt = now()->subDay()->format('d/m/y');
        $notification = (new NotificationService)
            ->expiredBid($bid->id, $bid->client_id, $bid->product->name, $expiredAt);

        // Assert title
        $this->assertEquals(
            __('rejectionMotive.bid.expired.push.title', ['bid' => $bid->id]),
            $notification->title
        );

        // Assert message
        $this->assertEquals(
            __(
                'rejectionMotive.bid.expired.push.message',
                ['product' => $bid->product->name, 'expired_at' => $expiredAt]
            ),
            $notification->message
        );

        // Assert attributes
        $this->assertEquals(
            json_encode(['route' => 'VisualizarLancePage', 'resource' => $bid->id]),
            $notification->attributes
        );
    }

    public function testCreateNotificationForCanceledBid()
    {
        $bid = factory(Bid::class)->create([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::PRICE_REJECTED)->id
        ]);

        $notification = (new NotificationService)
            ->canceledBid($bid->client_id, $bid->id, $bid->product->name, $bid->rejectionMotives);

        // Assert title
        $this->assertEquals(
            __("rejectionMotive.bid.{$bid->rejectionMotives->name}.push.title", ['bid' => $bid->id]),
            $notification->title
        );

        // Assert message
        $this->assertEquals(
            __(
                "rejectionMotive.bid.{$bid->rejectionMotives->name}.push.message",
                ['product' => $bid->product->name]
            ),
            $notification->message
        );

        // Assert attributes
        $this->assertEquals(
            json_encode(['route' => 'VisualizarLancePage', 'resource' => $bid->id]),
            $notification->attributes
        );
    }

    public function testCreateNotificationForFinishedBid()
    {
        $today = now()->toDateString();
        $bid = factory(Bid::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'date' => $today,
        ]);

        $notification = (new NotificationService)
            ->finishedBid($bid->client_id, $bid->id, $bid->product->name, $today);

        // Assert title
        $this->assertEquals(
            __('status.order_bid.approved.push.title', ['bid' => $bid->id]),
            $notification->title
        );

        // Assert message
        $this->assertEquals(
            __(
                'status.order_bid.approved.push.message',
                ['product' => $bid->product->name, 'date' => $today]
            ),
            $notification->message
        );

        // Assert attributes
        $this->assertEquals(
            json_encode(['route' => 'VisualizarLancePage', 'resource' => $bid->id]),
            $notification->attributes
        );
    }

    public function testCreateNotificationForApprovedBid()
    {
        $bid = factory(Bid::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'date' => now()->toDateString(),
        ]);

        $fefo = factory(Fefo::class)->create([
            'cod_origem' => $bid->origin_code,
            'codigo_produto' => $bid->product_code,
            'semanas' => $bid->weeks,
        ]);

        $discount = calcDiscount($fefo->max_price, $bid->kg_price);

        $notification = (new NotificationService)
            ->approvedBid($bid->client_id, $bid->id, $bid->product->name, $bid->kg_price, $discount);

        // Assert title
        $this->assertEquals(
            __('status.order_bid.pending.push.title', ['bid' => $bid->id]),
            $notification->title
        );

        // Assert message
        $this->assertEquals(
            __(
                'status.order_bid.pending.push.message',
                [
                    'product' => $bid->product->name,
                    'price' => price_br($bid->kg_price),
                    'discount' => $discount,
                ]
            ),
            $notification->message
        );

        // Assert attributes
        $this->assertEquals(
            json_encode(['route' => 'VisualizarLancePage', 'resource' => $bid->id]),
            $notification->attributes
        );
    }

    public function testCreateNotificationForAllApprovedOrder()
    {
        $order = factory(Order::class)->create();

        $notification = (new NotificationService)->allApprovedOrder($order->client->id, $order->id);

        // Assert title
        $this->assertEquals(
            __('status.order.approved.push.title', ['order_id' => $order->id]),
            $notification->title
        );

        // Assert message
        $this->assertEquals(__('status.order.approved.push.message'), $notification->message);

        // Assert attributes
        $this->assertEquals(
            json_encode(['route' => 'PedidoPage', 'resource' => $order->id]),
            $notification->attributes
        );
    }

    public function testCreateNotificationForPartialApprovedOrder()
    {
        $order = factory(Order::class)->create();

        $notification = (new NotificationService)->partiallyBilledOrder($order->client->id, $order->id);

        // Assert title
        $this->assertEquals(
            __('status.order.partially_billed.push.title', ['order_id' => $order->id]),
            $notification->title
        );

        // Assert message
        $this->assertEquals(__('status.order.partially_billed.push.message'), $notification->message);

        // Assert attributes
        $this->assertEquals(
            json_encode(['route' => 'PedidoPage', 'resource' => $order->id]),
            $notification->attributes
        );
    }

    public function testCreateNotificationForAllCanceledOrder()
    {
        $order = factory(Order::class)->create();

        $notification = (new NotificationService)->allCanceledOrder($order->client->id, $order->id);

        // Assert title
        $this->assertEquals(
            __('rejectionMotive.order.canceled.push.title', ['order_id' => $order->id]),
            $notification->title
        );

        // Assert message
        $this->assertEquals(__('rejectionMotive.order.canceled.push.message'), $notification->message);

        // Assert attributes
        $this->assertEquals(
            json_encode(['route' => 'PedidoPage', 'resource' => $order->id]),
            $notification->attributes
        );
    }

    public function testCreateNotificationforPartiallyBilledBid()
    {
        $order = factory(Order::class)->create([
            'bid_id' => function () {
                return factory(Bid::class)->create()->id;
            }
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
            'original_box_amount' => 10,
            'box_amount' => 9,
        ]);

        $notification = (new NotificationService)
            ->partiallyBilledBid(
                $order->client->id,
                $order->bid->id,
                $orderProduct->product->name,
                $order->delivery_date->format('d/m/Y')
            );

        $this->assertEquals(
            __('status.order_bid.partially_billed.push.title', ['bid' => $order->bid->id]),
            $notification->title
        );

        $this->assertEquals(
            __(
                'status.order_bid.partially_billed.push.message',
                ['product' => $orderProduct->product->name, 'date' => $order->delivery_date->format('d/m/Y')]
            ),
            $notification->message
        );
    }

    public function testCreateNotificationforBilledAboveBid()
    {
        $order = factory(Order::class)->create([
            'bid_id' => function () {
                return factory(Bid::class)->create()->id;
            }
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
            'original_box_amount' => 9,
            'box_amount' => 10,
        ]);

        $notification = (new NotificationService)
            ->billedAboveBid(
                $order->client->id,
                $order->bid->id,
                $orderProduct->product->name,
                $order->delivery_date->format('d/m/Y')
            );

        $this->assertEquals(
            __('status.order_bid.billed_above.push.title', ['bid' => $order->bid->id]),
            $notification->title
        );

        $this->assertEquals(
            __(
                'status.order_bid.billed_above.push.message',
                ['product' => $orderProduct->product->name, 'date' => $order->delivery_date->format('d/m/Y')]
            ),
            $notification->message
        );
    }
}
