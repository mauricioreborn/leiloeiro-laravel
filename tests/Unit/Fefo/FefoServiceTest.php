<?php

namespace Tests\Unit\Fefo;

use App\Company\Company;
use App\Company\Services\CompanySettingService;
use App\Fefo\Fefo;
use App\Fefo\Services\FefoService;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;

class FefoServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
    }

    /**
     * Method to set test total weeks on fefo stats
     *
     * @return null
     */
    public function testTotalWeeksOnFefoStatsMethod()
    {
        $faker = \Faker\Factory::create();
        factory(Fefo::class,50)->create(
            [
                'volume' => $faker->randomFloat(2, 1, 10),
            ]
        );
        $fefoService = new FefoService();
        $data = $fefoService->stats($this->company->id);
        $totalSemanas = 0;
        foreach ($data['tabela_semanas'] as $statsData) {
            $totalSemanas += $statsData;
        }

        $totalSemanas += $data['restante_semanas'];

        $this->assertEquals(100, $totalSemanas);

    }

    /**
     * Method to test totalValues on fefo stats
     *
     * @return null
     */
    public function testTotalValueOnFefoStatsMethod()
    {
        $faker = \Faker\Factory::create();
        factory(Fefo::class,50)->create(
            [
                'volume' => $faker->randomFloat(2, 1, 10),
            ]
        );
        $fefoService = new FefoService();
        $data = $fefoService->stats($this->company->id);
        $total = 0;
        foreach ($data["tabela_familias"] as $statsData) {
            $total += $statsData;
        }
        $data['total'] = intval($data['total']);
        $total = intval ($total);
        if ($data['total'] > $total) {
            $this->assertGreaterThan($data['total'], $total + 2);
        } elseif ($data['total'] < $total) {
            $this->assertLessThan($data['total'], $total - 2);
        } else {
            $this->assertEquals($data['total'], $total);
        }
    }

    /**
     * Method to test totalFromOrigens on fefo stats
     *
     * @return null
     */
    public function testTotalFromOrigensOnFefoStatsMethod()
    {
        $faker = \Faker\Factory::create();
        factory(Fefo::class,50)->create(
            [
                'volume' => $faker->randomFloat(2, 1, 10),
            ]
        );
        $fefoService = new FefoService();
        $data = $fefoService->stats($this->company->id);
        $total = 0;
        foreach ($data["tabela_origens"] as $statsData) {
            $total += $statsData;
        }

        $total += $data["restante_origens"];

        $data['total'] = intval($data['total']);
        $total = intval ($total);

        if ($data['total'] > $total) {
            $this->assertGreaterThan($data['total'], ($total + 2.2));
        } elseif ($data['total'] < $total) {
            $this->assertLessThan($data['total'], ($total - 2.75));
        } else {
            $this->assertEquals($data['total'], $total);
        }
    }

    /**
     * @return void
     */
    public function testReplicateFefoWithCompanySetting()
    {
        $totalReplicate = 10;
        $totalReplicateWithHighlightApp = 5;
        $totalNotReplicate = 20;
        $cdEmpresa = 6;
        $cdFaixaFefo = "500003";
        $faixaFefo= "003";

        (new CompanySettingService())->createFefoReplicate($this->company, 1);

        $companyWithoutSettingReplicate = factory(Company::class)->create();

        factory(Fefo::class, $totalReplicate)->create([
            'date' => today()->subDay(1)->toDateString(),
            'company_id' => $this->company->id,
            'cd_empresa' => $cdEmpresa,
            'cd_faixa_fefo' => $cdFaixaFefo,
            'faixa_fefo' => $faixaFefo,
            'highlight_app' => 0,
        ]);

        factory(Fefo::class, $totalReplicateWithHighlightApp)->create([
            'date' => today()->subDay(1)->toDateString(),
            'company_id' => $this->company->id,
            'cd_empresa' => $cdEmpresa,
            'cd_faixa_fefo' => $cdFaixaFefo,
            'faixa_fefo' => $faixaFefo,
            'highlight_app' => 1,
        ]);

        factory(Fefo::class, $totalNotReplicate)->create([
            'date' => today()->subDay(1)->toDateString(),
            'company_id' => $companyWithoutSettingReplicate->id
        ]);

        (new FefoService())->replicate();

        $fefo = new Fefo();

        $fefoReplicate = $fefo->scopeToday()->fromCompany($this->company->id)->get();
        $fefoDoNotReplicate = $fefo->scopeYesterday()->fromCompany($companyWithoutSettingReplicate->id)->get();
        $highlightApp = $fefoReplicate->where('highlight_app', 1);

        $this->assertEquals($fefoReplicate->toArray()[2]['cd_empresa'], $cdEmpresa);
        $this->assertEquals($fefoReplicate->toArray()[2]['cd_faixa_fefo'], $cdFaixaFefo);
        $this->assertEquals($fefoReplicate->toArray()[2]['faixa_fefo'], $faixaFefo);
        $this->assertCount(($totalReplicate + $totalReplicateWithHighlightApp), $fefoReplicate);
        $this->assertCount($totalReplicateWithHighlightApp, $highlightApp);
        $this->assertCount($totalNotReplicate, $fefoDoNotReplicate);
        $this->assertCount(0, $fefo->scopeToday()->fromCompany($companyWithoutSettingReplicate->id)
            ->get());
    }

    /**
     * @return void
     */
    public function testDoNotReplicateFefocdFaixaFefoZero()
    {
        $totalReplicate = 10;
        $cdEmpresa = 0;
        $cdFaixaFefo = "0";
        $faixaFefo= "600001";

        (new CompanySettingService())->createFefoReplicate($this->company, 1);

        factory(Fefo::class, $totalReplicate)->create([
            'date' => today()->subDay(1)->toDateString(),
            'company_id' => $this->company->id,
            'cd_empresa' => $cdEmpresa,
            'cd_faixa_fefo' => $cdFaixaFefo,
            'faixa_fefo' => $faixaFefo,
        ]);

        (new FefoService())->replicate();

        $fefo = new Fefo();

        $this->assertCount(0, $fefo->scopeToday()->fromCompany($this->company->id)->get());
        $this->assertCount($totalReplicate, $fefo->scopeYesterday()->fromCompany($this->company->id)->get());
    }
    /**
     * @return void
     */
    public function testDoNotReplicateChangedAdminPrice()
    {

        (new CompanySettingService())->createFefoReplicate($this->company, 1);

        factory(Fefo::class)->create([
            'date' => today()->subDay(1)->toDateString(),
            'company_id' => $this->company->id,
            'changed_admin_price' => 1,
        ]);

        (new FefoService())->replicate();

        $this->assertEquals(0, Fefo::today()->first()->changed_admin_price);
    }

}
