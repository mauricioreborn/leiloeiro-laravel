<?php

namespace Tests\Unit\Fefo;

use App\Company\Company;
use App\Fefo\FefoPrice;
use App\Fefo\Services\FefoPriceService;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FefoPriceServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
    }

    /**
     * Update fefo price existing in company
     *
     * @return null
     */
    public function testFefoPriceUpdate()
    {
        $fefoPrice = factory(FefoPrice::class)->create([
            'company_id' => $this->company->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.51,
            'expired_at' => now()->addDays(2)->toDateString(),
        ]);

        $service = (new FefoPriceService())->create($this->company, 2, 697, 112233, 8.52, now()->toDateString());

        $this->assertCount(1, FefoPrice::withTrashed()->get());

        $this->assertDatabaseHas('fefo_prices', [
            'company_id' => $this->company->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.52,
            'expired_at' => now()->toDateString(),
        ]);
    }

    /**
     * Update fefo price with date nullable, will delete registry
     *
     * @return null
     */
    public function testFefoPriceUpdateWithDateNullable()
    {
        $fefoPrice = factory(FefoPrice::class)->create([
            'company_id' => $this->company->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.51,
            'expired_at' => now()->addDays(2)->toDateString(),
        ]);

        $service = (new FefoPriceService())->create($this->company, 2, 697, 112233, 8.52);

        $this->assertCount(1, FefoPrice::onlyTrashed()->get());

        $this->assertCount(0, FefoPrice::get());

        $this->assertDatabaseHas('fefo_prices', [
            'company_id' => $this->company->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 8.51,
            'expired_at' => now()->addDays(2)->toDateString(),
        ]);
    }

    /**
     * Create fefo price with date nullable, will not create any registry
     *
     * @return null
     */
    public function testFefoPriceCreateWithDateNullable()
    {
        $service = (new FefoPriceService())->create($this->company, 2, 697, 112233, 8.52);

        $this->assertCount(0, FefoPrice::withTrashed()->get());
    }

    /**
     * Create fefo price
     *
     * @return null
     */
    public function testFefoPriceCreate()
    {
        $service = (new FefoPriceService())->create($this->company, 2, 697, 112233, 6.52, now()->toDateString());

        $this->assertCount(1, FefoPrice::withTrashed()->get());

        $this->assertDatabaseHas('fefo_prices', [
            'company_id' => $this->company->id,
            'weeks' => 2,
            'leadtime_code' => 697,
            'product_code' => 112233,
            'selling_price' => 6.52,
            'expired_at' => now()->toDateString(),
        ]);
    }
}
