<?php

namespace Tests\Unit\Policies;

use App\Company\Company;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Fefo\Http\Policies\FefoCompanyPolicy;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Fefo\Fefo;
use App\Products\Product;

class FefoCompanyPolicyTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup
     * @return void
     **/
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedJwt();
    }

    /**
     * Test userAccess method
     * @return void
     **/
    public function testUserAccessShouldReturnTrue()
    {
        $companyPolicy = new FefoCompanyPolicy();
        $fefo = factory(Fefo::class)->create([
            'product_code' => function () {
                return factory(Product::class)->create([
                    'company_id' => $this->company->id,
                ])->code;
            },
        ]);

        $response = (bool) $companyPolicy->userAccess($this->user, $fefo);
        $this->assertTrue($response);
    }

    /**
     * Test userAccess method
     * @return void
     **/
    public function testUserAccessShouldReturnFalse()
    {
        $companyPolicy = new FefoCompanyPolicy();

        $company2 = factory(Company::class)->create();

        $product = factory(Product::class)->create([
            'company_id' => $company2,
        ]);

        $fefo = factory(Fefo::class)->create([
            'product_code' => $product->code,
            'company_id' => $company2->id,
        ]);

        $response = (bool) $companyPolicy->userAccess($this->user, $fefo);
        $this->assertFalse($response);
    }
}
