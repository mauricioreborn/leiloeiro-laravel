<?php

namespace Tests\Unit\StockWorkers;

use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Company\CompanySetting;
use App\Core\Entities\Status;
use App\DistributionCenter\DistributionCenter;
use App\Fefo\Fefo;
use App\FileQueue\FileQueue;
use App\Jobs\ProcessStockJob;
use App\LeadTime\LeadTime;
use App\ProcessedFefo\ProcessedFefo;
use App\Products\Product;
use App\Products\ProductPrice;
use App\RangePrice\RangePrice;
use App\Stock\Services\StockService;
use App\Stock\Stock;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class StockTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testEmptyListShouldNotCreateStock()
    {
        $this->assertEquals(0, RangePrice::count());
        $this->assertEquals(0, ProcessedFefo::count());
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testInsertStockShouldCreateRangePriceAndProcessedFefo()
    {
        $stock = $this->buildStockData();

        (new StockService)->processFefo($stock, today());

        $this->assertEquals(1, RangePrice::count());
        $this->assertEquals(1, ProcessedFefo::count());
    }

    /**
     * @return void
     */
    public function testReplicateStockShouldCreateRangePriceAndProcessedFefo()
    {
        Carbon::setTestNow(now()->subDay(1));

        $stockService = new StockService();
        $stockFaixa1 = $this->buildStockData();
        $stockService->processFefo($stockFaixa1, today());
        $stockFaixa2 = $this->buildStockData(12);
        $stockService->processFefo($stockFaixa2, today());

        Carbon::setTestNow();

        $stockService->replicate();

        Queue::assertPushed(ProcessStockJob::class, function ($job) use ($stockFaixa1) {
            return $stockFaixa1->id === $job->stockId;
        });
    }

    /**
     * @param int $days
     * @return Stock
     */
    public function buildStockData($days = 30)
    {
        Queue::fake();

        $client  = Client::first();
        $fileQueue = factory(FileQueue::class)->create(['company_id' => $this->company->id]);

        $this->company->companySettings()->where('setting', 'hasStockReplicate')->update(['value' => 1]);

        $distributionCenter = factory(DistributionCenter::class)->create(['company_id' => $this->company->id]);

        $product = factory(Product::class)->create(['company_id' => $this->company->id]);

        factory(ProductPrice::class)->create([
            'product_id' => $product->id,
            'state' => $distributionCenter->state,
            'price' => 10,
        ]);

        factory(LeadTime::class)->create(['cnpj' => $client->cnpj, 'company_id' => $this->company->id]);

        return factory(Stock::class)->create([
            'company_id' => $this->company->id,
            'product_id' => $product->id,
            'origin_code' => $distributionCenter->code,
            'status_id' => Status::type(Status::ACTIVE)->id,
            'file_queue_id' => $fileQueue->id,
            'expired_at' => now()->addDays($days)->toDateTimeString(),
            'volume' => 10000,
        ]);
    }
}
