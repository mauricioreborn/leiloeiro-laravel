<?php
namespace Tests\Unit\Campaign;

use App\Campaign\Services\CampaignHomeService;
use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\Jobs\Campaign\ProductsByTypologyJob;
use App\LeadTime\LeadTime;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Products\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;
use Mockery;

class HomeCampaignTest extends TestCase
{
    use DatabaseTransactions;


    /**
     * Method to test the creation of cache from typology product
     * @return void
     */
    public function testCreateCacheFromTypologyProductCampaign()
    {
        $company = Company::first();
        $client  = Client::first();

        $clientCompany = ClientCompany::where('company_id', $company->id)
            ->where('client_id', $client->id)
            ->first();

        $clientCompany->update(['typology' => 'idk', 'typology_code' => 999]);

        $order = factory(Order::class)->create([
            'client_id' => $client->id,
            'company_id' => $company->id
        ]);
        $leadtime = factory(LeadTime::class)->create([
            'cnpj' => $client->cnpj,
            'code' => 32244000,
            'company_id' => $company->id,
        ]);

        $product = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'origin_code' => $leadtime->code,
        ]);

        (new CampaignHomeService())->getTopProductsByOrigin(
            $company->id,
            $clientCompany->typology_code,
            $clientCompany->typology,
            [$leadtime->code]
        );

        $this->assertEquals(
            Cache::get("campaign:home:typology:{$company->id}:{$leadtime->code}.{$clientCompany->typology}"),
            [
                $product->product_id
            ]
        );

        Cache::forget("campaign.home.{$company->id}.{$leadtime->code}.{$clientCompany->typology}");
    }


    public function testCreateSuggestedCampaignFromSpecificallyClient()
    {
        $this->setUpMockedJwtClient();

        $clientCompany = ClientCompany::where('company_id', $this->company->id)
            ->where('client_id', $this->client->id)
            ->first();

        $clientCompany->update(['typology' => 'idk', 'account_balance' => 500, 'status' => 1]);

        $leadTime = factory(LeadTime::class)->states('canPurchase')->create([
            'cnpj' => $this->client->cnpj,
            'company_id' => $this->company->id,
        ]);

        $order = factory(Order::class)->create([
            'client_id' => $this->client->id,
            'company_id' => $this->company->id,
            'date' => now()->subDays(30)->format('Y-m-d'),
            'rejection_motives_id' => null,
        ]);

        $product = factory(Product::class)->create([
            'company_id' => $this->company->id
        ]);

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'product_id' => $product->code,
            'origin_code' => $leadTime->code,
        ]);

        $campaignHomeService = new CampaignHomeService();
        $campaignHomeService->createCacheToSuggestedCampaign();

        $hash = "campaign:home:suggested:{$this->company->id}:{$this->client->id}";

        $this->assertEquals(
            [$product->code],
            Cache::get($hash)
        );

        Cache::forget($hash);
    }
}
