<?php

namespace Tests\Feature\Iugu;

use App\Client\ClientCard;
use App\Client\ClientCompanyPayment;
use App\Company\CompanyPayment;
use App\Core\Entities\Status;
use App\Core\Entities\RejectionMotive;
use App\Invoice\Invoice;
use App\Invoice\Services\InvoiceService;
use App\Iugu\Services\IuguService;
use App\Jobs\CancelInvoiceJob;
use App\Jobs\IntegrateInvoiceJob;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Payment\Payment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Mockery;
use Tests\Mockup\IuguInvoice;
use Tests\TestCase;

class InvoiceServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to setUp configuration
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedClient();
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testSyncInvoiceCanceledWithOrderOpen()
    {
        $this->markTestSkipped('Identificar quando reserva expirar e reservar novamente, deve implementar no sync');

        Queue::fake();

        $iuguService = resolve(IuguService::class);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $invoice = factory(Invoice::class)->states('pending')->create([
            'order_id' => $order->id
        ]);

        $showInvoice = factory(IuguInvoice::class)->states('canceled')->make([
            'order_id' => $invoice->id
        ])->toArray();

        $iuguService->shouldReceive('showInvoice')
            ->withAnyArgs()
            ->andReturn($showInvoice);

        (new InvoiceService())->syncInvoice($invoice->id);

        $invoice = $invoice->fresh();

        $canceled = Status::type(Status::CANCELED);

        $this->assertEquals($invoice->status_id, $canceled->id);
        $this->assertEquals(2, Invoice::count());

        $newInvoice = $order->invoices()->where('id', '!=', $invoice->id)->first();

        Queue::assertPushed(IntegrateInvoiceJob::class, function ($job) use ($newInvoice) {
            return $job->getInvoiceId() == $newInvoice->id;
        });
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testSyncInvoiceCanceledWithOrderCanceled()
    {
        Queue::fake();

        $iuguService = resolve(IuguService::class);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id,
            'client_id' => $this->client->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::CANCELED)->id,
            'rejection_motives_id' => RejectionMotive::type(RejectionMotive::INVALID_FEFO)->id
        ]);

        $invoice = factory(Invoice::class)->states('pending')->create([
            'order_id' => $order->id
        ]);

        $showInvoice = factory(IuguInvoice::class)->states('canceled')->make([
            'order_id' => $invoice->id
        ])->toArray();

        $iuguService->shouldReceive('showInvoice')
            ->withAnyArgs()
            ->andReturn($showInvoice);

        (new InvoiceService())->syncInvoice($invoice->id);

        $invoice = $invoice->fresh();

        $canceled = Status::type(Status::CANCELED);

        $this->assertEquals($invoice->status_id, $canceled->id);
        $this->assertEquals(1, Invoice::count());

        Queue::assertNotPushed(IntegrateInvoiceJob::class);
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testIntegrateOrder()
    {
        $iuguService = resolve(IuguService::class);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'tax_percent' => 2.51,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $invoice = factory(Invoice::class)->states('pending')->create([
            'order_id' => $order->id,
            'client_card_id' => $clientCard->id
        ]);

        $iuguService->shouldReceive('createCharge')
            ->withAnyArgs()
            ->andReturn([
                'message' => 'Autorizado',
                'errors' => [],
                'success' => true,
                'url' => 'https://faturas.iugu.com/019361df-247f-40d8-a97f-70d655bfe12d-e4e5',
                'pdf' => 'https://faturas.iugu.com/019361df-247f-40d8-a97f-70d655bfe12d-e4e5.pdf',
                'identification' => null,
                'invoice_id' => '123',
                'LR' => '00',
            ]);

        (new InvoiceService())->integrate($invoice);

        $invoice = $invoice->fresh();
        $order = $order->fresh();

        $paid = Status::type(Status::PAID);
        $pending = Status::type(Status::PENDING);

        $this->assertEquals($invoice->status_id, $pending->id);
        $this->assertEquals($invoice->invoice_id, 123);

        $this->assertEquals($order->payment_status_id, $paid->id);

        $this->assertEquals(1, Invoice::count());
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testIntegrateAuthorizationInvoice()
    {
        Queue::fake();

        $iuguService = resolve(IuguService::class);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('pending')->create([
            'client_id' => $this->client->id,
        ]);

        $invoice = factory(Invoice::class)->states(['authorization'])->create([
            'client_card_id' => $clientCard->id
        ]);

        $iuguService->shouldReceive('createCharge')
            ->withAnyArgs()
            ->andReturn([
                'message' => 'Autorizado',
                'errors' => [],
                'success' => true,
                'url' => 'https://faturas.iugu.com/019361df-247f-40d8-a97f-70d655bfe12d-e4e5',
                'pdf' => 'https://faturas.iugu.com/019361df-247f-40d8-a97f-70d655bfe12d-e4e5.pdf',
                'identification' => null,
                'invoice_id' => '123',
                'LR' => '00',
            ]);

        (new InvoiceService())->integrate($invoice);

        $invoice = $invoice->fresh();
        $clientCard = $clientCard->fresh();

        $authorized = Status::type(Status::AUTHORIZED);

        $this->assertEquals($invoice->invoice_id, 123);

        $this->assertEquals($clientCard->status_id, $authorized->id);

        Queue::assertPushed(CancelInvoiceJob::class, function ($job) use ($invoice) {
            return $job->getInvoiceId() == $invoice->id;
        });
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testRejectIntegrateOrder()
    {
        $iuguService = resolve(IuguService::class);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING),
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'tax_percent' => 2.51,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $invoice = factory(Invoice::class)->states('pending')->create([
            'order_id' => $order->id,
            'client_card_id' => $clientCard->id
        ]);

        $iuguService->shouldReceive('createCharge')
            ->withAnyArgs()
            ->andReturn([
                'message' => 'Transação negada',
                'errors' => [],
                'success' => false,
                'url' => 'https://faturas.iugu.com/019361df-247f-40d8-a97f-70d655bfe12d-e4e5',
                'pdf' => 'https://faturas.iugu.com/019361df-247f-40d8-a97f-70d655bfe12d-e4e5.pdf',
                'identification' => null,
                'invoice_id' => '123',
                'LR' => '00',
            ]);

        (new InvoiceService())->integrate($invoice);

        $invoice = $invoice->fresh();
        $order = $order->fresh();

        $canceled = Status::type(Status::CANCELED);
        $rejected = Status::type(Status::REJECTED);

        $this->assertEquals($invoice->invoice_id, 123);

        $this->assertEquals($order->status_id, Status::type(Status::CANCELED)->id);

        $this->assertEquals($order->payment_status_id, $rejected->id);

        $this->assertEquals(1, Invoice::count());
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testRejectAuthorizationInvoice()
    {
        Queue::fake();

        $iuguService = resolve(IuguService::class);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('pending')->create([
            'client_id' => $this->client->id,
        ]);

        $invoice = factory(Invoice::class)->states(['authorization'])->create([
            'client_card_id' => $clientCard->id
        ]);

        $iuguService->shouldReceive('createCharge')
            ->withAnyArgs()
            ->andReturn([
                'message' => 'Transação negada',
                'errors' => [],
                'success' => false,
                'url' => 'https://faturas.iugu.com/019361df-247f-40d8-a97f-70d655bfe12d-e4e5',
                'pdf' => 'https://faturas.iugu.com/019361df-247f-40d8-a97f-70d655bfe12d-e4e5.pdf',
                'identification' => null,
                'invoice_id' => '123',
                'LR' => '00',
            ]);

        (new InvoiceService())->integrate($invoice);

        $invoice = $invoice->fresh();
        $clientCard = $clientCard->fresh();

        $rejected = Status::type(Status::REJECTED);
        $canceled = Status::type(Status::CANCELED);

        $this->assertEquals($invoice->invoice_id, 123);
        $this->assertEquals($clientCard->status_id, $rejected->id);
    }

    /**
     * Method to test companyPayment with success
     *
     * @param string $invoiceStatus Invoice status
     *
     * @dataProvider invoiceStatus
     *
     * @return void
     */
    public function testSyncInvoiceStatusChanged($invoiceStatus)
    {
        Queue::fake();

        $iuguService = resolve(IuguService::class);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $invoice = factory(Invoice::class)->states('pending')->create([
            'order_id' => $order->id
        ]);

        $showInvoice = factory(IuguInvoice::class)->states($invoiceStatus)->make([
            'order_id' => $invoice->id
        ])->toArray();

        $iuguService->shouldReceive('showInvoice')
            ->withAnyArgs()
            ->andReturn($showInvoice);

        (new InvoiceService())->syncInvoice($invoice->id);

        $invoice = $invoice->fresh();

        $status = Status::type($invoiceStatus);

        $this->assertEquals($invoice->status_id, $status->id);

        $totalPaid = round(($showInvoice['total_paid_cents'] / 100), 2);

        $this->assertEquals($invoice->paid, $totalPaid);

        $this->assertEquals($invoice->canceled_at, rfc3339ExtentedToDateTime($showInvoice['canceled_at_iso']));
        $this->assertEquals($invoice->paid_at, rfc3339ExtentedToDateTime($showInvoice['paid_at']));
        $this->assertEquals($invoice->expired_at, rfc3339ExtentedToDateTime($showInvoice['expired_at_iso']));
        $this->assertEquals($invoice->authorized_at, rfc3339ExtentedToDateTime($showInvoice['authorized_at_iso']));
        $this->assertEquals($invoice->refunded_at, rfc3339ExtentedToDateTime($showInvoice['refunded_at_iso']));
        $this->assertEquals($invoice->protested_at, rfc3339ExtentedToDateTime($showInvoice['protested_at_iso']));
        $this->assertEquals($invoice->chargeback_at, rfc3339ExtentedToDateTime($showInvoice['chargeback_at_iso']));
    }

    /**
     * Method to return invoice events
     *
     * @return array
     */
    public function invoiceStatus()
    {
        return [
            [ Status::PENDING ],
            [ Status::IN_ANALYSIS ],
            [ Status::PAID ],
            [ Status::CANCELED ],
            [ Status::PARTIALLY_PAID ],
            [ Status::EXPIRED ],
            [ Status::AUTHORIZED ],
            [ Status::REFUNDED ],
            [ Status::IN_PROTEST ],
            [ Status::CHARGEBACK ],
        ];
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testRefundedInvoice()
    {
        Queue::fake();

        $iuguService = resolve(IuguService::class);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::APPROVED)->id,
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'tax_percent' => 2.51,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::APPROVED)->id,
        ]);

        $invoice = factory(Invoice::class)->states('paid')->create([
            'order_id' => $order->id,
            'client_card_id' => $clientCard->id
        ]);

        $showInvoice = factory(IuguInvoice::class)->states(Status::REFUNDED)->make([
            'order_id' => $invoice->id
        ])->toArray();

        $iuguService->shouldReceive('refundCharge')
            ->withAnyArgs()
            ->andReturn($showInvoice);

        (new InvoiceService())->cancel($invoice);

        $invoice = $invoice->fresh();

        $refunded = Status::type(Status::REFUNDED);

        $this->assertEquals($invoice->status_id, $refunded->id);
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testCancelInvoice()
    {
        Queue::fake();

        $iuguService = resolve(IuguService::class);

        $payment = Payment::where('type', Payment::CREDIT_CARD)->first();

        $companyPayment = factory(CompanyPayment::class)->create([
            'payment_id' => $payment->id
        ]);

        $clientCompanyPayment = factory(ClientCompanyPayment::class)->create([
            'company_payment_id' => $companyPayment->id,
            'client_id' => $this->client->id
        ]);

        $clientCard = factory(ClientCard::class)->states('authorized')->create([
            'client_id' => $this->client->id,
        ]);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
            'client_company_payment_id' => $clientCompanyPayment->id,
            'client_card_id' => $clientCard->id,
            'tax_percent' => 2.51,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        $invoice = factory(Invoice::class)->states('pending')->create([
            'order_id' => $order->id,
            'client_card_id' => $clientCard->id
        ]);

        $showInvoice = factory(IuguInvoice::class)->states('canceled')->make([
            'order_id' => $invoice->id
        ])->toArray();

        $iuguService->shouldReceive('cancelCharge')
            ->withAnyArgs()
            ->andReturn($showInvoice);

        (new InvoiceService())->cancel($invoice);

        $invoice = $invoice->fresh();

        $canceled = Status::type(Status::CANCELED);

        $this->assertEquals($invoice->status_id, $canceled->id);
    }

    /**
     * Method to test companyPayment with success
     *
     * @return void
     */
    public function testCancelOrderWithoutCreditCard()
    {
        Queue::fake();

        $iuguService = resolve(IuguService::class);

        $order = factory(Order::class)->create([
            'status_id' => Status::type(Status::PENDING)->id,
            'client_id' => $this->client->id,
        ]);

        factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
        ]);

        (new InvoiceService())->cancelOrder($order);

        Queue::assertNotPushed(CancelInvoiceJob::class);
    }
}
