<?php
namespace Tests\Unit\Order;

use App\Client\Client;
use App\Company\Company;
use App\Order\Order;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FirstPurchaseFlagTest extends TestCase
{
    use DatabaseTransactions;

    public function testFirstPurchaseFlag()
    {
        factory(Company::class)->create();
        $client = factory(Client::class)->create();
        $order = factory(Order::class)->create(['client_id' => $client->id]);

        $this->assertTrue($order->fresh()->first_purchase);

        $secondOrder = factory(Order::class)->create(['client_id' => $client->id]);

        $this->assertFalse($secondOrder->fresh()->first_purchase);
    }
}