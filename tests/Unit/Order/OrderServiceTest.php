<?php

namespace Tests\Unit\Order;

use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Order\Order;
use App\Order\OrderProduct;
use App\Core\Entities\Status;
use App\Order\Services\OrderService;

class OrderServiceTest extends TestCase
{
    protected $orderService;

    use DatabaseTransactions;

    /**
     * Method to SetUp the tests
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();

        $this->orderService = new OrderService();
    }

    /**
     * Test isValidMinOrder() should return false
     * @return void
     **/
    public function testIsValidMinOrderShouldReturnFalse()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'min_order' => 1000.00
        ]);

        $order = factory(Order::class)->create([
            'client_id' => $client->id
        ]);

        $kg_total = 60;
        $kg_price = 6;

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
            'kg_total' => $kg_total,
            'kg_price' => $kg_price,
        ]);

        $response = $this->orderService->isValidMinOrder($order);

        $this->assertFalse($response);
    }

    /**
     * Test isValidAccountBallance() should return true
     * @return void
     **/
    public function testIsValidAccountBallanceShouldReturnTrue()
    {
        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $this->company->id,
            'client_id' => $client->id,
            'account_balance' => 500.00
        ]);

        $order = factory(Order::class)->create([
            'client_id' => $client->id
        ]);

        $kg_total = 60;
        $kg_price = 6;

        $orderProduct = factory(OrderProduct::class)->create([
            'order_id' => $order->id,
            'status_id' => Status::type(Status::PENDING)->id,
            'kg_total' => $kg_total,
            'kg_price' => $kg_price,
        ]);

        $response = $this->orderService->isValidAccountBallance($order);

        $this->assertTrue($response);
    }
}
