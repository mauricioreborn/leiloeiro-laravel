<?php

namespace Tests\Unit\Season;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;

use App\Season\SeasonProduct;
use App\Season\SeasonType;
use App\Season\Services\SeasonService;
use App\Company\Company;
use App\Mobile\Exceptions\MobileException;

class SeasonServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to SetUp the tests
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
    }

    /**
     * Method to test if an product is returned on seasonProducts
     *
     * @return void
     */
    public function testGetChristmasProducts()
    {
        $seasonType = factory(SeasonType::class)->create([
            'company_id' => $this->company->id
        ]);

        $seasonProducts = factory(SeasonProduct::class)->create(
            [
                'season_id' => $seasonType->id
            ]
        );

        $seasonService = new SeasonService();
        $products = $seasonService->getAllSeasonProducts($this->company->id);
        $this->assertEquals($products[$seasonType->name][0], $seasonProducts->product_code);
    }


    /**
     * Method to test if an product is returned on season colors name
     *
     * @return void
     */
    public function testGetCustomColorsBySeasonNameShouldReturnEqual()
    {
        $seasonProduct = factory(SeasonProduct::class)->create();
        $seasonService = new SeasonService();

        $customColors = $seasonProduct->seasonType['custom_colors'];
        $seasonColors = $seasonService->getCustomColorBySeasonName($seasonProduct->seasonType->name);

        $this->assertArraySubset($customColors, $seasonColors);
    }

    /**
     * Method to test if an product is returned on season colors name with another company
     * and expect a MobileException
     *
     * @return void
     */
    public function testGetCustomColorsBySeasonNameWithAnotherCompanyExpectMobileException()
    {
        $newCompany = factory(Company::class)->create();

        $seasonProduct = factory(SeasonProduct::class)->create([
            'season_id' => function () use ($newCompany) {
                return factory(SeasonType::class)->create([
                    'custom_colors' => [
                        'card_bg' => '#000'
                    ],
                ]);
            }
        ]);

        $seasonService = new SeasonService();

        $customColors = $seasonProduct->seasonType['custom_colors'];

        $this->expectException(MobileException::class);
        
        $seasonColors = $seasonService->getCustomColorBySeasonName(
            $seasonProduct->seasonType->name,
             $newCompany->id
        );
    }

    /**
     * @return void
     */
    public function testSeasonProductWithDeletedAtShouldNotAppearInResponse()
    {
        $seasonType = factory(SeasonType::class)->create([
            'company_id' => $this->company->id
        ]);

        $seasonProducts = factory(SeasonProduct::class)->create(
            [
                'season_id' => $seasonType->id
            ]
        );

        $seasonProductWithDeletedAt = factory(SeasonProduct::class)->create(
            [
                'season_id' => $seasonType->id,
                'deleted_at' => date("Y-m-d H:i:s"),
            ]
        );

        $seasonService = new SeasonService();
        $products = $seasonService->getAllSeasonProducts($this->company->id);
        $this->assertCount(1, $products[$seasonType->name]);
    }

    /**
     * @return void
     */
    public function testSeasonWithStartAtOnNextWeekCantBeReturned()
    {
        $start_season = Carbon::now()->addDays(7);

        $seasonType = factory(SeasonType::class)->create([
            'company_id' => $this->company->id,
            'start_season' => $start_season,
            'name' => 'soukland'
        ]);

        $seasonProducts = factory(SeasonProduct::class)->create(
            [
                'season_id' => $seasonType->id
            ]
        );

        $seasonProductWithDeletedAt = factory(SeasonProduct::class)->create(
            [
                'season_id' => $seasonType->id,
            ]
        );

        $seasonService = new SeasonService();
        $products = $seasonService->getAllSeasonProducts($this->company->id);
        $this->assertCount(0, $products);
    }
}
