<?php

namespace Tests\Unit\Helpers;

use App\Core\Helpers\JWT;
use App\User;
use Carbon\Carbon;
use Firebase\JWT\JWT as FJWT;
use Tests\TestCase;

class JWTTest extends TestCase
{

    public function testDecode()
    {
        $expiration = Carbon::now()->addDay();
        $jwt = FJWT::encode(['test' => 1], config('app.jwt_key'));
        $token = (array)JWT::decode($jwt, config('app.jwt_key'), ['HS256']);
        $this->assertEquals($token, [
            'test' => true,
        ]);
    }

    public function testEncode()
    {
        $expiration = Carbon::now()->addDay();
        $jwt = JWT::encode(1, $expiration, 'a-type');
        $token = (array) FJWT::decode($jwt, config('app.jwt_key'), ['HS256']);
        $this->assertEquals($token, [
            'iss' => config('app.url'),
            'exp' => $expiration->timestamp,
            'iat' => Carbon::now()->timestamp,
            'type' => 'a-type',
            'uid' => 1,
        ]);
    }

    public function testEncodeUser()
    {
        $jwt = JWT::encodeUser($userId = 0);
        $token = (array)JWT::decode($jwt);
        $this->assertEquals($token, [
            'iss' => config('app.url'),
            'exp' => Carbon::now()->addDay()->timestamp,
            'iat' => Carbon::now()->timestamp,
            'type' => JWT::USER_TYPE,
            'uid' => 0,
        ]);
    }

    public function testEncodeUserWithCustomExpiration()
    {
        $userId = 0;
        $expiration = Carbon::now()->addDay(3);
        $jwt = JWT::encodeUser($userId, $expiration);
        $token = (array)JWT::decode($jwt);
        $this->assertEquals($token, [
            'iss' => config('app.url'),
            'exp' => $expiration->timestamp,
            'iat' => Carbon::now()->timestamp,
            'type' => JWT::USER_TYPE,
            'uid' => $userId,
        ]);
    }

    public function testEncodeSA()
    {
        $jwt = JWT::encodeSA($owner = 'helipa');
        $token = (array)JWT::decode($jwt);
        $this->assertEquals($token, [
            'iss' => config('app.url'),
            'iat' => Carbon::now()->timestamp,
            'type' => JWT::SERVICE_ACCOUNT_TYPE,
            'uid' => 'helipa',
        ]);
    }
}
