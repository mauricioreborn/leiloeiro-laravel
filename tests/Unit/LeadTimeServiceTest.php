<?php

namespace Tests\Unit;

use App\Client\Client;
use App\Company\Company;
use App\LeadTime\Http\Factories\Search;
use App\LeadTime\LeadTime;
use App\LeadTime\Services\LeadTimeService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;

class LeadTimeServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to set up client and jtw
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
    }

    /**
     * Test checkWeekDay method with weekend day set as date. Should return next week day
     * @return void
     **/
    public function testCheckWeekDayWithWeekendShouldReturnNextWeekDay()
    {
        $date = Carbon::create(date('Y'), 01, 04, 0, 0, 0);

        $leadTimeService = new LeadTimeService();
        $response = $leadTimeService->checkWeekDay($date);

        $this->assertEquals($date->nextWeekday()->format('Y-m-d'), $response->format('Y-m-d'));
    }

    /**
     * Test checkWeekDay method with holiday (christmas) set as date. Should return next week day
     * @return void
     **/
    public function testCheckWeekDayWithHolydayShouldReturnNextWeekDay()
    {
        $date = Carbon::create(date('Y'), 12, 25, 0, 0, 0);

        $leadTimeService = new LeadTimeService();
        $response = $leadTimeService->checkWeekDay($date);

        $this->assertEquals($date->nextWeekday()->format('Y-m-d'), $response->format('Y-m-d'));
    }

    /**
     * Test checkWeekDay method with weekday set as date. Should return false
     * @return void
     **/
    public function testCheckWeekDayWithWeekDayShouldReturnFalse()
    {
        $date = Carbon::create(date('Y'), 01, 02, 0, 0, 0);

        $leadTimeService = new LeadTimeService();
        $response = $leadTimeService->checkWeekDay($date);
        $this->assertFalse($response);
    }

    /**
     * Method to get logistica with all days is zero
     *
     * @return null
     */
    public function testGetLogisticaWithOperatesZero()
    {
        Carbon::setTestNow(now()->next(Carbon::MONDAY));

        $client1 = factory(Client::class)->create();
        $company2 = factory(Company::class)->create();

        $client1->companies()->save($company2);

        $leadtime1 = factory(LeadTime::class)->create([
            'name' => 'SAO PAULO - SP',
            'cnpj' => $client1->cnpj,
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'operates_saturday' => 0,
            'operates_sunday' => 0,
            'company_id' => $this->company->id
        ]);

        $hour = substr($leadtime1->limit_hour, 0, 2);
        $minute = substr($leadtime1->limit_hour, 2, 4);

        $shipmentDate = now()->addBusinessDays($leadtime1->days);

        $purchaseLimit = $shipmentDate->copy()
            ->subBusinessDays()
            ->hour($hour)
            ->minute($minute)
            ->second(0);

        $logisticDate = $purchaseLimit->copy()->subBusinessDays($leadtime1->prediction)->addSecond();

        $deliveryDate = $shipmentDate->copy()->addDays($leadtime1->days);
        $daysToDelivery = now()->startOfDay()->diffInDays($deliveryDate);
        $weeks = ceil($daysToDelivery/7);

        $return = (new LeadTimeService())->getLogistica(
            [$client1->id],
            $leadtime1->code,
            $this->company->id
        );

        $clientLogistic = $return[$client1->id];

        $this->assertEquals(array_get($clientLogistic, 'visao_logistica'), $logisticDate->toDateString());
        $this->assertEquals(array_get($clientLogistic, 'dias_ate_entrega'), $daysToDelivery);
        $this->assertEquals(array_get($clientLogistic, 'semanas_subtrair'), $weeks);
        $this->assertEquals(array_get($clientLogistic, 'data_embarque'), $shipmentDate->toDateString());
        $this->assertEquals(array_get($clientLogistic, 'data_entrega'), $deliveryDate->toDateString());
        $this->assertEquals(array_get($clientLogistic, 'aceita_compra'), true);
        $this->assertEquals(array_get($clientLogistic, 'limite_compra'), $purchaseLimit->toDateTimeString());
        $this->assertEquals(
            Carbon::parse($return[$client1->id]['hora_considerada'])->format("H:i"),
            now()->format('H:i')
        );

        Carbon::setTestNow();
    }

    /**
     * Method to get logistica method without days
     *
     * @return null
     */
    public function testGetLogisticaWithoutDays()
    {
        Carbon::setTestNow(now()->next(Carbon::MONDAY));

        $client1 = factory(Client::class)->create();
        $company2 = factory(Company::class)->create();

        $client1->companies()->save($company2);

        $leadtime1 = factory(LeadTime::class)->create([
            'name' => 'SAO PAULO - SP',
            'cnpj' => $client1->cnpj,
            'days' => 1,
            'prediction' => 9,
            'limit_hour' => 2300,
            'monday' => 0,
            'tuesday' => 0,
            'wednesday' => 0,
            'thursday' => 0,
            'friday' => 0,
            'saturday' => 0,
            'sunday' => 0,
            'operates_saturday' => 1,
            'operates_sunday' => 1,
            'company_id' => $this->company->id
        ]);

        $hour = substr($leadtime1->limit_hour, 0, 2);
        $minute = substr($leadtime1->limit_hour, 2, 4);

        $shipmentDate = now()->addBusinessDays($leadtime1->days);

        $purchaseLimit = $shipmentDate->copy()
            ->subBusinessDays()
            ->hour($hour)
            ->minute($minute)
            ->second(0);

        $logisticDate = $purchaseLimit->copy()->subBusinessDays($leadtime1->prediction)->addSecond();

        $deliveryDate = $shipmentDate->copy()->addDays($leadtime1->days);
        $daysToDelivery = now()->startOfDay()->diffInDays($deliveryDate);
        $weeks = ceil($daysToDelivery/7);

        $return = (new LeadTimeService())->getLogistica(
            [$client1->id],
            $leadtime1->code,
            $this->company->id
        );

        $clientLogistic = $return[$client1->id];

        $this->assertEquals(array_get($clientLogistic, 'aceita_compra'), false);
        $this->assertEquals(array_get($clientLogistic, 'visao_logistica'), now()->addBusinessDays()->toDateString());
        $this->assertEquals(
            array_get($clientLogistic, 'data_embarque'),
            now()->addBusinessDays($leadtime1->prediction)->toDateString()
        );
    }


    /**
     * Method to get Logistica with all days on the week
     *
     * @return null
     */
    public function testGetLogisticaWithAllDays()
    {
        $client1 = factory(Client::class)->create();
        $company2 = factory(Company::class)->create();

        $client1->companies()->save($company2);

        $leadtime1 = factory(LeadTime::class)->create([
            'name' => 'SAO PAULO - SP',
            'cnpj' => $client1->cnpj,
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'operates_saturday' => 1,
            'operates_sunday' => 1,
            'company_id' => $this->company->id
        ]);

        $leadTimeService = new LeadTimeService();

        $logistic = $leadTimeService->nextDeliveryDay($leadtime1);
        $inicioVisaoLotistica = array_get($logistic, 'logistic')->toDateString();

        $return = $leadTimeService->getLogistica(
            [$client1->id],
            $leadtime1->code,
            $this->company->id
        );

        $shipment = array_get($logistic, 'shipment');
        $delivery = $shipment->copy()->addDay()->endOfDay();

        $diasAteEntrega = now()->diffInDays($delivery, false);
        $semanasSubtrair = ceil($diasAteEntrega/7);

        $dataEmbarque = $shipment->toDateString();
        $dataEntrega = date(
            'Y-m-d',
            strtotime('+'.$leadtime1->days.' days',strtotime($dataEmbarque))
        );

        $_diaAnterior = array_get($logistic, 'purchaseLimit')->toDateTimeString();

        $this->assertEquals($return[$client1->id]['visao_logistica'], $inicioVisaoLotistica);
        $this->assertEquals($return[$client1->id]['dias_ate_entrega'], $diasAteEntrega);
        $this->assertEquals($return[$client1->id]['semanas_subtrair'], $semanasSubtrair);
        $this->assertEquals($return[$client1->id]['data_embarque'], $dataEmbarque);
        $this->assertEquals($return[$client1->id]['data_entrega'], $dataEntrega);
        $this->assertEquals($return[$client1->id]['aceita_compra'], true);
        $this->assertEquals($return[$client1->id]['limite_compra'], $_diaAnterior);

    }

    /**
     * Method to get Logistica with an another cnpj
     *
     * @return null
     */
    public function testGetLogisticaWithAnotherCnpj()
    {
        $client1 = factory(Client::class)->create();
        $company2 = factory(Company::class)->create();

        $faker = \Faker\Factory::create('pt_BR');
        $client1->companies()->save($company2);

        $leadtime1 = factory(LeadTime::class)->create([
            'name' => 'SAO PAULO - SP',
            'cnpj' => $faker->cnpj(),
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'operates_saturday' => 1,
            'operates_sunday' => 1,
            'company_id' => $this->company->id
        ]);


        $leadTimeService = new LeadTimeService();
        $return = $leadTimeService->getLogistica(
            [$client1->id],
            $leadtime1->code,
            $this->company->id
        );

        $this->assertEquals(count($return), 1);
        $this->assertEquals($return[$client1->id]['aceita_compra'], false);
    }


    /**
     * Method to get Logistica with an another company id
     *
     * @return null
     */
    public function testGetLogisticaWithAnotherCompanyId()
    {
        $client1 = factory(Client::class)->create();
        $company2 = factory(Company::class)->create();

        $faker = \Faker\Factory::create('pt_BR');
        $client1->companies()->save($company2);
        $leadtime1 = factory(LeadTime::class)->create([
            'name' => 'SAO PAULO - SP',
            'cnpj' => $client1->cnpj,
            'days' => 1,
            'prediction' => 1,
            'limit_hour' => 2300,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'operates_saturday' => 1,
            'operates_sunday' => 1,
            'company_id' => $company2->id
        ]);


        $leadTimeService = new LeadTimeService();
        $return = $leadTimeService->getLogistica([$client1->id], $leadtime1->code, $company2->id);

        $logistic = $leadTimeService->nextDeliveryDay($leadtime1);
        $shipment = array_get($logistic, 'shipment');
        $delivery = $shipment->copy()->addDay()->endOfDay();
        $inicioVisaoLotistica = array_get($logistic, 'logistic')->toDateString();

        $diasAteEntrega = now()->diffInDays($delivery, false);
        $semanasSubtrair = ceil($diasAteEntrega/7);


        $dataEmbarque = $shipment->toDateString();
        $dataEntrega = date(
            'Y-m-d',
            strtotime('+'.$leadtime1->days.' days',strtotime($dataEmbarque)
            )
        );

        $_diaAnterior = array_get($logistic, 'purchaseLimit')->toDateTimeString();

        $this->assertEquals($return[$client1->id]['visao_logistica'], $inicioVisaoLotistica);
        $this->assertEquals($return[$client1->id]['dias_ate_entrega'], $diasAteEntrega);
        $this->assertEquals($return[$client1->id]['semanas_subtrair'], $semanasSubtrair);
        $this->assertEquals($return[$client1->id]['data_embarque'], $dataEmbarque);
        $this->assertEquals($return[$client1->id]['data_entrega'], $dataEntrega);
        $this->assertEquals($return[$client1->id]['aceita_compra'], true);
        $this->assertEquals($return[$client1->id]['limite_compra'], $_diaAnterior);
    }
}
