<?php

namespace Tests\Unit\BidLogs;

use App\Auth\User;
use App\Bid\Bid;
use App\Bid\BidLog;
use App\Bid\Services\BidLogService;
use App\Client\Client;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BidLogServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedUser();
    }

    /**
     * Method to test if is possible
     *
     * @return void
     */
    public function testAddABidLogWithClient()
    {
        $client = factory(Client::class)->create()->first();

        $bid = factory(Bid::class)->create()->first();
        $bidLogService = new BidLogService();

        $bidLogParams = [
            'client_id' => $client->id,
            'bid_id' => $bid->id,
            'box_amount' => $bid->box_amount
        ];

        $bidLogService->saveBidLog($bidLogParams);

        $insertedBid = BidLog::first();
        $this->assertEquals($bidLogParams['client_id'], $insertedBid->client_id);
        $this->assertEquals($bidLogParams['bid_id'], $insertedBid->bid_id);
        $this->assertEquals($bidLogParams['box_amount'], $insertedBid->box_amount);
        $this->assertNull($insertedBid->user_id);

    }

    /**
     * Method to test if is possible
     *
     * @return void
     */
    public function testAddABidLogWithUser()
    {
        $user = factory(User::class)->create()->first();

        $bid = factory(Bid::class)->create()->first();
        $bidLogService = new BidLogService();

        $bidLogParams = [
            'user_id' => $user->id,
            'bid_id' => $bid->id,
            'box_amount' => $bid->box_amount
        ];

        $bidLogService->saveBidLog($bidLogParams);

        $insertedBid = BidLog::first();
        $this->assertEquals($bidLogParams['user_id'], $insertedBid->user_id);
        $this->assertEquals($bidLogParams['bid_id'], $insertedBid->bid_id);
        $this->assertEquals($bidLogParams['box_amount'], $insertedBid->box_amount);
        $this->assertNull($insertedBid->client_id);
    }

    /**
     * Method to test logs list with user modification
     *
     * @return void
     */
    public function testGetLogsByBid()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $bid = factory(Bid::class)->create([
            'client_id' => $client->id,
            'box_amount' => '5',
            'weight' => '50',
            'kg_price' => '2',
            'price' => '100',
            'partial_accept' => true,
        ]);

        factory(BidLog::class)->create([
            'bid_id' => $bid->id,
            'user_id' => null,
            'client_id' => $client->id,
            'box_amount' => 10,
        ]);

        factory(BidLog::class)->create([
            'bid_id' => $bid->id,
            'user_id' => $user->id,
            'client_id' => null,
            'box_amount' => 2,
        ]);

        $logs = (new BidLogService())->getLogsByBid($bid->id);

        $client_log = $logs->first();
        $user_log = $logs->last();

        $this->assertEquals($client_log->box_amount, 10);
        $this->assertEquals($client_log->weight, 100);
        $this->assertEquals($client_log->price, 200);

        $this->assertEquals($user_log->box_amount, 2);
        $this->assertEquals($user_log->weight, 20);
        $this->assertEquals($user_log->price, 40);

        $this->assertEquals($client_log->responsible, $client->name);
        $this->assertEquals($user_log->responsible, $user->name);
    }
}