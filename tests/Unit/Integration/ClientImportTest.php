<?php

namespace Tests\Unit\Integration;

use App\ClientCompanies\ClientCompany;
use App\Client\Client;
use App\Client\ClientCompanyPayment;
use App\Client\Seller;
use App\Client\Services\ClientService;
use App\ClientsCompanies\Services\ClientsCompaniesService;
use App\Company\Company;
use App\Company\CompanyPayment;
use App\DistributionCenter\DistributionCenter;
use App\Fefo\FefoPrice;
use App\Fefo\Services\FefoPriceService;
use App\LeadTime\LeadTime;
use App\LeadTime\Services\LeadTimeService;
use App\Payment\Payment;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class ClientImportTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedClient();
    }

    /**
     * Method to test the tirolez client import
     */
    public function testTirolezClientImport()
    {
        $data = collect($this->generateImportFaker());

        $companyCredit = factory(CompanyPayment::class)->states('companyCredit')->create([
            'company_id' => $this->company->id
        ]);

        $anotherCompany = factory(Company::class)->create();

        $otherSeller = factory(Seller::class)->create([
            'name' => 'Vendedor2',
            'company_id' => $anotherCompany->id,
            'email' => $data['seller']['email'],
        ]);

        $clientData = $data->only('cnpj', 'name', 'password', 'email_verified')->toArray();
        $clientCompanyData = $data->only(
            'min_order',
            'account_balance',
            'client_code',
            'address',
            'state',
            'city',
            'payment_conditions',
            'status',
            'company_id'
        )->toArray();

        $sellerData = $data['seller'];

        $clientCompany = (new ClientsCompaniesService())->processCreateClientCompanies(
            $this->company,
            $clientData,
            $clientCompanyData,
            $sellerData
        );

        $this->assertEquals(sanitize_number($data['cnpj']), $clientCompany->client->cnpj);
        $this->assertEquals($data['client_code'], $clientCompany->client_code);
        $this->assertEquals($data['name'], $clientCompany->client->name);
        $this->assertEquals($data['account_balance'], $clientCompany->account_balance);
        $this->assertEquals($data['min_order'], $clientCompany->min_order);
        $this->assertEquals($data['address'], $clientCompany->address);
        $this->assertEquals($data['state'], $clientCompany->state);
        $this->assertEquals($data['payment_conditions'], $clientCompany->payment_conditions);
        $this->assertEquals($data['seller']['name'], $clientCompany->client->seller[0]->nome);
        $this->assertEquals($data['seller']['email'], $clientCompany->client->seller[0]->email);
        $this->assertEquals(sanitize_number($data['seller']['phone']), $clientCompany->client->seller[0]->phone);

        $this->assertEquals(1, $clientCompany->client->companyPayments()->count());

        $sellers = Seller::where('email', $otherSeller->email)->get();
        $seller = $sellers->firstWhere('company_id', $this->company->id);

        $this->assertEquals(2, $sellers->count());
        $this->assertNotEquals($otherSeller->name, $seller->name);
    }

    public function generateImportFaker()
    {
        $faker = \Faker\Factory::create('pt_BR');
        $email = $faker->email;
        return [
            'company_id' => $this->company->id,
            'cnpj' => $faker->cnpj,
            'client_code' => $faker->numberBetween(2454,9500),
            'name' => $faker->name('m'),
            'password' => Str::random(32),
            'email_verified' => 0,
            'clientCodeMatriz' =>  $faker->numberBetween(2454,9500),
            'status' => true,
            'account_balance' => 4000,
            'min_order' => 1500,
            'address' => $faker->address,
            'state' => $faker->country,
            'city' => $faker->city,
            'payment_conditions' => $faker->streetName,
            'seller' => [
                'name' => $faker->firstName(),
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
            ],
        ];
    }
}
