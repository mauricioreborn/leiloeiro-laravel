<?php
namespace Tests\Unit\Integration;

use App\Client\Client;
use App\ClientCompanies\ClientCompany;
use App\Company\Company;
use App\DistributionCenter\DistributionCenter;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\LeadTime\Services\LeadTimeService;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LeadtimeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Method to set configutation
     *
     * @return null
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpMockedClient();
    }

    public function testNewLeadTimeInsertion()
    {
        $company = factory(Company::class)->create();

        $distributionCenter = factory(DistributionCenter::class)->create([
            'company_id' => $company->id,
            'code' => 666,
            'name' => 'CD SÃO PAULO',
            'state' => 'SP'
        ]);

        $client = factory(Client::class)->create();

        $clientCompany = factory(ClientCompany::class)->create([
            'company_id' => $company->id,
            'client_id' => $client->id,
            'client_code' => 322440
        ]);

        $leadtimeService = new LeadTimeService();
        $leadtimeService->getClientWithCodes($company);

        $days = [
            'dias' => 1,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
        ];

        $logisticInformation = '9:00am at 18:00pm';

        $leadtime = $leadtimeService->importLeadTime(
            $company,
            322440,
            $distributionCenter,
            $days['dias'],
            $days['tuesday'],
            $days['wednesday'],
            $days['thursday'],
            $days['friday'],
            $days['saturday'],
            $days['sunday'],
            $days['monday'],
            $logisticInformation
        );

        $this->assertEquals($leadtime->cod_origem, $distributionCenter->code);
        $this->assertEquals($leadtime->logistic_information, $logisticInformation);
        $this->assertEquals($leadtime->monday, $days['monday']);
        $this->assertEquals($leadtime->tuesday, $days['tuesday']);
        $this->assertEquals($leadtime->wednesday, $days['wednesday']);
        $this->assertEquals($leadtime->friday, $days['friday']);
    }
}
