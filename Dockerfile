FROM php:7.3-fpm-alpine

ENV NEW_RELIC_VERSION=9.6.0.255
RUN curl -sL https://download.newrelic.com/php_agent/release/newrelic-php5-${NEW_RELIC_VERSION}-linux-musl.tar.gz | tar -C /tmp -zx && \
    export NR_INSTALL_USE_CP_NOT_LN=1 && \
    export NR_INSTALL_SILENT=1 && \
    /tmp/newrelic-php5-*/newrelic-install install && \
    rm -rfv /tmp/newrelic-php5-* /tmp/nrinstall* /usr/local/etc/php/conf.d/newrelic.ini

RUN apk add --no-cache \
    freetype freetype-dev \
    libjpeg-turbo libjpeg-turbo-dev \
    libpng libpng-dev \
    libwebp-dev \
    libxpm-dev \
    libzip-dev \
    zlib-dev
RUN docker-php-ext-install bcmath iconv mysqli gd opcache pcntl pdo pdo_mysql zip
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=1.9.1
COPY php-fpm.conf /usr/local/etc/php-fpm.d/www.conf
RUN cp -v /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini && \
    sed -i -E 's/^expose_php ?= .*$/expose_php = Off/' /usr/local/etc/php/php.ini && \
    sed -i -E 's/^memory_limit ?= .*$/memory_limit = -1/' /usr/local/etc/php/php.ini && \
    sed -i -E 's/^post_max_size ?=.*$/post_max_size = 10M/' /usr/local/etc/php/php.ini && \
    sed -i -E 's/^upload_max_filesize ?=.*$/upload_max_filesize = 10M/' /usr/local/etc/php/php.ini && \
    sed -i -E 's/^;opcache.enable ?=.*$/opcache.enable=1/' /usr/local/etc/php/php.ini && \
    sed -i -E 's/^;opcache.validate_timestamps ?=.*$/opcache.validate_timestamps=0/' /usr/local/etc/php/php.ini && \
    grep '^opcache' /usr/local/etc/php/php.ini

USER www-data
COPY --chown=www-data:www-data composer.json /usr/src/app/composer.json
COPY --chown=www-data:www-data composer.lock /usr/src/app/composer.lock
WORKDIR /usr/src/app
RUN composer install -n --no-autoloader --no-dev \
    && rm -rf $HOME/.composer/cache/

COPY --chown=www-data:www-data . /usr/src/app
RUN composer dump-autoload --optimize --no-dev \
    && php artisan route:cache
